﻿namespace ProcessResources
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Xml;
    using ORM;

    [Serializable]
    public class GenericProcessResources
    {
        /*
         * Xml Call Type
         */
        public XMLAPITypes XMLApiType { get; set; }
        public string PartnerName { get; set; }
        public int PartnerId { get; set; }
        public string AgentDutyCode { get; set; }
        public string ProviderName { get; set; }
        public int ProviderId { get; set; }
        public string BrandName { get; set; }
        public string Language { get; set; }
        public int BrandId { get; set; }
        public bool ModifyProcess { get; set; }

        /*
         * Rate Qualifier
         */
        public string ProviderRateQualifier { get; set; }
        public string RequestedRateQualifier { get; set; }
        public bool tourInfoTag { get; set; }
        public bool IsThriftyRQerror { get; set; }
        public string CorpDiscountNo { get; set; }

        /*
        * Location Properties
        */
        public string PickupLocationCode { get; set; }
        public string PickupLocationName { get; set; }
        public string PickupLocationExtendedCode { get; set; }
        public string PickupCTLocationId { get; set; }

        public string DropoffLocationCode { get; set; }
        public string DropoffLocationName { get; set; }
        public string DropoffLocationExtendedCode { get; set; }
        public string DropoffCTLocationId { get; set; }
        public string DestCountryCode { get; set; }

        /*
        * Source Country Properties
        */
        public string SourceCountryCode { get; set; }
        public string SourceCountryName { get; set; }

        /*
        * Datetime Properties
        */
        public DateTime PickupDateTime { get; set; }
        public DateTime DropoffDatetime { get; set; }

        /*
        * Provider Properties
        */
        public string ProviderEndPointUrl { get; set; }
        public string ConsumerIP { get; set; }

        /*
        * Request Parameter Properties
        */
        public XmlDocument OriginalRequest { get; set; }
        public XmlDocument ValidatedRequest { get; set; }
        public XmlDocument ReceivedResponse { get; set; }
        public XmlDocument ValidatedResponse { get; set; }

        /*
        * Request Accessories Properties
        */
        public bool IsExtraValid { get; set; }
    }

    [Serializable]
    public class VehicleAvailRateRQResources : GenericProcessResources
    {
        /*
        * Partner and Brand Related Properties
        */
        public bool IsVehicleLimitEnabled { get; set; }
        public int VehicleLimit { get; set; }
        public bool IsVehiclePrefEnabled { get; set; }

        public List<CarDetails> Cars { get; set; }

        public VehicleAvailRateRQResources()
        {
            Cars = new List<CarDetails>();
        }
    }

    [Serializable]
    public class VehicleResRQResources : GenericProcessResources
    {
        /*
        * Booking
        */
        public string CustomerFirstname { get; set; }
        public string CustomerSurname { get; set; }
        public string CustomerPhone { get; set; }
        public string CustomerEmail { get; set; }
        public string Address { get; set; }
        public string CityName { get; set; }
        public string PostalCode { get; set; }
        public string CountryOfResidenceCode { get; set; }
        public bool IsMailFunctionEnabled { get; set; }
        public DateTime OrderRequestDatetime { get; set; }
        public bool IsGoldNumberError { get; set; }
        public bool IsFlightNo { get; set; }
        public bool IsCTAvailload { get; set; }
        public bool IsCTResload { get; set; }
        public bool IsThermeonAccess { get; set; }
        public string BirthDay { get; set; }
        public string DriverLicence { get; set; }
        public string expireDate { get; set; }
        public string passportNo { get; set; }
        public string IssueCountry { get; set; }

        /*
         * Location Country Details
         */
        public string PickUpCountryCode { get; set; }
        public string PickUpCountryName { get; set; }
        public string DropOffCountryCode { get; set; }
        public string DropOffCountryName { get; set; }

        /*
         * Reservation request Details
         */
        public string phoneTechType { get; set; }
        public string phoneAreaCode { get; set; }
        public string TransportCode { get; set; }
        public string TransportNumber { get; set; }
        public string operatingCompany { get; set; }
        public string cardType { get; set; }
        public string cardCode { get; set; }
        public string cardNumber { get; set; }
        public string CardExpDate { get; set; }
        public string CardseriesCode { get; set; }
        public string Cardholder { get; set; }
        public string refID { get; set; }
        public string refDateTime { get; set; }
        public string refUrl { get; set; }
        public string memberId { get; set; }
        public string programId { get; set; }
        public string travelSector { get; set; }
        /*
         * Accessories List 
         */
        public List<Accessory> AccesoriesList { get; set; }
        public List<CTAccessory> CTAccesoriesList { get; set; }

        /*
         * Vehicle Details List
         */
        public List<VehicleDetail> VehicleDetailsList { get; set; }

        public bool HasError { get; set; }
        public string ErrorDescription { get; set; }
        public string ConfirmationID { get; set; }
        public string Thermeon_ConfirmationID { get; set; }

        public BookedVehicle ReservedVehicle { get; set; }
    }

    [Serializable]
    public class BookedVehicle
    {
        public string Name { get; set; }
        public string ImagePath { get; set; }
        public int Passenger_Count { get; set; }
        public int Baggage_Count { get; set; }
        public bool AirCond { get; set; }
        public string TransmissionTP { get; set; }
        public string FuelTP { get; set; }
        public string DriverTP { get; set; }
        public int Door_Count { get; set; }
        public int Veh_Category { get; set; }
        public string Currency { get; set; }
        public decimal Price { get; set; }
        public decimal DrpFee { get; set; }
        public decimal priceInclude { get; set; }
        public string distance { get; set; }
    }



    [Serializable]
    public class CarDetails
    {
        public string Name { get; set; }
        public string ThemeonID { get; set; }
        public string Image { get; set; }

        public string Availability { get; set; }
        public string SippCode { get; set; }
        public string Price { get; set; }
        public string Currency { get; set; }
        public string CurrencySymbol { get; set; }
        public string DistanceIncluded { get; set; }
        public bool IsPrePaid { get; set; }
        public bool IsResRetReturnedCar { get; set; }
        public string NextCarSippCode { get; set; }
        public string CarPassengers { get; set; }
        public string AirConditionIndValue { get; set; }
        public string TransmissionType { get; set; }
        public string FuelType { get; set; }
        public string DriveType { get; set; }
        public string Baggage_Count { get; set; }
        public string DoorCount { get; set; }
        public string VehClassSize { get; set; }
        public string VehicleCategory { get; set; }

    }



    [Serializable]
    public class VehicleDetail
    {
        public string SippCode { get; set; }
        public string RateID { get; set; }
        public string Name { get; set; }
        public string ImagePath { get; set; }
        public string Amount { get; set; }
    }

    [Serializable]
    public class CTAccessory
    {
        public int Quantity { get; set; }
        public string EquipType { get; set; } // accessory code
        //public string CurrencySymbol { get; set; }
        public bool IsSelected { get; set; }
        public bool Delete { get; set; }
    }

    [Serializable]
    public class VehicleRetrivaleRQResources : GenericProcessResources
    {
        /*
        * Booking Retrival
        */
        public string ConfirmationID { get; set; }
        public string EmailAdd { get; set; }
        public string LastName { get; set; }
    }

    [Serializable]
    public class VehicleResModifyRQResources : GenericProcessResources
    {
        /*
        * Booking
        */
        public string CustomerFirstname { get; set; }
        public string CustomerSurname { get; set; }
        public string CustomerPhone { get; set; }
        public string CustomerEmail { get; set; }
        public string Address { get; set; }
        public string CityName { get; set; }
        public string PostalCode { get; set; }
        public string CountryOfResidenceCode { get; set; }
        public bool IsMailFunctionEnabled { get; set; }
        public DateTime OrderRequestDatetime { get; set; }
        public string BirthDay { get; set; }
        public string DriverLicence { get; set; }
        public string expireDate { get; set; }
        public string passportNo { get; set; }
        public string IssueCountry { get; set; }

        /*
         * Location Country Details
         */
        public string PickUpCountryCode { get; set; }
        public string PickUpCountryName { get; set; }
        public string DropOffCountryCode { get; set; }
        public string DropOffCountryName { get; set; }

        /*
         * Accessories List 
         */
        public List<Accessory> AccesoriesList { get; set; }

        /*
         * Vehicle Details List
         */
        public List<VehicleDetail> VehicleDetailsList { get; set; }

        public bool HasError { get; set; }
        public string ErrorDescription { get; set; }
        public string ConfirmationID { get; set; }
        public string Thermeon_ConfirmationID { get; set; }

        public BookedVehicle ReservedVehicle { get; set; }
    }
    [Serializable]
    public class VehicleCancelRQResources : GenericProcessResources
    {
        /*
        * Booking Retrival
        */
        public string ConfirmationID { get; set; }
        public string EmailAdd { get; set; }
        public string LastName { get; set; }
    }

    [Serializable]
    public class VehicleRentalRQResources : GenericProcessResources
    {
        /*
        * Partner and Brand Related Properties
        */
        

    }
    
}