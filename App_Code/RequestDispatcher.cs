﻿using System;
using System.Web;

/// <summary>
/// Summary description for RequestDispatcher
/// </summary>
public class RequestDispatcher : System.Web.Routing.IRouteHandler
{
    public IHttpHandler GetHttpHandler(System.Web.Routing.RequestContext requestContext)
    {
        RouteResolver routeResolver = new RouteResolver();
        return routeResolver;
    }

    public class RouteResolver : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            ProcessEngine engine = new ProcessEngine();
            engine.initProcess(context);
        }

        #region IHttpHandler Members

        public bool IsReusable
        {
            get { throw new NotImplementedException(); }
        }

        #endregion
    }

}