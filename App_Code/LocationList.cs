﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Services;
using System.Web.Script.Services;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Configuration;
using System.Text;
using CMSConfigs;

[ScriptService]
public class LocationList : WebService
{
    SqlConnection conn = new SqlConnection(CMSSettings.connctionString);

    public class location
    {
        public string CountryCode { get; set; }
        public string CountryName { get; set; }
        public string LocationCode { get; set; }
        public string LocationName { get; set; }
        public string DropoffEnable { get; set; }
        public string CommercialVehiclesEnable { get; set; }
        public string TerminalNo { get; set; }
        public string DefaultTerminalFlag { get; set; }
        public string LocationType { get; set; }
        public string TerminalsEnabled { get; set; }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public List<location> getLocationList(string loc, string lang)
    {
        DataSet ds = new DataSet(); 
        using (SqlConnection connLocations = new SqlConnection(ConfigurationManager.ConnectionStrings["DatabaseEntities"].ConnectionString))
        {
            using (SqlDataAdapter commandLocations = new SqlDataAdapter("usp_GetLocationsByStringCommercialVehicles", connLocations))
            {
                Logger.Debug("commandLocations====" + commandLocations);

                commandLocations.SelectCommand.CommandType = CommandType.StoredProcedure;
                commandLocations.SelectCommand.Parameters.Add("@loc", SqlDbType.NVarChar).Value = HttpContext.Current.Server.HtmlEncode(loc);
                commandLocations.SelectCommand.Parameters.Add("@LanguageCode", SqlDbType.NVarChar).Value = HttpContext.Current.Server.HtmlEncode(lang);
                connLocations.Open();
                ds = new DataSet();
                commandLocations.Fill(ds);
            }
            connLocations.Close();
        }

        

        // store results in list
        List<location> locations = new List<location>();
        foreach (DataRow dr in ds.Tables[0].Rows)
        {
            //LocationID, L.CountryIDFK, Loc.CountryName, L.LocationName,L.LocationType, L.AirportCode, C.CountryCode

            location location = new location();
            location.LocationName = dr["LocationName"].ToString();
            location.LocationCode = dr["LocationCode"].ToString();
            location.CountryCode = dr["CountryCode"].ToString();
            location.CountryName = dr["CountryName"].ToString();
            location.LocationType = dr["LocationType"].ToString();
            locations.Add(location);
        }

        return locations.OrderBy(w => w.LocationName).ToList();
    }

    [WebMethod]
    public List<location> getLocations(string loc, string lang)
    {
        DataSet ds = new DataSet();
        List<location> locations = new List<location>();
        using (var command = new SqlCommand("usp_GetLocationsByStringCommercialVehicles", conn))
        {
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.Add("@loc", SqlDbType.NVarChar).Value = HttpContext.Current.Server.HtmlEncode(loc);
            command.Parameters.Add("@LanguageCode", SqlDbType.NVarChar).Value = HttpContext.Current.Server.HtmlEncode(lang);
            conn.Open();
            var sqlReader = command.ExecuteReader();
            if (sqlReader.HasRows)
            {
                while (sqlReader.Read())
                {
                    location location = new location();
                    location.LocationName = sqlReader["LocationName"].ToString();
                    location.LocationCode = sqlReader["LocationCode"].ToString();
                    location.CountryCode = sqlReader["CountryCode"].ToString();
                    location.CountryName = sqlReader["CountryName"].ToString();
                    location.LocationType = sqlReader["LocationType"].ToString();
                    locations.Add(location);
                }
            }
        }

        return locations.OrderBy(w => w.LocationName).ToList();
    }


    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public List<location> test()
    {
        List<location> locations = new List<location>();
        location location = new location();
        location.LocationName = "LocationName";
        location.LocationCode = "AirportCode";
        location.CountryCode = "CountryCode";
        location.CountryName = "CountryName";
        location.LocationType = "LocationType";
        locations.Add(location);

        return locations.OrderBy(w => w.LocationName).ToList();
    }

}
