﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for XMLAPITypes
/// </summary>
public enum XMLProviders
{
    HERTZ,
    DTAG,
    CARTRAWLER,
    THERMEON
}
