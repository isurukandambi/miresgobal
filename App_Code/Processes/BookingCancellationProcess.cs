﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;
using Exceptions;
using ProcessResources;
using ORM;

/// <summary>
/// Summary description for BookingCancellationProcess
/// </summary>
public class BookingCancellationProcess : GenericProcess
{
    protected DatabaseEntities db;
    protected XmlNamespaceManager namespaceManager;
    protected XmlDocument xmlDocument;
    public override GenericProcessResources beginProcess(GenericProcessResources processResource, IProviderChannel providerChannel)
    {
        VehicleCancelRQResources vehCancelRQRes = Reflector.Construct<GenericProcessResources, VehicleCancelRQResources>(processResource);
        
        providerChannel.getValidatedRequest(vehCancelRQRes, namespaceMgr, db);

        string responseFromServer = commitRequest(vehCancelRQRes);
        Logger.Debug("Response From Server : " + responseFromServer);
        XmlDocument responseXmlDocument = convertXmlStringToDocument(responseFromServer);
        vehCancelRQRes.ReceivedResponse = responseXmlDocument;

        if (!isErrorXml(vehCancelRQRes.ReceivedResponse))
        {
            providerChannel.getValidatedResponse(vehCancelRQRes, namespaceMgr, db);
        }
        else
        {
            vehCancelRQRes.ValidatedResponse = vehCancelRQRes.ReceivedResponse;
        }
        //logErrors(vehRetrivalRQRes);

        return vehCancelRQRes;
    }

    public override void execute(BrandResolver brandResolver, PartnerResolver partnerResolver, ProviderResolver providerResolver, string brandParam, string payload, XMLAPITypes xmlApiType, HttpContext context)
    {
        GenericProcessResources processResource = new GenericProcessResources();
        VehicleCancelRQResources CancelProcess = new VehicleCancelRQResources();
        CancelProcess.XMLApiType = xmlApiType;

        CancelProcess.ProviderId = 1;
        try
        {
            setDatabaseEntities(new DatabaseEntities());
            RemoteDebugger.SendXmlForDebugging(payload, "Request Received from partner - " + brandParam);
            Logger.Debug("Requested XML From Partner : \n" + payload);
            xmlDocument = convertXmlStringToDocument(payload);
            //RemoteDebugger.SendXmlForDebugging(XmlUtils.getXmlString(requestXmlDocument), "Original Request");
            CancelProcess.OriginalRequest = xmlDocument;
            setNamespace(namespaceMgr);
            setRequestData(CancelProcess);
            setProvider(CancelProcess);
            //Logger.Debug(CancelProcess);
            setProviderEndpointURL(CancelProcess.ProviderName, CancelProcess);
            IProviderChannel providerChannel = getProviderChannel(CancelProcess.ProviderName);
            processResource = beginProcess(CancelProcess, providerChannel);
            //RemoteDebugger.SendXmlForDebugging(CancelProcess.ProviderName, "Provider122334");
            commitResponse(context, processResource);

        }
        catch (Exception ex)
        {
            try
            {
                //Logger.Debug("Error1234");
                RemoteDebugger.SendXmlForDebugging(ex.Message+"</br>"+ex.StackTrace, "Error Message and Stacktrace");
                Logger.Error(ex);
                Logger.Debug(processResource.XMLApiType);
                processResource.ValidatedResponse = getErrorXML(ex.Message, processResource.XMLApiType);
                //logErrors(processResource);
                commitResponse(context, processResource);
            }
            catch (Exception logErrorEx)
            {
                Logger.Error(ex);
                processResource.ValidatedResponse = getErrorXML(logErrorEx.Message, processResource.XMLApiType);
                commitResponse(context, processResource);
            }
        }
    }

    protected void setRequestData(VehicleCancelRQResources processResource)
    {
        XmlNode confirmationIdNode = xmlDocument.SelectSingleNode("//a1:UniqueID", namespaceManager);
        if (confirmationIdNode != null)
        {
            processResource.ConfirmationID = confirmationIdNode.Attributes["ID"].Value;
        }
        XmlNode surNameNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:Surname", namespaceManager);
        if (surNameNode != null)
        {
            processResource.LastName = surNameNode.InnerText.Trim();
        }
        XmlNode Source = xmlDocument.DocumentElement.SelectSingleNode("//a1:Source", namespaceManager);
        if (Source != null)
        {
            if (Source.Attributes["AgentDutyCode"] != null)
            {
                processResource.AgentDutyCode = Source.Attributes["AgentDutyCode"].Value;
            }
        }
    }

    protected void setNamespace(XmlNamespaceManager namespaceManager)
    {
        this.namespaceManager = namespaceManager;
    }

    protected void setProvider(VehicleCancelRQResources processResource)
    {
        db = new DatabaseEntities();
        var ConfId = from o in db.Orders
                     where o.ConfirmationID == processResource.ConfirmationID
                     select new
                     {
                         providerName = o.XML_Provider,
                         brandName = o.XML_Provider_PickupSupplier,
                         pickup = o.PickUpLocation
                     };
        if (ConfId.Count() > 0)
        {
            processResource.ProviderName = ConfId.FirstOrDefault().providerName.ToUpper();
            processResource.BrandName = ConfId.FirstOrDefault().brandName;
            processResource.PickupLocationCode = ConfId.FirstOrDefault().pickup;
        }
        else
        {
            throw new InvalidRequestException("No provided parameter");
        }
    }
}