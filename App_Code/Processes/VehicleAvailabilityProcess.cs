﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;
using Exceptions;
using Extractors;
using ORM;
using ProcessResources;
using Templates;
/// <summary>
/// Summary description for VehicleAvailabilityProcess
/// </summary>
public class VehicleAvailabilityProcess : GenericProcess
{
    public override GenericProcessResources beginProcess(GenericProcessResources processResource, IProviderChannel providerChannel)
    {
        VehicleAvailRateRQResources vehAvailRateRQRes = Reflector.Construct<GenericProcessResources, VehicleAvailRateRQResources>(processResource);
        
        providerChannel.getValidatedRequest(vehAvailRateRQRes, namespaceMgr, db);

        string responseFromServer = commitRequest(vehAvailRateRQRes);
        RemoteDebugger.SendXmlForDebugging(responseFromServer, "Response From Server");
        Logger.Debug("Response From Server : " + responseFromServer);

        XmlDocument responseXmlDocument = convertXmlStringToDocument(responseFromServer);
        vehAvailRateRQRes.ReceivedResponse = responseXmlDocument;

        providerChannel.getValidatedResponse(vehAvailRateRQRes, namespaceMgr, db);

        if (vehAvailRateRQRes.IsThriftyRQerror)
        {
            responseFromServer = commitRequest(vehAvailRateRQRes);
            responseXmlDocument = convertXmlStringToDocument(responseFromServer);
            vehAvailRateRQRes.ReceivedResponse = responseXmlDocument;
            providerChannel.getValidatedResponse(vehAvailRateRQRes, namespaceMgr, db);
        }
        logErrors(vehAvailRateRQRes);       

        return vehAvailRateRQRes;
    }
}