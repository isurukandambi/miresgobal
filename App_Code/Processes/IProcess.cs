﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;
using ORM;
using ProcessResources;

/// <summary>
/// Summary description for Process
/// </summary>
public interface IProcess
{
    string commitRequest(GenericProcessResources processResource);
    void injectMandatoryDataFromRequestXml(GenericProcessResources processResource);
    void commitResponse(HttpContext context, GenericProcessResources processResource);
    void logErrors(GenericProcessResources processResource);
    void execute(BrandResolver brandResolver, PartnerResolver partnerResolver, ProviderResolver providerResolver, string brandParam, string payload, XMLAPITypes xmlApiType, HttpContext context);
    IProviderChannel getProviderChannel(string providerCode);
}