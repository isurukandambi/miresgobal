﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;
using ProcessResources;
using ORM;

/// <summary>
/// Summary description for CT_RentalTermsProcess
/// </summary>
public class CT_RentalTermsProcess : GenericProcess
{
    public override GenericProcessResources beginProcess(GenericProcessResources processResource, IProviderChannel providerChannel)
    {
        VehicleRentalRQResources vehRentRateRQRes = Reflector.Construct<GenericProcessResources, VehicleRentalRQResources>(processResource);

        providerChannel.getValidatedRequest(vehRentRateRQRes, namespaceMgr, db);
        string responseFromServer = commitRequest(vehRentRateRQRes);
        RemoteDebugger.SendXmlForDebugging(responseFromServer, "Response From Server");

        XmlDocument responseXmlDocument = convertXmlStringToDocument(responseFromServer);
        vehRentRateRQRes.ReceivedResponse = responseXmlDocument;

        providerChannel.getValidatedResponse(vehRentRateRQRes, namespaceMgr, db);

        logErrors(vehRentRateRQRes);

        return vehRentRateRQRes;
    }
}