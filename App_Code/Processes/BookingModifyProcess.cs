﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;
using Exceptions;
using ProcessResources;
using ORM;

/// <summary>
/// Summary description for BookingModifyProcess
/// </summary>
public class BookingModifyProcess : GenericProcess
{
    protected DatabaseEntities db;
    protected XmlNamespaceManager namespaceManager;
    protected XmlDocument xmlDocument;
    public override GenericProcessResources beginProcess(GenericProcessResources processResource, IProviderChannel providerChannel)
    {
        VehicleResModifyRQResources vehResModRQRes = Reflector.Construct<GenericProcessResources, VehicleResModifyRQResources>(processResource);
        providerChannel.getValidatedRequest(vehResModRQRes, namespaceMgr, db);
        string responseFromServer = commitRequest(vehResModRQRes);
        RemoteDebugger.SendXmlForDebugging(responseFromServer, "Response");
        Logger.Debug("Response From Server : " + responseFromServer);
        XmlDocument responseXmlDocument = convertXmlStringToDocument(responseFromServer);
        vehResModRQRes.ReceivedResponse = responseXmlDocument;

        if (!isErrorXml(vehResModRQRes.ReceivedResponse))
        {
            providerChannel.getValidatedResponse(vehResModRQRes, namespaceMgr, db);
        }
        else
        {
            vehResModRQRes.ValidatedResponse = vehResModRQRes.ReceivedResponse;
            if (vehResModRQRes.IsThriftyRQerror)
            {
                responseFromServer = commitRequest(vehResModRQRes);
                responseXmlDocument = convertXmlStringToDocument(responseFromServer);
                vehResModRQRes.ReceivedResponse = responseXmlDocument;
                providerChannel.getValidatedResponse(vehResModRQRes, namespaceMgr, db);
            }
        }
        logErrors(vehResModRQRes);

        return vehResModRQRes;
    }

    public override void execute(BrandResolver brandResolver, PartnerResolver partnerResolver, ProviderResolver providerResolver, string brandParam, string payload, XMLAPITypes xmlApiType, HttpContext context)
    {
        GenericProcessResources processResource = new GenericProcessResources();
        VehicleResModifyRQResources ResprocessResources = new VehicleResModifyRQResources();
        ResprocessResources.XMLApiType = xmlApiType;

        try
        {
            setDatabaseEntities(new DatabaseEntities());
            Logger.Debug("Requested XML From Partner : \n" + payload);
            RemoteDebugger.SendXmlForDebugging(payload, "Request");
            xmlDocument = convertXmlStringToDocument(payload);
            //RemoteDebugger.SendXmlForDebugging(XmlUtils.getXmlString(requestXmlDocument), "Original Request");
            ResprocessResources.OriginalRequest = xmlDocument;

            //setBrandData(ResprocessResources, brandResolver, brandParam);
            setNamespace(namespaceMgr);
            setRequestData(ResprocessResources);
            setPartnerData(ResprocessResources, partnerResolver);
            setProvider(ResprocessResources);

            setProviderEndpointURL(ResprocessResources.ProviderName, ResprocessResources);
            IProviderChannel providerChannel = getProviderChannel(ResprocessResources.ProviderName);
            processResource = beginProcess(ResprocessResources, providerChannel);

            commitResponse(context, processResource);

        }
        catch (Exception ex)
        {
            try
            {
                //Logger.Debug("Error1234");
                RemoteDebugger.SendXmlForDebugging(ex.Message + "</br>" + ex.StackTrace, "Error Message and Stacktrace");
                Logger.Error(ex);
                Logger.Debug(ResprocessResources.XMLApiType);
                ResprocessResources.ValidatedResponse = getErrorXML(ex.Message, ResprocessResources.XMLApiType);
                //logErrors(processResource);
                commitResponse(context, ResprocessResources);
            }
            catch (Exception logErrorEx)
            {
                RemoteDebugger.SendXmlForDebugging(logErrorEx.Message + "</br>" + logErrorEx.StackTrace, "Error in error");
                Logger.Error(ex);
                ResprocessResources.ValidatedResponse = getErrorXML(logErrorEx.Message, ResprocessResources.XMLApiType);
                commitResponse(context, ResprocessResources);
            }
        }
    }

    protected void setNamespace(XmlNamespaceManager namespaceManager)
    {
        this.namespaceManager = namespaceManager;
    }

    protected void setRequestData(VehicleResModifyRQResources processResource)
    {
        XmlNode confirmationIdNode = xmlDocument.SelectSingleNode("//a1:UniqueID", namespaceManager);
        if (confirmationIdNode != null)
        {
            processResource.ConfirmationID = confirmationIdNode.Attributes["ID"].Value;
            //RemoteDebugger.SendXmlForDebugging(processResource.ConfirmationID, "Confirmation Id");
        }
        XmlNode Source = xmlDocument.DocumentElement.SelectSingleNode("//a1:Source", namespaceManager);
        if (Source != null)
        {
            processResource.AgentDutyCode = Source.Attributes["AgentDutyCode"].Value;
        }
    }

    protected void setProvider(VehicleResModifyRQResources processResource)
    {
        db = new DatabaseEntities();
        var ConfId = from o in db.Orders
                     where o.ConfirmationID == processResource.ConfirmationID
                     select new
                     {
                         providerName = o.XML_Provider,
                         brandName = o.XML_Provider_PickupSupplier,
                         rateQualifier = o.XML_RateQualifier,
                         pickup = o.PickUpLocation
                     };
        if (ConfId.Count() > 0)
        {
            processResource.ProviderName = ConfId.FirstOrDefault().providerName.ToUpper();
            processResource.BrandName = ConfId.FirstOrDefault().brandName;
            processResource.ProviderRateQualifier = ConfId.FirstOrDefault().rateQualifier.ToUpper();
            processResource.PickupLocationCode = ConfId.FirstOrDefault().pickup;
        }
        else
        {
            throw new InvalidRequestException("No provided parameter");
        }
    }
}