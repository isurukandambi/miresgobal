﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;
using Exceptions;
using ProcessResources;
using ORM;

/// <summary>
/// Summary description for ReservationRetrievalProcess
/// </summary>

//public class xxxx { }
public class ReservationRetrievalProcess : GenericProcess
{
    protected DatabaseEntities db;
    protected XmlNamespaceManager namespaceManager;
    protected XmlDocument xmlDocument;
    public override GenericProcessResources beginProcess(GenericProcessResources processResource, IProviderChannel providerChannel)
    {
        VehicleRetrivaleRQResources vehRetrivalRQRes = Reflector.Construct<GenericProcessResources, VehicleRetrivaleRQResources>(processResource);

        providerChannel.getValidatedRequest(vehRetrivalRQRes, namespaceMgr, db);

        string responseFromServer = commitRequest(vehRetrivalRQRes);
        Logger.Debug("Response From Server : " + responseFromServer);
        XmlDocument responseXmlDocument = convertXmlStringToDocument(responseFromServer);
        vehRetrivalRQRes.ReceivedResponse = responseXmlDocument;

        if (!isErrorXml(vehRetrivalRQRes.ReceivedResponse))
        {
            providerChannel.getValidatedResponse(vehRetrivalRQRes, namespaceMgr, db);
        }
        else
        {
            vehRetrivalRQRes.ValidatedResponse = vehRetrivalRQRes.ReceivedResponse;
        }
        //logErrors(vehRetrivalRQRes);

        return vehRetrivalRQRes;
    }

    public override void execute(BrandResolver brandResolver, PartnerResolver partnerResolver, ProviderResolver providerResolver, string brandParam, string payload, XMLAPITypes xmlApiType, HttpContext context)
    {     
        GenericProcessResources processResource = new GenericProcessResources();
        VehicleRetrivaleRQResources ResprocessResources = new VehicleRetrivaleRQResources();
        ResprocessResources.XMLApiType = xmlApiType;
        //string provider = setProviderData(VehicleRetrivaleRQResources retResources);//"HERTZ";
        //processResource.ProviderName = provider;
        ResprocessResources.ProviderId = 1;
        try
        {
            setDatabaseEntities(new DatabaseEntities());
            Logger.Debug("Requested XML From Partner : \n" + payload);
            RemoteDebugger.SendXmlForDebugging(payload, "Request");
            xmlDocument = convertXmlStringToDocument(payload);
            //RemoteDebugger.SendXmlForDebugging(XmlUtils.getXmlString(requestXmlDocument), "Original Request");
            ResprocessResources.OriginalRequest = xmlDocument;
            setNamespace(namespaceMgr);
            setRequestData(ResprocessResources);
            setProvider(ResprocessResources);

            setProviderEndpointURL(ResprocessResources.ProviderName, ResprocessResources);
            IProviderChannel providerChannel = getProviderChannel(ResprocessResources.ProviderName);
            processResource = beginProcess(ResprocessResources, providerChannel);

            commitResponse(context, processResource);

        }
        catch (Exception ex)
        {
            try
            {
                //Logger.Debug("Error1234");
                RemoteDebugger.SendXmlForDebugging(ex.Message+"</br>"+ex.StackTrace, "Error Message and Stacktrace");
                Logger.Error(ex);
                Logger.Debug(ResprocessResources.XMLApiType);
                ResprocessResources.ValidatedResponse = getErrorXML(ex.Message, ResprocessResources.XMLApiType);
                //logErrors(processResource);
                commitResponse(context, ResprocessResources);
            }
            catch (Exception logErrorEx)
            {
                RemoteDebugger.SendXmlForDebugging(logErrorEx.Message + "</br>" + logErrorEx.StackTrace, "Error in error");
                Logger.Error(ex);
                ResprocessResources.ValidatedResponse = getErrorXML(logErrorEx.Message, ResprocessResources.XMLApiType);
                commitResponse(context, ResprocessResources);
            }
        }
    }

    protected void setRequestData(VehicleRetrivaleRQResources processResource)
    {
        XmlNode agentDutyNode = xmlDocument.SelectSingleNode("//a1:Source", namespaceManager);
        if (agentDutyNode != null)
        {
            processResource.AgentDutyCode = agentDutyNode.Attributes["AgentDutyCode"].Value;
        }
        XmlNode confirmationIdNode = xmlDocument.SelectSingleNode("//a1:UniqueID", namespaceManager);
        if (confirmationIdNode != null)
        {
            processResource.ConfirmationID = confirmationIdNode.Attributes["ID"].Value;
            //RemoteDebugger.SendXmlForDebugging(processResource.ConfirmationID, "Confirmation Id");
        }
        XmlNode surNameNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:Surname", namespaceManager);
        if (surNameNode != null)
        {
            processResource.LastName = surNameNode.InnerText.Trim();
        }
    }

    protected void setNamespace(XmlNamespaceManager namespaceManager)
    {
        this.namespaceManager = namespaceManager;
    }

    protected void setProvider(VehicleRetrivaleRQResources processResource)
    {
        db = new DatabaseEntities();
        var ConfId = from o in db.Orders
                     where o.ConfirmationID == processResource.ConfirmationID
                       select new
                       {
                           providerName = o.XML_Provider,
                           brandName = o.XML_Provider_PickupSupplier,
                           pickup = o.PickUpLocation
                       };
        if (ConfId.Count() > 0)
        {
            processResource.ProviderName = ConfId.FirstOrDefault().providerName.ToUpper();
            processResource.BrandName = ConfId.FirstOrDefault().brandName;
            processResource.PickupLocationCode = ConfId.FirstOrDefault().pickup;
        }
        else
        {
            throw new InvalidRequestException("No provided parameter");
        }
    }
}