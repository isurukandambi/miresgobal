﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;
using System.Xml.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using Extractors;
using ORM;
using ProcessResources;
using Templates;
using ServiceRateReference;
using ServiceReservationReference;
using TestClient;

public abstract class GenericProcess : IProcess
{
    protected XmlNamespaceManager namespaceMgr;
    protected DatabaseEntities db;
    public abstract GenericProcessResources beginProcess(GenericProcessResources processResource, IProviderChannel providerChannel);

    #region IProcess Members
    public string commitRequest(GenericProcessResources processResource)
    {
        string validatedRequestXml;
        if (processResource.ProviderName == XMLProviders.CARTRAWLER.ToString() || processResource.ProviderName == XMLProviders.THERMEON.ToString())
        {
            validatedRequestXml = XmlUtils.getXmlString(processResource.ValidatedRequest).Replace("utf-16", "utf-8");
        }
        else
        {
            validatedRequestXml = XmlUtils.getXmlString(processResource.ValidatedRequest);
        }

        RemoteDebugger.SendXmlForDebugging(validatedRequestXml, "Validated Request ready to send to provider for - " + processResource.BrandName);
        
        
        Logger.Debug("Validated Request for Provider : \n" + validatedRequestXml);
        if (processResource.ProviderName == XMLProviders.CARTRAWLER.ToString() || processResource.ProviderName == XMLProviders.HERTZ.ToString() || processResource.ProviderName == XMLProviders.THERMEON.ToString())
        {
            AppHttpClient httpClient = new AppHttpClient();
            string responseFromProvider = httpClient.createPostRequest(processResource.ProviderEndPointUrl, validatedRequestXml);
            RemoteDebugger.SendXmlForDebugging(responseFromProvider, "Response receieved from provider - " + processResource.BrandName);
            return responseFromProvider;
        }
        else if (processResource.ProviderName == XMLProviders.DTAG.ToString())
        {
            var namespaceMgr = this.namespaceMgr;

            if (processResource.XMLApiType == XMLAPITypes.AvailabilityRequest)
            {
                XmlNode rootNode = processResource.ValidatedRequest.DocumentElement.SelectSingleNode("//a1:OTA_VehAvailRateRQ", namespaceMgr);

                List<XmlNode> xmlNodes = new List<XmlNode>(rootNode.ChildNodes.Cast<XmlNode>());

                GetRatesRequestBody rateReqBody = new GetRatesRequestBody
                {
                    OTA_VehAvailRateRQ = new OTA_VehAvailRateRQ
                    {
                        Nodes = xmlNodes.ToArray<XmlNode>(),
                    }
                };
                GetRatesRequest rateReq = new GetRatesRequest(rateReqBody);

                OTA2010ARateServiceClient client = new OTA2010ARateServiceClient();
                client.InnerChannel.OperationTimeout = new TimeSpan(0, 0, 25);
                OTA_VehAvailRateRS rateResponse = client.GetRates(rateReqBody.OTA_VehAvailRateRQ);

                var writer = new StringWriter();
                new XmlSerializer(rateResponse.GetType()).Serialize(writer, rateResponse);
                var xmlEncodedString = writer.GetStringBuilder().ToString();
                RemoteDebugger.SendXmlForDebugging(xmlEncodedString, "Response receieved from provider for - " + processResource.BrandName);

                return xmlEncodedString;
            }

            else if (processResource.XMLApiType == XMLAPITypes.ReservationRequest)
            {
                XmlNode rootNode = processResource.ValidatedRequest.DocumentElement.SelectSingleNode("//a1:OTA_VehResRQ", namespaceMgr);

                List<XmlNode> xmlNodes = new List<XmlNode>(rootNode.ChildNodes.Cast<XmlNode>());

                MakeReservationRequestBody bookingrequestBody = new MakeReservationRequestBody
                {
                    OTA_VehResRQ = new OTA_VehResRQ
                    {
                        Nodes = xmlNodes.ToArray<XmlNode>(),
                    }

                };

                OTA2010AReservationServiceClient client = new OTA2010AReservationServiceClient();
                client.InnerChannel.OperationTimeout = new TimeSpan(0, 0, 25);

                OTA_VehResRS bookingResponse = client.MakeReservation(bookingrequestBody.OTA_VehResRQ);

                var writer = new StringWriter();
                new XmlSerializer(bookingResponse.GetType()).Serialize(writer, bookingResponse);
                var xmlEncodedString = writer.GetStringBuilder().ToString();
                RemoteDebugger.SendXmlForDebugging(xmlEncodedString, "Response receieved from provider");

                return xmlEncodedString;
            }

            else if (processResource.XMLApiType == XMLAPITypes.RetrivalRequest)
            {
                XmlNode rootNode = processResource.ValidatedRequest.DocumentElement.SelectSingleNode("//a1:OTA_VehRetResRQ", namespaceMgr);

                List<XmlNode> xmlNodes = new List<XmlNode>(rootNode.ChildNodes.Cast<XmlNode>());

                GetReservationRequestBody retriverequestBody = new GetReservationRequestBody
                {
                    OTA_VehRetResRQ = new OTA_VehRetResRQ
                    {
                        Nodes = xmlNodes.ToArray<XmlNode>(),
                    }

                };

                OTA2010AReservationServiceClient client = new OTA2010AReservationServiceClient();
                client.InnerChannel.OperationTimeout = new TimeSpan(0, 0, 5);

                OTA_VehRetResRS retriveResponse = client.GetReservation(retriverequestBody.OTA_VehRetResRQ);

                var writer = new StringWriter();
                new XmlSerializer(retriveResponse.GetType()).Serialize(writer, retriveResponse);
                var xmlEncodedString = writer.GetStringBuilder().ToString();
                RemoteDebugger.SendXmlForDebugging(xmlEncodedString, "Response receieved from provider");

                return xmlEncodedString;
            }

            else if (processResource.XMLApiType == XMLAPITypes.ModificationRequest)
            {
                XmlNode rootNode = processResource.ValidatedRequest.DocumentElement.SelectSingleNode("//a1:OTA_VehModifyRQ", namespaceMgr);

                List<XmlNode> xmlNodes = new List<XmlNode>(rootNode.ChildNodes.Cast<XmlNode>());

                ModifyReservationRequestBody modifyrequestBody = new ModifyReservationRequestBody
                {
                    OTA_VehModifyRQ = new OTA_VehModifyRQ
                    {
                        Nodes = xmlNodes.ToArray<XmlNode>(),
                    }

                };

                OTA2010AReservationServiceClient client = new OTA2010AReservationServiceClient();
                client.InnerChannel.OperationTimeout = new TimeSpan(0, 0, 25);

                OTA_VehModifyRS modifyResponse = client.ModifyReservation(modifyrequestBody.OTA_VehModifyRQ);

                var writer = new StringWriter();
                new XmlSerializer(modifyResponse.GetType()).Serialize(writer, modifyResponse);
                var xmlEncodedString = writer.GetStringBuilder().ToString();
                RemoteDebugger.SendXmlForDebugging(xmlEncodedString, "Response receieved from provider");

                return xmlEncodedString;
            }

            else if (processResource.XMLApiType == XMLAPITypes.CancellationRequest)
            {
                XmlNode rootNode = processResource.ValidatedRequest.DocumentElement.SelectSingleNode("//a1:OTA_VehCancelRQ", namespaceMgr);

                List<XmlNode> xmlNodes = new List<XmlNode>(rootNode.ChildNodes.Cast<XmlNode>());

                CancelReservationRequestBody cancelrequestBody = new CancelReservationRequestBody
                {
                    OTA_VehCancelRQ = new OTA_VehCancelRQ
                    {
                        Nodes = xmlNodes.ToArray<XmlNode>(),
                    }

                };

                OTA2010AReservationServiceClient client = new OTA2010AReservationServiceClient();
                client.InnerChannel.OperationTimeout = new TimeSpan(0, 0, 25);

                OTA_VehCancelRS cancelResponse = client.CancelReservation(cancelrequestBody.OTA_VehCancelRQ);

                var writer = new StringWriter();
                new XmlSerializer(cancelResponse.GetType()).Serialize(writer, cancelResponse);
                var xmlEncodedString = writer.GetStringBuilder().ToString();
                RemoteDebugger.SendXmlForDebugging(xmlEncodedString, "Response receieved from provider");

                return xmlEncodedString;
            }
            else
            {
                return "";
            }
        }
        else
        {
            return "";
        }
    }

    public void injectMandatoryDataFromRequestXml(GenericProcessResources processResource)
    {
        GenericXmlDataExtractor genericExtractor = new GenericXmlDataExtractor(db);
        //Pickup Location Details
        Location pickupLocationObj = genericExtractor.getPickupLocationDetails(processResource.OriginalRequest, namespaceMgr);
        processResource.PickupLocationCode = pickupLocationObj.LocationCode;
        processResource.PickupLocationExtendedCode = pickupLocationObj.ExtendedLocationCode;
        processResource.PickupLocationName = pickupLocationObj.LocationName;

        //Dropoff Location Details
        Location returnLocationObj = genericExtractor.getDropOffLocationDetails(processResource.OriginalRequest, namespaceMgr);
        processResource.DropoffLocationCode = returnLocationObj.LocationCode;
        processResource.DropoffLocationExtendedCode = returnLocationObj.ExtendedLocationCode;
        processResource.DropoffLocationName = returnLocationObj.LocationName;

        //Extract Pickup and Return datetime
        DateTime[] vehAvailDateTimes = genericExtractor.getPickupAndReturnDates(processResource.OriginalRequest, namespaceMgr);
        processResource.PickupDateTime = vehAvailDateTimes[0];
        processResource.DropoffDatetime = vehAvailDateTimes[1];

        //Extract ISO Country and Agentduty Code
        string[] isoCountryAndAgentDutyCode = genericExtractor.getISOCodeAgentDutyCode(processResource.OriginalRequest, namespaceMgr);
        processResource.SourceCountryCode = isoCountryAndAgentDutyCode[0];
        processResource.AgentDutyCode = isoCountryAndAgentDutyCode[1];
    }

    public void commitResponse(HttpContext context, GenericProcessResources processResource)
    {
        
        string commitResponseXmlString = XmlUtils.getXmlString(processResource.ValidatedResponse);
        RemoteDebugger.SendXmlForDebugging(commitResponseXmlString, "Commited Response for - "+processResource.BrandName);
        Logger.Debug("Response Ready to Commit......... : \n" + commitResponseXmlString);
        context.Response.Write(commitResponseXmlString);
    }


    public virtual void logErrors(GenericProcessResources processResource)
    {
        if (isErrorXml(processResource.ValidatedResponse))
        {
            ErrorLogger errorLogger = new ErrorLogger();
            errorLogger.logErrorsInDatabase(processResource, db, namespaceMgr);
        }
    }

    public IProviderChannel getProviderChannel(string providerCode)
    {
        XMLProviderRouter providerRouter = new XMLProviderRouter();
        return providerRouter.switchProviderChannel(providerCode);
    }

    public virtual void execute(BrandResolver brandResolver, PartnerResolver partnerResolver, ProviderResolver providerResolver, string brandParam, string payload, XMLAPITypes xmlApiType, HttpContext context)
    {
        GenericProcessResources processResource = new GenericProcessResources();
        processResource.XMLApiType = xmlApiType;
        try
        {
            setDatabaseEntities(new DatabaseEntities());
            RemoteDebugger.SendXmlForDebugging(payload, "Request Received from partner - "+brandParam);
            Logger.Debug("Requested XML From Partner : \n" + payload);
            XmlDocument requestXmlDocument = convertXmlStringToDocument(payload);
            processResource.OriginalRequest = requestXmlDocument;
            Logger.Debug(processResource);
            injectMandatoryDataFromRequestXml(processResource);

            setBrandData(processResource, brandResolver, brandParam);
            setPartnerData(processResource, partnerResolver);
            setRateQualifierANDtourTag(processResource.OriginalRequest, namespaceMgr, processResource);
            setISO_Destcountry(processResource.OriginalRequest, namespaceMgr, processResource);

            //if (brandParam == "firefly")
            //{
            //    processResource.ProviderName = "HERTZ";
            //    processResource.ProviderId = 1;
            //}
            //check brand nad if it is hertz hardcode to provider like Hertz.
            //if (brandParam == "hertz")
            //{
            //    processResource.ProviderName = "CARTRAWLER";
            //    processResource.ProviderId = 1;
            //}
            ////check brand and if it is thrifty hardcode to provider like Hertz.
            //if (brandParam == "thrifty")
            //{
            //    processResource.ProviderName = "HERTZ";
            //    processResource.ProviderId = 1;
            //}
            //else
            //{
                setProviderData(processResource, providerResolver);
            //}

            //if (processResource.ProviderName == "THERMEON")
            //{
            //    this.namespaceMgr = new XmlNamespaceManager(requestXmlDocument.NameTable);
            //    this.namespaceMgr.AddNamespace("a1", Configs.THERMEON_XML_NAMESPACE);
            //}


            //setProviderData(processResource, providerResolver);
            Logger.Debug("Brand : " + processResource.BrandName + "\n Partner : " + processResource.PartnerName + "\n Provider : " + processResource.ProviderName);

            setProviderEndpointURL(processResource.ProviderName, processResource);

            IProviderChannel providerChannel = getProviderChannel(processResource.ProviderName);

            processResource = beginProcess(processResource, providerChannel);

            commitResponse(context, processResource);

        }
        catch (Exception ex)
        {
            try
            {
                RemoteDebugger.SendXmlForDebugging(ex.StackTrace, "Error StackRace");
                RemoteDebugger.SendXmlForDebugging(ex.Message, "Error Message");
                Logger.Error(ex);
                Logger.Debug(processResource.XMLApiType);
                processResource.ValidatedResponse = getErrorXML(ex.Message, processResource.XMLApiType);
                logErrors(processResource);
                commitResponse(context, processResource);
            }
            catch (Exception logErrorEx)
            {
                Logger.Error(ex);
                processResource.ValidatedResponse = getErrorXML(logErrorEx.Message, processResource.XMLApiType);
                commitResponse(context, processResource);
            }
        }
    }

    #endregion

    protected XmlDocument getErrorXML(string errorReason, XMLAPITypes xmlApiType)
    {
        return ErrorXmlTemplate.getUnhandledErrorXml(errorReason, xmlApiType);
    }

    protected void setDatabaseEntities(DatabaseEntities dbObject)
    {
        this.db = dbObject;
    }

    protected void setNamespaceManager(XmlDocument xmlDocument)
    {
        this.namespaceMgr = new XmlNamespaceManager(xmlDocument.NameTable);
        this.namespaceMgr.AddNamespace("a1", Configs.XML_NAMESPACE);
    }

    protected XmlDocument convertXmlStringToDocument(string xmlString)
    {
        var xmlDocument = new XmlDocument();
        xmlDocument.LoadXml(xmlString);
        setNamespaceManager(xmlDocument);

        xmlDocument.CreateXmlDeclaration("1.0", "UTF-8", "yes");

        return xmlDocument;
    }

    protected void setBrandData(GenericProcessResources processResource, BrandResolver brandResolver, string pathParamBrand)
    {
        Brand brand = brandResolver.extractBrand(pathParamBrand, db);
        processResource.BrandId = brand.BrandID;
        processResource.BrandName = brand.BrandName.ToUpper();
    }

    protected void setRateQualifierANDtourTag(XmlDocument xmlDocument, XmlNamespaceManager namespaceMgr, GenericProcessResources processResource)
    {
        XmlNode rateQualifierNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:RateQualifier", namespaceMgr);
        if (rateQualifierNode != null)
        {
            processResource.ProviderRateQualifier = rateQualifierNode.Attributes["RateQualifier"].Value.ToString().TrimEnd();
        }

        if (xmlDocument.DocumentElement.SelectSingleNode("//a1:TourInfo", namespaceMgr) == null)
        {
            processResource.tourInfoTag = false;
        }
        else
        {
            processResource.tourInfoTag = true;
        }
    }

    protected void setISO_Destcountry(XmlDocument xmlDocument, XmlNamespaceManager namespaceMgr, GenericProcessResources processResource)
    {
        //Set Source Country code
        XmlNode sourceNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:Source", namespaceMgr);
        if (sourceNode != null)
        {
            if (sourceNode.Attributes["ISOCountry"] != null)
            {
                processResource.SourceCountryCode = sourceNode.Attributes["ISOCountry"].Value;
            }
        }

        //Set Destination Country code
        var destinationCountry = (from c in db.Countries
                                 join l in db.Locations on c.CountryID equals l.CountryIDFK
                                  where l.LocationCode == processResource.PickupLocationCode
                                 orderby c.CountryName
                                 select new
                                 {
                                     c.CountryCode
                                 });
        if (destinationCountry.Count() > 0)
        {
            processResource.DestCountryCode = destinationCountry.First().CountryCode;
        }
    }

    protected void setPartnerData(GenericProcessResources processResource, PartnerResolver partnerResolver)
    {
        Logger.Debug("Agent Duty Code : " + processResource.AgentDutyCode);
        Partner partner = partnerResolver.extractPartner(processResource.AgentDutyCode, db);
        processResource.PartnerId = partner.PartnerId;
        processResource.PartnerName = partner.DomainName.ToUpper();
    }

    protected void setProviderData(GenericProcessResources processResource, ProviderResolver providerResolver)
    {
        Logger.Debug("Source Country Code : " + processResource.SourceCountryCode + " \n Brand : " + processResource.BrandName + " \n Pickup Location Code : " + processResource.PickupLocationCode);
        Provider provider = providerResolver.extractProvider(processResource.ProviderRateQualifier, processResource.AgentDutyCode, processResource.BrandName, processResource.PickupLocationCode, db, processResource.tourInfoTag, processResource.SourceCountryCode, processResource.DestCountryCode);
        processResource.ProviderName = provider.ProviderName.ToUpper();
        processResource.ProviderId = provider.Pro_Id;
        RemoteDebugger.SendXmlForDebugging(processResource.ProviderName, "Provider for "+processResource.BrandName);
    }

    public bool isErrorXml(XmlDocument xmlDocument)
    {
        if (xmlDocument.DocumentElement.SelectSingleNode("//a1:Errors", namespaceMgr) != null)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    protected void setProviderEndpointURL(string providerCode, GenericProcessResources processResource)
    {
        if (providerCode == XMLProviders.HERTZ.ToString())
        {
            processResource.ProviderEndPointUrl = AppSettings.HertzXmlEndPoint;
        }
        else if (providerCode == XMLProviders.DTAG.ToString())
        {
            processResource.ProviderEndPointUrl = "-";
        }
        else if (providerCode == XMLProviders.CARTRAWLER.ToString())
        {
            if (processResource.XMLApiType.ToString() == "ReservationRequest")
            {
                processResource.ProviderEndPointUrl = AppSettings.CarTrawlerResXmlEndPoint;
            }
            else
            {
                processResource.ProviderEndPointUrl = AppSettings.CarTrawlerXmlEndPoint;
            }
        }
        else if (providerCode == XMLProviders.THERMEON.ToString())
        {
            processResource.ProviderEndPointUrl = AppSettings.ThermeonXmlEndPoint;
        }
    }

}