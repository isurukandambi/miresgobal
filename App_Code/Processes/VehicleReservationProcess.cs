﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;
using ProcessResources;
using ORM;

/// <summary>
/// Summary description for VehicleReservationProcess
/// </summary>
public class VehicleReservationProcess:GenericProcess
{
    public override GenericProcessResources beginProcess(GenericProcessResources processResource, IProviderChannel providerChannel)
    {
        string responseFromServer;
        XmlDocument responseXmlDocument;

        VehicleResRQResources vehResRQRes = Reflector.Construct<GenericProcessResources, VehicleResRQResources>(processResource);

        if (processResource.ProviderName == "CARTRAWLER")
        {
            providerChannel.getValidatedRequest(vehResRQRes, namespaceMgr, db);
            responseFromServer = commitRequest(vehResRQRes);
            responseXmlDocument = convertXmlStringToDocument(responseFromServer);
            vehResRQRes.ReceivedResponse = responseXmlDocument;
            providerChannel.getValidatedResponse(vehResRQRes, namespaceMgr, db);
            if (vehResRQRes.IsCTResload)
            {
                providerChannel.getValidatedRequest(vehResRQRes, namespaceMgr, db);
                responseFromServer = commitRequest(vehResRQRes);
                responseXmlDocument = convertXmlStringToDocument(responseFromServer);
                vehResRQRes.ReceivedResponse = responseXmlDocument;
                providerChannel.getValidatedResponse(vehResRQRes, namespaceMgr, db);
            }
        }
        else
        {
            providerChannel.getValidatedRequest(vehResRQRes, namespaceMgr, db);
            responseFromServer = commitRequest(vehResRQRes);
            responseXmlDocument = convertXmlStringToDocument(responseFromServer);
            vehResRQRes.ReceivedResponse = responseXmlDocument;
            providerChannel.getValidatedResponse(vehResRQRes, namespaceMgr, db);

            if (vehResRQRes.IsFlightNo)
            {
                responseFromServer = commitRequest(vehResRQRes);
                responseXmlDocument = convertXmlStringToDocument(responseFromServer);
                vehResRQRes.ReceivedResponse = responseXmlDocument;
                providerChannel.getValidatedResponse(vehResRQRes, namespaceMgr, db);
            }

            if (vehResRQRes.IsGoldNumberError)
            {
                responseFromServer = commitRequest(vehResRQRes);
                responseXmlDocument = convertXmlStringToDocument(responseFromServer);
                vehResRQRes.ReceivedResponse = responseXmlDocument;
                providerChannel.getValidatedResponse(vehResRQRes, namespaceMgr, db);
            }
        }
        
        return vehResRQRes;
    }
}