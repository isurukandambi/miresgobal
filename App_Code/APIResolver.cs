﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for APIResolver
/// </summary>
public class APIResolver
{
    public XMLAPITypes getAPICallType(string payload)
    {
        if (payload.IndexOf("OTA_VehAvailRateRQ") != -1)
        {
            return XMLAPITypes.AvailabilityRequest;
        }
        else if (payload.IndexOf("OTA_VehResRQ") != -1)
        {
            return XMLAPITypes.ReservationRequest;
        }
        else if (payload.IndexOf("OTA_VehRetResRQ") != -1)
        {
            return XMLAPITypes.RetrivalRequest;
        }
        else if (payload.IndexOf("OTA_VehModifyRQ") != -1)
        {
            return XMLAPITypes.ModificationRequest;
        }
        else if (payload.IndexOf("OTA_VehCancelRQ") != -1)
        {
            return XMLAPITypes.CancellationRequest;
        }
        else if (payload.IndexOf("CT_RentalConditionsRQ") != -1)
        {
            return XMLAPITypes.RentalTermsRequest;
        }
        else
        {
            return XMLAPITypes.InvalidRequest;
        }
    }
}