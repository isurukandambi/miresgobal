﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Exceptions;
using ORM;

/// <summary>
/// Summary description for ProviderResolver
/// </summary>
public class ProviderResolver
{
    public Provider extractProvider(string providerRQ, string agentDutyCode, string brand, string pickupLocationCode, DatabaseEntities db, bool tourtag, string isoCountry, string destCountry)
    {
        if (brand == Brands.HERTZ.ToString())
        {
            var provider = from p in db.Providers
                           where (p.ProviderName == "Hertz")
                           select new
                           {
                               providerData = p
                           };
            if (provider.Count() == 1)
            {
                //Set Provider Hertz
                return (Provider)provider.First().providerData;
            }
            else
            {
                throw new InvalidRequestException("No Provider in Database");
            }
        }
        else if (brand == Brands.THRIFTY.ToString())
        {
            //Check Partner is the Thrifty4carrental
            if (agentDutyCode == "3CA1R18R20L")
            {
                //Check source country is United States
                if (isoCountry == "US")
                {
                    //Check Rate Qualifier value in DB
                    var RQ = from r in db.Rate_Qulifiers
                             where (r.Rate_Qulifier == providerRQ)
                             select new
                             {
                                 r.Rate_Qulifier
                             };
                    if (RQ.Count() == 1)
                    {
                        //Check Partner in DB
                        var Partner = from p in db.Partners
                                      where (p.AgentDutyCode == agentDutyCode)
                                      select new
                                      {
                                          p.CT_Allowed
                                      };
                        if (Partner.Count() == 1)
                        {
                            //Check Partner allowed the CarTrawler
                            if (Partner.FirstOrDefault().CT_Allowed == 1)
                            {
                                throw new InvalidRequestException("No access for PrePay Requests");
                            }
                            //Partner not allowed
                            else
                            {
                                var provider = from p in db.Providers
                                               where (p.ProviderName == "DTag")
                                               select new
                                               {
                                                   providerData = p
                                               };
                                if (provider.Count() == 1)
                                {
                                    //Set Provider D-Tag
                                    return (Provider)provider.First().providerData;
                                }
                                else
                                {
                                    throw new InvalidRequestException("No Provider in Database");
                                }
                            }
                        }
                        else
                        {
                            throw new InvalidRequestException("No Partner in select AgentDuty Code");
                        }
                    }
                    //Rate Qualifier not in DB
                    else
                    {
                        //Check has Tour Tag
                        if (tourtag == true)
                        {
                            //Check Partner in DB
                            var Partner = from p in db.Partners
                                          where (p.AgentDutyCode == agentDutyCode)
                                          select new
                                          {
                                              p.CT_Allowed
                                          };
                            if (Partner.Count() == 1)
                            {
                                //Check Partner allowed the CarTrawler
                                if (Partner.FirstOrDefault().CT_Allowed == 1)
                                {
                                    throw new InvalidRequestException("No access for PrePay Requests");
                                }
                                //Partner not Allowed
                                else
                                {
                                    var provider = from p in db.Providers
                                                   where (p.ProviderName == "DTag")
                                                   select new
                                                   {
                                                       providerData = p
                                                   };
                                    if (provider.Count() == 1)
                                    {
                                        //Set Provider D-Tag
                                        return (Provider)provider.First().providerData;
                                    }
                                    else
                                    {
                                        throw new InvalidRequestException("No Provider in Database");
                                    }
                                }
                            }
                            else
                            {
                                throw new InvalidRequestException("No Partner in select AgentDuty Code");
                            }
                        }
                        //Tour Tag hasn't in XML
                        else
                        {
                            var provider = from p in db.Providers
                                           where (p.ProviderName == "DTag")
                                           select new
                                           {
                                               providerData = p
                                           };
                            if (provider.Count() == 1)
                            {
                                //Set Provider D-Tag
                                return (Provider)provider.First().providerData;
                            }
                            else
                            {
                                throw new InvalidRequestException("No Provider in Database");
                            }
                        }
                    }
                }
                //Check destination country is United States
                else if (destCountry == "US")
                {
                    //Check Rate Qualifier value in DB
                    var RQ = from r in db.Rate_Qulifiers
                             where (r.Rate_Qulifier == providerRQ)
                             select new
                             {
                                 r.Rate_Qulifier
                             };
                    if (RQ.Count() == 1)
                    {
                        //Check Partner in DB
                        var Partner = from p in db.Partners
                                      where (p.AgentDutyCode == agentDutyCode)
                                      select new
                                      {
                                          p.CT_Allowed
                                      };
                        if (Partner.Count() == 1)
                        {
                            //Check Partner allowed the CarTrawler
                            if (Partner.FirstOrDefault().CT_Allowed == 1)
                            {
                                var provider = from p in db.Providers
                                               where (p.ProviderName == "CarTrawler")
                                               select new
                                               {
                                                   providerData = p
                                               };
                                if (provider.Count() == 1)
                                {
                                    //Set Provider CarTrawler
                                    return (Provider)provider.First().providerData;
                                }
                                else
                                {
                                    throw new InvalidRequestException("No Provider in Database");
                                }
                            }
                            //Partner not allowed
                            else
                            {
                                throw new InvalidRequestException("No access for Non PrePay Requests");
                            }
                        }
                        else
                        {
                            throw new InvalidRequestException("No Partner in select AgentDuty Code");
                        }
                    }
                    //Rate Qualifier not in DB
                    else
                    {
                        //Check has Tour Tag
                        if (tourtag == true)
                        {
                            //Check Partner in DB
                            var Partner = from p in db.Partners
                                          where (p.AgentDutyCode == agentDutyCode)
                                          select new
                                          {
                                              p.CT_Allowed
                                          };
                            if (Partner.Count() == 1)
                            {
                                //Check Partner allowed the CarTrawler
                                if (Partner.FirstOrDefault().CT_Allowed == 1)
                                {
                                    var provider = from p in db.Providers
                                                   where (p.ProviderName == "CarTrawler")
                                                   select new
                                                   {
                                                       providerData = p
                                                   };
                                    if (provider.Count() == 1)
                                    {
                                        //Set Provider CarTrawler
                                        return (Provider)provider.First().providerData;
                                    }
                                    else
                                    {
                                        throw new InvalidRequestException("No Provider in Database");
                                    }
                                }
                                //Partner not Allowed
                                else
                                {
                                    throw new InvalidRequestException("No access for Non PrePay Requests");
                                }
                            }
                            else
                            {
                                throw new InvalidRequestException("No Partner in select AgentDuty Code");
                            }
                        }
                        //Tour Tag hasn't in XML
                        else
                        {
                            throw new InvalidRequestException("No access for Non PrePay Requests");
                        }
                    }
                }
                //Countinue to normal way
                else
                {
                    // Check pickup Country in TC4R List
                    var tcList = from t in db.TC4R_List
                                 join c in db.Countries on t.CountryIDFK equals c.CountryID
                                 join l in db.Locations on c.CountryID equals l.CountryIDFK
                                 where (l.LocationCode == pickupLocationCode)
                                 select new
                                 {
                                     t.CountryIDFK,
                                     c.CountryName
                                 };
                    if (tcList.Count() > 0)
                    {
                        var provider = from p in db.Providers
                                       where (p.ProviderName == "Hertz")
                                       select new
                                       {
                                           providerData = p
                                       };
                        if (provider.Count() == 1)
                        {
                            //Set Provider Hertz
                            return (Provider)provider.First().providerData;
                        }
                        else
                        {
                            throw new InvalidRequestException("No Provider in Database");
                        }
                    }
                    // Not TC4R List
                    else
                    {
                        //Check Rate Qualifier value in DB
                        var RQ = from r in db.Rate_Qulifiers
                                 where (r.Rate_Qulifier == providerRQ)
                                 select new
                                 {
                                     r.Rate_Qulifier
                                 };
                        if (RQ.Count() == 1)
                        {
                            //Check Partner in DB
                            var Partner = from p in db.Partners
                                          where (p.AgentDutyCode == agentDutyCode)
                                          select new
                                          {
                                              p.CT_Allowed
                                          };
                            if (Partner.Count() == 1)
                            {
                                //Check Partner allowed the CarTrawler
                                if (Partner.FirstOrDefault().CT_Allowed == 1)
                                {
                                    var provider = from p in db.Providers
                                                   where (p.ProviderName == "CarTrawler")
                                                   select new
                                                   {
                                                       providerData = p
                                                   };
                                    if (provider.Count() == 1)
                                    {
                                        //Set Provider CarTrawler
                                        return (Provider)provider.First().providerData;
                                    }
                                    else
                                    {
                                        throw new InvalidRequestException("No Provider in Database");
                                    }
                                }
                                //Partner not allowed
                                else
                                {
                                    var provider = from p in db.Providers
                                                   where (p.ProviderName == "DTag")
                                                   select new
                                                   {
                                                       providerData = p
                                                   };
                                    if (provider.Count() == 1)
                                    {
                                        //Set Provider D-Tag
                                        return (Provider)provider.First().providerData;
                                    }
                                    else
                                    {
                                        throw new InvalidRequestException("No Provider in Database");
                                    }
                                }
                            }
                            else
                            {
                                throw new InvalidRequestException("No Partner in select AgentDuty Code");
                            }
                        }
                        //Rate Qualifier not in DB
                        else
                        {
                            //Check has Tour Tag
                            if (tourtag == true)
                            {
                                //Check Partner in DB
                                var Partner = from p in db.Partners
                                              where (p.AgentDutyCode == agentDutyCode)
                                              select new
                                              {
                                                  p.CT_Allowed
                                              };
                                if (Partner.Count() == 1)
                                {
                                    //Check Partner allowed the CarTrawler
                                    if (Partner.FirstOrDefault().CT_Allowed == 1)
                                    {
                                        var provider = from p in db.Providers
                                                       where (p.ProviderName == "CarTrawler")
                                                       select new
                                                       {
                                                           providerData = p
                                                       };
                                        if (provider.Count() == 1)
                                        {
                                            //Set Provider CarTrawler
                                            return (Provider)provider.First().providerData;
                                        }
                                        else
                                        {
                                            throw new InvalidRequestException("No Provider in Database");
                                        }
                                    }
                                    //Partner not Allowed
                                    else
                                    {
                                        var provider = from p in db.Providers
                                                       where (p.ProviderName == "DTag")
                                                       select new
                                                       {
                                                           providerData = p
                                                       };
                                        if (provider.Count() == 1)
                                        {
                                            //Set Provider D-Tag
                                            return (Provider)provider.First().providerData;
                                        }
                                        else
                                        {
                                            throw new InvalidRequestException("No Provider in Database");
                                        }
                                    }
                                }
                                else
                                {
                                    throw new InvalidRequestException("No Partner in select AgentDuty Code");
                                }
                            }
                            //Tour Tag hasn't in XML
                            else
                            {
                                var provider = from p in db.Providers
                                               where (p.ProviderName == "DTag")
                                               select new
                                               {
                                                   providerData = p
                                               };
                                if (provider.Count() == 1)
                                {
                                    //Set Provider D-Tag
                                    return (Provider)provider.First().providerData;
                                }
                                else
                                {
                                    throw new InvalidRequestException("No Provider in Database");
                                }
                            }
                        }
                    }
                }
            }
            else if (agentDutyCode == "3C1A20T8H25")
            {
                var provider = from p in db.Providers
                               where (p.ProviderName == "hertz")
                               select new
                               {
                                   providerdata = p
                               };
                if (provider.Count() == 1)
                {
                    //set provider hertz
                    return (Provider)provider.First().providerdata;
                }
                else
                {
                    throw new InvalidRequestException("no provider in database");
                }
            }
            else
            {
                var tcList = from t in db.TC4R_List
                             join c in db.Countries on t.CountryIDFK equals c.CountryID
                             join l in db.Locations on c.CountryID equals l.CountryIDFK
                             where (l.LocationCode == pickupLocationCode)
                             select new
                             {
                                 t.CountryIDFK,
                                 c.CountryName
                             };
                if (tcList.Count() > 0)
                {
                    var provider = from p in db.Providers
                                   where (p.ProviderName == "Hertz")
                                   select new
                                   {
                                       providerData = p
                                   };
                    if (provider.Count() == 1)
                    {
                        //Set Provider Hertz
                        return (Provider)provider.First().providerData;
                    }
                    else
                    {
                        throw new InvalidRequestException("No Provider in Database");
                    }
                }
                // Not TC4R List
                else
                {
                    throw new InvalidRequestException("Not Allowed this Partners for Non TC4R List Countries");
                }
            }

            //----------------------------------------------------------------------------------
            // Check pickup Country in TC4R List
            //    var tcList = from t in db.TC4R_List
            //                 join c in db.Countries on t.CountryIDFK equals c.CountryID
            //                 join l in db.Locations on c.CountryID equals l.CountryIDFK
            //                 where (l.LocationCode == pickupLocationCode)
            //                 select new
            //                 {
            //                     t.CountryIDFK,
            //                     c.CountryName
            //                 };
            //    if (tcList.Count() > 0)
            //    {
            //        var provider = from p in db.Providers
            //                       where (p.ProviderName == "Hertz")
            //                       select new
            //                       {
            //                           providerData = p
            //                       };
            //        if (provider.Count() == 1)
            //        {
            //            //Set Provider Hertz
            //            return (Provider)provider.First().providerData;
            //        }
            //        else
            //        {
            //            throw new InvalidRequestException("No Provider in Database");
            //        }
            //    }
            //    // Not TC4R List
            //    else
            //    {
            //        //Check Rate Qualifier value in DB
            //        var RQ = from r in db.Rate_Qulifiers
            //                 where (r.Rate_Qulifier == providerRQ)
            //                 select new
            //                 {
            //                     r.Rate_Qulifier
            //                 };
            //        if (RQ.Count() == 1)
            //        {
            //            //Check Partner in DB
            //            var Partner = from p in db.Partners
            //                          where (p.AgentDutyCode == agentDutyCode)
            //                          select new
            //                          {
            //                              p.CT_Allowed
            //                          };
            //            if (Partner.Count() == 1)
            //            {
            //                //Check Partner allowed the CarTrawler
            //                if (Partner.FirstOrDefault().CT_Allowed == 1)
            //                {
            //                    var provider = from p in db.Providers
            //                                   where (p.ProviderName == "CarTrawler")
            //                                   select new
            //                                   {
            //                                       providerData = p
            //                                   };
            //                    if (provider.Count() == 1)
            //                    {
            //                        //Set Provider CarTrawler
            //                        return (Provider)provider.First().providerData;
            //                    }
            //                    else
            //                    {
            //                        throw new InvalidRequestException("No Provider in Database");
            //                    }
            //                }
            //                //Partner not Allowed
            //                else
            //                {
            //                    var provider = from p in db.Providers
            //                                   where (p.ProviderName == "DTag")
            //                                   select new
            //                                   {
            //                                       providerData = p
            //                                   };
            //                    if (provider.Count() == 1)
            //                    {
            //                        //Set Provider D-Tag
            //                        return (Provider)provider.First().providerData;
            //                    }
            //                    else
            //                    {
            //                        throw new InvalidRequestException("No Provider in Database");
            //                    }
            //                }
            //            }
            //            else
            //            {
            //                throw new InvalidRequestException("No Partner in select AgentDuty Code");
            //            }
            //        }
            //        //Rate Qualifier not in DB
            //        else
            //        {
            //            //Check has Tour Tag
            //            if (tourtag == true)
            //            {
            //                //Check Partner in DB
            //                var Partner = from p in db.Partners
            //                              where (p.AgentDutyCode == agentDutyCode)
            //                              select new
            //                              {
            //                                  p.CT_Allowed
            //                              };
            //                if (Partner.Count() == 1)
            //                {
            //                    //Check Partner allowed the CarTrawler
            //                    if (Partner.FirstOrDefault().CT_Allowed == 1)
            //                    {
            //                        var provider = from p in db.Providers
            //                                       where (p.ProviderName == "CarTrawler")
            //                                       select new
            //                                       {
            //                                           providerData = p
            //                                       };
            //                        if (provider.Count() == 1)
            //                        {
            //                            //Set Provider CarTrawler
            //                            return (Provider)provider.First().providerData;
            //                        }
            //                        else
            //                        {
            //                            throw new InvalidRequestException("No Provider in Database");
            //                        }
            //                    }
            //                    //Partner not Allowed
            //                    else
            //                    {
            //                        var provider = from p in db.Providers
            //                                       where (p.ProviderName == "DTag")
            //                                       select new
            //                                       {
            //                                           providerData = p
            //                                       };
            //                        if (provider.Count() == 1)
            //                        {
            //                            //Set Provider D-Tag
            //                            return (Provider)provider.First().providerData;
            //                        }
            //                        else
            //                        {
            //                            throw new InvalidRequestException("No Provider in Database");
            //                        }
            //                    }
            //                }
            //                else
            //                {
            //                    throw new InvalidRequestException("No Partner in select AgentDuty Code");
            //                }
            //            }
            //            //Tour Tag hasn't in XML
            //            else
            //            {
            //                var provider = from p in db.Providers
            //                               where (p.ProviderName == "DTag")
            //                               select new
            //                               {
            //                                   providerData = p
            //                               };
            //                if (provider.Count() == 1)
            //                {
            //                    //Set Provider D-Tag
            //                    return (Provider)provider.First().providerData;
            //                }
            //                else
            //                {
            //                    throw new InvalidRequestException("No Provider in Database");
            //                }
            //            }
            //        }
            //    }
        }
        else if (brand == Brands.DOLLAR.ToString())
        {
            //var provider = from p in db.Providers
            //               where (p.ProviderName == "DTag")
            //               select new
            //               {
            //                   providerData = p
            //               };
            //if (provider.Count() == 1)
            //{
            //    //Set Provider D-Tag
            //    return (Provider)provider.First().providerData;
            //}
            //else
            //{
            //    throw new InvalidRequestException("No Provider in Database");
            //}
            var provider = from p in db.Providers
                           where (p.ProviderName == "hertz")
                           select new
                           {
                               providerdata = p
                           };
            if (provider.Count() == 1)
            {
                //set provider hertz
                return (Provider)provider.First().providerdata;
            }
            else
            {
                throw new InvalidRequestException("no provider in database");
            }
        }
        else if (brand == Brands.FIREFLY.ToString())
        {
            //Check Rate Qualifier value in DB
            var RQ = from r in db.Rate_Qulifiers
                               where (r.Rate_Qulifier == providerRQ)
                               select new
                               {
                                   r.Rate_Qulifier
                               };
            if (RQ.Count() == 1)
            {
                var provider = from p in db.Providers
                               where (p.ProviderName == "Hertz")
                               select new
                               {
                                   providerData = p
                               };
                if (provider.Count() == 1)
                {
                    //Set Provider Hertz
                    return (Provider)provider.First().providerData;
                }
                else
                {
                    throw new InvalidRequestException("No Provider in Database");
                }
            }
            //Rate Qualifier not in DB
            else 
            {
                var provider = from p in db.Providers
                               where (p.ProviderName == "Thermeon")
                               select new
                               {
                                   providerData = p
                               };
                if (provider.Count() == 1)
                {
                    //Set Provider Thermeon
                    return (Provider)provider.First().providerData;
                }
                else
                {
                    throw new InvalidRequestException("No Provider in Database");
                }
            }
        }
        else
        {
            throw new InvalidRequestException("No Provider found for provided parameters");
        }
    }
}