﻿namespace Extractors
{

    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Xml;
    using Exceptions;
    using ORM;

    /// <summary>
    /// Summary description for GenericXmlDataExtractor
    /// </summary>
    public class GenericXmlDataExtractor
    {
        private DatabaseEntities db;

        public GenericXmlDataExtractor(DatabaseEntities db)
        {
            this.db = db;
        }

        /*
        * Extract Pickup location Data
        */
        public Location getPickupLocationDetails(XmlDocument xmlDocument, XmlNamespaceManager namespaceMgr)
        {
            XmlNode pickupLocationNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:PickUpLocation", namespaceMgr);
            if (pickupLocationNode != null)
            {
                if (pickupLocationNode.Attributes != null)
                {
                    XmlAttribute pickupLocationAttr = pickupLocationNode.Attributes["LocationCode"];
                    string pickupLocationCode = pickupLocationAttr.Value.ToString().TrimEnd();
                    var location = db.Locations.Where(l => l.LocationCode == pickupLocationCode);
                    Logger.Debug("Received Location Code...................... : " + pickupLocationCode + " ---------------------- Count : " + location.Count());
                    if (location.Count() > 0)
                    {
                        return location.First();
                    }
                    else
                    {
                        throw new InvalidRequestException("No Location found for received Pickup Location Code.");
                    }
                }
                else
                {
                    throw new InvalidRequestException("Cannot find Pickup LocationCode Attribute.");
                }
            }
            else
            {
                throw new InvalidRequestException("Cannot find Pickup Location Node.");
            }
        }

        /*
        * Extract Return location Data
        */
        public Location getDropOffLocationDetails(XmlDocument xmlDocument, XmlNamespaceManager namespaceMgr)
        {
            XmlNode returnLocationNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:ReturnLocation", namespaceMgr);
            if (returnLocationNode != null)
            {
                if (returnLocationNode.Attributes != null)
                {
                    XmlAttribute returnLocationAttr = returnLocationNode.Attributes["LocationCode"];
                    string returnLocationCode = returnLocationAttr.Value.ToString().TrimEnd();
                    var location = db.Locations.Where(l => l.LocationCode == returnLocationCode);
                    if (location.Count() > 0)
                    {
                        return location.First();
                    }
                    else
                    {
                        throw new InvalidRequestException("No Location found for received Return Location Code.");
                    }
                }
                else
                {
                    throw new InvalidRequestException("Cannot find Return LocationCode Attribute.");
                }
            }
            else
            {
                throw new InvalidRequestException("Cannot find Return Location Node.");
            }
        }

        /*
        * Extract Pickupdate and returndate
        */
        public DateTime[] getPickupAndReturnDates(XmlDocument xmlDocument, XmlNamespaceManager namespaceMgr)
        {
            XmlNode datetimeNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:VehRentalCore", namespaceMgr);
            if (datetimeNode != null)
            {
                if (datetimeNode.Attributes != null)
                {
                    return new DateTime[]{
                        DateTime.Parse(datetimeNode.Attributes["PickUpDateTime"].Value.ToString().TrimEnd()),
                        DateTime.Parse(datetimeNode.Attributes["ReturnDateTime"].Value.ToString().TrimEnd())
                    };
                }
                else
                {
                    throw new InvalidRequestException("Cannot find VehRentalCore Attributes.");
                }
            }
            else
            {
                throw new InvalidRequestException("Cannot find VehRentalCore Node.");
            }
        }

        /*
        * Extract Agent Duty Code and Source Country code
        */
        public string[] getISOCodeAgentDutyCode(XmlDocument xmlDocument, XmlNamespaceManager namespaceMgr)
        {
            string sourceCountry = "";
            string agentDutyCode = "";
            XmlNode sourceNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:Source", namespaceMgr);
            if (sourceNode != null)
            {
                if (sourceNode.Attributes != null)
                {
                    if (sourceNode.Attributes["ISOCountry"] != null)
                    {
                        sourceCountry = sourceNode.Attributes["ISOCountry"].Value.ToString().TrimEnd();
                    }
                    else
                    {
                        XmlNode citizenCountryNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:CitizenCountryName", namespaceMgr);
                        if (citizenCountryNode != null)
                        {
                            if (citizenCountryNode.Attributes != null)
                            {
                                if (citizenCountryNode.Attributes["Code"] != null)
                                {
                                    sourceCountry = citizenCountryNode.Attributes["Code"].Value.ToString().TrimEnd();
                                }
                                else
                                {
                                    throw new InvalidRequestException("Cannot find Source Attributes.");
                                }
                            }
                            else
                            {
                                throw new InvalidRequestException("Cannot find Source Attributes.");
                            }
                        }
                        else
                        {
                            throw new InvalidRequestException("Cannot find Source Node.");
                        }
                    }

                    if (sourceNode.Attributes["AgentDutyCode"] != null)
                    {
                        agentDutyCode = sourceNode.Attributes["AgentDutyCode"].Value.ToString().Trim();
                    }
                    else
                    {
                        XmlNode requestorIdNode = sourceNode.SelectSingleNode("//a1:RequestorID", namespaceMgr);
                        if (requestorIdNode != null)
                        {
                            if (requestorIdNode.Attributes != null)
                            {
                                agentDutyCode = requestorIdNode.Attributes["ID"].Value.ToString().TrimEnd();
                            }
                            else
                            {
                                throw new InvalidRequestException("Cannot find RequestorID Agentcode Attributes.");
                            }
                        }
                        else
                        {
                            throw new InvalidRequestException("Cannot find RequestorID Agentcode Node.");
                        }
                    }
                    return new string[]{
                        sourceCountry,
                        agentDutyCode
                    };
                }
                else
                {
                    throw new InvalidRequestException("Cannot find Source Attributes.");
                }
            }
            else
            {
                throw new InvalidRequestException("Cannot find Source Node.");
            }
        }
    }
}