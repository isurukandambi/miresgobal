﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Exceptions;
using ORM;

/// <summary>
/// Resole Brand Identifier from request
/// </summary>
public class BrandResolver
{

    public Brand extractBrand(string pathParamBrand, DatabaseEntities db)
    {
        var brand = db.Brands.Where(b => b.BrandName == pathParamBrand);
        if (brand.Count() == 1)
        {
            return brand.First();
        }
        else
        {
            throw new InvalidRequestException("No Brand found for provided parameter");
        }
    }
}