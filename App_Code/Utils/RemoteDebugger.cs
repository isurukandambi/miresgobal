﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Class1
/// </summary>
public class RemoteDebugger
{
    public static void SendXmlForDebugging(string payloadXml, string subject)
    {
        if (AppSettings.IsDebugXmlEmailEnabled)
        {
            List<string> toAddressList = AppSettings.DebugXmlDetailsNotifyEmails.Split(',').ToList();
            EmailClient.sendEmail(AppSettings.SiteEmailsSentFrom, toAddressList, subject + " " + DateTime.Now, payloadXml, false);
        }
    }
}