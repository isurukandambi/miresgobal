﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;

/// <summary>
/// Summary description for EmailClient
/// </summary>
public class EmailClient
{
    public static bool sendEmail(string FromAddress, List<string> ToAddressList, string Subject, string EmailMessage, bool HTMLMessage)
    {
        bool MsgSent = false;
        try
        {
            MailMessage mailMessage = new MailMessage();
            mailMessage.From = new MailAddress(FromAddress);
            foreach (string toAddress in ToAddressList)
            {
                mailMessage.To.Add(new MailAddress(toAddress));
            }
            mailMessage.Subject = Subject;
            mailMessage.Body = EmailMessage;
            mailMessage.IsBodyHtml = HTMLMessage;
            SmtpClient smtpClient = new SmtpClient();
            smtpClient.Send(mailMessage);
            MsgSent = true;
        }
        catch
        {
            MsgSent = false;
        }
        return MsgSent;
    }
}