﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

/// <summary>
/// Summary description for Reflector
/// </summary>
public class Reflector
{
    public static T Construct<F, T>(F Base) where T : F, new()
    {
        T derived = new T();
        PropertyInfo[] properties = typeof(F).GetProperties();
        foreach (PropertyInfo bp in properties)
        {
            PropertyInfo dp = typeof(T).GetProperty(bp.Name, bp.PropertyType);
            if (
                (dp != null)
                && (dp.GetSetMethod() != null)
                && (bp.GetIndexParameters().Length == 0)
                && (dp.GetIndexParameters().Length == 0)
            )
             dp.SetValue(derived, dp.GetValue(Base, null), null);
        }

        return derived;
    }
}