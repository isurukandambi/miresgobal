﻿using System;
using System.Text;
using System.Web;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Globalization;
using ORM;

using CMSConfigs;
using ProcessResources;

/// <summary>
/// Summary description for BuildConfirmationEmail
/// </summary>
public class BuildConfirmationEmail
{
    VehicleResRQResources resources;
    BookedVehicle vehicle;

    public BuildConfirmationEmail(VehicleResRQResources resources, BookedVehicle vehDetail)
	{
        this.resources = resources;
        this.vehicle = vehDetail;
 
	}

    //public string BuildBookingFailedEmail()
    //{

    //    var emailTemplate = new StringBuilder();
    //    emailTemplate.Append("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">");

    //    emailTemplate.Append("<html>");
    //    emailTemplate.Append("<head>");
    //    emailTemplate.Append("<title>" + HttpContext.GetGlobalResourceObject("SiteStrings", "BookingEmailSubject") + "</title>");
    //    emailTemplate.Append("<link href=\"http://" + CMSSettings.SiteUrl + "/groupemail/groupemail.css?v=1\" rel=\"stylesheet\" type=\"text/css\" />");
    //    emailTemplate.Append("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />");
    //    emailTemplate.Append("</head>");
    //    emailTemplate.Append("<body>");
    //    emailTemplate.Append("<table class=\"toplevel\">");

    //    //email banner
    //    var bannerLogo = "hertz-header-logo.gif";
    //    if (reso.CarProviderSelected == CarProvider.Thrifty)
    //        bannerLogo = "thrifty-header-logo.gif";
    //    if (_booking.CarProviderSelected == CarProvider.FireFly)
    //        bannerLogo = "firefly-header-logo.gif";

    //    emailTemplate.Append("<tr>");
    //    emailTemplate.Append("<td>");
    //    emailTemplate.Append("<table width=\"600\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">");
    //    emailTemplate.Append("<tr>");
    //    emailTemplate.Append("<td width=\"151\">");
    //    emailTemplate.Append("<a href=\"http://" + "www.hertz4ryanair.com" + "?link=OrderConfirmation&lang=" + _booking.Language + "\">");
    //    emailTemplate.Append("<img src=\"http://" + CMSSettings.SiteUrl + "/groupemail/images/" + bannerLogo + "\" border=\"0\" width=\"151\"></a></td>");
    //    emailTemplate.Append("<td width=\"449\">");
    //    emailTemplate.Append("<a href=\"http://" + "www.hertz4ryanair.com" + "?link=OrderConfirmation&lang=" + _booking.Language + "\">");
    //    emailTemplate.Append("<img src=\"http://" + CMSSettings.SiteUrl + "/groupemail/images/header-banner1.gif\" border=\"0\" width=\"449\"></a></td>");
    //    emailTemplate.Append("</tr>");
    //    emailTemplate.Append("</table>");
    //    emailTemplate.Append("</td>");
    //    emailTemplate.Append("</tr>");

    //    emailTemplate.Append("<tr>");
    //    emailTemplate.Append("<td class=\"maincontent\">");
    //    emailTemplate.Append("<table width=\"600px\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" >");



    //    emailTemplate.Append("<tr>");
    //    emailTemplate.Append("<td colspan=\"3\" style=\"height: 20px;\"></td>");
    //    emailTemplate.Append("</tr>");
    //    emailTemplate.Append("<tr>");
    //    emailTemplate.Append("<td colspan=\"3\">" + FirstLineForFailEmail() + "</td>");
    //    emailTemplate.Append("</tr>");
    //    emailTemplate.Append("<tr>");
    //    emailTemplate.Append("<td colspan=\"3\" style=\"height: 20px;\"></td>");
    //    emailTemplate.Append("</tr>");
    //    emailTemplate.Append("<tr>");
    //    emailTemplate.Append("<td colspan=\"3\">" + ErrorReasonFailEmail() + "</td>");
    //    emailTemplate.Append("</tr>");
    //    emailTemplate.Append("<tr>");
    //    emailTemplate.Append("<td colspan=\"3\" style=\"height: 20px;\"></td>");
    //    emailTemplate.Append("</tr>");
    //    emailTemplate.Append("<tr>");
    //    emailTemplate.Append("<td colspan=\"3\">" + DeepLinkAdviseText() + "</td>");
    //    emailTemplate.Append("</tr>");
    //    emailTemplate.Append("<tr>");
    //    emailTemplate.Append("<td colspan=\"3\" style=\"height: 20px;\"></td>");
    //    emailTemplate.Append("</tr>");
    //    emailTemplate.Append("<tr>");
    //    emailTemplate.Append("<td colspan=\"3\">" + DeepLinkToHertzForRyanair() + "</td>");
    //    emailTemplate.Append("</tr>");
    //    emailTemplate.Append("<tr>");
    //    emailTemplate.Append("<td colspan=\"3\" style=\"height: 20px;\"></td>");
    //    emailTemplate.Append("</tr>");
    //    emailTemplate.Append("<tr>");
    //    emailTemplate.Append("<td colspan=\"3\">" + FaqAndContactAdviseText() + "</td>");
    //    emailTemplate.Append("</tr>");
    //    emailTemplate.Append("<tr>");
    //    emailTemplate.Append("<td colspan=\"3\" style=\"height:20px;\">" + AppendErrorRulerToBottom() + "</td>");
    //    emailTemplate.Append("</tr>");
    //    emailTemplate.Append("</table>");


    //    //Terms and conditions
    //    //emailTemplate.Append("<br>");
    //    emailTemplate.Append("<table width=\"600px\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" >");

    //    // partner footer
    //    emailTemplate.Append(PartnerFooter());

    //    //Footer details
    //    emailTemplate.Append("</table></td></tr>");
    //    emailTemplate.Append("<tr><td class=\"footer\"><a href=\"" + HttpContext.GetGlobalResourceObject("SiteStrings", "BookingEmailTermsLink") + "\">" + HttpContext.GetGlobalResourceObject("SiteStrings", "BookingEmailTermsLinkText") + "</a> | <a href=\"" + HttpContext.GetGlobalResourceObject("SiteStrings", "BookingEmailContactLink") + "?lang=" + _booking.Language + "\">" + HttpContext.GetGlobalResourceObject("SiteStrings", "BookingEmailContactLinkText") + "</a> | <a href=\"" + HttpContext.GetGlobalResourceObject("SiteStrings", "BookingEmailPrivacyLink") + "\">" + HttpContext.GetGlobalResourceObject("SiteStrings", "BookingEmailPrivacyLinkText") + "</a>");
    //    emailTemplate.Append("<br/><br/>The Hertz Corporation, 14501 Hertz Quail Springs Parkway, PO Box 269033, Oklahoma City, OK, 73126-9033, U.S.A</td></tr>");
    //    emailTemplate.Append("</table>");
    //    emailTemplate.Append("</body>");
    //    emailTemplate.Append("</html>");

    //    return emailTemplate.ToString();
    //}

    //private string FirstLineForFailEmail()
    //{
    //    return HttpContext.GetGlobalResourceObject("SiteStrings", "BookingFailEmailDeartext") + " " + _booking.Customer.Firstname + " " + _booking.Customer.Surname + ", ";
    //}

    //private string ErrorReasonFailEmail()
    //{
    //    return HttpContext.GetGlobalResourceObject("SiteStrings", "BookingFailBrandNoticeBeforeText") + " " + _booking.CarProviderSelected.ToString() + " " + HttpContext.GetGlobalResourceObject("SiteStrings", "BookingFailBrandNoticeAfterText") + " " + (string)HttpContext.GetGlobalResourceObject("HertzErrorMessages", "E" + _booking.ErrorDetails.ErrorCode) + ". ";
    //}

    //private string DeepLinkAdviseText()
    //{
    //    return HttpContext.GetGlobalResourceObject("SiteStrings", "BookingFailHertzLinkAdviseText").ToString();
    //}
    //private string DeepLinkToHertzForRyanair()
    //{
    //    string pickupdate = _booking.PickupDateTime.Year + "" + _booking.PickupDateTime.Month.ToString("D2") + "" + _booking.PickupDateTime.Day.ToString("D2");
    //    string pickuptime = _booking.PickupDateTime.Hour.ToString("D2") + "" + _booking.PickupDateTime.Minute.ToString("D2");
    //    string dropoffupdate = _booking.DropOffDateTime.Year + "" + _booking.DropOffDateTime.Month.ToString("D2") + "" + _booking.DropOffDateTime.Day.ToString("D2");
    //    string dropofftime = _booking.DropOffDateTime.Hour.ToString("D2") + "" + _booking.DropOffDateTime.Minute.ToString("D2");
    //    return "<a href=\"http://www.hertz4ryanair.com/default.aspx?POS=" + _booking.CountryOfResidenceCode + "&lang=" + _booking.Language + "&PULoc=" + _booking.PickUpLocationCode + "&DOLoc=" + _booking.DropOffLocationCode + "&PUDate=" + pickupdate + "&PUTime=" + pickuptime + "&DODate=" + dropoffupdate + "&DOTime=" + dropofftime + "\" target=\"_blank\">http://www.hertz4ryanair.com</a>";
    //}

    //private string FaqAndContactAdviseText()
    //{
    //    return HttpContext.GetGlobalResourceObject("SiteStrings", "BookingFailProblemText") + " " + "<a href='http://www.hertz4ryanair.com/FAQContacts.aspx?lang=" + _booking.Language + "'>" + HttpContext.GetGlobalResourceObject("SiteStrings", "BookingFailFaqContactText") + "</a>";
    //}
    //private string AppendErrorRulerToBottom()
    //{
    //    var bannerLogo = "error-ruler-grey.gif";
    //    if (_booking.CarProviderSelected == CarProvider.Hertz)
    //        bannerLogo = "error-ruler.gif";
    //    return "<img src=\"http://" + CMSSettings.SiteUrl + "/groupemail/images/" + bannerLogo + "\" alt=\"horizontal line\" >";
    //}

    public string BuildEmail(string confirmationNum, string carCurrencySymbol, string carCost, string carImage, string localizedCar, decimal additionalDropOffFee, string priceIncludes, string distanceIncluded, int numDaysRental)
    {
        var totalHeaderText = (string)HttpContext.GetGlobalResourceObject("SiteStrings", "BookingEmailTotalPaidPrePaid");
        var payForExtrasText = (string)HttpContext.GetGlobalResourceObject("SiteStrings", "BookingEmailPriceIncludesSubTextPrePaid");
        //if (_booking.CarProviderSelected == CarProvider.FireFly)
        //{
        //    payForExtrasText += (string)HttpContext.GetGlobalResourceObject("SiteStrings", "LocationInstructions_Alghero");
        //}
        /*if (!_booking.PrePaySelected)
        {
            totalHeaderText = (string)HttpContext.GetGlobalResourceObject("SiteStrings", "BookingEmailTotalPaidNonPrePaid");
            payForExtrasText = (string)HttpContext.GetGlobalResourceObject("SiteStrings", "BookingEmailPriceIncludesSubTextNonPrePaid");
        }*/

        var emailTemplate = new StringBuilder();
        emailTemplate.Append("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">");

        emailTemplate.Append("<html>");
        emailTemplate.Append("<head>");
        emailTemplate.Append("<title>" + HttpContext.GetGlobalResourceObject("SiteStrings", "BookingEmailSubject") + "</title>");
        emailTemplate.Append("<link href=\"http://" + CMSSettings.SiteUrl + "/groupemail/groupemail.css?v=1\" rel=\"stylesheet\" type=\"text/css\" />");
        emailTemplate.Append("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />");
        emailTemplate.Append("</head>");
        emailTemplate.Append("<body>");
        emailTemplate.Append("<table class=\"toplevel\">");

        //email banner
        var bannerLogo = "hertz-header-logo.gif";
        if (this.resources.BrandName == Brands.THRIFTY.ToString())
            bannerLogo = "thrifty-header-logo.gif";
        if (this.resources.BrandName == Brands.FIREFLY.ToString())
            bannerLogo = "firefly-header-logo.gif";

        emailTemplate.Append("<tr>");
        emailTemplate.Append("<td>");
        emailTemplate.Append("<table width=\"600\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">");
        emailTemplate.Append("<tr>");
        emailTemplate.Append("<td width=\"151\">");
        emailTemplate.Append("<a href=\"http://" + "www.hertz4ryanair.com" + "?link=OrderConfirmation&lang=" + resources.Language + "\">");
        emailTemplate.Append("<img src=\"http://" + CMSSettings.SiteUrl + "/groupemail/images/" + bannerLogo + "\" border=\"0\" width=\"151\"></a></td>");
        emailTemplate.Append("<td width=\"449\">");
        emailTemplate.Append("<a href=\"http://" + "www.hertz4ryanair.com" + "?link=OrderConfirmation&lang=" + resources.Language + "\">");
        emailTemplate.Append("<img src=\"http://" + CMSSettings.SiteUrl + "/groupemail/images/header-banner1.gif\" border=\"0\" width=\"449\"></a></td>");
        emailTemplate.Append("</tr>");
        emailTemplate.Append("</table>");
        emailTemplate.Append("</td>");
        emailTemplate.Append("</tr>");

        emailTemplate.Append("<tr>");
        emailTemplate.Append("<td class=\"maincontent\">");
        emailTemplate.Append("<table width=\"600px\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" >");

        //Email opening
        emailTemplate.Append(OpeningLineForEmail());
        //RemoteDebugger.SendXmlForDebugging("Test", "Test Mail1214");
        //Confirmation number, Price and Price Includes text
        emailTemplate.Append(ConfirmationNumberAndPriceAndPriceIncludesForEmail(totalHeaderText, priceIncludes, distanceIncluded, payForExtrasText));
        //RemoteDebugger.SendXmlForDebugging("Test", "Test Mail12");
        //// Location Advisory
        emailTemplate.Append(LocationAdvisoryForEmail());

        ////Manage My Booking and Online Check-in links
        emailTemplate.Append(ManageBookingLinkForEmail());

        //Firefly - Cancel or Modify booking Text
        /* if (_booking.CarProviderSelected == CarProvider.FireFly)
         {
             emailTemplate.Append(FireFlyTextForEmail());
         }*/

        emailTemplate.Append("<tr>");
        emailTemplate.Append("<td colspan=\"3\" style=\"height: 20px;\"></td>");
        emailTemplate.Append("</tr>");
        emailTemplate.Append("</table>");

        emailTemplate.Append("<table width=\"600px\" style=\"border: 1px solid #DEE0E0;\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" >");

        ////Pickup, Drop Off details
        emailTemplate.Append(PickupDropOffDetailsForEmail());

        //// Location Details - Opening hrs, full address, provider, etc
        emailTemplate.Append(LocationDetailsForEmail());

        //// Flight Number
        emailTemplate.Append(FlightNumberForEmail());

        //// Car Details
        emailTemplate.Append(CarDetailsForEmail(carImage, localizedCar, carCurrencySymbol, carCost));

        //// Accessories or Drop off
        //emailTemplate.Append(AccessoriesAndDropFeeForEmail());

        //// MFP text - if not valid a generic price includes text is diplayed.
        emailTemplate.Append(MandatoryFuelPurchaseForEmail(numDaysRental));

        //// Cleaning Fee
        emailTemplate.Append(CleaningFeeForEmail());

        emailTemplate.Append("</table>");

        //Terms and conditions
        emailTemplate.Append("<br>");
        emailTemplate.Append("<table width=\"600px\" style=\"border: 1px solid #DEE0E0;\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" >");

        // Do not show Ryanair terms content for Auto Europe bookings
        //emailTemplate.Append(GetBookingTermsLabelText());
        emailTemplate.Append("<tr>");
        //emailTemplate.Append("<td colspan=\"2\">" + GetBookingTermsLabelText() + "</td>");
        emailTemplate.Append("</tr>");

        // eBilling Text
        emailTemplate.Append(EBillingContent());

        // partner footer
        emailTemplate.Append(PartnerFooter());

        //Footer details
        emailTemplate.Append("</table></td></tr>");
        emailTemplate.Append("<tr><td class=\"footer\"><a href=\"" + HttpContext.GetGlobalResourceObject("SiteStrings", "BookingEmailTermsLink") + "\">" + HttpContext.GetGlobalResourceObject("SiteStrings", "BookingEmailTermsLinkText") + "</a> | <a href=\"" + HttpContext.GetGlobalResourceObject("SiteStrings", "BookingEmailContactLink") + "?lang=" + this.resources.Language + "\">" + HttpContext.GetGlobalResourceObject("SiteStrings", "BookingEmailContactLinkText") + "</a> | <a href=\"" + HttpContext.GetGlobalResourceObject("SiteStrings", "BookingEmailPrivacyLink") + "\">" + HttpContext.GetGlobalResourceObject("SiteStrings", "BookingEmailPrivacyLinkText") + "</a>");
        emailTemplate.Append("<br/><br/>The Hertz Corporation, 14501 Hertz Quail Springs Parkway, PO Box 269033, Oklahoma City, OK, 73126-9033, U.S.A</td></tr>");
        emailTemplate.Append("</table>");
        emailTemplate.Append("</body>");
        emailTemplate.Append("</html>");

        //RemoteDebugger.SendXmlForDebugging("Test","Test Mail");
        return emailTemplate.ToString();
        //return "";
    }

    protected string GetGoldcardAdvisor()
    {
        string message = "";

        if (this.resources.ConfirmationID.IndexOf("GOLD") != -1)
        {
            //replace with resx file
            message = "<tr><td colspan=\"2\" bgcolor=\"#FFF326\" align=\"center\" style=\"font-size: 14px;\" >" + HttpContext.GetGlobalResourceObject("SiteStrings", "GoldServiceConfirmedText") + "</td></tr>";
        }
        else if (this.resources.ConfirmationID.IndexOf("CNTR") != -1)
        {
            //replace with resx file
            message = "<tr><td colspan=\"3\"><strong>" + HttpContext.GetGlobalResourceObject("SiteStrings", "GoldServiceNotConfirmedText") + "</strong>" + "</td></tr>";
        }

        return message;
    }

    //protected string GetBookingTermsLabelText()
    //{

    //    string termsLabel = "";
    //    string prepayterms = "";
    //    //choose terms to show and pricing block to display - terms are decided by source country as specific pricing is displayed
    //    if (_booking.PrePaySelected)
    //    {
    //        if (_booking.CarProviderSelected == CarProvider.Hertz)
    //        {
    //            prepayterms = (string)HttpContext.GetGlobalResourceObject("SiteStrings", "RyTermsCondlbl");
    //            prepayterms += (string)HttpContext.GetGlobalResourceObject("SiteStrings", "RyanairImportantInfo");
    //            prepayterms = prepayterms.Replace("Hertz", _booking.CarProviderSelected.ToString());
    //            prepayterms += SelectTermsText.LongTermsToUse(_booking.Country, _booking.CarProviderSelected);
    //        }
    //        else
    //        {
    //            prepayterms = (string)HttpContext.GetGlobalResourceObject("SiteStrings", "RyTermsCondlbl");
    //            prepayterms = prepayterms.Replace("Hertz", _booking.CarProviderSelected.ToString());
    //            prepayterms += SelectTermsText.LongTermsToUse(_booking.Country, _booking.CarProviderSelected);
    //        }
    //        //prepayterms = SelectTermsText.LongTermsToUse(_booking.Country, _booking.CarProviderSelected);
    //        //replace link and site url by ryanair.com
    //        string getlink = RentalQualLinkLocationCode();
    //        string correctlink = prepayterms.Replace("<RQRlink>", getlink);
    //        correctlink = correctlink.Replace("<Contact>", "http://www.hertz4ryanair.com/FAQContacts.aspx?lang=" + _booking.Language);
    //        correctlink = correctlink.Replace("<LINK>", "http://www.hertz4ryanair.com/managebooking.aspx?confirmationNumber=&lang=" + _booking.Language + "&POS=" + _booking.Country);
    //        termsLabel = correctlink;
    //    }
    //    else
    //    {
    //        termsLabel = (string)HttpContext.GetGlobalResourceObject("SiteStrings", "NonPrePaidTermsResource");
    //    }


    //    return termsLabel;
    //}

    //protected string RentalQualLinkLocationCode()
    //{
    //    string link = "https://www.hertz.com/rentacar/reservation/reviewmodifycancel/templates/rentalTerms.jsp?language=" + _booking.Language + "&EOAG=" + _booking.PickUpLocationCode;
    //    if (_booking.CarProviderSelected == CarProvider.FireFly)
    //    {
    //        string lang = "uk";
    //        switch (_booking.Language.ToLower())
    //        {
    //            case "en":
    //                lang = "uk";
    //                break;

    //            case "fr":
    //                lang = "fr";
    //                break;

    //            case "de":
    //                lang = "de";
    //                break;

    //            case "it":
    //                lang = "it";
    //                break;

    //            case "es":
    //                lang = "espana";
    //                break;

    //            case "nl":
    //                lang = "nl";
    //                break;

    //            default:
    //                goto case "en";
    //        }

    //        link = "https://" + lang + ".fireflycarrental.com/qualifications-" + _booking.PickUpCountryCode.ToUpper() + ".html";

    //    }

    //    return link;
    //}

    protected string OpeningLineForEmail()
    {
        var emailTemplate = new StringBuilder();

        //string emailLinePart2 = _booking.ModifyProcess ? (string)HttpContext.GetGlobalResourceObject("SiteStrings", "ModifyEmailFirstLinePt2") : (string)HttpContext.GetGlobalResourceObject("SiteStrings", "BookingEmailFirstLinePt2");

        emailTemplate.Append("<tr>");
        emailTemplate.Append("<td colspan=\"3\">" + HttpContext.GetGlobalResourceObject("SiteStrings", "BookingEmailFirstLinePt1") + " " + resources.CustomerFirstname + " " + resources.CustomerSurname + " " + resources.CustomerEmail + " " + resources.PickupDateTime.ToString("dd/MM/yyyy HH:mm:ss") + "</td>");
        emailTemplate.Append("</tr>");

        return emailTemplate.ToString();
    }
    ///*  protected string ManageBookingLinkForEmail()
    //  {
    //      var emailTemplate = new StringBuilder();

    //      //if ((_booking.CarProviderSelected == CarProvider.Hertz) || (_booking.CarProviderSelected == CarProvider.Thrifty) || (_booking.CarProviderSelected == CarProvider.FireFlyAutoEurope))
    //      //{
    //      var manageBookingLink = WebsiteSettings.HttpType + "://" + WebsiteSettings.SiteUrl + "/managebooking.aspx?confirmationNumber=" + _booking.ConfirmationId + "&lang=" + _booking.Language + "&POS=" + _booking.Country;
    //      // if provider is AutoEurope then send to manage booking page
        
    //      emailTemplate.Append("<tr>");
    //      emailTemplate.Append("<td align=\"left\"><a href=\"" + manageBookingLink + "\"><img src=\"http://" + WebsiteSettings.SiteUrl + "/GroupEmail/images/" + HttpContext.GetGlobalResourceObject("SiteStrings", "ManageMyBookingImg") + "\" alt=\"" + HttpContext.GetGlobalResourceObject("SiteStrings", "ManageMyBookingTxt") + "\" border=\"0\" /></a></td>");
    //      emailTemplate.Append("<td colspan=\"2\"align=\"right\"></td>");
    //      emailTemplate.Append("</tr>");
    //      //}

    //      return emailTemplate.ToString();
    //  }*/



    protected string ConfirmationNumberAndPriceAndPriceIncludesForEmail(string totalHeaderText, string priceIncludes, string distanceIncluded, string payForExtrasText)
    {
        //RemoteDebugger.SendXmlForDebugging("Test", "Test Mail12asfsa");
        //var totalHeaderAdvisor = (string)HttpContext.GetGlobalResourceObject("SiteStrings", "PricingAdvisorText");
        var totalHeaderAdvisor = "";
        var emailTemplate = new StringBuilder();
        
        emailTemplate.Append("<tr>");
        emailTemplate.Append("<td colspan=\"3\" style=\"height: 10px;\"></td>");
        emailTemplate.Append("</tr>");
        emailTemplate.Append("<tr>");
        emailTemplate.Append("<td colspan=\"3\">");
        emailTemplate.Append("<table width=\"600px\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\">");
        emailTemplate.Append("<tr height=\"33\" bgcolor=\"#2F79BB\" >");
        emailTemplate.Append("<td width=\"46\"><img src=\"http://" + CMSSettings.SiteUrl + "/groupemail/images/icon-car.gif\" width=\"46\" height=\"33\" border=\"0\" /></td>");
        emailTemplate.Append("<td width=\"350\"><span style=\"color: #ffffff; font-size: 14px; font-weight: bold;\">" + HttpContext.GetGlobalResourceObject("SiteStrings", "BookingEmailConfirmationNum") + " " + resources.ConfirmationID + "</span></td>");
        emailTemplate.Append("<td bgcolor=\"#ffffff\"></td>");
        emailTemplate.Append("</tr>");
        emailTemplate.Append("</table>");
        emailTemplate.Append("</td>");
        emailTemplate.Append("</tr>");
        emailTemplate.Append("<tr>");
        emailTemplate.Append("<td colspan=\"3\" style=\"height: 10px;\"></td>");
        emailTemplate.Append("</tr>");
        // if auto europe then include some extra details about rate code, business account and supplier id if available
        //if (_booking.CarProviderSelected == CarProvider.FireFlyAutoEurope)
        //{
        //    emailTemplate.Append("<tr>");
        //    emailTemplate.Append("<td colspan=\"3\">");
        //    if (!String.IsNullOrEmpty(_booking.AutoEurope.RateCode))
        //    {
        //        emailTemplate.Append("<b>" + HttpContext.GetGlobalResourceObject("SiteStrings", "AE_RateCode") + "</b><span class=\"loud\">" + _booking.AutoEurope.RateCode + "</span><br />");
        //    }
        //    if (!String.IsNullOrEmpty(_booking.AutoEurope.BusinessAccount))
        //    {
        //        emailTemplate.Append("<b>" + HttpContext.GetGlobalResourceObject("SiteStrings", "AE_BusinessAccount") + "</b><span class=\"loud\">" + _booking.AutoEurope.BusinessAccount + "</span><br />");
        //    }
        //    if (!String.IsNullOrEmpty(_booking.AutoEurope.SupplierConfirmation))
        //    {
        //        emailTemplate.Append("<b>" + HttpContext.GetGlobalResourceObject("SiteStrings", "AE_SupplierConfirmation") + "</b><span class=\"loud\">" + _booking.AutoEurope.SupplierConfirmation + "</span>");
        //    }
        //    emailTemplate.Append("</td>");
        //    emailTemplate.Append("</tr>");
        //}

        emailTemplate.Append("<tr>");
        emailTemplate.Append("<td colspan=\"3\" style=\"height: 10px;\"></td>");
        emailTemplate.Append("</tr>");
        //emailTemplate.Append(GetGoldcardAdvisor());
        emailTemplate.Append("<tr>");
        emailTemplate.Append("<td colspan=\"3\" class=\"loud\">" + totalHeaderText + " " + vehicle.Currency + vehicle.Price + " " + totalHeaderAdvisor + "</td>");
        emailTemplate.Append("</tr>");
        emailTemplate.Append("<tr>");
        emailTemplate.Append("<td colspan=\"3\" style=\"height: 10px;\"></td>");
        emailTemplate.Append("</tr>");
        emailTemplate.Append("<tr>");
        //emailTemplate.Append("<td colspan=\"3\">" + HttpContext.GetGlobalResourceObject("SiteStrings", "BookingEmailPriceIncludes") + " " + priceIncludes + (!String.IsNullOrEmpty(distanceIncluded) ? (", " + distanceIncluded) : "") + "<br>" + payForExtrasText + "</td>");
        emailTemplate.Append("<td colspan=\"3\">" + HttpContext.GetGlobalResourceObject("SiteStrings", "BookingEmailPriceIncludes") + " " + priceIncludes + (!String.IsNullOrEmpty(distanceIncluded) ? (", " + distanceIncluded.Replace("included", "")) : "") + "<br>" + payForExtrasText + "</td>");
        emailTemplate.Append("</tr>");
        emailTemplate.Append("<tr>");
        emailTemplate.Append("<td colspan=\"3\" style=\"height: 20px;\"></td>");
        emailTemplate.Append("</tr>");

        return emailTemplate.ToString();
    }

    protected string ManageBookingLinkForEmail()
    {
        var emailTemplate = new StringBuilder();

        if ((this.resources.BrandName.ToUpper() == Brands.HERTZ.ToString()) || (this.resources.BrandName.ToUpper() == Brands.THRIFTY.ToString()) || (this.resources.BrandName.ToUpper() == Brands.FIREFLY.ToString()))
        {
            var manageBookingLink = "http://" + "www.hertz4ryanair.com" + "/managebooking.aspx?confirmationNumber=" + this.resources.ConfirmationID + "&lang=" + this.resources.Language + "&POS=" + this.resources.CountryOfResidenceCode;
            // if provider is AutoEurope then send to manage booking page
            //if (_booking.CarProviderSelected == CarProvider.FireFlyAutoEurope)
            //    manageBookingLink = WebsiteSettings.HttpType + "://" + WebsiteSettings.SiteUrl + "/managebooking.aspx?ConfirmationNumber=" + _booking.ConfirmationId + "&Email=" + Server.UrlEncode(_booking.Customer.Email) + "&lang=" + _booking.Language + "&POS=" + _booking.Country;

            emailTemplate.Append("<tr>");
            //manage booking link
            emailTemplate.Append("<td align=\"left\"><a href=\"" + manageBookingLink + "\"><img src=\"http://" + CMSSettings.SiteUrl + "/GroupEmail/images/" + HttpContext.GetGlobalResourceObject("SiteStrings", "ManageMyBookingImg") + "\" alt=\"" + HttpContext.GetGlobalResourceObject("SiteStrings", "ManageMyBookingTxt") + "\" border=\"0\" /></a></td>");
            emailTemplate.Append("<td colspan=\"2\"align=\"right\"></td>");
            emailTemplate.Append("</tr>");
        }

        return emailTemplate.ToString();
    }

    //protected string FireFlyTextForEmail()
    //{
    //    var emailTemplate = new StringBuilder();

    //    //Firefly - Cancel or Modify booking Text
    //    emailTemplate.Append("<tr>");
    //    emailTemplate.Append("<td colspan=\"3\">");
    //    emailTemplate.Append("<table width=\"600px\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\">");
    //    emailTemplate.Append("<tr height=\"33\" bgcolor=\"#2F79BB\" >");
    //    emailTemplate.Append("<td width=\"46\"><img src=\"http://" + CMSSettings.SiteUrl + "/groupemail/images/icon-car.gif\" width=\"46\" height=\"33\" border=\"0\" /></td>");
    //    emailTemplate.Append("<td width=\"350\"><span style=\"color: #ffffff; font-size: 14px; font-weight: bold;\">" + HttpContext.GetGlobalResourceObject("SiteStrings", "BookingEmailFireflyHeader") + "</span></td>");
    //    emailTemplate.Append("<td bgcolor=\"#ffffff\"></td>");
    //    emailTemplate.Append("</tr>");
    //    emailTemplate.Append("<tr>");
    //    emailTemplate.Append("<td colspan=\"3\" style=\"height: 10px;\"></td>");
    //    emailTemplate.Append("</tr>");
    //    emailTemplate.Append("</table>");
    //    emailTemplate.Append("<tr>");
    //    emailTemplate.Append("<td colspan=\"3\">" + HttpContext.GetGlobalResourceObject("SiteStrings", "BookingEmailFireflyMain"));
    //    emailTemplate.Append("</td>");
    //    emailTemplate.Append("</tr>");
    //    emailTemplate.Append("</td>");
    //    emailTemplate.Append("</tr>");
    //    emailTemplate.Append("<tr>");
    //    emailTemplate.Append("<td colspan=\"3\" style=\"height: 10px;\"></td>");
    //    emailTemplate.Append("</tr>");

    //    return emailTemplate.ToString();

    //}

    protected string PickupDropOffDetailsForEmail()
    {
        var emailTemplate = new StringBuilder();

        emailTemplate.Append("<tr>");
        emailTemplate.Append("<td colspan=\"2\" style=\"background-color: #DEE0E0; padding: 0px 0px 0px 5px;\"  class=\"loud\"><b>" + HttpContext.GetGlobalResourceObject("SiteStrings", "BookingEmailPickUpDetailsHeader") + "</b></td>");
        emailTemplate.Append("<td colspan=\"2\" rowspan=\"14\"><img src=\"" + HttpContext.GetGlobalResourceObject("SiteStrings", "BookingEmailBanner") + "\" /></td>");
        emailTemplate.Append("</tr>");
        emailTemplate.Append("<tr>");
        emailTemplate.Append("<td width=\"299\"><b>" + HttpContext.GetGlobalResourceObject("SiteStrings", "BookingEmailPickupLocation") + "</b></td>");
        emailTemplate.Append("<td width=\"299\"><b>" + HttpContext.GetGlobalResourceObject("SiteStrings", "BookingEmailDropOffLocation") + "</b></td>");
        emailTemplate.Append("</tr>");
        emailTemplate.Append("<tr>");
        emailTemplate.Append("<td>" + this.resources.PickupLocationName + "</td>");
        emailTemplate.Append("<td>" + this.resources.DropoffLocationName + "</td>");
        emailTemplate.Append("</tr>");
        emailTemplate.Append("<tr>");
        emailTemplate.Append("<td colspan=\"2\" style=\"height: 5px;\"></td>");
        emailTemplate.Append("</tr>");
        emailTemplate.Append("<tr>");
        emailTemplate.Append("<td width=\"299\"><b>" + HttpContext.GetGlobalResourceObject("SiteStrings", "BookingEmailPickupDate") + "</b></td>");
        emailTemplate.Append("<td width=\"299\"><b>" + HttpContext.GetGlobalResourceObject("SiteStrings", "BookingEmailDropOffDate") + "</b></td>");
        emailTemplate.Append("</tr>");
        emailTemplate.Append("<tr>");
        emailTemplate.Append("<td>" + this.resources.PickupDateTime.ToString("dd/MM/yyyy HH:mm:ss") + "</td>");
        emailTemplate.Append("<td>" + this.resources.DropoffDatetime.ToString("dd/MM/yyyy HH:mm:ss") + "</td>");
        emailTemplate.Append("</tr>");
        emailTemplate.Append("<tr>");
        emailTemplate.Append("<td colspan=\"2\" style=\"height: 30px;\"></td>");
        emailTemplate.Append("</tr>");

        return emailTemplate.ToString();
    }

    protected string LocationDetailsForEmail()
    {
        var emailTemplate = new StringBuilder();

        //if (_booking.CarProviderSelected == CarProvider.FireFlyAutoEurope)
        //{
        //    //pickup location details
        //    emailTemplate.Append("<tr>");
        //    emailTemplate.Append("<td colspan=\"2\" style=\"background-color: #DEE0E0; padding: 0px 0px 0px 5px;\"  class=\"loud\"><b>" + GetLocalResourceObject("PickupLocDtlsLit") + "</b></td>");
        //    emailTemplate.Append("</tr>");
        //    emailTemplate.Append("<tr>");
        //    emailTemplate.Append("<td width=\"299\"><b>" + HttpContext.GetGlobalResourceObject("SiteStrings", "BookingEmailCarHireOperator") + "</b></td>");
        //    emailTemplate.Append("<td width=\"299\"><b>" + HttpContext.GetGlobalResourceObject("SiteStrings", "BookingEmailPickupVehicleAt") + "</b></td>");
        //    emailTemplate.Append("</tr>");
        //    emailTemplate.Append("<tr>");
        //    emailTemplate.Append("<td><strong>" + _booking.AutoEurope.PickupProvider + "</strong></td>");
        //    emailTemplate.Append("<td rowspan=\"2\">" + _booking.AutoEurope.PickupAddress + "</td>");
        //    emailTemplate.Append("</tr>");
        //    emailTemplate.Append("<tr>");
        //    emailTemplate.Append("<td>" + _booking.AutoEurope.PickupTel + "</td>");
        //    emailTemplate.Append("</tr>");
        //    emailTemplate.Append("<tr>");
        //    emailTemplate.Append("<td colspan=\"2\" style=\"height: 30px;\"></td>");
        //    emailTemplate.Append("</tr>");
        //}

        return emailTemplate.ToString();
    }

    protected string FlightNumberForEmail()
    {
        var emailTemplate = new StringBuilder();

        //if (!String.IsNullOrEmpty(_booking.FlightNum))
        //{
        //    emailTemplate.Append("<tr>");
        //    emailTemplate.Append("<td colspan=\"2\" style=\"background-color: #DEE0E0; padding: 0px 0px 0px 5px;\"  class=\"loud\">" + HttpContext.GetGlobalResourceObject("SiteStrings", "BookingEmailFlightInfomation") + "</td>");
        //    emailTemplate.Append("</tr>");
        //    emailTemplate.Append("<tr>");
        //    emailTemplate.Append("<td colspan=\"2\"><b>" + HttpContext.GetGlobalResourceObject("SiteStrings", "BookingEmailFlightNumber") + " " + _booking.FlightNum + "</b></td>");
        //    emailTemplate.Append("</tr>");
        //    emailTemplate.Append("<tr>");
        //    emailTemplate.Append("<td colspan=\"2\" style=\"height: 30px;\"></td>");
        //    emailTemplate.Append("</tr>");
        //}

        return emailTemplate.ToString();
    }

    protected string CarDetailsForEmail(string carImage, string localizedCar, string carCurrencySymbol, string carCost)
    {
        var emailTemplate = new StringBuilder();

        emailTemplate.Append("<tr>");
        emailTemplate.Append("<td colspan=\"2\" style=\"background-color: #DEE0E0; padding: 5px 0 5px 5px;\" class=\"loud\"><b>" + HttpContext.GetGlobalResourceObject("SiteStrings", "BookingEmailCarDetails") + "</b></td>");
        emailTemplate.Append("</tr>");
        emailTemplate.Append("<tr>");
        emailTemplate.Append("<td colspan=\"2\">");
        emailTemplate.Append("<table cellpadding=\"0\" cellspacing=\"0\" border=\"0\">");
        emailTemplate.Append("<tr>");
        emailTemplate.Append("<td width=\"200\" valign=\"top\"><img src=\"" + carImage + "\" /> </td>");
        emailTemplate.Append("<td valign=\"middle\" bgcolor=\"#E6E6E6\" style=\"font-weight: bold; padding: 5px; text-align: center;\" > <div style=\"background-color: #3776B9; color: #fff; padding: 10px; font-size: 16px;\">" + localizedCar + "</div><br><br><span style=\"font-size: 24px;\">" + carCurrencySymbol + carCost + "</span><br>");
        emailTemplate.Append("</td>");
        emailTemplate.Append("</tr>");
        emailTemplate.Append("</table>");
        emailTemplate.Append("</td></tr><tr>");
        emailTemplate.Append("<td colspan=\"2\" style=\"height: 10px;\"></td>");
        emailTemplate.Append("</tr>");

        return emailTemplate.ToString();
    }

    //protected string AccessoriesAndDropFeeForEmail()
    //{
    //    var emailTemplate = new StringBuilder();

    //    // only show if NOT auto europe booking
    //    var accessoriesSelected = _booking.Accessories.Where(a => a.IsSelected && !a.Delete).ToList();
    //    if ((accessoriesSelected.Any()) || (_booking.CrossBorderDropFee > 0m))
    //    {
    //        emailTemplate.Append("<tr>");
    //        emailTemplate.Append("<td colspan=\"2\" style=\"background-color: #DEE0E0; padding: 5px 0 5px 5px;\" class=\"loud\"><b>" + HttpContext.GetGlobalResourceObject("SiteStrings", "BookingEmailExtrasDetails") + "</b></td>");
    //        emailTemplate.Append("</tr>");
    //        emailTemplate.Append("<tr>");
    //        emailTemplate.Append("<td colspan=\"2\">");
    //        emailTemplate.Append("<table cellpadding=\"0\" cellspacing=\"0\" border=\"0\">");
    //        if (accessoriesSelected.Any())
    //        {
    //            foreach (var accessory in accessoriesSelected)
    //            {
    //                string quantity = (accessory.Quantity > 1) ? accessory.Quantity + " x " : "";

    //                // if winter tyres are selected and it's within the timeframe allowed for Germany then say price is included
    //                if ((accessory.EquipType == "14") &&
    //                    (WinterTyres.Hertz.IsValid(_booking.PickUpCountryCode, Convert.ToDateTime(_booking.PickupDateTime))))
    //                {
    //                    emailTemplate.Append("<tr>");
    //                    emailTemplate.Append("<td width=\"200\" valign=\"top\"><img src=\"http://" + CMSSettings.SiteUrl + "/Pics/Accessories/Original/" + accessory.Image + "\" /> </td>");
    //                    emailTemplate.Append("<td valign=\"middle\"><b>" + quantity + accessory.Title + "</b><br />" + HttpContext.GetGlobalResourceObject("SiteStrings", "XML_InclSnowTyresIncludedInPrice") + "</td>");
    //                    emailTemplate.Append("</tr>");
    //                }
    //                else
    //                {
    //                    emailTemplate.Append("<tr>");
    //                    emailTemplate.Append("<td width=\"200\" valign=\"top\"><img src=\"http://" + CMSSettings.SiteUrl + "/Pics/Accessories/Original/" + accessory.Image + "\" /> </td>");
    //                    emailTemplate.Append("<td valign=\"middle\"><b>" + quantity + accessory.Title + "</b><br />" + accessory.CurrencySymbol + String.Format("{0:0.00}", accessory.Price) + "</td>");
    //                    emailTemplate.Append("</tr>");
    //                }

    //            }
    //        }

    //        if (_booking.CrossBorderDropFee > 0m)
    //        {
    //            emailTemplate.Append("<tr>");
    //            emailTemplate.Append("<td width=\"200\" valign=\"top\"></td>");
    //            emailTemplate.Append("<td valign=\"middle\"><b>" + HttpContext.GetGlobalResourceObject("SiteStrings", "CrossBorderFeelblResource1.Text") + " " + _booking.CrossBorderDropFeeSymbol + _booking.CrossBorderDropFee + "</b><br />");
    //            emailTemplate.Append("<td><b>" + _booking.PayAtCounterCurrencySymbol + _booking.PayAtCounterFee + "</b><br />");
    //            emailTemplate.Append("</td></tr>");
    //        }

    //        if (_booking.PayAtCounterFee > 0m)
    //        {
    //            emailTemplate.Append("<tr>");
    //            emailTemplate.Append("<td width=\"200\" valign=\"top\"></td>");
    //            //emailTemplate.Append("<td><b>" + _booking.PayAtCounterCurrencySymbol + _booking.PayAtCounterFee + "</b><br />");
    //            emailTemplate.Append("<td><b>" + HttpContext.GetGlobalResourceObject("SiteStrings", "BookingEmailPayAtCounterFee") + " " + _booking.PayAtCounterCurrencySymbol + _booking.PayAtCounterFee + "</b><br />");
    //            emailTemplate.Append("</td></tr>");
    //        }
    //        emailTemplate.Append("</table>");
    //        emailTemplate.Append("</td></tr>");
    //    }

    //    return emailTemplate.ToString();
    //}

    protected string MandatoryFuelPurchaseForEmail(int numDaysRental)
    {
        var emailTemplate = new StringBuilder();


        emailTemplate.Append("<tr>");
        emailTemplate.Append("<td colspan=\"2\">");

        //if (FuelPurchase.IsMfpValid(numDaysRental, this.resources.PickUpCountryCode, this.resources.PickupLocationCode, this.resources.BrandName, this.resources.PickupDateTime))
        //{
        //    string mfpText = HttpContext.GetGlobalResourceObject("SiteStrings", "BookingEmailFuelPurchasePrePay1") + _booking.PickUpLocationCode + "\">" + HttpContext.GetGlobalResourceObject("SiteStrings", "FuelPurchasePrePay2") + "</a>";
        //    string mfpTextAlternate = HttpContext.GetGlobalResourceObject("SiteStrings", "BookingEmailFuelPurchasePrePay1Alternate") + _booking.PickUpLocationCode + "\">" + HttpContext.GetGlobalResourceObject("SiteStrings", "FuelPurchasePrePay2Alternate") + "</a>";
        //    if (!_booking.PrePaySelected)
        //    {
        //        mfpText = HttpContext.GetGlobalResourceObject("SiteStrings", "FuelPurchase1") + _booking.PickUpLocationCode + "\">" + HttpContext.GetGlobalResourceObject("SiteStrings", "FuelPurchase2") + "</a>";
        //    }
        //    if (_booking.PickUpCountryCode == "ES" || _booking.PickUpCountryCode == "IT" || _booking.PickUpCountryCode == "FR" || _booking.PickUpCountryCode == "PT")
        //    {
        //        mfpText = mfpText.Replace("<ph1>", "KEYWORD=" + MFPKeyword(_booking.CarProviderSelected, false) + "&EOAG=");
        //        emailTemplate.Append(mfpText);
        //    }
        //    else
        //    {
        //        mfpTextAlternate = mfpTextAlternate.Replace("<ph1>", "KEYWORD=" + MFPKeyword(_booking.CarProviderSelected, true) + "&EOAG=");
        //        emailTemplate.Append(mfpTextAlternate);
        //    }
        //    // custome rule for Germany Firefly
        //    if (_booking.PickUpCountryCode == "DE" && _booking.CarProviderSelected == CarProvider.FireFly)
        //    {
        //        mfpText = mfpText.Replace("<ph1>", "KEYWORD=" + MFPKeyword(_booking.CarProviderSelected, false) + "&EOAG=");
        //        emailTemplate.Append(mfpText);
        //    }
        //}
        //else
        //{
        //    emailTemplate.Append(HttpContext.GetGlobalResourceObject("SiteStrings", "FuelPurchaseNone"));
        //}
        emailTemplate.Append("</td></tr><tr>");
        emailTemplate.Append("<td colspan=\"2\" style=\"height: 30px;\"></td>");
        emailTemplate.Append("</tr>");

        return emailTemplate.ToString();
    }

    //protected string MFPKeyword(CarProvider carProvider, bool alternate)
    //{
    //    string keyword = "CFPO";
    //    switch (carProvider)
    //    {
    //        case CarProvider.Thrifty:
    //            keyword = "CFPO3";
    //            break;
    //        case CarProvider.FireFly:
    //            keyword = "CFPO2";
    //            break;
    //    }
    //    if (alternate)
    //        keyword = "FUEL";

    //    return keyword;
    //}

    protected string LocationAdvisoryForEmail()
    {
        var emailTemplate = new StringBuilder();

        //work out if the location advisory details should display
        var getAdvisoryWording = new LocationAdvisory();
        if (LocationAdvisory.IsAdvisoryValid(this.resources.PickupLocationCode, this.resources.BrandName))
        {
            emailTemplate.Append("<tr>");
            emailTemplate.Append("<td colspan=\"2\">");

            emailTemplate.Append("<strong>" + HttpContext.GetGlobalResourceObject("SiteStrings", "LocalInstuctionsHeader") + "</strong><br />" + getAdvisoryWording.AdvisoryText(this.resources.PickupLocationCode));

            emailTemplate.Append("</td></tr><tr>");
            emailTemplate.Append("<td colspan=\"2\" style=\"height: 30px;\"></td>");
            emailTemplate.Append("</tr>");
        }

        return emailTemplate.ToString();
    }

    protected string CleaningFeeForEmail()
    {
        var emailTemplate = new StringBuilder();

        if (this.resources.PickUpCountryCode == "FR")
        {
            emailTemplate.Append("<tr>");
            emailTemplate.Append("<td colspan=\"2\">");

            emailTemplate.Append(HttpContext.GetGlobalResourceObject("SiteStrings", "CleaningFeeDetailsForEmail"));

            emailTemplate.Append("</td></tr><tr>");
            emailTemplate.Append("<td colspan=\"2\" style=\"height: 30px;\"></td>");
            emailTemplate.Append("</tr>");
        }

        return emailTemplate.ToString();
    }

    //protected string RyanairTermsForEmail()
    //{
    //    var emailTemplate = new StringBuilder();

    //    emailTemplate.Append("<tr>");
    //    emailTemplate.Append("<td colspan=\"2\">" + GetPriceTextAndTerms() + "</td>");
    //    emailTemplate.Append("</tr>");
    //    emailTemplate.Append("<tr>");
    //    emailTemplate.Append("<td colspan=\"2\" style=\"height: 5px;\"></td>");
    //    emailTemplate.Append("</tr>");

    //    return emailTemplate.ToString();
    //}

    //protected string GetPriceTextAndTerms()
    //{
    //    string RyTermslbl = "";
    //    if (_booking.CarProviderSelected == CarProvider.Hertz)
    //    {
    //        RyTermslbl = (string)HttpContext.GetGlobalResourceObject("SiteStrings", "RyanairImportantInfo");
    //        RyTermslbl += (string)HttpContext.GetGlobalResourceObject("SiteStrings", "RyTermslbl");
    //        RyTermslbl = RyTermslbl.Replace("Hertz", _booking.CarProviderSelected.ToString());
    //    }
    //    else
    //    {
    //        RyTermslbl = (string)HttpContext.GetGlobalResourceObject("SiteStrings", "RyTermslbl");
    //        RyTermslbl = RyTermslbl.Replace("Hertz", _booking.CarProviderSelected.ToString());
    //    }

    //    return RyTermslbl;
    //}
    protected string EBillingContent()
    {
        var emailTemplate = new StringBuilder();

        // only show if NOT auto europe booking AND booking is NPP
        //if ((!_booking.PrePaySelected))
        //{
            if ((this.resources.PickUpCountryCode == "GB") || (this.resources.DropOffCountryCode == "GB") || (this.resources.PickUpCountryCode == "BE") || (this.resources.DropOffCountryCode == "BE") || (this.resources.PickUpCountryCode == "LU") || (this.resources.DropOffCountryCode == "LU") || (this.resources.PickUpCountryCode == "NL") || (this.resources.DropOffCountryCode == "NL"))
            {
                emailTemplate.Append("<tr>");
                emailTemplate.Append("<td colspan=\"2\" style=\"height: 5px;\"></td>");
                emailTemplate.Append("</tr>");
                emailTemplate.Append("<tr>");
                emailTemplate.Append("<td colspan=\"2\">");
                emailTemplate.Append("<h4>");
                emailTemplate.Append(HttpContext.GetGlobalResourceObject("SiteStrings", "eBillingHeaderlblResource1"));
                emailTemplate.Append("</h4>");
                emailTemplate.Append("</td>");
                emailTemplate.Append("</tr>");
                emailTemplate.Append("<tr>");
                emailTemplate.Append("<td colspan=\"2\">");
                emailTemplate.Append(HttpContext.GetGlobalResourceObject("SiteStrings", "eBillingTextlblResource1"));
                emailTemplate.Append("</td>");
                emailTemplate.Append("</tr>");
            }
        //}

        return emailTemplate.ToString();
    }

    protected string PartnerFooter()
    {
        var emailTemplate = new StringBuilder();

        //// only show if auto europe booking
        //if (_booking.CarProviderSelected == CarProvider.FireFlyAutoEurope)
        //{
        //    emailTemplate.Append("<tr>");
        //    emailTemplate.Append("<td colspan=\"2\" style=\"height: 5px;\"></td>");
        //    emailTemplate.Append("</tr>");
        //    emailTemplate.Append("<tr>");
        //    emailTemplate.Append("<td align=\"left\"><span class=\"partners pBottom\">" + (string)HttpContext.GetGlobalResourceObject("SiteStrings", "BookingEmailPartnerOperated") + "</span></td>");
        //    emailTemplate.Append("<td align=\"right\" valign=\"bottom\"><a href=\"http://" + WebsiteSettings.SiteUrl + "?link=BrokerOrderConfirmation&lang=" + _booking.Language + "\"><img src=\"http://" + WebsiteSettings.SiteUrl + "/groupemail/images/advantage-logo.gif\" border=\"0\" width=\"200\"></a></td>");
        //    emailTemplate.Append("</tr>");
        //}

        return emailTemplate.ToString();
    }
}