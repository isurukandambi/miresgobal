﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for WinterTyres
/// </summary>
public class WinterTyres
{
    public class Hertz
    {
        public static bool IsValid(string pickUpCountryCode, DateTime pickUpDate)
        {
            return IsWinterTyreValid(pickUpCountryCode, pickUpDate);
        }
    }

    private static bool IsWinterTyreValid(string pickUpCountryCode, DateTime pickUpDate)
    {
        /*
         * Current Rule:
         * Display Winter Tyres if Pickup Country = Germany
         * AND 
         * Pickup date is within the range:
         * 1 Jan - 31 Mar AND 1 Nov - 31 Dec (both this yr)
         * AND 1 Jan - 31 Mar (next yr)
         */

        bool display = (pickUpCountryCode == "DE") &&
                       (((pickUpDate >= new DateTime(DateTime.Now.Year, 1, 1)) && (pickUpDate <= new DateTime(DateTime.Now.Year, 3, 31))) ||
                        ((pickUpDate >= new DateTime(DateTime.Now.Year, 11, 1)) && (pickUpDate <= new DateTime(DateTime.Now.Year, 12, 31))) ||
                        ((pickUpDate >= new DateTime(DateTime.Now.Year + 1, 1, 1)) && (pickUpDate <= new DateTime(DateTime.Now.Year + 1, 3, 31)))
                       );

        return display;
    }
}