﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Xml;

/// <summary>
/// Summary description for XmlUtils
/// </summary>
public class XmlUtils
{
    public static string getXmlString(XmlDocument xmlDocument)
    {
        StringBuilder sb = new StringBuilder();
        XmlWriterSettings settings = new XmlWriterSettings();
        settings.Indent = true;
        settings.IndentChars = "  ";
        settings.NewLineChars = "\r\n";
        settings.NewLineHandling = NewLineHandling.Replace;
        using (XmlWriter writer = XmlWriter.Create(sb, settings))
        {
            xmlDocument.Save(writer);
        }
        return sb.ToString();
    }
}