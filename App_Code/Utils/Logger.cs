﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Logger
/// </summary>
public static class Logger
{
    private static log4net.ILog Log { get; set; }

    static Logger()
    {
        Log = log4net.LogManager.GetLogger(typeof(Logger));
    }

    public static void Error(object msg, Type classType)
    {
        Log.Error(formatMessage(msg, 9));
    }

    public static void Error(object msg, Exception ex)
    {
        Log.Error(formatMessage(msg, 9), ex);
    }

    public static void Error(Exception ex)
    {
        Log.Error(formatMessage(ex.Message, 9), ex);
    }

    public static void Info(object msg)
    {
        Log.Info(formatMessage(msg, 2));
    }

    public static void Debug(object msg)
    {
        Log.Debug(formatMessage(msg, 1));
    }

    public static void Warn(object msg)
    {
        Log.Warn(formatMessage(msg,3));
    }

    private static object formatMessage(object msg, int type){
        //debug
        if (type == 1)
        {
            msg = "<textarea rows=\"20\" cols=\"40\" style=\"border:none; width: 100%;\" readonly=\"readonly\">" + msg + "</textarea>";
        }
        //Info
        else if (type == 2)
        {
            msg = "<textarea rows=\"20\" cols=\"40\" style=\"border:none; width: 100%;\" readonly=\"readonly\">" + msg + "</textarea>";
        }
        //warning
        else if (type == 3)
        {
            msg = "<textarea rows=\"20\" cols=\"40\" style=\"border:none; width: 100%;\" readonly=\"readonly\">" + msg + "</textarea>";
        }
        //Error
        else if (type == 9)
        {
            msg = "<textarea rows=\"20\" cols=\"40\" style=\"border:none; width: 100%;\" readonly=\"readonly\">" + msg + "</textarea>";
        }

        return msg;
    }
}