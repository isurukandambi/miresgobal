﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TestClient;

/// <summary>
/// Summary description for LocationAdvisory
/// </summary>
public class LocationAdvisory
{
    public string AdvisoryText(string locationcode)
    {
        string AdvisoryWording;
        switch (locationcode)
        {
            case "BRI":
                AdvisoryWording = (string)HttpContext.GetGlobalResourceObject("SiteStrings", "LocationInstructions_Bari");
                break;
            case "CAG":
                AdvisoryWording = (string)HttpContext.GetGlobalResourceObject("SiteStrings", "LocationInstructions_Cagliari");
                break;
            //case "CTAT51":
            case "CTA":
                AdvisoryWording = (string)HttpContext.GetGlobalResourceObject("SiteStrings", "LocationInstructions_Catania");
                break;
            case "PMO":
                AdvisoryWording = (string)HttpContext.GetGlobalResourceObject("SiteStrings", "LocationInstructions_Palermo");
                break;
            //case "CIAT51":
            case "CIA":
                AdvisoryWording = (string)HttpContext.GetGlobalResourceObject("SiteStrings", "LocationInstructions_Rome");
                break;
            case "TPS":
                AdvisoryWording = (string)HttpContext.GetGlobalResourceObject("SiteStrings", "LocationInstructions_Trapani");
                break;
            case "MXP":
                AdvisoryWording = (string)HttpContext.GetGlobalResourceObject("SiteStrings", "LocationInstructions_Malpensa");
                break;
            case "BDS":
                AdvisoryWording = (string)HttpContext.GetGlobalResourceObject("SiteStrings", "LocationInstructions_Bari");
                break;
            case "AHO":
                AdvisoryWording = (string)HttpContext.GetGlobalResourceObject("SiteStrings", "LocationInstructions_Alghero");
                break;
            case "FCO":
                AdvisoryWording = (string)HttpContext.GetGlobalResourceObject("SiteStrings", "LocationInstructions_RomeFCO");
                break;
            default:
                AdvisoryWording = "";
                break;
        }
        return AdvisoryWording;
    }

    public string BookingSummaryAdvisoryText(string locationcode)
    {
        string AdvisoryWording;
        switch (locationcode)
        {
            case "BRIT50":
                AdvisoryWording = (string)HttpContext.GetGlobalResourceObject("SiteStrings", "BSLocationInstructions_Bari");
                break;
            case "CAGT50":
                AdvisoryWording = (string)HttpContext.GetGlobalResourceObject("SiteStrings", "BSLocationInstructions_Cagliari");
                break;
            //case "CTAT51":
            case "CTA":
                AdvisoryWording = (string)HttpContext.GetGlobalResourceObject("SiteStrings", "BSLocationInstructions_Catania");
                break;
            case "PMOT51":
                AdvisoryWording = (string)HttpContext.GetGlobalResourceObject("SiteStrings", "BSLocationInstructions_Palermo");
                break;
            //case "CIAT51":
            case "CIA":
                AdvisoryWording = (string)HttpContext.GetGlobalResourceObject("SiteStrings", "BSLocationInstructions_Rome");
                break;
            case "TPST51":
                AdvisoryWording = (string)HttpContext.GetGlobalResourceObject("SiteStrings", "BSLocationInstructions_Trapani");
                break;
            case "MXP":
                AdvisoryWording = (string)HttpContext.GetGlobalResourceObject("SiteStrings", "BSLocationInstructions_Malpensa");
                break;
            default:
                AdvisoryWording = "";
                break;
        }
        return AdvisoryWording;
    }

    public static bool IsAdvisoryValid(string pickUpLocationCode, string carProvider)
    {
        /*
         * Current Rule:
         * Display Advisory Text if - 
         * Pick up location is in the list and carProvider is firefly
         * 
         */

        // bool display = ((Locations().Contains(pickUpLocationCode)) && (carProvider.ToString() == "FireFlyHertz"));
        bool display = ((Locations().Contains(pickUpLocationCode)) && (carProvider.ToString() == "FireFly"));
        return display;
    }

    private static List<string> Locations()
    {
        var locationList = new List<string>
        {
            "BRI",
            "CAG",
            "BDS",
            "CTA",
            "PMO",
            //"CIAT51",
            "TPS",
            "MXP",
            "AHO",
            "CIA",
            "FCO",
        };

        return locationList;
    }
}