﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for TimeBetweenDates
/// </summary>
public static class TimeBetweenDates
{
    public static int CalculateDays(DateTime start, DateTime end)
    {
        TimeSpan ts = end - start;
        return (int)Math.Floor(ts.TotalDays);
    }

    public static int CalculateMinutes(DateTime start, DateTime end)
    {
        TimeSpan ts = end - start;
        return (int)Math.Floor(ts.TotalMinutes);
    }

    public static int CalculateHours(DateTime start, DateTime end)
    {
        TimeSpan ts = end - start;
        return (int)Math.Floor(ts.TotalHours);
    }
}