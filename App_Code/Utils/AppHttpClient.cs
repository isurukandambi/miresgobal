﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;

/// <summary>
/// Summary description for AppHttpClient
/// </summary>
public class AppHttpClient
{
	public AppHttpClient()
	{

	}

    public string createPostRequest(string url, string postData)
    {
        WebRequest request = WebRequest.Create(url);
        request.Method = "POST";
        request.ContentType = "text/xml";
        request.Timeout = 25000;
        byte[] byteArray = Encoding.UTF8.GetBytes(postData);
        request.ContentLength = byteArray.Length;
        Stream dataStream = request.GetRequestStream();
        dataStream.Write(byteArray, 0, byteArray.Length);
        dataStream.Close();
        WebResponse response = request.GetResponse();

        dataStream = response.GetResponseStream();
        StreamReader reader = new StreamReader(dataStream);
        string responseFromServer = reader.ReadToEnd();
        reader.Close();
        dataStream.Close();
        response.Close();

        return responseFromServer;
    }
}