﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for CommonClass
/// </summary>
public partial class CommonClass
{
    public static string GetErrorXML(string xmlCallType, string errorType, string errorCode, string error)
    {
        return "<?xml version=\"1.0\" encoding=\"UTF-8\"?><OTA_" + xmlCallType + " xmlns=\"http://www.opentravel.org/OTA/2003/05\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.opentravel.org/OTA/2003/05\"><Errors><Error Type=\"" + errorType + "\" ShortText=\"" + error + " \" RecordID=\"" + errorCode + "\" Code=\"120\"/></Errors></OTA_" + xmlCallType + ">";
    }
}