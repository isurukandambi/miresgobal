﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TestClient;

/// <summary>
/// Summary description for SelectTermsText
/// </summary>
public class SelectTermsText
{
    public static string LongTermsToUse(string sourceCountry, CarProvider carProvider)
    {
        // default terms to use
        string termsToShow = (string)HttpContext.GetGlobalResourceObject("SiteStrings", "PrePaidTerms_Long_EUR");

        if (!String.IsNullOrEmpty(sourceCountry))
        {
            if (carProvider == CarProvider.Hertz)
            {
                switch (sourceCountry.ToLower())
                {
                    case "ie":
                        termsToShow = (string)HttpContext.GetGlobalResourceObject("SiteStrings", "PrePaidTerms_Long_EUR");
                        break;
                    case "gb":
                        termsToShow = (string)HttpContext.GetGlobalResourceObject("SiteStrings", "PrePaidTerms_Long_GBP");
                        break;
                    case "ch":
                        termsToShow = (string)HttpContext.GetGlobalResourceObject("SiteStrings", "PrePaidTerms_Long_CHF");
                        break;
                    case "dk":
                        termsToShow = (string)HttpContext.GetGlobalResourceObject("SiteStrings", "PrePaidTerms_Long_DKK");
                        break;
                    case "se":
                        termsToShow = (string)HttpContext.GetGlobalResourceObject("SiteStrings", "PrePaidTerms_Long_SEK");
                        break;
                    case "no":
                        termsToShow = (string)HttpContext.GetGlobalResourceObject("SiteStrings", "PrePaidTerms_Long_NOK");
                        break;
                    default:
                        goto case "ie";
                }
            }
            else if (carProvider == CarProvider.Thrifty)
            {
                switch (sourceCountry.ToLower())
                {
                    case "ie":
                        termsToShow = (string)HttpContext.GetGlobalResourceObject("SiteStrings", "ThriftyTerms_EUR");
                        break;
                    case "gb":
                        termsToShow = (string)HttpContext.GetGlobalResourceObject("SiteStrings", "ThriftyTerms_GBP");
                        break;
                    case "ch":
                        termsToShow = (string)HttpContext.GetGlobalResourceObject("SiteStrings", "ThriftyTerms_CHF");
                        break;
                    case "dk":
                        termsToShow = (string)HttpContext.GetGlobalResourceObject("SiteStrings", "ThriftyTerms_DKK");
                        break;
                    case "se":
                        termsToShow = (string)HttpContext.GetGlobalResourceObject("SiteStrings", "ThriftyTerms_SEK");
                        break;
                    case "no":
                        termsToShow = (string)HttpContext.GetGlobalResourceObject("SiteStrings", "ThriftyTerms_NOK");
                        break;
                    default:
                        goto case "ie";
                }
            }
            else
            {
                switch (sourceCountry.ToLower())
                {
                    case "ie":
                        termsToShow = (string)HttpContext.GetGlobalResourceObject("SiteStrings", "FireflyTerms_EUR");
                        break;
                    case "gb":
                        termsToShow = (string)HttpContext.GetGlobalResourceObject("SiteStrings", "FireflyTerms_GBP");
                        break;
                    case "ch":
                        termsToShow = (string)HttpContext.GetGlobalResourceObject("SiteStrings", "FireflyTerms_CHF");
                        break;
                    case "dk":
                        termsToShow = (string)HttpContext.GetGlobalResourceObject("SiteStrings", "FireflyTerms_DKK");
                        break;
                    case "se":
                        termsToShow = (string)HttpContext.GetGlobalResourceObject("SiteStrings", "FireflyTerms_SEK");
                        break;
                    case "no":
                        termsToShow = (string)HttpContext.GetGlobalResourceObject("SiteStrings", "FireflyTerms_NOK");
                        break;
                    default:
                        goto case "ie";
                }
            }
        }

        return termsToShow;
    }

    public static string ShortTermsToUse(string sourceCountry, CarProvider carProvider)
    {
        // default terms to use
        string termsToShow = (string)HttpContext.GetGlobalResourceObject("SiteStrings", "PrePaidTerms_Short_EUR");

        if (!String.IsNullOrEmpty(sourceCountry))
        {
            if (carProvider == CarProvider.Hertz)
            {
                switch (sourceCountry.ToLower())
                {
                    case "ie":
                        termsToShow = (string)HttpContext.GetGlobalResourceObject("SiteStrings", "PrePaidTerms_Short_EUR");
                        break;
                    case "gb":
                        termsToShow = (string)HttpContext.GetGlobalResourceObject("SiteStrings", "PrePaidTerms_Short_GBP");
                        break;
                    case "ch":
                        termsToShow = (string)HttpContext.GetGlobalResourceObject("SiteStrings", "PrePaidTerms_Short_CHF");
                        break;
                    case "dk":
                        termsToShow = (string)HttpContext.GetGlobalResourceObject("SiteStrings", "PrePaidTerms_Short_DKK");
                        break;
                    case "se":
                        termsToShow = (string)HttpContext.GetGlobalResourceObject("SiteStrings", "PrePaidTerms_Short_SEK");
                        break;
                    case "no":
                        termsToShow = (string)HttpContext.GetGlobalResourceObject("SiteStrings", "PrePaidTerms_Short_NOK");
                        break;
                    default:
                        goto case "ie";
                }
            }
            else if (carProvider == CarProvider.Thrifty)
            {
                switch (sourceCountry.ToLower())
                {
                    case "ie":
                        termsToShow = (string)HttpContext.GetGlobalResourceObject("SiteStrings", "ThriftyTerms_EUR");
                        break;
                    case "gb":
                        termsToShow = (string)HttpContext.GetGlobalResourceObject("SiteStrings", "ThriftyTerms_GBP");
                        break;
                    case "ch":
                        termsToShow = (string)HttpContext.GetGlobalResourceObject("SiteStrings", "ThriftyTerms_CHF");
                        break;
                    case "dk":
                        termsToShow = (string)HttpContext.GetGlobalResourceObject("SiteStrings", "ThriftyTerms_DKK");
                        break;
                    case "se":
                        termsToShow = (string)HttpContext.GetGlobalResourceObject("SiteStrings", "ThriftyTerms_SEK");
                        break;
                    case "no":
                        termsToShow = (string)HttpContext.GetGlobalResourceObject("SiteStrings", "ThriftyTerms_NOK");
                        break;
                    default:
                        goto case "ie";
                }
            }
            else
            {
                switch (sourceCountry.ToLower())
                {
                    case "ie":
                        termsToShow = (string)HttpContext.GetGlobalResourceObject("SiteStrings", "FireflyTerms_EUR");
                        break;
                    case "gb":
                        termsToShow = (string)HttpContext.GetGlobalResourceObject("SiteStrings", "FireflyTerms_GBP");
                        break;
                    case "ch":
                        termsToShow = (string)HttpContext.GetGlobalResourceObject("SiteStrings", "FireflyTerms_CHF");
                        break;
                    case "dk":
                        termsToShow = (string)HttpContext.GetGlobalResourceObject("SiteStrings", "FireflyTerms_DKK");
                        break;
                    case "se":
                        termsToShow = (string)HttpContext.GetGlobalResourceObject("SiteStrings", "FireflyTerms_SEK");
                        break;
                    case "no":
                        termsToShow = (string)HttpContext.GetGlobalResourceObject("SiteStrings", "FireflyTerms_NOK");
                        break;
                    default:
                        goto case "ie";
                }
            }

        }

        return termsToShow;
    }

    public static string NonPrePaidTerms()
    {
        return (string)HttpContext.GetGlobalResourceObject("SiteStrings", "NonPrePaidTermsResource");
    }
}