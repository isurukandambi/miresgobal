﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TestClient;

/// <summary>
/// Summary description for FuelPurchase
/// </summary>
public class FuelPurchase
{
    private static int HertzMinuteThreshold()
    {
        // 7229 = over 5 days
        return 7229;
    }

    private static int ThriftyMinuteThreshold()
    {
        // 7229 = over 5 days
        return 7229;
    }

    private static int FireflyMinuteThreshold(string pickUpCountryCode)
    {
        // 5789 = over 4 days
        // 4320 = over 3 days
        return pickUpCountryCode == "ES" ? 4320 : 5789;
    }

    public static bool IsMfpValid(int numMinutesWithCar, string pickUpCountryCode, string pickUpLocationCode, CarProvider carProvider, DateTime pickUpDate)
    {
        /*
         * Current Rule:
         * Display Fuel Purchase if - 
         * Minutes with car is over 5 days (Hertz and Thrifty)
         * AND 
         * Pickup country matches list OR Firefly
         * 
         */
        bool display = false;
        var locationslist = Locations();

        if (carProvider == CarProvider.FireFly)
            return true;

        int maxMins = HertzMinuteThreshold();
        if (carProvider == CarProvider.Thrifty)
            maxMins = ThriftyMinuteThreshold();

        var pickupCountryInCountryList = Countries(carProvider).Contains(pickUpCountryCode);

        if (TimedCountries(carProvider).Contains(pickUpCountryCode))
        {
            if (carProvider == CarProvider.Hertz)
                display = (((numMinutesWithCar > maxMins) && (pickupCountryInCountryList) && !Locations().Contains(pickUpLocationCode)));

            if (carProvider == CarProvider.Thrifty)
                display = (((numMinutesWithCar > maxMins) && (pickupCountryInCountryList)));
        }
        else
        {
            if (carProvider == CarProvider.Hertz)
                display = (((pickupCountryInCountryList) && !Locations().Contains(pickUpLocationCode)));

            if (carProvider == CarProvider.Thrifty)
                display = (((pickupCountryInCountryList)));
        }
        DateTime checkdate = DateTime.Now;
        try
        {
            checkdate = Convert.ToDateTime("8/18/2014 00:00:01 AM");
        }
        catch
        {
            checkdate = Convert.ToDateTime("18/8/2014 00:00:01 AM");
        }
        if (pickUpDate >= checkdate)
        {
            if (pickUpCountryCode.ToLower() == "fr")
            {
                if (carProvider == CarProvider.Hertz || carProvider == CarProvider.Thrifty)
                {
                    display = false;
                }
            }

            if (pickUpCountryCode.ToLower() == "es")
            {
                if (carProvider == CarProvider.Hertz)
                {
                    display = false;
                }
            }
        }

        return display;
    }

    // allowed countries are different depending on provider
    private static List<string> Countries(CarProvider carProvider)
    {
        var countryList = new List<string>();

        if (carProvider == CarProvider.Hertz)
        {
            countryList.Add("ES");
            countryList.Add("FR");
            countryList.Add("IT");
            countryList.Add("PT");
            countryList.Add("DE");
            countryList.Add("GR");
            countryList.Add("CY");
            countryList.Add("ME");
            countryList.Add("RO");
            countryList.Add("RS");
            countryList.Add("BG");
            countryList.Add("BE");
        }

        if (carProvider == CarProvider.Thrifty)
        {
            countryList.Add("ES");
            countryList.Add("FR");
            countryList.Add("IT");
            countryList.Add("PT");
            countryList.Add("GR");
            countryList.Add("CY");
            countryList.Add("ME");
            countryList.Add("RO");
            countryList.Add("RS");
            countryList.Add("BG");
            countryList.Add("BE");
        }

        return countryList;
    }

    // allowed countries are different depending on provider
    private static List<string> TimedCountries(CarProvider carProvider)
    {
        var countryList = new List<string>();

        if (carProvider == CarProvider.Hertz)
        {
            countryList.Add("ES");
            countryList.Add("FR");
            countryList.Add("IT");
            countryList.Add("PT");
            countryList.Add("BE");
        }

        if (carProvider == CarProvider.Thrifty)
        {
            countryList.Add("ES");
            countryList.Add("FR");
            countryList.Add("IT");
            countryList.Add("PT");
            countryList.Add("BE");
        }

        return countryList;
    }

    private static List<string> Locations()
    {
        var locationList = new List<string>
        {
            "IBZT50",
            "MAHT51",
            "ACET50",
            "FUEO53",
            "LPAT54",
            "TFNT56",
            "TFST57",
            "PUYT51",
            "RJKT57",
            "ZADT50",
            "OSIT51",
            "PMIT50"
        };

        return locationList;
    }

    private static List<string> OnlyHertzCountries()
    {
        var countryList = new List<string>
        {
            "DE"
        };

        return countryList;
    }
}