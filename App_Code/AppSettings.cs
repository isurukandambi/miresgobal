﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;

/// <summary>
/// Summary description for AppSettings
/// </summary>
public static class AppSettings
{
    //get the cartrawler Endpoint url
    public static string CarTrawlerXmlEndPoint
    {
        get
        {
            return WebConfigurationManager.AppSettings["CurrentCarTrawlerEndPointURL"] == "ProductionCarTrawlerEndPointURL" ? WebConfigurationManager.AppSettings["ProductionCarTrawlerEndPointURL"] : WebConfigurationManager.AppSettings["TestCarTrawlerEndPointURL"];
        }
    }

    public static string CarTrawlerResXmlEndPoint
    {
        get
        {
            return WebConfigurationManager.AppSettings["CurrentCarTrawlerResEndPointURL"] == "ProductionCarTrawlerResEndPointURL" ? WebConfigurationManager.AppSettings["ProductionCarTrawlerResEndPointURL"] : WebConfigurationManager.AppSettings["TestCarTrawlerResEndPointURL"];
        }
    }

    public static string HertzXmlEndPoint
    {
        get
        {
            return WebConfigurationManager.AppSettings["CurrentHertzEndPointURL"] == "ProductionHertzEndPointURL" ? WebConfigurationManager.AppSettings["ProductionHertzEndPointURL"] : WebConfigurationManager.AppSettings["TestHertzEndPointURL"];
        }
    }

    public static string ThermeonXmlEndPoint
    {
        get
        {
            return WebConfigurationManager.AppSettings["CurrentThermeonEndPointURL"] == "ProductionThermeonEndPointURL" ? WebConfigurationManager.AppSettings["ProductionThermeonEndPointURL"] : WebConfigurationManager.AppSettings["TestThermeonEndPointURL"];
        }
    }

    //Hertz Configurations Begins
    public static bool IsWiterTyresEnabled
    {
        get
        {
            return WebConfigurationManager.AppSettings["IsWinterTyreEnabledForGermany"] == "1";
        }
    }
    //Hertz Configurations Ends

    //Is Debug Xml email enabled 
    public static bool IsDebugXmlEmailEnabled
    {
        get
        {
            return WebConfigurationManager.AppSettings["DebugXMLEmail"] == "1";
        }
    }

    //get the Debug XML notify email account - if the above value is set to '1' then this email account will be sent the XML values.
    public static string DebugXmlDetailsNotifyEmails
    {
        get
        {
            return WebConfigurationManager.AppSettings["DebugXMLNotifyEmails"];
        }
    }

    //get web site default emails sending email address
    public static string SiteEmailsSentFrom
    {
        get
        {
            return WebConfigurationManager.AppSettings["EmailsSentFrom"];
        }
    }
}