﻿namespace Templates
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Xml;

    /// <summary>
    /// Summary description for ErrorXmlTemplate
    /// </summary>
    public static class ErrorXmlTemplate
    {
        public static XmlDocument getUnhandledErrorXml(string message)
        {
           var xmlDocument = new XmlDocument();
           xmlDocument.LoadXml("<?xml version=\"1.0\" encoding=\"UTF-8\"?><OTA_ErrorRS xmlns=\"http://www.opentravel.org/OTA/2003/05\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.opentravel.org/OTA/2003/05\"><Errors><Error Type=\"500\" ShortText=\"" + message + "\" Code=\"500\"/></Errors></OTA_VehAvailRateRS>");
           return xmlDocument;
        }

        public static XmlDocument getUnhandledErrorXml(string message, XMLAPITypes apiType)
        {
            var xmlDocument = new XmlDocument();
            if (apiType == XMLAPITypes.AvailabilityRequest)
            {
                xmlDocument.LoadXml("<?xml version=\"1.0\" encoding=\"UTF-8\"?><OTA_VehAvailRateRS xmlns=\"http://www.opentravel.org/OTA/2003/05\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.opentravel.org/OTA/2003/05\"><Errors><Error Type=\"500\" ShortText=\"" + message + "\" Code=\"500\"/></Errors></OTA_VehAvailRateRS>");
            }
            else if (apiType == XMLAPITypes.ReservationRequest)
            {
                xmlDocument.LoadXml("<?xml version=\"1.0\" encoding=\"UTF-8\"?><OTA_VehResRS xmlns=\"http://www.opentravel.org/OTA/2003/05\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.opentravel.org/OTA/2003/05\"><Errors><Error Type=\"500\" ShortText=\"" + message + "\" Code=\"500\"/></Errors></OTA_VehResRS>");
            }
            else if (apiType == XMLAPITypes.RetrivalRequest)
            {
                xmlDocument.LoadXml("<?xml version=\"1.0\" encoding=\"UTF-8\"?><OTA_VehRetResRS xmlns=\"http://www.opentravel.org/OTA/2003/05\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.opentravel.org/OTA/2003/05\"><Errors><Error Type=\"500\" ShortText=\"" + message + "\" Code=\"500\"/></Errors></OTA_VehRetResRS>");
            }
            else if (apiType == XMLAPITypes.ModificationRequest)
            {
                xmlDocument.LoadXml("<?xml version=\"1.0\" encoding=\"UTF-8\"?><OTA_VehModifyRS xmlns=\"http://www.opentravel.org/OTA/2003/05\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.opentravel.org/OTA/2003/05\"><Errors><Error Type=\"500\" ShortText=\"" + message + "\" Code=\"500\"/></Errors></OTA_VehModifyRS>");
            }
            else if (apiType == XMLAPITypes.CancellationRequest)
            {
                xmlDocument.LoadXml("<?xml version=\"1.0\" encoding=\"UTF-8\"?><OTA_VehCancelRS xmlns=\"http://www.opentravel.org/OTA/2003/05\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.opentravel.org/OTA/2003/05\"><Errors><Error Type=\"500\" ShortText=\"" + message + "\" Code=\"500\"/></Errors></OTA_VehCancelRS>");
            }
            else
            {
                xmlDocument.LoadXml("<?xml version=\"1.0\" encoding=\"UTF-8\"?><OTA_ErrorRS xmlns=\"http://www.opentravel.org/OTA/2003/05\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.opentravel.org/OTA/2003/05\"><Errors><Error Type=\"500\" ShortText=\"" + message + "\" Code=\"500\"/></Errors></OTA_ErrorRS>");
            }

            return xmlDocument;
        }
    }
}