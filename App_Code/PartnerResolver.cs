﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Exceptions;
using ORM;

/// <summary>
/// Summary description for PartnerResolver
/// </summary>
public class PartnerResolver
{
    public Partner extractPartner(string agentDutyCode, DatabaseEntities db)
    {
        var partner = db.Partners.Where(p => p.AgentDutyCode == agentDutyCode);
        if (partner.Count() == 1)
        {
            return partner.First();
        }
        else
        {
            throw new InvalidRequestException("No Registered Partner found for provided parameter");
        }
    }
}