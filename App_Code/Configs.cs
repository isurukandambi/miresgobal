﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

/// <summary>
/// Summary description for Configs
/// </summary>
public class Configs
{
    public static readonly string RESPONSE_CONTENT_TYPE = "text/xml";
    public static readonly Encoding REQUEST_ENCODE_TYPE = System.Text.Encoding.UTF8;
    public static readonly Encoding RESPONSE_ENCODE_TYPE = System.Text.Encoding.UTF8;
    public static readonly string XML_NAMESPACE = "http://www.opentravel.org/OTA/2003/05";
    public static readonly string THERMEON_XML_NAMESPACE = "http://www.thermeon.com/webXG/xml/webxml/";
}