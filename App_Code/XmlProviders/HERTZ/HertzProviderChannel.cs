﻿namespace HERTZ
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Xml;
    using Exceptions;
    using ORM;
    using ProcessResources;
    using WebServiceMail;

    public class HertzProviderChannel : GenericProviderChannel
    {
        public override void doProviderSpecificValidationsForRequest(VehicleAvailRateRQResources resources)
        {
            //removeCustLoyaltyNodes(resources.BrandName);
            replaceOperatingCompany();
            replaceShortLoationCodesForFirefly(resources.BrandName, resources.PickupLocationExtendedCode, resources.DropoffLocationExtendedCode);
            changeRateQualifierThriftyAvail(resources);
            removeTPANode();
            addSourceNode(resources);
        }

        public override void doProviderSpecificValidationsForResponse(VehicleAvailRateRQResources resources)
        {
            if (hasErrorsInAvailableResponse(resources))
            {
                if (resources.BrandName.ToString().ToUpper() == Brands.THRIFTY.ToString())
                {
                    if (hasRQErrorsInAvailResponse(resources))
                    {
                        resources.IsThriftyRQerror = true;
                        changeRateQualifierThriftyAvail(resources);
                    }
                }
            }
            else
            {
                replaceExtendedLoationCodesForFirefly(resources.BrandName, resources.PickupLocationCode, resources.DropoffLocationCode);
                setVendorCodeForResponse(resources.BrandName);
                modifyVehicleDetails(resources.PickupLocationCode, resources.BrandId);
            }
        }

        public override void doProviderSpecificValidationsForRequest(VehicleResRQResources resources)
        {
            removeVendorNode();
            //removeCustLoyaltyNodes(resources.BrandName);
            replaceOperatingCompany();
            replaceShortLoationCodesForFirefly(resources.BrandName, resources.PickupLocationExtendedCode, resources.DropoffLocationExtendedCode);
            removeWrittenConfInstNode();
            changeRateQualifierThriftyRes(resources);
            addSourceNode(resources);
            setBookingRequestData(resources);
            setAccessoriesData(resources);
            setVehicleDetailsList(resources);
            setIsMailFunctionEnabled(resources);
            savePreBookingInDatabase(resources);
        }

        public override void doProviderSpecificValidationsForResponse(VehicleResRQResources resources)
        {
            try
            {
                //First Check is Error XML (If Error Do not Execute below methods)
                if (hasErrorsInBookingResponse(resources))
                {
                    //Check is Gold number node Error XML (If Error Do not Execute below methods)
                    if (isGoldNumberRelatedError(resources))
                    {
                        resources.IsGoldNumberError = true;
                        removeCustLoyaltyNode(resources);
                    }
                    else
                    {
                        if (resources.BrandName.ToString().ToUpper() == Brands.THRIFTY.ToString())
                        {
                            if (hasRQErrorsInResResponse(resources))
                            {
                                resources.IsThriftyRQerror = true;
                                changeRateQualifierThriftyRes(resources);
                            }
                            else
                            {
                                removeArrivalNode(resources);
                            }
                        }
                        else
                        {
                            removeArrivalNode(resources);
                        }
                    }
                }
                else
                {
                    replaceExtendedLoationCodesForFirefly(resources.BrandName, resources.PickupLocationCode, resources.DropoffLocationCode);
                    setVendorCodeForResponse(resources.BrandName);
                    modifyVehicleDetails(resources);
                    setBookingResponseData(resources);
                    SendEmail(resources);
                    saveBookingInDatabase(resources);
                    //setAccesoriesData(resources);
                }
            }
            catch (Exception e)
            {
                RemoteDebugger.SendXmlForDebugging(e.StackTrace, "Stack");
            }
        }
        public override void doProviderSpecificValidationsForRequest(VehicleRetrivaleRQResources resources)
        {
            //throw new NotImplementedException();
            addSourceNode(resources);
            //RemoteDebugger.SendXmlForDebugging("Hertz Provider specific mapping or retreival request", "mapping or retreival request");
        }


        public override void doProviderSpecificValidationsForResponse(VehicleRetrivaleRQResources resources)
        {
            //throw new NotImplementedException();
            modifyResRetVehicleDetails(resources);
            //setRetrivalRequestData(resources);
            //replaceExtendedLoationCodesForFirefly(resources.BrandName, resources.PickupLocationCode, resources.DropoffLocationCode);
            setVendorCodeForResponse(resources.BrandName);
            //modifyVehicleDetails(resources.PickupLocationCode, resources.BrandId);
        }

        public override void doProviderSpecificValidationsForRequest(VehicleResModifyRQResources resources)
        {
            //removeCustLoyaltyNodes(resources.BrandName);
            //replaceOperatingCompany();
            //replaceShortLoationCodesForFirefly(resources.BrandName, resources.PickupLocationExtendedCode, resources.DropoffLocationExtendedCode);
            //removeWrittenConfInstNode();
            //setBookingRequestData(resources);
            setDateTimeMod(resources);
            changeRateQualifierThriftyMod(resources);
            addSourceNode(resources);
            RemoveOrdersDB(resources);
            //setAccessoriesData(resources);
            //setVehicleDetailsList(resources);
            //setIsMailFunctionEnabled(resources);
            //savePreBookingInDatabase(resources);
        }

        public override void doProviderSpecificValidationsForResponse(VehicleResModifyRQResources resources)
        {
            if (hasErrorsInModifyResponse(resources))
            {
                if (resources.BrandName.ToString().ToUpper() == Brands.THRIFTY.ToString())
                {
                    if (hasRQErrorsInModResponse(resources))
                    {
                        resources.IsThriftyRQerror = true;
                        changeRateQualifierThriftyMod(resources);
                    }
                }
            }
            else
            {
                //setHasErrorsInBookingResponse(resources);
                //SendEmail(resources);
                setModifyBookingResponseData(resources);
                setModifyRequestData(resources);
                setVendorCodeForResponse(resources.BrandName);
                //if (!resources.HasError)
                //{
                //    replaceExtendedLoationCodesForFirefly(resources.BrandName, resources.PickupLocationCode, resources.DropoffLocationCode);
                //    setVendorCodeForResponse(resources.BrandName, resources.BrandId, resources.PartnerId);
                //    modifyVehicleDetails(resources);
                //    setBookingResponseData(resources);
                //}
            }
        }

        public override void doProviderSpecificValidationsForRequest(VehicleCancelRQResources resources)
        {
            //throw new NotImplementedException();
            addSourceNode(resources);
            //RemoteDebugger.SendXmlForDebugging("Hertz Provider specific mapping or retreival request", "mapping or retreival request");
        }


        public override void doProviderSpecificValidationsForResponse(VehicleCancelRQResources resources)
        {
            //throw new NotImplementedException();
            setCancelRequestData(resources);
            //replaceExtendedLoationCodesForFirefly(resources.BrandName, resources.PickupLocationCode, resources.DropoffLocationCode);
            //setVendorCodeForResponse(resources.BrandName, resources.BrandId, resources.PartnerId);
            //modifyVehicleDetails(resources.PickupLocationCode, resources.BrandId);
        }
        public override void doProviderSpecificValidationsForRequest(VehicleRentalRQResources resources)
        {
            //throw new NotImplementedException();
        }
        public override void doProviderSpecificValidationsForResponse(VehicleRentalRQResources resources)
        {
            //throw new NotImplementedException();
        }

        //Comman Methods

        private void replaceOperatingCompany()
        {
            XmlNode OperatingCompanyNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:OperatingCompany", namespaceManager);
            if (OperatingCompanyNode != null)
            {
                if (OperatingCompanyNode.Attributes != null)
                {
                    XmlAttribute OperatingCompanyCode = OperatingCompanyNode.Attributes["Code"];
                    if (OperatingCompanyCode.Value == "MK")
                    {
                        OperatingCompanyCode.Value = "FR";
                    }
                }
            }
        }

        private void replaceShortLoationCodesForFirefly(string brand, string pickupExtendedLocationcode, string dropoffExtendedLocationCode)
        {
            if (brand.ToUpper() == Brands.FIREFLY.ToString())
            {
                XmlAttribute PickUpLocationAttr = xmlDocument.DocumentElement.SelectSingleNode("//a1:PickUpLocation", namespaceManager).Attributes["LocationCode"];
                XmlAttribute ReturnLocationAttr = xmlDocument.DocumentElement.SelectSingleNode("//a1:ReturnLocation", namespaceManager).Attributes["LocationCode"];
                PickUpLocationAttr.Value = pickupExtendedLocationcode;
                ReturnLocationAttr.Value = dropoffExtendedLocationCode;
            }
        }

        private void replaceExtendedLoationCodesForFirefly(string brand, string pickupLocationcode, string dropoffLocationCode)
        {
            if (brand.ToUpper() == Brands.FIREFLY.ToString())
            {
                XmlAttribute PickUpLocationAttr = xmlDocument.DocumentElement.SelectSingleNode("//a1:PickUpLocation", namespaceManager).Attributes["LocationCode"];
                XmlAttribute ReturnLocationAttr = xmlDocument.DocumentElement.SelectSingleNode("//a1:ReturnLocation", namespaceManager).Attributes["LocationCode"];
                PickUpLocationAttr.Value = pickupLocationcode;
                ReturnLocationAttr.Value = dropoffLocationCode;
            }
        }

        private void setVendorCodeForResponse(string brandName)
        {
            XmlNode vendor = xmlDocument.DocumentElement.SelectSingleNode("//a1:Vendor", namespaceManager);
            if (vendor != null)
            {
                if (brandName.ToString().ToUpper() == Brands.HERTZ.ToString())
                {
                    vendor.Attributes["Code"].Value = "ZE";
                }
                else if (brandName.ToString().ToUpper() == Brands.THRIFTY.ToString())
                {
                    vendor.Attributes["Code"].Value = "ZT";
                }
                else if (brandName.ToString().ToUpper() == Brands.FIREFLY.ToString())
                {
                    vendor.Attributes["Code"].Value = "FF";
                }
            }
        }

        //-------------------------------------------------------------------------------------------------------

        //Available Methods

        private void changeRateQualifierThriftyAvail(VehicleAvailRateRQResources resources)
        {
            if (resources.IsThriftyRQerror == true)
            {
                XmlDocument validatedRequestXml = resources.ValidatedRequest;
                XmlNode rateQualifierNode = validatedRequestXml.DocumentElement.SelectSingleNode("//a1:RateQualifier", namespaceManager);

                if (resources.ProviderRateQualifier == "NTG")
                {
                    rateQualifierNode.Attributes["RateQualifier"].Value = "DTG";
                }
                else
                {
                    resources.IsThriftyRQerror = false;
                }
            }
            else
            {
                XmlNode rateQualifierNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:RateQualifier", namespaceManager);

                if (resources.BrandName.ToString().ToUpper() == Brands.THRIFTY.ToString())
                {
                    if (resources.ProviderRateQualifier == "PPP")
                    {
                        resources.ProviderRateQualifier = "PTG";
                        rateQualifierNode.Attributes["RateQualifier"].Value = "PTG";
                    }

                    if (resources.ProviderRateQualifier == "NPP")
                    {
                        resources.ProviderRateQualifier = "NTG";
                        rateQualifierNode.Attributes["RateQualifier"].Value = "NTG";
                    }
                }
            }
        }

        private void removeTPANode()
        {
            XmlNode TPANode = xmlDocument.DocumentElement.SelectSingleNode("//a1:TPA_Extensions", namespaceManager);
            if (TPANode != null)
            {
                TPANode.ParentNode.RemoveChild(TPANode);
            }
        }

        private void addSourceNode(VehicleAvailRateRQResources resources)
        {
            string pickupLocationCode = resources.PickupLocationCode;

            if (resources.BrandName.ToString().ToUpper() == Brands.DOLLAR.ToString())
            {

                XmlNode POSNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:POS", namespaceManager);
                if (POSNode != null)
                {
                    XmlElement SoceNode = xmlDocument.CreateElement("Source", Configs.XML_NAMESPACE);
                    POSNode.AppendChild(SoceNode);

                    XmlElement ReqId = xmlDocument.CreateElement("RequestorID", Configs.XML_NAMESPACE);
                    ReqId.SetAttribute("Type", "8");
                    ReqId.SetAttribute("ID", "ZR");
                    SoceNode.AppendChild(ReqId);
                }
            }
            else if (resources.BrandName.ToString().ToUpper() == Brands.THRIFTY.ToString())
            {
                if (resources.AgentDutyCode == "3C1A20T8H25")
                {
                    XmlNode POSNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:POS", namespaceManager);
                    if (POSNode != null)
                    {
                        var tcList = from t in db.TC4R_List
                                     join c in db.Countries on t.CountryIDFK equals c.CountryID
                                     join l in db.Locations on c.CountryID equals l.CountryIDFK
                                     where (l.LocationCode == pickupLocationCode)
                                     select new
                                     {
                                         t.CountryIDFK,
                                         c.CountryName
                                     };
                        if (tcList.Count() > 0)
                        {

                        }
                        else
                        {
                            XmlElement SoceNode = xmlDocument.CreateElement("Source", Configs.XML_NAMESPACE);
                            POSNode.AppendChild(SoceNode);

                            XmlElement ReqId = xmlDocument.CreateElement("RequestorID", Configs.XML_NAMESPACE);
                            ReqId.SetAttribute("Type", "8");
                            ReqId.SetAttribute("ID", "ZT");
                            SoceNode.AppendChild(ReqId);
                        }
                    }
                }
            }
        }

        private bool hasErrorsInAvailableResponse(VehicleAvailRateRQResources resources)
        {
            XmlNode error = xmlDocument.DocumentElement.SelectSingleNode("//a1:Errors", namespaceManager);
            if (error != null)
            {
                return true;
            }
            return false;
        }

        private bool hasRQErrorsInAvailResponse(VehicleAvailRateRQResources resources)
        {
            XmlNode error = xmlDocument.DocumentElement.SelectSingleNode("//a1:Errors", namespaceManager);
            if (error != null)
            {
                XmlNodeList errorList = xmlDocument.DocumentElement.SelectNodes(".//a1:Error", namespaceManager);
                if (errorList != null)
                {
                    foreach (XmlNode node in errorList)
                    {
                        if (node.Attributes != null)
                        {
                            if (node.Attributes["RecordID"].Value == "138")
                            {
                                return true;
                            }
                        }
                    }
                }
            }
            return false;
        }

        private void modifyVehicleDetails(string locationCode, int brandId)
        {
            XmlNodeList vehicleListNode = xmlDocument.DocumentElement.SelectNodes("//a1:VehAvail", namespaceManager);
            if (vehicleListNode.Count > 0)
            {
                foreach (XmlNode vehicleNode in vehicleListNode)
                {
                    string sippCode = vehicleNode.SelectSingleNode(".//a1:Vehicle", namespaceManager).Attributes["Code"].Value.ToString().Trim();
                    var vehicleDetails = (from l in db.Locations
                                          join c in db.Countries on l.CountryIDFK equals c.CountryID
                                          join vr in db.VehicleResources on c.CountryID equals vr.CountryIDFK
                                          join vdb in db.VehicleDetailsByBrands on vr.VDBIDFK equals vdb.VDBID
                                          join b in db.Brands on vdb.BrandIDFK equals b.BrandID
                                          join cg in db.CarGroups on vdb.CarGroupIDFK equals cg.CarGroupID
                                          where b.BrandID == brandId
                                          && l.LocationCode == locationCode
                                          && cg.CarCode == sippCode
                                          select new
                                          {
                                              vdb.CarDescription,
                                              vdb.CarImagePath,
                                              c.CountryCode
                                          });
                    XmlNode vehicleMakeModelNode = vehicleNode.SelectSingleNode(".//a1:VehMakeModel", namespaceManager);
                    XmlNode pictureUrlNode = vehicleNode.SelectSingleNode(".//a1:PictureURL", namespaceManager);
                    if (vehicleDetails.Count() > 0)
                    {
                        vehicleMakeModelNode.Attributes["Name"].Value = vehicleDetails.FirstOrDefault().CarDescription;
                        pictureUrlNode.InnerText = vehicleDetails.FirstOrDefault().CarImagePath;

                        //Remove Special Equipment (Winter Tyres if enabled)
                        //if (AppSettings.IsWiterTyresEnabled)
                        //{
                        //    setWinterTyresForGermany(vehicleDetails.FirstOrDefault().CountryCode, vehicleNode);
                        //}
                    }
                    string vehicleName = vehicleMakeModelNode.Attributes["Name"].Value;
                    int indexOfSimilarText = vehicleName.IndexOf("OR SIMILAR");
                    if (indexOfSimilarText != -1)
                    {
                        vehicleMakeModelNode.Attributes["Name"].Value = vehicleName.Substring(0, indexOfSimilarText);
                    }
                }
            }
        }

        private void setWinterTyresForGermany(string countryCode, XmlNode vehicleNode)
        {
            if (countryCode == "DE")
            {
                XmlNodeList equipList = vehicleNode.SelectNodes("//a1:PricedEquip", namespaceManager);
                foreach (XmlNode equipNode in equipList)
                {
                    if (equipNode.SelectSingleNode("//a1:Equipment", namespaceManager).Attributes["EquipType"].Value == "14")
                    {
                        equipNode.ParentNode.RemoveChild(equipNode);
                    }
                }
                if (vehicleNode.SelectSingleNode("//a1:PricedEquips", namespaceManager) != null)
                {
                    if (!vehicleNode.SelectSingleNode("//a1:PricedEquips", namespaceManager).HasChildNodes)
                    {
                        XmlNode equiNodeParent = vehicleNode.SelectSingleNode("//a1:PricedEquips", namespaceManager);
                        vehicleNode.SelectSingleNode("//a1:VehAvailCore", namespaceManager).RemoveChild(equiNodeParent);
                    }
                }
            }
        }

        //-------------------------------------------------------------------------------------------------------

        //Reservation Methods

        private void removeVendorNode()
        {
            XmlNode PrefNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:VehPref", namespaceManager);
            if (PrefNode != null)
            {
                PrefNode.ParentNode.RemoveChild(PrefNode);
            }

            //Remove card holder's name

            XmlNode payment = xmlDocument.DocumentElement.SelectSingleNode("//a1:PaymentCard", namespaceManager);
            if (payment != null)
            {
                if (payment.Attributes["Name"] != null)
                {
                    payment.Attributes.RemoveNamedItem("Name");
                }

                if (payment.Attributes["CVV"] != null)
                {
                    payment.Attributes.RemoveNamedItem("CVV");
                }
            }
        }

        private void removeWrittenConfInstNode()
        {
            XmlNode WrittenConfInstNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:WrittenConfInst", namespaceManager);
            if (WrittenConfInstNode != null)
            {
                WrittenConfInstNode.ParentNode.RemoveChild(WrittenConfInstNode);
            }
            XmlNode TPANode = xmlDocument.DocumentElement.SelectSingleNode("//a1:TPA_Extensions", namespaceManager);
            if (TPANode != null)
            {
                TPANode.ParentNode.RemoveChild(TPANode);
            }
        }

        private void changeRateQualifierThriftyRes(VehicleResRQResources resources)
        {
            if (resources.IsThriftyRQerror == true)
            {
                XmlDocument validatedRequestXml = resources.ValidatedRequest;
                XmlNode rateQualifierNode = validatedRequestXml.DocumentElement.SelectSingleNode("//a1:RateQualifier", namespaceManager);

                if (resources.ProviderRateQualifier == "NTG")
                {
                    rateQualifierNode.Attributes["RateQualifier"].Value = "DTG";
                }
                else
                {
                    resources.IsThriftyRQerror = false;
                }
            }
            else
            {
                XmlNode rateQualifierNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:RateQualifier", namespaceManager);

                if (resources.BrandName.ToString().ToUpper() == Brands.THRIFTY.ToString())
                {
                    if (resources.ProviderRateQualifier == "PPP")
                    {
                        resources.ProviderRateQualifier = "PTG";
                        rateQualifierNode.Attributes["RateQualifier"].Value = "PTG";
                    }

                    if (resources.ProviderRateQualifier == "NPP")
                    {
                        resources.ProviderRateQualifier = "NTG";
                        rateQualifierNode.Attributes["RateQualifier"].Value = "NTG";
                    }
                }
            }
        }

        private void addSourceNode(VehicleResRQResources resources)
        {
            string pickupLocationCode = resources.PickupLocationCode;

            if (resources.BrandName.ToString().ToUpper() == Brands.DOLLAR.ToString())
            {

                XmlNode POSNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:POS", namespaceManager);
                if (POSNode != null)
                {
                    XmlElement SoceNode = xmlDocument.CreateElement("Source", Configs.XML_NAMESPACE);
                    POSNode.AppendChild(SoceNode);

                    XmlElement ReqId = xmlDocument.CreateElement("RequestorID", Configs.XML_NAMESPACE);
                    ReqId.SetAttribute("Type", "8");
                    ReqId.SetAttribute("ID", "ZR");
                    SoceNode.AppendChild(ReqId);
                }
            }
            else if (resources.BrandName.ToString().ToUpper() == Brands.THRIFTY.ToString())
            {
                if (resources.AgentDutyCode == "3C1A20T8H25")
                {
                    XmlNode POSNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:POS", namespaceManager);
                    if (POSNode != null)
                    {
                        var tcList = from t in db.TC4R_List
                                     join c in db.Countries on t.CountryIDFK equals c.CountryID
                                     join l in db.Locations on c.CountryID equals l.CountryIDFK
                                     where (l.LocationCode == pickupLocationCode)
                                     select new
                                     {
                                         t.CountryIDFK,
                                         c.CountryName
                                     };
                        if (tcList.Count() > 0)
                        {

                        }
                        else
                        {
                            XmlElement SoceNode = xmlDocument.CreateElement("Source", Configs.XML_NAMESPACE);
                            POSNode.AppendChild(SoceNode);

                            XmlElement ReqId = xmlDocument.CreateElement("RequestorID", Configs.XML_NAMESPACE);
                            ReqId.SetAttribute("Type", "8");
                            ReqId.SetAttribute("ID", "ZT");
                            SoceNode.AppendChild(ReqId);
                        }
                    }
                }
            }
        }

        private void setBookingRequestData(VehicleResRQResources bookingResouce)
        {
            XmlNode firstNameNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:GivenName", namespaceManager);
            if (firstNameNode != null)
            {
                bookingResouce.CustomerFirstname = firstNameNode.InnerText.Trim();
            }
            XmlNode surNameNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:Surname", namespaceManager);
            if (surNameNode != null)
            {
                bookingResouce.CustomerSurname = surNameNode.InnerText.Trim();
            }
            XmlNode customerEmailNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:Email", namespaceManager);
            if (customerEmailNode != null)
            {
                bookingResouce.CustomerEmail = customerEmailNode.InnerText.Trim();
            }
            XmlNode customerPhoneNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:Telephone", namespaceManager);
            if (customerPhoneNode != null)
            {
                bookingResouce.CustomerPhone = customerPhoneNode.Attributes["PhoneNumber"].Value.Trim();
            }
            XmlNode customerAddressNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:AddressLine", namespaceManager);
            if (customerAddressNode != null)
            {
                bookingResouce.Address = customerAddressNode.InnerText.Trim();
            }
            XmlNode customerCityNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:CityName", namespaceManager);
            if (customerCityNode != null)
            {
                bookingResouce.CityName = customerCityNode.InnerText.Trim();
            }
            XmlNode customerPostalCodeNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:PostalCode", namespaceManager);
            if (customerPostalCodeNode != null)
            {
                bookingResouce.PostalCode = customerPostalCodeNode.InnerText.Trim();
            }
            XmlNode customerCountryNameNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:CountryName", namespaceManager);
            if (customerCountryNameNode != null)
            {
                bookingResouce.CountryOfResidenceCode = customerCountryNameNode.Attributes["Code"].Value.Trim();
            }
            XmlNode primaryNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:Primary", namespaceManager);
            if (primaryNode != null)
            {
                if (primaryNode.Attributes["BirthDate"] != null)
                {
                    bookingResouce.BirthDay = primaryNode.Attributes["BirthDate"].Value.Trim();
                }
            }
            setBookingLocationCountryDetails(bookingResouce.PickupLocationCode, true, bookingResouce);
            setBookingLocationCountryDetails(bookingResouce.DropoffLocationCode, false, bookingResouce);
        }

        private void setBookingLocationCountryDetails(string locationCode, bool mode, VehicleResRQResources bookingResouce)
        {
            var CountryData = (from l in db.Locations
                               join c in db.Countries on l.CountryIDFK equals c.CountryID
                               where (l.LocationCode == locationCode)
                               select new
                               {
                                   c.CountryCode,
                                   c.CountryName
                               });
            if (CountryData.Count() > 0)
            {
                if (mode)
                {
                    bookingResouce.PickUpCountryCode = CountryData.First().CountryCode;
                    bookingResouce.PickUpCountryName = CountryData.First().CountryName;
                }
                else
                {
                    bookingResouce.DropOffCountryCode = CountryData.First().CountryCode;
                    bookingResouce.DropOffCountryName = CountryData.First().CountryName;
                }
            }
        }

        private void setAccessoriesData(VehicleResRQResources bookingResouce)
        {
            XmlNode accessoryNode = xmlDocument.SelectSingleNode("//a1:SpecialEquipPrefs", namespaceManager);
            if (accessoryNode != null)
            {
                bookingResouce.AccesoriesList = new List<Accessory>();
                XmlNodeList accessoryNodeList = accessoryNode.SelectNodes(".//a1:SpecialEquipPref", namespaceManager);
                foreach (XmlNode accessory in accessoryNodeList)
                {
                    if (accessory != null)
                    {
                        if (accessory.Attributes != null)
                        {
                            string accessoryType = accessory.Attributes["EquipType"].Value.Trim();
                            var accessoryDbObj = (from a in db.Accessories
                                                  where a.HertzCode == accessoryType
                                                  select new
                                                  {
                                                      a.AccessoryID,
                                                      a.Image1,
                                                      a.Title,
                                                      a.Price,
                                                      a.Details,
                                                      a.HertzCode
                                                  });
                            string accessoryImage = "Default picture";
                            string accessoryTitle = "Default Name";
                            if (accessoryDbObj != null)
                            {
                                accessoryImage = accessoryDbObj.FirstOrDefault().Image1;
                                accessoryTitle = HttpContext.GetGlobalResourceObject("SiteStrings", "Extra_" + accessoryDbObj.FirstOrDefault().HertzCode + "_Title").ToString();


                                Accessory accessoryObj = new Accessory
                                {
                                    AccessoryID = accessoryDbObj.FirstOrDefault().AccessoryID,
                                    Price = accessoryDbObj.FirstOrDefault().Price,
                                    Title = accessoryDbObj.FirstOrDefault().Title,
                                    Image1 = accessoryDbObj.FirstOrDefault().Image1,
                                    HertzCode = accessoryDbObj.FirstOrDefault().HertzCode,
                                    Details = accessoryDbObj.FirstOrDefault().Details
                                };
                                bookingResouce.AccesoriesList.Add(accessoryObj);
                            }
                        }
                    }
                }
            }
        }

        private void setVehicleDetailsList(VehicleResRQResources bookingResouce)
        {
            int brand = bookingResouce.BrandId;
            bookingResouce.VehicleDetailsList = new List<VehicleDetail>();
            var vehicleDetails = (from l in db.Locations
                                  join c in db.Countries on l.CountryIDFK equals c.CountryID
                                  join vr in db.VehicleResources on c.CountryID equals vr.CountryIDFK
                                  join vdb in db.VehicleDetailsByBrands on vr.VDBIDFK equals vdb.VDBID
                                  join cg in db.CarGroups on vdb.CarGroupIDFK equals cg.CarGroupID
                                  where vdb.BrandIDFK == bookingResouce.BrandId
                                  && (l.LocationCode == bookingResouce.PickupLocationCode)
                                  select new
                                  {
                                      vdb.CarDescription,
                                      vdb.CarImagePath,
                                      cg.CarCode
                                  });
            if (vehicleDetails.Count() > 0)
            {
                foreach (var vehicleData in vehicleDetails)
                {
                    VehicleDetail vehicle = new VehicleDetail
                    {
                        SippCode = vehicleData.CarCode,
                        Name = vehicleData.CarDescription,
                        ImagePath = vehicleData.CarImagePath
                    };
                    bookingResouce.VehicleDetailsList.Add(vehicle);
                }
            }
        }

        private void setIsMailFunctionEnabled(VehicleResRQResources resources)
        {
            var partnerData = db.Partners.Where(p => p.PartnerId == resources.PartnerId);
            resources.IsMailFunctionEnabled = (partnerData.FirstOrDefault().ConfimationMailSend == 1 ? true : false);
        }

        private void savePreBookingInDatabase(VehicleResRQResources resources)
        {
            BokkingRequest preBookingObj = new BokkingRequest
            {
                CustomerName = resources.CustomerFirstname + " " + resources.CustomerSurname ?? "",
                CustomerEmail = resources.CustomerEmail ?? "",
                CustomerPhone = resources.CustomerPhone ?? "",
                PickUpDate = resources.PickupDateTime,
                DropOffDate = resources.DropoffDatetime,
                PickUpLocation = resources.PickupLocationCode,
                DropOffLocation = resources.DropoffLocationCode ?? "",
                CustomerSurname = resources.CustomerSurname ?? "",
                CustomerFirstname = resources.CustomerFirstname ?? "",
                AddressLine1 = resources.Address ?? "",
                AddressLine2 = resources.CityName ?? "",
                AddressTown = resources.CityName ?? "",
                AddressPostCodeZip = resources.PostalCode ?? "",
                CountryOfResidence = resources.CountryOfResidenceCode ?? "",
                CameFrom = resources.PartnerName ?? "",
                XML_Provider = resources.ProviderName ?? "",
                OrderDate = DateTime.Now
            };
            db.BokkingRequests.Add(preBookingObj);
            db.SaveChanges();
        }

        private bool hasErrorsInBookingResponse(VehicleResRQResources resources)
        {
            XmlNode error = xmlDocument.DocumentElement.SelectSingleNode("//a1:Errors", namespaceManager);
            if (error != null)
            {
                return true;
            }
            return false;
        }

        private bool isGoldNumberRelatedError(VehicleResRQResources resources)
        {
            XmlNode error = xmlDocument.DocumentElement.SelectSingleNode("//a1:Errors", namespaceManager);
            if (error != null)
            {
                XmlNodeList errorList = xmlDocument.DocumentElement.SelectNodes(".//a1:Error", namespaceManager);
                if (errorList != null)
                {
                    foreach (XmlNode node in errorList)
                    {
                        if (node.Attributes["RecordID"] != null)
                        {
                            if (node.Attributes["RecordID"].Value == "011" || node.Attributes["RecordID"].Value == "079")
                            {
                                return true;
                            }
                        }
                    }
                }
            }
            return false;
        }

        private void removeCustLoyaltyNode(VehicleResRQResources resources)
        {
            XmlDocument validatedRequestXml = resources.ValidatedRequest;
            XmlNode GoldNo = validatedRequestXml.DocumentElement.SelectSingleNode("//a1:CustLoyalty", namespaceManager);
            if (GoldNo != null)
            {
                GoldNo.ParentNode.RemoveChild(GoldNo);
            }
            resources.ValidatedRequest = validatedRequestXml;
        }

        private bool hasRQErrorsInResResponse(VehicleResRQResources resources)
        {
            XmlNode error = xmlDocument.DocumentElement.SelectSingleNode("//a1:Errors", namespaceManager);
            if (error != null)
            {
                XmlNodeList errorList = xmlDocument.DocumentElement.SelectNodes(".//a1:Error", namespaceManager);
                if (errorList != null)
                {
                    foreach (XmlNode node in errorList)
                    {
                        if (node.Attributes["RecordID"].Value == "138")
                        {
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        private void removeArrivalNode(VehicleResRQResources resources)
        {
            XmlDocument validatedRequestXml = resources.ValidatedRequest;
            XmlNode arrival = validatedRequestXml.DocumentElement.SelectSingleNode("//a1:ArrivalDetails", namespaceManager);
            if (arrival != null)
            {
                resources.IsFlightNo = true;
                arrival.ParentNode.RemoveChild(arrival);
            }
            resources.ValidatedRequest = validatedRequestXml;
        }

        private void modifyVehicleDetails(VehicleResRQResources bookingResouce)
        {
            XmlNode vehicleNode = xmlDocument.SelectSingleNode("//a1:Vehicle", namespaceManager);
            if (vehicleNode != null)
            {
                string carCode = vehicleNode.Attributes["Code"].Value;
                var carCodeList = bookingResouce.VehicleDetailsList.Where(c => c.SippCode == carCode);
                XmlNode vehicleMakeModelNode = vehicleNode.SelectSingleNode(".//a1:VehMakeModel", namespaceManager);
                if (carCodeList.Count() > 0)
                {
                    vehicleMakeModelNode.Attributes["Name"].Value = carCodeList.FirstOrDefault().Name;
                    vehicleNode.SelectSingleNode(".//a1:PictureURL", namespaceManager).InnerText = carCodeList.FirstOrDefault().ImagePath;
                }
                string vehicleName = vehicleMakeModelNode.Attributes["Name"].Value;
                int indexOfSimilarText = vehicleName.IndexOf("OR SIMILAR");
                if (indexOfSimilarText != -1)
                {
                    vehicleMakeModelNode.Attributes["Name"].Value = vehicleName.Substring(0, indexOfSimilarText);
                }
            }
        }

        private void setBookingResponseData(VehicleResRQResources resources)
        {
            resources.ReservedVehicle = new BookedVehicle();
            XmlNode firstNameNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:GivenName", namespaceManager);
            if (firstNameNode != null)
            {
                resources.CustomerFirstname = firstNameNode.InnerText.Trim();
            }
            XmlNode surNameNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:Surname", namespaceManager);
            if (surNameNode != null)
            {
                resources.CustomerSurname = surNameNode.InnerText.Trim();
            }
            XmlNode confirmationIdNode = xmlDocument.SelectSingleNode("//a1:ConfID", namespaceManager);
            if (confirmationIdNode != null)
            {
                resources.ConfirmationID = confirmationIdNode.Attributes["ID"].Value;
            }
            XmlNode VehModelNode = xmlDocument.SelectSingleNode("//a1:VehMakeModel", namespaceManager);
            if (VehModelNode != null)
            {
                resources.ReservedVehicle.Name = VehModelNode.Attributes["Name"].Value.ToString();
            }
            XmlNode ImageNode = xmlDocument.SelectSingleNode("//a1:PictureURL", namespaceManager);
            if (ImageNode != null)
            {
                resources.ReservedVehicle.ImagePath = ImageNode.InnerText.Trim();
            }
            XmlNode PaymentNode = xmlDocument.SelectSingleNode("//a1:TotalCharge", namespaceManager);
            if (PaymentNode != null)
            {
                resources.ReservedVehicle.Price = decimal.Parse(PaymentNode.Attributes["EstimatedTotalAmount"].Value);
                resources.ReservedVehicle.Currency = PaymentNode.Attributes["CurrencyCode"].Value;
            }
            XmlNode priceIncludeNode = xmlDocument.SelectSingleNode("//a1:TaxAmount", namespaceManager);
            if (priceIncludeNode != null)
            {
                resources.ReservedVehicle.ImagePath = priceIncludeNode.Attributes["Total"].Value;
            }
        }

        private void SendEmail(VehicleResRQResources bookingResouce)
        {

            var email = (from p in db.Partners
                         where p.DomainName == bookingResouce.PartnerName
                         select new
                         {
                             p.ConfimationMailSend
                         });

            if (email.First().ConfimationMailSend == 1)
            {
                var selectedBrand = bookingResouce.BrandName;

                try
                {
                    BuildConfirmationEmail confEmailBuilder = new BuildConfirmationEmail(bookingResouce, bookingResouce.ReservedVehicle);

                    string emailMessage = confEmailBuilder.BuildEmail(bookingResouce.ConfirmationID, bookingResouce.ReservedVehicle.Currency, bookingResouce.ReservedVehicle.Price.ToString(), bookingResouce.ReservedVehicle.ImagePath, bookingResouce.ReservedVehicle.Name, bookingResouce.ReservedVehicle.DrpFee, bookingResouce.ReservedVehicle.priceInclude.ToString(), bookingResouce.ReservedVehicle.distance, TimeBetweenDates.CalculateMinutes(bookingResouce.PickupDateTime, Convert.ToDateTime(bookingResouce.DropoffDatetime)));
                    string emailSubject = bookingResouce.ModifyProcess ? (string)HttpContext.GetGlobalResourceObject("SiteStrings", "ModifyEmailSubject") : (string)HttpContext.GetGlobalResourceObject("SiteStrings", "BookingEmailSubject");
                    //Send confirmation email

                    bool isSent = SendConfMail.SendMessage(AppSettings.SiteEmailsSentFrom, bookingResouce.CustomerEmail, emailSubject, emailMessage, true);

                    if (!isSent)
                    {
                        string erroremailMessage = CommonClass.GetErrorXML("VehResRS", "1", "099", "Booking Completed Successfully. Error Occured while sending confirmation email. Please Contact Hertz.");
                        string errorSubject = "Error in Booking";

                        SendConfMail.SendMessage(AppSettings.SiteEmailsSentFrom, bookingResouce.CustomerEmail, errorSubject, erroremailMessage, true);
                    }
                }

                catch (Exception ex)
                {
                    RemoteDebugger.SendXmlForDebugging(ex.Message, "Errors");
                    RemoteDebugger.SendXmlForDebugging(ex.StackTrace, "Errors");
                }
            }
        }

        private void saveBookingInDatabase(VehicleResRQResources resources)
        {
            Order BookingObj = new Order
            {
                ConfirmationID = resources.ConfirmationID,
                CustomerName = resources.CustomerFirstname + " " + resources.CustomerSurname ?? "",
                CustomerEmail = resources.CustomerEmail ?? "",
                EmailCode = "",
                EmailConfirmed = 1,
                IncludeInEshot = 1,
                CustomerPhone = resources.CustomerPhone ?? "",
                CarBooked = resources.ReservedVehicle.Name ?? "",
                VehicleType = "",
                CountryOfResidence = resources.CountryOfResidenceCode ?? "",
                PickUpDate = resources.PickupDateTime,
                DropOffDate = resources.DropoffDatetime,
                OrderDate = DateTime.Now,
                PickUpLocation = resources.PickupLocationCode,
                DropOffLocation = resources.DropoffLocationCode ?? "",
                NoDaysHired = 0,
                AmountPaid = Convert.ToDouble(resources.ReservedVehicle.Price),
                AmountPaidCurrency = resources.ReservedVehicle.Currency ?? "",
                CameFrom = resources.PartnerName ?? "",
                LanguageCode = "",
                ReminderEmail = DateTime.Now,
                ThanksEmail = DateTime.Now,
                HertzNum1 = "",
                XML_Provider = resources.ProviderName ?? "",
                XML_Provider_PickupSupplier = resources.BrandName ?? "",
                XML_Provider_DropoffSupplier = resources.BrandName ?? "",
                XML_RateQualifier = resources.ProviderRateQualifier ?? "",
                CustomerSurname = resources.CustomerSurname ?? "",
                CustomerFirstname = resources.CustomerFirstname ?? "",
                AddressLine1 = resources.Address ?? "",
                AddressLine2 = resources.CityName ?? "",
                AddressTown = resources.CityName ?? "",
                AddressPostCodeZip = resources.PostalCode ?? "",
                AddressCountryCode = resources.SourceCountryCode ?? "",
                ThermeonConf_Id = resources.Thermeon_ConfirmationID ?? "",
                CorpDiscountNo = resources.CorpDiscountNo ?? "",
                Customer_BDay = Convert.ToDateTime(resources.BirthDay)
            };
            db.Orders.Add(BookingObj);
            db.SaveChanges();

            int orderID = BookingObj.OrderID;

            try
            {
                if (resources.AccesoriesList != null)
                {
                    XmlNode PriceNode = xmlDocument.SelectSingleNode("//a1:PricedEquips", namespaceManager);
                    if (PriceNode != null)
                    {
                        XmlNodeList PriceNodeList = PriceNode.SelectNodes("//a1:PricedEquip", namespaceManager);
                        foreach (XmlNode accessory in PriceNodeList)
                        {
                            if (accessory != null)
                            {
                                XmlNode EquipmentNode = accessory.SelectSingleNode(".//a1:Equipment", namespaceManager);
                                XmlNode ChargeNode = accessory.SelectSingleNode(".//a1:Charge", namespaceManager);

                                string accessoryType = EquipmentNode.Attributes["EquipType"].Value.Trim();
                                double price = Convert.ToDouble(ChargeNode.Attributes["Amount"].Value.Trim());
                                int quantity = Convert.ToInt32(EquipmentNode.Attributes["Quantity"].Value.Trim());
                                string currency = ChargeNode.Attributes["CurrencyCode"].Value.Trim();

                                AccessoriesOrdered accessoryOrdered = new AccessoriesOrdered
                                {
                                    OrderIDFK = orderID,
                                    Accessory = accessoryType,
                                    Price = price,
                                    Quantity = quantity,
                                    Currency = currency
                                };
                                db.AccessoriesOrdereds.Add(accessoryOrdered);
                                db.SaveChanges();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                RemoteDebugger.SendXmlForDebugging(ex.StackTrace, "ErrorDB");
                RemoteDebugger.SendXmlForDebugging(ex.Message, "ErrorDBMessage");
            }
        }

        //-------------------------------------------------------------------------------------------------------

        //Retrival Methods

        private void addSourceNode(VehicleRetrivaleRQResources resources)
        {
            string pickupLocationCode = resources.PickupLocationCode;

            if (resources.BrandName.ToString().ToUpper() == Brands.DOLLAR.ToString())
            {

                XmlNode POSNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:POS", namespaceManager);
                if (POSNode != null)
                {
                    XmlElement SoceNode = xmlDocument.CreateElement("Source", Configs.XML_NAMESPACE);
                    POSNode.AppendChild(SoceNode);

                    XmlElement ReqId = xmlDocument.CreateElement("RequestorID", Configs.XML_NAMESPACE);
                    ReqId.SetAttribute("Type", "8");
                    ReqId.SetAttribute("ID", "ZR");
                    SoceNode.AppendChild(ReqId);
                }
            }
            else if (resources.BrandName.ToString().ToUpper() == Brands.THRIFTY.ToString())
            {
                if (resources.AgentDutyCode == "3C1A20T8H25")
                {
                    XmlNode POSNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:POS", namespaceManager);
                    if (POSNode != null)
                    {
                        var tcList = from t in db.TC4R_List
                                     join c in db.Countries on t.CountryIDFK equals c.CountryID
                                     join l in db.Locations on c.CountryID equals l.CountryIDFK
                                     where (l.LocationCode == pickupLocationCode)
                                     select new
                                     {
                                         t.CountryIDFK,
                                         c.CountryName
                                     };
                        if (tcList.Count() > 0)
                        {

                        }
                        else
                        {
                            XmlElement SoceNode = xmlDocument.CreateElement("Source", Configs.XML_NAMESPACE);
                            POSNode.AppendChild(SoceNode);

                            XmlElement ReqId = xmlDocument.CreateElement("RequestorID", Configs.XML_NAMESPACE);
                            ReqId.SetAttribute("Type", "8");
                            ReqId.SetAttribute("ID", "ZT");
                            SoceNode.AppendChild(ReqId);
                        }
                    }
                }
            }
        }

        private void modifyResRetVehicleDetails(VehicleRetrivaleRQResources resources)
        {
            string brandname = resources.BrandName;
            XmlNode location = xmlDocument.DocumentElement.SelectSingleNode("//a1:PickUpLocation", namespaceManager);
            string locationCode = location.Attributes["LocationCode"].Value.Trim();

            XmlNode vehicleModel = xmlDocument.DocumentElement.SelectSingleNode("//a1:VehMakeModel", namespaceManager);
            XmlNode vehicleImage = xmlDocument.DocumentElement.SelectSingleNode("//a1:PictureURL", namespaceManager);
            string sippCode = vehicleModel.Attributes["Code"].Value.Trim();

            var vehicleDetails = (from l in db.Locations
                                  join c in db.Countries on l.CountryIDFK equals c.CountryID
                                  join vr in db.VehicleResources on c.CountryID equals vr.CountryIDFK
                                  join vdb in db.VehicleDetailsByBrands on vr.VDBIDFK equals vdb.VDBID
                                  join b in db.Brands on vdb.BrandIDFK equals b.BrandID
                                  join cg in db.CarGroups on vdb.CarGroupIDFK equals cg.CarGroupID
                                  where b.BrandName == brandname
                                  && l.LocationCode == locationCode
                                  && cg.CarCode == sippCode
                                  select new
                                  {
                                      vdb.CarDescription,
                                      vdb.CarImagePath,
                                      c.CountryCode
                                  });

            if (vehicleDetails.Count() > 0)
            {
                vehicleModel.Attributes["Name"].Value = vehicleDetails.FirstOrDefault().CarDescription;
                vehicleImage.InnerText = vehicleDetails.FirstOrDefault().CarImagePath;
            }
        }

        //-------------------------------------------------------------------------------------------------------

        //Modify Methods

        private void setDateTimeMod(VehicleResModifyRQResources resources)
        {
            XmlNode vehRentalCore = xmlDocument.SelectSingleNode("//a1:VehRentalCore", namespaceManager);
            if (vehRentalCore != null)
            {
                resources.PickupDateTime = Convert.ToDateTime(vehRentalCore.Attributes["PickUpDateTime"].Value);
                resources.DropoffDatetime = Convert.ToDateTime(vehRentalCore.Attributes["ReturnDateTime"].Value);
            }
        }

        private void changeRateQualifierThriftyMod(VehicleResModifyRQResources resources)
        {
            if (resources.IsThriftyRQerror == true)
            {
                XmlDocument validatedRequestXml = resources.ValidatedRequest;
                XmlNode rateQualifierNode = validatedRequestXml.DocumentElement.SelectSingleNode("//a1:RateQualifier", namespaceManager);

                if (resources.ProviderRateQualifier == "NTG")
                {
                    rateQualifierNode.Attributes["RateQualifier"].Value = "DTG";
                }
                else
                {
                    resources.IsThriftyRQerror = false;
                }
            }
            else
            {
                XmlNode rateQualifierNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:RateQualifier", namespaceManager);

                if (resources.BrandName.ToString().ToUpper() == Brands.THRIFTY.ToString())
                {
                    if (resources.ProviderRateQualifier == "PPP")
                    {
                        resources.ProviderRateQualifier = "PTG";
                        rateQualifierNode.Attributes["RateQualifier"].Value = "PTG";
                    }

                    if (resources.ProviderRateQualifier == "NPP")
                    {
                        resources.ProviderRateQualifier = "NTG";
                        rateQualifierNode.Attributes["RateQualifier"].Value = "NTG";
                    }
                }
            }
        }

        private void addSourceNode(VehicleResModifyRQResources resources)
        {
            string pickupLocationCode = resources.PickupLocationCode;

            if (resources.BrandName.ToString().ToUpper() == Brands.DOLLAR.ToString())
            {

                XmlNode POSNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:POS", namespaceManager);
                if (POSNode != null)
                {
                    XmlElement SoceNode = xmlDocument.CreateElement("Source", Configs.XML_NAMESPACE);
                    POSNode.AppendChild(SoceNode);

                    XmlElement ReqId = xmlDocument.CreateElement("RequestorID", Configs.XML_NAMESPACE);
                    ReqId.SetAttribute("Type", "8");
                    ReqId.SetAttribute("ID", "ZR");
                    SoceNode.AppendChild(ReqId);
                }
            }
            else if (resources.BrandName.ToString().ToUpper() == Brands.THRIFTY.ToString())
            {
                if (resources.AgentDutyCode == "3C1A20T8H25")
                {
                    XmlNode POSNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:POS", namespaceManager);
                    if (POSNode != null)
                    {
                        var tcList = from t in db.TC4R_List
                                     join c in db.Countries on t.CountryIDFK equals c.CountryID
                                     join l in db.Locations on c.CountryID equals l.CountryIDFK
                                     where (l.LocationCode == pickupLocationCode)
                                     select new
                                     {
                                         t.CountryIDFK,
                                         c.CountryName
                                     };
                        if (tcList.Count() > 0)
                        {
                            XmlNodeList SourceNode = POSNode.SelectNodes("//a1:Source", namespaceManager);
                            if (SourceNode != null)
                            {
                                foreach (XmlNode source in SourceNode)
                                {
                                    if (source.SelectSingleNode(".//a1:RequestorID", namespaceManager).Attributes["ID"].Value == "ZR")
                                    {
                                        source.ParentNode.RemoveChild(source);
                                    }
                                }
                            }
                        }
                        else
                        {
                            XmlElement SoceNode = xmlDocument.CreateElement("Source", Configs.XML_NAMESPACE);
                            POSNode.AppendChild(SoceNode);

                            XmlElement ReqId = xmlDocument.CreateElement("RequestorID", Configs.XML_NAMESPACE);
                            ReqId.SetAttribute("Type", "8");
                            ReqId.SetAttribute("ID", "ZT");
                            SoceNode.AppendChild(ReqId);
                        }
                    }
                }
            }
        }

        private void RemoveOrdersDB(VehicleResModifyRQResources resources)
        {
            XmlNode confID = xmlDocument.DocumentElement.SelectSingleNode("//a1:UniqueID", namespaceManager);
            if (confID != null)
            {
                string ConfirmationID = confID.Attributes["ID"].Value;

                var order = db.Orders.Where(o => o.ConfirmationID == ConfirmationID);
                if (order.Count() > 0)
                {
                    int orderID = order.FirstOrDefault().OrderID;

                    var AccOrders = db.AccessoriesOrdereds.Where(ao => ao.OrderIDFK == orderID);
                    foreach (var accOrd in AccOrders)
                    {
                        db.AccessoriesOrdereds.Remove(accOrd);
                    }
                }
            }

            XmlNode accessory = xmlDocument.DocumentElement.SelectSingleNode("//a1:SpecialEquipPref", namespaceManager);
            if (accessory != null)
            {
                resources.IsExtraValid = true;
            }
        }

        private bool hasErrorsInModifyResponse(VehicleResModifyRQResources resources)
        {
            XmlNode error = xmlDocument.DocumentElement.SelectSingleNode("//a1:Errors", namespaceManager);
            if (error != null)
            {
                return true;
            }
            return false;
        }

        private bool hasRQErrorsInModResponse(VehicleResModifyRQResources resources)
        {
            XmlNode error = xmlDocument.DocumentElement.SelectSingleNode("//a1:Errors", namespaceManager);
            if (error != null)
            {
                XmlNodeList errorList = xmlDocument.DocumentElement.SelectNodes(".//a1:Error", namespaceManager);
                if (errorList != null)
                {
                    foreach (XmlNode node in errorList)
                    {
                        if (node.Attributes["RecordID"].Value == "138")
                        {
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        private void setModifyBookingResponseData(VehicleResModifyRQResources resources)
        {
            resources.ReservedVehicle = new BookedVehicle();
            XmlNode firstNameNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:GivenName", namespaceManager);
            if (firstNameNode != null)
            {
                resources.CustomerFirstname = firstNameNode.InnerText.Trim();
            }
            XmlNode surNameNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:Surname", namespaceManager);
            if (surNameNode != null)
            {
                resources.CustomerSurname = surNameNode.InnerText.Trim();
            }
            XmlNode confirmationIdNode = xmlDocument.SelectSingleNode("//a1:ConfID", namespaceManager);
            if (confirmationIdNode != null)
            {
                resources.ConfirmationID = confirmationIdNode.Attributes["ID"].Value;
            }
            XmlNode VehModelNode = xmlDocument.SelectSingleNode("//a1:VehMakeModel", namespaceManager);
            if (VehModelNode != null)
            {
                resources.ReservedVehicle.Name = VehModelNode.Attributes["Name"].Value.ToString();
            }
            XmlNode ImageNode = xmlDocument.SelectSingleNode("//a1:PictureURL", namespaceManager);
            if (ImageNode != null)
            {
                resources.ReservedVehicle.ImagePath = ImageNode.InnerText.Trim();
            }
            XmlNode PaymentNode = xmlDocument.SelectSingleNode("//a1:TotalCharge", namespaceManager);
            if (PaymentNode != null)
            {
                resources.ReservedVehicle.Price = decimal.Parse(PaymentNode.Attributes["EstimatedTotalAmount"].Value);
                resources.ReservedVehicle.Currency = PaymentNode.Attributes["CurrencyCode"].Value;
            }
            XmlNode priceIncludeNode = xmlDocument.SelectSingleNode("//a1:TaxAmount", namespaceManager);
            if (priceIncludeNode != null)
            {
                resources.ReservedVehicle.ImagePath = priceIncludeNode.Attributes["Total"].Value;
            }
        }

        private void setModifyRequestData(VehicleResModifyRQResources bookingResouce)
        {
            XmlNode Status = xmlDocument.DocumentElement.SelectSingleNode("//a1:VehModifyRSCore", namespaceManager);

            if (Status.Attributes["ModifyStatus"].Value == "Modified")
            {
                XmlNode confirmationId = xmlDocument.DocumentElement.SelectSingleNode("//a1:ConfID", namespaceManager);
                if (confirmationId != null)
                {
                    bookingResouce.ConfirmationID = confirmationId.Attributes["ID"].Value.Trim();


                    var order = (from o in db.Orders
                                 where (o.ConfirmationID == bookingResouce.ConfirmationID)
                                 select new
                                 {
                                     o.OrderID,
                                     o.CustomerName,
                                     o.CustomerEmail,
                                     o.EmailConfirmed,
                                     o.CarBooked,
                                     o.VehicleType,
                                     o.CountryOfResidence,
                                     o.PickUpDate,
                                     o.DropOffDate,
                                     o.OrderDate,
                                     o.PickUpLocation,
                                     o.DropOffLocation,
                                     o.NoDaysHired,
                                     o.AmountPaid,
                                     o.XML_Provider,
                                     o.XML_IATANumber,
                                     o.XML_RateQualifier
                                 });

                    if (order.Count() > 0)
                    {
                        ModifiedOrder orderModify = new ModifiedOrder
                        {
                            OrderID = order.FirstOrDefault().OrderID,
                            ConfirmationID = bookingResouce.ConfirmationID,
                            CustomerName = order.FirstOrDefault().CustomerName,
                            CustomerEmail = order.FirstOrDefault().CustomerEmail,
                            EmailConfirmed = order.FirstOrDefault().EmailConfirmed,
                            CarBooked = order.FirstOrDefault().CarBooked,
                            VehicleType = order.FirstOrDefault().VehicleType,
                            CountryOfResidence = order.FirstOrDefault().CountryOfResidence,
                            PickUpDate = order.FirstOrDefault().PickUpDate,
                            DropOffDate = order.FirstOrDefault().DropOffDate,
                            OrderDate = order.FirstOrDefault().OrderDate,
                            PickUpLocation = order.FirstOrDefault().PickUpLocation,
                            DropOffLocation = order.FirstOrDefault().DropOffLocation,
                            NoDaysHired = order.FirstOrDefault().NoDaysHired,
                            AmountPaid = order.FirstOrDefault().AmountPaid,
                            XML_Provider = order.FirstOrDefault().XML_Provider,
                            XML_IATANumber = order.FirstOrDefault().XML_IATANumber,
                            XML_RateQualifier = order.FirstOrDefault().XML_RateQualifier
                        };
                        db.ModifiedOrders.Add(orderModify);
                        db.SaveChanges();
                    }

                    Order ModOrder = (from o in db.Orders
                                      where o.ConfirmationID == bookingResouce.ConfirmationID
                                      select o).SingleOrDefault();

                    ModOrder.PickUpDate = bookingResouce.PickupDateTime;
                    ModOrder.DropOffDate = bookingResouce.DropoffDatetime;
                    ModOrder.OrderDate = DateTime.Now;
                    ModOrder.AmountPaid = Convert.ToDouble(bookingResouce.ReservedVehicle.Price);
                    ModOrder.AmountPaidCurrency = bookingResouce.ReservedVehicle.Currency;
                    ModOrder.CarBooked = bookingResouce.ReservedVehicle.Name;
                    ModOrder.Modified = true;
                    ModOrder.ModifiedDate = DateTime.Now;
                    ModOrder.CorpDiscountNo = bookingResouce.CorpDiscountNo;

                    db.SaveChanges();

                    if (bookingResouce.IsExtraValid == true)
                    {
                        int orderID = ModOrder.OrderID;

                        try
                        {
                            XmlNode PriceNode = xmlDocument.SelectSingleNode("//a1:PricedEquips", namespaceManager);
                            if (PriceNode != null)
                            {
                                XmlNodeList PriceNodeList = PriceNode.SelectNodes("//a1:PricedEquip", namespaceManager);
                                foreach (XmlNode accessory in PriceNodeList)
                                {
                                    if (accessory != null)
                                    {
                                        XmlNode EquipmentNode = accessory.SelectSingleNode(".//a1:Equipment", namespaceManager);
                                        XmlNode ChargeNode = accessory.SelectSingleNode(".//a1:Charge", namespaceManager);

                                        string accessoryType = EquipmentNode.Attributes["EquipType"].Value.Trim();
                                        double price = Convert.ToDouble(ChargeNode.Attributes["Amount"].Value.Trim());
                                        int quantity = Convert.ToInt32(EquipmentNode.Attributes["Quantity"].Value.Trim());
                                        string currency = ChargeNode.Attributes["CurrencyCode"].Value.Trim();

                                        AccessoriesOrdered accessoryOrdered = new AccessoriesOrdered
                                        {
                                            OrderIDFK = orderID,
                                            Accessory = accessoryType,
                                            Price = price,
                                            Quantity = quantity,
                                            Currency = currency
                                        };
                                        db.AccessoriesOrdereds.Add(accessoryOrdered);
                                        db.SaveChanges();
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            RemoteDebugger.SendXmlForDebugging(ex.StackTrace, "ErrorDB From Accessories");
                            RemoteDebugger.SendXmlForDebugging(ex.Message, "ErrorDBMessage From Accessories");
                        }
                    }
                }
            }
        }

        //-------------------------------------------------------------------------------------------------------

        //Cancel Methods

        private void addSourceNode(VehicleCancelRQResources resources)
        {
            string pickupLocationCode = resources.PickupLocationCode;

            if (resources.BrandName.ToString().ToUpper() == Brands.DOLLAR.ToString())
            {

                XmlNode POSNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:POS", namespaceManager);
                if (POSNode != null)
                {
                    XmlElement SoceNode = xmlDocument.CreateElement("Source", Configs.XML_NAMESPACE);
                    POSNode.AppendChild(SoceNode);

                    XmlElement ReqId = xmlDocument.CreateElement("RequestorID", Configs.XML_NAMESPACE);
                    ReqId.SetAttribute("Type", "8");
                    ReqId.SetAttribute("ID", "ZR");
                    SoceNode.AppendChild(ReqId);
                }
            }
            else if (resources.BrandName.ToString().ToUpper() == Brands.THRIFTY.ToString())
            {
                if (resources.AgentDutyCode == "3C1A20T8H25")
                {
                    XmlNode POSNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:POS", namespaceManager);
                    if (POSNode != null)
                    {
                        var tcList = from t in db.TC4R_List
                                     join c in db.Countries on t.CountryIDFK equals c.CountryID
                                     join l in db.Locations on c.CountryID equals l.CountryIDFK
                                     where (l.LocationCode == pickupLocationCode)
                                     select new
                                     {
                                         t.CountryIDFK,
                                         c.CountryName
                                     };
                        if (tcList.Count() > 0)
                        {

                        }
                        else
                        {
                            XmlElement SoceNode = xmlDocument.CreateElement("Source", Configs.XML_NAMESPACE);
                            POSNode.AppendChild(SoceNode);

                            XmlElement ReqId = xmlDocument.CreateElement("RequestorID", Configs.XML_NAMESPACE);
                            ReqId.SetAttribute("Type", "8");
                            ReqId.SetAttribute("ID", "ZT");
                            SoceNode.AppendChild(ReqId);
                        }
                    }
                }
            }
        }

        private void setCancelRequestData(VehicleCancelRQResources bookingResouce)
        {
            XmlNode confirmationId = xmlDocument.DocumentElement.SelectSingleNode("//a1:UniqueID", namespaceManager);
            if (confirmationId != null)
            {
                bookingResouce.ConfirmationID = confirmationId.Attributes["ID"].Value.Trim();


                var order = (from o in db.Orders
                             where (o.ConfirmationID == bookingResouce.ConfirmationID)
                             select new
                             {
                                 o.OrderID,
                                 o.CustomerName,
                                 o.CustomerEmail,
                                 o.EmailConfirmed,
                                 o.CarBooked,
                                 o.VehicleType,
                                 o.CountryOfResidence,
                                 o.PickUpDate,
                                 o.DropOffDate,
                                 o.OrderDate,
                                 o.PickUpLocation,
                                 o.DropOffLocation,
                                 o.NoDaysHired,
                                 o.AmountPaid,
                                 o.XML_Provider,
                                 o.XML_IATANumber,
                                 o.XML_RateQualifier
                             });

                if (order.Count() > 0)
                {
                    CanclledOrder orderCancel = new CanclledOrder
                    {
                        OrderID = order.FirstOrDefault().OrderID,
                        ConfirmationID = bookingResouce.ConfirmationID,
                        CustomerName = order.FirstOrDefault().CustomerName,
                        CustomerEmail = order.FirstOrDefault().CustomerEmail,
                        EmailConfirmed = order.FirstOrDefault().EmailConfirmed,
                        CarBooked = order.FirstOrDefault().CarBooked,
                        VehicleType = order.FirstOrDefault().VehicleType,
                        CountryOfResidence = order.FirstOrDefault().CountryOfResidence,
                        PickUpDate = order.FirstOrDefault().PickUpDate,
                        DropOffDate = order.FirstOrDefault().DropOffDate,
                        OrderDate = order.FirstOrDefault().OrderDate,
                        PickUpLocation = order.FirstOrDefault().PickUpLocation,
                        DropOffLocation = order.FirstOrDefault().DropOffLocation,
                        NoDaysHired = order.FirstOrDefault().NoDaysHired,
                        AmountPaid = order.FirstOrDefault().AmountPaid,
                        XML_Provider = order.FirstOrDefault().XML_Provider,
                        XML_IATANumber = order.FirstOrDefault().XML_IATANumber,
                        XML_RateQualifier = order.FirstOrDefault().XML_RateQualifier
                    };
                    db.CanclledOrders.Add(orderCancel);
                    db.SaveChanges();
                }

                var dbCstInfo = db.Orders
                .Where(w => w.ConfirmationID == bookingResouce.ConfirmationID);

                if (dbCstInfo != null)
                {
                    foreach (var cancel in dbCstInfo)
                    {
                        cancel.CancelledBooking = true;
                    }
                    db.SaveChanges();
                }
            }
        }

        //-------------------------------------------------------------------------------------------------------

        private void removeCustLoyaltyNodes(string brand)
        {
            if (brand.ToUpper() != Brands.HERTZ.ToString())
            {
                if (xmlDocument.DocumentElement.SelectNodes("//a1:CustLoyalty", namespaceManager) != null)
                {
                    XmlNodeList custLoyaltyNodes = xmlDocument.DocumentElement.SelectNodes("//a1:CustLoyalty", namespaceManager);
                    if (custLoyaltyNodes != null)
                    {
                        foreach (XmlNode xmlElement in custLoyaltyNodes)
                        {
                            XmlNode node = xmlElement.ParentNode;
                            node.RemoveChild(xmlElement);
                        }
                    }
                }
            }
        }

    }
}