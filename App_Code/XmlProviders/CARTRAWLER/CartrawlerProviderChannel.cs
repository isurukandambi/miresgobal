﻿namespace CARTRAWLER
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Xml;
    using System.Xml.Linq;
    using Exceptions;
    using ORM;
    using ProcessResources;
    using TestClient;
    using WebServiceMail;

    public class CartrawlerProviderChannel : GenericProviderChannel
    {
        public override void doProviderSpecificValidationsForRequest(VehicleAvailRateRQResources resources)
        {
            setLocationId(resources);
            setAvailable_Nodes(resources);
            //removeSourceAttribute();
            addSourceAttribute(resources);
        }
        public override void doProviderSpecificValidationsForResponse(VehicleAvailRateRQResources resources)
        {
            //do validation
            if (hasErrorsInAvailableResponse(resources))
            {

            }
            else
            {
                setAvailRS();
                ReverseLocationCode();
                setAvailPrepayTag();
                removeEquiptTag();
                modifyVehicleDetails(resources.PickupLocationCode, resources.BrandId);
                //setMaxVehicles(resources);
            }
        }

        public override void doProviderSpecificValidationsForRequest(VehicleResRQResources resources)
        {
            if (resources.IsCTAvailload)
            {
                setReservationRequest(resources);
                resources.ProviderEndPointUrl = AppSettings.CarTrawlerResXmlEndPoint;
                savePreBookingInDatabase(resources);
                resources.IsCTAvailload = false;
            }
            else
            {
                setLocationsId(resources);
                setBookingRequestData(resources);
                setAccessoriesData(resources);
                setAvailableRequest(resources);
                resources.ProviderEndPointUrl = AppSettings.CarTrawlerXmlEndPoint;
                resources.IsCTAvailload = true;
            }
        }
        public override void doProviderSpecificValidationsForResponse(VehicleResRQResources resources)
        {
            if (resources.IsCTAvailload)
            {
                if (hasErrorsInBookingResponse(resources))
                {
                    resources.IsCTResload = false;
                }
                else
                {
                    ReverseLocationCode(resources);
                    setCarUrlfromID(resources);
                    resources.IsCTResload = true;
                }
            }
            else
            {
                //do validation
                if (hasErrorsInBookingResponse(resources))
                {

                }
                else
                {
                    ReverseLocationCode(resources);
                    setBookingResponseData(resources);
                    setResPrepayTag();
                    modifyVehicleDetailsRes(resources);
                    SendEmail(resources);
                    saveBookingInDatabase(resources);
                    resources.IsCTResload = false;
                }
            }
        }
        public override void doProviderSpecificValidationsForRequest(VehicleRetrivaleRQResources resources)
        {
            //throw new NotImplementedException();
            removeSourceAttribute();
            set_RetrivalNodes(resources);
        }
        public override void doProviderSpecificValidationsForResponse(VehicleRetrivaleRQResources resources)
        {
            //throw new NotImplementedException();
            setdateTime();
            remove_Id_setId(resources);
            ReverseLocationCode();
            modifyVehicleDetailRet(resources);
        }
        public override void doProviderSpecificValidationsForRequest(VehicleResModifyRQResources resources)
        {
            //throw new NotImplementedException();
        }

        public override void doProviderSpecificValidationsForResponse(VehicleResModifyRQResources resources)
        {
            //throw new NotImplementedException();
        }
        public override void doProviderSpecificValidationsForRequest(VehicleCancelRQResources resources)
        {
            //throw new NotImplementedException();
            //removeSourceAttribute();
            set_CancelNode(resources);
        }
        public override void doProviderSpecificValidationsForResponse(VehicleCancelRQResources resources)
        {
            //throw new NotImplementedException();
            setCancelData_toDB(resources);
            ReverseLocationCode();
        }
        public override void doProviderSpecificValidationsForRequest(VehicleRentalRQResources resources)
        {
            //throw new NotImplementedException();
            setLocationId(resources);
            setSourceNode(resources);
        }
        public override void doProviderSpecificValidationsForResponse(VehicleRentalRQResources resources)
        {
            //throw new NotImplementedException();
        }

        //Common Methods

        private void ReverseLocationCode()
        {
            XmlNode picLoc = xmlDocument.DocumentElement.SelectSingleNode("//a1:PickUpLocation", namespaceManager);
            XmlNode dropLoc = xmlDocument.DocumentElement.SelectSingleNode("//a1:ReturnLocation", namespaceManager);

            if (picLoc != null && dropLoc != null)
            {

                int pic = Convert.ToInt32(picLoc.Attributes["LocationCode"].Value);
                int drop = Convert.ToInt32(dropLoc.Attributes["LocationCode"].Value);

                var PlocationId = (from l in db.Locations
                                   where l.CTLocationId == pic
                                   select new
                                   {
                                       l.LocationCode
                                   });

                if (PlocationId.FirstOrDefault() != null)
                {
                    picLoc.Attributes["LocationCode"].Value = PlocationId.FirstOrDefault().LocationCode.ToString();
                    picLoc.Attributes["CodeContext"].Value = "IATA";
                }
                else
                {
                    throw new InvalidRequestException("PickUp Locations not Identified");
                }

                var DlocationId = (from l in db.Locations
                                   where l.CTLocationId == drop
                                   select new
                                   {
                                       l.LocationCode
                                   });

                if (DlocationId.FirstOrDefault() != null)
                {
                    dropLoc.Attributes["LocationCode"].Value = DlocationId.FirstOrDefault().LocationCode.ToString();
                    dropLoc.Attributes["CodeContext"].Value = "IATA";
                }
                else
                {
                    throw new InvalidRequestException("DropOff Locations not Identified");
                }
            }
        }

        //------------------------------------------------------------------------------------------------------

        //Available Methods

        private void setLocationId(VehicleAvailRateRQResources resources)
        {
            XmlNode picLoc = xmlDocument.DocumentElement.SelectSingleNode("//a1:PickUpLocation", namespaceManager);
            XmlNode dropLoc = xmlDocument.DocumentElement.SelectSingleNode("//a1:ReturnLocation", namespaceManager);

            //pickup Location
            var PlocationId = (from l in db.Locations
                               where l.LocationCode == resources.PickupLocationCode
                               select new
                               {
                                   l.CTLocationId
                               });

            if (PlocationId.Count() > 0)
            {
                int i = 0;
                foreach (var ctLoc in PlocationId)
                {
                    if (ctLoc.CTLocationId != null || ctLoc.CTLocationId == 0)
                    {
                        if (i == 0)
                        {
                            picLoc.Attributes["LocationCode"].Value = ctLoc.CTLocationId.ToString();
                            picLoc.Attributes["CodeContext"].Value = "CARTRAWLER";
                        }
                        i++;
                    }
                    else
                    {
                        picLoc.Attributes["CodeContext"].Value = "CARTRAWLER";
                    }
                }
            }
            else
            {
                throw new InvalidRequestException("PickUp Location not Identified");
            }

            //drop location
            var DlocationId = (from l in db.Locations
                               where l.LocationCode == resources.DropoffLocationCode
                               select new
                               {
                                   l.CTLocationId
                               });

            if (DlocationId.Count() > 0)
            {
                int i = 0;
                foreach (var ctLoc in DlocationId)
                {
                    if (ctLoc.CTLocationId != null || ctLoc.CTLocationId == 0)
                    {
                        if (i == 0)
                        {
                            dropLoc.Attributes["LocationCode"].Value = ctLoc.CTLocationId.ToString();
                            dropLoc.Attributes["CodeContext"].Value = "CARTRAWLER";
                        }
                        i++;
                    }
                    else
                    {
                        picLoc.Attributes["CodeContext"].Value = "CARTRAWLER";
                    }
                }
            }
            else
            {
                throw new InvalidRequestException("DropOff Location not Identified");
            }
        }

        private void setAvailable_Nodes(VehicleAvailRateRQResources resources)
        {
            XmlNode OTA = xmlDocument.DocumentElement.SelectSingleNode("//a1:OTA_VehAvailRateRQ", namespaceManager);
            if (OTA != null)
            {
                OTA.Attributes.RemoveNamedItem("MaxResponses");
                OTA.Attributes.RemoveNamedItem("version");
                XmlAttribute Tearget = xmlDocument.CreateAttribute("Target");
                XmlAttribute version = xmlDocument.CreateAttribute("Version");
                OTA.Attributes.Append(Tearget);
                OTA.Attributes.Append(version);
                OTA.Attributes["Target"].Value = "Production";
                OTA.Attributes["Version"].Value = "1.005";
            }

            XmlNode POSNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:POS", namespaceManager);

            XmlNode pos = xmlDocument.DocumentElement.SelectSingleNode("//a1:Source", namespaceManager);
            if (pos != null)
            {
                resources.SourceCountryCode = pos.Attributes["ISOCountry"].Value;
                pos.ParentNode.RemoveChild(pos);
            }
            XmlNode pos2 = xmlDocument.DocumentElement.SelectSingleNode("//a1:Source", namespaceManager);
            if (pos2 != null)
            {
                pos2.ParentNode.RemoveChild(pos2);
            }

            XmlElement SoceNode = xmlDocument.CreateElement("Source", Configs.XML_NAMESPACE);
            POSNode.AppendChild(SoceNode);

            XmlElement ReqId = xmlDocument.CreateElement("RequestorID", Configs.XML_NAMESPACE);
            ReqId.SetAttribute("Type", "16");
            ReqId.SetAttribute("ID", "621233");
            ReqId.SetAttribute("ID_Context", "CARTRAWLER");
            SoceNode.AppendChild(ReqId);

            XmlNode Available = xmlDocument.DocumentElement.SelectSingleNode("//a1:VehAvailRQCore", namespaceManager);
            Available.Attributes["Status"].Value = "Available";

            XmlElement DriveAge = xmlDocument.CreateElement("DriverType", Configs.XML_NAMESPACE);
            DriveAge.SetAttribute("Age", "30");
            Available.AppendChild(DriveAge);

            XmlNode Extras = xmlDocument.DocumentElement.SelectSingleNode("//a1:SpecialEquipPrefs", namespaceManager);
            if (Extras != null)
            {
                Extras.ParentNode.RemoveChild(Extras);
            }

            XmlNode TPANode = xmlDocument.DocumentElement.SelectSingleNode("//a1:TPA_Extensions", namespaceManager);
            if (TPANode != null)
            {
                resources.ConsumerIP = TPANode.SelectSingleNode("//a1:ConsumerIP", namespaceManager).InnerText;
                TPANode.ParentNode.RemoveChild(TPANode);
            }

            XmlNode RateQualifier = xmlDocument.DocumentElement.SelectSingleNode("//a1:RateQualifier", namespaceManager);
            if (RateQualifier != null)
            {
                RateQualifier.ParentNode.RemoveChild(RateQualifier);
            }

            //XmlNode ArrivalNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:ArrivalDetails", namespaceManager);
            //if (ArrivalNode != null)
            //{
            //    ArrivalNode.ParentNode.RemoveChild(ArrivalNode);
            //}

            XmlNode AvailableInfo = xmlDocument.DocumentElement.SelectSingleNode("//a1:VehAvailRQInfo", namespaceManager);
            if (AvailableInfo != null)
            {
                XmlNode CustomerNode = AvailableInfo.SelectSingleNode("//a1:Customer", namespaceManager);
                if (CustomerNode != null)
                {
                    XmlNode PrimaryNode = AvailableInfo.SelectSingleNode("//a1:Primary", namespaceManager);
                    if (PrimaryNode != null)
                    {
                        XmlElement sourceCountry = xmlDocument.CreateElement("CitizenCountryName", Configs.XML_NAMESPACE);
                        sourceCountry.SetAttribute("Code", resources.SourceCountryCode);
                        PrimaryNode.AppendChild(sourceCountry);

                        if (PrimaryNode.Attributes["BirthDate"] != null)
                        {
                            XmlNode Driver = xmlDocument.DocumentElement.SelectSingleNode("//a1:DriverType", namespaceManager);
                            // process age
                            DriveAge.Attributes["Age"].Value = setAge(PrimaryNode.Attributes["BirthDate"].Value.ToString());
                            //setAge(PrimaryNode.Attributes["BirthDate"].Value.ToString());
                        }
                    }
                }
                else
                {
                    XmlElement customer = xmlDocument.CreateElement("Customer", Configs.XML_NAMESPACE);
                    XmlElement primary = xmlDocument.CreateElement("Primary", Configs.XML_NAMESPACE);
                    XmlElement sourceCountry = xmlDocument.CreateElement("CitizenCountryName", Configs.XML_NAMESPACE);
                    sourceCountry.SetAttribute("Code", resources.SourceCountryCode);
                    primary.AppendChild(sourceCountry);
                    customer.AppendChild(primary);
                    AvailableInfo.AppendChild(customer);
                }

                XmlElement TPA = xmlDocument.CreateElement("TPA_Extensions", Configs.XML_NAMESPACE);
                XmlElement IP = xmlDocument.CreateElement("ConsumerIP", Configs.XML_NAMESPACE);

                //IP.InnerText = "182.456.432.123";
                IP.InnerText = resources.ConsumerIP;
                AvailableInfo.AppendChild(TPA);
                TPA.AppendChild(IP);
            }
            else
            {
                XmlElement info = xmlDocument.CreateElement("VehAvailRQInfo", Configs.XML_NAMESPACE);

                XmlElement customer = xmlDocument.CreateElement("Customer", Configs.XML_NAMESPACE);
                XmlElement primary = xmlDocument.CreateElement("Primary", Configs.XML_NAMESPACE);
                XmlElement sourceCountry = xmlDocument.CreateElement("CitizenCountryName", Configs.XML_NAMESPACE);
                sourceCountry.SetAttribute("Code", resources.SourceCountryCode);
                primary.AppendChild(sourceCountry);
                customer.AppendChild(primary);
                info.AppendChild(customer);

                XmlElement TPA = xmlDocument.CreateElement("TPA_Extensions", Configs.XML_NAMESPACE);
                XmlElement IP = xmlDocument.CreateElement("ConsumerIP", Configs.XML_NAMESPACE);

                IP.InnerText = "86.47.96.55";
                info.AppendChild(TPA);
                TPA.AppendChild(IP);
                OTA.AppendChild(info);
            }
        }

        private string setAge(string dob)
        {
            DateTime bday = Convert.ToDateTime(dob);
            DateTime PresentYear = DateTime.Today;
            TimeSpan ts = PresentYear - bday;
            int Age = ts.Days / 365;

            return Age.ToString();
        }

        private void addSourceAttribute(VehicleAvailRateRQResources resources)
        {
            XmlNode addSource = xmlDocument.DocumentElement.SelectSingleNode("//a1:Source", namespaceManager);
            if (addSource != null)
            {
                if (addSource.Attributes["ISOCurrency"] == null)
                {
                    XmlAttribute currency = xmlDocument.CreateAttribute("ISOCurrency");
                    addSource.Attributes.Append(currency);
                }

                XmlNode addSourceAtt = xmlDocument.DocumentElement.SelectSingleNode("//a1:Source", namespaceManager);
                if (addSource != null)
                {
                    //addSourceAtt.Attributes["ISOCurrency"].Value = "EUR";
                    addSourceAtt.Attributes["ISOCurrency"].Value = setAvailCurrencyCode(resources);
                }
            }
        }

        private string setAvailCurrencyCode(VehicleAvailRateRQResources resources)
        {
            var currency = (from cl in db.CountryLanguages
                            join c in db.Countries on cl.CountryIDFK equals c.CountryID
                            where c.CountryCode == resources.SourceCountryCode
                            select new
                            {
                                cl.Currency_Code
                            });
            if (currency.Count() > 0)
            {
                if (currency.FirstOrDefault().Currency_Code != null)
                {
                    return currency.FirstOrDefault().Currency_Code;
                }
                else
                {
                    return "EUR";
                }
            }
            else
            {
                return "EUR";
            }
        }

        private bool hasErrorsInAvailableResponse(VehicleAvailRateRQResources resources)
        {
            var errorRS = xmlDocument.SelectNodes("//OTA_ErrorRS");
            if (errorRS != null)
            {
                foreach (XmlNode error in errorRS)
                {
                    if (error.Attributes != null)
                    {
                        return true;
                    }
                }
            }
            else
            {
                XmlNode error = xmlDocument.DocumentElement.SelectSingleNode("//a1:Errors", namespaceManager);
                if (error != null)
                {
                    return true;
                }
            }
            return false;
        }

        private void setAvailRS()
        {
            XmlNodeList nodeList = xmlDocument.DocumentElement.SelectNodes("//a1:VehAvail", namespaceManager);
            foreach (XmlNode node in nodeList)
            {
                XmlNode fees = node.SelectSingleNode(".//a1:Fees", namespaceManager);
                foreach (XmlNode feeNode in fees)
                {
                    if (feeNode.Attributes["Description"] == null)
                    {
                        XmlAttribute discription = xmlDocument.CreateAttribute("Description");
                        feeNode.Attributes.Append(discription);
                        if (feeNode.Attributes["Purpose"].Value == "6")
                        {
                            feeNode.Attributes["Description"].Value = "CarTrawler Booking Fee";
                        }
                        if (feeNode.Attributes["Purpose"].Value == "22")
                        {
                            feeNode.Attributes["Description"].Value = "Deposit (includes CT Booking Fee)";
                        }
                        if (feeNode.Attributes["Purpose"].Value == "23")
                        {
                            feeNode.Attributes["Description"].Value = "Pay on Arrival Amount";
                        }
                    }
                }

                XmlNode RateDis = node.SelectSingleNode(".//a1:RateDistance", namespaceManager);
                if (RateDis != null)
                {
                    XmlAttribute ratedis = xmlDocument.CreateAttribute("DistUnitName");
                    RateDis.Attributes.Append(ratedis);
                    RateDis.Attributes["DistUnitName"].Value = "Km/Mile";
                }
            }
        }

        private void setAvailPrepayTag()
        {
            XmlNodeList vehNode = xmlDocument.DocumentElement.SelectNodes("//a1:VehAvail", namespaceManager);
            if (vehNode != null)
            {
                foreach (XmlNode node in vehNode)
                {
                    try
                    {
                        XmlNode charge = node.SelectSingleNode(".//a1:TotalCharge", namespaceManager);
                        string amount = charge.Attributes["EstimatedTotalAmount"].Value;
                        string currency = charge.Attributes["CurrencyCode"].Value;
                        XmlNode vehInfo = node.SelectSingleNode(".//a1:VehAvailInfo", namespaceManager);
                        XmlElement paymentRules = xmlDocument.CreateElement("PaymentRules", Configs.XML_NAMESPACE);
                        XmlElement paymentRule = xmlDocument.CreateElement("PaymentRule", Configs.XML_NAMESPACE);
                        paymentRule.SetAttribute("RuleType", "2");
                        paymentRule.SetAttribute("Amount", amount);
                        paymentRule.SetAttribute("CurrencyCode", currency);
                        paymentRules.AppendChild(paymentRule);
                        vehInfo.AppendChild(paymentRules);
                    }
                    catch { }
                }
            }
        }

        private void removeEquiptTag()
        {
            XmlNodeList vehNode = xmlDocument.DocumentElement.SelectNodes("//a1:VehAvail", namespaceManager);
            if (vehNode != null)
            {
                foreach (XmlNode node in vehNode)
                {
                    XmlNode price = node.SelectSingleNode("//a1:PricedEquips", namespaceManager);
                    if (price != null)
                    {
                        price.ParentNode.RemoveChild(price);
                    }
                    XmlNodeList TPA = node.SelectNodes("//a1:TPA_Extensions", namespaceManager);
                    if (TPA != null)
                    {
                        foreach (XmlNode tpanode in TPA)
                        {
                            tpanode.ParentNode.RemoveChild(tpanode);
                        }
                    }
                }
            }
        }

        private void modifyVehicleDetails(string locationCode, int brandId)
        {
            XmlNodeList vehicleListNode = xmlDocument.DocumentElement.SelectNodes("//a1:VehAvail", namespaceManager);
            if (vehicleListNode.Count > 0)
            {
                foreach (XmlNode vehicleNode in vehicleListNode)
                {
                    string sippCode = vehicleNode.SelectSingleNode(".//a1:Vehicle", namespaceManager).Attributes["Code"].Value.ToString().Trim();
                    var vehicleDetails = (from l in db.Locations
                                          join c in db.Countries on l.CountryIDFK equals c.CountryID
                                          join vr in db.VehicleResources on c.CountryID equals vr.CountryIDFK
                                          join vdb in db.VehicleDetailsByBrands on vr.VDBIDFK equals vdb.VDBID
                                          join b in db.Brands on vdb.BrandIDFK equals b.BrandID
                                          join cg in db.CarGroups on vdb.CarGroupIDFK equals cg.CarGroupID
                                          where b.BrandID == brandId
                                          && l.LocationCode == locationCode
                                          && cg.CarCode == sippCode
                                          select new
                                          {
                                              vdb.CarDescription,
                                              vdb.CarImagePath,
                                              c.CountryCode
                                          });
                    XmlNode vehicleMakeModelNode = vehicleNode.SelectSingleNode(".//a1:VehMakeModel", namespaceManager);
                    XmlNode pictureUrlNode = vehicleNode.SelectSingleNode(".//a1:PictureURL", namespaceManager);
                    if (vehicleDetails.Count() > 0)
                    {
                        vehicleMakeModelNode.Attributes["Name"].Value = vehicleDetails.FirstOrDefault().CarDescription;
                        pictureUrlNode.InnerText = vehicleDetails.FirstOrDefault().CarImagePath;
                    }
                    string vehicleName = vehicleMakeModelNode.Attributes["Name"].Value;
                    int indexOfSimilarText = vehicleName.IndexOf("OR SIMILAR");
                    if (indexOfSimilarText != -1)
                    {
                        vehicleMakeModelNode.Attributes["Name"].Value = vehicleName.Substring(0, indexOfSimilarText);
                    }
                }
            }
        }

        //------------------------------------------------------------------------------------------------------

        //Reservation Methods

        private void setLocationsId(VehicleResRQResources resources)
        {
            string pick;
            string drop;

            XmlNode picLoc = xmlDocument.DocumentElement.SelectSingleNode("//a1:PickUpLocation", namespaceManager);
            XmlNode dropLoc = xmlDocument.DocumentElement.SelectSingleNode("//a1:ReturnLocation", namespaceManager);

            pick = picLoc.Attributes["LocationCode"].Value;
            drop = dropLoc.Attributes["LocationCode"].Value;

            var picklocationId = (from l in db.Locations
                                  where l.LocationCode == pick
                                  select new
                                  {
                                      l.CTLocationId
                                  });

            var droplocationId = (from l in db.Locations
                                  where l.LocationCode == drop
                                  select new
                                  {
                                      l.CTLocationId
                                  });

            if (picklocationId.Count() > 0)
            {
                int i = 0;
                foreach (var ctPicLoc in picklocationId)
                {
                    if (ctPicLoc.CTLocationId != null || ctPicLoc.CTLocationId == 0)
                    {
                        if (i == 0)
                        {
                            picLoc.Attributes["LocationCode"].Value = ctPicLoc.CTLocationId.ToString();
                            picLoc.Attributes["CodeContext"].Value = "CARTRAWLER";
                            resources.PickupLocationCode = ctPicLoc.CTLocationId.ToString();
                        }
                        i++;
                    }
                }
            }
            else
            {
                throw new InvalidRequestException("PickUp and DropOff Locations not Identified");
            }

            if (droplocationId.Count() > 0)
            {
                int i = 0;
                foreach (var ctDropLoc in droplocationId)
                {
                    if (ctDropLoc.CTLocationId != null || ctDropLoc.CTLocationId == 0)
                    {
                        if (i == 0)
                        {
                            dropLoc.Attributes["LocationCode"].Value = ctDropLoc.CTLocationId.ToString();
                            dropLoc.Attributes["CodeContext"].Value = "CARTRAWLER";
                            resources.DropoffLocationCode = ctDropLoc.CTLocationId.ToString();
                        }
                        i++;
                    }
                }
            }
            else
            {
                throw new InvalidRequestException("PickUp and DropOff Locations not Identified");
            }
        }

        private void setBookingRequestData(VehicleResRQResources bookingResouce)
        {
            XmlNode firstNameNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:GivenName", namespaceManager);
            if (firstNameNode != null)
            {
                bookingResouce.CustomerFirstname = firstNameNode.InnerText.Trim();
            }
            XmlNode surNameNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:Surname", namespaceManager);
            if (surNameNode != null)
            {
                bookingResouce.CustomerSurname = surNameNode.InnerText.Trim();
            }
            XmlNode customerEmailNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:Email", namespaceManager);
            if (customerEmailNode != null)
            {
                bookingResouce.CustomerEmail = customerEmailNode.InnerText.Trim();
            }
            XmlNode customerPhoneNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:Telephone", namespaceManager);
            if (customerPhoneNode != null)
            {
                bookingResouce.CustomerPhone = customerPhoneNode.Attributes["PhoneNumber"].Value.Trim();
                bookingResouce.phoneTechType = customerPhoneNode.Attributes["PhoneTechType"].Value.Trim();
                bookingResouce.phoneAreaCode = customerPhoneNode.Attributes["AreaCityCode"].Value.Trim();
            }
            XmlNode customerAddressNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:AddressLine", namespaceManager);
            if (customerAddressNode != null)
            {
                bookingResouce.Address = customerAddressNode.InnerText.Trim();
            }
            XmlNode customerCityNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:CityName", namespaceManager);
            if (customerCityNode != null)
            {
                bookingResouce.CityName = customerCityNode.InnerText.Trim();
            }
            XmlNode customerPostalCodeNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:PostalCode", namespaceManager);
            if (customerPostalCodeNode != null)
            {
                bookingResouce.PostalCode = customerPostalCodeNode.InnerText.Trim();
            }
            XmlNode customerCountryNameNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:CountryName", namespaceManager);
            if (customerCountryNameNode != null)
            {
                bookingResouce.CountryOfResidenceCode = customerCountryNameNode.Attributes["Code"].Value.Trim();
            }
            //setBookingLocationCountryDetails(bookingResouce.PickupLocationCode, true, bookingResouce);
            //setBookingLocationCountryDetails(bookingResouce.DropoffLocationCode, false, bookingResouce);

            /////Other Details
            XmlNode arrivalNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:ArrivalDetails", namespaceManager);
            if (arrivalNode != null)
            {
                bookingResouce.TransportCode = arrivalNode.Attributes["TransportationCode"].Value.Trim();
                if (arrivalNode.Attributes["Number"] != null)
                {
                    bookingResouce.TransportNumber = arrivalNode.Attributes["Number"].Value.Trim();
                }
                else
                {
                    bookingResouce.TransportNumber = "1932";
                }
            }
            XmlNode companyNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:OperatingCompany", namespaceManager);
            if (companyNode != null)
            {
                bookingResouce.operatingCompany = companyNode.Attributes["Code"].Value.Trim();
            }
            XmlNode paymentCard = xmlDocument.DocumentElement.SelectSingleNode("//a1:PaymentCard", namespaceManager);
            if (paymentCard != null)
            {
                bookingResouce.cardType = paymentCard.Attributes["CardType"].Value.Trim();
                bookingResouce.cardCode = paymentCard.Attributes["CardCode"].Value.Trim();
                bookingResouce.cardNumber = paymentCard.Attributes["CardNumber"].Value.Trim();
                bookingResouce.CardExpDate = paymentCard.Attributes["ExpireDate"].Value.Trim();
                if (paymentCard.Attributes["CVV"] != null)
                {
                    bookingResouce.CardseriesCode = paymentCard.Attributes["CVV"].Value.Trim();
                }
                else
                {
                    bookingResouce.CardseriesCode = "123";
                }

                if (paymentCard.Attributes["Name"] != null)
                {
                    bookingResouce.Cardholder = paymentCard.Attributes["Name"].Value.Trim();
                }
                else
                {
                    XmlNode surName = xmlDocument.DocumentElement.SelectSingleNode("//a1:Surname", namespaceManager);
                    if (surName != null)
                    {
                        bookingResouce.Cardholder = surName.InnerText.Trim();
                    }
                }
            }
            XmlNode referenceNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:Reference", namespaceManager);
            if (referenceNode != null)
            {
                bookingResouce.refID = referenceNode.Attributes["ID"].Value.Trim();
            }
            XmlNode primaryNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:Primary", namespaceManager);
            if (primaryNode != null)
            {
                bookingResouce.BirthDay = primaryNode.Attributes["BirthDate"].Value.Trim();
            }
            XmlNode custLoyalNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:CustLoyalty", namespaceManager);
            if (custLoyalNode != null)
            {
                bookingResouce.memberId = custLoyalNode.Attributes["MembershipID"].Value.Trim();
                bookingResouce.programId = custLoyalNode.Attributes["ProgramID"].Value.Trim();
                bookingResouce.travelSector = custLoyalNode.Attributes["TravelSector"].Value.Trim();
            }
        }

        private void setAccessoriesData(VehicleResRQResources bookingResouce)
        {
            XmlNode accessoryNode = xmlDocument.SelectSingleNode("//a1:SpecialEquipPrefs", namespaceManager);
            if (accessoryNode != null)
            {
                bookingResouce.CTAccesoriesList = new List<CTAccessory>();
                XmlNodeList accessoryNodeList = accessoryNode.SelectNodes(".//a1:SpecialEquipPref", namespaceManager);
                foreach (XmlNode accessory in accessoryNodeList)
                {
                    if (accessory != null)
                    {
                        if (accessory.Attributes != null)
                        {
                            bookingResouce.IsExtraValid = true;
                            string accessoryType = accessory.Attributes["EquipType"].Value.Trim();
                            int quantity = Convert.ToInt32(accessory.Attributes["Quantity"].Value.Trim());

                            RemoteDebugger.SendXmlForDebugging(accessoryType + "," + quantity, "Extras");

                            CTAccessory extras = new CTAccessory
                            {
                                IsSelected = true,
                                Delete = false,
                                Quantity = quantity,
                                EquipType = accessoryType
                            };
                            bookingResouce.CTAccesoriesList.Add(extras);
                        }
                    }
                }
            }
        }

        private void setAvailableRequest(VehicleResRQResources resources)
        {
            string pickDateTime = resources.PickupDateTime.ToString().TrimEnd().TrimStart();
            pickDateTime = resources.PickupDateTime.ToString("s");

            string dropDateTime = resources.DropoffDatetime.ToString().TrimEnd().TrimStart();
            dropDateTime = resources.DropoffDatetime.ToString("s");

            string Request = //"<?xml version=\"1.0\" encoding=\"UTF-8\"?>"+
                             "<OTA_VehAvailRateRQ xmlns=\"http://www.opentravel.org/OTA/2003/05\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.opentravel.org/OTA/2003/05 OTA_VehAvailRateRQ.xsd\" Target=\"Production\" Version=\"1.005\">" +
                             "<POS>" +
                             "<Source ISOCurrency=\"" + setResCurrencyCode(resources) + "\">" +
                             "<RequestorID Type=\"16\" ID=\"621233\" ID_Context=\"CARTRAWLER\" />" +
                             "</Source>" +
                             "</POS>" +
                             "<VehAvailRQCore Status=\"Available\">" +
                             "<VehRentalCore PickUpDateTime=\"" + pickDateTime + "\" ReturnDateTime=\"" + dropDateTime + "\">" +
                             "<PickUpLocation CodeContext=\"CARTRAWLER\" LocationCode=\"" + resources.PickupLocationCode + "\" />" +
                             "<ReturnLocation CodeContext=\"CARTRAWLER\" LocationCode=\"" + resources.DropoffLocationCode + "\" />" +
                             "</VehRentalCore>" +
                             "<DriverType Age='30'/>" +
                             "</VehAvailRQCore>" +
                             "<VehAvailRQInfo PassengerQty='3'>" +
                             "<Customer>" +
                             "<Primary>" +
                             "<CitizenCountryName Code='" + resources.CountryOfResidenceCode + "' />" +
                             "</Primary>" +
                             "</Customer>" +
                             "<TPA_Extensions>" +
                             "<ConsumerIP>86.47.96.55</ConsumerIP>" +
                             "</TPA_Extensions>" +
                             "</VehAvailRQInfo>" +
                             "</OTA_VehAvailRateRQ>";


            xmlDocument.LoadXml(Request);
        }

        private string setResCurrencyCode(VehicleResRQResources resources)
        {
            var currency = (from cl in db.CountryLanguages
                            join c in db.Countries on cl.CountryIDFK equals c.CountryID
                            where c.CountryCode == resources.SourceCountryCode
                            select new
                            {
                                cl.Currency_Code
                            });
            if (currency.Count() > 0)
            {
                if (currency.FirstOrDefault().Currency_Code != null)
                {
                    return currency.FirstOrDefault().Currency_Code;
                }
                else
                {
                    return "EUR";
                }
            }
            else
            {
                return "EUR";
            }
        }

        private bool hasErrorsInBookingResponse(VehicleResRQResources resources)
        {
            var errorRS = xmlDocument.SelectNodes("//OTA_ErrorRS");
            if (errorRS != null)
            {
                foreach (XmlNode error in errorRS)
                {
                    if (error.Attributes != null)
                    {
                        return true;
                    }
                }
            }
            else
            {
                XmlNode error = xmlDocument.DocumentElement.SelectSingleNode("//a1:Errors", namespaceManager);
                if (error != null)
                {
                    return true;
                }
            }
            return false;
        }

        private void ReverseLocationCode(VehicleResRQResources resources)
        {
            XmlNode picLoc = xmlDocument.DocumentElement.SelectSingleNode("//a1:PickUpLocation", namespaceManager);
            XmlNode dropLoc = xmlDocument.DocumentElement.SelectSingleNode("//a1:ReturnLocation", namespaceManager);

            if (picLoc != null && dropLoc != null)
            {
                int pic = Convert.ToInt32(picLoc.Attributes["LocationCode"].Value);
                int drop = Convert.ToInt32(dropLoc.Attributes["LocationCode"].Value);

                var PlocationId = (from l in db.Locations
                                   where l.CTLocationId == pic
                                   select new
                                   {
                                       l.LocationCode
                                   });

                if (PlocationId.FirstOrDefault() != null)
                {
                    picLoc.Attributes["LocationCode"].Value = PlocationId.FirstOrDefault().LocationCode.ToString();
                    picLoc.Attributes["CodeContext"].Value = "IATA";
                    resources.PickupLocationCode = PlocationId.FirstOrDefault().LocationCode.ToString();
                }
                else
                {
                    throw new InvalidRequestException("PickUp Locations not Identified");
                }

                var DlocationId = (from l in db.Locations
                                   where l.CTLocationId == drop
                                   select new
                                   {
                                       l.LocationCode
                                   });

                if (DlocationId.FirstOrDefault() != null)
                {
                    dropLoc.Attributes["LocationCode"].Value = DlocationId.FirstOrDefault().LocationCode.ToString();
                    dropLoc.Attributes["CodeContext"].Value = "IATA";
                    resources.DropoffLocationCode = DlocationId.FirstOrDefault().LocationCode.ToString();
                }
                else
                {
                    throw new InvalidRequestException("DropOff Locations not Identified");
                }
            }
        }

        private void setCarUrlfromID(VehicleResRQResources resources)
        {
            XmlNodeList vehAvail = xmlDocument.DocumentElement.SelectNodes("//a1:VehAvail", namespaceManager);
            if (vehAvail.Count > 0)
            {
                foreach (XmlNode reference in vehAvail)
                {
                    XmlNode refKey = reference.SelectSingleNode(".//a1:Reference", namespaceManager);
                    if (refKey.Attributes["ID"].Value == resources.refID)
                    {
                        resources.refDateTime = refKey.Attributes["DateTime"].Value;
                        resources.refUrl = refKey.Attributes["URL"].Value;
                    }
                }
            }
        }

        private void setReservationRequest(VehicleResRQResources resources)
        {
            string pick;
            string drop;
            string Accessories = "", ArrivalDet = "", custLoyal = "";

            XmlNode picLoc = xmlDocument.DocumentElement.SelectSingleNode("//a1:PickUpLocation", namespaceManager);
            XmlNode dropLoc = xmlDocument.DocumentElement.SelectSingleNode("//a1:ReturnLocation", namespaceManager);

            pick = picLoc.Attributes["LocationCode"].Value;
            drop = dropLoc.Attributes["LocationCode"].Value;

            string pickDateTime = resources.PickupDateTime.ToString().TrimEnd().TrimStart();
            pickDateTime = resources.PickupDateTime.ToString("s");

            string dropDateTime = resources.DropoffDatetime.ToString().TrimEnd().TrimStart();
            dropDateTime = resources.DropoffDatetime.ToString("s");

            if (resources.IsExtraValid == true)
            {
                Accessories = CarAccessories(resources).ToString();
            }
            if (resources.TransportCode != null && resources.TransportNumber != null)
            {
                ArrivalDet = "<ArrivalDetails TransportationCode=\"" + resources.TransportCode + "\" Number=\"" + resources.TransportNumber + "\">" +
                             "<OperatingCompany>" + resources.operatingCompany + "</OperatingCompany>" +
                             "</ArrivalDetails>";
            }
            if (resources.memberId != null && resources.programId != null && resources.travelSector != null)
            {
                custLoyal = " <CustLoyalty MembershipID=\"" + resources.memberId + "\" ProgramID=\"" + resources.programId + "\" TravelSector=\"" + resources.travelSector + "\"/>";
            }

            string Request = "<OTA_VehResRQ xmlns=\"http://www.opentravel.org/OTA/2003/05\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.opentravel.org/OTA/2003/05 OTA_VehResRQ.xsd\" Target=\"Production\" Version=\"1.005\">" +
                             "<POS>" +
                             "<Source ISOCurrency=\"" + setResCurrencyCode(resources) + "\">" +
                             "<RequestorID Type=\"16\" ID=\"621233\" ID_Context=\"CARTRAWLER\" />" +
                             "</Source>" +
                             "<Source>" +
                             "<RequestorID Type=\"16\" ID=\"621233\" ID_Context=\"ORDERID\" />" +
                             "</Source>" +
                             "</POS>" +
                             "<VehResRQCore Status=\"All\">" +
                             "<VehRentalCore PickUpDateTime=\"" + pickDateTime + "\" ReturnDateTime=\"" + dropDateTime + "\">" +
                             "<PickUpLocation CodeContext=\"CARTRAWLER\" LocationCode=\"" + pick + "\"/>" +
                             "<ReturnLocation CodeContext=\"CARTRAWLER\" LocationCode=\"" + drop + "\"/>" +
                             "</VehRentalCore>" +
                             "<Customer>" +
                             "<Primary>" +
                             "<PersonName>" +
                             "<NamePrefix>Mr.</NamePrefix>" +
                             "<GivenName>" + resources.CustomerFirstname + "</GivenName>" +
                             "<Surname>" + resources.CustomerSurname + "</Surname>" +
                             "</PersonName>" +
                             "<Telephone PhoneTechType=\"" + resources.phoneTechType + "\" AreaCityCode=\"" + resources.phoneAreaCode + "\" PhoneNumber=\"" + resources.CustomerPhone + "\"/>" +
                             "<Email EmailType=\"2\">" + resources.CustomerEmail + "</Email>" +
                             "<Address Type=\"2\">" +
                             "<AddressLine>" + resources.Address + "</AddressLine>" +
                             "<CountryName Code=\"" + resources.CountryOfResidenceCode + "\" />" +
                             "</Address>" +
                             "<CitizenCountryName Code=\"" + resources.CountryOfResidenceCode + "\" />" +
                             custLoyal +
                             "</Primary>" +
                             "</Customer>" +
                             "<DriverType Age=\"29\"/>" +
                             Accessories +
                             "</VehResRQCore>" +
                             "<VehResRQInfo PassengerQty=\"3\">" +
                             ArrivalDet +
                             "<RentalPaymentPref>" +
                             "<PaymentCard CardType=\"" + resources.cardType + "\" CardCode=\"" + resources.cardCode + "\" CardNumber=\"" + resources.cardNumber + "\" ExpireDate=\"" + resources.CardExpDate + "\" SeriesCode=\"" + resources.CardseriesCode + "\">" +
                             "<CardHolderName>" + resources.Cardholder + "</CardHolderName>" +
                             "</PaymentCard>" +
                             "</RentalPaymentPref>" +
                             "<Reference Type=\"16\" ID=\"" + resources.refID + "\" ID_Context=\"CARTRAWLER\" DateTime=\"" + resources.refDateTime + "\" URL=\"" + resources.refUrl + "\"/>" +
                             "<TPA_Extensions>" +
                             "<CompanyName VAT=\"98765\">Cartrawler Ltd</CompanyName>" +
                //"<ConsumerIP>86.47.96.55</ConsumerIP>" +
                             "</TPA_Extensions>" +
                             "</VehResRQInfo>" +
                             "</OTA_VehResRQ>";


            xmlDocument.LoadXml(Request);
        }

        public XElement CarAccessories(VehicleResRQResources bookingResouce)
        {
            XElement ExtraMainNode = null;

            if (bookingResouce.IsExtraValid == true)
            {
                ExtraMainNode = new XElement("SpecialEquipPrefs");
                foreach (var accessory in bookingResouce.CTAccesoriesList.Where(a => a.IsSelected && !a.Delete))
                {
                    var accessoryElement = new XElement("SpecialEquipPref",
                                                             new XAttribute("EquipType", accessory.EquipType),
                                                             new XAttribute("Quantity", accessory.Quantity)
                        );
                    ExtraMainNode.Add(accessoryElement);
                }
            }
            return ExtraMainNode;
        }

        private void savePreBookingInDatabase(VehicleResRQResources resources)
        {
            BokkingRequest preBookingObj = new BokkingRequest
            {
                CustomerName = resources.CustomerFirstname + " " + resources.CustomerSurname,
                CustomerEmail = resources.CustomerEmail,
                CustomerPhone = resources.CustomerPhone,
                PickUpDate = resources.PickupDateTime,
                DropOffDate = resources.DropoffDatetime,
                PickUpLocation = resources.PickupLocationCode,
                DropOffLocation = resources.DropoffLocationCode,
                CustomerSurname = resources.CustomerSurname,
                CustomerFirstname = resources.CustomerFirstname,
                AddressLine1 = resources.Address,
                AddressLine2 = resources.CityName,
                AddressTown = resources.CityName,
                AddressPostCodeZip = resources.PostalCode,
                CountryOfResidence = resources.CountryOfResidenceCode,
                CameFrom = resources.PartnerName,
                XML_Provider = resources.ProviderName,
                OrderDate = DateTime.Now
            };
            db.BokkingRequests.Add(preBookingObj);
            db.SaveChanges();
        }

        private void setBookingResponseData(VehicleResRQResources resources)
        {
            resources.ReservedVehicle = new BookedVehicle();
            XmlNode firstNameNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:GivenName", namespaceManager);
            if (firstNameNode != null)
            {
                resources.CustomerFirstname = firstNameNode.InnerText.Trim();
            }
            XmlNode surNameNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:Surname", namespaceManager);
            if (surNameNode != null)
            {
                resources.CustomerSurname = surNameNode.InnerText.Trim();
            }
            XmlNode confirmationIdNode = xmlDocument.SelectSingleNode("//a1:ConfID", namespaceManager);
            if (confirmationIdNode != null)
            {
                resources.ConfirmationID = confirmationIdNode.Attributes["ID"].Value;
            }
            XmlNode VehModelNode = xmlDocument.SelectSingleNode("//a1:VehMakeModel", namespaceManager);
            if (VehModelNode != null)
            {
                resources.ReservedVehicle.Name = VehModelNode.Attributes["Name"].Value.ToString();
            }
            XmlNode ImageNode = xmlDocument.SelectSingleNode("//a1:PictureURL", namespaceManager);
            if (ImageNode != null)
            {
                resources.ReservedVehicle.ImagePath = ImageNode.InnerText.Trim();
            }
            XmlNode PaymentNode = xmlDocument.SelectSingleNode("//a1:TotalCharge", namespaceManager);
            if (PaymentNode != null)
            {
                resources.ReservedVehicle.Price = decimal.Parse(PaymentNode.Attributes["EstimatedTotalAmount"].Value);
                resources.ReservedVehicle.Currency = PaymentNode.Attributes["CurrencyCode"].Value;
            }
            XmlNode priceIncludeNode = xmlDocument.SelectSingleNode("//a1:TaxAmount", namespaceManager);
            if (priceIncludeNode != null)
            {
                resources.ReservedVehicle.ImagePath = priceIncludeNode.Attributes["Total"].Value;
            }
        }

        private void setResPrepayTag()
        {
            try
            {
                XmlNode charge = xmlDocument.DocumentElement.SelectSingleNode("//a1:TotalCharge", namespaceManager);
                if (charge != null)
                {
                    string amount = charge.Attributes["EstimatedTotalAmount"].Value;
                    string currency = charge.Attributes["CurrencyCode"].Value;
                    XmlNode vehSegInfo = xmlDocument.DocumentElement.SelectSingleNode("//a1:VehSegmentCore", namespaceManager);
                    if (vehSegInfo != null)
                    {
                        XmlElement paymentRules = xmlDocument.CreateElement("PaymentRules", Configs.XML_NAMESPACE);
                        XmlElement paymentRule = xmlDocument.CreateElement("PaymentRule", Configs.XML_NAMESPACE);
                        paymentRule.SetAttribute("RuleType", "2");
                        paymentRule.SetAttribute("Amount", amount);
                        paymentRule.SetAttribute("CurrencyCode", currency);
                        paymentRules.AppendChild(paymentRule);
                        vehSegInfo.AppendChild(paymentRules);
                    }
                }
            }
            catch { }
        }

        private void modifyVehicleDetailsRes(VehicleResRQResources bookingResouce)
        {
            XmlNode vehicle = xmlDocument.DocumentElement.SelectSingleNode("//a1:Vehicle", namespaceManager);
            if (vehicle != null)
            {
                string sippCode = vehicle.Attributes["Code"].Value.ToString().Trim();
                var vehicleDetails = (from l in db.Locations
                                      join c in db.Countries on l.CountryIDFK equals c.CountryID
                                      join vr in db.VehicleResources on c.CountryID equals vr.CountryIDFK
                                      join vdb in db.VehicleDetailsByBrands on vr.VDBIDFK equals vdb.VDBID
                                      join b in db.Brands on vdb.BrandIDFK equals b.BrandID
                                      join cg in db.CarGroups on vdb.CarGroupIDFK equals cg.CarGroupID
                                      where b.BrandID == bookingResouce.BrandId
                                      && l.LocationCode == bookingResouce.PickupLocationCode
                                      && cg.CarCode == sippCode
                                      select new
                                      {
                                          vdb.CarDescription,
                                          vdb.CarImagePath,
                                          c.CountryCode
                                      });
                XmlNode vehicleMakeModelNode = vehicle.SelectSingleNode(".//a1:VehMakeModel", namespaceManager);
                XmlNode pictureUrlNode = vehicle.SelectSingleNode(".//a1:PictureURL", namespaceManager);
                if (vehicleDetails.Count() > 0)
                {
                    vehicleMakeModelNode.Attributes["Name"].Value = vehicleDetails.FirstOrDefault().CarDescription;
                    pictureUrlNode.InnerText = vehicleDetails.FirstOrDefault().CarImagePath;

                    //Remove Special Equipment (Winter Tyres if enabled)
                    //if (AppSettings.IsWiterTyresEnabled)
                    //{
                    //    //setWinterTyresForGermany(vehicleDetails.FirstOrDefault().CountryCode, vehicleNode);
                    //}
                }
                string vehicleName = vehicleMakeModelNode.Attributes["Name"].Value;
                int indexOfSimilarText = vehicleName.IndexOf("OR SIMILAR");
                if (indexOfSimilarText != -1)
                {
                    vehicleMakeModelNode.Attributes["Name"].Value = vehicleName.Substring(0, indexOfSimilarText);
                }
            }
        }

        private void SendEmail(VehicleResRQResources bookingResouce)
        {

            var email = (from p in db.Partners
                         where p.DomainName == bookingResouce.PartnerName
                         select new
                         {
                             p.ConfimationMailSend
                         });

            if (email.First().ConfimationMailSend == 1)
            {

                var selectedBrand = bookingResouce.BrandName;

                try
                {
                    BuildConfirmationEmail confEmailBuilder = new BuildConfirmationEmail(bookingResouce, bookingResouce.ReservedVehicle);
                    //RemoteDebugger.SendXmlForDebugging(bookingResouce.ConfirmationID + "  " + bookingResouce.ReservedVehicle.Currency + "  " + bookingResouce.ReservedVehicle.Price.ToString() + "  " + bookingResouce.ReservedVehicle.ImagePath + "  " + bookingResouce.ReservedVehicle.Name + "  " + bookingResouce.ReservedVehicle.DrpFee + "  " + bookingResouce.ReservedVehicle.priceInclude.ToString() + "  " + bookingResouce.ReservedVehicle.distance, "mail Progress");
                    string emailMessage = confEmailBuilder.BuildEmail(bookingResouce.ConfirmationID, bookingResouce.ReservedVehicle.Currency, bookingResouce.ReservedVehicle.Price.ToString(), bookingResouce.ReservedVehicle.ImagePath, bookingResouce.ReservedVehicle.Name, bookingResouce.ReservedVehicle.DrpFee, bookingResouce.ReservedVehicle.priceInclude.ToString(), bookingResouce.ReservedVehicle.distance, TimeBetweenDates.CalculateMinutes(bookingResouce.PickupDateTime, Convert.ToDateTime(bookingResouce.DropoffDatetime)));
                    string emailSubject = bookingResouce.ModifyProcess ? (string)HttpContext.GetGlobalResourceObject("SiteStrings", "ModifyEmailSubject") : (string)HttpContext.GetGlobalResourceObject("SiteStrings", "BookingEmailSubject");
                    //Send confirmation email

                    bool isSent = SendConfMail.SendMessage(AppSettings.SiteEmailsSentFrom, bookingResouce.CustomerEmail, emailSubject, emailMessage, true);

                    if (!isSent)
                    {
                        string erroremailMessage = CommonClass.GetErrorXML("VehResRS", "1", "099", "Booking Completed Successfully. Error Occured while sending confirmation email. Please Contact Hertz.");
                        string errorSubject = "Error in Booking";

                        SendConfMail.SendMessage(AppSettings.SiteEmailsSentFrom, bookingResouce.CustomerEmail, errorSubject, erroremailMessage, true);
                    }
                }

                catch (Exception ex)
                {
                    RemoteDebugger.SendXmlForDebugging(ex.Message, "Errors");
                    RemoteDebugger.SendXmlForDebugging(ex.StackTrace, "Errors");
                }
            }
        }

        private void saveBookingInDatabase(VehicleResRQResources resources)
        {
            Order BookingObj = new Order
            {
                ConfirmationID = resources.ConfirmationID,
                CustomerName = resources.CustomerFirstname + " " + resources.CustomerSurname ?? "",
                CustomerEmail = resources.CustomerEmail ?? "",
                EmailCode = "",
                EmailConfirmed = 1,
                IncludeInEshot = 1,
                CustomerPhone = resources.CustomerPhone ?? "",
                CarBooked = resources.ReservedVehicle.Name ?? "",
                VehicleType = "",
                CountryOfResidence = resources.CountryOfResidenceCode ?? "",
                PickUpDate = resources.PickupDateTime,
                DropOffDate = resources.DropoffDatetime,
                OrderDate = DateTime.Now,
                PickUpLocation = resources.PickupLocationCode,
                DropOffLocation = resources.DropoffLocationCode ?? "",
                NoDaysHired = 0,
                AmountPaid = Convert.ToDouble(resources.ReservedVehicle.Price),
                AmountPaidCurrency = resources.ReservedVehicle.Currency ?? "",
                CameFrom = resources.PartnerName ?? "",
                LanguageCode = "",
                ReminderEmail = DateTime.Now,
                ThanksEmail = DateTime.Now,
                HertzNum1 = "",
                XML_Provider = resources.ProviderName ?? "",
                XML_Provider_PickupSupplier = resources.BrandName ?? "",
                XML_Provider_DropoffSupplier = resources.BrandName ?? "",
                XML_RateQualifier = resources.ProviderRateQualifier ?? "",
                CustomerSurname = resources.CustomerSurname ?? "",
                CustomerFirstname = resources.CustomerFirstname ?? "",
                AddressLine1 = resources.Address ?? "",
                AddressLine2 = resources.CityName ?? "",
                AddressTown = resources.CityName ?? "",
                AddressPostCodeZip = resources.PostalCode ?? "",
                AddressCountryCode = resources.SourceCountryCode ?? "",
                ThermeonConf_Id = resources.Thermeon_ConfirmationID ?? "",
                CorpDiscountNo = resources.CorpDiscountNo ?? "",
                Customer_BDay = Convert.ToDateTime(resources.BirthDay)
            };
            db.Orders.Add(BookingObj);
            db.SaveChanges();

            int orderID = BookingObj.OrderID;

            try
            {
                if (resources.AccesoriesList != null)
                {
                    XmlNode PriceNode = xmlDocument.SelectSingleNode("//a1:PricedEquips", namespaceManager);
                    if (PriceNode != null)
                    {
                        XmlNodeList PriceNodeList = PriceNode.SelectNodes("//a1:PricedEquip", namespaceManager);
                        foreach (XmlNode accessory in PriceNodeList)
                        {
                            if (accessory != null)
                            {
                                XmlNode EquipmentNode = accessory.SelectSingleNode(".//a1:Equipment", namespaceManager);
                                XmlNode ChargeNode = accessory.SelectSingleNode(".//a1:Charge", namespaceManager);

                                string accessoryType = EquipmentNode.Attributes["EquipType"].Value.Trim();
                                double price = Convert.ToDouble(ChargeNode.Attributes["Amount"].Value.Trim());
                                int quantity = Convert.ToInt32(EquipmentNode.Attributes["Quantity"].Value.Trim());
                                string currency = ChargeNode.Attributes["CurrencyCode"].Value.Trim();

                                AccessoriesOrdered accessoryOrdered = new AccessoriesOrdered
                                {
                                    OrderIDFK = orderID,
                                    Accessory = accessoryType,
                                    Price = price,
                                    Quantity = quantity,
                                    Currency = currency
                                };
                                db.AccessoriesOrdereds.Add(accessoryOrdered);
                                db.SaveChanges();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                RemoteDebugger.SendXmlForDebugging(ex.StackTrace, "ErrorDB");
                RemoteDebugger.SendXmlForDebugging(ex.Message, "ErrorDBMessage");
            }
        }

        //------------------------------------------------------------------------------------------------------

        //Retrival Methods

        private void removeSourceAttribute()
        {
            XmlNodeList sourceNode = xmlDocument.DocumentElement.SelectNodes("//a1:Source", namespaceManager);
            if (sourceNode != null)
            {
                foreach (XmlNode source in sourceNode)
                {
                    source.ParentNode.RemoveChild(source);
                }

                XmlNode pos = xmlDocument.DocumentElement.SelectSingleNode("//a1:POS", namespaceManager);
                if (pos != null)
                {
                    XmlElement sNode = xmlDocument.CreateElement("Source", Configs.XML_NAMESPACE);
                    XmlElement requesterID = xmlDocument.CreateElement("RequestorID", Configs.XML_NAMESPACE);
                    requesterID.SetAttribute("Type", "16");
                    requesterID.SetAttribute("ID", "621233");
                    requesterID.SetAttribute("ID_Context", "CARTRAWLER");
                    sNode.AppendChild(requesterID);
                    pos.AppendChild(sNode);
                }
            }
        }

        private void set_RetrivalNodes(VehicleRetrivaleRQResources resources)
        {
            XmlNode OTA = xmlDocument.DocumentElement.SelectSingleNode("//a1:OTA_VehRetResRQ", namespaceManager);
            if (OTA != null)
            {
                if (OTA.Attributes["version"] != null)
                {
                    OTA.Attributes.RemoveNamedItem("version");
                    XmlAttribute version = xmlDocument.CreateAttribute("Version");
                    OTA.Attributes.Append(version);
                }

                XmlAttribute Tearget = xmlDocument.CreateAttribute("Target");
                OTA.Attributes.Append(Tearget);
                OTA.Attributes["Target"].Value = "Production";
                OTA.Attributes["Version"].Value = "1.002";
            }

            XmlNode UniquId = xmlDocument.DocumentElement.SelectSingleNode("//a1:UniqueID", namespaceManager);
            if (UniquId.Attributes["Type"] != null)
            {
                UniquId.Attributes["Type"].Value = "15";
                resources.ConfirmationID = UniquId.Attributes["ID"].Value;
            }

            XmlNode Name = xmlDocument.DocumentElement.SelectSingleNode("//a1:PersonName", namespaceManager);
            if (Name != null)
            {
                Name.ParentNode.RemoveChild(Name);
            }

        }

        private void setdateTime()
        {
            XmlNode rentalDetailsNode = xmlDocument.SelectSingleNode(".//a1:VehRentalCore ", namespaceManager);
            if (rentalDetailsNode != null)
            {
                string pickdate = rentalDetailsNode.Attributes["PickUpDateTime"].Value;
                string dropdate = rentalDetailsNode.Attributes["ReturnDateTime"].Value;

                rentalDetailsNode.Attributes["PickUpDateTime"].Value = pickdate + "-06:00";
                rentalDetailsNode.Attributes["ReturnDateTime"].Value = dropdate + "-06:00";
            }
        }

        private void remove_Id_setId(VehicleRetrivaleRQResources resources)
        {
            XmlNode reservation = xmlDocument.DocumentElement.SelectSingleNode("//a1:VehReservation", namespaceManager);
            if (reservation != null)
            {
                if (reservation.Attributes["Status"].Value == "Cancelled")
                {
                    throw new InvalidRequestException("The reservation has been cancelled");
                }
                else
                {
                    XmlNode confId1 = xmlDocument.DocumentElement.SelectSingleNode("//a1:ConfID", namespaceManager);
                    if (confId1 != null)
                    {
                        confId1.Attributes["ID"].Value = resources.ConfirmationID;
                    }
                }
            }
        }

        private void modifyVehicleDetailRet(VehicleRetrivaleRQResources resources)
        {
            XmlNode pickup = xmlDocument.DocumentElement.SelectSingleNode("//a1:PickUpLocation", namespaceManager);
            XmlNode dropoff = xmlDocument.DocumentElement.SelectSingleNode("//a1:ReturnLocation", namespaceManager);

            if (pickup != null && dropoff != null)
            {
                resources.PickupLocationCode = pickup.Attributes["LocationCode"].Value;
                resources.DropoffLocationCode = dropoff.Attributes["LocationCode"].Value;

                XmlNode vehicle = xmlDocument.DocumentElement.SelectSingleNode("//a1:Vehicle", namespaceManager);
                if (vehicle != null)
                {
                    string sippCode = vehicle.Attributes["Code"].Value.ToString().Trim();
                    var vehicleDetails = (from l in db.Locations
                                          join c in db.Countries on l.CountryIDFK equals c.CountryID
                                          join vr in db.VehicleResources on c.CountryID equals vr.CountryIDFK
                                          join vdb in db.VehicleDetailsByBrands on vr.VDBIDFK equals vdb.VDBID
                                          join b in db.Brands on vdb.BrandIDFK equals b.BrandID
                                          join cg in db.CarGroups on vdb.CarGroupIDFK equals cg.CarGroupID
                                          where b.BrandName == resources.BrandName
                                          && l.LocationCode == resources.PickupLocationCode
                                          && cg.CarCode == sippCode
                                          select new
                                          {
                                              vdb.CarDescription,
                                              vdb.CarImagePath,
                                              c.CountryCode
                                          });
                    XmlNode vehicleMakeModelNode = vehicle.SelectSingleNode(".//a1:VehMakeModel", namespaceManager);
                    XmlNode pictureUrlNode = vehicle.SelectSingleNode(".//a1:PictureURL", namespaceManager);
                    if (vehicleDetails.Count() > 0)
                    {
                        vehicleMakeModelNode.Attributes["Name"].Value = vehicleDetails.FirstOrDefault().CarDescription;
                        pictureUrlNode.InnerText = vehicleDetails.FirstOrDefault().CarImagePath;
                    }
                    string vehicleName = vehicleMakeModelNode.Attributes["Name"].Value;
                    int indexOfSimilarText = vehicleName.IndexOf("OR SIMILAR");
                    if (indexOfSimilarText != -1)
                    {
                        vehicleMakeModelNode.Attributes["Name"].Value = vehicleName.Substring(0, indexOfSimilarText);
                    }
                }
            }
        }

        //------------------------------------------------------------------------------------------------------

        //Cancel Methods

        private void set_CancelNode(VehicleCancelRQResources resources)
        {
            XmlNode OTA = xmlDocument.DocumentElement.SelectSingleNode("//a1:OTA_VehCancelRQ", namespaceManager);
            if (OTA != null)
            {
                OTA.Attributes.RemoveNamedItem("version");
                XmlAttribute Tearget = xmlDocument.CreateAttribute("Target");
                XmlAttribute version = xmlDocument.CreateAttribute("Version");
                OTA.Attributes.Append(Tearget);
                OTA.Attributes.Append(version);
                OTA.Attributes["Target"].Value = "Production";
                OTA.Attributes["Version"].Value = "1.007";
            }

            XmlNode POSNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:POS", namespaceManager);

            XmlNode pos = xmlDocument.DocumentElement.SelectSingleNode("//a1:Source", namespaceManager);
            if (pos != null)
            {
                pos.ParentNode.RemoveChild(pos);
            }
            XmlNode pos2 = xmlDocument.DocumentElement.SelectSingleNode("//a1:Source", namespaceManager);
            if (pos2 != null)
            {
                pos2.ParentNode.RemoveChild(pos2);
            }

            XmlElement SoceNode = xmlDocument.CreateElement("Source", Configs.XML_NAMESPACE);
            POSNode.AppendChild(SoceNode);

            XmlElement ReqId = xmlDocument.CreateElement("RequestorID", Configs.XML_NAMESPACE);
            ReqId.SetAttribute("Type", "16");
            ReqId.SetAttribute("ID", "621233");
            ReqId.SetAttribute("ID_Context", "CARTRAWLER");
            SoceNode.AppendChild(ReqId);

            XmlNode Available = xmlDocument.DocumentElement.SelectSingleNode("//a1:VehCancelRQCore", namespaceManager);
            XmlAttribute status = xmlDocument.CreateAttribute("CancelType");
            Available.Attributes.Append(status);
            Available.Attributes["CancelType"].Value = "Book";

            XmlNode id = xmlDocument.DocumentElement.SelectSingleNode("//a1:UniqueID", namespaceManager);

            resources.ConfirmationID = id.Attributes["ID"].Value;

            XmlNode ota = xmlDocument.DocumentElement.SelectSingleNode("//a1:OTA_VehCancelRQ", namespaceManager);

            XmlElement info = xmlDocument.CreateElement("VehCancelRQInfo", Configs.XML_NAMESPACE);
            ota.AppendChild(info);

            XmlElement rental = xmlDocument.CreateElement("RentalInfo", Configs.XML_NAMESPACE);
            info.AppendChild(rental);
            XmlAttribute pickDate = xmlDocument.CreateAttribute("PickUpDateTime");
            XmlAttribute dropDate = xmlDocument.CreateAttribute("ReturnDateTime");
            rental.Attributes.Append(pickDate);
            rental.Attributes.Append(dropDate);

            XmlElement pickLoc = xmlDocument.CreateElement("PickUpLocation", Configs.XML_NAMESPACE);
            pickLoc.SetAttribute("CodeContext", "CARTRAWLER");
            rental.AppendChild(pickLoc);

            XmlElement dropLoc = xmlDocument.CreateElement("ReturnLocation", Configs.XML_NAMESPACE);
            dropLoc.SetAttribute("CodeContext", "CARTRAWLER");
            rental.AppendChild(dropLoc);

            XmlElement tpa = xmlDocument.CreateElement("TPA_Extensions", Configs.XML_NAMESPACE);
            info.AppendChild(tpa);

            XmlElement refund = xmlDocument.CreateElement("Refund", Configs.XML_NAMESPACE);
            refund.SetAttribute("Type", "FULL");
            tpa.AppendChild(refund);

            set_locationDB(resources.ConfirmationID);
        }

        private void set_locationDB(string id)
        {
            XmlNode rent = xmlDocument.DocumentElement.SelectSingleNode("//a1:RentalInfo", namespaceManager);
            XmlNode pick = xmlDocument.DocumentElement.SelectSingleNode("//a1:PickUpLocation", namespaceManager);
            XmlNode drop = xmlDocument.DocumentElement.SelectSingleNode("//a1:ReturnLocation", namespaceManager);

            XmlAttribute pickLoc = xmlDocument.CreateAttribute("LocationCode");
            XmlAttribute dropLoc = xmlDocument.CreateAttribute("LocationCode");
            pick.Attributes.Append(pickLoc);
            drop.Attributes.Append(dropLoc);

            var locations = (from o in db.Orders
                             where o.ConfirmationID == id
                             select new
                             {
                                 o.PickUpDate,
                                 o.PickUpLocation,
                                 o.DropOffDate
                             });
            if (locations.FirstOrDefault() != null)
            {
                string LocationCode = locations.FirstOrDefault().PickUpLocation.Trim().ToString();
                var CTlocations = (from l in db.Locations
                                   where l.LocationCode == LocationCode
                                   select new
                                   {
                                       l.CTLocationId
                                   });

                if (CTlocations.Count() > 0)
                {
                    int i = 0;
                    foreach (var ctLoc in CTlocations)
                    {
                        if (ctLoc.CTLocationId != null || ctLoc.CTLocationId == 0)
                        {
                            if (i == 0)
                            {
                                pick.Attributes["LocationCode"].Value = ctLoc.CTLocationId.ToString();
                                drop.Attributes["LocationCode"].Value = ctLoc.CTLocationId.ToString();
                            }
                            i++;
                        }
                    }
                }
                else
                {
                    throw new InvalidRequestException("PickUp and DropOff Locations not Identified");
                }

                rent.Attributes["PickUpDateTime"].Value = Convert.ToDateTime(locations.FirstOrDefault().PickUpDate.ToString()).ToString("s");
                rent.Attributes["ReturnDateTime"].Value = Convert.ToDateTime(locations.FirstOrDefault().DropOffDate.ToString()).ToString("s");
            }
            else
            {
                throw new InvalidRequestException("No data in DB equal Confirmation Id");
            }
        }

        private void setCancelData_toDB(VehicleCancelRQResources bookingResouce)
        {
            try
            {
                var order = (from o in db.Orders
                             where (o.ConfirmationID == bookingResouce.ConfirmationID)
                             select new
                             {
                                 o.OrderID,
                                 o.CustomerName,
                                 o.CustomerEmail,
                                 o.EmailConfirmed,
                                 o.CarBooked,
                                 o.VehicleType,
                                 o.CountryOfResidence,
                                 o.PickUpDate,
                                 o.DropOffDate,
                                 o.OrderDate,
                                 o.PickUpLocation,
                                 o.DropOffLocation,
                                 o.NoDaysHired,
                                 o.AmountPaid,
                                 o.XML_Provider,
                                 o.XML_IATANumber,
                                 o.XML_RateQualifier
                             });

                if (order.Count() > 0)
                {
                    CanclledOrder orderCancel = new CanclledOrder
                    {
                        OrderID = order.FirstOrDefault().OrderID,
                        ConfirmationID = bookingResouce.ConfirmationID,
                        CustomerName = order.FirstOrDefault().CustomerName,
                        CustomerEmail = order.FirstOrDefault().CustomerEmail,
                        EmailConfirmed = order.FirstOrDefault().EmailConfirmed,
                        CarBooked = order.FirstOrDefault().CarBooked,
                        VehicleType = order.FirstOrDefault().VehicleType,
                        CountryOfResidence = order.FirstOrDefault().CountryOfResidence,
                        PickUpDate = order.FirstOrDefault().PickUpDate,
                        DropOffDate = order.FirstOrDefault().DropOffDate,
                        OrderDate = order.FirstOrDefault().OrderDate,
                        PickUpLocation = order.FirstOrDefault().PickUpLocation,
                        DropOffLocation = order.FirstOrDefault().DropOffLocation,
                        NoDaysHired = order.FirstOrDefault().NoDaysHired,
                        AmountPaid = order.FirstOrDefault().AmountPaid,
                        XML_Provider = order.FirstOrDefault().XML_Provider,
                        XML_IATANumber = order.FirstOrDefault().XML_IATANumber,
                        XML_RateQualifier = order.FirstOrDefault().XML_RateQualifier
                    };
                    db.CanclledOrders.Add(orderCancel);
                    db.SaveChanges();
                }

                var dbCstInfo = db.Orders
                .Where(w => w.ConfirmationID == bookingResouce.ConfirmationID);

                if (dbCstInfo != null)
                {
                    foreach (var cancel in dbCstInfo)
                    {
                        cancel.CancelledBooking = true;
                    }
                    db.SaveChanges();
                }
            }
            catch (Exception e)
            {
                RemoteDebugger.SendXmlForDebugging(e.Message, "Error Message");
                RemoteDebugger.SendXmlForDebugging(e.StackTrace, "Error StackRace");
            }
        }

        //------------------------------------------------------------------------------------------------------

        // Rental Terms Methods

        private void setLocationId(VehicleRentalRQResources resources)
        {
            XmlNode picLoc = xmlDocument.DocumentElement.SelectSingleNode("//a1:PickUpLocation", namespaceManager);
            XmlNode dropLoc = xmlDocument.DocumentElement.SelectSingleNode("//a1:ReturnLocation", namespaceManager);

            //pickup location
            var PlocationId = (from l in db.Locations
                               where l.LocationCode == resources.PickupLocationCode
                               select new
                               {
                                   l.CTLocationId
                               });
            if (PlocationId.Count() > 0)
            {
                int i = 0;
                foreach (var ctLoc in PlocationId)
                {
                    if (ctLoc.CTLocationId != null || ctLoc.CTLocationId == 0)
                    {
                        if (i == 0)
                        {
                            picLoc.Attributes["LocationCode"].Value = ctLoc.CTLocationId.ToString();
                        }
                        i++;
                    }
                }
            }
            else
            {
                throw new InvalidRequestException("PickUp Location not Identified");
            }

            //drop location
            var DlocationId = (from l in db.Locations
                               where l.LocationCode == resources.DropoffLocationCode
                               select new
                               {
                                   l.CTLocationId
                               });
            if (DlocationId.Count() > 0)
            {
                int i = 0;
                foreach (var ctLoc in DlocationId)
                {
                    if (ctLoc.CTLocationId != null || ctLoc.CTLocationId == 0)
                    {
                        if (i == 0)
                        {
                            dropLoc.Attributes["LocationCode"].Value = ctLoc.CTLocationId.ToString();
                        }
                        i++;
                    }
                }
            }
            else
            {
                throw new InvalidRequestException("PickUp and DropOff Locations not Identified");
            }
        }

        private void setSourceNode(VehicleRentalRQResources resources)
        {
            XmlNode OTA = xmlDocument.DocumentElement.SelectSingleNode("//a1:CT_RentalConditionsRQ", namespaceManager);
            if (OTA != null)
            {
                OTA.Attributes.RemoveNamedItem("version");
                XmlAttribute Tearget = xmlDocument.CreateAttribute("Target");
                XmlAttribute version = xmlDocument.CreateAttribute("Version");
                OTA.Attributes.Append(Tearget);
                OTA.Attributes.Append(version);
                OTA.Attributes["Target"].Value = "Production";
                OTA.Attributes["Version"].Value = "1.000";
                OTA.Attributes["xsi:schemaLocation"].Value = "http://www.opentravel.org/OTA/2003/05 OTA_VehResRQ.xsd";
            }

            XmlNode pos = xmlDocument.DocumentElement.SelectSingleNode("//a1:POS", namespaceManager);
            if (pos != null)
            {
                XmlNodeList source = pos.SelectNodes(".//a1:Source", namespaceManager);
                if (source != null)
                {
                    foreach (XmlNode srce in source)
                    {
                        srce.ParentNode.RemoveChild(srce);
                    }
                }

                XmlElement SoceNode = xmlDocument.CreateElement("Source", Configs.XML_NAMESPACE);
                pos.AppendChild(SoceNode);

                XmlElement ReqId = xmlDocument.CreateElement("RequestorID", Configs.XML_NAMESPACE);
                ReqId.SetAttribute("Type", "16");
                ReqId.SetAttribute("ID", "621233");
                ReqId.SetAttribute("ID_Context", "CARTRAWLER");
                SoceNode.AppendChild(ReqId);

            }

            XmlNode rateQualifier = xmlDocument.DocumentElement.SelectSingleNode("//a1:RateQualifier", namespaceManager);
            if (rateQualifier != null)
            {
                rateQualifier.ParentNode.RemoveChild(rateQualifier);
            }
        }

        //---------------------------------------------------------------------------------------------------------

        private void setMaxVehicles(VehicleAvailRateRQResources resources)
        {
            string pick;
            int brandId = resources.BrandId;

            XmlNode picLoc = xmlDocument.DocumentElement.SelectSingleNode("//a1:PickUpLocation", namespaceManager);

            pick = picLoc.Attributes["LocationCode"].Value;

            var VehLimit = (from bc in db.BrandCountries
                            join l in db.Locations on bc.CountryIDFK equals l.CountryIDFK
                            where l.LocationCode == pick && bc.BrandIDFK == brandId
                            select new
                            {
                                bc.VehicleLimit
                            });

            if (VehLimit.FirstOrDefault() != null)
            {
                XmlNodeList vehicleListNode = xmlDocument.DocumentElement.SelectNodes("//a1:VehAvail", namespaceManager);
                if (vehicleListNode.Count > 0)
                {
                    for (int i = vehicleListNode.Count; i < VehLimit.FirstOrDefault().VehicleLimit; i--)
                    {
                        vehicleListNode[i].ParentNode.RemoveChild(vehicleListNode[i]);
                    }
                }
            }
            else
            {
                throw new InvalidRequestException("Vehicle Limit not set");
            }
        }
    }
}