﻿namespace DTAG
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Xml;
    using ORM;
    using Exceptions;
    using ProcessResources;
    using WebServiceMail;

    public class DtagProviderChannel : GenericProviderChannel
    {
        public override void doProviderSpecificValidationsForRequest(VehicleAvailRateRQResources resources)
        {
            removeSourceAttribute();
            removeRateQualifierAttributes(resources);
            addSourceAttribute(resources);
            addAvailabilityNodesAttribute(resources);
        }
        public override void doProviderSpecificValidationsForResponse(VehicleAvailRateRQResources resources)
        {
            //do validation
            setAvailPrepayTag(resources);
            modifyVehicleDetails(resources.PickupLocationCode, resources.BrandId);
        }
        public override void doProviderSpecificValidationsForRequest(VehicleResRQResources resources)
        {
            setBookingRequestData(resources);
            removeSourceAttribute();
            removeCustomerNodes();
            removeRateQulifier(resources);
            removeBookingNodesAttribute();
            addBookingNodesAttribute(resources);
            savePreBookingInDatabase(resources);
        }
        public override void doProviderSpecificValidationsForResponse(VehicleResRQResources resources)
        {
            if (hasErrorsInBookingResponse(resources))
            {

            }
            else
            {
                setBookingResponseData(resources);
                modifyVehicleDetailsRes(resources);
                SendEmail(resources);
                saveBookingInDatabase(resources);
            }
        }
        public override void doProviderSpecificValidationsForRequest(VehicleRetrivaleRQResources resources)
        {
            //throw new NotImplementedException();
            removeSourceAttribute();
            removeCompanyName();
            addRetrivalNodes(resources);
        }
        public override void doProviderSpecificValidationsForResponse(VehicleRetrivaleRQResources resources)
        {
            //throw new NotImplementedException();
            setdateTime();
            modifyVehicleDetailRet(resources);
        }

        public override void doProviderSpecificValidationsForRequest(VehicleResModifyRQResources resources)
        {
            //throw new NotImplementedException();
            setDateTimeMod(resources);
            removeSourceAttribute();
            removeCustomerNodes();
            removeRateQulifier();
            addModifyNodes(resources);
            setPickDropDate();
            RemoveOrdersDB(resources);
        }

        public override void doProviderSpecificValidationsForResponse(VehicleResModifyRQResources resources)
        {
            //throw new NotImplementedException();
            setModifyBookingResponseData(resources);
            modifyVehicleDetailMod(resources);
            setModifyRequestData(resources);
        }
        public override void doProviderSpecificValidationsForRequest(VehicleCancelRQResources resources)
        {
            //throw new NotImplementedException();
            removeSourceAttribute();
            removeCompanyName();
            addCancelNodes(resources);
        }
        public override void doProviderSpecificValidationsForResponse(VehicleCancelRQResources resources)
        {
            //throw new NotImplementedException();
            setCancelRequestData(resources);
            modifyVehicleDetailsCan(resources);
        }
        public override void doProviderSpecificValidationsForRequest(VehicleRentalRQResources resources)
        {
            //throw new NotImplementedException();
        }
        public override void doProviderSpecificValidationsForResponse(VehicleRentalRQResources resources)
        {
            //throw new NotImplementedException();
        }

        //Comman Methods

        private void removeSourceAttribute()
        {
            XmlNode POSNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:POS", namespaceManager);
            if (POSNode != null)
            {
                XmlNodeList SourceNode = POSNode.SelectNodes("//a1:Source", namespaceManager);
                if (SourceNode != null)
                {
                    foreach (XmlNode source in SourceNode)
                    {
                        source.ParentNode.RemoveChild(source);
                    }
                }
            }
            //XmlNode SourceNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:Source", namespaceManager);
            //if (SourceNode != null)
            //{
            //    SourceNode.Attributes.RemoveNamedItem("ISOCountry");
            //    SourceNode.Attributes.RemoveNamedItem("AgentDutyCode");
            //}
        }

        private void removeCustomerNodes()
        {
            XmlNode Telephone = xmlDocument.DocumentElement.SelectSingleNode("//a1:Telephone", namespaceManager);
            if (Telephone != null)
            {
                Telephone.ParentNode.RemoveChild(Telephone);
            }
            XmlNode Email = xmlDocument.DocumentElement.SelectSingleNode("//a1:Email", namespaceManager);
            if (Email != null)
            {
                Email.ParentNode.RemoveChild(Email);
            }
            XmlNode Address = xmlDocument.DocumentElement.SelectSingleNode("//a1:Address", namespaceManager);
            if (Address != null)
            {
                Address.ParentNode.RemoveChild(Address);
            }
            //XmlNode custLoyal = xmlDocument.DocumentElement.SelectSingleNode("//a1:CustLoyalty", namespaceManager);
            //if (custLoyal != null)
            //{
            //    custLoyal.ParentNode.RemoveChild(custLoyal);
            //}
        }

        private void removeCompanyName()
        {
            XmlNode CompanyNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:CompanyName", namespaceManager);
            if (CompanyNode != null)
            {
                CompanyNode.ParentNode.RemoveChild(CompanyNode);
            }
        }

        private void setBookingLocationCountryDetails(string locationCode, bool mode, VehicleResRQResources bookingResouce)
        {
            var CountryData = (from l in db.Locations
                               join c in db.Countries on l.CountryIDFK equals c.CountryID
                               where (l.LocationCode == locationCode)
                               select new
                               {
                                   c.CountryCode,
                                   c.CountryName
                               });
            if (CountryData.Count() > 0)
            {
                if (mode)
                {
                    bookingResouce.PickUpCountryCode = CountryData.First().CountryCode;
                    bookingResouce.PickUpCountryName = CountryData.First().CountryName;
                }
                else
                {
                    bookingResouce.DropOffCountryCode = CountryData.First().CountryCode;
                    bookingResouce.DropOffCountryName = CountryData.First().CountryName;
                }
            }
        }

        // -------------------------------------------------------------------------------------------------------

        //Available Methods

        private void removeRateQualifierAttributes(VehicleAvailRateRQResources resources)
        {
            XmlNode RateQualNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:RateQualifier", namespaceManager);
            if (RateQualNode != null)
            {
                if (RateQualNode.Attributes["TravelPurpose"] != null)
                {
                    RateQualNode.Attributes.RemoveNamedItem("TravelPurpose");
                }
                if (RateQualNode.Attributes["CorpDiscountNmbr"] != null)
                {
                    RateQualNode.Attributes.RemoveNamedItem("CorpDiscountNmbr");
                }

                if (resources.tourInfoTag == true)
                {
                }
                else
                {
                    if (RateQualNode.Attributes["RateQualifier"] != null)
                    {
                        RateQualNode.Attributes.RemoveNamedItem("RateQualifier");
                    }

                    XmlAttribute category = xmlDocument.CreateAttribute("RateCategory");
                    XmlAttribute period = xmlDocument.CreateAttribute("RatePeriod");
                    RateQualNode.Attributes.Append(category);
                    RateQualNode.Attributes.Append(period);

                    RateQualNode.Attributes["RateCategory"].Value = "16";
                    RateQualNode.Attributes["RatePeriod"].Value = "Daily";
                }
            }

            XmlNode TPANode = xmlDocument.DocumentElement.SelectSingleNode("//a1:TPA_Extensions", namespaceManager);
            if (TPANode != null)
            {
                TPANode.ParentNode.RemoveChild(TPANode);
            }
        }

        private void addSourceAttribute(VehicleAvailRateRQResources resources)
        {
            XmlNode POSNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:POS", namespaceManager);
            if (POSNode != null)
            {
                XmlElement SoceNode = xmlDocument.CreateElement("Source", Configs.XML_NAMESPACE);
                POSNode.AppendChild(SoceNode);

                XmlElement ReqId = xmlDocument.CreateElement("RequestorID", Configs.XML_NAMESPACE);
                ReqId.SetAttribute("Type", "4");
                ReqId.SetAttribute("ID", "CE8AD9499FC04010 -145180");
                SoceNode.AppendChild(ReqId);

                //if (resources.BrandName.ToString().ToUpper() == Brands.THRIFTY.ToString())
                //{
                //    XmlElement ReqId = xmlDocument.CreateElement("RequestorID", Configs.XML_NAMESPACE);
                //    ReqId.SetAttribute("Type", "4");
                //    ReqId.SetAttribute("ID", "CE8AD9499FC04010 -145180");
                //    SoceNode.AppendChild(ReqId);
                //}
                //else if (resources.BrandName.ToString().ToUpper() == Brands.DOLLAR.ToString())
                //{
                //    XmlElement ReqId = xmlDocument.CreateElement("RequestorID", Configs.XML_NAMESPACE);
                //    ReqId.SetAttribute("Type", "4");
                //    ReqId.SetAttribute("ID", "0390CE6F2758418F -145180");
                //    SoceNode.AppendChild(ReqId);
                //}
            }
        }

        private void addAvailabilityNodesAttribute(VehicleAvailRateRQResources resources)
        {
            XmlNode OTA = xmlDocument.DocumentElement.SelectSingleNode("//a1:OTA_VehAvailRateRQ", namespaceManager);
            if (OTA != null)
            {
                if (OTA.Attributes["Target"] != null)
                {
                    OTA.Attributes["Target"].Value = "Production";
                }
                else
                {
                    XmlAttribute Target = xmlDocument.CreateAttribute("Target");
                    OTA.Attributes.Append(Target);
                    OTA.Attributes["Target"].Value = "Production";
                }
            }
            XmlNode VehNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:VendorPrefs", namespaceManager);
            if (VehNode != null) { }
            else
            {
                XmlElement elem = xmlDocument.CreateElement("VendorPrefs", Configs.XML_NAMESPACE);

                if (resources.BrandName.ToString().ToUpper() == Brands.THRIFTY.ToString())
                {
                    XmlElement elem1 = xmlDocument.CreateElement("VendorPref", Configs.XML_NAMESPACE);
                    elem1.SetAttribute("Code", "ZT");
                    elem.AppendChild(elem1);
                }
                else if (resources.BrandName.ToString().ToUpper() == Brands.DOLLAR.ToString())
                {
                    XmlElement elem1 = xmlDocument.CreateElement("VendorPref", Configs.XML_NAMESPACE);
                    elem1.SetAttribute("Code", "ZR");
                    elem.AppendChild(elem1);
                }

                XmlNode VehPrefNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:VehAvailRQCore", namespaceManager);
                VehPrefNode.AppendChild(elem);
            }

            //XmlNode vehAvailRQ = xmlDocument.DocumentElement.SelectSingleNode("//a1:OTA_VehAvailRateRQ", namespaceManager);
            //if (vehAvailRQ != null)
            //{
            //    XmlElement soap = xmlDocument.CreateElement("soapenv:Envelope", Configs.XML_NAMESPACE);
            //    soap.SetAttribute("xmlns:soapenv", "http://schemas.xmlsoap.org/soap/envelope/");
            //    soap.SetAttribute("xmlns:ns", "http://www.opentravel.org/OTA/2003/05");
            //    XmlElement header = xmlDocument.CreateElement("soapenv:Header", Configs.XML_NAMESPACE);
            //    XmlElement body = xmlDocument.CreateElement("soapenv:Body", Configs.XML_NAMESPACE);
            //    XmlElement getRates = xmlDocument.CreateElement("ns:GetRates", Configs.XML_NAMESPACE);

            //    soap.AppendChild(header);
            //    soap.AppendChild(body);
            //    body.AppendChild(getRates);
            //    getRates.AppendChild(vehAvailRQ);
            //}
        }

        private void setAvailPrepayTag(VehicleAvailRateRQResources resources)
        {
            if (resources.tourInfoTag == true)
            {
                XmlNodeList vehNode = xmlDocument.DocumentElement.SelectNodes("//a1:VehAvail", namespaceManager);
                foreach (XmlNode node in vehNode)
                {
                    try
                    {
                        XmlNode charge = node.SelectSingleNode(".//a1:TotalCharge", namespaceManager);
                        string amount = charge.Attributes["EstimatedTotalAmount"].Value;
                        string currency = charge.Attributes["CurrencyCode"].Value;
                        XmlElement paymentRules = xmlDocument.CreateElement("PaymentRules", Configs.XML_NAMESPACE);
                        XmlElement paymentRule = xmlDocument.CreateElement("PaymentRule", Configs.XML_NAMESPACE);
                        paymentRule.SetAttribute("RuleType", "2");
                        paymentRule.SetAttribute("Amount", amount);
                        paymentRule.SetAttribute("CurrencyCode", currency);
                        paymentRules.AppendChild(paymentRule);
                        if (node.SelectSingleNode(".//a1:VehAvailInfo", namespaceManager) == null)
                        {
                            XmlElement vehAvailInfo = xmlDocument.CreateElement("VehAvailInfo", Configs.XML_NAMESPACE);
                            vehAvailInfo.AppendChild(paymentRules);
                            node.AppendChild(vehAvailInfo);
                        }
                        else
                        {
                            XmlNode vehInfo = node.SelectSingleNode(".//a1:VehAvailInfo", namespaceManager);
                            vehInfo.AppendChild(paymentRules);
                        }
                    }
                    catch { }
                }
            }
        }

        private void modifyVehicleDetails(string locationCode, int brandId)
        {
            XmlNodeList vehicleListNode = xmlDocument.DocumentElement.SelectNodes("//a1:VehAvail", namespaceManager);
            if (vehicleListNode.Count > 0)
            {
                foreach (XmlNode vehicleNode in vehicleListNode)
                {
                    string sippCode = vehicleNode.SelectSingleNode(".//a1:Vehicle", namespaceManager).Attributes["Code"].Value.ToString().Trim();
                    var vehicleDetails = (from l in db.Locations
                                          join c in db.Countries on l.CountryIDFK equals c.CountryID
                                          join vr in db.VehicleResources on c.CountryID equals vr.CountryIDFK
                                          join vdb in db.VehicleDetailsByBrands on vr.VDBIDFK equals vdb.VDBID
                                          join b in db.Brands on vdb.BrandIDFK equals b.BrandID
                                          join cg in db.CarGroups on vdb.CarGroupIDFK equals cg.CarGroupID
                                          where b.BrandID == brandId
                                          && l.LocationCode == locationCode
                                          && cg.CarCode == sippCode
                                          select new
                                          {
                                              vdb.CarDescription,
                                              vdb.CarImagePath,
                                              c.CountryCode
                                          });
                    XmlNode vehicleMakeModelNode = vehicleNode.SelectSingleNode(".//a1:VehMakeModel", namespaceManager);
                    XmlNode pictureUrlNode = vehicleNode.SelectSingleNode(".//a1:PictureURL", namespaceManager);
                    if (vehicleDetails.Count() > 0)
                    {
                        vehicleMakeModelNode.Attributes["Name"].Value = vehicleDetails.FirstOrDefault().CarDescription;
                        pictureUrlNode.InnerText = vehicleDetails.FirstOrDefault().CarImagePath;

                        //Remove Special Equipment (Winter Tyres if enabled)
                        //if (AppSettings.IsWiterTyresEnabled)
                        //{
                        //    //setWinterTyresForGermany(vehicleDetails.FirstOrDefault().CountryCode, vehicleNode);
                        //}
                    }
                    string vehicleName = vehicleMakeModelNode.Attributes["Name"].Value;
                    int indexOfSimilarText = vehicleName.IndexOf("OR SIMILAR");
                    if (indexOfSimilarText != -1)
                    {
                        vehicleMakeModelNode.Attributes["Name"].Value = vehicleName.Substring(0, indexOfSimilarText);
                    }
                }
            }
        }

        // -----------------------------------------------------------------------------------------------

        //Reservation Methods

        private void setBookingRequestData(VehicleResRQResources bookingResouce)
        {
            XmlNode source = xmlDocument.DocumentElement.SelectSingleNode("//a1:Source", namespaceManager);
            if (source != null)
            {
                bookingResouce.CountryOfResidenceCode = source.Attributes["ISOCountry"].Value;
            }
            XmlNode firstNameNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:GivenName", namespaceManager);
            if (firstNameNode != null)
            {
                bookingResouce.CustomerFirstname = firstNameNode.InnerText.Trim();
            }
            XmlNode surNameNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:Surname", namespaceManager);
            if (surNameNode != null)
            {
                bookingResouce.CustomerSurname = surNameNode.InnerText.Trim();
            }
            XmlNode customerEmailNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:Email", namespaceManager);
            if (customerEmailNode != null)
            {
                bookingResouce.CustomerEmail = customerEmailNode.InnerText.Trim();
            }
            XmlNode customerPhoneNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:Telephone", namespaceManager);
            if (customerPhoneNode != null)
            {
                bookingResouce.CustomerPhone = customerPhoneNode.Attributes["PhoneNumber"].Value.Trim();
            }
            XmlNode customerAddressNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:AddressLine", namespaceManager);
            if (customerAddressNode != null)
            {
                bookingResouce.Address = customerAddressNode.InnerText.Trim();
            }
            XmlNode customerCityNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:CityName", namespaceManager);
            if (customerCityNode != null)
            {
                bookingResouce.CityName = customerCityNode.InnerText.Trim();
            }
            XmlNode customerPostalCodeNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:PostalCode", namespaceManager);
            if (customerPostalCodeNode != null)
            {
                bookingResouce.PostalCode = customerPostalCodeNode.InnerText.Trim();
            }
            XmlNode customerCountryNameNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:CountryName", namespaceManager);
            if (customerCountryNameNode != null)
            {
                bookingResouce.CountryOfResidenceCode = customerCountryNameNode.Attributes["Code"].Value.Trim();
            }
            XmlNode primaryNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:Primary", namespaceManager);
            if (primaryNode != null)
            {
                if (primaryNode.Attributes["BirthDate"] != null)
                {
                    bookingResouce.BirthDay = primaryNode.Attributes["BirthDate"].Value.Trim();
                }
            }
            setBookingLocationCountryDetails(bookingResouce.PickupLocationCode, true, bookingResouce);
            setBookingLocationCountryDetails(bookingResouce.DropoffLocationCode, false, bookingResouce);
        }

        private void removeRateQulifier(VehicleResRQResources resources)
        {
            XmlNode RateNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:RateQualifier", namespaceManager);
            if (RateNode != null)
            {
                if (resources.tourInfoTag == true)
                {
                    if (RateNode.Attributes["TravelPurpose"] != null)
                    {
                        RateNode.Attributes.RemoveNamedItem("TravelPurpose");
                    }
                    if (RateNode.Attributes["CorpDiscountNmbr"] != null)
                    {
                        RateNode.Attributes.RemoveNamedItem("CorpDiscountNmbr");
                    }
                }
                else
                {
                    RateNode.ParentNode.RemoveChild(RateNode);
                }
            }
        }

        private void removeBookingNodesAttribute()
        {
            XmlNode VehCore = xmlDocument.DocumentElement.SelectSingleNode("//a1:VehResRQCore", namespaceManager);
            if (VehCore != null)
            {
                if (VehCore.Attributes["Status"] != null)
                {
                    VehCore.Attributes.RemoveNamedItem("Status");
                }
            }

            XmlNode VehPayInfo = xmlDocument.DocumentElement.SelectSingleNode("//a1:RentalPaymentPref", namespaceManager);
            if (VehPayInfo != null)
            {
                VehPayInfo.ParentNode.RemoveChild(VehPayInfo);
            }
            //XmlNode VehArrivalInfo = xmlDocument.DocumentElement.SelectSingleNode("//a1:ArrivalDetails", namespaceManager);
            //if (VehArrivalInfo != null)
            //{
            //    VehArrivalInfo.ParentNode.RemoveChild(VehArrivalInfo);
            //}
            XmlNode WrittenlInfo = xmlDocument.DocumentElement.SelectSingleNode("//a1:WrittenConfInst", namespaceManager);
            if (WrittenlInfo != null)
            {
                WrittenlInfo.ParentNode.RemoveChild(WrittenlInfo);
            }
            XmlNode TPANode = xmlDocument.DocumentElement.SelectSingleNode("//a1:TPA_Extensions", namespaceManager);
            if (TPANode != null)
            {
                TPANode.ParentNode.RemoveChild(TPANode);
            }
        }

        private void addBookingNodesAttribute(VehicleResRQResources resources)
        {
            XmlNode POSNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:POS", namespaceManager);
            if (POSNode != null)
            {
                XmlElement SoceNode = xmlDocument.CreateElement("Source", Configs.XML_NAMESPACE);
                POSNode.AppendChild(SoceNode);

                XmlElement ReqId = xmlDocument.CreateElement("RequestorID", Configs.XML_NAMESPACE);
                ReqId.SetAttribute("Type", "4");
                ReqId.SetAttribute("ID", "CE8AD9499FC04010 -145180");
                SoceNode.AppendChild(ReqId);

                //if (resources.BrandName.ToString().ToUpper() == Brands.THRIFTY.ToString())
                //{
                //    XmlElement ReqId = xmlDocument.CreateElement("RequestorID", Configs.XML_NAMESPACE);
                //    ReqId.SetAttribute("Type", "4");
                //    ReqId.SetAttribute("ID", "CE8AD9499FC04010 -145180");
                //    SoceNode.AppendChild(ReqId);
                //}
                //else if (resources.BrandName.ToString().ToUpper() == Brands.DOLLAR.ToString())
                //{
                //    XmlElement ReqId = xmlDocument.CreateElement("RequestorID", Configs.XML_NAMESPACE);
                //    ReqId.SetAttribute("Type", "4");
                //    ReqId.SetAttribute("ID", "0390CE6F2758418F -145180");
                //    SoceNode.AppendChild(ReqId);
                //}
            }

            XmlNode OTA = xmlDocument.DocumentElement.SelectSingleNode("//a1:OTA_VehResRQ", namespaceManager);
            if (OTA != null)
            {
                if (OTA.Attributes["Target"] != null)
                {
                    OTA.Attributes["Target"].Value = "Production";
                }
                else
                {
                    XmlAttribute Target = xmlDocument.CreateAttribute("Target");
                    OTA.Attributes.Append(Target);
                    OTA.Attributes["Target"].Value = "Production";
                }
            }

            XmlNode ResCore = xmlDocument.DocumentElement.SelectSingleNode("//a1:VehResRQCore", namespaceManager);
            if (ResCore != null)
            {
                if (ResCore.Attributes["OptionChangeIndicator"] == null)
                {
                    XmlAttribute indicator = xmlDocument.CreateAttribute("OptionChangeIndicator");
                    ResCore.Attributes.Append(indicator);
                }
            }

            XmlNode setResCore = xmlDocument.DocumentElement.SelectSingleNode("//a1:VehResRQCore", namespaceManager);
            if (setResCore != null)
            {
                setResCore.Attributes["OptionChangeIndicator"].Value = "true";
            }

            XmlNode VehBrandNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:VendorPref", namespaceManager);
            if (VehBrandNode == null)
            {
                if (resources.BrandName.ToString().ToUpper() == Brands.THRIFTY.ToString())
                {
                    XmlElement vendorPref = xmlDocument.CreateElement("VendorPref", Configs.XML_NAMESPACE);
                    vendorPref.SetAttribute("Code", "ZT");

                    XmlNode VehPrefNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:VehResRQCore", namespaceManager);
                    VehPrefNode.AppendChild(vendorPref);
                }
                else if (resources.BrandName.ToString().ToUpper() == Brands.DOLLAR.ToString())
                {
                    XmlElement vendorPref = xmlDocument.CreateElement("VendorPref", Configs.XML_NAMESPACE);
                    vendorPref.SetAttribute("Code", "ZR");

                    XmlNode VehPrefNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:VehResRQCore", namespaceManager);
                    VehPrefNode.AppendChild(vendorPref);
                }
            }

            XmlNode Reference = xmlDocument.DocumentElement.SelectSingleNode("//a1:Reference", namespaceManager);
            if (Reference != null)
            {
                if (Reference.Attributes["ID_Context"] == null)
                {
                    XmlAttribute context = xmlDocument.CreateAttribute("ID_Context");
                    Reference.Attributes.Append(context);
                }
            }

            XmlNode setReference = xmlDocument.DocumentElement.SelectSingleNode("//a1:Reference", namespaceManager);
            if (setReference != null)
            {
                setReference.Attributes["ID_Context"].Value = "InetID";
            }
        }

        private void savePreBookingInDatabase(VehicleResRQResources resources)
        {
            BokkingRequest preBookingObj = new BokkingRequest
            {
                CustomerName = resources.CustomerFirstname + " " + resources.CustomerSurname,
                CustomerEmail = resources.CustomerEmail,
                CustomerPhone = resources.CustomerPhone,
                PickUpDate = resources.PickupDateTime,
                DropOffDate = resources.DropoffDatetime,
                PickUpLocation = resources.PickupLocationCode,
                DropOffLocation = resources.DropoffLocationCode,
                CustomerSurname = resources.CustomerSurname,
                CustomerFirstname = resources.CustomerFirstname,
                AddressLine1 = resources.Address,
                AddressLine2 = resources.CityName,
                AddressTown = resources.CityName,
                AddressPostCodeZip = resources.PostalCode,
                CountryOfResidence = resources.CountryOfResidenceCode,
                CameFrom = resources.PartnerName,
                XML_Provider = resources.ProviderName,
                OrderDate = DateTime.Now
            };
            db.BokkingRequests.Add(preBookingObj);
            db.SaveChanges();
        }

        private bool hasErrorsInBookingResponse(VehicleResRQResources resources)
        {
            XmlNode error = xmlDocument.DocumentElement.SelectSingleNode("//a1:Errors", namespaceManager);
            if (error != null)
            {
                return true;
            }
            return false;
        }

        private void setBookingResponseData(VehicleResRQResources resources)
        {
            resources.ReservedVehicle = new BookedVehicle();
            XmlNode firstNameNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:GivenName", namespaceManager);
            if (firstNameNode != null)
            {
                resources.CustomerFirstname = firstNameNode.InnerText.Trim();
            }
            XmlNode surNameNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:Surname", namespaceManager);
            if (surNameNode != null)
            {
                resources.CustomerSurname = surNameNode.InnerText.Trim();
            }
            XmlNode confirmationIdNode = xmlDocument.SelectSingleNode("//a1:ConfID", namespaceManager);
            if (confirmationIdNode != null)
            {
                resources.ConfirmationID = confirmationIdNode.Attributes["ID"].Value;
            }
            //XmlNode flightNoNode = xmlDocument.SelectSingleNode("//a1:ArrivalDetails", namespaceManager);
            XmlNode VehModelNode = xmlDocument.SelectSingleNode("//a1:VehMakeModel", namespaceManager);
            if (VehModelNode != null)
            {
                //RemoteDebugger.SendXmlForDebugging(VehModelNode.Attributes["Name"].Value, "Vehicle name");
                resources.ReservedVehicle.Name = VehModelNode.Attributes["Name"].Value.ToString();
            }
            XmlNode ImageNode = xmlDocument.SelectSingleNode("//a1:PictureURL", namespaceManager);
            if (ImageNode != null)
            {
                resources.ReservedVehicle.ImagePath = ImageNode.InnerText.Trim();
            }
            XmlNode PaymentNode = xmlDocument.SelectSingleNode("//a1:VehicleCharge", namespaceManager);
            if (PaymentNode != null)
            {
                resources.ReservedVehicle.Price = decimal.Parse(PaymentNode.Attributes["Amount"].Value);
                resources.ReservedVehicle.Currency = PaymentNode.Attributes["CurrencyCode"].Value;
            }
            XmlNode priceIncludeNode = xmlDocument.SelectSingleNode("//a1:Calculation", namespaceManager);
            if (priceIncludeNode != null)
            {
                resources.ReservedVehicle.ImagePath = priceIncludeNode.Attributes["UnitCharge"].Value;
            }
        }

        private void modifyVehicleDetailsRes(VehicleResRQResources bookingResouce)
        {
            XmlNode vehicle = xmlDocument.DocumentElement.SelectSingleNode("//a1:Vehicle", namespaceManager);
            if (vehicle != null)
            {
                string sippCode = vehicle.Attributes["Code"].Value.ToString().Trim();
                var vehicleDetails = (from l in db.Locations
                                      join c in db.Countries on l.CountryIDFK equals c.CountryID
                                      join vr in db.VehicleResources on c.CountryID equals vr.CountryIDFK
                                      join vdb in db.VehicleDetailsByBrands on vr.VDBIDFK equals vdb.VDBID
                                      join b in db.Brands on vdb.BrandIDFK equals b.BrandID
                                      join cg in db.CarGroups on vdb.CarGroupIDFK equals cg.CarGroupID
                                      where b.BrandID == bookingResouce.BrandId
                                      && l.LocationCode == bookingResouce.PickupLocationCode
                                      && cg.CarCode == sippCode
                                      select new
                                      {
                                          vdb.CarDescription,
                                          vdb.CarImagePath,
                                          c.CountryCode
                                      });
                XmlNode vehicleMakeModelNode = vehicle.SelectSingleNode(".//a1:VehMakeModel", namespaceManager);
                XmlNode pictureUrlNode = vehicle.SelectSingleNode(".//a1:PictureURL", namespaceManager);
                if (vehicleDetails.Count() > 0)
                {
                    vehicleMakeModelNode.Attributes["Name"].Value = vehicleDetails.FirstOrDefault().CarDescription;
                    pictureUrlNode.InnerText = vehicleDetails.FirstOrDefault().CarImagePath;

                    //Remove Special Equipment (Winter Tyres if enabled)
                    //if (AppSettings.IsWiterTyresEnabled)
                    //{
                    //    //setWinterTyresForGermany(vehicleDetails.FirstOrDefault().CountryCode, vehicleNode);
                    //}
                }
                string vehicleName = vehicleMakeModelNode.Attributes["Name"].Value;
                int indexOfSimilarText = vehicleName.IndexOf("OR SIMILAR");
                if (indexOfSimilarText != -1)
                {
                    vehicleMakeModelNode.Attributes["Name"].Value = vehicleName.Substring(0, indexOfSimilarText);
                }
            }
        }

        private void SendEmail(VehicleResRQResources bookingResouce)
        {

            var email = (from p in db.Partners
                         where p.DomainName == bookingResouce.PartnerName
                         select new
                         {
                             p.ConfimationMailSend
                         });

            if (email.First().ConfimationMailSend == 1)
            {

                var selectedBrand = bookingResouce.BrandName;

                try
                {
                    BuildConfirmationEmail confEmailBuilder = new BuildConfirmationEmail(bookingResouce, bookingResouce.ReservedVehicle);
                    //RemoteDebugger.SendXmlForDebugging(bookingResouce.ConfirmationID + "  " + bookingResouce.ReservedVehicle.Currency + "  " + bookingResouce.ReservedVehicle.Price.ToString() + "  " + bookingResouce.ReservedVehicle.ImagePath + "  " + bookingResouce.ReservedVehicle.Name + "  " + bookingResouce.ReservedVehicle.DrpFee + "  " + bookingResouce.ReservedVehicle.priceInclude.ToString() + "  " + bookingResouce.ReservedVehicle.distance, "mail Progress");
                    string emailMessage = confEmailBuilder.BuildEmail(bookingResouce.ConfirmationID, bookingResouce.ReservedVehicle.Currency, bookingResouce.ReservedVehicle.Price.ToString(), bookingResouce.ReservedVehicle.ImagePath, bookingResouce.ReservedVehicle.Name, bookingResouce.ReservedVehicle.DrpFee, bookingResouce.ReservedVehicle.priceInclude.ToString(), bookingResouce.ReservedVehicle.distance, TimeBetweenDates.CalculateMinutes(bookingResouce.PickupDateTime, Convert.ToDateTime(bookingResouce.DropoffDatetime)));
                    string emailSubject = bookingResouce.ModifyProcess ? (string)HttpContext.GetGlobalResourceObject("SiteStrings", "ModifyEmailSubject") : (string)HttpContext.GetGlobalResourceObject("SiteStrings", "BookingEmailSubject");
                    //Send confirmation email

                    bool isSent = SendConfMail.SendMessage(AppSettings.SiteEmailsSentFrom, bookingResouce.CustomerEmail, emailSubject, emailMessage, true);

                    if (!isSent)
                    {
                        string erroremailMessage = CommonClass.GetErrorXML("VehResRS", "1", "099", "Booking Completed Successfully. Error Occured while sending confirmation email. Please Contact Hertz.");
                        string errorSubject = "Error in Booking";

                        SendConfMail.SendMessage(AppSettings.SiteEmailsSentFrom, bookingResouce.CustomerEmail, errorSubject, erroremailMessage, true);
                    }
                }

                catch (Exception ex)
                {
                    RemoteDebugger.SendXmlForDebugging(ex.Message, "Errors");
                    RemoteDebugger.SendXmlForDebugging(ex.StackTrace, "Errors");
                }
            }
        }

        private void saveBookingInDatabase(VehicleResRQResources resources)
        {
            Order BookingObj = new Order
            {
                ConfirmationID = resources.ConfirmationID,
                CustomerName = resources.CustomerFirstname + " " + resources.CustomerSurname ?? "",
                CustomerEmail = resources.CustomerEmail ?? "",
                EmailCode = "",
                EmailConfirmed = 1,
                IncludeInEshot = 1,
                CustomerPhone = resources.CustomerPhone ?? "",
                CarBooked = resources.ReservedVehicle.Name ?? "",
                VehicleType = "",
                CountryOfResidence = resources.CountryOfResidenceCode ?? "",
                PickUpDate = resources.PickupDateTime,
                DropOffDate = resources.DropoffDatetime,
                OrderDate = DateTime.Now,
                PickUpLocation = resources.PickupLocationCode,
                DropOffLocation = resources.DropoffLocationCode ?? "",
                NoDaysHired = 0,
                AmountPaid = Convert.ToDouble(resources.ReservedVehicle.Price),
                AmountPaidCurrency = resources.ReservedVehicle.Currency ?? "",
                CameFrom = resources.PartnerName ?? "",
                LanguageCode = "",
                ReminderEmail = DateTime.Now,
                ThanksEmail = DateTime.Now,
                HertzNum1 = "",
                XML_Provider = resources.ProviderName ?? "",
                XML_Provider_PickupSupplier = resources.BrandName ?? "",
                XML_Provider_DropoffSupplier = resources.BrandName ?? "",
                XML_RateQualifier = resources.ProviderRateQualifier ?? "",
                CustomerSurname = resources.CustomerSurname ?? "",
                CustomerFirstname = resources.CustomerFirstname ?? "",
                AddressLine1 = resources.Address ?? "",
                AddressLine2 = resources.CityName ?? "",
                AddressTown = resources.CityName ?? "",
                AddressPostCodeZip = resources.PostalCode ?? "",
                AddressCountryCode = resources.SourceCountryCode ?? "",
                ThermeonConf_Id = resources.Thermeon_ConfirmationID ?? "",
                CorpDiscountNo = resources.CorpDiscountNo ?? "",
                Customer_BDay = Convert.ToDateTime(resources.BirthDay)
            };
            db.Orders.Add(BookingObj);
            db.SaveChanges();

            int orderID = BookingObj.OrderID;

            try
            {
                XmlNode PriceNode = xmlDocument.SelectSingleNode("//a1:PricedEquips", namespaceManager);
                if (PriceNode != null)
                {
                    XmlNodeList PriceNodeList = PriceNode.SelectNodes("//a1:PricedEquip", namespaceManager);
                    foreach (XmlNode accessory in PriceNodeList)
                    {
                        if (accessory != null)
                        {
                            XmlNode EquipmentNode = accessory.SelectSingleNode(".//a1:Equipment", namespaceManager);
                            XmlNode ChargeNode = accessory.SelectSingleNode(".//a1:Charge", namespaceManager);

                            string accessoryType = EquipmentNode.Attributes["EquipType"].Value.Trim();
                            double price = Convert.ToDouble(ChargeNode.Attributes["Amount"].Value.Trim());
                            int quantity = Convert.ToInt32(EquipmentNode.Attributes["Quantity"].Value.Trim());
                            string currency = resources.ReservedVehicle.Currency;

                            AccessoriesOrdered accessoryOrdered = new AccessoriesOrdered
                            {
                                OrderIDFK = orderID,
                                Accessory = accessoryType,
                                Price = price,
                                Quantity = quantity,
                                Currency = currency
                            };
                            db.AccessoriesOrdereds.Add(accessoryOrdered);
                            db.SaveChanges();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                RemoteDebugger.SendXmlForDebugging(ex.StackTrace, "ErrorDB");
                RemoteDebugger.SendXmlForDebugging(ex.Message, "ErrorDBMessage");
            }
        }

        // -------------------------------------------------------------------------------------------------

        // Retrival Methods

        private void addRetrivalNodes(VehicleRetrivaleRQResources resources)
        {
            XmlNode POSNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:POS", namespaceManager);
            if (POSNode != null)
            {
                XmlElement SoceNode = xmlDocument.CreateElement("Source", Configs.XML_NAMESPACE);
                POSNode.AppendChild(SoceNode);

                XmlElement ReqId = xmlDocument.CreateElement("RequestorID", Configs.XML_NAMESPACE);
                ReqId.SetAttribute("Type", "4");
                ReqId.SetAttribute("ID", "CE8AD9499FC04010 -145180");
                SoceNode.AppendChild(ReqId);

                //if (resources.BrandName.ToString().ToUpper() == Brands.THRIFTY.ToString())
                //{
                //    XmlElement ReqId = xmlDocument.CreateElement("RequestorID", Configs.XML_NAMESPACE);
                //    ReqId.SetAttribute("Type", "4");
                //    ReqId.SetAttribute("ID", "CE8AD9499FC04010 -145180");
                //    SoceNode.AppendChild(ReqId);
                //}
                //else if (resources.BrandName.ToString().ToUpper() == Brands.DOLLAR.ToString())
                //{
                //    XmlElement ReqId = xmlDocument.CreateElement("RequestorID", Configs.XML_NAMESPACE);
                //    ReqId.SetAttribute("Type", "4");
                //    ReqId.SetAttribute("ID", "0390CE6F2758418F -145180");
                //    SoceNode.AppendChild(ReqId);
                //}
            }

            XmlNode OTA = xmlDocument.DocumentElement.SelectSingleNode("//a1:OTA_VehRetResRQ", namespaceManager);
            if (OTA != null)
            {
                if (OTA.Attributes["Target"] != null)
                {
                    OTA.Attributes["Target"].Value = "Production";
                }
                else
                {
                    XmlAttribute Target = xmlDocument.CreateAttribute("Target");
                    OTA.Attributes.Append(Target);
                    OTA.Attributes["Target"].Value = "Production";
                }
            }

            XmlNode info = xmlDocument.DocumentElement.SelectSingleNode("//a1:VehRetResRQInfo", namespaceManager);
            if (info == null)
            {
                XmlElement vehinfo = xmlDocument.CreateElement("VehRetResRQInfo", Configs.XML_NAMESPACE);

                if (resources.BrandName.ToString().ToUpper() == Brands.THRIFTY.ToString())
                {
                    XmlElement vendorPref = xmlDocument.CreateElement("Vendor", Configs.XML_NAMESPACE);
                    vendorPref.SetAttribute("Code", "ZT");

                    vehinfo.AppendChild(vendorPref);
                }
                else if (resources.BrandName.ToString().ToUpper() == Brands.DOLLAR.ToString())
                {
                    XmlElement vendorPref = xmlDocument.CreateElement("Vendor", Configs.XML_NAMESPACE);
                    vendorPref.SetAttribute("Code", "ZR");

                    vehinfo.AppendChild(vendorPref);
                }

                XmlNode OTANode = xmlDocument.DocumentElement.SelectSingleNode("//a1:OTA_VehRetResRQ", namespaceManager);
                OTANode.AppendChild(vehinfo);
            }
        }

        private void setdateTime()
        {
            XmlNode rentalDetailsNode = xmlDocument.SelectSingleNode(".//a1:VehRentalCore ", namespaceManager);
            if (rentalDetailsNode != null)
            {
                string pickdate = rentalDetailsNode.Attributes["PickUpDateTime"].Value;
                string dropdate = rentalDetailsNode.Attributes["ReturnDateTime"].Value;

                rentalDetailsNode.Attributes["PickUpDateTime"].Value = pickdate + "-06:00";
                rentalDetailsNode.Attributes["ReturnDateTime"].Value = dropdate + "-06:00";
            }
        }

        private void modifyVehicleDetailRet(VehicleRetrivaleRQResources resources)
        {
            XmlNode pickup = xmlDocument.DocumentElement.SelectSingleNode("//a1:PickUpLocation", namespaceManager);
            XmlNode dropoff = xmlDocument.DocumentElement.SelectSingleNode("//a1:ReturnLocation", namespaceManager);

            if (pickup != null && dropoff != null)
            {
                resources.PickupLocationCode = pickup.Attributes["LocationCode"].Value;
                resources.DropoffLocationCode = dropoff.Attributes["LocationCode"].Value;

                XmlNode vehicle = xmlDocument.DocumentElement.SelectSingleNode("//a1:Vehicle", namespaceManager);
                if (vehicle != null)
                {
                    string sippCode = vehicle.Attributes["Code"].Value.ToString().Trim();
                    var vehicleDetails = (from l in db.Locations
                                          join c in db.Countries on l.CountryIDFK equals c.CountryID
                                          join vr in db.VehicleResources on c.CountryID equals vr.CountryIDFK
                                          join vdb in db.VehicleDetailsByBrands on vr.VDBIDFK equals vdb.VDBID
                                          join b in db.Brands on vdb.BrandIDFK equals b.BrandID
                                          join cg in db.CarGroups on vdb.CarGroupIDFK equals cg.CarGroupID
                                          where b.BrandName == resources.BrandName
                                          && l.LocationCode == resources.PickupLocationCode
                                          && cg.CarCode == sippCode
                                          select new
                                          {
                                              vdb.CarDescription,
                                              vdb.CarImagePath,
                                              c.CountryCode
                                          });
                    XmlNode vehicleMakeModelNode = vehicle.SelectSingleNode(".//a1:VehMakeModel", namespaceManager);
                    XmlNode pictureUrlNode = vehicle.SelectSingleNode(".//a1:PictureURL", namespaceManager);
                    if (vehicleDetails.Count() > 0)
                    {
                        vehicleMakeModelNode.Attributes["Name"].Value = vehicleDetails.FirstOrDefault().CarDescription;
                        pictureUrlNode.InnerText = vehicleDetails.FirstOrDefault().CarImagePath;

                        //Remove Special Equipment (Winter Tyres if enabled)
                        //if (AppSettings.IsWiterTyresEnabled)
                        //{
                        //    //setWinterTyresForGermany(vehicleDetails.FirstOrDefault().CountryCode, vehicleNode);
                        //}
                    }
                    string vehicleName = vehicleMakeModelNode.Attributes["Name"].Value;
                    int indexOfSimilarText = vehicleName.IndexOf("OR SIMILAR");
                    if (indexOfSimilarText != -1)
                    {
                        vehicleMakeModelNode.Attributes["Name"].Value = vehicleName.Substring(0, indexOfSimilarText);
                    }
                }
            }
        }

        // --------------------------------------------------------------------------------------------------

        // Modify Methods

        private void setDateTimeMod(VehicleResModifyRQResources resources)
        {
            XmlNode vehRentalCore = xmlDocument.SelectSingleNode("//a1:VehRentalCore", namespaceManager);
            if (vehRentalCore != null)
            {
                resources.PickupDateTime = Convert.ToDateTime(vehRentalCore.Attributes["PickUpDateTime"].Value);
                resources.DropoffDatetime = Convert.ToDateTime(vehRentalCore.Attributes["ReturnDateTime"].Value);
            }
        }

        private void removeRateQulifier()
        {
            XmlNode RateNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:RateQualifier", namespaceManager);
            if (RateNode != null)
            {
                RateNode.ParentNode.RemoveChild(RateNode);
            }
        }

        private void addModifyNodes(VehicleResModifyRQResources resources)
        {
            XmlNode POSNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:POS", namespaceManager);
            if (POSNode != null)
            {
                XmlElement SoceNode = xmlDocument.CreateElement("Source", Configs.XML_NAMESPACE);
                POSNode.AppendChild(SoceNode);

                XmlElement ReqId = xmlDocument.CreateElement("RequestorID", Configs.XML_NAMESPACE);
                ReqId.SetAttribute("Type", "4");
                ReqId.SetAttribute("ID", "CE8AD9499FC04010 -145180");
                SoceNode.AppendChild(ReqId);

                //if (resources.BrandName.ToString().ToUpper() == Brands.THRIFTY.ToString())
                //{
                //    XmlElement ReqId = xmlDocument.CreateElement("RequestorID", Configs.XML_NAMESPACE);
                //    ReqId.SetAttribute("Type", "4");
                //    ReqId.SetAttribute("ID", "CE8AD9499FC04010 -145180");
                //    SoceNode.AppendChild(ReqId);
                //}
                //else if (resources.BrandName.ToString().ToUpper() == Brands.DOLLAR.ToString())
                //{
                //    XmlElement ReqId = xmlDocument.CreateElement("RequestorID", Configs.XML_NAMESPACE);
                //    ReqId.SetAttribute("Type", "4");
                //    ReqId.SetAttribute("ID", "0390CE6F2758418F -145180");
                //    SoceNode.AppendChild(ReqId);
                //}
            }

            XmlNode OTA = xmlDocument.DocumentElement.SelectSingleNode("//a1:OTA_VehModifyRQ", namespaceManager);
            if (OTA != null)
            {
                if (OTA.Attributes["Target"] != null)
                {
                    OTA.Attributes["Target"].Value = "Production";
                }
                else
                {
                    XmlAttribute Target = xmlDocument.CreateAttribute("Target");
                    OTA.Attributes.Append(Target);
                    OTA.Attributes["Target"].Value = "Production";
                }
            }

            XmlNode vehModify = xmlDocument.DocumentElement.SelectSingleNode("//a1:VehModifyRQCore", namespaceManager);
            if (vehModify != null)
            {
                vehModify.Attributes["Status"].Value = "Available";
                vehModify.Attributes["ModifyType"].Value = "Modify";
            }

            XmlNode VehBrandNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:VendorPref", namespaceManager);
            if (VehBrandNode == null)
            {
                if (resources.BrandName.ToString().ToUpper() == Brands.THRIFTY.ToString())
                {
                    XmlElement vendorPref = xmlDocument.CreateElement("VendorPref", Configs.XML_NAMESPACE);
                    vendorPref.SetAttribute("Code", "ZT");

                    XmlNode VehPrefNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:VehModifyRQCore", namespaceManager);
                    VehPrefNode.AppendChild(vendorPref);
                }
                else if (resources.BrandName.ToString().ToUpper() == Brands.DOLLAR.ToString())
                {
                    XmlElement vendorPref = xmlDocument.CreateElement("VendorPref", Configs.XML_NAMESPACE);
                    vendorPref.SetAttribute("Code", "ZR");

                    XmlNode VehPrefNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:VehModifyRQCore", namespaceManager);
                    VehPrefNode.AppendChild(vendorPref);
                }
            }

            XmlNode Reference = xmlDocument.DocumentElement.SelectSingleNode("//a1:Reference", namespaceManager);
            if (Reference != null)
            {
                if (Reference.Attributes["ID_Context"] == null)
                {
                    XmlAttribute context = xmlDocument.CreateAttribute("ID_Context");
                    Reference.Attributes.Append(context);
                }
            }

            XmlNode setReference = xmlDocument.DocumentElement.SelectSingleNode("//a1:Reference", namespaceManager);
            if (setReference != null)
            {
                setReference.Attributes["ID_Context"].Value = "InetID";
            }
        }

        private void setPickDropDate()
        {
            XmlNode pickDate = xmlDocument.DocumentElement.SelectSingleNode("//a1:PickUpLocation", namespaceManager);
            XmlNode dropDate = xmlDocument.DocumentElement.SelectSingleNode("//a1:ReturnLocation", namespaceManager);

            string pick = pickDate.Attributes["LocationCode"].Value;
            string drop = dropDate.Attributes["LocationCode"].Value;

            pickDate.Attributes["LocationCode"].Value = pick.TrimEnd();
            dropDate.Attributes["LocationCode"].Value = drop.TrimEnd();
        }

        private void RemoveOrdersDB(VehicleResModifyRQResources resources)
        {
            XmlNode confID = xmlDocument.DocumentElement.SelectSingleNode("//a1:UniqueID", namespaceManager);
            if (confID != null)
            {
                string ConfirmationID = confID.Attributes["ID"].Value;

                var order = db.Orders.Where(o => o.ConfirmationID == ConfirmationID);
                if (order.Count() > 0)
                {
                    int orderID = order.FirstOrDefault().OrderID;

                    var AccOrders = db.AccessoriesOrdereds.Where(ao => ao.OrderIDFK == orderID);
                    foreach (var accOrd in AccOrders)
                    {
                        db.AccessoriesOrdereds.Remove(accOrd);
                    }
                }
            }

            XmlNode accessory = xmlDocument.DocumentElement.SelectSingleNode("//a1:SpecialEquipPref", namespaceManager);
            if (accessory != null)
            {
                resources.IsExtraValid = true;
            }
        }

        private void setModifyBookingResponseData(VehicleResModifyRQResources resources)
        {
            resources.ReservedVehicle = new BookedVehicle();
            XmlNode firstNameNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:GivenName", namespaceManager);
            if (firstNameNode != null)
            {
                resources.CustomerFirstname = firstNameNode.InnerText.Trim();
            }
            XmlNode surNameNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:Surname", namespaceManager);
            if (surNameNode != null)
            {
                resources.CustomerSurname = surNameNode.InnerText.Trim();
            }
            XmlNode VehModelNode = xmlDocument.SelectSingleNode("//a1:VehMakeModel", namespaceManager);
            if (VehModelNode != null)
            {
                resources.ReservedVehicle.Name = VehModelNode.Attributes["Name"].Value.ToString();
            }
            XmlNode ImageNode = xmlDocument.SelectSingleNode("//a1:PictureURL", namespaceManager);
            if (ImageNode != null)
            {
                resources.ReservedVehicle.ImagePath = ImageNode.InnerText.Trim();
            }
            XmlNode PaymentNode = xmlDocument.SelectSingleNode("//a1:TotalCharge", namespaceManager);
            if (PaymentNode != null)
            {
                resources.ReservedVehicle.Price = decimal.Parse(PaymentNode.Attributes["EstimatedTotalAmount"].Value);
                resources.ReservedVehicle.Currency = PaymentNode.Attributes["CurrencyCode"].Value;
            }
            XmlNode vehRentalCore = xmlDocument.SelectSingleNode("//a1:VehRentalCore", namespaceManager);
            if (vehRentalCore != null)
            {
                resources.PickupDateTime = Convert.ToDateTime(vehRentalCore.Attributes["PickUpDateTime"].Value);
                resources.DropoffDatetime = Convert.ToDateTime(vehRentalCore.Attributes["ReturnDateTime"].Value);
            }
        }

        private void modifyVehicleDetailMod(VehicleResModifyRQResources resources)
        {
            XmlNode pickup = xmlDocument.DocumentElement.SelectSingleNode("//a1:PickUpLocation", namespaceManager);
            XmlNode dropoff = xmlDocument.DocumentElement.SelectSingleNode("//a1:ReturnLocation", namespaceManager);

            if (pickup != null && dropoff != null)
            {
                resources.PickupLocationCode = pickup.Attributes["LocationCode"].Value;
                resources.DropoffLocationCode = dropoff.Attributes["LocationCode"].Value;

                XmlNode vehicle = xmlDocument.DocumentElement.SelectSingleNode("//a1:Vehicle", namespaceManager);
                if (vehicle != null)
                {
                    string sippCode = vehicle.Attributes["Code"].Value.ToString().Trim();
                    var vehicleDetails = (from l in db.Locations
                                          join c in db.Countries on l.CountryIDFK equals c.CountryID
                                          join vr in db.VehicleResources on c.CountryID equals vr.CountryIDFK
                                          join vdb in db.VehicleDetailsByBrands on vr.VDBIDFK equals vdb.VDBID
                                          join b in db.Brands on vdb.BrandIDFK equals b.BrandID
                                          join cg in db.CarGroups on vdb.CarGroupIDFK equals cg.CarGroupID
                                          where b.BrandID == resources.BrandId
                                          && l.LocationCode == resources.PickupLocationCode
                                          && cg.CarCode == sippCode
                                          select new
                                          {
                                              vdb.CarDescription,
                                              vdb.CarImagePath,
                                              c.CountryCode
                                          });
                    XmlNode vehicleMakeModelNode = vehicle.SelectSingleNode(".//a1:VehMakeModel", namespaceManager);
                    XmlNode pictureUrlNode = vehicle.SelectSingleNode(".//a1:PictureURL", namespaceManager);
                    if (vehicleDetails.Count() > 0)
                    {
                        vehicleMakeModelNode.Attributes["Name"].Value = vehicleDetails.FirstOrDefault().CarDescription;
                        pictureUrlNode.InnerText = vehicleDetails.FirstOrDefault().CarImagePath;

                        //Remove Special Equipment (Winter Tyres if enabled)
                        //if (AppSettings.IsWiterTyresEnabled)
                        //{
                        //    //setWinterTyresForGermany(vehicleDetails.FirstOrDefault().CountryCode, vehicleNode);
                        //}
                    }
                    string vehicleName = vehicleMakeModelNode.Attributes["Name"].Value;
                    int indexOfSimilarText = vehicleName.IndexOf("OR SIMILAR");
                    if (indexOfSimilarText != -1)
                    {
                        vehicleMakeModelNode.Attributes["Name"].Value = vehicleName.Substring(0, indexOfSimilarText);
                    }
                }
            }
        }

        private void setModifyRequestData(VehicleResModifyRQResources bookingResouce)
        {
            string currencyCode = "";

            XmlNode confirmationId = xmlDocument.DocumentElement.SelectSingleNode("//a1:ConfID", namespaceManager);
            if (confirmationId != null)
            {
                bookingResouce.ConfirmationID = confirmationId.Attributes["ID"].Value.Trim();


                var order = (from o in db.Orders
                             where (o.ConfirmationID == bookingResouce.ConfirmationID)
                             select new
                             {
                                 o.OrderID,
                                 o.CustomerName,
                                 o.CustomerEmail,
                                 o.EmailConfirmed,
                                 o.CarBooked,
                                 o.VehicleType,
                                 o.CountryOfResidence,
                                 o.PickUpDate,
                                 o.DropOffDate,
                                 o.OrderDate,
                                 o.PickUpLocation,
                                 o.DropOffLocation,
                                 o.NoDaysHired,
                                 o.AmountPaid,
                                 o.AmountPaidCurrency,
                                 o.XML_Provider,
                                 o.XML_IATANumber,
                                 o.XML_RateQualifier
                             });

                if (order.Count() > 0)
                {
                    ModifiedOrder orderModify = new ModifiedOrder
                    {
                        OrderID = order.FirstOrDefault().OrderID,
                        ConfirmationID = bookingResouce.ConfirmationID,
                        CustomerName = order.FirstOrDefault().CustomerName,
                        CustomerEmail = order.FirstOrDefault().CustomerEmail,
                        EmailConfirmed = order.FirstOrDefault().EmailConfirmed,
                        CarBooked = order.FirstOrDefault().CarBooked,
                        VehicleType = order.FirstOrDefault().VehicleType,
                        CountryOfResidence = order.FirstOrDefault().CountryOfResidence,
                        PickUpDate = order.FirstOrDefault().PickUpDate,
                        DropOffDate = order.FirstOrDefault().DropOffDate,
                        OrderDate = order.FirstOrDefault().OrderDate,
                        PickUpLocation = order.FirstOrDefault().PickUpLocation,
                        DropOffLocation = order.FirstOrDefault().DropOffLocation,
                        NoDaysHired = order.FirstOrDefault().NoDaysHired,
                        AmountPaid = order.FirstOrDefault().AmountPaid,
                        XML_Provider = order.FirstOrDefault().XML_Provider,
                        XML_IATANumber = order.FirstOrDefault().XML_IATANumber,
                        XML_RateQualifier = order.FirstOrDefault().XML_RateQualifier
                    };
                    db.ModifiedOrders.Add(orderModify);
                    db.SaveChanges();

                    currencyCode = order.FirstOrDefault().AmountPaidCurrency;
                }

                Order ModOrder = (from o in db.Orders
                                  where o.ConfirmationID == bookingResouce.ConfirmationID
                                  select o).SingleOrDefault();

                ModOrder.PickUpDate = bookingResouce.PickupDateTime;
                ModOrder.DropOffDate = bookingResouce.DropoffDatetime;
                ModOrder.OrderDate = DateTime.Now;
                ModOrder.AmountPaid = Convert.ToDouble(bookingResouce.ReservedVehicle.Price);
                ModOrder.AmountPaidCurrency = bookingResouce.ReservedVehicle.Currency;
                ModOrder.CarBooked = bookingResouce.ReservedVehicle.Name;
                ModOrder.Modified = true;
                ModOrder.ModifiedDate = DateTime.Now;
                ModOrder.CorpDiscountNo = bookingResouce.CorpDiscountNo;

                db.SaveChanges();

                if (bookingResouce.IsExtraValid == true)
                {
                    int orderID = ModOrder.OrderID;

                    try
                    {
                        XmlNode PriceNode = xmlDocument.SelectSingleNode("//a1:PricedEquips", namespaceManager);
                        if (PriceNode != null)
                        {
                            XmlNodeList PriceNodeList = PriceNode.SelectNodes("//a1:PricedEquip", namespaceManager);
                            foreach (XmlNode accessory in PriceNodeList)
                            {
                                if (accessory != null)
                                {
                                    XmlNode EquipmentNode = accessory.SelectSingleNode(".//a1:Equipment", namespaceManager);
                                    XmlNode ChargeNode = accessory.SelectSingleNode(".//a1:Charge", namespaceManager);

                                    string accessoryType = EquipmentNode.Attributes["EquipType"].Value.Trim();
                                    double price = Convert.ToDouble(ChargeNode.Attributes["Amount"].Value.Trim());
                                    int quantity = Convert.ToInt32(EquipmentNode.Attributes["Quantity"].Value.Trim());
                                    string currency = currencyCode;

                                    AccessoriesOrdered accessoryOrdered = new AccessoriesOrdered
                                    {
                                        OrderIDFK = orderID,
                                        Accessory = accessoryType,
                                        Price = price,
                                        Quantity = quantity,
                                        Currency = currency
                                    };
                                    db.AccessoriesOrdereds.Add(accessoryOrdered);
                                    db.SaveChanges();
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        RemoteDebugger.SendXmlForDebugging(ex.StackTrace, "ErrorDB From Accessories");
                        RemoteDebugger.SendXmlForDebugging(ex.Message, "ErrorDBMessage From Accessories");
                    }
                }
            }
        }

        // -----------------------------------------------------------------------------------------------------

        // Cancel Methods
        private void addCancelNodes(VehicleCancelRQResources resources)
        {
            XmlNode POSNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:POS", namespaceManager);
            if (POSNode != null)
            {
                XmlElement SoceNode = xmlDocument.CreateElement("Source", Configs.XML_NAMESPACE);
                POSNode.AppendChild(SoceNode);

                XmlElement ReqId = xmlDocument.CreateElement("RequestorID", Configs.XML_NAMESPACE);
                ReqId.SetAttribute("Type", "4");
                ReqId.SetAttribute("ID", "CE8AD9499FC04010 -145180");
                SoceNode.AppendChild(ReqId);

                //if (resources.BrandName.ToString().ToUpper() == Brands.THRIFTY.ToString())
                //{
                //    XmlElement ReqId = xmlDocument.CreateElement("RequestorID", Configs.XML_NAMESPACE);
                //    ReqId.SetAttribute("Type", "4");
                //    ReqId.SetAttribute("ID", "CE8AD9499FC04010 -145180");
                //    SoceNode.AppendChild(ReqId);
                //}
                //else if (resources.BrandName.ToString().ToUpper() == Brands.DOLLAR.ToString())
                //{
                //    XmlElement ReqId = xmlDocument.CreateElement("RequestorID", Configs.XML_NAMESPACE);
                //    ReqId.SetAttribute("Type", "4");
                //    ReqId.SetAttribute("ID", "0390CE6F2758418F -145180");
                //    SoceNode.AppendChild(ReqId);
                //}
            }

            XmlNode OTA = xmlDocument.DocumentElement.SelectSingleNode("//a1:OTA_VehCancelRQ", namespaceManager);
            if (OTA != null)
            {
                if (OTA.Attributes["Target"] != null)
                {
                    OTA.Attributes["Target"].Value = "Production";
                }
                else
                {
                    XmlAttribute Target = xmlDocument.CreateAttribute("Target");
                    OTA.Attributes.Append(Target);
                    OTA.Attributes["Target"].Value = "Production";
                }
            }

            XmlNode cancelRQ = xmlDocument.DocumentElement.SelectSingleNode("//a1:VehCancelRQCore", namespaceManager);
            if (cancelRQ != null)
            {
                if (cancelRQ.Attributes["CancelType"] != null)
                {
                    cancelRQ.Attributes["CancelType"].Value = "Cancel";
                }
                else
                {
                    XmlAttribute type = xmlDocument.CreateAttribute("CancelType");
                    cancelRQ.Attributes.Append(type);

                    cancelRQ.Attributes["CancelType"].Value = "Cancel";
                }
            }

            XmlNode info = xmlDocument.DocumentElement.SelectSingleNode("//a1:VehCancelRQInfo", namespaceManager);
            if (info == null)
            {
                XmlElement vehinfo = xmlDocument.CreateElement("VehCancelRQInfo", Configs.XML_NAMESPACE);

                if (resources.BrandName.ToString().ToUpper() == Brands.THRIFTY.ToString())
                {
                    XmlElement vendorPref = xmlDocument.CreateElement("Vendor", Configs.XML_NAMESPACE);
                    vendorPref.SetAttribute("Code", "ZT");

                    vehinfo.AppendChild(vendorPref);
                }
                else if (resources.BrandName.ToString().ToUpper() == Brands.DOLLAR.ToString())
                {
                    XmlElement vendorPref = xmlDocument.CreateElement("Vendor", Configs.XML_NAMESPACE);
                    vendorPref.SetAttribute("Code", "ZR");

                    vehinfo.AppendChild(vendorPref);
                }

                XmlNode OTANode = xmlDocument.DocumentElement.SelectSingleNode("//a1:OTA_VehCancelRQ", namespaceManager);
                OTANode.AppendChild(vehinfo);
            }
        }

        private void setCancelRequestData(VehicleCancelRQResources bookingResouce)
        {
            XmlNode confirmationId = xmlDocument.DocumentElement.SelectSingleNode("//a1:ConfID", namespaceManager);
            if (confirmationId != null)
            {
                bookingResouce.ConfirmationID = confirmationId.Attributes["ID"].Value.Trim();
                try
                {

                    var order = (from o in db.Orders
                                 where (o.ConfirmationID == bookingResouce.ConfirmationID)
                                 select new
                                 {
                                     o.OrderID,
                                     o.CustomerName,
                                     o.CustomerEmail,
                                     o.EmailConfirmed,
                                     o.CarBooked,
                                     o.VehicleType,
                                     o.CountryOfResidence,
                                     o.PickUpDate,
                                     o.DropOffDate,
                                     o.OrderDate,
                                     o.PickUpLocation,
                                     o.DropOffLocation,
                                     o.NoDaysHired,
                                     o.AmountPaid,
                                     o.XML_Provider,
                                     o.XML_IATANumber,
                                     o.XML_RateQualifier
                                 });

                    if (order.Count() > 0)
                    {
                        CanclledOrder orderCancel = new CanclledOrder
                        {
                            OrderID = order.FirstOrDefault().OrderID,
                            ConfirmationID = bookingResouce.ConfirmationID,
                            CustomerName = order.FirstOrDefault().CustomerName,
                            CustomerEmail = order.FirstOrDefault().CustomerEmail,
                            EmailConfirmed = order.FirstOrDefault().EmailConfirmed,
                            CarBooked = order.FirstOrDefault().CarBooked,
                            VehicleType = order.FirstOrDefault().VehicleType,
                            CountryOfResidence = order.FirstOrDefault().CountryOfResidence,
                            PickUpDate = order.FirstOrDefault().PickUpDate,
                            DropOffDate = order.FirstOrDefault().DropOffDate,
                            OrderDate = order.FirstOrDefault().OrderDate,
                            PickUpLocation = order.FirstOrDefault().PickUpLocation,
                            DropOffLocation = order.FirstOrDefault().DropOffLocation,
                            NoDaysHired = order.FirstOrDefault().NoDaysHired,
                            AmountPaid = order.FirstOrDefault().AmountPaid,
                            XML_Provider = order.FirstOrDefault().XML_Provider,
                            XML_IATANumber = order.FirstOrDefault().XML_IATANumber,
                            XML_RateQualifier = order.FirstOrDefault().XML_RateQualifier
                        };
                        db.CanclledOrders.Add(orderCancel);
                        db.SaveChanges();
                    }

                    var dbCstInfo = db.Orders
                    .Where(w => w.ConfirmationID == bookingResouce.ConfirmationID);

                    if (dbCstInfo != null)
                    {
                        foreach (var cancel in dbCstInfo)
                        {
                            cancel.CancelledBooking = true;
                        }
                        db.SaveChanges();
                    }
                }
                catch (Exception e)
                {
                    RemoteDebugger.SendXmlForDebugging(e.Message, "Error Message");
                    RemoteDebugger.SendXmlForDebugging(e.StackTrace, "Error StackRace");
                }
            }
        }

        private void modifyVehicleDetailsCan(VehicleCancelRQResources resources)
        {
            XmlNode vehicle = xmlDocument.DocumentElement.SelectSingleNode("//a1:Vehicle", namespaceManager);
            if (vehicle != null)
            {
                db = new DatabaseEntities();
                var pick = from o in db.Orders
                           where o.ConfirmationID == resources.ConfirmationID
                           select new
                           {
                               pickup = o.PickUpLocation
                           };
                if (pick.Count() > 0)
                {
                    resources.PickupLocationCode = pick.FirstOrDefault().pickup.ToUpper();

                    // Vehicle Detals Swapping
                    string sippCode = vehicle.Attributes["Code"].Value.ToString().Trim();
                    var vehicleDetails = (from l in db.Locations
                                          join c in db.Countries on l.CountryIDFK equals c.CountryID
                                          join vr in db.VehicleResources on c.CountryID equals vr.CountryIDFK
                                          join vdb in db.VehicleDetailsByBrands on vr.VDBIDFK equals vdb.VDBID
                                          join b in db.Brands on vdb.BrandIDFK equals b.BrandID
                                          join cg in db.CarGroups on vdb.CarGroupIDFK equals cg.CarGroupID
                                          where b.BrandName == resources.BrandName
                                          && l.LocationCode == resources.PickupLocationCode
                                          && cg.CarCode == sippCode
                                          select new
                                          {
                                              vdb.CarDescription,
                                              vdb.CarImagePath,
                                              c.CountryCode
                                          });
                    XmlNode vehicleMakeModelNode = vehicle.SelectSingleNode(".//a1:VehMakeModel", namespaceManager);
                    XmlNode pictureUrlNode = vehicle.SelectSingleNode(".//a1:PictureURL", namespaceManager);
                    if (vehicleDetails.Count() > 0)
                    {
                        vehicleMakeModelNode.Attributes["Name"].Value = vehicleDetails.FirstOrDefault().CarDescription;
                        pictureUrlNode.InnerText = vehicleDetails.FirstOrDefault().CarImagePath;

                        //Remove Special Equipment (Winter Tyres if enabled)
                        //if (AppSettings.IsWiterTyresEnabled)
                        //{
                        //    //setWinterTyresForGermany(vehicleDetails.FirstOrDefault().CountryCode, vehicleNode);
                        //}
                    }
                    string vehicleName = vehicleMakeModelNode.Attributes["Name"].Value;
                    int indexOfSimilarText = vehicleName.IndexOf("OR SIMILAR");
                    if (indexOfSimilarText != -1)
                    {
                        vehicleMakeModelNode.Attributes["Name"].Value = vehicleName.Substring(0, indexOfSimilarText);
                    }
                }
                else
                {
                    throw new InvalidRequestException("No Pickup Location Code in Database");
                }
            }
        }
    }
}