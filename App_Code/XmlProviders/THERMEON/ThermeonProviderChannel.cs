﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;
using Exceptions;
using ORM;
using ProcessResources;
using TestClient;
using WebServiceMail;

/// <summary>
/// Summary description for ThermeonProviderChannel
/// </summary>
namespace THERMEON
{
    public class ThermeonProviderChannel : GenericProviderChannel
    {
        //protected XmlNamespaceManager namespaceMgr;
        public override void doProviderSpecificValidationsForRequest(VehicleAvailRateRQResources resources)
        {
            setXMLRequest(resources);

        }
        public override void doProviderSpecificValidationsForResponse(VehicleAvailRateRQResources resources)
        {
            if (hasErrorsInBookingResponse(resources))
            {
                set_AvailableErrorResponse();
            }
            else
            {
                LoadResponse(resources);
                string AvilaResponse = CreateResponse(resources);
                //RemoteDebugger.SendXmlForDebugging(AvilaResponse, "22222");
                xmlDocument.LoadXml(AvilaResponse);
            }
        }

        public override void doProviderSpecificValidationsForRequest(VehicleResRQResources resources)
        {
            //do validation
            setReservation_Data(resources);
            setAccessoriesData(resources);
            set_Reservation_Request(resources);
            Reverse_setReservation_Locations(resources);
            savePreBookingInDatabase(resources);
        }
        public override void doProviderSpecificValidationsForResponse(VehicleResRQResources resources)
        {
            //do validation
            if (hasErrorsInBookingResponse(resources))
            {
                set_ErrorResponse();
            }
            else
            {
                setReservationData_DB(resources);
                Load_ReservationResponse(resources);
                SendEmail(resources);
                saveBookingInDatabase(resources);
            }
        }
        public override void doProviderSpecificValidationsForRequest(VehicleRetrivaleRQResources resources)
        {
            //throw new NotImplementedException();
            set_RetriveRequest(resources);
        }
        public override void doProviderSpecificValidationsForResponse(VehicleRetrivaleRQResources resources)
        {
            //throw new NotImplementedException();
            set_RetrivalResponse(resources);
        }
        public override void doProviderSpecificValidationsForRequest(VehicleResModifyRQResources resources)
        {
            //throw new NotImplementedException();
            set_ModificationRequest(resources);
            set_ModifyRequest(resources);
            Reverse_setReservation_Locations(resources);
        }

        public override void doProviderSpecificValidationsForResponse(VehicleResModifyRQResources resources)
        {
            //throw new NotImplementedException();
            if (hasErrorsInBookingResponse(resources))
            {
                set_ModifyErrorResponse();
            }
            else
            {
                setModificationData_DB(resources);
                Load_ModificationResponse(resources);
                setModifyRequestData(resources);
            }
        }
        public override void doProviderSpecificValidationsForRequest(VehicleCancelRQResources resources)
        {
            //do validation
            set_CancelRequest(resources);
        }
        public override void doProviderSpecificValidationsForResponse(VehicleCancelRQResources resources)
        {
            //throw new NotImplementedException();
            set_CancelResponse(resources);
            setCancelData_toDB(resources);
        }
        public override void doProviderSpecificValidationsForRequest(VehicleRentalRQResources resources)
        {
            //throw new NotImplementedException();
        }
        public override void doProviderSpecificValidationsForResponse(VehicleRentalRQResources resources)
        {
            //throw new NotImplementedException();
        }

        //Available methods
        private void setXMLRequest(VehicleAvailRateRQResources resources)
        {
            set_LocationId(resources);

            string pickDateTime = resources.PickupDateTime.ToString().TrimEnd().TrimStart();
            pickDateTime = resources.PickupDateTime.ToString("s");

            string dropDateTime = resources.DropoffDatetime.ToString().TrimEnd().TrimStart();
             dropDateTime = resources.DropoffDatetime.ToString("s");



             string Request = "<Request xmlns=\"http://www.thermeon.com/webXG/xml/webxml/\" referenceNumber=\"12345\" version=\"2.2600\">" +
                              "<ResRates>" +
                              "<Pickup locationCode=\"" + resources.PickupLocationCode + "\" dateTime=\"" + pickDateTime + "\"/>" +
                              "<Return locationCode=\"" + resources.DropoffLocationCode + "\" dateTime=\"" + dropDateTime + "\"/>" +
                              "<CorpRateID>API</CorpRateID>" +
                              "<EstimateType>3</EstimateType>" +
                              "</ResRates>" +
                              "</Request>";


            xmlDocument.LoadXml(Request);
        }

        private void set_LocationId(VehicleAvailRateRQResources resources)
        {
            XmlNode picLoc = xmlDocument.DocumentElement.SelectSingleNode("//a1:PickUpLocation", namespaceManager);
            XmlNode dropLoc = xmlDocument.DocumentElement.SelectSingleNode("//a1:ReturnLocation", namespaceManager);

            var PicklocationId = (from l in db.Locations
                                  where l.LocationCode == resources.PickupLocationCode
                                  select new
                                  {
                                      l.ThermeonLocation
                                  });

            if (PicklocationId.FirstOrDefault() != null)
            {
                resources.PickupLocationCode = PicklocationId.FirstOrDefault().ThermeonLocation.ToString();
            }
            else
            {
                throw new InvalidRequestException("PickUp Locations not Identified");
            }

            var DroplocationId = (from l in db.Locations
                                  where l.LocationCode == resources.DropoffLocationCode
                                  select new
                                  {
                                      l.ThermeonLocation
                                  });

            if (DroplocationId.FirstOrDefault() != null)
            {
                resources.DropoffLocationCode = DroplocationId.FirstOrDefault().ThermeonLocation.ToString();
            }
            else
            {
                throw new InvalidRequestException("DropOff Locations not Identified");
            }
        }

        private bool hasErrorsInBookingResponse(VehicleAvailRateRQResources resources)
        {
            setNamespaceManager(xmlDocument);

            XmlNode reservation = xmlDocument.DocumentElement.SelectSingleNode("//a1:ResRates", namespaceManager);
            if (reservation != null)
            {

                if (reservation.Attributes["success"].Value.Trim() == "false")
                {
                    return true;
                }
            }
            return false;
        }

        private void set_AvailableErrorResponse()
        {
            XmlNode errorMessageNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:Message", namespaceManager);

            string MsgType = errorMessageNode.Attributes["number"].Value.Trim();

            XmlNode errorTextNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:Text", namespaceManager);

            string ErrText = errorTextNode.InnerText.Trim();

            string errorXml = "<?xml version=\"1.0\" encoding=\"utf-16\"?>" +
                              "<OTA_VehResRS xmlns=\"http://www.opentravel.org/OTA/2003/05\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.opentravel.org/OTA/2003/05\">" +
                              "<Errors>" +
                              "<Error Type=\"" + MsgType + "\" ShortText=\"" + ErrText + "\" Code=\"" + MsgType + "\" />" +
                              "</Errors>" +
                              "</OTA_VehResRS>";

            xmlDocument.LoadXml(errorXml);
        }

        private void LoadResponse(VehicleAvailRateRQResources resources)
        {
            setNamespaceManager(xmlDocument);
            LoadCars(xmlDocument, resources);
        }

        private string CreateResponse(VehicleAvailRateRQResources resources)
        {

            string hading = "<?xml version=\"1.0\" encoding=\"UTF-8\"?> " +
              "<OTA_VehAvailRateRS xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns=\"http://www.opentravel.org/OTA/2003/05\" Target=\"Qual\" Version=\"2.007\"> " +
              "  <Success /> " +
              "  <VehAvailRSCore> " +
              "    <VehRentalCore PickUpDateTime=\"" + resources.PickupDateTime.ToString("s") + "\" ReturnDateTime=\"" + resources.DropoffDatetime.ToString("s") + "\"> " +
              "      <PickUpLocation ExtendedLocationCode=\"" + resources.PickupLocationExtendedCode + "\" LocationCode=\"" + resources.PickupLocationCode + "\" CodeContext=\"IATA\" /> " +
              "      <ReturnLocation ExtendedLocationCode=\"" + resources.DropoffLocationExtendedCode + "\" LocationCode=\"" + resources.DropoffLocationCode + "\" CodeContext=\"IATA\" /> " +
              "    </VehRentalCore> " +
              "    <VehVendorAvails> " +
              "      <VehVendorAvail> " +
              "        <Vendor Code=\"FF\" /> " +
              "        <VehAvails> ";

            string car_body = "";
            foreach (var car in resources.Cars)
            {
                car_body = car_body + "<VehAvail> " +
            "            <VehAvailCore Status=\"" + car.Availability + "\" IsAlternateInd=\"true\"> " +
            "              <Vehicle PassengerQuantity=\"" + car.CarPassengers + "\" BaggageQuantity=\"" + car.Baggage_Count + "\" AirConditionInd=\"" + car.AirConditionIndValue + "\" TransmissionType=\"" + car.TransmissionType + "\" FuelType=\"" + car.FuelType + "\" DriveType=\"" + car.DriveType + "\" Code=\"" + car.SippCode + "\" CodeContext=\"SIPP\"> " +
            "                <VehType VehicleCategory=\"" + car.VehicleCategory + "\" />" +
            "                <VehMakeModel Name=\"" + car.Name + "\" Code=\"" + car.SippCode + "\" /> " +
            "                 <PictureURL>" + car.Image + "</PictureURL>" +
            "              </Vehicle> " +
            "              <RentalRate> " +
            "                <RateDistance Unlimited=\"true\" DistUnitName=\"Km\" VehiclePeriodUnitName=\"RentalPeriod\" /> " +
            "                <VehicleCharges> " +
            "                  <VehicleCharge Purpose=\"1\" TaxInclusive=\"false\" GuaranteedInd=\"true\" Amount=\"" + car.Price + "\" CurrencyCode=\"" + car.CurrencySymbol + "\" IncludedInRate=\"false\"> " +
            "                    <Calculation UnitCharge=\"1.14\" UnitName=\"Day\" Quantity=\"4\" /> " +
            "                  </VehicleCharge> " +
            "                </VehicleCharges> " +
            "                <RateQualifier ArriveByFlight=\"true\" RateQualifier=\"RTG2EU\" /> " +
            "              </RentalRate> " +
            "              <TotalCharge RateTotalAmount=\"" + car.Price + "\" EstimatedTotalAmount=\"" + car.Price + "\" CurrencyCode=\"" + car.CurrencySymbol + "\" /> " +
            "              <Reference Type=\"16\" ID=\"" + car.ThemeonID + "\" /> " +
            "            </VehAvailCore> " +
            "            <VehAvailInfo> " +
            "              <PaymentRules> " +
            "                <PaymentRule RuleType=\"2\" Amount=\"" + car.Price + "\" CurrencyCode=\"" + car.CurrencySymbol + "\" /> " +
            "              </PaymentRules> " +
            "            </VehAvailInfo> </VehAvail>";


            }

            string fotter = "</VehAvails>  " +
            "      </VehVendorAvail> " +
            "    </VehVendorAvails> " +
            "  </VehAvailRSCore> " +
            "</OTA_VehAvailRateRS> ";

            return hading + car_body + fotter;
        }

        private string setAccessoriesNode()
        {
            string accessNode = "";

            XmlNode Accessories = xmlDocument.DocumentElement.SelectSingleNode("//a1:SpecialEquipPrefs", namespaceManager);
            if (Accessories != null)
            {
                XmlNodeList accessoryNodeList = Accessories.SelectNodes(".//a1:SpecialEquipPref", namespaceManager);
                foreach (XmlNode accessory in accessoryNodeList)
                {
                    if (accessory != null)
                    {
                        if (accessory.Attributes != null)
                        {
                            string hetrzCode = accessory.Attributes["EquipType"].Value;
                            int quantity = Convert.ToInt32(accessory.Attributes["Quantity"].Value);

                            string ThermeonCode = getThermeonAccessCode(hetrzCode);

                            accessNode = "<Option>" +
                                         "<Code>" + ThermeonCode + "</Code>" +
                                         "<Qty>1</Qty>" +
                                         "</Option>";
                        }
                    }
                }
            }
            return accessNode;
        }

        private string getThermeonAccessCode(string hertzCode)
        {
            var ThermeonId = (from a in db.Accessories
                              where a.HertzCode == hertzCode
                              select new
                              {
                                  a.Thermeon_Code
                              });
            if (ThermeonId.FirstOrDefault() != null)
            {
                return ThermeonId.FirstOrDefault().Thermeon_Code.ToString();
            }
            else
            {
                return "";
            }
        }      
       
        protected void setNamespaceManager(XmlDocument xmlDocument)
        {
            //RemoteDebugger.SendXmlForDebugging(xmlDocument.ToString(), "NameSpace");
            this.namespaceManager = new XmlNamespaceManager(xmlDocument.NameTable);
            this.namespaceManager.AddNamespace("a1", Configs.THERMEON_XML_NAMESPACE);

        }


        protected void LoadCars(XmlDocument vehAvailRateRSXML,VehicleAvailRateRQResources resources)
        {

            XmlNodeList nodeList = vehAvailRateRSXML.DocumentElement.SelectNodes("//a1:Rate", namespaceManager);

            var termeonCarDetailsList = (from tm in db.Thermeon_CarMapping 
                                         join l in db.Locations on tm.CountryId equals l.CountryIDFK
                                         join cg in db.CarGroups on tm.CarCode equals cg.CarGroupID
                                         join tc in db.Thermeon_CarDetails on tm.ThermeonCarId equals tc.TCId 
                                         where l.ThermeonLocation == resources.PickupLocationCode 
                                         select new 
                                         {
                                             tc.Car_Description,
                                             tc.CarImage_Path,
                                             tc.Passenger_Count,
                                             tc.Baggage_Count,
                                             tc.AirConditionInd,
                                             tc.TransmissionType,
                                             tc.FuelType,
                                             tc.DriveType,
                                             tc.Door_Count,
                                             tc.VehicleCategory,
                                             cg.CarCode
                                         });


            if (nodeList.Count > 0)
            {
                
                foreach (XmlNode node in nodeList)
                {
                    var car = new CarDetails();

                    car.ThemeonID = node.SelectSingleNode(".//a1:RateID", namespaceManager).InnerText;
                    car.SippCode = node.SelectSingleNode(".//a1:Class", namespaceManager).InnerText;
                    car.Availability = node.SelectSingleNode(".//a1:Availability", namespaceManager).InnerText;
                    car.CurrencySymbol = node.SelectSingleNode(".//a1:CurrencyCode", namespaceManager).InnerText;
                    car.Price = node.SelectSingleNode(".//a1:Estimate", namespaceManager).InnerText;

                    if (node.SelectSingleNode(".//a1:PrePaid", namespaceManager).InnerText == "false")
                    {
                        car.IsPrePaid = false;
                    }
                    else
                    {
                        car.IsPrePaid = true;
                    }

                    var carDetails = termeonCarDetailsList.FirstOrDefault(td => td.CarCode == car.SippCode);

                    

                    if (carDetails.Car_Description == "")
                    {
                        car.Name = "Unspecified";
                    }
                    else
                    {
                        car.Name = carDetails.Car_Description;
                    }


                    if (carDetails.CarImage_Path == "")
                    {
                        car.Image= "Unspecified";
                    }
                    else
                    {
                        car.Image = carDetails.CarImage_Path;
                    }


                    if (carDetails.AirConditionInd.ToString() == "")
                    {
                        car.AirConditionIndValue = "Unspecified";
                    }
                    else
                    {
                        car.AirConditionIndValue = carDetails.AirConditionInd.ToString();
                    }


                   // car.AirConditionIndValue = carDetails.AirConditionInd;
                    if (carDetails.Baggage_Count.ToString() == "")
                    {
                        car.Baggage_Count = "Unspecified";
                    }
                    else
                    {
                        car.Baggage_Count = carDetails.Baggage_Count.ToString();
                    }

                    if (carDetails.Passenger_Count.ToString() == "")
                    {
                        car.CarPassengers = "Unspecified";
                    }
                    else
                    {
                        car.CarPassengers = carDetails.Passenger_Count.ToString();
                    }

                    if (carDetails.TransmissionType == null)
                    {
                        car.TransmissionType = "Unspecified";
                    }
                    else
                    {
                        car.TransmissionType = carDetails.TransmissionType;
                    }

                    if (carDetails.DriveType == null)
                    {
                        car.DriveType = "Unspecified";
                    }
                    else
                    {
                        car.DriveType = carDetails.DriveType;
                    }

                    if (carDetails.FuelType == null)
                    {
                        car.FuelType = "Unspecified";
                    }
                    else
                    {
                        car.FuelType = carDetails.FuelType;
                    }

                    if (carDetails.Door_Count.ToString() == "")
                    {
                        car.DoorCount = "Unspecified";
                    }
                    else
                    {
                        car.DoorCount = carDetails.Door_Count.ToString();
                    }

                    if (carDetails.VehicleCategory.ToString() == "")
                    {
                        car.VehicleCategory = "Unspecified";
                    }
                    else
                    {
                        car.VehicleCategory = carDetails.VehicleCategory.ToString();
                    }
                    
                    

                    resources.Cars.Add(car);
                }
            }
            else
            {
                
            }
        }

        //-----------------------------------------------------------------------------------------------------------

        //Reservation Methods

        private void setReservation_Data(VehicleResRQResources resources)
        {
            XmlNode firstNameNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:GivenName", namespaceManager);
            if (firstNameNode != null)
            {
                resources.CustomerFirstname = firstNameNode.InnerText.Trim();
            }
            XmlNode surNameNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:Surname", namespaceManager);
            if (surNameNode != null)
            {
                resources.CustomerSurname = surNameNode.InnerText.Trim();
            }
            XmlNode customerEmailNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:Email", namespaceManager);
            if (customerEmailNode != null)
            {
                resources.CustomerEmail = customerEmailNode.InnerText.Trim();
            }
            XmlNode customerPhoneNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:Telephone", namespaceManager);
            if (customerPhoneNode != null)
            {
                resources.CustomerPhone = customerPhoneNode.Attributes["PhoneNumber"].Value.Trim();
            }
            XmlNode customerAddressNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:AddressLine", namespaceManager);
            if (customerAddressNode != null)
            {
                resources.Address = customerAddressNode.InnerText.Trim();
            }
            XmlNode customerCityNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:CityName", namespaceManager);
            if (customerCityNode != null)
            {
                resources.CityName = customerCityNode.InnerText.Trim();
            }
            XmlNode customerPostalCodeNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:PostalCode", namespaceManager);
            if (customerPostalCodeNode != null)
            {
                resources.PostalCode = customerPostalCodeNode.InnerText.Trim();
            }
            XmlNode customerCountryNameNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:CountryName", namespaceManager);
            if (customerCountryNameNode != null)
            {
                resources.CountryOfResidenceCode = customerCountryNameNode.Attributes["Code"].Value.Trim();
            }
            XmlNode primary = xmlDocument.DocumentElement.SelectSingleNode("//a1:Primary", namespaceManager);
            if (primary != null)
            {
                if (primary.Attributes["BirthDate"] != null)
                {
                    resources.BirthDay = primary.Attributes["BirthDate"].Value.Trim();
                }

                XmlNodeList document = primary.SelectNodes(".//a1:Document", namespaceManager);
                foreach (XmlNode doc in document)
                {
                    if (doc != null)
                    {
                        if (doc.Attributes["DocType"].Value == "4")
                        {
                            resources.DriverLicence = doc.Attributes["DocID"].Value.Trim();
                            resources.expireDate = doc.Attributes["ExpireDate"].Value.Trim();
                        }
                        else if (doc.Attributes["DocType"].Value == "2")
                        {
                            resources.passportNo = doc.Attributes["DocID"].Value.Trim();
                            resources.IssueCountry = doc.Attributes["DocIssueCountry"].Value.Trim();
                        }
                    }
                }
            }

            //vehicle details

            resources.VehicleDetailsList = new List<VehicleDetail>();

            XmlNode vehCodeNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:VehPref", namespaceManager);
            XmlNode vehRateIdNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:Reference", namespaceManager);
            if (vehCodeNode != null && vehRateIdNode != null)
            {
                VehicleDetail vehicle = new VehicleDetail
                {
                    SippCode = vehCodeNode.Attributes["Code"].Value.Trim(),
                    RateID = vehRateIdNode.Attributes["ID"].Value.Trim()
                };
                resources.VehicleDetailsList.Add(vehicle);
            }
        }

        private void setAccessoriesData(VehicleResRQResources bookingResouce)
        {
            XmlNode accessoryNode = xmlDocument.SelectSingleNode("//a1:SpecialEquipPrefs", namespaceManager);
            if (accessoryNode != null)
            {
                bookingResouce.AccesoriesList = new List<Accessory>();
                XmlNodeList accessoryNodeList = accessoryNode.SelectNodes(".//a1:SpecialEquipPref", namespaceManager);
                foreach (XmlNode accessory in accessoryNodeList)
                {
                    if (accessory != null)
                    {
                        if (accessory.Attributes != null)
                        {
                            string accessoryType = accessory.Attributes["EquipType"].Value.Trim();
                            var accessoryDbObj = (from a in db.Accessories
                                                  where a.HertzCode == accessoryType
                                                  select new
                                                  {
                                                      a.AccessoryID,
                                                      a.Image1,
                                                      a.Title,
                                                      a.Price,
                                                      a.Details,
                                                      a.HertzCode
                                                  });
                            string accessoryImage = "Default picture";
                            string accessoryTitle = "Default Name";
                            if (accessoryDbObj != null)
                            {
                                accessoryImage = accessoryDbObj.FirstOrDefault().Image1;
                                accessoryTitle = HttpContext.GetGlobalResourceObject("SiteStrings", "Extra_" + accessoryDbObj.FirstOrDefault().HertzCode + "_Title").ToString();


                                Accessory accessoryObj = new Accessory
                                {
                                    AccessoryID = accessoryDbObj.FirstOrDefault().AccessoryID,
                                    Price = accessoryDbObj.FirstOrDefault().Price,
                                    Title = accessoryDbObj.FirstOrDefault().Title,
                                    Image1 = accessoryDbObj.FirstOrDefault().Image1,
                                    HertzCode = accessoryDbObj.FirstOrDefault().HertzCode,
                                    Details = accessoryDbObj.FirstOrDefault().Details
                                };
                                bookingResouce.AccesoriesList.Add(accessoryObj);
                            }
                        }
                    }
                }
            }
        }

        private void set_Reservation_Request(VehicleResRQResources resources)
        {
            setReservation_Locations(resources);

            string accessories = "";
            string bDay = "";
            string driveDet = "";
            //string passprt = "";
            //string issueCount = "";

            if (resources.AccesoriesList != null)
            {
                accessories = setAccessoriesNode();
            }
            if(resources.BirthDay != null)
            {
                bDay = "<BirthDate>" + resources.BirthDay + "</BirthDate>";
            }
            if (resources.DriverLicence != null && resources.expireDate != null)
            {
                driveDet = "<DrivingLicence Number=\"" + resources.DriverLicence + "\" ExpiryDate=\"" + resources.expireDate + "\"/>";
            }

            string UniqNum = DateTime.Now.ToString("yyyyMMddHHmmssfff");
            resources.Thermeon_ConfirmationID = UniqNum.Substring(7, 10);

            string pickDateTime = resources.PickupDateTime.ToString().TrimEnd().TrimStart();
            pickDateTime = resources.PickupDateTime.ToString("s");

            string dropDateTime = resources.DropoffDatetime.ToString().TrimEnd().TrimStart();
            dropDateTime = resources.DropoffDatetime.ToString("s");

            string Request = "<Request xmlns=\"http://www.thermeon.com/webXG/xml/webxml/\" referenceNumber=\"12345\" version=\"2.2600\">"+
                             "<NewReservationRequest confirmAvailability=\"true\">"+
                             "<Pickup locationCode=\""+resources.PickupLocationCode+"\" dateTime=\""+pickDateTime+"\"/>"+
                             "<Return locationCode=\""+resources.DropoffLocationCode+"\" dateTime=\""+dropDateTime+"\"/>"+
                             "<Source confirmationNumber=\"" + resources.Thermeon_ConfirmationID + "\" countryCode=\"UK\"/>" +
                             "<Vehicle classCode=\"" + resources.VehicleDetailsList.FirstOrDefault().SippCode + "\"/>" +
                             "<Renter>"+
                             "<RenterName firstName=\"" + resources.CustomerFirstname + "\" lastName=\"" + resources.CustomerSurname + "\"/>" +
                             "<Address>"+
                             "<Street>" + resources.Address + "</Street>" +
                             "<City>" + resources.CityName + "</City>" +
                             "<PostalCode>" + resources.PostalCode + "</PostalCode>" +
                             "<CountryCode>" + resources.CountryOfResidenceCode + "</CountryCode>" +
                             "<Email>" + resources.CustomerEmail + "</Email>" +
                             "<HomeTelephoneNumber>"+ resources.CustomerPhone +"</HomeTelephoneNumber>" +
                             "</Address>"+
                             bDay +
                             driveDet +
                             "</Renter>"+
                             "<QuotedRate rateID=\"" + resources.VehicleDetailsList.FirstOrDefault().RateID + "\" classCode=\"" + resources.VehicleDetailsList.FirstOrDefault().SippCode + "\" corporateRateID=\"API\"/>" +
                             accessories +
                             "</NewReservationRequest>"+
                             "</Request>";


            xmlDocument.LoadXml(Request);
        }

        private void setReservation_Locations(VehicleResRQResources resources)
        {
            var PicklocationId = (from l in db.Locations
                                  where l.LocationCode == resources.PickupLocationCode
                                  select new
                                  {
                                      l.ThermeonLocation
                                  });

            if (PicklocationId.FirstOrDefault() != null)
            {
                resources.PickupLocationCode = PicklocationId.FirstOrDefault().ThermeonLocation.ToString();
            }
            else
            {
                throw new InvalidRequestException("PickUp Locations not Identified");
            }

            var DroplocationId = (from l in db.Locations
                                  where l.LocationCode == resources.DropoffLocationCode
                                  select new
                                  {
                                      l.ThermeonLocation
                                  });

            if (DroplocationId.FirstOrDefault() != null)
            {
                resources.DropoffLocationCode = DroplocationId.FirstOrDefault().ThermeonLocation.ToString();
            }
            else
            {
                throw new InvalidRequestException("DropOff Locations not Identified");
            }
        }

        private void Reverse_setReservation_Locations(VehicleResRQResources resources)
        {
            var PicklocationId = (from l in db.Locations
                                  where l.ThermeonLocation == resources.PickupLocationCode
                                  select new
                                  {
                                      l.LocationCode
                                  });

            if (PicklocationId.FirstOrDefault() != null)
            {
                resources.PickupLocationCode = PicklocationId.FirstOrDefault().LocationCode.ToString();
            }
            else
            {
                throw new InvalidRequestException("Reverse PickUp Locations not Identified");
            }

            var DroplocationId = (from l in db.Locations
                                  where l.ThermeonLocation == resources.DropoffLocationCode
                                  select new
                                  {
                                      l.LocationCode
                                  });

            if (DroplocationId.FirstOrDefault() != null)
            {
                resources.DropoffLocationCode = DroplocationId.FirstOrDefault().LocationCode.ToString();
            }
            else
            {
                throw new InvalidRequestException("Reverse DropOff Locations not Identified");
            }
        }

        private void savePreBookingInDatabase(VehicleResRQResources resources)
        {
            BokkingRequest preBookingObj = new BokkingRequest
            {
                CustomerName = resources.CustomerFirstname + " " + resources.CustomerSurname,
                CustomerEmail = resources.CustomerEmail,
                CustomerPhone = resources.CustomerPhone,
                PickUpDate = resources.PickupDateTime,
                DropOffDate = resources.DropoffDatetime,
                PickUpLocation = resources.PickupLocationCode,
                DropOffLocation = resources.DropoffLocationCode,
                CustomerSurname = resources.CustomerSurname,
                CustomerFirstname = resources.CustomerFirstname,
                AddressLine1 = resources.Address,
                AddressLine2 = resources.CityName,
                AddressTown = resources.CityName,
                AddressPostCodeZip = resources.PostalCode,
                CountryOfResidence = resources.CountryOfResidenceCode,
                CameFrom = resources.PartnerName,
                XML_Provider = resources.ProviderName,
                OrderDate = DateTime.Now
            };
            db.BokkingRequests.Add(preBookingObj);
            db.SaveChanges();
        }

        private bool hasErrorsInBookingResponse(VehicleResRQResources resources)
        {
            setNamespaceManager(xmlDocument);

            XmlNode reservation = xmlDocument.DocumentElement.SelectSingleNode("//a1:NewReservationResponse", namespaceManager);

            if (reservation.Attributes["success"].Value.Trim() == "false")
            {
                return true;
            }
            return false;
        }

        private void set_ErrorResponse()
        {
            XmlNode errorMessageNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:Message", namespaceManager);

            string MsgType = errorMessageNode.Attributes["number"].Value.Trim();

            XmlNode errorTextNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:Text", namespaceManager);

            string ErrText = errorTextNode.InnerText.Trim();

            string errorXml = "<?xml version=\"1.0\" encoding=\"utf-16\"?>" +
                              "<OTA_VehResRS xmlns=\"http://www.opentravel.org/OTA/2003/05\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.opentravel.org/OTA/2003/05\">" +
                              "<Errors>" +
                              "<Error Type=\"" + MsgType + "\" ShortText=\"" + ErrText + "\" Code=\"" + MsgType + "\" />" +
                              "</Errors>" +
                              "</OTA_VehResRS>";

            xmlDocument.LoadXml(errorXml);
        }

        private void setReservationData_DB(VehicleResRQResources resources)
        {
            string Group = resources.VehicleDetailsList.FirstOrDefault().SippCode;
            var termeonCarDetail = (from tc in db.Thermeon_CarDetails
                                    join tm in db.Thermeon_CarMapping on tc.TCId equals tm.ThermeonCarId
                                    join cg in db.CarGroups on tm.CarCode equals cg.CarGroupID
                                    join c in db.Countries on tm.CountryId equals c.CountryID
                                    join l in db.Locations on c.CountryID equals l.CountryIDFK
                                    where cg.CarCode == Group
                                    && l.LocationCode == resources.DropoffLocationCode.TrimEnd().TrimStart()
                                    select new
                                    {
                                        tc.Car_Description,
                                        tc.CarImage_Path,
                                        tc.Passenger_Count,
                                        tc.Baggage_Count,
                                        tc.AirConditionInd,
                                        tc.TransmissionType,
                                        tc.FuelType,
                                        tc.DriveType,
                                        tc.Door_Count,
                                        tc.VehicleCategory,
                                        cg.CarCode
                                    });

            if (termeonCarDetail.Count() > 0)
            {
                resources.ReservedVehicle = new BookedVehicle();
                resources.ReservedVehicle.Name = termeonCarDetail.FirstOrDefault().Car_Description;
                resources.ReservedVehicle.ImagePath = termeonCarDetail.FirstOrDefault().CarImage_Path;
                resources.ReservedVehicle.Passenger_Count = Convert.ToInt32(termeonCarDetail.FirstOrDefault().Passenger_Count);
                resources.ReservedVehicle.Baggage_Count = Convert.ToInt32(termeonCarDetail.FirstOrDefault().Baggage_Count);
                resources.ReservedVehicle.AirCond = Convert.ToBoolean(termeonCarDetail.FirstOrDefault().AirConditionInd);
                resources.ReservedVehicle.TransmissionTP = termeonCarDetail.FirstOrDefault().TransmissionType;
                resources.ReservedVehicle.FuelTP = termeonCarDetail.FirstOrDefault().FuelType;
                resources.ReservedVehicle.DriverTP = termeonCarDetail.FirstOrDefault().DriveType;
                resources.ReservedVehicle.Veh_Category = Convert.ToInt32(termeonCarDetail.FirstOrDefault().VehicleCategory);
                resources.ReservedVehicle.Door_Count = Convert.ToInt32(termeonCarDetail.FirstOrDefault().Door_Count);
            }
            else
            {
                throw new InvalidRequestException("No selected CarGroup in DB");
            }
        }

        private void Load_ReservationResponse(VehicleResRQResources resources)
        {
            XmlNode reservation = xmlDocument.DocumentElement.SelectSingleNode("//a1:NewReservationResponse", namespaceManager);

            resources.ConfirmationID = reservation.Attributes["reservationNumber"].Value.Trim();

            //string accessoryPart = "";

            //if (resources.AccesoriesList != null)
            //{
            //    accessoryPart = "<PricedEquips>" +
            //                    "<PricedEquip>" +
            //                    "<Equipment EquipType=\"8\" Quantity=\"1\"/>" +
            //                    "<Charge TaxInclusive=\"false\" Amount=\"30.00\" CurrencyCode=\"GBP\" IncludedInRate=\"false\"/>" +
            //                    "</PricedEquip>" +
            //                    "</PricedEquips>";
            //}

            string reservationResponse = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><OTA_VehResRS xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns=\"http://www.opentravel.org/OTA/2003/05\" Target=\"Qual\" Version=\"2.007\">" +
                                         "<Success />" +
                                         "<VehResRSCore><VehReservation>" +
                                         "<Customer><Primary><PersonName>" +
                                         "<GivenName>" + resources.CustomerFirstname + "</GivenName>" +
                                         "<Surname>" + resources.CustomerSurname + "</Surname>" +
                                         "</PersonName></Primary></Customer>" +
                //"<VehSegmentCore>" +
                                         "<ConfID Type=\"14\" ID=\"" + resources.ConfirmationID + "\" /><Vendor Code=\"FF\" />" +
                                         "<VehRentalCore PickUpDateTime=\"" + resources.PickupDateTime + "\" ReturnDateTime=\"" + resources.DropoffDatetime + "\">" +
                                         "<PickUpLocation ExtendedLocationCode=\"" + resources.PickupLocationExtendedCode.TrimEnd() + "\" LocationCode=\"" + resources.PickupLocationExtendedCode.TrimEnd() + "\" CodeContext=\"IATA\" />" +
                                         "<ReturnLocation ExtendedLocationCode=\"" + resources.DropoffLocationExtendedCode.TrimEnd() + "\" LocationCode=\"" + resources.DropoffLocationExtendedCode.TrimEnd() + "\" CodeContext=\"IATA\" />" +
                                         "</VehRentalCore>" +
                                         "<Vehicle PassengerQuantity=\"" + resources.ReservedVehicle.Passenger_Count + "\" BaggageQuantity=\"" + resources.ReservedVehicle.Baggage_Count + "\" AirConditionInd=\"" + resources.ReservedVehicle.AirCond + "\" TransmissionType=\"" + resources.ReservedVehicle.TransmissionTP + "\" FuelType=\"" + resources.ReservedVehicle.FuelTP + "\" DriveType=\"" + resources.ReservedVehicle.DriverTP + "\" Code=\"" + resources.VehicleDetailsList.FirstOrDefault().SippCode + "\" CodeContext=\"SIPP\">" +
                                         "<VehType VehicleCategory=\"" + resources.ReservedVehicle.Veh_Category + "\" /><VehClass Size=\"3\" /><VehMakeModel Name=\"" + resources.ReservedVehicle.Name + "\" Code=\"" + resources.VehicleDetailsList.FirstOrDefault().SippCode + "\" /><PictureURL>" + resources.ReservedVehicle.ImagePath + "</PictureURL></Vehicle>" +
                //"<RentalRate>" +
                //"<RateDistance Unlimited=\"true\" DistUnitName=\"Km\" VehiclePeriodUnitName=\"RentalPeriod\" />" +
                //"<VehicleCharges>" +
                //"<VehicleCharge Purpose=\"1\" TaxInclusive=\"false\" GuaranteedInd=\"true\" Amount=\"89.44\" CurrencyCode=\"EUR\" IncludedInRate=\"false\">" +
                //"<TaxAmounts>" +
                //"<TaxAmount Total=\"15.17\" CurrencyCode=\"EUR\" Percentage=\"16.00\" Description=\"Tax\" />" +
                //"</TaxAmounts>" +
                //"<Calculation UnitCharge=\"22.36\" UnitName=\"Day\" Quantity=\"4\" />" +
                //"</VehicleCharge>" +
                //"</VehicleCharges>" +
                //"<RateQualifier ArriveByFlight=\"true\" RateQualifier=\"RTG2EU\" />" +
                //"</RentalRate>" +
                //"<Fees>" +
                //"<Fee Purpose=\"5\" TaxInclusive=\"false\" Description=\"LOCATION SERVICE CHARGE:\" IncludedInRate=\"false\" Amount=\"5.37\" CurrencyCode=\"EUR\" />" +
                //"</Fees>" +
                //"<TotalCharge RateTotalAmount=\"89.44\" EstimatedTotalAmount=\"109.98\" CurrencyCode=\"EUR\" />" +
                //"</VehSegmentCore>" +
                //"<VehSegmentInfo>" +
                //"<PaymentRules>" +
                //"<PaymentRule RuleType=\"2\" Amount=\"109.98\" CurrencyCode=\"EUR\" />" +
                //"</PaymentRules>" +
                //"<PricedCoverages>" +
                //"<PricedCoverage Required=\"false\">" +
                //"<Coverage CoverageType=\"7\" />" +
                //"<Charge IncludedInRate=\"true\" Amount=\"0.00\" CurrencyCode=\"EUR\" />" +
                //"</PricedCoverage>" +
                //"<PricedCoverage Required=\"false\">" +
                //"<Coverage CoverageType=\"38\" />" +
                //"<Charge TaxInclusive=\"false\" IncludedInRate=\"false\" CurrencyCode=\"EUR\">" +
                //"<Calculation UnitCharge=\"6.00\" UnitName=\"Day\" Quantity=\"1\" />" +
                //"</Charge>" +
                //"</PricedCoverage>" +
                //"<PricedCoverage Required=\"false\">" +
                //"<Coverage CoverageType=\"40\" />" +
                //"<Charge TaxInclusive=\"false\" IncludedInRate=\"false\" CurrencyCode=\"EUR\">" +
                //"<Calculation UnitCharge=\"7.00\" UnitName=\"Day\" Quantity=\"1\" />" +
                //"</Charge>" +
                //"</PricedCoverage>" +
                //"<PricedCoverage Required=\"false\">" +
                //"<Coverage CoverageType=\"48\" />" +
                //"<Charge IncludedInRate=\"true\" Amount=\"0.00\" CurrencyCode=\"EUR\" />" +
                //"</PricedCoverage>" +
                //"</PricedCoverages>" +
                //"<LocationDetails Code=\"KGS\" Name=\"KOS AIRPORT\" ExtendedLocationCode=\"KGST50\">" +
                //"<Address DefaultInd=\"false\" FormattedInd=\"false\">" +
                //"<AddressLine>SERVD BY GRKOS60</AddressLine>" +
                //"<AddressLine>KOS AIRPORT</AddressLine>" +
                //"<CityName>KOS, ISLAND OF</CityName>" +
                //"<PostalCode>000000000</PostalCode>" +
                //"<CountryName Code=\"GR\">GREECE</CountryName>" +
                //"</Address>" +
                //"<Telephone PhoneLocationType=\"4\" PhoneTechType=\"1\" PhoneNumber=\"22420 51400\" FormattedInd=\"false\" />" +
                //"<AdditionalInfo>" +
                //"<ParkLocation Location=\"1\" />" +
                //"<CounterLocation Location=\"1\" />" +
                //"<OperationSchedules>" +
                //"<OperationSchedule>" +
                //"<OperationTimes>" +
                //"<OperationTime Text=\"MO-SU 0800-2100\" />" +
                //"<OperationTime Mon=\"true\" Start=\"08:00\" End=\"21:00\" />" +
                //"<OperationTime Tue=\"true\" Start=\"08:00\" End=\"21:00\" />" +
                //"<OperationTime Weds=\"true\" Start=\"08:00\" End=\"21:00\" />" +
                //"<OperationTime Thur=\"true\" Start=\"08:00\" End=\"21:00\" />" +
                //"<OperationTime Fri=\"true\" Start=\"08:00\" End=\"21:00\" />" +
                //"<OperationTime Sat=\"true\" Start=\"08:00\" End=\"21:00\" />" +
                //"<OperationTime Sun=\"true\" Start=\"08:00\" End=\"21:00\" />" +
                //"</OperationTimes>" +
                //"</OperationSchedule>" +
                //"</OperationSchedules>" +
                //"</AdditionalInfo>" +
                //"</LocationDetails>" +
                //"</VehSegmentInfo>" +
                                         "</VehReservation>" +
                                         "</VehResRSCore>" +
                                         "</OTA_VehResRS>";

            xmlDocument.LoadXml(reservationResponse);
        }

        private void SendEmail(VehicleResRQResources bookingResouce)
        {

            var email = (from p in db.Partners
                         where p.DomainName == bookingResouce.PartnerName
                         select new
                         {
                             p.ConfimationMailSend
                         });

            if (email.First().ConfimationMailSend == 1)
            {

                var selectedBrand = bookingResouce.BrandName;

                try
                {
                    BuildConfirmationEmail confEmailBuilder = new BuildConfirmationEmail(bookingResouce, bookingResouce.ReservedVehicle);
                    //RemoteDebugger.SendXmlForDebugging(bookingResouce.ConfirmationID + "  " + bookingResouce.ReservedVehicle.Currency + "  " + bookingResouce.ReservedVehicle.Price.ToString() + "  " + bookingResouce.ReservedVehicle.ImagePath + "  " + bookingResouce.ReservedVehicle.Name + "  " + bookingResouce.ReservedVehicle.DrpFee + "  " + bookingResouce.ReservedVehicle.priceInclude.ToString() + "  " + bookingResouce.ReservedVehicle.distance, "mail Progress");
                    string emailMessage = confEmailBuilder.BuildEmail(bookingResouce.ConfirmationID, bookingResouce.ReservedVehicle.Currency, bookingResouce.ReservedVehicle.Price.ToString(), bookingResouce.ReservedVehicle.ImagePath, bookingResouce.ReservedVehicle.Name, bookingResouce.ReservedVehicle.DrpFee, bookingResouce.ReservedVehicle.priceInclude.ToString(), bookingResouce.ReservedVehicle.distance, TimeBetweenDates.CalculateMinutes(bookingResouce.PickupDateTime, Convert.ToDateTime(bookingResouce.DropoffDatetime)));
                    string emailSubject = bookingResouce.ModifyProcess ? (string)HttpContext.GetGlobalResourceObject("SiteStrings", "ModifyEmailSubject") : (string)HttpContext.GetGlobalResourceObject("SiteStrings", "BookingEmailSubject");
                    //Send confirmation email

                    bool isSent = SendConfMail.SendMessage(AppSettings.SiteEmailsSentFrom, bookingResouce.CustomerEmail, emailSubject, emailMessage, true);

                    if (!isSent)
                    {
                        string erroremailMessage = CommonClass.GetErrorXML("VehResRS", "1", "099", "Booking Completed Successfully. Error Occured while sending confirmation email. Please Contact Hertz.");
                        string errorSubject = "Error in Booking";

                        SendConfMail.SendMessage(AppSettings.SiteEmailsSentFrom, bookingResouce.CustomerEmail, errorSubject, erroremailMessage, true);
                    }
                }

                catch (Exception ex)
                {
                    RemoteDebugger.SendXmlForDebugging(ex.Message, "Errors");
                    RemoteDebugger.SendXmlForDebugging(ex.StackTrace, "Errors");
                }
            }
        }

        private void saveBookingInDatabase(VehicleResRQResources resources)
        {
            Order BookingObj = new Order
            {
                ConfirmationID = resources.ConfirmationID,
                CustomerName = resources.CustomerFirstname + " " + resources.CustomerSurname ?? "",
                CustomerEmail = resources.CustomerEmail ?? "",
                EmailCode = "",
                EmailConfirmed = 1,
                IncludeInEshot = 1,
                CustomerPhone = resources.CustomerPhone ?? "",
                CarBooked = resources.ReservedVehicle.Name ?? "",
                VehicleType = "",
                CountryOfResidence = resources.CountryOfResidenceCode ?? "",
                PickUpDate = resources.PickupDateTime,
                DropOffDate = resources.DropoffDatetime,
                OrderDate = DateTime.Now,
                PickUpLocation = resources.PickupLocationCode,
                DropOffLocation = resources.DropoffLocationCode ?? "",
                NoDaysHired = 0,
                AmountPaid = Convert.ToDouble(resources.ReservedVehicle.Price),
                AmountPaidCurrency = resources.ReservedVehicle.Currency ?? "",
                CameFrom = resources.PartnerName ?? "",
                LanguageCode = "",
                ReminderEmail = DateTime.Now,
                ThanksEmail = DateTime.Now,
                HertzNum1 = "",
                XML_Provider = resources.ProviderName ?? "",
                XML_Provider_PickupSupplier = resources.BrandName ?? "",
                XML_Provider_DropoffSupplier = resources.BrandName ?? "",
                XML_RateQualifier = resources.ProviderRateQualifier ?? "",
                CustomerSurname = resources.CustomerSurname ?? "",
                CustomerFirstname = resources.CustomerFirstname ?? "",
                AddressLine1 = resources.Address ?? "",
                AddressLine2 = resources.CityName ?? "",
                AddressTown = resources.CityName ?? "",
                AddressPostCodeZip = resources.PostalCode ?? "",
                AddressCountryCode = resources.SourceCountryCode ?? "",
                ThermeonConf_Id = resources.Thermeon_ConfirmationID ?? "",
                CorpDiscountNo = resources.CorpDiscountNo ?? "",
                Customer_BDay = Convert.ToDateTime(resources.BirthDay)
            };
            db.Orders.Add(BookingObj);
            db.SaveChanges();
        } 
       
        //-------------------------------------------------------------------------------------------------------

        //Retrival Methods
        private void set_RetriveRequest(VehicleRetrivaleRQResources resources)
        {
            XmlNode confirmationIdNode = xmlDocument.SelectSingleNode("//a1:UniqueID", namespaceManager);
            if (confirmationIdNode != null)
            {
                resources.ConfirmationID = confirmationIdNode.Attributes["ID"].Value;
            }

            string retrivalRequest = "<Request xmlns=\"http://www.thermeon.com/webXG/xml/webxml/\" referenceNumber=\"12345\" version=\"2.2600\">"+
                                     "<RetrieveReservationRequest reservationNumber=\""+ resources.ConfirmationID +"\"/>"+
                                     "</Request>";

            xmlDocument.LoadXml(retrivalRequest);
        }

        private void set_RetrivalResponse(VehicleRetrivaleRQResources resources)
        {
            string firstName, surName, vehicleCode, pickCode, DropCode, pickDate, dropDate, option, quantity;
            setNamespaceManager(xmlDocument);

            XmlNode ResStatus = xmlDocument.DocumentElement.SelectSingleNode("//a1:Res", namespaceManager);
            if (ResStatus != null)
            {

                if (ResStatus.Attributes["status"].Value.Trim() == "O")
                {

                    XmlNode NameNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:RenterName", namespaceManager);
                    if (NameNode != null)
                    {
                        firstName = NameNode.Attributes["firstName"].Value.Trim();
                        surName = NameNode.Attributes["lastName"].Value.Trim();
                    }
                    else
                    {
                        firstName = "";
                        surName = "";
                    }
                    XmlNode vehNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:Vehicle", namespaceManager);
                    if (vehNode != null)
                    {
                        vehicleCode = vehNode.Attributes["classCode"].Value.Trim();
                    }
                    else
                    {
                        vehicleCode = "";
                    }

                    XmlNode PickLocation = xmlDocument.DocumentElement.SelectSingleNode("//a1:Pickup", namespaceManager);
                    if (PickLocation != null)
                    {
                        pickCode = ReverseLocationCode(PickLocation.Attributes["locationCode"].Value.Trim());
                        pickDate = PickLocation.Attributes["dateTime"].Value.Trim();
                    }
                    else
                    {
                        pickCode = "";
                        pickDate = "";
                    }

                    XmlNode ReturnLocation = xmlDocument.DocumentElement.SelectSingleNode("//a1:Return", namespaceManager);
                    if (ReturnLocation != null)
                    {
                        DropCode = ReverseLocationCode(ReturnLocation.Attributes["locationCode"].Value.Trim());
                        dropDate = ReturnLocation.Attributes["dateTime"].Value.Trim();
                    }
                    else
                    {
                        DropCode = "";
                        dropDate = "";
                    }

                    XmlNode optionNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:Option", namespaceManager);
                    if (optionNode != null)
                    {
                        XmlNode optionCode = optionNode.SelectSingleNode("//a1:Code", namespaceManager);
                        if (optionCode != null)
                        {
                            option = optionCode.InnerText;
                            quantity = optionNode.SelectSingleNode("//a1:Qty", namespaceManager).InnerText;
                        }
                    }
                    else
                    {
                        option = "";
                        quantity = "";
                    }

                    var termeonCarDetail = (from tc in db.Thermeon_CarDetails
                                            join tm in db.Thermeon_CarMapping on tc.TCId equals tm.ThermeonCarId
                                            join cg in db.CarGroups on tm.CarCode equals cg.CarGroupID
                                            join c in db.Countries on tm.CountryId equals c.CountryID
                                            join l in db.Locations on c.CountryID equals l.CountryIDFK
                                            where cg.CarCode == vehicleCode
                                            && l.LocationCode == DropCode
                                            select new
                                            {
                                                tc.Car_Description,
                                                tc.CarImage_Path,
                                                tc.Passenger_Count,
                                                tc.Baggage_Count,
                                                tc.AirConditionInd,
                                                tc.TransmissionType,
                                                tc.FuelType,
                                                tc.DriveType,
                                                tc.Door_Count,
                                                tc.VehicleCategory,
                                                cg.CarCode
                                            });

                    if (termeonCarDetail.Count() > 0)
                    {
                        string retrivalResponse = "<?xml version=\"1.0\" encoding=\"utf-16\"?>" +
                                                  "<OTA_VehRetResRS xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns=\"http://www.opentravel.org/OTA/2003/05\" Target=\"Qual\" Version=\"1.008\">" +
                                                  "<Success />" +
                                                  "<VehRetResRSCore>" +
                                                  "<VehReservation>" +
                                                  "<Customer><Primary><PersonName><Surname>" + firstName + " " + surName + "</Surname></PersonName></Primary></Customer>" +
                            //"<VehSegmentCore>" +
                                                  "<ConfID Type=\"14\" ID=\"" + resources.ConfirmationID + "\" /><Vendor Code=\"FF\" />" +
                                                  "<VehRentalCore PickUpDateTime=\"" + pickDate + "\" ReturnDateTime=\"" + dropDate + "\">" +
                                                  "<PickUpLocation ExtendedLocationCode=\"" + pickCode + "\" LocationCode=\"" + pickCode + "\" CodeContext=\"IATA\" />" +
                                                  "<ReturnLocation ExtendedLocationCode=\"" + DropCode + "\" LocationCode=\"" + DropCode + "\" CodeContext=\"IATA\" />" +
                                                  "</VehRentalCore>" +
                                                  "<Vehicle PassengerQuantity=\"" + termeonCarDetail.FirstOrDefault().Passenger_Count + "\" BaggageQuantity=\"" + termeonCarDetail.FirstOrDefault().Baggage_Count + "\" AirConditionInd=\"" + termeonCarDetail.FirstOrDefault().AirConditionInd + "\" TransmissionType=\"" + termeonCarDetail.FirstOrDefault().TransmissionType + "\" FuelType=\"" + termeonCarDetail.FirstOrDefault().FuelType + "\" DriveType=\"" + termeonCarDetail.FirstOrDefault().DriveType + "\" Code=\"" + vehicleCode + "\" CodeContext=\"SIPP\">" +
                                                  "<VehType VehicleCategory=\"" + termeonCarDetail.FirstOrDefault().VehicleCategory + "\" /><VehClass Size=\"3\" />" +
                                                  "<VehMakeModel Name=\"" + termeonCarDetail.FirstOrDefault().Car_Description + "\" Code=\"" + vehicleCode + "\" /><PictureURL>" + termeonCarDetail.FirstOrDefault().CarImage_Path + "</PictureURL></Vehicle>" +
                                                  //"<PricedEquips>" +
                                                  //"<PricedEquip>" +
                                                  //"<Equipment EquipType=\"8\" Quantity=\"" + quantity + "\" />" +
                                                  //"<Charge TaxInclusive=\"false\" Amount=\"30.00\" CurrencyCode=\"GBP\" IncludedInRate=\"false\" />" +
                                                  //"</PricedEquip>" +
                                                  //"</PricedEquips>" +
                            //"<RentalRate>" +
                            //"<RateDistance Unlimited=\"true\" DistUnitName=\"Km\" VehiclePeriodUnitName=\"RentalPeriod\" />" +
                            //"<VehicleCharges>" +
                            //"<VehicleCharge Purpose=\"1\" TaxInclusive=\"false\" GuaranteedInd=\"true\" Amount=\"89.44\" CurrencyCode=\"EUR\" IncludedInRate=\"false\">" +
                            //"<TaxAmounts>" +
                            //"<TaxAmount Total=\"15.17\" CurrencyCode=\"EUR\" Percentage=\"16.00\" Description=\"Tax\" />" +
                            //"</TaxAmounts>" +
                            //"<Calculation UnitCharge=\"22.36\" UnitName=\"Day\" Quantity=\"4\" />" +
                            //"</VehicleCharge></VehicleCharges>" +
                            //"<RateQualifier ArriveByFlight=\"true\" RateQualifier=\"RTG2EU\" /></RentalRate>" +
                            //"<Fees><Fee Purpose=\"5\" TaxInclusive=\"false\" Description=\"LOCATION SERVICE CHARGE:\" IncludedInRate=\"false\" Amount=\"5.37\" CurrencyCode=\"EUR\" /></Fees>" +
                            //"<TotalCharge RateTotalAmount=\"89.44\" EstimatedTotalAmount=\"109.98\" CurrencyCode=\"EUR\" />" +
                            //"</VehSegmentCore>" +
                            //"<VehSegmentInfo>" +
                            //"<PaymentRules><PaymentRule RuleType=\"2\" Amount=\"109.98\" CurrencyCode=\"EUR\" /></PaymentRules>" +
                            //"<PricedCoverages>" +
                            //"<PricedCoverage Required=\"false\"><Coverage CoverageType=\"7\" /><Charge IncludedInRate=\"true\" Amount=\"0.00\" CurrencyCode=\"EUR\" /></PricedCoverage>" +
                            //"<PricedCoverage Required=\"false\"><Coverage CoverageType=\"38\" /><Charge TaxInclusive=\"false\" IncludedInRate=\"false\" CurrencyCode=\"EUR\"><Calculation UnitCharge=\"6.00\" UnitName=\"Day\" Quantity=\"1\" /></Charge></PricedCoverage>" +
                            //"<PricedCoverage Required=\"false\"><Coverage CoverageType=\"40\" /><Charge TaxInclusive=\"false\" IncludedInRate=\"false\" CurrencyCode=\"EUR\"><Calculation UnitCharge=\"7.00\" UnitName=\"Day\" Quantity=\"1\" /></Charge></PricedCoverage>" +
                            //"<PricedCoverage Required=\"false\"><Coverage CoverageType=\"48\" /><Charge IncludedInRate=\"true\" Amount=\"0.00\" CurrencyCode=\"EUR\" /></PricedCoverage>" +
                            //"</PricedCoverages>" +
                            //"</VehSegmentInfo>" +
                                                  "</VehReservation>" +
                                                  "</VehRetResRSCore>" +
                                                  "</OTA_VehRetResRS>";

                        xmlDocument.LoadXml(retrivalResponse);
                    }
                    else
                    {
                        throw new InvalidRequestException("No selected CarGroup in DB");
                    }
                }
                else if (ResStatus.Attributes["status"].Value.Trim() == "C")
                {
                    throw new InvalidRequestException("The reservation has been cancelled");
                }
                else if (ResStatus.Attributes["status"].Value.Trim() == "R")
                {
                    throw new InvalidRequestException("The reservation is pending review");
                }
                else if (ResStatus.Attributes["status"].Value.Trim() == "T")
                {
                    throw new InvalidRequestException("The reservation has been turned down");
                }
                else if (ResStatus.Attributes["status"].Value.Trim() == "N")
                {
                    throw new InvalidRequestException("The pickup date has passed and the customer has not picked up the reservation");
                }
            }
        }

        private string ReverseLocationCode(string LocationCode)
        {
            var locationId = (from l in db.Locations
                              where l.ThermeonLocation == LocationCode
                                  select new
                                  {
                                      l.LocationCode
                                  });

            if (locationId.FirstOrDefault() != null)
            {
                return locationId.FirstOrDefault().LocationCode.ToString().TrimEnd();
            }
            else
            {
                return "";
            }
        }

        //-------------------------------------------------------------------------------------------------------

        ////Cancel Methods

        private void set_CancelRequest(VehicleCancelRQResources resources)
        {
            XmlNode confirmationIdNode = xmlDocument.SelectSingleNode("//a1:UniqueID", namespaceManager);
            if (confirmationIdNode != null)
            {
                resources.ConfirmationID = confirmationIdNode.Attributes["ID"].Value;
            }

            string retrivalRequest = "<Request xmlns=\"http://www.thermeon.com/webXG/xml/webxml/\" referenceNumber=\"12345\" version=\"2.2600\">"+
                                     "<CancelReservationRequest reservationNumber=\"" + resources.ConfirmationID + "\"/>" +
                                     "</Request>";

            xmlDocument.LoadXml(retrivalRequest);
        }

        private void set_CancelResponse(VehicleCancelRQResources resources)
        {
            setNamespaceManager(xmlDocument);

            XmlNode CanStatus = xmlDocument.DocumentElement.SelectSingleNode("//a1:CancelReservationResponse", namespaceManager);

            if (CanStatus.Attributes["success"].Value.Trim() == "true")
            {
                string cancelResponse = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
                                        "<OTA_VehCancelRS xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns=\"http://www.opentravel.org/OTA/2003/05\" Target=\"Qual\" Version=\"3.003\">" +
                                        "<Success/>" +
                                        "<VehCancelRSCore CancelStatus=\"Cancelled\">" +
                                        "<UniqueID Type=\"15\" ID=\"" + resources.ConfirmationID + "\"/>" +
                                        "</VehCancelRSCore>" +
                                        "<VehCancelRSInfo/>" +
                                        "</OTA_VehCancelRS>";
                
                xmlDocument.LoadXml(cancelResponse);
            }
            else
            {
                throw new InvalidRequestException("The Cancel not complete");
            }
        }

        private void setCancelData_toDB(VehicleCancelRQResources bookingResouce)
        {
            try
            {
                var order = (from o in db.Orders
                             where (o.ConfirmationID == bookingResouce.ConfirmationID)
                             select new
                             {
                                 o.OrderID,
                                 o.CustomerName,
                                 o.CustomerEmail,
                                 o.EmailConfirmed,
                                 o.CarBooked,
                                 o.VehicleType,
                                 o.CountryOfResidence,
                                 o.PickUpDate,
                                 o.DropOffDate,
                                 o.OrderDate,
                                 o.PickUpLocation,
                                 o.DropOffLocation,
                                 o.NoDaysHired,
                                 o.AmountPaid,
                                 o.XML_Provider,
                                 o.XML_IATANumber,
                                 o.XML_RateQualifier
                             });

                if (order.Count() > 0)
                {
                    CanclledOrder orderCancel = new CanclledOrder
                    {
                        OrderID = order.FirstOrDefault().OrderID,
                        ConfirmationID = bookingResouce.ConfirmationID,
                        CustomerName = order.FirstOrDefault().CustomerName,
                        CustomerEmail = order.FirstOrDefault().CustomerEmail,
                        EmailConfirmed = order.FirstOrDefault().EmailConfirmed,
                        CarBooked = order.FirstOrDefault().CarBooked,
                        VehicleType = order.FirstOrDefault().VehicleType,
                        CountryOfResidence = order.FirstOrDefault().CountryOfResidence,
                        PickUpDate = order.FirstOrDefault().PickUpDate,
                        DropOffDate = order.FirstOrDefault().DropOffDate,
                        OrderDate = order.FirstOrDefault().OrderDate,
                        PickUpLocation = order.FirstOrDefault().PickUpLocation,
                        DropOffLocation = order.FirstOrDefault().DropOffLocation,
                        NoDaysHired = order.FirstOrDefault().NoDaysHired,
                        AmountPaid = order.FirstOrDefault().AmountPaid,
                        XML_Provider = order.FirstOrDefault().XML_Provider,
                        XML_IATANumber = order.FirstOrDefault().XML_IATANumber,
                        XML_RateQualifier = order.FirstOrDefault().XML_RateQualifier
                    };
                    db.CanclledOrders.Add(orderCancel);
                    db.SaveChanges();
                }

                var dbCstInfo = db.Orders
                .Where(w => w.ConfirmationID == bookingResouce.ConfirmationID);

                if (dbCstInfo != null)
                {
                    foreach (var cancel in dbCstInfo)
                    {
                        cancel.CancelledBooking = true;  
                    }
                    db.SaveChanges();
                }
            }
            catch (Exception e)
            {
                RemoteDebugger.SendXmlForDebugging(e.Message,"Error Message");
                RemoteDebugger.SendXmlForDebugging(e.StackTrace, "Error StackRace");
            }
        }

        //-------------------------------------------------------------------------------------------------------

        ////Modification Methods

        private void set_ModificationRequest(VehicleResModifyRQResources resources)
        {
            XmlNode confID = xmlDocument.DocumentElement.SelectSingleNode("//a1:UniqueID", namespaceManager);
            if (confID != null)
            {
                resources.ConfirmationID = confID.Attributes["ID"].Value.Trim();
            }
            XmlNode firstNameNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:GivenName", namespaceManager);
            if (firstNameNode != null)
            {
                resources.CustomerFirstname = firstNameNode.InnerText.Trim();
            }
            XmlNode surNameNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:Surname", namespaceManager);
            if (surNameNode != null)
            {
                resources.CustomerSurname = surNameNode.InnerText.Trim();
            }
            XmlNode customerEmailNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:Email", namespaceManager);
            if (customerEmailNode != null)
            {
                resources.CustomerEmail = customerEmailNode.InnerText.Trim();
            }
            XmlNode customerPhoneNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:Telephone", namespaceManager);
            if (customerPhoneNode != null)
            {
                resources.CustomerPhone = customerPhoneNode.Attributes["PhoneNumber"].Value.Trim();
            }
            XmlNode customerAddressNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:AddressLine", namespaceManager);
            if (customerAddressNode != null)
            {
                resources.Address = customerAddressNode.InnerText.Trim();
            }
            XmlNode customerCityNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:CityName", namespaceManager);
            if (customerCityNode != null)
            {
                resources.CityName = customerCityNode.InnerText.Trim();
            }
            //XmlNode customerPostalCodeNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:PostalCode", namespaceManager);
            //if (customerPostalCodeNode != null)
            //{
            //    resources.PostalCode = customerPostalCodeNode.InnerText.Trim();
            //}
            XmlNode customerCountryNameNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:CountryName", namespaceManager);
            if (customerCountryNameNode != null)
            {
                resources.CountryOfResidenceCode = customerCountryNameNode.Attributes["Code"].Value.Trim();
            }
            XmlNode primary = xmlDocument.DocumentElement.SelectSingleNode("//a1:Primary", namespaceManager);
            if (primary != null)
            {
                if (primary.Attributes["BirthDate"] != null)
                {
                    resources.BirthDay = primary.Attributes["BirthDate"].Value.Trim();
                }

                XmlNodeList document = primary.SelectNodes(".//a1:Document", namespaceManager);
                foreach (XmlNode doc in document)
                {
                    if (doc != null)
                    {
                        if (doc.Attributes["DocType"].Value == "4")
                        {
                            resources.DriverLicence = doc.Attributes["DocID"].Value.Trim();
                            resources.expireDate = doc.Attributes["ExpireDate"].Value.Trim();
                        }
                        else if (doc.Attributes["DocType"].Value == "2")
                        {
                            resources.passportNo = doc.Attributes["DocID"].Value.Trim();
                            resources.IssueCountry = doc.Attributes["DocIssueCountry"].Value.Trim();
                        }
                    }
                }
            }

            //vehicle details

            resources.VehicleDetailsList = new List<VehicleDetail>();

            XmlNode vehCodeNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:VehPref", namespaceManager);
            XmlNode vehRateIdNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:Reference", namespaceManager);
            if (vehCodeNode != null && vehRateIdNode != null)
            {
                VehicleDetail vehicle = new VehicleDetail
                {
                    SippCode = vehCodeNode.Attributes["Code"].Value.Trim(),
                    RateID = vehRateIdNode.Attributes["ID"].Value.Trim()
                };
                resources.VehicleDetailsList.Add(vehicle);
            }

            getThermeonConf_Id(resources);

            //Locations and DateTime 

            XmlNode PickLocation = xmlDocument.DocumentElement.SelectSingleNode("//a1:PickUpLocation", namespaceManager);
            if (PickLocation != null)
            {
                resources.PickupLocationCode = PickLocation.Attributes["LocationCode"].Value.Trim();
            }

            XmlNode DropLocation = xmlDocument.DocumentElement.SelectSingleNode("//a1:ReturnLocation", namespaceManager);
            if (PickLocation != null)
            {
                resources.DropoffLocationCode = PickLocation.Attributes["LocationCode"].Value.Trim();
            }

            XmlNode datetimeNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:VehRentalCore", namespaceManager);
            if (datetimeNode != null)
            {
                if (datetimeNode.Attributes != null)
                {
                    resources.PickupDateTime = DateTime.Parse(datetimeNode.Attributes["PickUpDateTime"].Value.Trim());
                    resources.DropoffDatetime = DateTime.Parse(datetimeNode.Attributes["ReturnDateTime"].Value.Trim());
                }
                else
                {
                    throw new InvalidRequestException("Cannot find VehRentalCore Attributes.");
                }
            }
            else
            {
                throw new InvalidRequestException("Cannot find VehRentalCore Node.");
            }
        }

        private void getThermeonConf_Id(VehicleResModifyRQResources resources)
        {
            var ConfId = from o in db.Orders
                         where o.ConfirmationID == resources.ConfirmationID
                         select new
                         {
                             confId = o.ThermeonConf_Id
                         };

            if (ConfId.Count() > 0)
            {
                resources.Thermeon_ConfirmationID = ConfId.FirstOrDefault().confId.ToUpper();
            }
            else
            {
                throw new InvalidRequestException("No provided parameter");
            }
        }

        private void setReservation_Locations(VehicleResModifyRQResources resources)
        {
            var PicklocationId = (from l in db.Locations
                                  where l.LocationCode == resources.PickupLocationCode
                                  select new
                                  {
                                      l.ThermeonLocation
                                  });

            if (PicklocationId.FirstOrDefault() != null)
            {
                resources.PickupLocationCode = PicklocationId.FirstOrDefault().ThermeonLocation.ToString();
            }
            else
            {
                throw new InvalidRequestException("PickUp Locations not Identified");
            }

            var DroplocationId = (from l in db.Locations
                                  where l.LocationCode == resources.DropoffLocationCode
                                  select new
                                  {
                                      l.ThermeonLocation
                                  });

            if (DroplocationId.FirstOrDefault() != null)
            {
                resources.DropoffLocationCode = DroplocationId.FirstOrDefault().ThermeonLocation.ToString();
            }
            else
            {
                throw new InvalidRequestException("DropOff Locations not Identified");
            }
        }

        private void set_ModifyRequest(VehicleResModifyRQResources resources)
        {
            setReservation_Locations(resources);

            string bDay = "";
            string driveDet = "";

            //if (resources.AccesoriesList != null)
            //{
            //    accessories = setAccessoriesNode();
            //}
            if (resources.BirthDay != null)
            {
                bDay = "<BirthDate>" + resources.BirthDay + "</BirthDate>";
            }
            if (resources.DriverLicence != null && resources.expireDate != null)
            {
                driveDet = "<DrivingLicence Number=\"" + resources.DriverLicence + "\" ExpiryDate=\"" + resources.expireDate + "\"/>";
            }

            string pickDateTime = resources.PickupDateTime.ToString().TrimEnd().TrimStart();
            pickDateTime = resources.PickupDateTime.ToString("s");

            string dropDateTime = resources.DropoffDatetime.ToString().TrimEnd().TrimStart();
            dropDateTime = resources.DropoffDatetime.ToString("s");

            string Request = "<Request xmlns=\"http://www.thermeon.com/webXG/xml/webxml/\" referenceNumber=\"12345\" version=\"2.2600\">"+
                             "<UpdateReservationRequest confirmAvailability=\"true\" reservationNumber=\""+resources.ConfirmationID+"\">"+
                             "<Pickup locationCode=\"" + resources.PickupLocationCode + "\" dateTime=\"" + pickDateTime + "\"/>"+
                             "<Return locationCode=\"" + resources.DropoffLocationCode + "\" dateTime=\"" + dropDateTime + "\"/>"+
                             "<Source confirmationNumber=\"" + resources.Thermeon_ConfirmationID + "\" countryCode=\"UK\"/>"+
                             "<Vehicle classCode=\"" + resources.VehicleDetailsList.FirstOrDefault().SippCode + "\"/>"+
                             "<Renter>"+
                             "<RenterName firstName=\"" + resources.CustomerFirstname + "\" lastName=\"" + resources.CustomerSurname + "\"/>"+
                             "<Address>"+
                             "<Street>" + resources.Address + "</Street>"+
                             "<City>" + resources.CityName + "</City>"+
                             "<CountryCode>" + resources.CountryOfResidenceCode + "</CountryCode>"+
                             "<Email>" + resources.CustomerEmail + "</Email>"+
                             "<HomeTelephoneNumber>" + resources.CustomerPhone + "</HomeTelephoneNumber>"+
                             "</Address>"+
                             bDay +
                             driveDet +
                             "</Renter>"+
                             "<QuotedRate rateID=\"" + resources.VehicleDetailsList.FirstOrDefault().RateID + "\" classCode=\"" + resources.VehicleDetailsList.FirstOrDefault().SippCode + "\" corporateRateID=\"API\"/>" +
                             "</UpdateReservationRequest>"+
                             "</Request>";


            xmlDocument.LoadXml(Request);
        }

        private void Reverse_setReservation_Locations(VehicleResModifyRQResources resources)
        {
            var PicklocationId = (from l in db.Locations
                                  where l.ThermeonLocation == resources.PickupLocationCode
                                  select new
                                  {
                                      l.LocationCode
                                  });

            if (PicklocationId.FirstOrDefault() != null)
            {
                resources.PickupLocationCode = PicklocationId.FirstOrDefault().LocationCode.ToString();
            }
            else
            {
                throw new InvalidRequestException("Reverse PickUp Locations not Identified");
            }

            var DroplocationId = (from l in db.Locations
                                  where l.ThermeonLocation == resources.DropoffLocationCode
                                  select new
                                  {
                                      l.LocationCode
                                  });

            if (DroplocationId.FirstOrDefault() != null)
            {
                resources.DropoffLocationCode = DroplocationId.FirstOrDefault().LocationCode.ToString();
            }
            else
            {
                throw new InvalidRequestException("Reverse DropOff Locations not Identified");
            }
        }

        private bool hasErrorsInBookingResponse(VehicleResModifyRQResources resources)
        {
            setNamespaceManager(xmlDocument);

            XmlNode reservation = xmlDocument.DocumentElement.SelectSingleNode("//a1:UpdateReservationResponse", namespaceManager);

            if (reservation.Attributes["success"].Value.Trim() == "false")
            {
                return true;
            }
            return false;
        }

        private void set_ModifyErrorResponse()
        {
            XmlNode errorMessageNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:Message", namespaceManager);

            string MsgType = errorMessageNode.Attributes["number"].Value.Trim();

            XmlNode errorTextNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:Text", namespaceManager);

            string ErrText = errorTextNode.InnerText.Trim();

            string errorXml = "<?xml version=\"1.0\" encoding=\"utf-16\"?>" +
                              "<OTA_VehModifyRS xmlns=\"http://www.opentravel.org/OTA/2003/05\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.opentravel.org/OTA/2003/05\">" +
                              "<Errors>" +
                              "<Error Type=\"" + MsgType + "\" ShortText=\"" + ErrText + "\" Code=\"" + MsgType + "\" />" +
                              "</Errors>" +
                              "</OTA_VehModifyRS>";

            xmlDocument.LoadXml(errorXml);
        }

        private void setModificationData_DB(VehicleResModifyRQResources resources)
        {
            string Group = resources.VehicleDetailsList.FirstOrDefault().SippCode;
            var termeonCarDetail = (from tc in db.Thermeon_CarDetails
                                    join tm in db.Thermeon_CarMapping on tc.TCId equals tm.ThermeonCarId
                                    join cg in db.CarGroups on tm.CarCode equals cg.CarGroupID
                                    join c in db.Countries on tm.CountryId equals c.CountryID
                                    join l in db.Locations on c.CountryID equals l.CountryIDFK
                                    where cg.CarCode == Group
                                    && l.LocationCode == resources.DropoffLocationCode.TrimEnd().TrimStart()
                                    select new
                                    {
                                        tc.Car_Description,
                                        tc.CarImage_Path,
                                        tc.Passenger_Count,
                                        tc.Baggage_Count,
                                        tc.AirConditionInd,
                                        tc.TransmissionType,
                                        tc.FuelType,
                                        tc.DriveType,
                                        tc.Door_Count,
                                        tc.VehicleCategory,
                                        cg.CarCode
                                    });

            if (termeonCarDetail.Count() > 0)
            {
                resources.ReservedVehicle = new BookedVehicle();
                resources.ReservedVehicle.Name = termeonCarDetail.FirstOrDefault().Car_Description;
                resources.ReservedVehicle.ImagePath = termeonCarDetail.FirstOrDefault().CarImage_Path;
                resources.ReservedVehicle.Passenger_Count = Convert.ToInt32(termeonCarDetail.FirstOrDefault().Passenger_Count);
                resources.ReservedVehicle.Baggage_Count = Convert.ToInt32(termeonCarDetail.FirstOrDefault().Baggage_Count);
                resources.ReservedVehicle.AirCond = Convert.ToBoolean(termeonCarDetail.FirstOrDefault().AirConditionInd);
                resources.ReservedVehicle.TransmissionTP = termeonCarDetail.FirstOrDefault().TransmissionType;
                resources.ReservedVehicle.FuelTP = termeonCarDetail.FirstOrDefault().FuelType;
                resources.ReservedVehicle.DriverTP = termeonCarDetail.FirstOrDefault().DriveType;
                resources.ReservedVehicle.Veh_Category = Convert.ToInt32(termeonCarDetail.FirstOrDefault().VehicleCategory);
                resources.ReservedVehicle.Door_Count = Convert.ToInt32(termeonCarDetail.FirstOrDefault().Door_Count);
            }
            else
            {
                throw new InvalidRequestException("No selected CarGroup in DB");
            }
        }

        private void Load_ModificationResponse(VehicleResModifyRQResources resources)
        {
            string ModifyResponse = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><OTA_VehModifyRS xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns=\"http://www.opentravel.org/OTA/2003/05\" Target=\"Qual\" Version=\"1.00\">"+
                                    "<Success />"+
                                    "<VehModifyRSCore ModifyStatus=\"Modified\">"+
                                    "<VehReservation>"+
                                    "<Customer><Primary><PersonName><GivenName>" + resources.CustomerFirstname + "</GivenName><Surname>" + resources.CustomerSurname + "</Surname></PersonName></Primary></Customer>"+
                                    //"<VehSegmentCore>"+
                                    "<ConfID Type=\"14\" ID=\"" + resources.ConfirmationID + "\" />"+
                                    "<Vendor Code=\"ZT\"/>"+
                                    "<VehRentalCore PickUpDateTime=\"" + resources.PickupDateTime + "\" ReturnDateTime=\"" + resources.DropoffDatetime + "\">"+
                                    "<PickUpLocation ExtendedLocationCode=\"" + resources.PickupLocationCode.TrimEnd() + "\" LocationCode=\"" + resources.PickupLocationCode.TrimEnd() + "\" CodeContext=\"IATA\" />"+
                                    "<ReturnLocation ExtendedLocationCode=\"" + resources.DropoffLocationCode.TrimEnd() + "\" LocationCode=\"" + resources.DropoffLocationCode.TrimEnd() + "\" CodeContext=\"IATA\" />"+
                                    "</VehRentalCore>"+
                                    "<Vehicle PassengerQuantity=\"" + resources.ReservedVehicle.Passenger_Count + "\" BaggageQuantity=\"" + resources.ReservedVehicle.Baggage_Count + "\" AirConditionInd=\"" + resources.ReservedVehicle.AirCond + "\" TransmissionType=\"" + resources.ReservedVehicle.TransmissionTP + "\" FuelType=\"" + resources.ReservedVehicle.FuelTP + "\" DriveType=\"" + resources.ReservedVehicle.DriverTP + "\" Code=\"" + resources.VehicleDetailsList.FirstOrDefault().SippCode + "\" CodeContext=\"SIPP\">"+
                                    "<VehType VehicleCategory=\"" + resources.ReservedVehicle.Veh_Category + "\" DoorCount=\"" + resources.ReservedVehicle.Door_Count + "\" />"+
                                    "<VehClass Size=\"4\" />"+
                                    "<VehMakeModel Name=\"" + resources.ReservedVehicle.Name + "\" Code=\"" + resources.VehicleDetailsList.FirstOrDefault().SippCode + "\" />"+
                                    "<PictureURL>" + resources.ReservedVehicle.ImagePath + "</PictureURL>"+
                                    "</Vehicle>"+
                                    //"<RentalRate>"+
                                    //"<RateDistance Unlimited=\"true\" DistUnitName=\"Km\" VehiclePeriodUnitName=\"RentalPeriod\" />"+
                                    //"<VehicleCharges>"+
                                    //"<VehicleCharge Purpose=\"1\" TaxInclusive=\"false\" GuaranteedInd=\"true\" Amount=\"109.80\" CurrencyCode=\"EUR\" IncludedInRate=\"false\">"+
                                    //"<TaxAmounts><TaxAmount Total=\"18.62\" CurrencyCode=\"EUR\" Percentage=\"16.00\" Description=\"Tax\" /></TaxAmounts>"+
                                    //"<Calculation UnitCharge=\"27.45\" UnitName=\"Day\" Quantity=\"4\" />"+
                                    //"</VehicleCharge>"+
                                    //"</VehicleCharges>"+
                                    //"<RateQualifier ArriveByFlight=\"true\" RateQualifier=\"RTG2EU\" />"+
                                    //"</RentalRate>"+
                                    //"<Fees><Fee Purpose=\"5\" TaxInclusive=\"false\" Description=\"LOCATION SERVICE CHARGE:\" IncludedInRate=\"false\" Amount=\"6.59\" CurrencyCode=\"EUR\" /></Fees>"+
                                    //"<TotalCharge RateTotalAmount=\"109.80\" EstimatedTotalAmount=\"135.01\" CurrencyCode=\"EUR\" />"+
                                    //"</VehSegmentCore>"+
                                    //"<VehSegmentInfo>"+
                                    //"<PaymentRules><PaymentRule RuleType=\"2\" Amount=\"135.01\" CurrencyCode=\"EUR\" /></PaymentRules>"+
                                    //"<PricedCoverages><PricedCoverage Required=\"false\"><Coverage CoverageType=\"7\" /><Charge IncludedInRate=\"true\" Amount=\"0.00\" CurrencyCode=\"EUR\" /></PricedCoverage>"+
                                    //"<PricedCoverage Required=\"false\"><Coverage CoverageType=\"38\" /><Charge TaxInclusive=\"false\" IncludedInRate=\"false\" CurrencyCode=\"EUR\"><Calculation UnitCharge=\"6.00\" UnitName=\"Day\" Quantity=\"1\" /></Charge></PricedCoverage>"+
                                    //"<PricedCoverage Required=\"false\"><Coverage CoverageType=\"40\" /><Charge TaxInclusive=\"false\" IncludedInRate=\"false\" CurrencyCode=\"EUR\"><Calculation UnitCharge=\"9.00\" UnitName=\"Day\" Quantity=\"1\" /></Charge></PricedCoverage>"+
                                    //"<PricedCoverage Required=\"false\"><Coverage CoverageType=\"48\" /><Charge IncludedInRate=\"true\" Amount=\"0.00\" CurrencyCode=\"EUR\" /></PricedCoverage>"+
                                    //"</PricedCoverages>"+
                                    //"</VehSegmentInfo>"+
                                    "</VehReservation></VehModifyRSCore>"+
                                    "</OTA_VehModifyRS>";

            xmlDocument.LoadXml(ModifyResponse);
        }

        private void setModifyRequestData(VehicleResModifyRQResources bookingResouce)
        {
            var order = (from o in db.Orders
                         where (o.ConfirmationID == bookingResouce.ConfirmationID)
                         select new
                         {
                             o.OrderID,
                             o.CustomerName,
                             o.CustomerEmail,
                             o.EmailConfirmed,
                             o.CarBooked,
                             o.VehicleType,
                             o.CountryOfResidence,
                             o.PickUpDate,
                             o.DropOffDate,
                             o.OrderDate,
                             o.PickUpLocation,
                             o.DropOffLocation,
                             o.NoDaysHired,
                             o.AmountPaid,
                             o.XML_Provider,
                             o.XML_IATANumber,
                             o.XML_RateQualifier
                         });

            if (order.Count() > 0)
            {
                ModifiedOrder orderModify = new ModifiedOrder
                {
                    OrderID = order.FirstOrDefault().OrderID,
                    ConfirmationID = bookingResouce.ConfirmationID,
                    CustomerName = order.FirstOrDefault().CustomerName,
                    CustomerEmail = order.FirstOrDefault().CustomerEmail,
                    EmailConfirmed = order.FirstOrDefault().EmailConfirmed,
                    CarBooked = order.FirstOrDefault().CarBooked,
                    VehicleType = order.FirstOrDefault().VehicleType,
                    CountryOfResidence = order.FirstOrDefault().CountryOfResidence,
                    PickUpDate = order.FirstOrDefault().PickUpDate,
                    DropOffDate = order.FirstOrDefault().DropOffDate,
                    OrderDate = order.FirstOrDefault().OrderDate,
                    PickUpLocation = order.FirstOrDefault().PickUpLocation,
                    DropOffLocation = order.FirstOrDefault().DropOffLocation,
                    NoDaysHired = order.FirstOrDefault().NoDaysHired,
                    AmountPaid = order.FirstOrDefault().AmountPaid,
                    XML_Provider = order.FirstOrDefault().XML_Provider,
                    XML_IATANumber = order.FirstOrDefault().XML_IATANumber,
                    XML_RateQualifier = order.FirstOrDefault().XML_RateQualifier
                };
                db.ModifiedOrders.Add(orderModify);
                db.SaveChanges();
            }

            Order ModOrder = (from o in db.Orders
                              where o.ConfirmationID == bookingResouce.ConfirmationID
                              select o).SingleOrDefault();

            ModOrder.PickUpDate = bookingResouce.PickupDateTime;
            ModOrder.DropOffDate = bookingResouce.DropoffDatetime;
            ModOrder.OrderDate = DateTime.Now;
            ModOrder.AmountPaid = Convert.ToDouble(bookingResouce.ReservedVehicle.Price);
            ModOrder.AmountPaidCurrency = bookingResouce.ReservedVehicle.Currency;
            ModOrder.CarBooked = bookingResouce.ReservedVehicle.Name;
            ModOrder.Modified = true;
            ModOrder.ModifiedDate = DateTime.Now;

            db.SaveChanges();
        }
    }
}