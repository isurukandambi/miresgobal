﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CARTRAWLER;
using DTAG;
using Exceptions;
using HERTZ;
using THERMEON;

/// <summary>
/// Summary description for XMLProviderRouter
/// </summary>
public class XMLProviderRouter
{
    public IProviderChannel switchProviderChannel(string providerCode)
    {
        if (providerCode == XMLProviders.HERTZ.ToString())
        {
           return new HertzProviderChannel();
        }
        else if (providerCode == XMLProviders.DTAG.ToString())
        {
            return new DtagProviderChannel();
        }
        else if (providerCode == XMLProviders.CARTRAWLER.ToString())
        {
            return new CartrawlerProviderChannel();
        }
        else if (providerCode == XMLProviders.THERMEON.ToString())
        {
            return new ThermeonProviderChannel();
        }
        else
        {
            throw new InvalidRequestException("Cannot retrieve XML Provider.");
        }
    }
}