﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;
using ORM;
using ProcessResources;

/// <summary>
/// Summary description for ProviderChannel
/// </summary>
public interface IProviderChannel
{
   XmlDocument getValidatedRequest(VehicleAvailRateRQResources resources, XmlNamespaceManager namespaceManager, DatabaseEntities db);
   XmlDocument getValidatedResponse(VehicleAvailRateRQResources resources, XmlNamespaceManager namespaceManager, DatabaseEntities db);
   XmlDocument getValidatedRequest(VehicleResRQResources resources, XmlNamespaceManager namespaceManager, DatabaseEntities db);
   XmlDocument getValidatedResponse(VehicleResRQResources resources, XmlNamespaceManager namespaceManager, DatabaseEntities db);
   XmlDocument getValidatedRequest(VehicleRetrivaleRQResources resources, XmlNamespaceManager namespaceManager, DatabaseEntities db);
   XmlDocument getValidatedResponse(VehicleRetrivaleRQResources resources, XmlNamespaceManager namespaceManager, DatabaseEntities db);
   XmlDocument getValidatedRequest(VehicleResModifyRQResources resources, XmlNamespaceManager namespaceManager, DatabaseEntities db);
   XmlDocument getValidatedResponse(VehicleResModifyRQResources resources, XmlNamespaceManager namespaceManager, DatabaseEntities db);
   XmlDocument getValidatedRequest(VehicleCancelRQResources resources, XmlNamespaceManager namespaceManager, DatabaseEntities db);
   XmlDocument getValidatedResponse(VehicleCancelRQResources resources, XmlNamespaceManager namespaceManager, DatabaseEntities db);
   XmlDocument getValidatedRequest(VehicleRentalRQResources resources, XmlNamespaceManager namespaceManager, DatabaseEntities db);
   XmlDocument getValidatedResponse(VehicleRentalRQResources resources, XmlNamespaceManager namespaceManager, DatabaseEntities db);
   void setChannelUtils(XmlNamespaceManager namespaceManager, XmlDocument xmlDocument, DatabaseEntities db);
}