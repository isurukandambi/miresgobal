﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;
using Exceptions;
using ORM;
using ProcessResources;

/// <summary>
/// Summary description for GenericProviderChannel
/// </summary>
public abstract class GenericProviderChannel : IProviderChannel
{

    protected XmlDocument xmlDocument;
    protected XmlNamespaceManager namespaceManager;
    protected DatabaseEntities db;

    #region ProviderChannel Members

    public virtual XmlDocument getValidatedRequest(VehicleAvailRateRQResources resources, XmlNamespaceManager namespaceManager, DatabaseEntities db)
    {
        resources.ValidatedRequest = resources.OriginalRequest;
        setChannelUtils(namespaceManager, resources.ValidatedRequest, db);

        //If brand is Hertz or Thrifty skip the mappings
        if (resources.BrandName.ToString().ToUpper() == Brands.HERTZ.ToString() || resources.BrandName.ToString().ToUpper() == Brands.THRIFTY.ToString())
        {

        }
        else
        {
            int brandCountryID = isLocationAvailableForBrandPartner(resources);
            setMaxVehicleLimit(brandCountryID, resources);
            setVehiclePreferences(resources.IsVehiclePrefEnabled, brandCountryID);
        }
        
        //setRateQualifier(resources);
        //setIsVehiclePreferenceEnabled(resources);       
        validatePickupAndReturnDates(resources);
        doProviderSpecificValidationsForRequest(resources);
        return xmlDocument;
    }

    public virtual XmlDocument getValidatedResponse(VehicleAvailRateRQResources resources, XmlNamespaceManager namespaceManager, DatabaseEntities db)
    {
        resources.ValidatedResponse = resources.ReceivedResponse;
        setChannelUtils(namespaceManager, resources.ValidatedResponse, db);
        doProviderSpecificValidationsForResponse(resources);
        return xmlDocument;
    }

    public virtual XmlDocument getValidatedRequest(VehicleResRQResources resources, XmlNamespaceManager namespaceManager, DatabaseEntities db)
    {
        resources.ValidatedRequest = resources.OriginalRequest;
        setChannelUtils(namespaceManager, resources.ValidatedRequest, db);

        //If brand is Hertz or Thrifty skip the mappings
        if (resources.BrandName.ToString().ToUpper() == Brands.HERTZ.ToString() || resources.BrandName.ToString().ToUpper() == Brands.THRIFTY.ToString())
        {

        }
        else
        {
            int brandCountryID = isLocationAvailableForBrandPartner(resources);
        }

        //setRateQualifier(resources);
        setCorpDiscountNo(resources);
        validatePickupAndReturnDates(resources);
        doProviderSpecificValidationsForRequest(resources);
        return xmlDocument;
    }

    public virtual XmlDocument getValidatedResponse(VehicleResRQResources resources, XmlNamespaceManager namespaceManager, DatabaseEntities db)
    {
        resources.ValidatedResponse = resources.ReceivedResponse;
        setChannelUtils(namespaceManager, resources.ValidatedResponse, db);
        doProviderSpecificValidationsForResponse(resources);
        return xmlDocument;
    }

    public virtual XmlDocument getValidatedRequest(VehicleRetrivaleRQResources resources, XmlNamespaceManager namespaceManager, DatabaseEntities db)
    {
        resources.ValidatedRequest = resources.OriginalRequest;
        setChannelUtils(namespaceManager, resources.ValidatedRequest, db);
        doProviderSpecificValidationsForRequest(resources);
        return xmlDocument;
    }

    public virtual XmlDocument getValidatedResponse(VehicleRetrivaleRQResources resources, XmlNamespaceManager namespaceManager, DatabaseEntities db)
    {
        resources.ValidatedResponse = resources.ReceivedResponse;
        setChannelUtils(namespaceManager, resources.ValidatedResponse, db);
        doProviderSpecificValidationsForResponse(resources);
        return xmlDocument;
    }

    public virtual XmlDocument getValidatedRequest(VehicleResModifyRQResources resources, XmlNamespaceManager namespaceManager, DatabaseEntities db)
    {
        resources.ValidatedRequest = resources.OriginalRequest;
        setChannelUtils(namespaceManager, resources.ValidatedRequest, db);
        //int brandLoctionID = isLocationAvailableForBrandPartner(resources);
        //setRateQualifier(resources);
        setCorpDiscountNo(resources);
        validatePickupAndReturnDates(resources);
        doProviderSpecificValidationsForRequest(resources);
        return xmlDocument;
    }

    public virtual XmlDocument getValidatedResponse(VehicleResModifyRQResources resources, XmlNamespaceManager namespaceManager, DatabaseEntities db)
    {
        resources.ValidatedResponse = resources.ReceivedResponse;
        setChannelUtils(namespaceManager, resources.ValidatedResponse, db);
        doProviderSpecificValidationsForResponse(resources);
        return xmlDocument;
    }

    public virtual XmlDocument getValidatedRequest(VehicleCancelRQResources resources, XmlNamespaceManager namespaceManager, DatabaseEntities db)
    {
        resources.ValidatedRequest = resources.OriginalRequest;
        setChannelUtils(namespaceManager, resources.ValidatedRequest, db);
        doProviderSpecificValidationsForRequest(resources);
        return xmlDocument;
    }

    public virtual XmlDocument getValidatedResponse(VehicleCancelRQResources resources, XmlNamespaceManager namespaceManager, DatabaseEntities db)
    {
        resources.ValidatedResponse = resources.ReceivedResponse;
        setChannelUtils(namespaceManager, resources.ValidatedResponse, db);
        doProviderSpecificValidationsForResponse(resources);
        return xmlDocument;
    }

    public virtual XmlDocument getValidatedRequest(VehicleRentalRQResources resources, XmlNamespaceManager namespaceManager, DatabaseEntities db)
    {
        resources.ValidatedRequest = resources.OriginalRequest;
        setChannelUtils(namespaceManager, resources.ValidatedRequest, db);
        setRentalError(resources);
        doProviderSpecificValidationsForRequest(resources);
        return xmlDocument;
    }
    public virtual XmlDocument getValidatedResponse(VehicleRentalRQResources resources, XmlNamespaceManager namespaceManager, DatabaseEntities db)
    {
        resources.ValidatedResponse = resources.ReceivedResponse;
        setChannelUtils(namespaceManager, resources.ValidatedResponse, db);
        doProviderSpecificValidationsForResponse(resources);
        return xmlDocument;
    }

    public void setChannelUtils(XmlNamespaceManager namespaceManager, XmlDocument xmlDocument, DatabaseEntities db)
    {
        this.db = db;
        this.xmlDocument = xmlDocument;
        this.namespaceManager = namespaceManager;
    }

    #endregion

    public abstract void doProviderSpecificValidationsForRequest(VehicleAvailRateRQResources resources);
    public abstract void doProviderSpecificValidationsForResponse(VehicleAvailRateRQResources resources);

    public abstract void doProviderSpecificValidationsForRequest(VehicleResRQResources resources);
    public abstract void doProviderSpecificValidationsForResponse(VehicleResRQResources resources);

    public abstract void doProviderSpecificValidationsForRequest(VehicleRetrivaleRQResources resources);
    public abstract void doProviderSpecificValidationsForResponse(VehicleRetrivaleRQResources resources);

    public abstract void doProviderSpecificValidationsForRequest(VehicleResModifyRQResources resources);
    public abstract void doProviderSpecificValidationsForResponse(VehicleResModifyRQResources resources);

    public abstract void doProviderSpecificValidationsForRequest(VehicleCancelRQResources resources);
    public abstract void doProviderSpecificValidationsForResponse(VehicleCancelRQResources resources);

    public abstract void doProviderSpecificValidationsForRequest(VehicleRentalRQResources resources);
    public abstract void doProviderSpecificValidationsForResponse(VehicleRentalRQResources resources);

    private int isLocationAvailableForBrandPartner(GenericProcessResources resources)
    {
        var brandLocationData = (from bl in db.BrandCountries
                                 join c in db.Countries on bl.CountryIDFK equals c.CountryID
                                 join b in db.Brands on bl.BrandIDFK equals b.BrandID
                                 join l in db.Locations on c.CountryID equals l.CountryIDFK
                                 where bl.BrandIDFK == resources.BrandId && l.LocationCode == resources.PickupLocationCode
                                 orderby c.CountryName
                                 select new
                                 {
                                     bl.BrandCountry_ID,
                                     b.BrandName,
                                     c.CountryName
                                 });
        //var brandLocationData = db.BrandLocations.Where(bl => bl.PartnerIDFK == resources.PartnerId && bl.BrandIDFK == resources.BrandId && bl.Location.LocationCode == resources.PickupLocationCode);
        if (brandLocationData.Count() > 0)
        {
            return brandLocationData.First().BrandCountry_ID;
        }
        else
        {
            throw new InvalidRequestException(resources.BrandName + " brand is not offer for selected Country.");
        }
    }

    private void setRentalError(VehicleRentalRQResources resources)
    {
        if (resources.ProviderName != "CARTRAWLER")
        {
            string errorXml = "<?xml version=\"1.0\" encoding=\"utf-16\"?>" +
                                  "<CT_RentalConditionsRS xmlns=\"http://www.opentravel.org/OTA/2003/05\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.opentravel.org/OTA/2003/05\">" +
                                  "<Errors>" +
                                  "<Error Type=\"Error\" ShortText=\"Terms Conditions only Car Trawler Provider\" Code=\"Validate\" />" +
                                  "</Errors>" +
                                  "</CT_RentalConditionsRS>";

            xmlDocument.LoadXml(errorXml);
        }
    }

    private void setMaxVehicleLimit(int brandLocationId, VehicleAvailRateRQResources resources)
    {
        var brandLocationData = db.BrandCountries.Where(bl => bl.BrandCountry_ID == brandLocationId);
        //var partnerVehicleLimitEnabled = brandLocationData.First().Partner.VehicleLimtEnable;
        var partnerVehicleLimitEnabled = db.Partners.Where(p => p.DomainName == resources.PartnerName);
        if (partnerVehicleLimitEnabled.FirstOrDefault().VehicleLimtEnable == 1)
        {
            resources.IsVehicleLimitEnabled = true;
            XmlNode maxResponseNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:OTA_VehAvailRateRQ", namespaceManager);
            if (maxResponseNode != null)
            {
                if (maxResponseNode.Attributes != null)
                {
                    maxResponseNode.Attributes["MaxResponses"].Value = brandLocationData.First().VehicleLimit.Value.ToString();
                }
            }
        }
        else
        {
            resources.IsVehicleLimitEnabled = false;
        }
    }

    //private void setRateQualifier(GenericProcessResources resource)
    //{
    //    XmlNode rateQualifierNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:RateQualifier", namespaceManager);
    //    if (rateQualifierNode != null)
    //    {
    //        if (rateQualifierNode.Attributes != null)
    //        {
    //            if (rateQualifierNode.Attributes["RateQualifier"] != null)
    //            {
    //                string receivedRateQualifier = rateQualifierNode.Attributes["RateQualifier"].Value.ToString().TrimEnd();
    //                resource.RequestedRateQualifier = receivedRateQualifier;
    //                //RemoteDebugger.SendXmlForDebugging("Received Rate Qualifier : " + receivedRateQualifier + " Brand Id : " + resource.BrandId + " Partner Id : " + resource.PartnerId + " Query Result Count: " + db.PatnerBrandMappings, "Test Partner");
    //                var partnerBrandDataForRQ = db.PatnerBrandMappings.Where(pb => pb.PartnerRateQualifier == receivedRateQualifier && pb.BrandId == resource.BrandId && pb.PartnerId == resource.PartnerId);

    //                Logger.Debug("Received Rate Qualifier : " + receivedRateQualifier + " Brand Id : " + resource.BrandId + " Partner Id : " + resource.PartnerId + " Query Result Count: " + partnerBrandDataForRQ.Count());
    //                //RemoteDebugger.SendXmlForDebugging("Received Rate Qualifier : " + receivedRateQualifier + " Brand Id : " + resource.BrandId + " Partner Id : " + resource.PartnerId + " Query Result Count: " + partnerBrandDataForRQ.Count(), "Test Partner");
    //                if (partnerBrandDataForRQ.Count() != 0)
    //                {
    //                    string providedRQ = partnerBrandDataForRQ.FirstOrDefault().XMLProviderRateQualifier.Trim();
    //                    resource.ProviderRateQualifier = providedRQ;
    //                    rateQualifierNode.Attributes["RateQualifier"].Value = providedRQ;
    //                }
    //                else
    //                {
    //                    throw new InvalidRequestException("RateQualifier mappings conflicted");
    //                }
    //            }
    //        }
    //        else
    //        {
    //            throw new InvalidRequestException("Cannot find RateQualifier Attributes");
    //        }
    //    }
    //    else
    //    {
    //        //throw new InvalidRequestException("Cannot find RateQualifier Xml Element.");
    //    }
    //}

    //private void setIsVehiclePreferenceEnabled(VehicleAvailRateRQResources resource)
    //{
    //    if (resource.ProviderName == "DTAG")
    //    {
    //        var partnerBrandDataForPreference = db.PatnerBrandMappings.Where(pb => pb.BrandId == resource.BrandId && pb.PartnerId == resource.PartnerId);
    //        if (partnerBrandDataForPreference.First().VehiclePrefEnable == 1)
    //        {
    //            resource.IsVehiclePrefEnabled = true;
    //        }
    //        else
    //        {
    //            resource.IsVehiclePrefEnabled = false;
    //        }
    //    }
    //    else if (resource.ProviderName == "CARTRAWLER")
    //    {
    //        var partnerBrandDataForPreference = db.PatnerBrandMappings.Where(pb => pb.BrandId == resource.BrandId && pb.PartnerId == resource.PartnerId);
    //        if (partnerBrandDataForPreference.First().VehiclePrefEnable == 1)
    //        {
    //            resource.IsVehiclePrefEnabled = true;
    //        }
    //        else
    //        {
    //            resource.IsVehiclePrefEnabled = false;
    //        }
    //    }
    //    else
    //    {
    //        var partnerBrandDataForPreference = db.PatnerBrandMappings.Where(pb => pb.PartnerRateQualifier == resource.RequestedRateQualifier && pb.BrandId == resource.BrandId && pb.PartnerId == resource.PartnerId);
    //        if (partnerBrandDataForPreference.First().VehiclePrefEnable == 1)
    //        {
    //            resource.IsVehiclePrefEnabled = true;
    //        }
    //        else
    //        {
    //            resource.IsVehiclePrefEnabled = false;
    //        }
    //    }
    //}

    private void setVehiclePreferences(bool isVehPrefEnabled, int brandLocationID)
    {
        if (isVehPrefEnabled)
        {
            var carGroups = (from cgb in db.CarGroupBrandMappings
                             join cg in db.CarGroups on cgb.CarGroupIDFK equals cg.CarGroupID
                             where cgb.BrandLocationIDFK == brandLocationID
                             select new
                             {
                                 cg.CarCode
                             });

            XmlNode vehPrefsNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:VehPrefs", namespaceManager);
            if (vehPrefsNode == null)
            {
                XmlNode vehAvailRQCoreNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:VehAvailRQCore", namespaceManager);
                if (vehAvailRQCoreNode != null)
                {
                    XmlElement vehPrefsElement = xmlDocument.CreateElement("VehPrefs");
                    foreach (var carGroup in carGroups)
                    {
                        XmlElement elem = xmlDocument.CreateElement("VehPref");
                        elem.SetAttribute("Code", carGroup.CarCode);
                        elem.SetAttribute("CodeContext", "SIPP");
                        vehPrefsElement.AppendChild(elem);
                    }
                    vehAvailRQCoreNode.AppendChild(vehPrefsElement);
                }
                else
                {
                    throw new InvalidRequestException("VehAvailRQCore node missing.");
                }
            }
            else
            {
                foreach (var carGroup in carGroups)
                {
                    XmlElement elem = xmlDocument.CreateElement("VehPref");
                    elem.SetAttribute("Code", carGroup.CarCode);
                    elem.SetAttribute("CodeContext", "SIPP");
                    vehPrefsNode.AppendChild(elem);
                }
            }
        }
    }


    private void validatePickupAndReturnDates(GenericProcessResources resources)
    {
        XmlNode ModType = xmlDocument.DocumentElement.SelectSingleNode("//a1:VehModifyRQCore", namespaceManager);
        if (ModType != null)
        {
            if (ModType.Attributes["ModifyType"].Value == "Book")
            {
                //XmlAttribute PickupDateTime = xmlDocument.DocumentElement.SelectSingleNode("//a1:VehRentalCore ", namespaceManager).Attributes["PickUpDateTime"];
                //XmlAttribute DropoffDateTime = xmlDocument.DocumentElement.SelectSingleNode("//a1:VehRentalCore ", namespaceManager).Attributes["ReturnDateTime"];
                //string pickUpStringTime = PickupDateTime.Value.Split('T')[1];
                //if (pickUpStringTime == "00:00:00")
                //{
                //    PickupDateTime.Value = PickupDateTime.Value.Split('T')[0] + "T" + "00:01:01";
                //}
                //string dropOffStringTime = DropoffDateTime.Value.Split('T')[1];
                //if (dropOffStringTime == "00:00:00")
                //{
                //    DropoffDateTime.Value = DropoffDateTime.Value.Split('T')[0] + "T" + "00:01:01";
                //}
                //resources.PickupDateTime = DateTime.Parse(PickupDateTime.Value);
                //resources.DropoffDatetime = DateTime.Parse(DropoffDateTime.Value);
            }
        }
        else 
        {
            XmlAttribute PickupDateTime = xmlDocument.DocumentElement.SelectSingleNode("//a1:VehRentalCore ", namespaceManager).Attributes["PickUpDateTime"];
            XmlAttribute DropoffDateTime = xmlDocument.DocumentElement.SelectSingleNode("//a1:VehRentalCore ", namespaceManager).Attributes["ReturnDateTime"];
            string pickUpStringTime = PickupDateTime.Value.Split('T')[1];
            if (pickUpStringTime == "00:00:00")
            {
                PickupDateTime.Value = PickupDateTime.Value.Split('T')[0] + "T" + "00:01:01";
            }
            string dropOffStringTime = DropoffDateTime.Value.Split('T')[1];
            if (dropOffStringTime == "00:00:00")
            {
                DropoffDateTime.Value = DropoffDateTime.Value.Split('T')[0] + "T" + "00:01:01";
            }
            resources.PickupDateTime = DateTime.Parse(PickupDateTime.Value);
            resources.DropoffDatetime = DateTime.Parse(DropoffDateTime.Value);
        }
    }

    private void setCorpDiscountNo(GenericProcessResources resources)
    {
        XmlNode RateQualifierNode = xmlDocument.DocumentElement.SelectSingleNode("//a1:RateQualifier", namespaceManager);
        if (RateQualifierNode != null)
        {
            if (RateQualifierNode.Attributes["CorpDiscountNmbr"] != null)
            {
                resources.CorpDiscountNo = RateQualifierNode.Attributes["CorpDiscountNmbr"].Value;
            }
        }
    }
}