﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Exceptions;
using ORM;
using Templates;

/// <summary>
/// Summary description for ProcessEngine
/// </summary>
public class ProcessEngine
{
    public void initProcess(HttpContext context)
    {
        context.Response.ContentType = Configs.RESPONSE_CONTENT_TYPE;
        context.Response.ContentEncoding = Configs.RESPONSE_ENCODE_TYPE;
        var streamReader = new StreamReader(context.Request.InputStream, Configs.REQUEST_ENCODE_TYPE);
        string payloadXml = streamReader.ReadToEnd().Trim();
        var routeValues = context.Request.RequestContext.RouteData.Values;
        //RemoteDebugger.SendXmlForDebugging(payloadXml,"Engine XML");
        //RemoteDebugger.SendXmlForDebugging(routeValues["brand"].ToString(), "Engine Route");
        string pathParamBrand = routeValues["brand"].ToString().ToLower();
        createProcessInstance(payloadXml, pathParamBrand, context);
    }

    public void createProcessInstance(string payload, string pathParamBrand, HttpContext context)
    {
        BrandResolver brandResolver = new BrandResolver();
        APIResolver apiResolver = new APIResolver();
        PartnerResolver partnerResolver = new PartnerResolver();
        ProviderResolver providerResolver = new ProviderResolver();
        ProcessFactory processFactory = new ProcessFactory();
        IProcess process = null;
        XMLAPITypes apiType;
        try{
           apiType = apiResolver.getAPICallType(payload);
           process = processFactory.createProcess(apiType, payload);
        }catch(InvalidRequestException ex){
            onErrorProcess(ex.Message, context);
            return;
        }
        process.execute(brandResolver, partnerResolver, providerResolver, pathParamBrand, payload, apiType, context);
    }

    private void onErrorProcess(string message, HttpContext context)
    {
        context.Response.Write(ErrorXmlTemplate.getUnhandledErrorXml(message));    
    }

}