﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;
using ORM;
using ProcessResources;

/// <summary>
/// Summary description for ErrorLogger
/// </summary>
public class ErrorLogger
{
    public void logErrorsInDatabase(GenericProcessResources processResource, DatabaseEntities db, XmlNamespaceManager namespaceManager)
    {
        DateTime currentDate = DateTime.Now;
        XMLErrorLog errorLog = new XMLErrorLog
        {
            CountryOfResidence = processResource.SourceCountryCode,
            Brand = processResource.BrandName,
            XML_Provider = processResource.ProviderName,
            XML_ErrorCode = "",
            XML_ErrorMsg = "",
            PickUpLocation = processResource.PickupLocationCode,
            DropOffLocation = processResource.DropoffLocationCode,
            PickUpDate = processResource.PickupDateTime,
            DropOffDate = processResource.DropoffDatetime,
            ErrorDate = currentDate,
            CameFrom = processResource.PartnerName
        };
        db.XMLErrorLogs.Add(errorLog);
        //db.SaveChanges();
        
        string validatedXmlRequestString = XmlUtils.getXmlString(processResource.OriginalRequest);
        if (processResource.ValidatedRequest != null)
        {
            validatedXmlRequestString = XmlUtils.getXmlString(processResource.ValidatedRequest);
        }
        string receivedXmlResponseString = XmlUtils.getXmlString(processResource.ValidatedResponse);
        Logger.Debug("Error Logger Response XML : \n" + receivedXmlResponseString);
        XMLErrorLogSent validatedXmlRequest = new XMLErrorLogSent
        {
            XMLErrorLogIDFK = errorLog.XMLErrorLogID,
            XMLSent = validatedXmlRequestString
        };
        db.XMLErrorLogSents.Add(validatedXmlRequest);

        XMLErrorLogReceived receivedXmlResponse = new XMLErrorLogReceived
        {
            XMLErrorLogIDFK = errorLog.XMLErrorLogID,
            XMLReceived = receivedXmlResponseString
        };
        db.XMLErrorLogReceiveds.Add(receivedXmlResponse);
        //db.SaveChanges();
    }
}