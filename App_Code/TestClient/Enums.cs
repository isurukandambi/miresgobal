﻿namespace TestClient
{
    // Holds the list of Providers in use on the site
    public enum CarProvider
    {
        Hertz,
        Thrifty,
        FireFly,
        Dollar
    }

    public enum CallType
    {
        VehAvailRateRQ,
        AccessoryRQ,
        BookingRQ
    }
}