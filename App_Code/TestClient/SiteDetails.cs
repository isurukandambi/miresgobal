﻿namespace TestClient{

using System;
using System.Collections.Generic;
using System.Web;

/// <summary>
/// Classes to handle the booking flow on the website.
/// 
/// Includes:
/// 
/// Booking: store booking travel information entered on the booking form
/// Car: store each car's details returned from the XML
/// CarLocationDetails: store the pickup address and time returned from the XML
/// Customer: stores the customer information entered on the reivew form
/// Error: to store any error codes we get returned from the XML
/// 
/// </summary>

[Serializable]
public class Booking
{
    public Booking()
    {
        Brands = new List<BrandT>();
        OriginalBooking = new OriginalBooking();
        Accessories = new List<AccessoryT>();
        Customer = new Customer();
        ErrorDetails = new ErrorDetails();
        WarningDetails = new List<WarningDetails>();
        AutoEurope = new AutoEuropeBooking();
        FullInclusionList = new List<PriceInclusion>();
        Location = new LocationT();
        PartnerDetails = new PartnerDetails();
    }

    public string Language { get; set; }
    public string Country { get; set; }
    public string PickUpCountryCode { get; set; }

    public string agentDutyCode { get; set; }
    public string PickUpCountryWord { get; set; }
    public string DropOffCountryCode { get; set; }
    public string DropOffCountryWord { get; set; }
    public string PickUpLocationCode { get; set; }
    public string PickUpLocationWord { get; set; }
    public string PickUpLocationType { get; set; }
    public string DropOffLocationCode { get; set; }
    public string DropOffLocationWord { get; set; }
    public string DropOffLocationType { get; set; }
    public DateTime PickupDateTime { get; set; }
    public DateTime DropOffDateTime { get; set; }
    public string FlightNum { get; set; }
    public string TransportService { get; set; }
    public string FrequentTravellerProgramme { get; set; }
    public string FrequentTravellerNumber { get; set; }
    public string CameFrom { get; set; }
    public string ConfirmationId { get; set; }
    public string DriversAge { get; set; }
    public string NumPassengers { get; set; }
    public bool EmailBannerRequest { get; set; }
    public CarProvider CarProviderSelected { get; set; }
    public bool PrePaySelected { get; set; }
    public bool Cancelled { get; set; }
    public bool ModifyProcess { get; set; }
    public decimal CrossBorderDropFee { get; set; }
    public string CrossBorderDropFeeCurrency { get; set; }
    public string CrossBorderDropFeeSymbol { get; set; }
    public decimal PayAtCounterFee { get; set; }
    public string PayAtCounterCurrency { get; set; }
    public string PayAtCounterCurrencySymbol { get; set; }
    public string CountryOfResidenceWord { get; set; }
    public string CountryOfResidenceCode { get; set; }
    public string IataNumber { get; set; }

    public string RateQualifierNumber { get; set; }

    //added for dual rate
    public bool DualRateEnabled { get; set; }
    public bool DisplayDualCurrency { get; set; }
    public bool DisplaySaveNow { get; set; }
    public bool DualRatePriority { get; set; }
    public bool PrePaidPriority { get; set; }
    public bool DeepLinkQuery { get; set; }

    //Moification by DMS Begins
    public bool HasHotDeal { get; set; }
    public bool HotDealsBannerClicked { get; set; }
    public string HotDealsBanner { get; set; }
    public string HotDealCDP { get; set; }
    public string DropoffEnabled { get; set; }
    //Moification by DMS Ends

    //Modification by DMS for airport terminal Begins
    public string TerminalNo { get; set; }
    public string LocationType { get; set; }
    public string DefaultTerminalFlag { get; set; }
    public string TerminalsEnabled { get; set; }
    //Modification by DMS for airport terminal Ends

    public OriginalBooking OriginalBooking { get; set; }
    public PartnerDetails PartnerDetails { get; set; }

    public List<BrandT> Brands { get; set; }
    public List<AccessoryT> Accessories { get; set; }
    public Customer Customer { get; set; }
    public ErrorDetails ErrorDetails { get; set; }

    public List<WarningDetails> WarningDetails { get; set; }
    public AutoEuropeBooking AutoEurope { get; set; }
    public List<PriceInclusion> FullInclusionList { get; set; }
    public LocationT Location { get; set; }
    public string hertzRQ { get; set; }
    public string thriftyRQ { get; set; }
    public string fireflyRQ { get; set; }
    public string dollarRQ { get; set; }

    public Booking Session
    {
        // Initialize class from session if available else new empty class
        get
        {
            if ((HttpContext.Current != null) && (HttpContext.Current.Session != null) && (HttpContext.Current.Session["Booking"] != null))
                return (Booking)HttpContext.Current.Session["Booking"];

            return new Booking();
        }

        // store value into session
        set {
            HttpContext.Current.Session["Booking"] = value;
        }
    }
}

[Serializable]
public class OriginalBooking
{
    public OriginalBooking()
    {
        Accessories = new List<AccessoryT>();
    }
    public decimal OrderPrice { get; set; }
    public string OrderCurrency { get; set; }
    public string DatabaseOrderId { get; set; }
    public Car BookedCar { get; set; }
    public string PickUpCountryCode { get; set; }
    public string PickUpCountryWord { get; set; }
    public string DropOffCountryCode { get; set; }
    public string DropOffCountryWord { get; set; }
    public string PickUpLocationCode { get; set; }
    public string PickUpLocationWord { get; set; }
    public string PickUpLocationType { get; set; }
    public string DropOffLocationCode { get; set; }
    public string DropOffLocationWord { get; set; }
    public string DropOffLocationType { get; set; }
    public DateTime PickupDateTime { get; set; }
    public DateTime DropOffDateTime { get; set; }
    public decimal CrossBorderDropFee { get; set; }
    public string CrossBorderDropFeeCurrency { get; set; }
    public string CrossBorderDropFeeSymbol { get; set; }
    public decimal PayAtCounterFee { get; set; }
    public string PayAtCounterCurrency { get; set; }
    public string PayAtCounterCurrencySymbol { get; set; }
    public List<AccessoryT> Accessories { get; set; }
    public string FlightNum { get; set; }
    public string TransportService { get; set; }
    public string FrequentTravellerProgramme { get; set; }
    public string FrequentTravellerNumber { get; set; }
    public string IataNumber { get; set; }
    public CarProvider CarProvider { get; set; }
}

// hold additional AutoEurope specific details
[Serializable]
public class AutoEuropeBooking
{
    public string SessionCode { get; set; }
    public string SupplierConfirmation { get; set; }
    public string RateCode { get; set; }
    public string BusinessAccount { get; set; }
    public string PickupProvider { get; set; }
    public string PickupAddress { get; set; }
    public string PickupTel { get; set; }
    public string DropoffProvider { get; set; }
    public string DropoffAddress { get; set; }
    public string DropoffTel { get; set; }
    public string BookingTermsAndConditions { get; set; }
}

[Serializable]
public class BrandT
{
    public BrandT()
    {
        Cars = new List<Car>();
        CarProvider = CarProvider.Hertz;
    }

    public List<Car> Cars { get; set; }
    public CarProvider CarProvider { get; set; }
    public string RateQualifier { get; set; }
    public bool SuperCover { get; set; }
    public bool ShowSuperCoverErrorMessage { get; set; }
    public bool RemovedSuperCover { get; set; }
    public string Cdp { get; set; }
    public bool CdpAutoRemoved { get; set; }
    public string Pc { get; set; }
    public bool PcAutoRemoved { get; set; }
    public string CpNumber { get; set; }
    public string IataNumber { get; set; }
    public string HertzNumber1 { get; set; }
    public string CollectionList { get; set; }
    public string LinktoExternalSite { get; set; }
    public bool IsPrePaid { get; set; }
    public int Order { get; set; }
    public string brandName { get; set; }
}

[Serializable]
public class Car
{
    public string Name { get; set; }
    public string NameNoOrSimilar { get; set; }
    public string Image { get; set; }
    public string AirConditionIndValue { get; set; }
    public string AirConditionInd { get; set; }
    public string AirConditionPref { get; set; }
    public string TransmissionType { get; set; }
    public string TransmissionPref { get; set; }
    public string FuelType { get; set; }
    public string DriveType { get; set; }
    public string SippCode { get; set; }
    public string VehicleCategory { get; set; }
    public string DoorCount { get; set; }
    public string VehClassSize { get; set; }
    public string VehClassSizeValue { get; set; }
    public string HertzReference { get; set; }
    public string CTCarURL { get; set; }
    public string EchoToken { get; set; }
    public string LocalizedName { get; set; }
    public string LocalizedDoorCount { get; set; }
    public string LocalizedTransmissionType { get; set; }
    public string LocalizedVehClassSizeValue { get; set; }
    public decimal Price { get; set; }
    public string Currency { get; set; }
    public string CurrencySymbol { get; set; }
    public string PriceIncludes { get; set; }
    public string DistanceIncluded { get; set; }
    public string OptionalExtras { get; set; }
    public string OptionalExtrasReview { get; set; }
    public bool IsSelected { get; set; }
    public bool IsPrePaid { get; set; }

    public bool IsResRetReturnedCar { get; set; }
    public string NextCarSippCode { get; set; }

    // additional AutoEurope values
    public string QuoteId { get; set; }
    public string CarPassengers { get; set; }
    public string LuggageCapacity { get; set; }
    public string CarCostForXml { get; set; }
    public string PickupLocationId { get; set; }
    public string PickupLocationArea { get; set; }
    public string PickupOpeningHrs { get; set; }
    public string DropoffLocationId { get; set; }
    public string DropoffLocationArea { get; set; }
    public string DropoffOpeningHrs { get; set; }
}

[Serializable]
public class AccessoryT
{
    //public string AccessoryId { get; set; }
    public string Title { get; set; }
    public string Details { get; set; }
    public string Image { get; set; }
    public string Price { get; set; }
    public string AdditionalInfoId { get; set; }
    public string AdditionalInfo { get; set; }
    public int Quantity { get; set; }
    public string EquipType { get; set; } // accessory code
    public string CurrencySymbol { get; set; }
    public bool IsSelected { get; set; }
    public bool Delete { get; set; }
}


[Serializable]
public class Customer
{
    public string Firstname { get; set; }
    public string Surname { get; set; }
    public string AreaCode { get; set; }
    public string PhoneNumber { get; set; }
    public string Email { get; set; }
    public string Address1 { get; set; }
    public string Address2 { get; set; }
    public string Town { get; set; }
    public string Postcode { get; set; }
    public string Country { get; set; }
    public string NameofCardOwner { get; set; }
    public string CardType { get; set; }
    public string CardNumber { get; set; }
    public string CardExpDate { get; set; }
    public string CardCcv { get; set; }
}

[Serializable]
public class ErrorDetails
{
    public string ErrorCode { get; set; }
    public string ErrorMsg { get; set; }
}

[Serializable]
public class WarningDetails
{
    public string WarningCode { get; set; }
    public string WarningShortMsg { get; set; }
    public string WarningType { get; set; }
}

[Serializable]
public class PriceInclusion
{
    public string InclusionWord { get; set; }
    public string Keyword { get; set; }
    public string InclusionType { get; set; }
    public bool Included { get; set; }
}

[Serializable]
public class OptionalExtra
{
    public string CoverageType { get; set; }
    public string Keyword { get; set; }
    public string CurrencyCode { get; set; }
    public string CurrencySymbol { get; set; }
    public string UnitCharge { get; set; }
    public string Quantity { get; set; }
    public string UnitName { get; set; }
}

[Serializable]
public class LocationT
{
    public string LocationAddress { get; set; }
    public string LocationTelNumber { get; set; }
    public string OpeningHrs { get; set; }
}


[Serializable]
public class RyPreReqDBBooking
{
    public CarProvider brand { get; set; }
    public string shortPickupLocationCode { get; set; }
    public string longPickupLocationCode { get; set; }
    public string shortReturnLocationCode { get; set; }
    public string longReturnLocationCode { get; set; }
    public Dictionary<string, RyDBCarObj> predictedCarList { get; set; }
    public string PickUpCountryCode { get; set; }
    public string PickUpCountryWord { get; set; }
    public string PickUpLocationWord { get; set; }
    public string DropOffCountryCode { get; set; }
    public string DropOffCountryWord { get; set; }
    public string DropOffLocationWord { get; set; }
    public string RequestRateQualifire { get; set; }

    public string RequestVendorCode { get; set; }

    public Dictionary<string, ORM.Accessory> accessories { get; set; }

    public List<ErrorDetails> errors { get; set; }

}


[Serializable]
public class RyDBCarObj
{
    public string carDescription { get; set; }
    public string carImagePath { get; set; }
}


[Serializable]
public class PartnerDetails
{

    public string agentDutyCode { get; set; }
    public string requestorID { get; set; }
    public string requestorIDType { get; set; }
    public string companyNameCode { get; set; }

    public string codeContext { get; set; }

}

}
