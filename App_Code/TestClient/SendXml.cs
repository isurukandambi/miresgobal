﻿namespace TestClient
{

    using System;
    using System.Net;
    using System.Web;
    using System.IO;
    using System.Text;

    /// <summary>
    /// The class below will send any XML request and return the XML result
    /// </summary>
    public class SendXml
    {
        //send XML details and receive the response.
        public static string HttpPost(string uri, string parameters, DateTime requestTime, string requestedDomain, string brand, string XMLType)
        {

            Encoding encoding = Encoding.GetEncoding("ISO-8859-1");
            DateTime XMLProviderSendTime = new DateTime(2014, 1, 01);
            DateTime XMLProviderReceivedTime = new DateTime(2014, 1, 01);
            try
            {
                var req = WebRequest.Create(uri);
                req.Timeout = 25000;
                req.ContentType = "text/xml";
                req.Method = "POST";
                req.Headers.Add("X-Forwarded-For", GetIp());

                var bytes = Encoding.ASCII.GetBytes(parameters);
                req.ContentLength = bytes.Length;

                var os = req.GetRequestStream();
                os.Write(bytes, 0, bytes.Length);
                os.Close();

                var resp = req.GetResponse();
                if (resp == null) return CommonTasks.GetErrorXML(XMLType, "1", "052", "empty"); ;
                var sr = new StreamReader(resp.GetResponseStream(), encoding);
                return sr.ReadToEnd().Trim();
            }
            catch (Exception e)
            {
                // XMLProviderSendTime = DateTime.Now;
                XMLProviderReceivedTime = DateTime.Now;
                return CommonTasks.GetErrorXML(XMLType, "1", "052", e.Message);

                //CommonTasks.LogDBTimeOutRequest(parameters, requestedDomain, requestTime, XMLProviderSendTime, XMLProviderReceivedTime, brand);

                //xml server is timing out
                // return "<?xml version=\"1.0\" encoding=\"UTF-8\"?><OTA_VehAvailRateRS xmlns=\"http://www.opentravel.org/OTA/2003/05\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.opentravel.org/OTA/2003/05\"><Errors><Error Type=\"1\" ShortText=\"THE REQUEST HAS TIMED OUT - PLEASE RETRY - Exception : "+e.Message+" \" RecordID=\"099\"/></Errors></OTA_VehAvailRateRS>";
                // return CommonTasks.GetErrorXML("VehResRS", "1", "THE REQUEST HAS TIMED OUT - PLEASE RETRY - Exception : " + e.Message + "", "099");
                //return CommonTasks.GetErrorXML(XMLType, "1", "052", WebsiteSettings.RequestTimeoutErrorMessage.ToString());
                //return CommonTasks.GetErrorXML(XMLType, "1", "052", e.Message);
            }
        }




        public static string HttpPost(string uri, string parameters, string brand, string XMLType)
        {

            Encoding encoding = Encoding.GetEncoding("ISO-8859-1");
            DateTime XMLProviderSendTime = new DateTime(2014, 1, 01);
            DateTime XMLProviderReceivedTime = new DateTime(2014, 1, 01);
            try
            {
                var req = WebRequest.Create(uri);
                req.Timeout = 25000;
                req.ContentType = "text/xml";
                req.Method = "POST";
                req.Headers.Add("X-Forwarded-For", GetIp());

                var bytes = Encoding.ASCII.GetBytes(parameters);
                req.ContentLength = bytes.Length;

                var os = req.GetRequestStream();
                os.Write(bytes, 0, bytes.Length);
                os.Close();

                var resp = req.GetResponse();
                if (resp == null) return null;
                var sr = new StreamReader(resp.GetResponseStream(), encoding);
                return sr.ReadToEnd().Trim();
            }
            catch (Exception e)
            {
                // XMLProviderSendTime = DateTime.Now;
                XMLProviderReceivedTime = DateTime.Now;
                return "";

            }
        }


        public static string HttpPostClient(string uri, string parameters, string brand, string XMLType)
        {

            Encoding encoding = Encoding.GetEncoding("ISO-8859-1");
            DateTime XMLProviderSendTime = new DateTime(2014, 1, 01);
            DateTime XMLProviderReceivedTime = new DateTime(2014, 1, 01);
            //HttpStatusCode status;
            try
            {
                //CommonTasks.SendXmlForDebugging("try in", "sendXMLOne");
                var req = WebRequest.Create(uri);
                req.Timeout = 40000;//Convert.ToInt32(WebsiteSettings.RequestTime);
                req.ContentType = "text/xml";
                req.Method = "POST";
                req.Headers.Add("X-Forwarded-For", GetIp());
                //CommonTasks.SendXmlForDebugging(req.Headers.ToString(), "sendXMLOne");

                var bytes = Encoding.ASCII.GetBytes(parameters);
                req.ContentLength = bytes.Length;
                //CommonTasks.SendXmlForDebugging(bytes.ToString(), "sendXML");
                var os = req.GetRequestStream();
                //CommonTasks.SendXmlForDebugging(os.ToString(), "sendXMLNext");
                os.Write(bytes, 0, bytes.Length);
                os.Close();
                //CommonTasks.SendXmlForDebugging(os.ToString(), "sendXML");
                var resp = req.GetResponse();
                //CommonTasks.SendXmlForDebugging(resp.ToString(), "sendXMLNext");
                if (resp == null) return null;
                var sr = new StreamReader(resp.GetResponseStream(), encoding);
                return sr.ReadToEnd().Trim();

            }
            catch (Exception e)
            {
                return "";
                //CommonTasks.GetErrorXML(XMLType, "1", "052", e.Message);
            }

        }

        public string createPostRequest(string url, string postData)
        {
            WebRequest request = WebRequest.Create(url);
            request.Method = "POST";
            request.ContentType = "text/xml";
            request.Timeout = 25000;
            byte[] byteArray = Encoding.UTF8.GetBytes(postData);
            request.ContentLength = byteArray.Length;
            Stream dataStream = request.GetRequestStream();
            dataStream.Write(byteArray, 0, byteArray.Length);
            dataStream.Close();
            WebResponse response = request.GetResponse();

            dataStream = response.GetResponseStream();
            StreamReader reader = new StreamReader(dataStream);
            string responseFromServer = reader.ReadToEnd();
            reader.Close();
            dataStream.Close();
            response.Close();

            return responseFromServer;
        }


        public static String GetIp()
        {
            String ip = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (string.IsNullOrEmpty(ip))
            {
                ip = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
            }

            // if the nightyscript runs it will set the IP addres to be a local address so override this with the front end website IP.
            if (ip == "192.168.202.80")
            {
                ip = "94.236.111.80";
            }

            return ip;
        }
    }
}
