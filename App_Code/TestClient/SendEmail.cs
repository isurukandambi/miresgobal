﻿namespace TestClient
{

    using System;
    using System.Collections.Generic;
    using System.Web;
    using System.Net.Mail;

    /// <summary>
    /// Send an email from the website.
    /// Expects: string, string, string, string.
    /// Returns: bool.
    /// </summary>
    public class SendEmail
    {
        public static bool SendMessage(string FromAddress, string ToAddress, string Subject, string EmailMessage, bool HTMLMessage)
        {
            bool MsgSent = false;
            try
            {
                MailMessage mailMessage = new MailMessage();
                mailMessage.From = new MailAddress(FromAddress);
                mailMessage.To.Add(new MailAddress(ToAddress));
                mailMessage.Subject = Subject;
                mailMessage.Body = EmailMessage;
                mailMessage.IsBodyHtml = HTMLMessage;
                SmtpClient smtpClient = new SmtpClient();
                smtpClient.Send(mailMessage);
                MsgSent = true;
            }
            catch
            {
                MsgSent = false;
            }
            return MsgSent;
        }
    }
}
