﻿namespace TestClient{
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Linq;
using ORM;
using CMSConfigs;

/// <summary>
/// Summary description for CommonTasks
/// </summary>
public partial class CommonTasks
{

    //public static Dictionary<string, string> GetDomainConfigsByDomainAndBrand(string brandName, string requestedDomain)
    //{

    //   // SendXmlForDebugging(brandName, "Brand");
    //    //SendXmlForDebugging(requestedDomain,"Partner");


    //    DatabaseEntities db = new DatabaseEntities();
    //    var domainConfigData = (from p in db.Partners
    //                            join bp in db.PatnerBrandMappings on p.PartnerId equals bp.PartnerId
    //                            join b in db.Brands on bp.BrandId equals b.BrandID
    //                            where p.DomainName == requestedDomain && b.BrandName == brandName
    //                            select new
    //                            {
    //                                bp.XMLProviderRateQualifier,
    //                                bp.VendorCode,
    //                            });
    //    if (domainConfigData.FirstOrDefault() != null)
    //    {
    //        Dictionary<string, string> domainConfigs = new Dictionary<string, string>();
    //        domainConfigs["RateQualifier"] = domainConfigData.FirstOrDefault().XMLProviderRateQualifier;
    //        domainConfigs["VendorCode"] = domainConfigData.FirstOrDefault().VendorCode;
    //        return domainConfigs;
    //    }
    //    else
    //    {
    //        return null;
    //    }
    //}



    //public static string GetXMLProviderRateQualifire(string brandName, string requestedDomain,string partnerRateQualifire)
    //{

    //    DatabaseEntities db = new DatabaseEntities();
    //    var XMLProviderData = (from p in db.Partners
    //                            join bp in db.PatnerBrandMappings on p.PartnerId equals bp.PartnerId
    //                            join b in db.Brands on bp.BrandId equals b.BrandID
    //                            where p.DomainName == requestedDomain && b.BrandName == brandName && bp.PartnerRateQualifier == partnerRateQualifire
    //                            select new
    //                            {
    //                                bp.XMLProviderRateQualifier,
    //                            });
    //    if (XMLProviderData.FirstOrDefault() != null)
    //    {
    //        string XMLProviderRQ = null;
    //        XMLProviderRQ = XMLProviderData.FirstOrDefault().XMLProviderRateQualifier;

    //        return XMLProviderRQ;
    //    }
    //    else
    //    {
    //        return null;
    //    }
    //}




    //public static string GetRequesrVendorCode(string brandName, string requestedDomain)
    //{

    //    DatabaseEntities db = new DatabaseEntities();
    //    var requestVendorCodeData = (from p in db.Partners
    //                            join bp in db.PatnerBrandMappings on p.PartnerId equals bp.PartnerId
    //                            join b in db.Brands on bp.BrandId equals b.BrandID
    //                            where p.DomainName == requestedDomain && b.BrandName == brandName
    //                            select new
    //                            {
    //                                bp.RequestVendorCode
    //                            });
    //    if (requestVendorCodeData.FirstOrDefault() != null)
    //    {   
    //        string rqVendor = requestVendorCodeData.FirstOrDefault().RequestVendorCode;
    //        return rqVendor;

    //    }
    //    else
    //    {
    //        return null;
    //    }
    //}



    public static string FKconstrainsFail()
    {
        string meaasge = "Operation Fail. This entry has already used.";
        return meaasge;
    }

    public static string GetErrorXML(string xmlCallType, string errorType, string errorCode, string error)
    {
        return "<?xml version=\"1.0\" encoding=\"UTF-8\"?><OTA_" + xmlCallType + " xmlns=\"http://www.opentravel.org/OTA/2003/05\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.opentravel.org/OTA/2003/05\"><Errors><Error Type=\"" + errorType + "\" ShortText=\"" + error + " \" RecordID=\"" + errorCode + "\" Code=\"120\"/></Errors></OTA_" + xmlCallType + ">";
    }

    //only send the XML in an email if the web.config setting is turned on
    //public static void SendXmlForDebugging(string xmlDetails, string subject)
    //{
    //    if (CMSSettings.DebugXmlDetails == "1")
    //    {
    //        string[] emailAccounts = CMSSettings.DebugXmlDetailsNotifyEmails.Split(',');
    //        foreach(string emailAccount in emailAccounts){
    //            SendEmail.SendMessage(CMSSettings.SiteEmailsSentFrom, emailAccount, subject + " " + DateTime.Now, xmlDetails, false);
    //        }
    //    }
    //}

    //Get operating Company Code for any request
    //public static string GetOperatingCompanyCode(XmlNode operatingCompanyNode)
    //{
    //    string opCompanyCode = "";
    //    if (operatingCompanyNode != null)
    //    {
    //        if (operatingCompanyNode.Attributes != null)
    //        {
    //            XmlAttribute OperatingCompanyCode = operatingCompanyNode.Attributes["Code"];
    //            opCompanyCode = OperatingCompanyCode.Value;
    //        }
    //    }
    //    return opCompanyCode;
    //}

    //Language code return from the cultural code
    //public static string GetLanguageCode(string CulturalCode)
    //{
        
    //    string[] LanguageCode = CulturalCode.Split('-');

    //    return LanguageCode[0];
    //}
    //public static string GetTermCodeTH(string TermCode, string Country)
    //{

    //    string Term;
       
    //    if (TermCode == "FUEL" && Country == "FRANCE")
    //    {
    //        Term = "FUEL2";
    //    }
    //    else if (TermCode == "FUEL" && Country == "BELGIUM")
    //    {
    //        Term = "FUEL2";
    //    }
    //    else if (TermCode == "FUEL" && Country == "ITALY")
    //    {
    //        Term = "FUEL1";
    //    }
    //    else if (TermCode == "FUEL" && Country == "PORTUGAL")
    //    {
    //        Term = "FUEL1";
    //    }
    //    else if (TermCode == "FUEL" && Country == "SPAIN")
    //    {
    //        Term = "FUEL2";
    //    }

    //    else if (TermCode == "VLF" && Country == "NETHERLANDS")
    //    {
    //        Term = "VLF2";
    //    }
    //    else if (TermCode == "VLF" && (Country == "BULGARIA" || Country == "MALTA" || Country == "NETHERLANDS"))
    //    {
    //        Term = "VLF2";
    //    }
    //    else if (TermCode == "VLF" && Country == "GERMANY")
    //    {
    //        Term = "F1";
    //    }
    //    else if (TermCode == "VLF" && Country == "IRELAND")
    //    {
    //        Term = "TOLL";
    //    }      
    //    else if (TermCode == "VLF" && Country == "LITHUANIA")
    //    {
    //        Term = "F3";
    //    }
    //    else if (TermCode == "VLF" && Country == "NORWAY")
    //    {
    //        Term = "ROADTAX";
    //    }
    //    else if (TermCode == "VLF" && (Country == "BELGIUM" || Country == "CROATIA" || Country == "GREECE" || Country == "MONTENEGRO" || Country == "MOROCCO" || Country == "POLAND" || Country == "SPAIN"))
    //    {
    //        Term = "NRF";
    //    }
    //    else if (TermCode == "MFP")
    //    {
    //        Term = "NRF";
    //    }
    //    else 
    //    {
    //        Term = TermCode;
    //    }


    //    return Term;
    //}
    //public static string GetTermCodeFF(string TermCode, string Country)
    //{

    //    string Term;

    //    if (TermCode == "FUEL" && Country == "FRANCE")
    //    {
    //        Term = "FUEL2";
    //    }
    //    else if (TermCode == "FUEL" && Country == "ITALY")
    //    {
    //        Term = "FUEL1";
    //    }
    //    else if (TermCode == "FUEL" && Country == "PORTUGAL")
    //    {
    //        Term = "FUEL1";
    //    }
    //    else if (TermCode == "FUEL" && Country == "SPAIN")
    //    {
    //        Term = "FUEL2";
    //    }

    //    else if (TermCode == "VLF" && Country == "NETHERLANDS")
    //    {
    //        Term = "VLF2";
    //    }
    //    else
    //    {
    //        Term = TermCode;
    //    }

    //    return Term;
    //}
    //// log any errors returned from XML into database
    //public static bool CheckErrorsAndLogErrorsInDatabase(string originalXMLRequest, string validatedXMLRequest, string originalXMLReceived, string validatedXMLReceived, CarProvider xmlProvider, string requestedDomain, RyPreReqDBBooking ryPreReqObj)
    //{
    //   bool hasErrors = false;
    //    try
    //    {
    //        if ((!String.IsNullOrEmpty(originalXMLRequest)))
    //        {
    //            // remove any credit card information in response
    //            originalXMLRequest = RemoveCreditCardDetails(originalXMLRequest, xmlProvider);

    //            DateTime todaysDate = DateTime.Now;
    //            string todaysDateString = todaysDate.ToString("yyyy/MM/dd HH:mm:ss", CultureInfo.InvariantCulture);

    //            var originalXML = new XmlDocument();
    //            originalXML.LoadXml(originalXMLRequest);
    //            var namespaceMgrOriginalXML = new XmlNamespaceManager(originalXML.NameTable);
    //            namespaceMgrOriginalXML.AddNamespace("a1", "http://www.opentravel.org/OTA/2003/05");

    //            var validatedXMLRQ = new XmlDocument();
    //            validatedXMLRQ.LoadXml(validatedXMLRequest);
    //            var namespaceMgrValidatedXMLRQ = new XmlNamespaceManager(validatedXMLRQ.NameTable);
    //            namespaceMgrValidatedXMLRQ.AddNamespace("a1", "http://www.opentravel.org/OTA/2003/05");

    //            var originalXMLRS = new XmlDocument();
    //            originalXMLRS.LoadXml(originalXMLReceived);
    //            var namespaceMgrOriginalXMLRS = new XmlNamespaceManager(originalXMLRS.NameTable);
    //            namespaceMgrOriginalXMLRS.AddNamespace("a1", "http://www.opentravel.org/OTA/2003/05");

    //            var validatedXMLRS = new XmlDocument();
    //            validatedXMLRS.LoadXml(validatedXMLReceived);
    //            var namespaceMgrValidatedXMLRS = new XmlNamespaceManager(validatedXMLRS.NameTable);
    //            namespaceMgrValidatedXMLRS.AddNamespace("a1", "http://www.opentravel.org/OTA/2003/05");

    //            XmlNodeList errorListRQ = validatedXMLRQ.DocumentElement.SelectNodes(".//a1:Error", namespaceMgrValidatedXMLRQ);
    //            XmlNodeList errorListORI = originalXMLRS.DocumentElement.SelectNodes(".//a1:Error", namespaceMgrOriginalXMLRS);
    //            XmlNodeList errorListRS = validatedXMLRS.DocumentElement.SelectNodes(".//a1:Error", namespaceMgrValidatedXMLRS);

    //            List<ErrorDetails> errorList = new List<ErrorDetails>();


    //            if (errorListRQ.Count > 0 || errorListORI.Count > 0 || errorListRS.Count > 0)
    //            {
    //                hasErrors = true;
    //                string ErrorCode = "";
    //                string ErrorMsg = "";
    //                if (validatedXMLRQ.DocumentElement.SelectSingleNode("//a1:Errors", namespaceMgrValidatedXMLRQ) != null && validatedXMLRQ.DocumentElement.SelectSingleNode("//a1:Errors", namespaceMgrValidatedXMLRQ).Attributes != null)
    //                {
    //                    ErrorCode = validatedXMLRQ.DocumentElement.SelectSingleNode("//a1:Error", namespaceMgrValidatedXMLRQ).Attributes["Code"].Value;
    //                    ErrorMsg = validatedXMLRQ.DocumentElement.SelectSingleNode("//a1:Error", namespaceMgrValidatedXMLRQ).Attributes["ShortText"].Value;
    //                    ErrorDetails error = new ErrorDetails
    //                    {
    //                        ErrorCode = ErrorCode,
    //                        ErrorMsg = ErrorMsg
    //                    };
    //                    errorList.Add(error);

    //                }
    //                if (originalXMLRS.DocumentElement.SelectSingleNode("//a1:Errors", namespaceMgrOriginalXMLRS) != null && originalXMLRS.DocumentElement.SelectSingleNode("//a1:Errors", namespaceMgrOriginalXMLRS).Attributes != null)
    //                {
    //                    ErrorCode = originalXMLRS.DocumentElement.SelectSingleNode("//a1:Error", namespaceMgrOriginalXMLRS).Attributes["Code"].Value;
    //                    ErrorMsg = originalXMLRS.DocumentElement.SelectSingleNode("//a1:Error", namespaceMgrOriginalXMLRS).Attributes["ShortText"].Value;

                        
                        
    //                    ErrorDetails error = new ErrorDetails
    //                    {
    //                        ErrorCode = ErrorCode,
    //                        ErrorMsg = ErrorMsg
    //                    };
    //                    errorList.Add(error);

    //                }
    //                if (validatedXMLRS.DocumentElement.SelectSingleNode("//a1:Errors", namespaceMgrValidatedXMLRS) != null && validatedXMLRS.DocumentElement.SelectSingleNode("//a1:Errors", namespaceMgrValidatedXMLRS).Attributes != null)
    //                {
    //                    if (validatedXMLRS.DocumentElement.SelectSingleNode("//a1:Error", namespaceMgrValidatedXMLRS) != null)
    //                    {
    //                        if (validatedXMLRS.DocumentElement.SelectSingleNode("//a1:Error", namespaceMgrValidatedXMLRS).Attributes != null)
    //                        {
    //                            ErrorCode = validatedXMLRS.DocumentElement.SelectSingleNode("//a1:Error", namespaceMgrValidatedXMLRS).Attributes["Code"].Value;
    //                            ErrorMsg = validatedXMLRS.DocumentElement.SelectSingleNode("//a1:Error", namespaceMgrValidatedXMLRS).Attributes["ShortText"].Value;
    //                            ErrorDetails error = new ErrorDetails
    //                            {
    //                                ErrorCode = ErrorCode,
    //                                ErrorMsg = ErrorMsg
    //                            };
    //                            errorList.Add(error);
    //                        }
    //                    }
    //                }
    //            }

    //            ryPreReqObj.errors = errorList;
    //        }
    //    }
    //    catch (Exception e)
    //    {
    //        CommonTasks.SendXmlForDebugging(e.StackTrace,"Error DB Logging Failed...");
    //        hasErrors = true;
    //    }

    //    return hasErrors;
    //}


    //public static bool DTagCheckErrorsAndLogErrorsInDatabase(string originalXMLRequest, string originalXMLReceived, CarProvider xmlProvider, string requestedDomain, RyPreReqDBBooking ryPreReqObj)
    //{
    //    bool hasErrors = false;
    //    try
    //    {
    //        if ((!String.IsNullOrEmpty(originalXMLRequest)))
    //        {
    //            // remove any credit card information in response
    //            originalXMLRequest = RemoveCreditCardDetails(originalXMLRequest, xmlProvider);

    //            DateTime todaysDate = DateTime.Now;
    //            string todaysDateString = todaysDate.ToString("yyyy/MM/dd HH:mm:ss", CultureInfo.InvariantCulture);

    //            var originalXML = new XmlDocument();
    //            originalXML.LoadXml(originalXMLRequest);
    //            var namespaceMgrOriginalXML = new XmlNamespaceManager(originalXML.NameTable);
    //            namespaceMgrOriginalXML.AddNamespace("a1", "http://www.opentravel.org/OTA/2003/05");

                
              
    //            var originalXMLRS = new XmlDocument();
    //            originalXMLRS.LoadXml(originalXMLReceived);
    //            var namespaceMgrOriginalXMLRS = new XmlNamespaceManager(originalXMLRS.NameTable);
    //            namespaceMgrOriginalXMLRS.AddNamespace("a1", "http://www.opentravel.org/OTA/2003/05");

                
    //            XmlNodeList errorListORI = originalXMLRS.DocumentElement.SelectNodes(".//a1:Error", namespaceMgrOriginalXMLRS);
                
    //            List<ErrorDetails> errorList = new List<ErrorDetails>();


    //            if (errorListORI.Count > 0 )
    //            {
    //                hasErrors = true;
    //                string ErrorCode = "";
    //                string ErrorMsg = "";
                    
    //                if (originalXMLRS.DocumentElement.SelectSingleNode("//a1:Errors", namespaceMgrOriginalXMLRS) != null && originalXMLRS.DocumentElement.SelectSingleNode("//a1:Errors", namespaceMgrOriginalXMLRS).Attributes != null)
    //                {
    //                    ErrorCode = originalXMLRS.DocumentElement.SelectSingleNode("//a1:Error", namespaceMgrOriginalXMLRS).Attributes["Code"].Value;
    //                    ErrorMsg = originalXMLRS.DocumentElement.SelectSingleNode("//a1:Error", namespaceMgrOriginalXMLRS).Attributes["ShortText"].Value;



    //                    ErrorDetails error = new ErrorDetails
    //                    {
    //                        ErrorCode = ErrorCode,
    //                        ErrorMsg = ErrorMsg
    //                    };
    //                    errorList.Add(error);

    //                }
                    
    //            }

    //            ryPreReqObj.errors = errorList;
    //        }
    //    }
    //    catch (Exception e)
    //    {
    //        CommonTasks.SendXmlForDebugging(e.StackTrace, "Error DB Logging Failed...");
    //        hasErrors = true;
    //    }

    //    return hasErrors;
    //}





    //public static bool CheckErrorsAndLogErrorsInDatabase(string originalXMLRequest, string validatedXMLRequest, string originalXMLReceived, string validatedXMLReceived, CarProvider xmlProvider, string requestedDomain)
    //{
    //    bool hasErrors = false;
    //    try
    //    {
    //        if ((!String.IsNullOrEmpty(originalXMLRequest)))
    //        {
    //            // remove any credit card information in response
    //            originalXMLRequest = RemoveCreditCardDetails(originalXMLRequest, xmlProvider);

    //            DateTime todaysDate = DateTime.Now;
    //            string todaysDateString = todaysDate.ToString("yyyy/MM/dd HH:mm:ss", CultureInfo.InvariantCulture);

    //            var originalXML = new XmlDocument();
    //            originalXML.LoadXml(originalXMLRequest);
    //            var namespaceMgrOriginalXML = new XmlNamespaceManager(originalXML.NameTable);
    //            namespaceMgrOriginalXML.AddNamespace("a1", "http://www.opentravel.org/OTA/2003/05");

    //            var validatedXMLRQ = new XmlDocument();
    //            validatedXMLRQ.LoadXml(validatedXMLRequest);
    //            var namespaceMgrValidatedXMLRQ = new XmlNamespaceManager(validatedXMLRQ.NameTable);
    //            namespaceMgrValidatedXMLRQ.AddNamespace("a1", "http://www.opentravel.org/OTA/2003/05");

    //            var originalXMLRS = new XmlDocument();
    //            originalXMLRS.LoadXml(originalXMLReceived);
    //            var namespaceMgrOriginalXMLRS = new XmlNamespaceManager(originalXMLRS.NameTable);
    //            namespaceMgrOriginalXMLRS.AddNamespace("a1", "http://www.opentravel.org/OTA/2003/05");

    //            var validatedXMLRS = new XmlDocument();
    //            validatedXMLRS.LoadXml(validatedXMLReceived);
    //            var namespaceMgrValidatedXMLRS = new XmlNamespaceManager(validatedXMLRS.NameTable);
    //            namespaceMgrValidatedXMLRS.AddNamespace("a1", "http://www.opentravel.org/OTA/2003/05");

    //            XmlNodeList errorListRQ = validatedXMLRQ.DocumentElement.SelectNodes(".//a1:Error", namespaceMgrValidatedXMLRQ);
    //            XmlNodeList errorListORI = originalXMLRS.DocumentElement.SelectNodes(".//a1:Error", namespaceMgrOriginalXMLRS);
    //            XmlNodeList errorListRS = validatedXMLRS.DocumentElement.SelectNodes(".//a1:Error", namespaceMgrValidatedXMLRS);

    //            if (errorListRQ.Count > 0 || errorListORI.Count > 0 || errorListRS.Count > 0)
    //            {
    //                hasErrors = true;
    //                string ErrorCode = "";
    //                string ErrorMsg = "";
    //                if (validatedXMLRQ.DocumentElement.SelectSingleNode("//a1:Errors", namespaceMgrValidatedXMLRQ) != null && validatedXMLRQ.DocumentElement.SelectSingleNode("//a1:Errors", namespaceMgrValidatedXMLRQ).Attributes != null)
    //                {
    //                    ErrorCode = validatedXMLRQ.DocumentElement.SelectSingleNode("//a1:Error", namespaceMgrValidatedXMLRQ).Attributes["Code"].Value;
    //                    ErrorMsg = validatedXMLRQ.DocumentElement.SelectSingleNode("//a1:Error", namespaceMgrValidatedXMLRQ).Attributes["ShortText"].Value;
    //                }
    //                else if (originalXMLRS.DocumentElement.SelectSingleNode("//a1:Errors", namespaceMgrOriginalXMLRS) != null && originalXMLRS.DocumentElement.SelectSingleNode("//a1:Errors", namespaceMgrOriginalXMLRS).Attributes != null)
    //                {
    //                    ErrorCode = originalXMLRS.DocumentElement.SelectSingleNode("//a1:Error", namespaceMgrOriginalXMLRS).Attributes["Code"].Value;
    //                    ErrorMsg = originalXMLRS.DocumentElement.SelectSingleNode("//a1:Error", namespaceMgrOriginalXMLRS).Attributes["ShortText"].Value;
    //                }
    //                else if (validatedXMLRS.DocumentElement.SelectSingleNode("//a1:Errors", namespaceMgrValidatedXMLRS) != null && validatedXMLRS.DocumentElement.SelectSingleNode("//a1:Errors", namespaceMgrValidatedXMLRS).Attributes != null)
    //                {
    //                    if (validatedXMLRS.DocumentElement.SelectSingleNode("//a1:Error", namespaceMgrValidatedXMLRS) != null)
    //                    {
    //                        if (validatedXMLRS.DocumentElement.SelectSingleNode("//a1:Error", namespaceMgrValidatedXMLRS).Attributes != null)
    //                        {
    //                            ErrorCode = validatedXMLRS.DocumentElement.SelectSingleNode("//a1:Error", namespaceMgrValidatedXMLRS).Attributes["Code"].Value;
    //                            ErrorMsg = validatedXMLRS.DocumentElement.SelectSingleNode("//a1:Error", namespaceMgrValidatedXMLRS).Attributes["ShortText"].Value;
    //                        }
    //                    }
    //                }

    //                DatabaseEntities db = new DatabaseEntities();
    //                string CountryOfResidence = originalXML.SelectSingleNode("//a1:Source", namespaceMgrOriginalXML).Attributes["ISOCountry"].Value;
    //                string PickupLocation = originalXML.SelectSingleNode("//a1:PickUpLocation", namespaceMgrOriginalXML).Attributes["LocationCode"].Value;
    //                string DropoffLocation = originalXML.SelectSingleNode("//a1:ReturnLocation", namespaceMgrOriginalXML).Attributes["LocationCode"].Value;
    //                DateTime PickUpDateTime = Convert.ToDateTime(originalXML.SelectSingleNode("//a1:VehRentalCore", namespaceMgrOriginalXML).Attributes["PickUpDateTime"].Value);
    //                DateTime ReturnDateTime = Convert.ToDateTime(originalXML.SelectSingleNode("//a1:VehRentalCore", namespaceMgrOriginalXML).Attributes["ReturnDateTime"].Value);
    //                string brand = xmlProvider.ToString();

    //                XMLErrorLog errorLog = new XMLErrorLog
    //                {
    //                    CountryOfResidence = CountryOfResidence,
    //                    Brand = brand,
    //                    XML_Provider = brand,
    //                    XML_ErrorCode = ErrorCode,
    //                    XML_ErrorMsg = ErrorMsg,
    //                    PickUpLocation = PickupLocation,
    //                    DropOffLocation = DropoffLocation,
    //                    PickUpDate = PickUpDateTime,
    //                    DropOffDate = ReturnDateTime,
    //                    ErrorDate = todaysDate,
    //                    CameFrom = requestedDomain//"www.ryanair.com"
    //                };
    //                db.XMLErrorLogs.AddObject(errorLog);
    //                db.SaveChanges();

    //                XMLErrorLogSent sentXMLErrorLog = new XMLErrorLogSent
    //                {
    //                    XMLErrorLogIDFK = errorLog.XMLErrorLogID,
    //                    XMLSent = originalXMLRequest
    //                };
    //                db.XMLErrorLogSents.AddObject(sentXMLErrorLog);

    //                XMLErrorLogReceived recievedXMLErrorLog = new XMLErrorLogReceived
    //                {
    //                    XMLErrorLogIDFK = errorLog.XMLErrorLogID,
    //                    XMLReceived = originalXMLReceived
    //                };
    //                db.XMLErrorLogReceiveds.AddObject(recievedXMLErrorLog);
    //                db.SaveChanges();
    //            }
    //        }
    //    }
    //    catch (Exception e)
    //    {
    //        CommonTasks.SendXmlForDebugging(e.StackTrace, "Error Logging Failed...");
    //        hasErrors = true;
    //    }

    //    return hasErrors;
    //}

    //public static bool CheckErrorsAndLogErrorsInDatabaseModifyRS(string originalXMLRequest, string validatedXMLRequest, string originalXMLReceived, string validatedXMLReceived, CarProvider xmlProvider, string requestedDomain)
    //{
    //    bool hasErrors = false;
    //    try
    //    {
    //        if ((!String.IsNullOrEmpty(originalXMLRequest)))
    //        {
    //            // remove any credit card information in response
    //            originalXMLRequest = RemoveCreditCardDetails(originalXMLRequest, xmlProvider);

    //            DateTime todaysDate = DateTime.Now;
    //            string todaysDateString = todaysDate.ToString("yyyy/MM/dd HH:mm:ss", CultureInfo.InvariantCulture);

    //            var originalXML = new XmlDocument();
    //            originalXML.LoadXml(originalXMLRequest);
    //            var namespaceMgrOriginalXML = new XmlNamespaceManager(originalXML.NameTable);
    //            namespaceMgrOriginalXML.AddNamespace("a1", "http://www.opentravel.org/OTA/2003/05");

    //            var validatedXMLRQ = new XmlDocument();
    //            validatedXMLRQ.LoadXml(validatedXMLRequest);
    //            var namespaceMgrValidatedXMLRQ = new XmlNamespaceManager(validatedXMLRQ.NameTable);
    //            namespaceMgrValidatedXMLRQ.AddNamespace("a1", "http://www.opentravel.org/OTA/2003/05");

    //            var originalXMLRS = new XmlDocument();
    //            originalXMLRS.LoadXml(originalXMLReceived);
    //            var namespaceMgrOriginalXMLRS = new XmlNamespaceManager(originalXMLRS.NameTable);
    //            namespaceMgrOriginalXMLRS.AddNamespace("a1", "http://www.opentravel.org/OTA/2003/05");

    //            var validatedXMLRS = new XmlDocument();
    //            validatedXMLRS.LoadXml(validatedXMLReceived);
    //            var namespaceMgrValidatedXMLRS = new XmlNamespaceManager(validatedXMLRS.NameTable);
    //            namespaceMgrValidatedXMLRS.AddNamespace("a1", "http://www.opentravel.org/OTA/2003/05");

    //            XmlNodeList errorListRQ = validatedXMLRQ.DocumentElement.SelectNodes(".//a1:Error", namespaceMgrValidatedXMLRQ);
    //            XmlNodeList errorListORI = originalXMLRS.DocumentElement.SelectNodes(".//a1:Error", namespaceMgrOriginalXMLRS);
    //            XmlNodeList errorListRS = validatedXMLRS.DocumentElement.SelectNodes(".//a1:Error", namespaceMgrValidatedXMLRS);

    //            if (errorListRQ.Count > 0 || errorListORI.Count > 0 || errorListRS.Count > 0)
    //            {
    //                hasErrors = true;
    //                string ErrorCode = "";
    //                string ErrorMsg = "";
    //                if (validatedXMLRQ.DocumentElement.SelectSingleNode("//a1:Errors", namespaceMgrValidatedXMLRQ) != null && validatedXMLRQ.DocumentElement.SelectSingleNode("//a1:Errors", namespaceMgrValidatedXMLRQ).Attributes != null)
    //                {
    //                    ErrorCode = validatedXMLRQ.DocumentElement.SelectSingleNode("//a1:Error", namespaceMgrValidatedXMLRQ).Attributes["Code"].Value;
    //                    ErrorMsg = validatedXMLRQ.DocumentElement.SelectSingleNode("//a1:Error", namespaceMgrValidatedXMLRQ).Attributes["ShortText"].Value;
    //                }
    //                else if (originalXMLRS.DocumentElement.SelectSingleNode("//a1:Errors", namespaceMgrOriginalXMLRS) != null && originalXMLRS.DocumentElement.SelectSingleNode("//a1:Errors", namespaceMgrOriginalXMLRS).Attributes != null)
    //                {
    //                    ErrorCode = originalXMLRS.DocumentElement.SelectSingleNode("//a1:Error", namespaceMgrOriginalXMLRS).Attributes["Code"].Value;
    //                    ErrorMsg = originalXMLRS.DocumentElement.SelectSingleNode("//a1:Error", namespaceMgrOriginalXMLRS).Attributes["ShortText"].Value;
    //                }
    //                else if (validatedXMLRS.DocumentElement.SelectSingleNode("//a1:Errors", namespaceMgrValidatedXMLRS) != null && validatedXMLRS.DocumentElement.SelectSingleNode("//a1:Errors", namespaceMgrValidatedXMLRS).Attributes != null)
    //                {
    //                    if (validatedXMLRS.DocumentElement.SelectSingleNode("//a1:Error", namespaceMgrValidatedXMLRS) != null)
    //                    {
    //                        if (validatedXMLRS.DocumentElement.SelectSingleNode("//a1:Error", namespaceMgrValidatedXMLRS).Attributes != null)
    //                        {
    //                            ErrorCode = validatedXMLRS.DocumentElement.SelectSingleNode("//a1:Error", namespaceMgrValidatedXMLRS).Attributes["Code"].Value;
    //                            ErrorMsg = validatedXMLRS.DocumentElement.SelectSingleNode("//a1:Error", namespaceMgrValidatedXMLRS).Attributes["ShortText"].Value;
    //                        }
    //                    }
    //                }

    //                DatabaseEntities db = new DatabaseEntities();
    //                //string CountryOfResidence = originalXML.SelectSingleNode("//a1:Source", namespaceMgrOriginalXML).Attributes["ISOCountry"].Value;
    //               // string PickupLocation = originalXML.SelectSingleNode("//a1:PickUpLocation", namespaceMgrOriginalXML).Attributes["LocationCode"].Value;
    //               // string DropoffLocation = originalXML.SelectSingleNode("//a1:ReturnLocation", namespaceMgrOriginalXML).Attributes["LocationCode"].Value;
    //               // DateTime PickUpDateTime = Convert.ToDateTime(originalXML.SelectSingleNode("//a1:VehRentalCore", namespaceMgrOriginalXML).Attributes["PickUpDateTime"].Value);
    //               // DateTime ReturnDateTime = Convert.ToDateTime(originalXML.SelectSingleNode("//a1:VehRentalCore", namespaceMgrOriginalXML).Attributes["ReturnDateTime"].Value);
    //                string brand = xmlProvider.ToString();

    //                XMLErrorLog errorLog = new XMLErrorLog
    //                {
    //                  //  CountryOfResidence = CountryOfResidence,
    //                    Brand = brand,
    //                    XML_Provider = brand,
    //                    XML_ErrorCode = ErrorCode,
    //                    XML_ErrorMsg = ErrorMsg,
    //                   // PickUpLocation = PickupLocation,
    //                  //  DropOffLocation = DropoffLocation,
    //                   // PickUpDate = PickUpDateTime,
    //                  //  DropOffDate = ReturnDateTime,
    //                    ErrorDate = todaysDate,
    //                    CameFrom = requestedDomain//"www.ryanair.com"
    //                };
    //                db.XMLErrorLogs.AddObject(errorLog);
    //                db.SaveChanges();

    //                XMLErrorLogSent sentXMLErrorLog = new XMLErrorLogSent
    //                {
    //                    XMLErrorLogIDFK = errorLog.XMLErrorLogID,
    //                    XMLSent = originalXMLRequest
    //                };
    //                db.XMLErrorLogSents.AddObject(sentXMLErrorLog);

    //                XMLErrorLogReceived recievedXMLErrorLog = new XMLErrorLogReceived
    //                {
    //                    XMLErrorLogIDFK = errorLog.XMLErrorLogID,
    //                    XMLReceived = originalXMLReceived
    //                };
    //                db.XMLErrorLogReceiveds.AddObject(recievedXMLErrorLog);
    //                db.SaveChanges();
    //            }
    //        }
    //    }
    //    catch (Exception e)
    //    {
    //        CommonTasks.SendXmlForDebugging(e.StackTrace.ToString(), "Error in XML Error Logging DB Failed");
    //        hasErrors = true;
    //    }

    //    return hasErrors;
    //}

    //public static string GetRequestDomain(string ryanairXML)
    //{
    //    string partnerName = null;

    //    string XmlNamespace = "http://www.opentravel.org/OTA/2003/05";
    //    XmlDocument receivedXMLDoc = null;
    //    var requestXML = new XmlDocument();
    //    requestXML.LoadXml(ryanairXML);

    //    receivedXMLDoc = requestXML;

    //    var namespaceMgr = new XmlNamespaceManager(requestXML.NameTable);
    //    namespaceMgr.AddNamespace("a1", XmlNamespace);

    //    XmlAttribute AgentDutyCodeAttr = requestXML.DocumentElement.SelectSingleNode("//a1:Source", namespaceMgr).Attributes["AgentDutyCode"];

    //    string agentCode = AgentDutyCodeAttr.Value.ToString();

    //    DatabaseEntities db = new DatabaseEntities();
    //    var partneragentCode = (from p in db.Partners
    //                            where p.AgentDutyCode == agentCode
    //                            select new
    //                            {
    //                                p.DomainName
    //                            });
    //    if (partneragentCode.FirstOrDefault() != null)
    //    {
    //        partnerName = partneragentCode.FirstOrDefault().DomainName;
    //    }

    //    return partnerName;

    //}

    //public static bool CheckErrorsAndLogErrorsInDatabaseofCancleRS(string originalXMLRequest, string validatedXMLRequest, string originalXMLReceived, string validatedXMLReceived, CarProvider xmlProvider, string requestedDomain)
    //{
    //    bool hasErrors = false;
    //    try
    //    {
    //        if ((!String.IsNullOrEmpty(originalXMLRequest)))
    //        {
    //            // remove any credit card information in response
    //            originalXMLRequest = RemoveCreditCardDetails(originalXMLRequest, xmlProvider);

    //            DateTime todaysDate = DateTime.Now;
    //            string todaysDateString = todaysDate.ToString("yyyy/MM/dd HH:mm:ss", CultureInfo.InvariantCulture);

    //            var originalXML = new XmlDocument();
    //            originalXML.LoadXml(originalXMLRequest);
    //            var namespaceMgrOriginalXML = new XmlNamespaceManager(originalXML.NameTable);
    //            namespaceMgrOriginalXML.AddNamespace("a1", "http://www.opentravel.org/OTA/2003/05");

    //            var validatedXMLRQ = new XmlDocument();
    //            validatedXMLRQ.LoadXml(validatedXMLRequest);
    //            var namespaceMgrValidatedXMLRQ = new XmlNamespaceManager(validatedXMLRQ.NameTable);
    //            namespaceMgrValidatedXMLRQ.AddNamespace("a1", "http://www.opentravel.org/OTA/2003/05");

    //            var originalXMLRS = new XmlDocument();
    //            originalXMLRS.LoadXml(originalXMLReceived);
    //            var namespaceMgrOriginalXMLRS = new XmlNamespaceManager(originalXMLRS.NameTable);
    //            namespaceMgrOriginalXMLRS.AddNamespace("a1", "http://www.opentravel.org/OTA/2003/05");

    //            var validatedXMLRS = new XmlDocument();
    //            validatedXMLRS.LoadXml(validatedXMLReceived);
    //            var namespaceMgrValidatedXMLRS = new XmlNamespaceManager(validatedXMLRS.NameTable);
    //            namespaceMgrValidatedXMLRS.AddNamespace("a1", "http://www.opentravel.org/OTA/2003/05");

    //            XmlNodeList errorListRQ = validatedXMLRQ.DocumentElement.SelectNodes(".//a1:Error", namespaceMgrValidatedXMLRQ);
    //            XmlNodeList errorListORI = originalXMLRS.DocumentElement.SelectNodes(".//a1:Error", namespaceMgrOriginalXMLRS);
    //            XmlNodeList errorListRS = validatedXMLRS.DocumentElement.SelectNodes(".//a1:Error", namespaceMgrValidatedXMLRS);

    //            if (errorListRQ.Count > 0 || errorListORI.Count > 0 || errorListRS.Count > 0)
    //            {
    //                hasErrors = true;
    //                string ErrorCode = "";
    //                string ErrorMsg = "";
    //                if (validatedXMLRQ.DocumentElement.SelectSingleNode("//a1:Errors", namespaceMgrValidatedXMLRQ) != null && validatedXMLRQ.DocumentElement.SelectSingleNode("//a1:Errors", namespaceMgrValidatedXMLRQ).Attributes != null)
    //                {
    //                    ErrorCode = validatedXMLRQ.DocumentElement.SelectSingleNode("//a1:Error", namespaceMgrValidatedXMLRQ).Attributes["Code"].Value;
    //                    ErrorMsg = validatedXMLRQ.DocumentElement.SelectSingleNode("//a1:Error", namespaceMgrValidatedXMLRQ).Attributes["ShortText"].Value;
    //                }
    //                else if (originalXMLRS.DocumentElement.SelectSingleNode("//a1:Errors", namespaceMgrOriginalXMLRS) != null && originalXMLRS.DocumentElement.SelectSingleNode("//a1:Errors", namespaceMgrOriginalXMLRS).Attributes != null)
    //                {
    //                    ErrorCode = originalXMLRS.DocumentElement.SelectSingleNode("//a1:Error", namespaceMgrOriginalXMLRS).Attributes["Code"].Value;
    //                    ErrorMsg = originalXMLRS.DocumentElement.SelectSingleNode("//a1:Error", namespaceMgrOriginalXMLRS).Attributes["ShortText"].Value;
    //                }
    //                else if (validatedXMLRS.DocumentElement.SelectSingleNode("//a1:Errors", namespaceMgrValidatedXMLRS) != null && validatedXMLRS.DocumentElement.SelectSingleNode("//a1:Errors", namespaceMgrValidatedXMLRS).Attributes != null)
    //                {
    //                    if (validatedXMLRS.DocumentElement.SelectSingleNode("//a1:Error", namespaceMgrValidatedXMLRS) != null)
    //                    {
    //                        if (validatedXMLRS.DocumentElement.SelectSingleNode("//a1:Error", namespaceMgrValidatedXMLRS).Attributes != null)
    //                        {
    //                            ErrorCode = validatedXMLRS.DocumentElement.SelectSingleNode("//a1:Error", namespaceMgrValidatedXMLRS).Attributes["Code"].Value;
    //                            ErrorMsg = validatedXMLRS.DocumentElement.SelectSingleNode("//a1:Error", namespaceMgrValidatedXMLRS).Attributes["ShortText"].Value;
    //                        }
    //                    }
    //                }

    //                DatabaseEntities db = new DatabaseEntities();
    //                string brand = xmlProvider.ToString();

    //                XMLErrorLog errorLog = new XMLErrorLog
    //                {
    //                    Brand = brand,
    //                    XML_Provider = brand,
    //                    XML_ErrorCode = ErrorCode,
    //                    XML_ErrorMsg = ErrorMsg,
    //                    ErrorDate = todaysDate,
    //                    CameFrom = requestedDomain//"www.ryanair.com"
    //                };
    //                db.XMLErrorLogs.AddObject(errorLog);
    //                db.SaveChanges();

    //                XMLErrorLogSent sentXMLErrorLog = new XMLErrorLogSent
    //                {
    //                    XMLErrorLogIDFK = errorLog.XMLErrorLogID,
    //                    XMLSent = originalXMLRequest
    //                };
    //                db.XMLErrorLogSents.AddObject(sentXMLErrorLog);

    //                XMLErrorLogReceived recievedXMLErrorLog = new XMLErrorLogReceived
    //                {
    //                    XMLErrorLogIDFK = errorLog.XMLErrorLogID,
    //                    XMLReceived = originalXMLReceived
    //                };
    //                db.XMLErrorLogReceiveds.AddObject(recievedXMLErrorLog);
    //                db.SaveChanges();
    //            }
    //        }
    //    }
    //    catch (Exception e)
    //    {
    //        CommonTasks.SendXmlForDebugging(e.StackTrace.ToString(), "Error in XML Error Logging DB Failed");
    //        hasErrors = true;
    //    }

    //    return hasErrors;
    //}

    //public static bool CheckErrorsAndLogErrorsInDatabaseofVehiRet(string originalXMLRequest, string validatedXMLRequest, string originalXMLReceived, string validatedXMLReceived, CarProvider xmlProvider, string requestedDomain)
    //{
    //    bool hasErrors = false;
    //    try
    //    {
    //        if ((!String.IsNullOrEmpty(originalXMLRequest)))
    //        {
    //            // remove any credit card information in response
    //            originalXMLRequest = RemoveCreditCardDetails(originalXMLRequest, xmlProvider);

    //            DateTime todaysDate = DateTime.Now;
    //            string todaysDateString = todaysDate.ToString("yyyy/MM/dd HH:mm:ss", CultureInfo.InvariantCulture);

    //            var originalXML = new XmlDocument();
    //            originalXML.LoadXml(originalXMLRequest);
    //            var namespaceMgrOriginalXML = new XmlNamespaceManager(originalXML.NameTable);
    //            namespaceMgrOriginalXML.AddNamespace("a1", "http://www.opentravel.org/OTA/2003/05");

    //            var validatedXMLRQ = new XmlDocument();
    //            validatedXMLRQ.LoadXml(validatedXMLRequest);
    //            var namespaceMgrValidatedXMLRQ = new XmlNamespaceManager(validatedXMLRQ.NameTable);
    //            namespaceMgrValidatedXMLRQ.AddNamespace("a1", "http://www.opentravel.org/OTA/2003/05");

    //            var originalXMLRS = new XmlDocument();
    //            originalXMLRS.LoadXml(originalXMLReceived);
    //            var namespaceMgrOriginalXMLRS = new XmlNamespaceManager(originalXMLRS.NameTable);
    //            namespaceMgrOriginalXMLRS.AddNamespace("a1", "http://www.opentravel.org/OTA/2003/05");

    //            var validatedXMLRS = new XmlDocument();
    //            validatedXMLRS.LoadXml(validatedXMLReceived);
    //            var namespaceMgrValidatedXMLRS = new XmlNamespaceManager(validatedXMLRS.NameTable);
    //            namespaceMgrValidatedXMLRS.AddNamespace("a1", "http://www.opentravel.org/OTA/2003/05");

    //            XmlNodeList errorListRQ = validatedXMLRQ.DocumentElement.SelectNodes(".//a1:Error", namespaceMgrValidatedXMLRQ);
    //            XmlNodeList errorListORI = originalXMLRS.DocumentElement.SelectNodes(".//a1:Error", namespaceMgrOriginalXMLRS);
    //            XmlNodeList errorListRS = validatedXMLRS.DocumentElement.SelectNodes(".//a1:Error", namespaceMgrValidatedXMLRS);

    //            if (errorListRQ.Count > 0 || errorListORI.Count > 0 || errorListRS.Count > 0)
    //            {
    //                hasErrors = true;
    //                string ErrorCode = "";
    //                string ErrorMsg = "";
    //                if (validatedXMLRQ.DocumentElement.SelectSingleNode("//a1:Errors", namespaceMgrValidatedXMLRQ) != null && validatedXMLRQ.DocumentElement.SelectSingleNode("//a1:Errors", namespaceMgrValidatedXMLRQ).Attributes != null)
    //                {
    //                    ErrorCode = validatedXMLRQ.DocumentElement.SelectSingleNode("//a1:Error", namespaceMgrValidatedXMLRQ).Attributes["Code"].Value;
    //                    ErrorMsg = validatedXMLRQ.DocumentElement.SelectSingleNode("//a1:Error", namespaceMgrValidatedXMLRQ).Attributes["ShortText"].Value;
    //                }
    //                else if (originalXMLRS.DocumentElement.SelectSingleNode("//a1:Errors", namespaceMgrOriginalXMLRS) != null && originalXMLRS.DocumentElement.SelectSingleNode("//a1:Errors", namespaceMgrOriginalXMLRS).Attributes != null)
    //                {
    //                    ErrorCode = originalXMLRS.DocumentElement.SelectSingleNode("//a1:Error", namespaceMgrOriginalXMLRS).Attributes["Code"].Value;
    //                    ErrorMsg = originalXMLRS.DocumentElement.SelectSingleNode("//a1:Error", namespaceMgrOriginalXMLRS).Attributes["ShortText"].Value;
    //                }
    //                else if (validatedXMLRS.DocumentElement.SelectSingleNode("//a1:Errors", namespaceMgrValidatedXMLRS) != null && validatedXMLRS.DocumentElement.SelectSingleNode("//a1:Errors", namespaceMgrValidatedXMLRS).Attributes != null)
    //                {
    //                    if (validatedXMLRS.DocumentElement.SelectSingleNode("//a1:Error", namespaceMgrValidatedXMLRS) != null)
    //                    {
    //                        if (validatedXMLRS.DocumentElement.SelectSingleNode("//a1:Error", namespaceMgrValidatedXMLRS).Attributes != null)
    //                        {
    //                            ErrorCode = validatedXMLRS.DocumentElement.SelectSingleNode("//a1:Error", namespaceMgrValidatedXMLRS).Attributes["Code"].Value;
    //                            ErrorMsg = validatedXMLRS.DocumentElement.SelectSingleNode("//a1:Error", namespaceMgrValidatedXMLRS).Attributes["ShortText"].Value;
    //                        }
    //                    }
    //                }

    //                DatabaseEntities db = new DatabaseEntities();
    //                string brand = xmlProvider.ToString();

    //                XMLErrorLog errorLog = new XMLErrorLog
    //                {
    //                    Brand = brand,
    //                    XML_Provider = brand,
    //                    XML_ErrorCode = ErrorCode,
    //                    XML_ErrorMsg = ErrorMsg,
    //                    ErrorDate = todaysDate,
    //                    CameFrom = requestedDomain
    //                };
    //                db.XMLErrorLogs.AddObject(errorLog);
    //                db.SaveChanges();

    //                XMLErrorLogSent sentXMLErrorLog = new XMLErrorLogSent
    //                {
    //                    XMLErrorLogIDFK = errorLog.XMLErrorLogID,
    //                    XMLSent = originalXMLRequest
    //                };
    //                db.XMLErrorLogSents.AddObject(sentXMLErrorLog);

    //                XMLErrorLogReceived recievedXMLErrorLog = new XMLErrorLogReceived
    //                {
    //                    XMLErrorLogIDFK = errorLog.XMLErrorLogID,
    //                    XMLReceived = originalXMLReceived
    //                };
    //                db.XMLErrorLogReceiveds.AddObject(recievedXMLErrorLog);
    //                db.SaveChanges();
    //            }
    //        }
    //    }
    //    catch (Exception e)
    //    {
    //        CommonTasks.SendXmlForDebugging(e.StackTrace.ToString(), "Error in XML Error Logging DB Failed");
    //        hasErrors = true;
    //    }

    //    return hasErrors;
    //}

    //Get Dummy long location code for firefly brand
    //public static string GetDummyLongLocationCode(string shortLocCode)
    //{
    //    DatabaseEntities db = new DatabaseEntities();
    //    var locEntry = db.Locations.SingleOrDefault(l => l.LocationCode == shortLocCode);
    //    if (locEntry != null)
    //    {
    //        return locEntry.ExtendedLocationCode.Trim();
    //    }
    //    else
    //    {
    //        return shortLocCode.Trim();
    //    }
    //}

    

    //Get Dummy long location code for firefly brand
    //public static string GetShortLocationCode(string dummyLocCode)
    //{
    //    DatabaseEntities db = new DatabaseEntities();
    //    var locEntry = db.Locations.SingleOrDefault(l => l.ExtendedLocationCode == dummyLocCode);
    //    if (locEntry != null)
    //    {
    //        return locEntry.LocationCode.Trim();
    //    }
    //    else
    //    {
    //        return dummyLocCode.Trim();
    //    }
    //}

    //Save booking in Database
    //public static string SaveBookingInDatabaseAndSendEmail(string responseXML, string requestXML, CarProvider carProvider, string requestedDomain, string ryanairXML, RyPreReqDBBooking ryPreReqObj)
    //{
    //    DatabaseEntities db = new DatabaseEntities();
    //    ExtractOrderDetails extractedOrderXml = new ExtractOrderDetails();
    //    Booking _booking = extractedOrderXml.InitializeBooking(responseXML, requestXML, carProvider, ryPreReqObj);

    //    //Add language initialization here.......

    //    _booking = extractedOrderXml.ModifyBokkingLanguage(ryanairXML, _booking);
    //    Thread.CurrentThread.CurrentUICulture = new CultureInfo(_booking.Language);
    //    Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(_booking.Language);

        


    //    var selectedBrand = _booking.Brands.SingleOrDefault(b => b.CarProvider == carProvider);
    //    var car = selectedBrand.Cars.FirstOrDefault();

    //     try
    //    {

    //        if (CheckPartnerMailSend(requestedDomain))
    //        {
                
    //        //Build confirmation email with current booking object
    //        BuildConfirmationEmail confEmailBuilder = new BuildConfirmationEmail(_booking, car, selectedBrand);

    //        string emailMessage = confEmailBuilder.BuildEmail(_booking.ConfirmationId, car.CurrencySymbol, car.Price.ToString(), car.Image, car.LocalizedName, _booking.CrossBorderDropFee, car.PriceIncludes, car.DistanceIncluded, TimeBetweenDates.CalculateMinutes(_booking.PickupDateTime, Convert.ToDateTime(_booking.DropOffDateTime)));
    //        string emailSubject = _booking.ModifyProcess ? (string)HttpContext.GetGlobalResourceObject("SiteStrings", "ModifyEmailSubject") : (string)HttpContext.GetGlobalResourceObject("SiteStrings", "BookingEmailSubject");
    //        //Send confirmation email

    //        bool isSent = SendEmail.SendMessage(WebsiteSettings.SiteEmailsSentFrom, _booking.Customer.Email, emailSubject, emailMessage, true);
            
    //        if (!isSent)
    //        {
    //            return CommonTasks.GetErrorXML("VehResRS", "1", "099", "Booking Completed Successfully. Error Occured while sending confirmation email. Please Contact Hertz.");
    //        }

    //        }

           

    //            //DatabaseEntities db = new DatabaseEntities();

    //            //create the 'email code' we will use this to track instead of sending customers email for privacy reasons
    //            DateTime todaysDate = DateTime.Now;
    //            string todaysDateString = todaysDate.ToString("yyyy/MM/dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);

    //            DateTime orderDate = DateTime.Now;

    //            var randNum = new Random();
    //            int myRandomNumber = randNum.Next(1, 99999);
    //            string todaysNumber = todaysDate.Day.ToString();
    //            string monthNumber = todaysDate.Month.ToString();
    //            string yearNumber = todaysDate.ToString("yy", System.Globalization.CultureInfo.InvariantCulture);
    //            string hourNumber = todaysDate.Hour.ToString();
    //            string minNumber = todaysDate.Minute.ToString();
    //            string secNumber = todaysDate.Second.ToString();


    //            var ts = _booking.DropOffDateTime - _booking.PickupDateTime;
    //            int noDaysHired = ts.Days;
    //            TimeSpan duration = new TimeSpan(2, 0, 0, 0);


    //            short includeInEshot = 1;
    //            string cameFrom = requestedDomain;//"www.ryanairdp.com";


    //            DateTime reminderEmail = _booking.PickupDateTime.Subtract(duration);
    //            DateTime thanksEmail = _booking.DropOffDateTime.Add(duration);
    //            string hertzNum1 = "";
    //            string xmlProvider = carProvider.ToString();
    //            string emailCode = todaysNumber + monthNumber + yearNumber + hourNumber + minNumber + secNumber + myRandomNumber;
    //            short emailConfirmed = 1;

    //            double amountPaid = Convert.ToDouble(car.Price);

    //            string pickUpSupplier = CarProvider.Hertz.ToString();
    //            string dropOffSupplier = CarProvider.Hertz.ToString();

    //            Order order = new Order
    //            {
    //                ConfirmationID = _booking.ConfirmationId,
    //                CustomerName = _booking.Customer.Firstname + " " + _booking.Customer.Surname,
    //                CustomerEmail = _booking.Customer.Email,
    //                EmailCode = emailCode,
    //                EmailConfirmed = emailConfirmed,
    //                CustomerPhone = _booking.Customer.PhoneNumber,
    //                CarBooked = car.Name,
    //                VehicleType = car.VehClassSizeValue,
    //                CountryOfResidence = _booking.CountryOfResidenceCode,
    //                PickUpDate = _booking.PickupDateTime,
    //                DropOffDate = _booking.DropOffDateTime,
    //                OrderDate = orderDate,
    //                PickUpLocation = _booking.PickUpLocationCode,
    //                DropOffLocation = _booking.DropOffLocationCode,
    //                NoDaysHired = noDaysHired,
    //                AmountPaid = amountPaid,
    //                AmountPaidCurrency = car.CurrencySymbol,
    //                IncludeInEshot = includeInEshot,
    //                CameFrom = cameFrom,
    //                LanguageCode = _booking.Language,
    //                ReminderEmail = reminderEmail,
    //                ThanksEmail = thanksEmail,
    //                HertzNum1 = hertzNum1,
    //                XML_Provider = xmlProvider,
    //                FlightNumber = _booking.FlightNum,
    //                XML_IATANumber = _booking.IataNumber,
    //                XML_RateQualifier = ryPreReqObj.RequestRateQualifire,
    //                IsPrePaid = _booking.PrePaySelected,
    //                CustomerSurname = _booking.Customer.Surname,
    //                CustomerFirstname = _booking.Customer.Firstname,
    //                Modified = false,
    //                CancelledBooking = false,
    //                AddressLine1 = _booking.Customer.Address1,
    //                AddressLine2 = _booking.Customer.Address2,
    //                AddressTown = _booking.Customer.Town,
    //                AddressPostCodeZip = _booking.Customer.Postcode,
    //                AddressCountryCode = _booking.Customer.Country,
    //                AddressCountryWord = _booking.Customer.Country,
    //                XML_Provider_SellingPrice = "",
    //                XML_Provider_CCFee = "",
    //                XML_Provider_WholesaleTotal = "",
    //                XML_Provider_MarginShare = "",
    //                XML_Provider_PickupSupplier = pickUpSupplier,
    //                XML_Provider_DropoffSupplier = dropOffSupplier
    //            };
    //            db.Orders.AddObject(order);
    //            db.SaveChanges();

    //            int orderID = order.OrderID;

    //            foreach (var equipment in _booking.Accessories)
    //            {
    //                string accessory = equipment.EquipType;
    //                double price = Convert.ToDouble(equipment.Price);
    //                string currency = equipment.CurrencySymbol;
    //                short quantity = Convert.ToInt16(equipment.Quantity);
    //                AccessoriesOrdered acc = new AccessoriesOrdered
    //                {
    //                    OrderIDFK = orderID,
    //                    Accessory = accessory,
    //                    Price = price,
    //                    Currency = currency,
    //                    Quantity = quantity
    //                };
    //                db.AccessoriesOrdereds.AddObject(acc);
    //            }
    //            db.SaveChanges();

    //            return "";

         

    //    }
    //    catch (Exception e)
    //    {
    //        return CommonTasks.GetErrorXML("VehResRS", "1", "099", "Booking Completed Successfully. Error Occured while sending confirmation email or save booking in system. Please Contact Hertz. " + e.Message);
    //    }

    //}

    //public static string SaveBookingInDatabaseAndSendEmailInD_Tag(string responseXML, string requestXML, CarProvider carProvider, string requestedDomain, string ryanairXML, RyPreReqDBBooking ryPreReqObj)
    //{
    //    DatabaseEntities db = new DatabaseEntities();
    //    ExtractOrderDetails extractedOrderXml = new ExtractOrderDetails();
    //    Booking _booking = extractedOrderXml.InitializeBookingD_Tag(responseXML, requestXML, carProvider, ryPreReqObj);
    //    //Add language initialization here.......

    //    _booking = extractedOrderXml.ModifyBokkingLanguage(ryanairXML, _booking);
    //    Thread.CurrentThread.CurrentUICulture = new CultureInfo(_booking.Language);
    //    Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(_booking.Language);


    //    var selectedBrand = _booking.Brands.SingleOrDefault(b => b.CarProvider == carProvider);
    //    var car = selectedBrand.Cars.FirstOrDefault();

    //    try
    //    {
    //        //if (CheckPartnerMailSend(requestedDomain))
    //        //{
    //        //    //Build confirmation email with current booking object
    //        //    BuildConfirmationEmail confEmailBuilder = new BuildConfirmationEmail(_booking, car, selectedBrand);
    //        //    //CommonTasks.SendXmlForDebugging(confEmailBuilder.ToString(), "Test1234");
    //        //    string emailMessage = confEmailBuilder.BuildEmail(_booking.ConfirmationId, car.CurrencySymbol, car.Price.ToString(), car.Image, car.LocalizedName, _booking.CrossBorderDropFee, car.PriceIncludes, car.DistanceIncluded, TimeBetweenDates.CalculateMinutes(_booking.PickupDateTime, Convert.ToDateTime(_booking.DropOffDateTime)));
    //        //    //CommonTasks.SendXmlForDebugging(requestedDomain.ToString(), "Test");
    //        //    string emailSubject = _booking.ModifyProcess ? (string)HttpContext.GetGlobalResourceObject("SiteStrings", "ModifyEmailSubject") : (string)HttpContext.GetGlobalResourceObject("SiteStrings", "BookingEmailSubject");
    //        //    //Send confirmation email
    //        //    //CommonTasks.SendXmlForDebugging(emailSubject.ToString() + "|||" + emailMessage.ToString(), "Test1234");
    //        //    bool isSent = SendEmail.SendMessage(WebsiteSettings.SiteEmailsSentFrom, _booking.Customer.Email, emailSubject, emailMessage, true);

    //        //    if (!isSent)
    //        //    {
    //        //        return CommonTasks.GetErrorXML("VehResRS", "1", "099", "Booking Completed Successfully. Error Occured while sending confirmation email. Please Contact Hertz.");
    //        //    }

    //        //}

    //        //CommonTasks.SendXmlForDebugging(requestedDomain.ToString(), "Test");

    //        //DatabaseEntities db = new DatabaseEntities();

    //        //create the 'email code' we will use this to track instead of sending customers email for privacy reasons
    //        DateTime todaysDate = DateTime.Now;
    //        string todaysDateString = todaysDate.ToString("yyyy/MM/dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);

    //        DateTime orderDate = DateTime.Now;

    //        var randNum = new Random();
    //        int myRandomNumber = randNum.Next(1, 99999);
    //        string todaysNumber = todaysDate.Day.ToString();
    //        string monthNumber = todaysDate.Month.ToString();
    //        string yearNumber = todaysDate.ToString("yy", System.Globalization.CultureInfo.InvariantCulture);
    //        string hourNumber = todaysDate.Hour.ToString();
    //        string minNumber = todaysDate.Minute.ToString();
    //        string secNumber = todaysDate.Second.ToString();

    //        //CommonTasks.SendXmlForDebugging(requestedDomain.ToString(), "Test");
    //        var ts = _booking.DropOffDateTime - _booking.PickupDateTime;
    //        int noDaysHired = ts.Days;
    //        TimeSpan duration = new TimeSpan(2, 0, 0, 0);


    //        short includeInEshot = 1;
    //        string cameFrom = requestedDomain;//"www.ryanairdp.com";


    //        DateTime reminderEmail = _booking.PickupDateTime.Subtract(duration);
    //        DateTime thanksEmail = _booking.DropOffDateTime.Add(duration);
    //        string hertzNum1 = "";
    //        string xmlProvider = carProvider.ToString();
    //        string emailCode = todaysNumber + monthNumber + yearNumber + hourNumber + minNumber + secNumber + myRandomNumber;
    //        short emailConfirmed = 1;

    //        double amountPaid = Convert.ToDouble(car.Price);

    //        string pickUpSupplier = CarProvider.Thrifty.ToString();
    //        string dropOffSupplier = CarProvider.Thrifty.ToString();

    //        //CommonTasks.SendXmlForDebugging(xmlProvider + "||" + pickUpSupplier.ToString() + "||" + orderDate.ToString(), "Test");
    //        //CommonTasks.SendXmlForDebugging("Test Ok", "Test123");
    //        //Order order = new Order
    //        //{
    //        //    ConfirmationID = _booking.ConfirmationId,
    //        //    CustomerName = _booking.Customer.Firstname + " " + _booking.Customer.Surname,
    //        //    CustomerEmail = _booking.Customer.Email,
    //        //    EmailCode = emailCode,
    //        //    EmailConfirmed = emailConfirmed,
    //        //    CustomerPhone = _booking.Customer.PhoneNumber,
    //        //    CarBooked = car.Name,
    //        //    VehicleType = car.VehClassSizeValue,
    //        //    CountryOfResidence = _booking.CountryOfResidenceCode,
    //        //    PickUpDate = _booking.PickupDateTime,
    //        //    DropOffDate = _booking.DropOffDateTime,
    //        //    OrderDate = orderDate,
    //        //    PickUpLocation = _booking.PickUpLocationCode,
    //        //    DropOffLocation = _booking.DropOffLocationCode,
    //        //    NoDaysHired = noDaysHired,
    //        //    AmountPaid = amountPaid,
    //        //    AmountPaidCurrency = car.CurrencySymbol,
    //        //    IncludeInEshot = includeInEshot,
    //        //    CameFrom = cameFrom,
    //        //    LanguageCode = _booking.Language,
    //        //    ReminderEmail = reminderEmail,
    //        //    ThanksEmail = thanksEmail,
    //        //    HertzNum1 = hertzNum1,
    //        //    XML_Provider = xmlProvider,
    //        //    FlightNumber = _booking.FlightNum,
    //        //    XML_IATANumber = _booking.IataNumber,
    //        //    XML_RateQualifier = ryPreReqObj.RequestRateQualifire,
    //        //    IsPrePaid = _booking.PrePaySelected,
    //        //    CustomerSurname = _booking.Customer.Surname,
    //        //    CustomerFirstname = _booking.Customer.Firstname,
    //        //    Modified = false,
    //        //    CancelledBooking = false,
    //        //    AddressLine1 = _booking.Customer.Address1,
    //        //    AddressLine2 = _booking.Customer.Address2,
    //        //    AddressTown = _booking.Customer.Town,
    //        //    AddressPostCodeZip = _booking.Customer.Postcode,
    //        //    AddressCountryCode = _booking.Customer.Country,
    //        //    AddressCountryWord = _booking.Customer.Country,
    //        //    XML_Provider_SellingPrice = "",
    //        //    XML_Provider_CCFee = "",
    //        //    XML_Provider_WholesaleTotal = "",
    //        //    XML_Provider_MarginShare = "",
    //        //    XML_Provider_PickupSupplier = pickUpSupplier,
    //        //    XML_Provider_DropoffSupplier = dropOffSupplier
    //        //};
    //        //db.Orders.AddObject(order);
    //        //db.SaveChanges();

    //        //int orderID = order.OrderID;

    //        //foreach (var equipment in _booking.Accessories)
    //        //{
    //        //    string accessory = equipment.EquipType;
    //        //    double price = Convert.ToDouble(equipment.Price);
    //        //    string currency = equipment.CurrencySymbol;
    //        //    short quantity = Convert.ToInt16(equipment.Quantity);
    //        //    AccessoriesOrdered acc = new AccessoriesOrdered
    //        //    {
    //        //        OrderIDFK = orderID,
    //        //        Accessory = accessory,
    //        //        Price = price,
    //        //        Currency = currency,
    //        //        Quantity = quantity
    //        //    };
    //        //    db.AccessoriesOrdereds.AddObject(acc);
    //        //}
    //        //db.SaveChanges();
    //        //CommonTasks.SendXmlForDebugging(carProvider.ToString(), "Test");
    //        return "";



    //    }
    //    catch (Exception e)
    //    {
    //        return CommonTasks.GetErrorXML("VehResRS", "1", "099", "Booking Completed Successfully. Error Occured while sending confirmation email or save booking in system. Please Contact Hertz. " + e.Message);
    //    }

    //}

    //public static bool CheckPartnerMailSend(string requestedDomain)
    //{
    //    DatabaseEntities db = new DatabaseEntities();
    //    var partnerDtl = db.Partners.SingleOrDefault(p => p.DomainName == requestedDomain && p.ConfimationMailSend == 1);
    //    if (partnerDtl != null)
    //    {
    //        return true;
    //    }
    //    else
    //    {
    //        return false;
    //    }

    //}



    //public static string SaveModifyBookingInDatabaseAndSendEmail(string responseXML, string requestXML, CarProvider carProvider, string requestedDomain, string ryanairXML)
    //{
    //    DatabaseEntities db = new DatabaseEntities();
    //    ExtractOrderDetails extractedOrderXml = new ExtractOrderDetails();
    //    Booking _booking = extractedOrderXml.InitializeBooking(responseXML, requestXML, carProvider);


    //    //Add language initialization here.......

    //    _booking = extractedOrderXml.ModifyBokkingLanguage(ryanairXML, _booking);
    //    Thread.CurrentThread.CurrentUICulture = new CultureInfo(_booking.Language);
    //    Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(_booking.Language);


    //    var selectedBrand = _booking.Brands.SingleOrDefault(b => b.CarProvider == carProvider);
    //    var car = selectedBrand.Cars.FirstOrDefault();

    //    //create the 'email code' we will use this to track instead of sending customers email for privacy reasons
    //    DateTime todaysDate = DateTime.Now;
    //    string todaysDateString = todaysDate.ToString("yyyy/MM/dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);

    //    DateTime orderDate = DateTime.Now;

    //    var randNum = new Random();
    //    int myRandomNumber = randNum.Next(1, 99999);
    //    string todaysNumber = todaysDate.Day.ToString();
    //    string monthNumber = todaysDate.Month.ToString();
    //    string yearNumber = todaysDate.ToString("yy", System.Globalization.CultureInfo.InvariantCulture);
    //    string hourNumber = todaysDate.Hour.ToString();
    //    string minNumber = todaysDate.Minute.ToString();
    //    string secNumber = todaysDate.Second.ToString();


    //    var ts = _booking.DropOffDateTime - _booking.PickupDateTime;
    //    int noDaysHired = ts.Days;
    //    TimeSpan duration = new TimeSpan(2, 0, 0, 0);


    //    short includeInEshot = 1;
    //    string cameFrom = requestedDomain;//"www.ryanairdp.com";


    //    DateTime reminderEmail = _booking.PickupDateTime.Subtract(duration);
    //    DateTime thanksEmail = _booking.DropOffDateTime.Add(duration);
    //    string hertzNum1 = "hertz";
    //    string xmlProvider = carProvider.ToString();
    //    string emailCode = todaysNumber + monthNumber + yearNumber + hourNumber + minNumber + secNumber + myRandomNumber;
    //    short emailConfirmed = 1;

    //    double amountPaid = Convert.ToDouble(car.Price);

    //    string pickUpSupplier = CarProvider.Hertz.ToString();
    //    string dropOffSupplier = CarProvider.Hertz.ToString();

    //        var bookedOrder = db.Orders.Where(o => o.ConfirmationID == _booking.ConfirmationId).FirstOrDefault();

    //        if (bookedOrder != null)
    //        {
    //            ModifiedOrder modifiedOrder = new ModifiedOrder
    //            {
    //                OrderID = bookedOrder.OrderID,
    //                ConfirmationID = bookedOrder.ConfirmationID,
    //                CustomerName = bookedOrder.CustomerName,
    //                CustomerEmail = bookedOrder.CustomerEmail,
    //                EmailCode = bookedOrder.EmailCode,
    //                EmailConfirmed = bookedOrder.EmailConfirmed,
    //                CustomerPhone = bookedOrder.CustomerPhone,
    //                CarBooked = bookedOrder.CarBooked,
    //                VehicleType = bookedOrder.VehicleType,
    //                CountryOfResidence = bookedOrder.CountryOfResidence,
    //                PickUpDate = bookedOrder.PickUpDate,
    //                DropOffDate = bookedOrder.DropOffDate,
    //                OrderDate = bookedOrder.OrderDate,
    //                PickUpLocation = bookedOrder.PickUpLocation,
    //                DropOffLocation = bookedOrder.DropOffLocation,
    //                NoDaysHired = bookedOrder.NoDaysHired,
    //                AmountPaid = bookedOrder.AmountPaid,
    //                AmountPaidCurrency = bookedOrder.AmountPaidCurrency,
    //                IncludeInEshot = bookedOrder.IncludeInEshot,
    //                CameFrom = bookedOrder.CameFrom,
    //                LanguageCode = bookedOrder.LanguageCode,
    //                ReminderEmail = bookedOrder.ReminderEmail,
    //                ThanksEmail = bookedOrder.ThanksEmail,
    //                HertzNum1 = bookedOrder.HertzNum1,
    //                XML_Provider = bookedOrder.XML_Provider,
    //                FlightNumber = bookedOrder.FlightNumber,
    //                XML_IATANumber = bookedOrder.XML_IATANumber,
    //                XML_RateQualifier = bookedOrder.XML_RateQualifier,
    //                IsPrePaid = bookedOrder.IsPrePaid,
    //                CustomerSurname = bookedOrder.CustomerSurname,
    //                CustomerFirstname = bookedOrder.CustomerFirstname,
    //                Modified = bookedOrder.Modified,
    //                CancelledBooking = bookedOrder.CancelledBooking,
    //                AddressLine1 = bookedOrder.AddressLine1,
    //                AddressLine2 = bookedOrder.AddressLine2,
    //                AddressTown = bookedOrder.AddressTown,
    //                AddressPostCodeZip = bookedOrder.AddressPostCodeZip,
    //                AddressCountryCode = bookedOrder.AddressCountryCode,
    //                AddressCountryWord = bookedOrder.AddressCountryWord,
    //                XML_Provider_SellingPrice = bookedOrder.XML_Provider_SellingPrice,
    //                XML_Provider_CCFee = bookedOrder.XML_Provider_CCFee,
    //                XML_Provider_WholesaleTotal = bookedOrder.XML_Provider_WholesaleTotal,
    //                XML_Provider_MarginShare = bookedOrder.XML_Provider_MarginShare,
    //                XML_Provider_PickupSupplier = bookedOrder.XML_Provider_PickupSupplier,
    //                XML_Provider_DropoffSupplier = bookedOrder.XML_Provider_DropoffSupplier
    //            };
    //            db.ModifiedOrders.AddObject(modifiedOrder);

    //            var bookedAccessory = db.AccessoriesOrdereds.Where(a => a.OrderIDFK == bookedOrder.OrderID);

    //            if (bookedAccessory != null)
    //            {
    //                foreach (var ba in bookedAccessory)
    //                {
    //                    AccessoriesModifiedOrdered assessoryModifyOrder = new AccessoriesModifiedOrdered
    //                    {
    //                        OrderIDFK = ba.OrderIDFK,
    //                        Accessory = ba.Accessory,
    //                        Price = ba.Price,
    //                        Currency = ba.Currency,
    //                        Quantity = ba.Quantity
    //                    };
    //                    db.AccessoriesModifiedOrdereds.AddObject(assessoryModifyOrder);
    //                    db.AccessoriesOrdereds.DeleteObject(ba);
    //                }
                    
    //            }


    //            db.SaveChanges();

    //            try
    //            {

    //                Order order = new Order
    //                {
    //                    OrderID = bookedOrder.OrderID,
    //                    ConfirmationID = _booking.ConfirmationId,
    //                    CustomerName = _booking.Customer.Firstname + " " + _booking.Customer.Surname,
    //                    CustomerEmail = _booking.Customer.Email,
    //                    EmailCode = emailCode,
    //                    EmailConfirmed = emailConfirmed,
    //                    CustomerPhone = _booking.Customer.PhoneNumber,
    //                    CarBooked = car.Name,
    //                    VehicleType = car.VehClassSizeValue,
    //                    CountryOfResidence = _booking.CountryOfResidenceCode,
    //                    PickUpDate = _booking.PickupDateTime,
    //                    DropOffDate = _booking.DropOffDateTime,
    //                    OrderDate = orderDate,
    //                    PickUpLocation = _booking.PickUpLocationCode,
    //                    DropOffLocation = _booking.DropOffLocationCode,
    //                    NoDaysHired = noDaysHired,
    //                    AmountPaid = amountPaid,
    //                    AmountPaidCurrency = car.CurrencySymbol,
    //                    IncludeInEshot = includeInEshot,
    //                    CameFrom = cameFrom,
    //                    LanguageCode = _booking.Language,
    //                    ReminderEmail = reminderEmail,
    //                    ThanksEmail = thanksEmail,
    //                    HertzNum1 = hertzNum1,
    //                    XML_Provider = xmlProvider,
    //                    FlightNumber = _booking.FlightNum,
    //                    XML_IATANumber = _booking.IataNumber,
    //                    XML_RateQualifier = _booking.RateQualifierNumber,
    //                    IsPrePaid = _booking.PrePaySelected,
    //                    CustomerSurname = _booking.Customer.Surname,
    //                    CustomerFirstname = _booking.Customer.Firstname,
    //                    Modified = true,
    //                    CancelledBooking = false,
    //                    AddressLine1 = _booking.Customer.Address1,
    //                    AddressLine2 = _booking.Customer.Address2,
    //                    AddressTown = _booking.Customer.Town,
    //                    AddressPostCodeZip = _booking.Customer.Postcode,
    //                    AddressCountryCode = _booking.Customer.Country,
    //                    AddressCountryWord = _booking.Customer.Country,
    //                    XML_Provider_SellingPrice = "",
    //                    XML_Provider_CCFee = "",
    //                    XML_Provider_WholesaleTotal = "",
    //                    XML_Provider_MarginShare = "",
    //                    XML_Provider_PickupSupplier = pickUpSupplier,
    //                    XML_Provider_DropoffSupplier = dropOffSupplier
    //                };
    //                // db.Orders.AddObject(order);
    //                db.SaveChanges();

    //                int orderID = order.OrderID;

    //                foreach (var equipment in _booking.Accessories)
    //                {
    //                    string accessory = equipment.EquipType;
    //                    double price = Convert.ToDouble(equipment.Price);
    //                    string currency = equipment.CurrencySymbol;
    //                    short quantity = Convert.ToInt16(equipment.Quantity);
    //                    AccessoriesOrdered acc = new AccessoriesOrdered
    //                    {
    //                        OrderIDFK = orderID,
    //                        Accessory = accessory,
    //                        Price = price,
    //                        Currency = currency,
    //                        Quantity = quantity
    //                    };
    //                    db.AccessoriesOrdereds.AddObject(acc);
    //                }
    //                db.SaveChanges();

    //            }
    //            catch(Exception e)
    //            {
    //                CommonTasks.GetErrorXML("VehResRS", "1", "099", "Error   "+e.StackTrace);
    //            }

                
    //        }

    //    try
    //    {
    //        //Build confirmation email with current booking object
    //        BuildConfirmationEmail confEmailBuilder = new BuildConfirmationEmail(_booking, car, selectedBrand);

    //        string emailMessage = confEmailBuilder.BuildEmail(_booking.ConfirmationId, car.CurrencySymbol, car.Price.ToString(), car.Image, car.LocalizedName, _booking.CrossBorderDropFee, car.PriceIncludes, car.DistanceIncluded, TimeBetweenDates.CalculateMinutes(_booking.PickupDateTime, Convert.ToDateTime(_booking.DropOffDateTime)));
    //        string emailSubject = _booking.ModifyProcess ? (string)HttpContext.GetGlobalResourceObject("SiteStrings", "ModifyEmailSubject") : (string)HttpContext.GetGlobalResourceObject("SiteStrings", "BookingEmailSubject");
    //        //Send confirmation email
    //        bool isSent = SendEmail.SendMessage(WebsiteSettings.SiteEmailsSentFrom, _booking.Customer.Email, emailSubject, emailMessage, true);
            
    //        if (!isSent)
    //        {
    //            return CommonTasks.GetErrorXML("VehModifyRS", "1", "099", "Booking Modify Completed Successfully. Error Occured while sending confirmation email. Please Contact Hertz.");
    //        }
    //        else
    //        {
    //            return "";
    //        }

    //    }
    //    catch
    //    {
    //        return CommonTasks.GetErrorXML("VehModifyRS", "1", "099", "Booking Modify Completed Successfully. Error Occured while sending confirmation email. Please Contact Hertz.");
    //    }

    //}

    //public static string SaveModifyBookingInDatabaseAndSendEmailInD_Tag(string responseXML, string requestXML, CarProvider carProvider, string requestedDomain, string ryanairXML)
    //{
    //    RyPreReqDBBooking ryPreReqObj = new RyPreReqDBBooking();
    //    DatabaseEntities db = new DatabaseEntities();
    //    ExtractOrderDetails extractedOrderXml = new ExtractOrderDetails();
    //    Booking _booking = extractedOrderXml.InitializeBookingD_Tag(responseXML, requestXML, carProvider, ryPreReqObj);

    //    //Add language initialization here.......

    //    _booking = extractedOrderXml.ModifyBokkingLanguage(ryanairXML, _booking);
    //    Thread.CurrentThread.CurrentUICulture = new CultureInfo(_booking.Language);
    //    Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(_booking.Language);

    //    //CommonTasks.SendXmlForDebugging(carProvider.ToString(), "Test3");
    //    var selectedBrand = _booking.Brands.SingleOrDefault(b => b.CarProvider == carProvider);
    //    var car = selectedBrand.Cars.FirstOrDefault();

    //    //create the 'email code' we will use this to track instead of sending customers email for privacy reasons
    //    DateTime todaysDate = DateTime.Now;
    //    string todaysDateString = todaysDate.ToString("yyyy/MM/dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);

    //    DateTime orderDate = DateTime.Now;

    //    var randNum = new Random();
    //    int myRandomNumber = randNum.Next(1, 99999);
    //    string todaysNumber = todaysDate.Day.ToString();
    //    string monthNumber = todaysDate.Month.ToString();
    //    string yearNumber = todaysDate.ToString("yy", System.Globalization.CultureInfo.InvariantCulture);
    //    string hourNumber = todaysDate.Hour.ToString();
    //    string minNumber = todaysDate.Minute.ToString();
    //    string secNumber = todaysDate.Second.ToString();


    //    var ts = _booking.DropOffDateTime - _booking.PickupDateTime;
    //    int noDaysHired = ts.Days;
    //    TimeSpan duration = new TimeSpan(2, 0, 0, 0);


    //    short includeInEshot = 1;
    //    string cameFrom = requestedDomain;//"www.ryanairdp.com";


    //    DateTime reminderEmail = _booking.PickupDateTime.Subtract(duration);
    //    DateTime thanksEmail = _booking.DropOffDateTime.Add(duration);
    //    string hertzNum1 = "hertz";
    //    string xmlProvider = carProvider.ToString();
    //    string emailCode = todaysNumber + monthNumber + yearNumber + hourNumber + minNumber + secNumber + myRandomNumber;
    //    short emailConfirmed = 1;

    //    double amountPaid = Convert.ToDouble(car.Price);

    //    string pickUpSupplier = CarProvider.Hertz.ToString();
    //    string dropOffSupplier = CarProvider.Hertz.ToString();

    //    var bookedOrder = db.Orders.Where(o => o.ConfirmationID == _booking.ConfirmationId).FirstOrDefault();

    //    if (bookedOrder != null)
    //    {
    //        CommonTasks.SendXmlForDebugging(bookedOrder.ConfirmationID.ToString(), "Test2");
    //        //ModifiedOrder modifiedOrder = new ModifiedOrder
    //        //{
    //        //    OrderID = bookedOrder.OrderID,
    //        //    ConfirmationID = bookedOrder.ConfirmationID,
    //        //    CustomerName = bookedOrder.CustomerName,
    //        //    CustomerEmail = bookedOrder.CustomerEmail,
    //        //    EmailCode = bookedOrder.EmailCode,
    //        //    EmailConfirmed = bookedOrder.EmailConfirmed,
    //        //    CustomerPhone = bookedOrder.CustomerPhone,
    //        //    CarBooked = bookedOrder.CarBooked,
    //        //    VehicleType = bookedOrder.VehicleType,
    //        //    CountryOfResidence = bookedOrder.CountryOfResidence,
    //        //    PickUpDate = bookedOrder.PickUpDate,
    //        //    DropOffDate = bookedOrder.DropOffDate,
    //        //    OrderDate = bookedOrder.OrderDate,
    //        //    PickUpLocation = bookedOrder.PickUpLocation,
    //        //    DropOffLocation = bookedOrder.DropOffLocation,
    //        //    NoDaysHired = bookedOrder.NoDaysHired,
    //        //    AmountPaid = bookedOrder.AmountPaid,
    //        //    AmountPaidCurrency = bookedOrder.AmountPaidCurrency,
    //        //    IncludeInEshot = bookedOrder.IncludeInEshot,
    //        //    CameFrom = bookedOrder.CameFrom,
    //        //    LanguageCode = bookedOrder.LanguageCode,
    //        //    ReminderEmail = bookedOrder.ReminderEmail,
    //        //    ThanksEmail = bookedOrder.ThanksEmail,
    //        //    HertzNum1 = bookedOrder.HertzNum1,
    //        //    XML_Provider = bookedOrder.XML_Provider,
    //        //    FlightNumber = bookedOrder.FlightNumber,
    //        //    XML_IATANumber = bookedOrder.XML_IATANumber,
    //        //    XML_RateQualifier = bookedOrder.XML_RateQualifier,
    //        //    IsPrePaid = bookedOrder.IsPrePaid,
    //        //    CustomerSurname = bookedOrder.CustomerSurname,
    //        //    CustomerFirstname = bookedOrder.CustomerFirstname,
    //        //    Modified = bookedOrder.Modified,
    //        //    CancelledBooking = bookedOrder.CancelledBooking,
    //        //    AddressLine1 = bookedOrder.AddressLine1,
    //        //    AddressLine2 = bookedOrder.AddressLine2,
    //        //    AddressTown = bookedOrder.AddressTown,
    //        //    AddressPostCodeZip = bookedOrder.AddressPostCodeZip,
    //        //    AddressCountryCode = bookedOrder.AddressCountryCode,
    //        //    AddressCountryWord = bookedOrder.AddressCountryWord,
    //        //    XML_Provider_SellingPrice = bookedOrder.XML_Provider_SellingPrice,
    //        //    XML_Provider_CCFee = bookedOrder.XML_Provider_CCFee,
    //        //    XML_Provider_WholesaleTotal = bookedOrder.XML_Provider_WholesaleTotal,
    //        //    XML_Provider_MarginShare = bookedOrder.XML_Provider_MarginShare,
    //        //    XML_Provider_PickupSupplier = bookedOrder.XML_Provider_PickupSupplier,
    //        //    XML_Provider_DropoffSupplier = bookedOrder.XML_Provider_DropoffSupplier
    //        //};
    //        //db.ModifiedOrders.AddObject(modifiedOrder);

    //        //var bookedAccessory = db.AccessoriesOrdereds.Where(a => a.OrderIDFK == bookedOrder.OrderID);

    //        //if (bookedAccessory != null)
    //        //{
    //        //    foreach (var ba in bookedAccessory)
    //        //    {
    //        //        AccessoriesModifiedOrdered assessoryModifyOrder = new AccessoriesModifiedOrdered
    //        //        {
    //        //            OrderIDFK = ba.OrderIDFK,
    //        //            Accessory = ba.Accessory,
    //        //            Price = ba.Price,
    //        //            Currency = ba.Currency,
    //        //            Quantity = ba.Quantity
    //        //        };
    //        //        db.AccessoriesModifiedOrdereds.AddObject(assessoryModifyOrder);
    //        //        db.AccessoriesOrdereds.DeleteObject(ba);
    //        //    }

    //        //}


    //        //db.SaveChanges();

    //        //try
    //        //{

    //        //    Order order = new Order
    //        //    {
    //        //        OrderID = bookedOrder.OrderID,
    //        //        ConfirmationID = _booking.ConfirmationId,
    //        //        CustomerName = _booking.Customer.Firstname + " " + _booking.Customer.Surname,
    //        //        CustomerEmail = _booking.Customer.Email,
    //        //        EmailCode = emailCode,
    //        //        EmailConfirmed = emailConfirmed,
    //        //        CustomerPhone = _booking.Customer.PhoneNumber,
    //        //        CarBooked = car.Name,
    //        //        VehicleType = car.VehClassSizeValue,
    //        //        CountryOfResidence = _booking.CountryOfResidenceCode,
    //        //        PickUpDate = _booking.PickupDateTime,
    //        //        DropOffDate = _booking.DropOffDateTime,
    //        //        OrderDate = orderDate,
    //        //        PickUpLocation = _booking.PickUpLocationCode,
    //        //        DropOffLocation = _booking.DropOffLocationCode,
    //        //        NoDaysHired = noDaysHired,
    //        //        AmountPaid = amountPaid,
    //        //        AmountPaidCurrency = car.CurrencySymbol,
    //        //        IncludeInEshot = includeInEshot,
    //        //        CameFrom = cameFrom,
    //        //        LanguageCode = _booking.Language,
    //        //        ReminderEmail = reminderEmail,
    //        //        ThanksEmail = thanksEmail,
    //        //        HertzNum1 = hertzNum1,
    //        //        XML_Provider = xmlProvider,
    //        //        FlightNumber = _booking.FlightNum,
    //        //        XML_IATANumber = _booking.IataNumber,
    //        //        XML_RateQualifier = _booking.RateQualifierNumber,
    //        //        IsPrePaid = _booking.PrePaySelected,
    //        //        CustomerSurname = _booking.Customer.Surname,
    //        //        CustomerFirstname = _booking.Customer.Firstname,
    //        //        Modified = true,
    //        //        CancelledBooking = false,
    //        //        AddressLine1 = _booking.Customer.Address1,
    //        //        AddressLine2 = _booking.Customer.Address2,
    //        //        AddressTown = _booking.Customer.Town,
    //        //        AddressPostCodeZip = _booking.Customer.Postcode,
    //        //        AddressCountryCode = _booking.Customer.Country,
    //        //        AddressCountryWord = _booking.Customer.Country,
    //        //        XML_Provider_SellingPrice = "",
    //        //        XML_Provider_CCFee = "",
    //        //        XML_Provider_WholesaleTotal = "",
    //        //        XML_Provider_MarginShare = "",
    //        //        XML_Provider_PickupSupplier = pickUpSupplier,
    //        //        XML_Provider_DropoffSupplier = dropOffSupplier
    //        //    };
    //        //    // db.Orders.AddObject(order);
    //        //    db.SaveChanges();

    //        //    int orderID = order.OrderID;

    //        //    foreach (var equipment in _booking.Accessories)
    //        //    {
    //        //        string accessory = equipment.EquipType;
    //        //        double price = Convert.ToDouble(equipment.Price);
    //        //        string currency = equipment.CurrencySymbol;
    //        //        short quantity = Convert.ToInt16(equipment.Quantity);
    //        //        AccessoriesOrdered acc = new AccessoriesOrdered
    //        //        {
    //        //            OrderIDFK = orderID,
    //        //            Accessory = accessory,
    //        //            Price = price,
    //        //            Currency = currency,
    //        //            Quantity = quantity
    //        //        };
    //        //        db.AccessoriesOrdereds.AddObject(acc);
    //        //    }
    //        //    db.SaveChanges();

    //        //}
    //        //catch (Exception e)
    //        //{
    //        //    CommonTasks.GetErrorXML("VehResRS", "1", "099", "Error   " + e.StackTrace);
    //        //}


    //    }

    //    try
    //    {
    //        //Build confirmation email with current booking object
    //        BuildConfirmationEmail confEmailBuilder = new BuildConfirmationEmail(_booking, car, selectedBrand);

    //        string emailMessage = confEmailBuilder.BuildEmail(_booking.ConfirmationId, car.CurrencySymbol, car.Price.ToString(), car.Image, car.LocalizedName, _booking.CrossBorderDropFee, car.PriceIncludes, car.DistanceIncluded, TimeBetweenDates.CalculateMinutes(_booking.PickupDateTime, Convert.ToDateTime(_booking.DropOffDateTime)));
    //        string emailSubject = _booking.ModifyProcess ? (string)HttpContext.GetGlobalResourceObject("SiteStrings", "ModifyEmailSubject") : (string)HttpContext.GetGlobalResourceObject("SiteStrings", "BookingEmailSubject");
    //        //Send confirmation email
    //        bool isSent = SendEmail.SendMessage(WebsiteSettings.SiteEmailsSentFrom, _booking.Customer.Email, emailSubject, emailMessage, true);

    //        if (!isSent)
    //        {
    //            return CommonTasks.GetErrorXML("VehModifyRS", "1", "099", "Booking Modify Completed Successfully. Error Occured while sending confirmation email. Please Contact Hertz.");
    //        }
    //        else
    //        {
    //            return "";
    //        }

    //    }
    //    catch
    //    {
    //        return CommonTasks.GetErrorXML("VehModifyRS", "1", "099", "Booking Modify Completed Successfully. Error Occured while sending confirmation email. Please Contact Hertz.");
    //    }

    //}


    //public static void SaveCancleBookingInDatabaseAndSendEmail(string responseXML, string requestXML, CarProvider carProvider, string requestedDomain)
    //{
    //    DatabaseEntities db = new DatabaseEntities();
    //    ExtractOrderDetails extractedOrderXml = new ExtractOrderDetails();
    //    Booking _booking = extractedOrderXml.InitializeCancledBooking(responseXML, requestXML, carProvider);

    //    //var selectedBrand = _booking.Brands.SingleOrDefault(b => b.CarProvider == carProvider);
    //    //var car = selectedBrand.Cars.FirstOrDefault();

    //    ////create the 'email code' we will use this to track instead of sending customers email for privacy reasons
    //    //DateTime todaysDate = DateTime.Now;
    //    //string todaysDateString = todaysDate.ToString("yyyy/MM/dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);

    //    //DateTime orderDate = DateTime.Now;

    //    //var randNum = new Random();
    //    //int myRandomNumber = randNum.Next(1, 99999);
    //    //string todaysNumber = todaysDate.Day.ToString();
    //    //string monthNumber = todaysDate.Month.ToString();
    //    //string yearNumber = todaysDate.ToString("yy", System.Globalization.CultureInfo.InvariantCulture);
    //    //string hourNumber = todaysDate.Hour.ToString();
    //    //string minNumber = todaysDate.Minute.ToString();
    //    //string secNumber = todaysDate.Second.ToString();


    //    //var ts = _booking.DropOffDateTime - _booking.PickupDateTime;
    //    //int noDaysHired = ts.Days;
    //    //TimeSpan duration = new TimeSpan(2, 0, 0, 0);


    //    //short includeInEshot = 1;
    //    //string cameFrom = requestedDomain;//"www.ryanairdp.com";


    //    //DateTime reminderEmail = _booking.PickupDateTime.Subtract(duration);
    //    //DateTime thanksEmail = _booking.DropOffDateTime.Add(duration);
    //    //string hertzNum1 = "hertz";
    //    //string xmlProvider = carProvider.ToString();
    //    //string emailCode = todaysNumber + monthNumber + yearNumber + hourNumber + minNumber + secNumber + myRandomNumber;
    //    //short emailConfirmed = 1;

    //    //double amountPaid = Convert.ToDouble(car.Price);

    //    //string pickUpSupplier = CarProvider.Hertz.ToString();
    //    //string dropOffSupplier = CarProvider.Hertz.ToString();

    //    if (_booking.ConfirmationId != null)
    //    {
    //        var bookedOrder = db.Orders.Where(o => o.ConfirmationID == _booking.ConfirmationId).FirstOrDefault();

    //        if (bookedOrder != null)
    //        {
    //            CanclledOrder canclledOrder = new CanclledOrder
    //                {
    //                    OrderID = bookedOrder.OrderID,
    //                    ConfirmationID= bookedOrder.ConfirmationID,
    //                    CustomerName = bookedOrder.CustomerName,
    //                    CustomerEmail = bookedOrder.CustomerEmail,
    //                    EmailCode = bookedOrder.EmailCode,
    //                    EmailConfirmed = bookedOrder.EmailConfirmed,
    //                    CustomerPhone = bookedOrder.CustomerPhone,
    //                    CarBooked = bookedOrder.CarBooked,
    //                    VehicleType = bookedOrder.VehicleType,
    //                    CountryOfResidence = bookedOrder.CountryOfResidence,
    //                    PickUpDate = bookedOrder.PickUpDate,
    //                    DropOffDate = bookedOrder.DropOffDate,
    //                    OrderDate = bookedOrder.OrderDate,
    //                    PickUpLocation = bookedOrder.PickUpLocation,
    //                    DropOffLocation = bookedOrder.DropOffLocation,
    //                    NoDaysHired = bookedOrder.NoDaysHired,
    //                    AmountPaid = bookedOrder.AmountPaid,
    //                    AmountPaidCurrency = bookedOrder.AmountPaidCurrency,
    //                    IncludeInEshot = bookedOrder.IncludeInEshot,
    //                    CameFrom = bookedOrder.CameFrom,
    //                    LanguageCode = bookedOrder.LanguageCode,
    //                    ReminderEmail = bookedOrder.ReminderEmail,
    //                    ThanksEmail = bookedOrder.ThanksEmail,
    //                    HertzNum1 = bookedOrder.HertzNum1,
    //                    XML_Provider = bookedOrder.XML_Provider,
    //                    FlightNumber = bookedOrder.FlightNumber,
    //                    XML_IATANumber = bookedOrder.XML_IATANumber,
    //                    XML_RateQualifier = bookedOrder.XML_RateQualifier,
    //                    IsPrePaid = bookedOrder.IsPrePaid,
    //                    CustomerSurname = bookedOrder.CustomerSurname,
    //                    CustomerFirstname = bookedOrder.CustomerFirstname,
    //                    Modified = bookedOrder.Modified,
    //                    CancelledBooking = bookedOrder.CancelledBooking,
    //                    AddressLine1 = bookedOrder.AddressLine1,
    //                    AddressLine2 = bookedOrder.AddressLine2,
    //                    AddressTown = bookedOrder.AddressTown,
    //                    AddressPostCodeZip = bookedOrder.AddressPostCodeZip,
    //                    AddressCountryCode = bookedOrder.AddressCountryCode,
    //                    AddressCountryWord = bookedOrder.AddressCountryWord,
    //                    XML_Provider_SellingPrice = bookedOrder.XML_Provider_SellingPrice,
    //                    XML_Provider_CCFee = bookedOrder.XML_Provider_CCFee,
    //                    XML_Provider_WholesaleTotal = bookedOrder.XML_Provider_WholesaleTotal,
    //                    XML_Provider_MarginShare = bookedOrder.XML_Provider_MarginShare,
    //                    XML_Provider_PickupSupplier = bookedOrder.XML_Provider_PickupSupplier,
    //                    XML_Provider_DropoffSupplier = bookedOrder.XML_Provider_DropoffSupplier
    //                };
    //            db.CanclledOrders.AddObject(canclledOrder);
                


    //            var bookedAccessory = db.AccessoriesOrdereds.Where(a => a.OrderIDFK == bookedOrder.OrderID);

    //            if (bookedAccessory != null)
    //            {
    //                foreach (var ba in bookedAccessory)
    //                {
    //                    AccessoriesCancelledOrdered assessoryCancleOrder = new AccessoriesCancelledOrdered
    //                    {
    //                        OrderIDFK = ba.OrderIDFK,
    //                        Accessory = ba.Accessory,
    //                        Price = ba.Price,
    //                        Currency = ba.Currency,
    //                        Quantity = ba.Quantity
    //                    };
    //                    db.AccessoriesCancelledOrdereds.AddObject(assessoryCancleOrder);
    //                    db.AccessoriesOrdereds.DeleteObject(ba);
    //                }      
    //            }
    //        }

    //        db.Orders.DeleteObject(bookedOrder);
    //        db.SaveChanges();
    //    }

    //    //try
    //    //{
    //    //    //Build confirmation email with current booking object
    //    //    BuildConfirmationEmail confEmailBuilder = new BuildConfirmationEmail(_booking, car, selectedBrand);

    //    //    string emailMessage = confEmailBuilder.BuildEmail(_booking.ConfirmationId, car.CurrencySymbol, car.Price.ToString(), car.Image, car.LocalizedName, _booking.CrossBorderDropFee, car.PriceIncludes, car.DistanceIncluded, TimeBetweenDates.CalculateMinutes(_booking.PickupDateTime, Convert.ToDateTime(_booking.DropOffDateTime)));
    //    //    string emailSubject = _booking.ModifyProcess ? (string)HttpContext.GetGlobalResourceObject("SiteStrings", "ModifyEmailSubject") : (string)HttpContext.GetGlobalResourceObject("SiteStrings", "BookingEmailSubject");
    //    //    //Send confirmation email
    //    //    bool isSent = SendEmail.SendMessage(WebsiteSettings.SiteEmailsSentFrom, _booking.Customer.Email, emailSubject, emailMessage, true);
    //    //    if (!isSent)
    //    //    {
    //    //        return CommonTasks.GetErrorXML("VehCancelRS", "1", "099", "Booking Cancle Completed Successfully. Error Occured while sending booking cancle email. Please Contact Hertz.");
    //    //    }
    //    //    else
    //    //    {
    //    //        return "";
    //    //    }

    //    //}
    //    //catch
    //    //{
    //    //    return CommonTasks.GetErrorXML("VehCancelRS", "1", "099", "Booking Cancle Completed Successfully. Error Occured while sending booking cancle email. Please Contact Hertz.");
    //    //}

    //}

    //public static void SaveCancleBookingInDatabaseAndSendEmailinD_Tag(string responseXML, string requestXML, CarProvider carProvider, string requestedDomain)
    //{
    //    DatabaseEntities db = new DatabaseEntities();
    //    ExtractOrderDetails extractedOrderXml = new ExtractOrderDetails();
    //    Booking _booking = extractedOrderXml.InitializeCancledBookingInD_Tag(responseXML, requestXML, carProvider);

    //    //var selectedBrand = _booking.Brands.SingleOrDefault(b => b.CarProvider == carProvider);
    //    //var car = selectedBrand.Cars.FirstOrDefault();

    //    ////create the 'email code' we will use this to track instead of sending customers email for privacy reasons
    //    //DateTime todaysDate = DateTime.Now;
    //    //string todaysDateString = todaysDate.ToString("yyyy/MM/dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);

    //    //DateTime orderDate = DateTime.Now;

    //    //var randNum = new Random();
    //    //int myRandomNumber = randNum.Next(1, 99999);
    //    //string todaysNumber = todaysDate.Day.ToString();
    //    //string monthNumber = todaysDate.Month.ToString();
    //    //string yearNumber = todaysDate.ToString("yy", System.Globalization.CultureInfo.InvariantCulture);
    //    //string hourNumber = todaysDate.Hour.ToString();
    //    //string minNumber = todaysDate.Minute.ToString();
    //    //string secNumber = todaysDate.Second.ToString();


    //    //var ts = _booking.DropOffDateTime - _booking.PickupDateTime;
    //    //int noDaysHired = ts.Days;
    //    //TimeSpan duration = new TimeSpan(2, 0, 0, 0);


    //    //short includeInEshot = 1;
    //    //string cameFrom = requestedDomain;//"www.ryanairdp.com";


    //    //DateTime reminderEmail = _booking.PickupDateTime.Subtract(duration);
    //    //DateTime thanksEmail = _booking.DropOffDateTime.Add(duration);
    //    //string hertzNum1 = "hertz";
    //    //string xmlProvider = carProvider.ToString();
    //    //string emailCode = todaysNumber + monthNumber + yearNumber + hourNumber + minNumber + secNumber + myRandomNumber;
    //    //short emailConfirmed = 1;

    //    //double amountPaid = Convert.ToDouble(car.Price);

    //    //string pickUpSupplier = CarProvider.Hertz.ToString();
    //    //string dropOffSupplier = CarProvider.Hertz.ToString();

    //    if (_booking.ConfirmationId != null)
    //    {
    //        var bookedOrder = db.Orders.Where(o => o.ConfirmationID == _booking.ConfirmationId).FirstOrDefault();

    //        if (bookedOrder != null)
    //        {
    //            CanclledOrder canclledOrder = new CanclledOrder
    //            {
    //                OrderID = bookedOrder.OrderID,
    //                ConfirmationID = bookedOrder.ConfirmationID,
    //                CustomerName = bookedOrder.CustomerName,
    //                CustomerEmail = bookedOrder.CustomerEmail,
    //                EmailCode = bookedOrder.EmailCode,
    //                EmailConfirmed = bookedOrder.EmailConfirmed,
    //                CustomerPhone = bookedOrder.CustomerPhone,
    //                CarBooked = bookedOrder.CarBooked,
    //                VehicleType = bookedOrder.VehicleType,
    //                CountryOfResidence = bookedOrder.CountryOfResidence,
    //                PickUpDate = bookedOrder.PickUpDate,
    //                DropOffDate = bookedOrder.DropOffDate,
    //                OrderDate = bookedOrder.OrderDate,
    //                PickUpLocation = bookedOrder.PickUpLocation,
    //                DropOffLocation = bookedOrder.DropOffLocation,
    //                NoDaysHired = bookedOrder.NoDaysHired,
    //                AmountPaid = bookedOrder.AmountPaid,
    //                AmountPaidCurrency = bookedOrder.AmountPaidCurrency,
    //                IncludeInEshot = bookedOrder.IncludeInEshot,
    //                CameFrom = bookedOrder.CameFrom,
    //                LanguageCode = bookedOrder.LanguageCode,
    //                ReminderEmail = bookedOrder.ReminderEmail,
    //                ThanksEmail = bookedOrder.ThanksEmail,
    //                HertzNum1 = bookedOrder.HertzNum1,
    //                XML_Provider = bookedOrder.XML_Provider,
    //                FlightNumber = bookedOrder.FlightNumber,
    //                XML_IATANumber = bookedOrder.XML_IATANumber,
    //                XML_RateQualifier = bookedOrder.XML_RateQualifier,
    //                IsPrePaid = bookedOrder.IsPrePaid,
    //                CustomerSurname = bookedOrder.CustomerSurname,
    //                CustomerFirstname = bookedOrder.CustomerFirstname,
    //                Modified = bookedOrder.Modified,
    //                CancelledBooking = bookedOrder.CancelledBooking,
    //                AddressLine1 = bookedOrder.AddressLine1,
    //                AddressLine2 = bookedOrder.AddressLine2,
    //                AddressTown = bookedOrder.AddressTown,
    //                AddressPostCodeZip = bookedOrder.AddressPostCodeZip,
    //                AddressCountryCode = bookedOrder.AddressCountryCode,
    //                AddressCountryWord = bookedOrder.AddressCountryWord,
    //                XML_Provider_SellingPrice = bookedOrder.XML_Provider_SellingPrice,
    //                XML_Provider_CCFee = bookedOrder.XML_Provider_CCFee,
    //                XML_Provider_WholesaleTotal = bookedOrder.XML_Provider_WholesaleTotal,
    //                XML_Provider_MarginShare = bookedOrder.XML_Provider_MarginShare,
    //                XML_Provider_PickupSupplier = bookedOrder.XML_Provider_PickupSupplier,
    //                XML_Provider_DropoffSupplier = bookedOrder.XML_Provider_DropoffSupplier
    //            };
    //            db.CanclledOrders.AddObject(canclledOrder);



    //            var bookedAccessory = db.AccessoriesOrdereds.Where(a => a.OrderIDFK == bookedOrder.OrderID);

    //            if (bookedAccessory != null)
    //            {
    //                foreach (var ba in bookedAccessory)
    //                {
    //                    AccessoriesCancelledOrdered assessoryCancleOrder = new AccessoriesCancelledOrdered
    //                    {
    //                        OrderIDFK = ba.OrderIDFK,
    //                        Accessory = ba.Accessory,
    //                        Price = ba.Price,
    //                        Currency = ba.Currency,
    //                        Quantity = ba.Quantity
    //                    };
    //                    db.AccessoriesCancelledOrdereds.AddObject(assessoryCancleOrder);
    //                    db.AccessoriesOrdereds.DeleteObject(ba);
    //                }
    //            }
    //        }

    //        db.Orders.DeleteObject(bookedOrder);
    //        db.SaveChanges();
    //    }

    //    //try
    //    //{
    //    //    //Build confirmation email with current booking object
    //    //    BuildConfirmationEmail confEmailBuilder = new BuildConfirmationEmail(_booking, car, selectedBrand);

    //    //    string emailMessage = confEmailBuilder.BuildEmail(_booking.ConfirmationId, car.CurrencySymbol, car.Price.ToString(), car.Image, car.LocalizedName, _booking.CrossBorderDropFee, car.PriceIncludes, car.DistanceIncluded, TimeBetweenDates.CalculateMinutes(_booking.PickupDateTime, Convert.ToDateTime(_booking.DropOffDateTime)));
    //    //    string emailSubject = _booking.ModifyProcess ? (string)HttpContext.GetGlobalResourceObject("SiteStrings", "ModifyEmailSubject") : (string)HttpContext.GetGlobalResourceObject("SiteStrings", "BookingEmailSubject");
    //    //    //Send confirmation email
    //    //    bool isSent = SendEmail.SendMessage(WebsiteSettings.SiteEmailsSentFrom, _booking.Customer.Email, emailSubject, emailMessage, true);
    //    //    if (!isSent)
    //    //    {
    //    //        return CommonTasks.GetErrorXML("VehCancelRS", "1", "099", "Booking Cancle Completed Successfully. Error Occured while sending booking cancle email. Please Contact Hertz.");
    //    //    }
    //    //    else
    //    //    {
    //    //        return "";
    //    //    }

    //    //}
    //    //catch
    //    //{
    //    //    return CommonTasks.GetErrorXML("VehCancelRS", "1", "099", "Booking Cancle Completed Successfully. Error Occured while sending booking cancle email. Please Contact Hertz.");
    //    //}

    //}



    //public static void SendBookingFailedEmail(string responseXML, string requestXML, CarProvider carProvider, string ryanairXML,string requestedDomain)
    //{
    //    ExtractOrderDetails extractedOrderXml = new ExtractOrderDetails();
    //    Booking _booking = extractedOrderXml.InitializeBooking(responseXML, requestXML, carProvider);

    //    //Add language initialization here.......

    //    _booking = extractedOrderXml.ModifyBokkingLanguage(ryanairXML, _booking);
    //    Thread.CurrentThread.CurrentUICulture = new CultureInfo(_booking.Language);
    //    Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(_booking.Language);


    //    //set Country



    //    var selectedBrand = _booking.Brands.SingleOrDefault(b => b.CarProvider == carProvider);
    //    var car = selectedBrand.Cars.FirstOrDefault();

    //    //Build confirmation email with current booking object
    //    BuildConfirmationEmail confEmailBuilder = new BuildConfirmationEmail(_booking, car, selectedBrand);

    //    string emailMessage = confEmailBuilder.BuildBookingFailedEmail();
    //    //email subject need to add to resource file
    //    string emailSubject = (string)HttpContext.GetGlobalResourceObject("SiteStrings", "BookingFailEmailSubject");
    //    //Send confirmation email
    //    SendEmail.SendMessage(WebsiteSettings.SiteEmailsSentFrom, _booking.Customer.Email, emailSubject, emailMessage, true);

    //    string partnerMail = GetPartnerMailId(requestedDomain);
    //    if (partnerMail != "")
    //    {
    //        SendEmail.SendMessage(WebsiteSettings.SiteEmailsSentFrom, partnerMail, emailSubject, emailMessage, true);
    //    }
    
    
    //}

    //public static string GetPartnerMailId(string requestedDomain)
    //{
    //    DatabaseEntities db = new DatabaseEntities();
    //    var partnerDtl = db.Partners.SingleOrDefault(p => p.DomainName == requestedDomain);
    //    if (partnerDtl != null)
    //    {
    //        return partnerDtl.Email.Trim();
    //    }
    //    else
    //    {
    //        return "";
    //    }

    //}


    //Remove credit cards informations
    //private static string RemoveCreditCardDetails(string xmlSent, CarProvider carProvider)
    //{

    //    // remove from Hertz calls
    //    if ((!String.IsNullOrEmpty(xmlSent)) && (xmlSent.IndexOf("CardNumber=", StringComparison.OrdinalIgnoreCase) > 0))
    //    {
    //        int startLocationOfCardNumber = xmlSent.IndexOf("CardNumber=", StringComparison.OrdinalIgnoreCase) + 12;
    //        xmlSent = xmlSent.Remove(startLocationOfCardNumber, 12);
    //    }

    //    return xmlSent;
    //}
    ////check any resource is available
    public static bool UrlExists(string url)
    {
        try
        {
            new System.Net.WebClient().DownloadData(url);
            return true;
        }
        catch (System.Net.WebException e)
        {
            return false;
        }
    }
    //public static string FormatXml(XmlDocument document)
    //{
    //    //StringBuilder builder = new StringBuilder();

    //    //using (StringWriter stringWriter = new StringWriter(builder))
    //    //{
    //    //    using (XmlTextWriter xmlTextWriter = new XmlTextWriter(stringWriter))
    //    //    {
    //    //        xmlTextWriter.Formatting = Formatting.Indented;
    //    //        xmlNode.WriteTo(xmlTextWriter);
    //    //    }
    //    //}



    //    //using (XmlNodeReader reader = new XmlNodeReader(document))
    //    //{
    //    //    return XDocument.Load(reader, LoadOptions.None).ToString();
    //    //}


    //    //return XDocument.Parse(xmlNode.OuterXml).ToString();
    //    //return xmlNode.ToString();

    //    StringBuilder sb = new StringBuilder();
    //    XmlWriterSettings settings = new XmlWriterSettings();
    //    settings.Indent = true;
    //    settings.IndentChars = "  ";
    //    settings.NewLineChars = "\r\n";
    //    settings.NewLineHandling = NewLineHandling.Replace;
    //    using (XmlWriter writer = XmlWriter.Create(sb, settings))
    //    {
    //        document.Save(writer);
    //    }
    //    return sb.ToString();

    //}

    //public static int GetVehicleEnable(string requestedDomain)
    //{
    //    int vehiclePref = 0;
    //    DatabaseEntities db = new DatabaseEntities();
    //    var partnervehiclelimit = (from p in db.Partners 
    //                               where p.DomainName == requestedDomain
    //                            select new
    //                            {
    //                                p.VehicleLimtEnable
    //                            });
    //    if (partnervehiclelimit.FirstOrDefault() != null)
    //    {
    //        vehiclePref = Convert.ToInt32(partnervehiclelimit.FirstOrDefault().VehicleLimtEnable);
    //    }

    //    return vehiclePref;
    //}


    //public static void LogDBTimeOutRequest(string requestXML, string requestedDomain, DateTime requestTime, DateTime XMLProviderSendTime, DateTime XMLProviderReceivedTime, string brand)
    //{
    //    DatabaseEntities db = new DatabaseEntities();
    //    DateTime orderDate = DateTime.Now;
    //    string cameFrom = requestedDomain;//"www.ryanairdp.com";
    //    RequestTimeOutLog RQTimeOut = new RequestTimeOutLog
    //    {
    //        RequestCameTime = requestTime,
    //        XMLProviderSendTime = XMLProviderSendTime,
    //        XMLProviderReceviedTime = XMLProviderReceivedTime,
    //        RequestDomain = requestedDomain,
    //        RequestXML = requestXML,
    //        LogDate = DateTime.Now,
    //        Brand = brand
    //    };
    //    db.RequestTimeOutLogs.Add(RQTimeOut);
    //    db.SaveChanges();



    //}


}
}

