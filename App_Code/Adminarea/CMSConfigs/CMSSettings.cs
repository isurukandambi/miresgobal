﻿namespace CMSConfigs{
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
    using System.Web.Configuration;

/// <summary>
/// Summary description for CMSSettings
/// </summary>
public  class CMSSettings
{
	public CMSSettings()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    //get the web site title
    public static string SiteTitle
    {
        get
        {
            return WebConfigurationManager.AppSettings["SiteName"];
        }
    }

    public static string RyanairDPCarImagePath
    {
        get
        {
            return WebConfigurationManager.AppSettings["RyanairDPCarImageURLPath"];
        }
    }

    //get the default Car Image Path
    public static string CarImagePath
    {
        get
        {
            return WebConfigurationManager.AppSettings["CarImageURLPath"];
        }
    }

    public static string ThermeonCarImagePath
    {
        get
        {
            return WebConfigurationManager.AppSettings["ThermeonCarImageURLPath"];
        }
    }

    //get max vehicle limit for brand
    public static string MaxVehicleLimit
    {
        get
        {
            return WebConfigurationManager.AppSettings["MaxVehicleLimit"];
        }
    }

    public static string DebugXmlDetails
    {
        get
        {
            return WebConfigurationManager.AppSettings["DebugXML"];
        }

    }

    public static string DebugXmlDetailsNotifyEmails
    {
        get
        {
            return WebConfigurationManager.AppSettings["DebugXMLNotifyEmails"];
        }
    }

    public static string HttpType
    {
        get
        {
            return WebConfigurationManager.AppSettings["UseHTTPS"] == "yes" ? "https" : "http";
        }
    }



    public static string SiteEmailsSentFrom
    {
        get
        {
            return WebConfigurationManager.AppSettings["EmailsSentFrom"];
        }
    }

    //get the web site url
    public static string SiteUrl
    {
        get
        {
            // if running locally use the url specified in sitelink (allows localhost testing) otherwise take the host name in url (allows CNAME use)
            return WebConfigurationManager.AppSettings["RunSiteOnLocalHost"] == "Yes" ? WebConfigurationManager.AppSettings["LocalSiteLink"] : HttpContext.Current.Request.Url.Host;
        }
    }


    public static string connctionString
    {
        get
        {
            return WebConfigurationManager.AppSettings["connection"];
        }
    }

}

}