﻿namespace CMSUtils
{
    using System;
    using System.Collections.Generic;
    using System.Web;
    using System.Net.Mail;
    using System.IO;
    using System.Drawing;
    using System.Drawing.Imaging;
    using System.Drawing.Drawing2D;
    using System.Text.RegularExpressions;

    /// <summary>
    /// Send an email from the website.
    /// Expects: string, string, string, string.
    /// Returns: bool.
    /// </summary>
    public class CMSUtilities
    {
        public static bool SendMessage(string FromAddress, string ToAddress, string Subject, string EmailMessage, bool HTMLMessage)
        {
            bool MsgSent = false;
            try
            {
                MailMessage mailMessage = new MailMessage();
                mailMessage.From = new MailAddress(FromAddress);
                mailMessage.To.Add(new MailAddress(ToAddress));
                mailMessage.Subject = Subject;
                mailMessage.Body = EmailMessage;
                mailMessage.IsBodyHtml = HTMLMessage;
                SmtpClient smtpClient = new SmtpClient();
                smtpClient.Send(mailMessage);
                MsgSent = true;
            }
            catch
            {
                MsgSent = false;
            }
            return MsgSent;
        }


        public static string FKconstrainsFail()
        {
            string meaasge = "Operation Fail. This entry has already used.";
            return meaasge;
        }


        public static string GetPrettyDate(DateTime d)
        {
            // 1.
            // Get time span elapsed since the date.
            TimeSpan s = DateTime.Now.Subtract(d);

            // 2.
            // Get total number of days elapsed.
            var dayDiff = (int)s.TotalDays;

            // 3.
            // Get total number of seconds elapsed.
            var secDiff = (int)s.TotalSeconds;

            // 4.
            // If a future date, just print out the date.
            if (d > DateTime.Now)
            {
                return d.ToString("f");
            }

            // 5.
            // Handle same-day times.
            if (dayDiff == 0)
            {
                // A.
                // Less than one minute ago.
                if (secDiff < 60)
                {
                    return "just now";
                }
                // B.
                // Less than 2 minutes ago.
                if (secDiff < 120)
                {
                    return "1 minute ago";
                }
                // C.
                // Less than one hour ago.
                if (secDiff < 3600)
                {
                    return string.Format("{0} minutes ago",
                        Math.Floor((double)secDiff / 60));
                }
                // D.
                // Less than 2 hours ago.
                if (secDiff < 7200)
                {
                    return "1 hour ago";
                }
                // E.
                // Less than one day ago.
                if (secDiff < 86400)
                {
                    return string.Format("{0} hours ago",
                        Math.Floor((double)secDiff / 3600));
                }
            }
            // 6.
            // Handle previous days.
            if (dayDiff == 1)
            {
                return "yesterday";
            }
            if (dayDiff < 7)
            {
                return string.Format("{0} days ago",
                    dayDiff);
            }
            if (dayDiff < 31)
            {
                return string.Format("{0} weeks ago",
                    Math.Ceiling((double)dayDiff / 7));
            }
            if (dayDiff < 365)
            {
                return string.Format("{0} months ago",
                    Math.Ceiling((double)dayDiff / 30));
            }
            if (dayDiff < 730)
            {
                return "Over 1 year ago";
            }
            if (dayDiff > 730)
            {
                return string.Format("Over {0} years ago",
                    Math.Ceiling((double)dayDiff / 730));
            }
            return null;
        }

        public static void Resize(string filePath, int maxWidth, int maxHeight, bool resizeByHeight, Stream buffer)
        {
            Image image = Image.FromStream(buffer);

            float origImgWidth = image.PhysicalDimension.Width;
            float origImgHeight = image.PhysicalDimension.Height;
            float imgWidth = origImgWidth, imgHeight = origImgHeight;
            float imgSize = imgWidth;
            float imgResize = imgSize <= maxWidth ? (float)1.0 : maxWidth / imgSize;
            if ((imgHeight > imgWidth) || (resizeByHeight))
            {
                imgSize = imgHeight;
                imgResize = imgSize <= maxWidth ? (float)1.0 : maxHeight / imgSize;
            }
            imgWidth *= imgResize;
            imgHeight *= imgResize;

            //below code
            ImageCodecInfo codec = ImageCodecInfo.GetImageEncoders()[1];
            var eParams = new EncoderParameters(1);
            eParams.Param[0] = new EncoderParameter(Encoder.Quality, 80L);
            var bmp = new Bitmap((int)imgWidth, (int)imgHeight);
            var gr = Graphics.FromImage(bmp);
            gr.SmoothingMode = SmoothingMode.HighQuality;
            gr.CompositingQuality = CompositingQuality.HighQuality;
            gr.InterpolationMode = InterpolationMode.High;
            var rectDestination = new Rectangle(0, 0, (int)imgWidth, (int)imgHeight);
            gr.DrawImage(image, rectDestination, 0, 0, origImgWidth, origImgHeight, GraphicsUnit.Pixel);
            bmp.Save(filePath, codec, eParams);
            bmp.Dispose();
            image.Dispose();
        }


        public static bool ImageChecker(string filename, Stream buffer)
        {
            try
            {
                using (var image = Image.FromStream(buffer))
                {
                    return true;
                }
            }
            catch (ArgumentException)
            {
                //throws exception if not valid image
            }
            return false;
        }


        public static bool FilenameChecker(string testName)
        {
            var containsABadCharacter = new Regex("[" + Path.GetInvalidFileNameChars() + "]");
            if (containsABadCharacter.IsMatch(testName)) { return false; }
            return true;
        }



    }
}
