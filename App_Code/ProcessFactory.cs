﻿using System;
using Exceptions;

/// <summary>
/// Summary description for RequestEngine
/// </summary>
public class ProcessFactory
{

    public IProcess createProcess(XMLAPITypes xmlAPIType, string payload)
    {
        switch (xmlAPIType)
        {
            case XMLAPITypes.AvailabilityRequest:
                GenericProcess availabilityProcess = new VehicleAvailabilityProcess();
                return availabilityProcess;
            case XMLAPITypes.ReservationRequest:
                GenericProcess reservationProcess = new VehicleReservationProcess();
                return reservationProcess;
            case XMLAPITypes.RetrivalRequest:
                ReservationRetrievalProcess retrivalProcess = new ReservationRetrievalProcess();
                return retrivalProcess;
            case XMLAPITypes.ModificationRequest:
                BookingModifyProcess modificationProcess = new BookingModifyProcess();
                return modificationProcess;
            case XMLAPITypes.CancellationRequest:
                BookingCancellationProcess cancelProcess = new BookingCancellationProcess();
                return cancelProcess;
            case XMLAPITypes.RentalTermsRequest:
                GenericProcess rentalProcess = new CT_RentalTermsProcess();
                return rentalProcess;
            default: throw new InvalidRequestException("Unsupported xml received");
        }
    }
}