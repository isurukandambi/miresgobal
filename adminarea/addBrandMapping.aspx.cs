﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ORM;
using CMSConfigs;

public partial class addBrandMapping : System.Web.UI.Page
{
    private DatabaseEntities db;
    protected void Page_Load(object sender, EventArgs e)
    {
        Master.Page.Title = CMSSettings.SiteTitle + " - " + PageName.Text;
        if (Session["LoggedInName"] == null)
        {
            Response.Redirect("default.aspx?TimeOut=y&ReturnPage=" + Server.UrlEncode(HttpContext.Current.Request.Url.AbsoluteUri));
        }
        if (!Page.IsPostBack)
        {
            int maxVehicleLimit = Convert.ToInt32(CMSSettings.MaxVehicleLimit);
            AdvisorText.InnerHtml = "Brands (Select brands and vehicle limit. (total vehicle limit of all brands not exceed " + maxVehicleLimit + "))";
            LoadPartners();
            LoadCountries();

        }
        LoadBrands();
    }
    protected int countryID()
    {
        int rowID;
        bool result = int.TryParse(Request.QueryString["Country"], out rowID);
        if (result)
        {
            return rowID;
        }
        return 0;
    }

    protected int partnerID()
    {
        int rowID;
        bool result = int.TryParse(Request.QueryString["Partner"], out rowID);
        if (result)
        {
            return rowID;
        }
        return 0;
    }
    private string checkFormIsValid()
    {
        string errorMsg = "";
        int maxVehicleLimit = Convert.ToInt32(CMSSettings.MaxVehicleLimit);
        //check country was selected
        if (CountryIDFK.SelectedValue == "0")
        {
            errorMsg = "<li>Please select Country</li>";
        }

        if (PartnerIDFK.SelectedValue == "0")
        {
            errorMsg = "<li>Please select Partner</li>";
        }
        //check one or more locations were selected from checkbox list
        if (Locationsddl.Items.Cast<ListItem>().Where(l => l.Selected && l.Enabled == true).Count() == 0)
        {
            errorMsg += "<li>Please select one or many Locations</li>";
        }
        //check brand checked from brand list
        if (BrandsPanel.Controls.OfType<Panel>().Where(p => (p.Controls.OfType<CheckBox>().Where(c => c.Checked == true).Count() > 0)).Count() == 0)
        {
            errorMsg += "<li>Please select one or many Brands</li>";
        }

        var selectedTotalLimit = (from d in BrandsPanel.Controls.OfType<Panel>()
                                  where ((CheckBox)d.Controls[0]).Checked == true
                                  group d by ((DropDownList)d.Controls[1]).Visible into t
                                  select new
                                  {
                                      TotalLimit = t.Sum(f => Convert.ToInt32(((DropDownList)f.Controls[1]).SelectedValue))
                                  });
        ////check total limit of all brands more than 9
        if (selectedTotalLimit.Count() > 0)
        {
            if (selectedTotalLimit.First().TotalLimit > maxVehicleLimit)
            {
                errorMsg += "<li>Total vehicle limit from all brands may not exceed " + maxVehicleLimit + "</li>";
            }
        }

        return errorMsg;
    }
    protected void AddButton_Click(object sender, EventArgs e)
    {

        MsgToShow.Visible = false;
        string errorMsg = checkFormIsValid();
        if (string.IsNullOrEmpty(errorMsg))
        {
            try
            {
                int countryID = Convert.ToInt32(CountryIDFK.SelectedValue);
                int partnerID = Convert.ToInt32(PartnerIDFK.SelectedValue);

                var locationItems = Locationsddl.Items.OfType<ListItem>().Where(l => l.Selected == true && l.Enabled == true);

                var checkedBrands = (from d in BrandsPanel.Controls.OfType<Panel>()
                                     where ((CheckBox)d.Controls[0]).Checked == true
                                     select new
                                     {
                                         BrandID = Convert.ToInt32(((CheckBox)d.Controls[0]).ID),
                                         VehicleLimit = Convert.ToInt32(((DropDownList)d.Controls[1]).SelectedValue),
                                         Cargroups = ((CheckBoxList)((Panel)d.Controls[3]).Controls[0]).Items.OfType<ListItem>().Where(f => f.Selected == true)
                                     });
                foreach (var locItem in locationItems)
                {
                    int locID = Convert.ToInt32(locItem.Value);
                    foreach (var brand in checkedBrands)
                    {
                        BrandLocation brandLocation = new BrandLocation
                        {
                            LocationIDFK = locID,
                            BrandIDFK = brand.BrandID,
                            CountryIDFK = countryID,
                            VehicleLimit = brand.VehicleLimit,
                            PartnerIDFK = partnerID
                        };
                        db.BrandLocations.Add(brandLocation);
                        db.SaveChanges();

                        foreach (var carGroup in brand.Cargroups)
                        {
                            int carGroupID = Convert.ToInt32(carGroup.Value);
                            CarGroupBrandMapping cgbm = new CarGroupBrandMapping
                            {
                                BrandLocationIDFK = brandLocation.BrandLocationID,
                                CarGroupIDFK = carGroupID
                            };
                            db.CarGroupBrandMappings.Add(cgbm);
                        }
                        db.SaveChanges();
                    }
                }
                Response.Redirect("manageBrandMapping.aspx?MsgType=2&MsgToShow=Brands mapping for locations added successfully&Country=" + countryID);
            }
            catch (Exception ex)
            {
                MsgToShow.Show(MyMessageBox.MessageType.Error, "Unexepected Error occured while processing." + ex.Message + ex.StackTrace);
                MsgToShow.Visible = true;
            }
        }
        else
        {
            MsgToShow.Show(MyMessageBox.MessageType.Error, "<ul>" + errorMsg + "</ul>");
        }

    }
    protected void AutoSelectExistingLocations()
    {
        var locationItems = Locationsddl.Items.OfType<ListItem>().OrderBy(l => l.Text);
        var checkedBrands = (from d in BrandsPanel.Controls.OfType<Panel>().Select(c => c.Controls.OfType<CheckBox>())
                             where d.Where(c => c.Checked == true).Count() > 0
                             select new
                             {
                                 BrandID = Convert.ToInt32(d.First().ID)
                             });
        foreach (var locItem in locationItems)
        {
            int locID = Convert.ToInt32(locItem.Value);
            foreach (var brand in checkedBrands)
            {
                if (db.BrandLocations.Where(b => b.LocationIDFK == locID && b.BrandIDFK == brand.BrandID).Count() > 0)
                {
                    locItem.Selected = true;
                    locItem.Enabled = false;
                }
                else
                {
                    locItem.Selected = false;
                    locItem.Enabled = true;
                }
            }
        }
    }
    protected void LoadCountries()
    {
        db = new DatabaseEntities();
        CountryIDFK.DataSource = db.Countries.OrderBy(c => c.CountryName).ToList();
        CountryIDFK.DataValueField = "CountryID";
        CountryIDFK.DataTextField = "CountryName";
        CountryIDFK.DataBind();
        CountryIDFK.Items.Insert(0, new ListItem("Select Country", "0"));
        CountryIDFK.SelectedValue = countryID().ToString();
        //LoadLocations(countryID(), Convert.ToInt32(PartnerIDFK.SelectedValue));
    }
    protected void LoadPartners()
    {

        db = new DatabaseEntities();
        PartnerIDFK.DataSource = db.Partners.OrderBy(b => b.DomainName).ToList();
        PartnerIDFK.DataTextField = "DomainName";
        PartnerIDFK.DataValueField = "PartnerId";
        PartnerIDFK.DataBind();
        PartnerIDFK.Items.Insert(0, new ListItem("All Partners", "0"));
        // PartnerIDFK.SelectedValue = PartnerID().ToString();
    }

    protected void CountryDDL_Changed(object sender, EventArgs e)
    {
        if (Convert.ToInt32(PartnerIDFK.SelectedValue) == 0)
        {
            MsgToShow.Show(MyMessageBox.MessageType.Error, "<p>First select partner.</p>");

        }
        else
        {
            LoadLocations(Convert.ToInt32(CountryIDFK.SelectedValue), Convert.ToInt32(PartnerIDFK.SelectedValue));
        }

    }

    protected void PartnerDDL_Changed(object sender, EventArgs e)
    {
        // LoadLocations(Convert.ToInt32(CountryIDFK.SelectedValue));
        if (Convert.ToInt32(PartnerIDFK.SelectedValue) == 0)
        {
            MsgToShow.Show(MyMessageBox.MessageType.Error, "<p>First select partner.</p>");

        }
        else
        {
            MsgToShow.Visible = false;
            CountryIDFK.SelectedIndex = 0;
            LoadLocations(Convert.ToInt32(CountryIDFK.SelectedValue), Convert.ToInt32(PartnerIDFK.SelectedValue));

        }
    }
    protected void BrandChecked_Changed(object sender, EventArgs e)
    {
        AutoSelectExistingLocations();
    }
    protected void LoadLocations(int countryID, int partnerId)
    {
        //int partnerId = Convert.ToInt32(PartnerIDFK.SelectedValue);
        Response.Write("Country " + countryID + "  Partner" + partnerId);
        if (countryID != 0 && partnerId != 0)
        {
            db = new DatabaseEntities();
            Locationsddl.DataSource = db.Locations.Where(l => l.CountryIDFK == countryID).OrderBy(l => l.LocationName).ToList();
            Locationsddl.DataValueField = "LocationID";
            Locationsddl.DataTextField = "LocationName";
            Locationsddl.DataBind();
            //AutoSelectExistingLocations();
            foreach (ListItem item in Locationsddl.Items)
            {
                int locID = Convert.ToInt32(item.Value);
                bool isExisting = (db.BrandLocations.Where(b => b.LocationIDFK == locID && b.PartnerIDFK == partnerId).Count() > 0);
                item.Selected = isExisting;
                item.Enabled = !isExisting;
            }
        }
    }


    protected void LoadBrands()
    {
        db = new DatabaseEntities();
        var brands = db.Brands.OrderBy(b => b.BrandName);
        int maxVehicleLimit = Convert.ToInt32(CMSSettings.MaxVehicleLimit);
        foreach (var brand in brands)
        {
            //adding main panel for all controls
            Panel controlsPanel = new Panel();
            controlsPanel.CssClass = "BrandHolderControlPanel";

            //Brand Checkbox
            CheckBox brandCheckBox = new CheckBox();
            brandCheckBox.CssClass = "BrandIndicatorCheckBox";
            brandCheckBox.Text = brand.BrandName;
            brandCheckBox.AutoPostBack = true;

            //validate locations by brand when selecting brands
            //brandCheckBox.CheckedChanged += new EventHandler(this.BrandChecked_Changed);
            brandCheckBox.ID = brand.BrandID.ToString();

            //Add Vehicle limit selct box
            DropDownList vehLimitddl = new DropDownList();
            vehLimitddl.CssClass = "VehicleLimitddl";
            vehLimitddl.ID = "VehicleLimitDDL" + brand.BrandID;
            vehLimitddl.Items.Insert(0, new ListItem("Select Vehicle Limit", "0"));

            //Populate Vehicle limit dropdown list
            for (int i = maxVehicleLimit; i >= 1; i--)
            {
                vehLimitddl.Items.Insert(1, new ListItem(i.ToString(), i.ToString()));
            }
            vehLimitddl.SelectedValue = "0";

            //Add vehicle group title for checkbox list
            Label vehicleGrpsLabel = new Label();
            vehicleGrpsLabel.Text = "<input type=\"checkbox\" class=\"toggleAllCarGroups\" style=\"margin-left: 6%;\" checked=\"true\" /> <b>Vehicle Groups</b>";

            //Add and populate Car groups
            CheckBoxList vehicleGroupsList = new CheckBoxList();
            vehicleGroupsList.DataSource = db.CarGroups.OrderBy(cg => cg.CarCode).ToList();
            vehicleGroupsList.DataValueField = "CarGroupID";
            vehicleGroupsList.DataTextField = "CarCode";
            vehicleGroupsList.DataBind();

            foreach (ListItem item in vehicleGroupsList.Items)
            {
                item.Selected = true;
            }

            //Add car groups to panel to add overflow scroll behavior to checkbox list
            Panel overflowPanel = new Panel();
            overflowPanel.CssClass = "checkboxListsInBrandPanel";
            overflowPanel.Controls.Add(vehicleGroupsList);

            //Adding All Controls and panels to Main Brandpanel
            controlsPanel.Controls.Add(brandCheckBox);
            controlsPanel.Controls.Add(vehLimitddl);
            controlsPanel.Controls.Add(vehicleGrpsLabel);
            controlsPanel.Controls.Add(overflowPanel);
            BrandsPanel.Controls.Add(controlsPanel);
        }
    }
}