﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.HtmlControls;
using ORM;
using CMSConfigs;
using CMSUtils;

public partial class addAccessory : System.Web.UI.Page
{
    private DatabaseEntities db;

    protected void Page_Load(object sender, EventArgs e)
    {
        Master.Page.Title = CMSSettings.SiteTitle + " - " + PageName.Text;
        if (Session["LoggedInName"] == null)
        {
            Response.Redirect("default.aspx?TimeOut=y&ReturnPage=" + Server.UrlEncode(HttpContext.Current.Request.Url.AbsoluteUri));
        }
        if (!Page.IsPostBack)
        {
            //LoadCountries();
            //LoadLanguages();
            LoadWithExistingAccessory();
        }
    }
    protected int rowID()
    {
        int rowID;
        bool result = int.TryParse(Request.QueryString["Row"], out rowID);
        if (result)
        {
            return rowID;
        }
        return 0;
    }
    protected void LoadWithExistingAccessory()
    {
        int row = rowID();
        if (row != 0)
        {
            db = new DatabaseEntities();
            var accessory = (from a in db.Accessories
                             where a.AccessoryID == row
                             select new
                             {
                                 a.AccessoryID,
                                 a.Image1,
                                 a.Title,
                                 a.Price,
                                 a.Details,
                                 a.HertzCode
                             });
            //var accessoryInfo = db.AccessoryInfoes.SingleOrDefault(ai => ai.AccessoryIDFK == row);
            if (accessory != null)
            {
                Title.Text = accessory.FirstOrDefault().Title;
                //CountryID.SelectedValue = accessory.CountryIDFK.ToString();
                //LanguageID.SelectedValue = accessory.LanguageIDFK.ToString();
                //LinkTitle.Text = accessoryInfo.Title;
                //LinkDetails.Text = accessoryInfo.Details;
                LinkDetails.Text = accessory.FirstOrDefault().Details;
                Price.Text = accessory.FirstOrDefault().Price;
                HertzCode.Text = accessory.FirstOrDefault().HertzCode;
                //VisibleOffer.Checked = Convert.ToBoolean(accessory.VisibleItem);
                //DefaultItem.Checked = Convert.ToBoolean(accessory.DefaultItem);
                OldImage.Value = accessory.FirstOrDefault().Image1;

                if (!string.IsNullOrEmpty(OldImage.Value))
                {
                    CurrentImage.Text = "<img src=\"../Pics/Accessories/Original/" + accessory.FirstOrDefault().Image1 + "\" />";
                }
                else
                {
                    CurrentImage.Text = "no image uploaded";
                }
            }
            else
            {
                hideCurrentImageControls();
            }
        }
        else
        {
            hideCurrentImageControls();
        }
    }
    protected void hideCurrentImageControls() {
        CurrentImageText.Visible = false;
        CurrentImageControlsHolder.Visible = false;
    }
    //protected void LoadCountries()
    //{
    //    db = new DatabaseEntities();
    //    CountryID.DataSource = db.Countries.OrderBy(c => c.CountryName);
    //    CountryID.DataValueField = "CountryID";
    //    CountryID.DataTextField = "CountryName";
    //    CountryID.DataBind();
    //}

    //protected void LoadLanguages()
    //{
    //    db = new DatabaseEntities();
    //    LanguageID.DataSource = db.Languages.OrderBy(la => la.LanguageName);
    //    LanguageID.DataValueField = "LanguageID";
    //    LanguageID.DataTextField = "LanguageName";
    //    LanguageID.DataBind();
    //}

    protected void AddButton_Click(object sender, EventArgs e)
    {
        try
        {
            // initialize the objectContext
            db = new DatabaseEntities();
            string MsgToShowText = "";
            string filename = "";
            string appPath = Request.PhysicalApplicationPath;
            string FilePathOrig = "";
            string UpPath = appPath + "\\Pics\\Accessories\\Original\\";
            if (!Directory.Exists(UpPath))
            {
                Directory.CreateDirectory(UpPath);
            }

            bool ErrorsWithFile = false;
            //start logic for Image 1
            if (filPhoto.PostedFile.ContentLength == 0)
            {
                
                filename = "";
                if (!string.IsNullOrEmpty(OldImage.Value))
                {
                    filename = OldImage.Value;
                }
            }
            else if ((filPhoto.PostedFile != null) && (filPhoto.PostedFile.ContentLength > 0))
            {
                filename = cleanFilePath(filPhoto);

                //check filename does not contain any illegal character
                if (!CMSUtilities.FilenameChecker(filename))
                {
                    MsgToShowText += "The uploaded file is not a valid file type.<br />";
                    ErrorsWithFile = true;
                }
                //check file is a valid image file and correct extension
                System.Text.RegularExpressions.Regex imageFilenameRegex = new System.Text.RegularExpressions.Regex(@"(.*?)\.(jpg|jpeg|png|gif)$");
                if ((!CMSUtilities.ImageChecker(filename, filPhoto.PostedFile.InputStream)) || (!imageFilenameRegex.IsMatch(filename)))
                {
                    MsgToShowText += "The file uploaded is not a valid image file.<br />";
                    ErrorsWithFile = true;
                }
                //check file is not too big
                if (filPhoto.PostedFile.ContentLength > 3145728) //3MB
                {
                    MsgToShowText += "Image too big. Try again.<br />";
                    ErrorsWithFile = true;
                }
                //check filename does not already exist
                if (File.Exists(UpPath + filename))
                {
                    MsgToShowText += "An image with the same filename already exists. Please rename your file before uploading.<br />";
                    ErrorsWithFile = true;
                }

                //if everything above is ok save the image
                if (ErrorsWithFile == false)
                {
                    FilePathOrig = UpPath + filename;
                    CMSUtilities.Resize(FilePathOrig, 140, 80, false, filPhoto.PostedFile.InputStream);
                }
            }
            //save details
            if (ErrorsWithFile == false)
            {
                //int selectedCountryID = Convert.ToInt32(CountryID.SelectedValue);
                //int selectedLangID = Convert.ToInt32(LanguageID.SelectedValue);
                ORM.Accessory accessory = new ORM.Accessory
                {
                    Title = Title.Text.Trim(),
                    //CountryIDFK = selectedCountryID,
                    //LanguageIDFK = selectedLangID,
                    Price = Price.Text,
                    Details = LinkDetails.Text,
                    HertzCode = HertzCode.Text,
                    Image1 = filename,
                    Thermeon_Code = ThermeonCode.Text
                   // CountryCountryID = 1,
                   // LanguageLanguageID = 1
                   // VisibleItem = VisibleOffer.Checked,
                   // Approved = true,
                   // Advantage = false,
                    //DefaultItem = DefaultItem.Checked
                };
                db.Accessories.Add(accessory);
                //AccessoryInfo accessoryInfo = new AccessoryInfo
                //{
                //    AccessoryIDFK = accessory.AccessoryID,
                //    Details = LinkDetails.Text,
                //    Title = LinkTitle.Text
                //};
                //db.AccessoryInfoes.AddObject(accessoryInfo);
                db.SaveChanges();
                Response.Redirect("manageAccessories.aspx?MsgType=2&MsgToShow='" + Title.Text + "' added successfully");
            }
            else
            {
                MsgToShow.Show(MyMessageBox.MessageType.Warning, MsgToShowText);
            }

        }
        catch(Exception ex)
        {
            MsgToShow.Show(MyMessageBox.MessageType.Error, "There was an error adding this information. Please try again."+ex.Message);
            MsgToShow.Visible = true;
        }
    }
    protected string cleanFilePath(HtmlInputFile fileInfo)
    {
        string fileName = Path.GetFileNameWithoutExtension(fileInfo.PostedFile.FileName);
        string fileExtension = Path.GetExtension(fileInfo.PostedFile.FileName);
        fileName = Server.HtmlEncode(fileName);
        fileName = fileName + DateTime.Now;
        fileName = fileName.Replace(" ", "");
        fileName = fileName.Replace("/", "");
        fileName = fileName.Replace(":", "");
        fileName = fileName.Replace("&", "");
        fileName = fileName.ToLower();
        fileName = fileName + fileExtension;
        return fileName;
    }
}