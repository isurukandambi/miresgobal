﻿<%@ Page Language="C#" MasterPageFile="adminarea.master" AutoEventWireup="true" CodeFile="addLocation.aspx.cs" Inherits="addLocation" %>

<%@ Register Src="~/adminarea/controls/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="js/validationEngine/css/validationEngine.jquery.css" rel="stylesheet" type="text/css" />
    <script src="js/validationEngine/jquery.validationEngine.js" type="text/javascript"></script>
    <script src="js/validationEngine/jquery.validationEngine-en.js" type="text/javascript"></script>
    <script type="text/javascript" language="javascript">
        $(document).ready(function () {
            $("#form1").validationEngine();
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="content">
        <uc1:MyMessageBox ID="MsgToShow" runat="server" ShowCloseButton="true" Visible="false" />

        <h3>
            <asp:Literal ID="PageName" runat="server" Text="Add Location" /></h3>
        <div id="actionArea">
            <fieldset>
                <div>
                    <label for="CountryID" class="inline">Country </label>
                    <asp:DropDownList ID="CountryID" runat="server">
                    </asp:DropDownList>
                </div>
                
                <table width="80%" align="center">
                  <tr>
                    <td>
                      <asp:Button Text="Mandetory Fields" BorderStyle="None" ID="ManFldPanel" CssClass="btn" runat="server"
                          OnClick="Tab1_Click" />
                      <asp:Button Text="Open and Close Time" BorderStyle="None" ID="OpenClosePanel" CssClass="btn" runat="server"
                          OnClick="Tab2_Click" />
                      <asp:Button Text="Section 1" BorderStyle="None" ID="Sec1" CssClass="btn" runat="server"
                          OnClick="Tab3_Click" />
                        <asp:Button Text="Section 2 - Options" BorderStyle="None" ID="Sec2" CssClass="btn" runat="server"
                          OnClick="Tab4_Click" />
                        <asp:Button Text="Section 3 - Notes" BorderStyle="None" ID="Sec3" CssClass="btn" runat="server"
                          OnClick="Tab5_Click" />
                      <asp:MultiView ID="MainView" runat="server">
                        <asp:View ID="View1" runat="server">
                          <table style="width: 100%; border-width: 1px; border-color: #666; border-style: solid">
                            <tr>
                              <td>
                                 <div>
                                    <label for="LocationName" class="inline">Location Name </label>
                                    <asp:TextBox ID="LocationName" CssClass="text inline" runat="server" MaxLength="255"></asp:TextBox>
                                </div>
                                <div>
                                    <label for="LocationCode" class="inline">Location Code </label>
                                    <asp:TextBox ID="LocationCode" CssClass="text inline" runat="server" MaxLength="10"></asp:TextBox>
                                </div>
                                <div>
                                    <label for="ExtendedLocationCode" class="inline">Extended Location Code </label>
                                    <asp:TextBox ID="ExtendedLocationCode" CssClass="text inline" runat="server" MaxLength="10"></asp:TextBox>
                                </div>
                                <div>
                                    <label for="City" class="inline">City</label>
                                    <asp:TextBox ID="City" CssClass="text inline" runat="server" MaxLength="255"></asp:TextBox>
                                </div>
                                <div>
                                    <label for="Address1" class="inline">Address Line 1</label>
                                    <asp:TextBox ID="Address1" CssClass="text inline" runat="server" MaxLength="255"></asp:TextBox>
                                </div>
                                <div>
                                    <label for="Address2" class="inline">Address Line 2</label>
                                    <asp:TextBox ID="Address2" CssClass="text inline" runat="server" MaxLength="255"></asp:TextBox>
                                </div>
                                <div>
                                    <label for="Address3" class="inline">Address Line 3</label>
                                    <asp:TextBox ID="Address3" CssClass="text inline" runat="server" MaxLength="255"></asp:TextBox>
                                </div>
                                <div>
                                    <label for="PostCode" class="inline">Zip/Post Code</label>
                                    <asp:TextBox ID="PostCode" CssClass="text inline" runat="server" MaxLength="30"></asp:TextBox>
                                </div>
                                <div>
                                    <label for="Phone" class="inline">Telephone</label>
                                    <asp:TextBox ID="Phone" CssClass="text inline" runat="server" MaxLength="30"></asp:TextBox>
                                </div>
                                <div>
                                    <label for="Latitude" class="inline">Latitude</label>
                                    <asp:TextBox ID="Latitude" CssClass="text inline" runat="server" MaxLength="50"></asp:TextBox>
                                </div>
                                <div>
                                    <label for="Longitude" class="inline">Longitude</label>
                                    <asp:TextBox ID="Longitude" CssClass="text inline" runat="server" MaxLength="50"></asp:TextBox>
                                </div>
                                <div>
                                    <label for="CarTrawlerLocCode" class="inline">CarTrawler Location Code </label>
                                    <asp:TextBox ID="CarTrawlerLocCode" CssClass="text inline" runat="server" MaxLength="10"></asp:TextBox>
                                </div>
                                <div>
                                    <label for="ThermeonLocCode" class="inline">Thermeon Location Code </label>
                                    <asp:TextBox ID="ThermeonLocCode" CssClass="text inline" runat="server" MaxLength="10"></asp:TextBox>
                                </div>
                                <div>
                                    <label for="LocationType" class="inline">Location Type </label>
                                    <asp:DropDownList ID="LocationType" runat="server">
                                        <asp:ListItem Value="A" Text="Airport"></asp:ListItem>
                                        <asp:ListItem Value="D" Text="Downtown"></asp:ListItem>
                                        <asp:ListItem Value="R" Text="Rail"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                                <div class="cr">
                                    <label>
                                        <asp:CheckBox ID="HertzAdvantage" Text="Tick if this is a Hertz Advantage location." runat="server" />
                                    </label>
                                </div>
                              </td>
                            </tr>
                          </table>
                        </asp:View>
                        <asp:View ID="View2" runat="server">
                          <table style="width: 100%; border-width: 1px; border-color: #666; border-style: solid">
                            <tr>
                              <td>
                                <div>
                                    <label for="Open1Mon" class="inline">Open 1 Mon</label>
                                    <asp:TextBox ID="Open1Mon" CssClass="text inline" runat="server" MaxLength="30"></asp:TextBox>
                                </div>
                                <div>
                                    <label for="Close1Mon" class="inline">Close 1 Mon</label>
                                    <asp:TextBox ID="Close1Mon" CssClass="text inline" runat="server" MaxLength="30"></asp:TextBox>
                                </div>
                                <div>
                                    <label for="Open2Mon" class="inline">Open 2 Mon</label>
                                    <asp:TextBox ID="Open2Mon" CssClass="text inline" runat="server" MaxLength="30"></asp:TextBox>
                                </div>
                                <div>
                                    <label for="Close2Mon" class="inline">Close 2 Mon</label>
                                    <asp:TextBox ID="Close2Mon" CssClass="text inline" runat="server" MaxLength="30"></asp:TextBox>
                                </div>
                                <div>
                                    <label for="Open3Mon" class="inline">Open 3 Mon</label>
                                    <asp:TextBox ID="Open3Mon" CssClass="text inline" runat="server" MaxLength="30"></asp:TextBox>
                                </div>
                                <div>
                                    <label for="Close3Mon" class="inline">Close 3 Mon</label>
                                    <asp:TextBox ID="Close3Mon" CssClass="text inline" runat="server" MaxLength="30"></asp:TextBox>
                                </div>
                                <div>
                                    <label for="Open1Tue" class="inline">Open 1 Tue</label>
                                    <asp:TextBox ID="Open1Tue" CssClass="text inline" runat="server" MaxLength="30"></asp:TextBox>
                                </div>
                                <div>
                                    <label for="Close1Tue" class="inline">Close 1 Tue</label>
                                    <asp:TextBox ID="Close1Tue" CssClass="text inline" runat="server" MaxLength="30"></asp:TextBox>
                                </div>
                                <div>
                                    <label for="Open2Tue" class="inline">Open 2 Tue</label>
                                    <asp:TextBox ID="Open2Tue" CssClass="text inline" runat="server" MaxLength="30"></asp:TextBox>
                                </div>
                                <div>
                                    <label for="Close2Tue" class="inline">Close 2 Tue</label>
                                    <asp:TextBox ID="Close2Tue" CssClass="text inline" runat="server" MaxLength="30"></asp:TextBox>
                                </div>
                                <div>
                                    <label for="Open3Tue" class="inline">Open 3 Tue</label>
                                    <asp:TextBox ID="Open3Tue" CssClass="text inline" runat="server" MaxLength="30"></asp:TextBox>
                                </div>
                                <div>
                                    <label for="Close3Tue" class="inline">Close 3 Tue</label>
                                    <asp:TextBox ID="Close3Tue" CssClass="text inline" runat="server" MaxLength="30"></asp:TextBox>
                                </div>
                                <div>
                                    <label for="Open1Wed" class="inline">Open 1 Wed</label>
                                    <asp:TextBox ID="Open1Wed" CssClass="text inline" runat="server" MaxLength="30"></asp:TextBox>
                                </div>
                                <div>
                                    <label for="Close1Wed" class="inline">Close 1 Wed</label>
                                    <asp:TextBox ID="Close1Wed" CssClass="text inline" runat="server" MaxLength="30"></asp:TextBox>
                                </div>
                                <div>
                                    <label for="Open2Wed" class="inline">Open 2 Wed</label>
                                    <asp:TextBox ID="Open2Wed" CssClass="text inline" runat="server" MaxLength="30"></asp:TextBox>
                                </div>
                                <div>
                                    <label for="Close2Wed" class="inline">Close 2 Wed</label>
                                    <asp:TextBox ID="Close2Wed" CssClass="text inline" runat="server" MaxLength="30"></asp:TextBox>
                                </div>
                                <div>
                                    <label for="Open3Wed" class="inline">Open 3 Wed</label>
                                    <asp:TextBox ID="Open3Wed" CssClass="text inline" runat="server" MaxLength="30"></asp:TextBox>
                                </div>
                                <div>
                                    <label for="Close3Wed" class="inline">Close 3 Wed</label>
                                    <asp:TextBox ID="Close3Wed" CssClass="text inline" runat="server" MaxLength="30"></asp:TextBox>
                                </div>
                                <div>
                                    <label for="Open1Thu" class="inline">Open 1 Thu</label>
                                    <asp:TextBox ID="Open1Thu" CssClass="text inline" runat="server" MaxLength="30"></asp:TextBox>
                                </div>
                                <div>
                                    <label for="Close1Thu" class="inline">Close 1 Thu</label>
                                    <asp:TextBox ID="Close1Thu" CssClass="text inline" runat="server" MaxLength="30"></asp:TextBox>
                                </div>
                                <div>
                                    <label for="Open2Thu" class="inline">Open 2 Thu</label>
                                    <asp:TextBox ID="Open2Thu" CssClass="text inline" runat="server" MaxLength="30"></asp:TextBox>
                                </div>
                                <div>
                                    <label for="Close2Thu" class="inline">Close 2 Thu</label>
                                    <asp:TextBox ID="Close2Thu" CssClass="text inline" runat="server" MaxLength="30"></asp:TextBox>
                                </div>
                                <div>
                                    <label for="Open3Thu" class="inline">Open 3 Thu</label>
                                    <asp:TextBox ID="Open3Thu" CssClass="text inline" runat="server" MaxLength="30"></asp:TextBox>
                                </div>
                                <div>
                                    <label for="Close3Thu" class="inline">Close 3 Thu</label>
                                    <asp:TextBox ID="Close3Thu" CssClass="text inline" runat="server" MaxLength="30"></asp:TextBox>
                                </div>
                                <div>
                                    <label for="Open1Fri" class="inline">Open 1 Fri</label>
                                    <asp:TextBox ID="Open1Fri" CssClass="text inline" runat="server" MaxLength="30"></asp:TextBox>
                                </div>
                                <div>
                                    <label for="Close1Fri" class="inline">Close 1 Fri</label>
                                    <asp:TextBox ID="Close1Fri" CssClass="text inline" runat="server" MaxLength="30"></asp:TextBox>
                                </div>
                                <div>
                                    <label for="Open2Fri" class="inline">Open 2 Fri</label>
                                    <asp:TextBox ID="Open2Fri" CssClass="text inline" runat="server" MaxLength="30"></asp:TextBox>
                                </div>
                                <div>
                                    <label for="Close2Fri" class="inline">Close 2 Fri</label>
                                    <asp:TextBox ID="Close2Fri" CssClass="text inline" runat="server" MaxLength="30"></asp:TextBox>
                                </div>
                                <div>
                                    <label for="Open3Fri" class="inline">Open 3 Fri</label>
                                    <asp:TextBox ID="Open3Fri" CssClass="text inline" runat="server" MaxLength="30"></asp:TextBox>
                                </div>
                                <div>
                                    <label for="Close3Fri" class="inline">Close 3 Fri</label>
                                    <asp:TextBox ID="Close3Fri" CssClass="text inline" runat="server" MaxLength="30"></asp:TextBox>
                                </div>
                                <div>
                                    <label for="Open1Sat" class="inline">Open 1 Sat</label>
                                    <asp:TextBox ID="Open1Sat" CssClass="text inline" runat="server" MaxLength="30"></asp:TextBox>
                                </div>
                                <div>
                                    <label for="Close1Sat" class="inline">Close 1 Sat</label>
                                    <asp:TextBox ID="Close1Sat" CssClass="text inline" runat="server" MaxLength="30"></asp:TextBox>
                                </div>
                                <div>
                                    <label for="Open2Sat" class="inline">Open 2 Sat</label>
                                    <asp:TextBox ID="Open2Sat" CssClass="text inline" runat="server" MaxLength="30"></asp:TextBox>
                                </div>
                                <div>
                                    <label for="Close2Sat" class="inline">Close 2 Sat</label>
                                    <asp:TextBox ID="Close2Sat" CssClass="text inline" runat="server" MaxLength="30"></asp:TextBox>
                                </div>
                                <div>
                                    <label for="Open3Sat" class="inline">Open 3 Sat</label>
                                    <asp:TextBox ID="Open3Sat" CssClass="text inline" runat="server" MaxLength="30"></asp:TextBox>
                                </div>
                                <div>
                                    <label for="Close3Sat" class="inline">Close 3 Sat</label>
                                    <asp:TextBox ID="Close3Sat" CssClass="text inline" runat="server" MaxLength="30"></asp:TextBox>
                                </div>
                                <div>
                                    <label for="Open1Sun" class="inline">Open 1 Sun</label>
                                    <asp:TextBox ID="Open1Sun" CssClass="text inline" runat="server" MaxLength="30"></asp:TextBox>
                                </div>
                                <div>
                                    <label for="Close1Sun" class="inline">Close 1 Sun</label>
                                    <asp:TextBox ID="Close1Sun" CssClass="text inline" runat="server" MaxLength="30"></asp:TextBox>
                                </div>
                                <div>
                                    <label for="Open2Sun" class="inline">Open 2 Sun</label>
                                    <asp:TextBox ID="Open2Sun" CssClass="text inline" runat="server" MaxLength="30"></asp:TextBox>
                                </div>
                                <div>
                                    <label for="Close2Sun" class="inline">Close 2 Sun</label>
                                    <asp:TextBox ID="Close2Sun" CssClass="text inline" runat="server" MaxLength="30"></asp:TextBox>
                                </div>
                                <div>
                                    <label for="Open3Sun" class="inline">Open 3 Sun</label>
                                    <asp:TextBox ID="Open3Sun" CssClass="text inline" runat="server" MaxLength="30"></asp:TextBox>
                                </div>
                                <div>
                                    <label for="Close3Sun" class="inline">Close 3 Sun</label>
                                    <asp:TextBox ID="Close3Sun" CssClass="text inline" runat="server" MaxLength="30"></asp:TextBox>
                                </div>
                              </td>
                            </tr>
                          </table>
                        </asp:View>
                        <asp:View ID="View3" runat="server">
                          <table style="width: 100%; border-width: 1px; border-color: #666; border-style: solid">
                            <tr>
                              <td>
                                <div>
                                    <label for="Preferred_OAG" class="inline">Preferred OAG Code</label>
                                    <asp:TextBox ID="Preferred_OAG" CssClass="text inline" runat="server" MaxLength="3"></asp:TextBox>
                                </div>
                                  <div>
                                    <label for="StateProviceCode" class="inline">State Provice Code</label>
                                    <asp:TextBox ID="StateProviceCode" CssClass="text inline" runat="server" MaxLength="2"></asp:TextBox>
                                </div>
                                  <div>
                                    <label for="IsoStateCode" class="inline">ISO State Province Code</label>
                                    <asp:TextBox ID="IsoStateCode" CssClass="text inline" runat="server" MaxLength="2"></asp:TextBox>
                                </div>
                                  <div>
                                    <label for="Number" class="inline">Number</label>
                                    <asp:TextBox ID="Number" CssClass="text inline" runat="server" MaxLength="2"></asp:TextBox>
                                </div>
                                  <div>
                                    <label for="AltPhone" class="inline">ALT Phone</label>
                                    <asp:TextBox ID="AltPhone" CssClass="text inline" runat="server" MaxLength="30"></asp:TextBox>
                                </div>
                                  <div>
                                    <label for="Fax" class="inline">Fax Number</label>
                                    <asp:TextBox ID="Fax" CssClass="text inline" runat="server" MaxLength="30"></asp:TextBox>
                                </div>
                                  <div>
                                    <label for="Telex" class="inline">TELEX</label>
                                    <asp:TextBox ID="Telex" CssClass="text inline" runat="server" MaxLength="30"></asp:TextBox>
                                </div>
                                  <div>
                                    <label for="Served_By" class="inline">Served By</label>
                                    <asp:TextBox ID="Served_By" CssClass="text inline" runat="server" MaxLength="2"></asp:TextBox>
                                </div>
                                  <div>
                                    <label for="Email_Add" class="inline">Email Address</label>
                                    <asp:TextBox ID="Email_Add" CssClass="text inline" runat="server" MaxLength="75"></asp:TextBox>
                                </div>
                                  <div>
                                    <label for="WebSite" class="inline">Web Site</label>
                                    <asp:TextBox ID="WebSite" CssClass="text inline" runat="server" MaxLength="75"></asp:TextBox>
                                </div>
                                  <div>
                                    <label for="Served_BY_Data" class="inline">Served By Data</label>
                                    <asp:TextBox ID="Served_BY_Data" CssClass="text inline" runat="server" MaxLength="255"></asp:TextBox>
                                </div>
                                  <div>
                                    <label for="Address4" class="inline">Address Line 4</label>
                                    <asp:TextBox ID="Address4" CssClass="text inline" runat="server" MaxLength="255"></asp:TextBox>
                                </div>
                                  <div>
                                    <label for="Address5" class="inline">Address Line 5</label>
                                    <asp:TextBox ID="Address5" CssClass="text inline" runat="server" MaxLength="600"></asp:TextBox>
                                </div>
                                  <div>
                                    <label for="CityAdd" class="inline">City Address</label>
                                    <asp:TextBox ID="CityAdd" CssClass="text inline" runat="server" MaxLength="255"></asp:TextBox>
                                </div>
                                  <div>
                                    <label for="Capital" class="inline">Capital</label>
                                    <asp:TextBox ID="Capital" CssClass="text inline" runat="server" MaxLength="50"></asp:TextBox>
                                </div>
                                  <div>
                                    <label for="StateProvineName" class="inline">State Provine Name</label>
                                    <asp:TextBox ID="StateProvineName" CssClass="text inline" runat="server" MaxLength="50"></asp:TextBox>
                                </div>
                                  <div>
                                    <label for="City_Variants" class="inline">City Variants</label>
                                    <asp:TextBox ID="City_Variants" CssClass="text inline" runat="server" MaxLength="500"></asp:TextBox>
                                </div>
                                  <div>
                                    <label for="Area" class="inline">Area</label>
                                    <asp:TextBox ID="Area" CssClass="text inline" runat="server" MaxLength="30"></asp:TextBox>
                                </div>
                                  <div>
                                      <br /><br />
                                    <label for="Locator_Code" class="inline">Locator Code (Y / N)</label>
                                    <asp:TextBox ID="Locator_Code" CssClass="text inline" runat="server" MaxLength="1"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="Locator_Code_Val" runat="server" CssClass="errorAsterik" ErrorMessage="Please enter n or y" ControlToValidate="Locator_Code" ValidationExpression="[yYnN]">*</asp:RegularExpressionValidator>
                                </div>
                                  <div>
                                    <label for="Top_Location" class="inline">Top Location (Y / N)</label>
                                    <asp:TextBox ID="Top_Location" CssClass="text inline" runat="server" MaxLength="1"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="Top_Location_Val" runat="server" CssClass="errorAsterik" ErrorMessage="Please enter n or y" ControlToValidate="Top_Location" ValidationExpression="[yYnN]">*</asp:RegularExpressionValidator>
                                </div>
                                  <div>
                                    <label for="Fbo" class="inline">FBO (Y / N)</label>
                                    <asp:TextBox ID="Fbo" CssClass="text inline" runat="server" MaxLength="1"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="Fbo_Val" runat="server" CssClass="errorAsterik" ErrorMessage="Please enter n or y" ControlToValidate="Fbo" ValidationExpression="[yYnN]">*</asp:RegularExpressionValidator>
                                </div>
                                  <div>
                                      <br /><br />
                                    <label for="Spec_Inst" class="inline">Special Instruction (Y / N)</label>
                                    <asp:TextBox ID="Spec_Inst" CssClass="text inline" runat="server" MaxLength="1"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="Spec_Inst_Val" runat="server" CssClass="errorAsterik" ErrorMessage="Please enter n or y" ControlToValidate="Spec_Inst" ValidationExpression="[yYnN]">*</asp:RegularExpressionValidator>
                                </div>
                                  <div>
                                    <label for="Spec_Inst_Data" class="inline">Special Instruction Data</label>
                                    <asp:TextBox ID="Spec_Inst_Data" CssClass="text inline" TextMode="multiline" runat="server" Columns="30" Rows="5"></asp:TextBox>
                                </div>
                                  <div>
                                      <br /><br />
                                    <label for="DoubleClickBanner" class="inline">Double Click Banner (Y / N)</label>
                                    <asp:TextBox ID="DoubleClickBanner" CssClass="text inline" runat="server" MaxLength="1"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="DoubleClickBanner_Val" runat="server" CssClass="errorAsterik" ErrorMessage="Please enter n or y" ControlToValidate="DoubleClickBanner" ValidationExpression="[yYnN]">*</asp:RegularExpressionValidator>
                                </div>
                                  <div>
                                    <label for="HZ_LocId" class="inline">HZ_LocID</label>
                                    <asp:TextBox ID="HZ_LocId" CssClass="text inline" runat="server" MaxLength="9"></asp:TextBox>
                                </div>
                                  <div>
                                    <label for="Counter_Bypass_Enable" class="inline">Counter Bypass Enable (Y / N)</label>
                                    <asp:TextBox ID="Counter_Bypass_Enable" CssClass="text inline" runat="server" MaxLength="1"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="Counter_Bypass_Enable_Val" runat="server" CssClass="errorAsterik" ErrorMessage="Please enter n or y" ControlToValidate="Counter_Bypass_Enable" ValidationExpression="[yYnN]">*</asp:RegularExpressionValidator>
                                </div>
                                  <div>
                                      <br /><br />
                                    <label for="Hod_LocId" class="inline">HOD LocID</label>
                                    <asp:TextBox ID="Hod_LocId" CssClass="text inline" runat="server" MaxLength="64"></asp:TextBox>
                                </div>
                                  <div>
                                    <label for="Hod_Image" class="inline">Hod_Image</label>
                                    <asp:TextBox ID="Hod_Image" CssClass="text inline" runat="server" MaxLength="30"></asp:TextBox>
                                </div>
                                  <div>
                                      <br /><br />
                                    <label for="Bookable" class="inline">Bookable (Y / N)</label>
                                    <asp:TextBox ID="Bookable" CssClass="text inline" runat="server" MaxLength="1"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="Bookable_Val" runat="server" CssClass="errorAsterik" ErrorMessage="Please enter n or y" ControlToValidate="Bookable" ValidationExpression="[yYnN]">*</asp:RegularExpressionValidator>
                                </div>
                                  <div>
                                    <label for="Major_AirPort" class="inline">Major AirPort (Y / N)</label>
                                    <asp:TextBox ID="Major_AirPort" CssClass="text inline" runat="server" MaxLength="1"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="Major_AirPort_Val" runat="server" CssClass="errorAsterik" ErrorMessage="Please enter n or y" ControlToValidate="Major_AirPort" ValidationExpression="[yYnN]">*</asp:RegularExpressionValidator>
                                </div>
                                  <div>
                                    <br /><br />
                                    <label for="Display_Add" class="inline">Display Address (Y / N)</label>
                                    <asp:TextBox ID="Display_Add" CssClass="text inline" runat="server" MaxLength="1"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="Display_Add_Val" runat="server" CssClass="errorAsterik" ErrorMessage="Please enter n or y" ControlToValidate="Display_Add" ValidationExpression="[yYnN]">*</asp:RegularExpressionValidator>
                                </div>
                                  <div>
                                    <label for="Display_Dl" class="inline">Display Dl (Y / N)</label>
                                    <asp:TextBox ID="Display_Dl" CssClass="text inline" runat="server" MaxLength="1"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="Display_Dl_Val" runat="server" CssClass="errorAsterik" ErrorMessage="Please enter n or y" ControlToValidate="Display_Dl" ValidationExpression="[yYnN]">*</asp:RegularExpressionValidator>
                                </div>
                                  <div>
                                    <label for="Display_Dest" class="inline">Display Dest (Y / N)</label>
                                    <asp:TextBox ID="Display_Dest" CssClass="text inline" runat="server" MaxLength="1"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="Display_Dest_Val" runat="server" CssClass="errorAsterik" ErrorMessage="Please enter n or y" ControlToValidate="Display_Dest" ValidationExpression="[yYnN]">*</asp:RegularExpressionValidator>
                                </div>
                                  <div>
                                    <label for="Display_Hle_Txt" class="inline">Display_HLE_Text (Y / N)</label>
                                    <asp:TextBox ID="Display_Hle_Txt" CssClass="text inline" runat="server" MaxLength="1"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="Display_Hle_Txt_Val" runat="server" CssClass="errorAsterik" ErrorMessage="Please enter n or y" ControlToValidate="Display_Hle_Txt" ValidationExpression="[yYnN]">*</asp:RegularExpressionValidator>
                                </div>
                                  <div>
                                    <label for="Refer_To_Qual_Req" class="inline">Refer To Qual Request (Y / N)</label>
                                    <asp:TextBox ID="Refer_To_Qual_Req" CssClass="text inline" runat="server" MaxLength="1"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="Refer_To_Qual_Req_Val" runat="server" CssClass="errorAsterik" ErrorMessage="Please enter n or y" ControlToValidate="Refer_To_Qual_Req" ValidationExpression="[yYnN]">*</asp:RegularExpressionValidator>
                                </div>
                                  <div>
                                    <label for="Vrac_Vehicle_Upgrade" class="inline">Vrac Vehicle Upgrade (Y / N)</label>
                                    <asp:TextBox ID="Vrac_Vehicle_Upgrade" CssClass="text inline" runat="server" MaxLength="1"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="Vrac_Vehicle_Upgrade_Val" runat="server" CssClass="errorAsterik" ErrorMessage="Please enter n or y" ControlToValidate="Vrac_Vehicle_Upgrade" ValidationExpression="[yYnN]">*</asp:RegularExpressionValidator>
                                </div>
                              </td>
                            </tr>
                          </table>
                        </asp:View>
                          <asp:View ID="View4" runat="server">
                          <table style="width: 100%; border-width: 1px; border-color: #666; border-style: solid">
                            <tr>
                              <td>
                                <div>
                                    <label for="Opt_HLE" class="inline">Opt HLE (Y / N)</label>
                                    <asp:TextBox ID="Opt_HLE" CssClass="text inline" runat="server" MaxLength="1"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="Opt_HLE_Val" ControlToValidate="Opt_HLE" runat="server" CssClass="errorAsterik" ErrorMessage="Please enter n or y" ValidationExpression="[yYnN]">*</asp:RegularExpressionValidator>
                                </div>
                                  <div>
                                    <label for="Opt_Insurance_Replace" class="inline">Opt Insurance Replace (Y / N)</label>
                                    <asp:TextBox ID="Opt_Insurance_Replace" CssClass="text inline" runat="server" MaxLength="1"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="Opt_Insurance_Replace_Val" ControlToValidate="Opt_Insurance_Replace" runat="server" CssClass="errorAsterik" ErrorMessage="Please enter n or y" ValidationExpression="[yYnN]">*</asp:RegularExpressionValidator>
                                </div>
                                  <div>
                                    <label for="Opt_Neverlost" class="inline">Opt Neverlost (Y / N)</label>
                                    <asp:TextBox ID="Opt_Neverlost" CssClass="text inline" runat="server" MaxLength="1"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="Opt_Neverlost_Val" ControlToValidate="Opt_Neverlost" runat="server" CssClass="errorAsterik" ErrorMessage="Please enter n or y" ValidationExpression="[yYnN]">*</asp:RegularExpressionValidator>
                                </div>
                                  <div>
                                    <label for="Opt_Child_Seats" class="inline">Opt Child Seats (Y / N)</label>
                                    <asp:TextBox ID="Opt_Child_Seats" CssClass="text inline" runat="server" MaxLength="1"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="Opt_Child_Seats_Val" ControlToValidate="Opt_Child_Seats" runat="server" CssClass="errorAsterik" ErrorMessage="Please enter n or y" ValidationExpression="[yYnN]">*</asp:RegularExpressionValidator>
                                </div>
                                  <div>
                                    <label for="Opt_Hand_Control" class="inline">Opt Hand Control (Y / N)</label>
                                    <asp:TextBox ID="Opt_Hand_Control" CssClass="text inline" runat="server" MaxLength="1"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="Opt_Hand_Control_Val" ControlToValidate="Opt_Hand_Control" runat="server" CssClass="errorAsterik" ErrorMessage="Please enter n or y" ValidationExpression="[yYnN]">*</asp:RegularExpressionValidator>
                                </div>
                                  <div>
                                    <label for="Opt_Gold_Customer" class="inline">Opt Gold Customer (Y / N)</label>
                                    <asp:TextBox ID="Opt_Gold_Customer" CssClass="text inline" runat="server" MaxLength="1"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="Opt_Gold_Customer_Val" ControlToValidate="Opt_Gold_Customer" runat="server" CssClass="errorAsterik" ErrorMessage="Please enter n or y" ValidationExpression="[yYnN]">*</asp:RegularExpressionValidator>
                                </div>
                                  <div>
                                    <label for="Opt_Gold_Canopy" class="inline">Opt Gold Canopy (Y / N)</label>
                                    <asp:TextBox ID="Opt_Gold_Canopy" CssClass="text inline" runat="server" MaxLength="1"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="Opt_Gold_Canopy_Val" ControlToValidate="Opt_Gold_Canopy" runat="server" CssClass="errorAsterik" ErrorMessage="Please enter n or y" ValidationExpression="[yYnN]">*</asp:RegularExpressionValidator>
                                </div>
                                  <div>
                                    <label for="Opt_Portable_Phones" class="inline">Opt Portable Phones (Y / N)</label>
                                    <asp:TextBox ID="Opt_Portable_Phones" CssClass="text inline" runat="server" MaxLength="1"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="Opt_Portable_Phones_Val" ControlToValidate="Opt_Portable_Phones" runat="server" CssClass="errorAsterik" ErrorMessage="Please enter n or y" ValidationExpression="[yYnN]">*</asp:RegularExpressionValidator>
                                </div>
                                  <div>
                                    <label for="Opt_Skierized" class="inline">Opt Skierized (Y / N)</label>
                                    <asp:TextBox ID="Opt_Skierized" CssClass="text inline" runat="server" MaxLength="1"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="Opt_Skierized_Val" ControlToValidate="Opt_Skierized" runat="server" CssClass="errorAsterik" ErrorMessage="Please enter n or y" ValidationExpression="[yYnN]">*</asp:RegularExpressionValidator>
                                </div>
                                  <div>
                                    <label for="Opt_Mobile_Phones" class="inline">Opt Mobile Phones (Y / N)</label>
                                    <asp:TextBox ID="Opt_Mobile_Phones" CssClass="text inline" runat="server" MaxLength="1"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="Opt_Mobile_Phones_Val" ControlToValidate="Opt_Mobile_Phones" runat="server" CssClass="errorAsterik" ErrorMessage="Please enter n or y" ValidationExpression="[yYnN]">*</asp:RegularExpressionValidator>
                                </div>
                                  <div>
                                    <label for="Opt_Luggqage_Rack" class="inline">Opt Luggqage Rack (Y / N)</label>
                                    <asp:TextBox ID="Opt_Luggqage_Rack" CssClass="text inline" runat="server" MaxLength="1"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="Opt_Luggqage_Rack_Val" ControlToValidate="Opt_Luggqage_Rack" runat="server" CssClass="errorAsterik" ErrorMessage="Please enter n or y" ValidationExpression="[yYnN]">*</asp:RegularExpressionValidator>
                                </div>
                                  <div>
                                    <label for="Opt_Prestige_Services" class="inline">Opt Prestige Services (Y / N)</label>
                                    <asp:TextBox ID="Opt_Prestige_Services" CssClass="text inline" runat="server" MaxLength="1"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="Opt_Prestige_Services_Val" ControlToValidate="Opt_Prestige_Services" runat="server" CssClass="errorAsterik" ErrorMessage="Please enter n or y" ValidationExpression="[yYnN]">*</asp:RegularExpressionValidator>
                                </div>
                                  <div>
                                    <label for="Opt_Return_Center" class="inline">Opt Return Center (Y / N)</label>
                                    <asp:TextBox ID="Opt_Return_Center" CssClass="text inline" runat="server" MaxLength="1"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="Opt_Return_Center_Val" ControlToValidate="Opt_Return_Center" runat="server" CssClass="errorAsterik" ErrorMessage="Please enter n or y" ValidationExpression="[yYnN]">*</asp:RegularExpressionValidator>
                                </div>
                                  <div>
                                    <label for="Opt_HTV" class="inline">Opt HTV (Y / N)</label>
                                    <asp:TextBox ID="Opt_HTV" CssClass="text inline" runat="server" MaxLength="1"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="Opt_HTV_Val" ControlToValidate="Opt_HTV" runat="server" CssClass="errorAsterik" ErrorMessage="Please enter n or y" ValidationExpression="[yYnN]">*</asp:RegularExpressionValidator>
                                </div>
                                  <div>
                                    <label for="Opt_Heavy_Trucks" class="inline">Opt Heavy Trucks (Y / N)</label>
                                    <asp:TextBox ID="Opt_Heavy_Trucks" CssClass="text inline" runat="server" MaxLength="1"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="Opt_Heavy_Trucks_Val" ControlToValidate="Opt_Heavy_Trucks" runat="server" CssClass="errorAsterik" ErrorMessage="Please enter n or y" ValidationExpression="[yYnN]">*</asp:RegularExpressionValidator>
                                </div>
                                  <div>
                                    <label for="Opt_Pickup_Services" class="inline">Opt Pickup Services (Y / N)</label>
                                    <asp:TextBox ID="Opt_Pickup_Services" CssClass="text inline" runat="server" MaxLength="1"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="Opt_Pickup_Services_Val" ControlToValidate="Opt_Pickup_Services" runat="server" CssClass="errorAsterik" ErrorMessage="Please enter n or y" ValidationExpression="[yYnN]">*</asp:RegularExpressionValidator>
                                </div>
                                  <div>
                                      <br /><br />
                                    <label for="Opt_Wifi" class="inline">Opt Wifi (Y / N)</label>
                                    <asp:TextBox ID="Opt_Wifi" CssClass="text inline" runat="server" MaxLength="1"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="Opt_Wifi_Val" ControlToValidate="Opt_Wifi" runat="server" CssClass="errorAsterik" ErrorMessage="Please enter n or y" ValidationExpression="[yYnN]">*</asp:RegularExpressionValidator>
                                </div>
                                  <div>
                                    <label for="Opt_Plate_Pass" class="inline">Opt Plate Pass (Y / N)</label>
                                    <asp:TextBox ID="Opt_Plate_Pass" CssClass="text inline" runat="server" MaxLength="1"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="Opt_Plate_Pass_Val" ControlToValidate="Opt_Plate_Pass" runat="server" CssClass="errorAsterik" ErrorMessage="Please enter n or y" ValidationExpression="[yYnN]">*</asp:RegularExpressionValidator>
                                </div>
                                  <div>
                                    <label for="Opt_Express_Return" class="inline">Opt Express Return (Y / N)</label>
                                    <asp:TextBox ID="Opt_Express_Return" CssClass="text inline" runat="server" MaxLength="1"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="Opt_Express_Return_Val" ControlToValidate="Opt_Express_Return" runat="server" CssClass="errorAsterik" ErrorMessage="Please enter n or y" ValidationExpression="[yYnN]">*</asp:RegularExpressionValidator>
                                </div>
                                  <div>
                                    <label for="Opt_No_Walkings" class="inline">Opt No Walkings (Y / N)</label>
                                    <asp:TextBox ID="Opt_No_Walkings" CssClass="text inline" runat="server" MaxLength="1"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="Opt_No_Walkings_Val" ControlToValidate="Opt_No_Walkings" runat="server" CssClass="errorAsterik" ErrorMessage="Please enter n or y" ValidationExpression="[yYnN]">*</asp:RegularExpressionValidator>
                                </div>
                                  <div>
                                    <label for="Opt_After_Hours_Drop" class="inline">Opt After Hours Drop (Y / N)</label>
                                    <asp:TextBox ID="Opt_After_Hours_Drop" CssClass="text inline" runat="server" MaxLength="1"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="Opt_After_Hours_Drop_Val" ControlToValidate="Opt_After_Hours_Drop" runat="server" CssClass="errorAsterik" ErrorMessage="Please enter n or y" ValidationExpression="[yYnN]">*</asp:RegularExpressionValidator>
                                </div>
                                  <div>
                                    <label for="Opt_After_Hours_Pickup" class="inline">Opt After Hours Pickup (Y / N)</label>
                                    <asp:TextBox ID="Opt_After_Hours_Pickup" CssClass="text inline" runat="server" MaxLength="1"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="Opt_After_Hours_Pickup_Val" ControlToValidate="Opt_After_Hours_Pickup" runat="server" CssClass="errorAsterik" ErrorMessage="Please enter n or y" ValidationExpression="[yYnN]">*</asp:RegularExpressionValidator>
                                </div>
                                  <div>
                                    <label for="Opt_Infant_Seat" class="inline">Opt Infant Seat (Y / N)</label>
                                    <asp:TextBox ID="Opt_Infant_Seat" CssClass="text inline" runat="server" MaxLength="1"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="Opt_Infant_Seat_Val" ControlToValidate="Opt_Infant_Seat" runat="server" CssClass="errorAsterik" ErrorMessage="Please enter n or y" ValidationExpression="[yYnN]">*</asp:RegularExpressionValidator>
                                </div>
                                  <div>
                                    <label for="Opt_Boosters_Seats" class="inline">Opt Boosters Seats (Y / N)</label>
                                    <asp:TextBox ID="Opt_Boosters_Seats" CssClass="text inline" runat="server" MaxLength="1"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="Opt_Boosters_Seats_Val" ControlToValidate="Opt_Boosters_Seats" runat="server" CssClass="errorAsterik" ErrorMessage="Please enter n or y" ValidationExpression="[yYnN]">*</asp:RegularExpressionValidator>
                                </div>
                                  <div>
                                    <label for="Opt_Military_Loc" class="inline">Opt Military Loc (Y / N)</label>
                                    <asp:TextBox ID="Opt_Military_Loc" CssClass="text inline" runat="server" MaxLength="1"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="Opt_Military_Loc_Val" ControlToValidate="Opt_Military_Loc" runat="server" CssClass="errorAsterik" ErrorMessage="Please enter n or y" ValidationExpression="[yYnN]">*</asp:RegularExpressionValidator>
                                </div>
                                  <div>
                                    <label for="Opt_Hotel_Guest_Req" class="inline">Opt Hotel Guest Req (Y / N)</label>
                                    <asp:TextBox ID="Opt_Hotel_Guest_Req" CssClass="text inline" runat="server" MaxLength="1"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="Opt_Hotel_Guest_Req_Val" ControlToValidate="Opt_Hotel_Guest_Req" runat="server" CssClass="errorAsterik" ErrorMessage="Please enter n or y" ValidationExpression="[yYnN]">*</asp:RegularExpressionValidator>
                                </div>
                                  <div>
                                    <label for="Opt_Leasing_Loc" class="inline">Opt Leasing Loc (Y / N)</label>
                                    <asp:TextBox ID="Opt_Leasing_Loc" CssClass="text inline" runat="server" MaxLength="1"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="Opt_Leasing_Loc_Val" ControlToValidate="Opt_Leasing_Loc" runat="server" CssClass="errorAsterik" ErrorMessage="Please enter n or y" ValidationExpression="[yYnN]">*</asp:RegularExpressionValidator>
                                </div>
                                  <div>
                                    <label for="Opt_RE_Fuel" class="inline">Opt RE Fuel (Y / N)</label>
                                    <asp:TextBox ID="Opt_RE_Fuel" CssClass="text inline" runat="server" MaxLength="1"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="Opt_RE_Fuel_Val" ControlToValidate="Opt_RE_Fuel" runat="server" CssClass="errorAsterik" ErrorMessage="Please enter n or y" ValidationExpression="[yYnN]">*</asp:RegularExpressionValidator>
                                </div>
                                  <div>
                                    <label for="Opt_Roadside_Assist" class="inline">Opt Roadside Assist (Y / N)</label>
                                    <asp:TextBox ID="Opt_Roadside_Assist" CssClass="text inline" runat="server" MaxLength="1"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="Opt_Roadside_Assist_Val" ControlToValidate="Opt_Roadside_Assist" runat="server" CssClass="errorAsterik" ErrorMessage="Please enter n or y" ValidationExpression="[yYnN]">*</asp:RegularExpressionValidator>
                                </div>
                                  <div>
                                    <label for="Opt_Kiosk_Loc" class="inline">Opt Kiosk Loc (Y / N)</label>
                                    <asp:TextBox ID="Opt_Kiosk_Loc" CssClass="text inline" runat="server" MaxLength="1"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="Opt_Kiosk_Loc_Val" ControlToValidate="Opt_Kiosk_Loc" runat="server" CssClass="errorAsterik" ErrorMessage="Please enter n or y" ValidationExpression="[yYnN]">*</asp:RegularExpressionValidator>
                                </div>
                                  <div>
                                    <label for="Opt_Chaffeur_Loc" class="inline">Opt Chaffeur Loc (Y / N)</label>
                                    <asp:TextBox ID="Opt_Chaffeur_Loc" CssClass="text inline" runat="server" MaxLength="1"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="Opt_Chaffeur_Loc_Val" ControlToValidate="Opt_Chaffeur_Loc" runat="server" CssClass="errorAsterik" ErrorMessage="Please enter n or y" ValidationExpression="[yYnN]">*</asp:RegularExpressionValidator>
                                </div>
                                  <div>
                                    <label for="Opt_Train_Loc" class="inline">Opt Train Loc (Y / N)</label>
                                    <asp:TextBox ID="Opt_Train_Loc" CssClass="text inline" runat="server" MaxLength="1"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="Opt_Train_Loc_Val" ControlToValidate="Opt_Train_Loc" runat="server" CssClass="errorAsterik" ErrorMessage="Please enter n or y" ValidationExpression="[yYnN]">*</asp:RegularExpressionValidator>
                                <div>
                                    <label for="Opt_Ship_Loc" class="inline">Opt Ship Loc (Y / N)</label>
                                    <asp:TextBox ID="Opt_Ship_Loc" CssClass="text inline" runat="server" MaxLength="1"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="Opt_Ship_Loc_Val" ControlToValidate="Opt_Ship_Loc" runat="server" CssClass="errorAsterik" ErrorMessage="Please enter n or y" ValidationExpression="[yYnN]">*</asp:RegularExpressionValidator>
                                </div>
                                <div>
                                    <label for="Opt_Cruise_Loc" class="inline">Opt Cruise Loc (Y / N)</label>
                                    <asp:TextBox ID="Opt_Cruise_Loc" CssClass="text inline" runat="server" MaxLength="1"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="Opt_Cruise_Loc_Val" ControlToValidate="Opt_Cruise_Loc" runat="server" CssClass="errorAsterik" ErrorMessage="Please enter n or y" ValidationExpression="[yYnN]">*</asp:RegularExpressionValidator>
                                </div>
                                <div>
                                    <label for="Opt_Bus_Loc" class="inline">Opt Bus Loc (Y / N)</label>
                                    <asp:TextBox ID="Opt_Bus_Loc" CssClass="text inline" runat="server" MaxLength="1"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="Opt_Bus_Loc_Val" ControlToValidate="Opt_Bus_Loc" runat="server" CssClass="errorAsterik" ErrorMessage="Please enter n or y" ValidationExpression="[yYnN]">*</asp:RegularExpressionValidator>
                                </div>
                                <div>
                                    <label for="Opt_Subway_Loc" class="inline">Opt Subway Loc (Y / N)</label>
                                    <asp:TextBox ID="Opt_Subway_Loc" CssClass="text inline" runat="server" MaxLength="1"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="Opt_Subway_Loc_Val" ControlToValidate="Opt_Subway_Loc" runat="server" CssClass="errorAsterik" ErrorMessage="Please enter n or y" ValidationExpression="[yYnN]">*</asp:RegularExpressionValidator>
                                </div>
                                <div>
                                    <label for="Opt_Ret_Serv_Loc" class="inline">Opt Ret Serv Loc (Y / N)</label>
                                    <asp:TextBox ID="Opt_Ret_Serv_Loc" CssClass="text inline" runat="server" MaxLength="1"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="Opt_Ret_Serv_Loc_Val" ControlToValidate="Opt_Ret_Serv_Loc" runat="server" CssClass="errorAsterik" ErrorMessage="Please enter n or y" ValidationExpression="[yYnN]">*</asp:RegularExpressionValidator>
                                </div>
                                <div>
                                    <label for="Opt_Meet_Pf" class="inline">Opt Meet Pf (Y / N)</label>
                                    <asp:TextBox ID="Opt_Meet_Pf" CssClass="text inline" runat="server" MaxLength="1"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="Opt_Meet_Pf_Val" ControlToValidate="Opt_Meet_Pf" runat="server" CssClass="errorAsterik" ErrorMessage="Please enter n or y" ValidationExpression="[yYnN]">*</asp:RegularExpressionValidator>
                                </div>
                                <div>
                                    <label for="Opt_Meet_Acv_Res" class="inline">Opt Meet Acv Res (Y / N)</label>
                                    <asp:TextBox ID="Opt_Meet_Acv_Res" CssClass="text inline" runat="server" MaxLength="1"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="Opt_Meet_Acv_Res_Val" ControlToValidate="Opt_Meet_Acv_Res" runat="server" CssClass="errorAsterik" ErrorMessage="Please enter n or y" ValidationExpression="[yYnN]">*</asp:RegularExpressionValidator>
                                </div>
                                <div>
                                    <label for="Opt_Flight_Mand_All" class="inline">Opt Flight Mand All (Y / N)</label>
                                    <asp:TextBox ID="Opt_Flight_Mand_All" CssClass="text inline" runat="server" MaxLength="1"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="Opt_Flight_Mand_All_Val" ControlToValidate="Opt_Flight_Mand_All" runat="server" CssClass="errorAsterik" ErrorMessage="Please enter n or y" ValidationExpression="[yYnN]">*</asp:RegularExpressionValidator>
                                </div>
                                <div>
                                    <label for="Opt_Flight_Mand_Private" class="inline">Opt Flight Mand Private (Y / N)</label>
                                    <asp:TextBox ID="Opt_Flight_Mand_Private" CssClass="text inline" runat="server" MaxLength="1"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="Opt_Flight_Mand_Private_Val" ControlToValidate="Opt_Flight_Mand_Private" runat="server" CssClass="errorAsterik" ErrorMessage="Please enter n or y" ValidationExpression="[yYnN]">*</asp:RegularExpressionValidator>
                                </div>
                                <div>
                                    <label for="Opt_HOD" class="inline">Opt HOD (Y / N)</label>
                                    <asp:TextBox ID="Opt_HOD" CssClass="text inline" runat="server" MaxLength="1"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="Opt_HOD_Val" ControlToValidate="Opt_HOD" runat="server" CssClass="errorAsterik" ErrorMessage="Please enter n or y" ValidationExpression="[yYnN]">*</asp:RegularExpressionValidator>
                                </div>
                                <div>
                                    <label for="Opt_Gold_Choice" class="inline">Opt Gold Choice (Y / N)</label>
                                    <asp:TextBox ID="Opt_Gold_Choice" CssClass="text inline" runat="server" MaxLength="1"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="Opt_Gold_Choice_Val" ControlToValidate="Opt_Gold_Choice" runat="server" CssClass="errorAsterik" ErrorMessage="Please enter n or y" ValidationExpression="[yYnN]">*</asp:RegularExpressionValidator>
                                </div>
                                <div>
                                    <label for="Opt_Hertz_Equip" class="inline">Opt Hertz Equip (Y / N)</label>
                                    <asp:TextBox ID="Opt_Hertz_Equip" CssClass="text inline" runat="server" MaxLength="1"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="Opt_Hertz_Equip_Val" ControlToValidate="Opt_Hertz_Equip" runat="server" CssClass="errorAsterik" ErrorMessage="Please enter n or y" ValidationExpression="[yYnN]">*</asp:RegularExpressionValidator>
                                </div>
                               <div>
                                    <label for="Opt_Private_Flight_Only" class="inline">Opt Private Flight Only (Y / N)</label>
                                    <asp:TextBox ID="Opt_Private_Flight_Only" CssClass="text inline" runat="server" MaxLength="1"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="Opt_Private_Flight_Only_Val" ControlToValidate="Opt_Private_Flight_Only" runat="server" CssClass="errorAsterik" ErrorMessage="Please enter n or y" ValidationExpression="[yYnN]">*</asp:RegularExpressionValidator>
                                </div>
                                <div>
                                      <br /><br />
                                    <label for="Opt_MGB" class="inline">Opt MGB (Y / N)</label>
                                    <asp:TextBox ID="Opt_MGB" CssClass="text inline" runat="server" MaxLength="1"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="Opt_MGB_Val" ControlToValidate="Opt_MGB" runat="server" CssClass="errorAsterik" ErrorMessage="Please enter n or y" ValidationExpression="[yYnN]">*</asp:RegularExpressionValidator>
                                </div>
                                <div>
                                    <label for="Opt_Ins_Replace_Only" class="inline">Opt Ins Replace Only (Y / N)</label>
                                    <asp:TextBox ID="Opt_Ins_Replace_Only" CssClass="text inline" runat="server" MaxLength="1"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="Opt_Ins_Replace_Only_Val" ControlToValidate="Opt_Ins_Replace_Only" runat="server" CssClass="errorAsterik" ErrorMessage="Please enter n or y" ValidationExpression="[yYnN]">*</asp:RegularExpressionValidator>
                                <div>
                                    <label for="Opt_Return_To_Servicing_Loc" class="inline">Opt Return To Servicing Loc (Y / N)</label>
                                    <asp:TextBox ID="Opt_Return_To_Servicing_Loc" CssClass="text inline" runat="server" MaxLength="1"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="Opt_Return_To_Servicing_Loc_Val" ControlToValidate="Opt_Return_To_Servicing_Loc" runat="server" CssClass="errorAsterik" ErrorMessage="Please enter n or y" ValidationExpression="[yYnN]">*</asp:RegularExpressionValidator>
                                </div>
                                  <div>
                                    <label for="Opt_Gold_Anytime" class="inline">Opt Gold Anytime (Y / N)</label>
                                    <asp:TextBox ID="Opt_Gold_Anytime" CssClass="text inline" runat="server" MaxLength="1"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="Opt_Gold_Anytime_Val" ControlToValidate="Opt_Gold_Anytime" runat="server" CssClass="errorAsterik" ErrorMessage="Please enter n or y" ValidationExpression="[yYnN]">*</asp:RegularExpressionValidator>
                                </div>
                                </div>
                                </div>
                              </td>
                            </tr>
                          </table>
                        </asp:View>
                          <asp:View ID="View5" runat="server">
                          <table style="width: 100%; border-width: 1px; border-color: #666; border-style: solid">
                            <tr>
                              <td>
                                 <div>
                                    <label for="Notes_1" class="inline">Notes 1</label>
                                    <asp:TextBox ID="Notes_1" CssClass="text inline" TextMode="multiline" runat="server" Columns="30" Rows="5"></asp:TextBox>
                                </div>
                                <div>
                                    <label for="Notes_2" class="inline">Notes 2</label>
                                    <asp:TextBox ID="Notes_2" CssClass="text inline" TextMode="multiline" runat="server" Columns="30" Rows="5"></asp:TextBox>
                                </div>
                                <div>
                                    <label for="Notes_3" class="inline">Notes 3</label>
                                    <asp:TextBox ID="Notes_3" CssClass="text inline" TextMode="multiline" runat="server" Columns="30" Rows="5"></asp:TextBox>
                                </div>
                                <div>
                                    <label for="Notes_4" class="inline">Notes 4</label>
                                    <asp:TextBox ID="Notes_4" CssClass="text inline" TextMode="multiline" runat="server" Columns="30" Rows="5"></asp:TextBox>
                                </div>
                                <div>
                                    <label for="Notes_5" class="inline">Notes 5</label>
                                    <asp:TextBox ID="Notes_5" CssClass="text inline" TextMode="multiline" runat="server" Columns="30" Rows="5"></asp:TextBox>
                                </div>
                                <div>
                                    <label for="HOURS_OF_OPERATION" class="inline">HOURS OF OPERATION</label>
                                    <asp:TextBox ID="HOURS_OF_OPERATION" CssClass="text inline" TextMode="multiline" runat="server" Columns="30" Rows="5"></asp:TextBox>
                                </div>
                                <div>
                                    <label for="HOURS_OF_OPERATION_2" class="inline">HOURS OF OPERATION 2</label>
                                    <asp:TextBox ID="HOURS_OF_OPERATION_2" CssClass="text inline" TextMode="multiline" runat="server" Columns="30" Rows="5"></asp:TextBox>
                                </div>
                                <div>
                                    <label for="HOURS_OF_OPERATION_3" class="inline">HOURS OF OPERATION 3</label>
                                    <asp:TextBox ID="HOURS_OF_OPERATION_3" CssClass="text inline" TextMode="multiline" runat="server" Columns="30" Rows="5"></asp:TextBox>
                                </div>
                              </td>
                            </tr>
                          </table>
                        </asp:View>
                      </asp:MultiView>
                    </td>
                  </tr>
                </table>
                <div class="formActions">
                <asp:table ID="Table1" runat="server" Width ="25%" CellPadding = "10" CellSpacing="5" GridLines="Both" BorderWidth="1" >
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:RadioButton id="Thrifty" Text="Thrifty Location" GroupName="LocType" runat="server" />
                        </asp:TableCell>                    
                    </asp:TableRow>
                    <asp:TableRow> 
                        <asp:TableCell>
                            <asp:RadioButton id="Hertz" Text="Hertz Location" GroupName="LocType" runat="server" />
                        </asp:TableCell>                   
                    </asp:TableRow>
                    </asp:table>
                </div>
                <div class="formActions">
                    <asp:Button ID="AddButton" CssClass="btn" OnClick="AddButton_Click" runat="server" Text="Add Location" />
                    <a href="manageLocations.aspx?Row=<%=countryID() %>" class="CancelButton">Cancel</a>
                </div>
            </fieldset>
        </div>
    </div>
</asp:Content>

