﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ORM;
using CMSConfigs;

public partial class adminarea_addBrandProviders : System.Web.UI.Page
{
    private DatabaseEntities db;
    protected void Page_Load(object sender, EventArgs e)
    {
        Master.Page.Title = CMSSettings.SiteTitle + " - " + PageName.Text;
        if (Session["LoggedInName"] == null)
        {
            Response.Redirect("default.aspx?TimeOut=y&ReturnPage=" + Server.UrlEncode(HttpContext.Current.Request.Url.AbsoluteUri));
        }
        if (!Page.IsPostBack)
        {
            LoadProviders();
            LoadBrands();
        }
    }

    protected int brandID()
    {
        int rowID;
        bool result = int.TryParse(Request.QueryString["Brand"], out rowID);
        if (result)
        {
            return rowID;
        }
        return 0;
    }

    protected void LoadBrands()
    {
        db = new DatabaseEntities();
        BrandID.DataSource = db.Brands.OrderBy(b => b.BrandName).ToList();
        BrandID.DataValueField = "BrandID";
        BrandID.DataTextField = "BrandName";
        BrandID.DataBind();
        BrandID.Items.Insert(0, new ListItem("Select Brand", "0"));
        BrandID.SelectedValue = brandID().ToString();
    }

    protected void LoadProviders()
    {
        db = new DatabaseEntities();
        ProviderID.DataSource = db.Providers.OrderBy(p => p.ProviderName).ToList();
        ProviderID.DataValueField = "Pro_Id";
        ProviderID.DataTextField = "ProviderName";
        ProviderID.DataBind();
        ProviderID.Items.Insert(0, new ListItem("Select Provider", "0"));
    }

    private string checkFormIsValid()
    {
        string errorMsg = "";
        //check brand was selected
        if (BrandID.SelectedValue == "0")
        {
            errorMsg = "<li>Please select Brand</li>";
        }
        //check provider was selected
        if (ProviderID.SelectedValue == "0")
        {
            errorMsg = "<li>Please select Provider</li>";
        }

        return errorMsg;
    }

    protected void AddButton_Click(object sender, EventArgs e)
    {
        db = new DatabaseEntities();
        Response.Write("test");
        string errorMsg = checkFormIsValid();
        if (string.IsNullOrEmpty(errorMsg))
        {
            try
            {
                // initialize the objectContext
                int brand = Convert.ToInt32(BrandID.SelectedValue);
                int provider = Convert.ToInt32(ProviderID.SelectedValue);

                var providerbrand = db.BrandProviderMappings.Where(bp => bp.BrandIDFK == brand && bp.ProviderIDFK == provider);
            if (providerbrand.Count() == 0)
                    {
                        BrandProviderMapping brandProvider = new BrandProviderMapping
                        {
                            BrandIDFK = brand,
                            ProviderIDFK = provider
                        };
                        db.BrandProviderMappings.Add(brandProvider);
                        db.SaveChanges();

                        Response.Redirect("manageBrandProviders.aspx?MsgType=2&MsgToShow=Brands mapping for provider added successfully");

                    }
                    else
                    {
                        MsgToShow.Show(MyMessageBox.MessageType.Warning, "That Provider already exists. Please use another Provider.");
                        MsgToShow.Visible = true;
                    }
                

            }
            catch(Exception ex)
            {
                Response.Write(ex.StackTrace);
                MsgToShow.Show(MyMessageBox.MessageType.Error, "There was an error adding this information. Please try again."+ ex.StackTrace.ToString());
                MsgToShow.Visible = true;
            }
        }
    }
}