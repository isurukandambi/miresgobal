﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Ionic.Zip;
using System.Text;
using System.IO;
using System.Linq;
using ORM;
using CMSConfigs;

public partial class manageXMLErrors : System.Web.UI.Page
{
    private DatabaseEntities db;
    protected void Page_Load(object sender, EventArgs e)
    {
        Master.Page.Title = CMSSettings.SiteTitle + " - Edit Section - Manage XML Errors";

        if (Session["LoggedInName"] == null)
        {
            Response.Redirect("default.aspx?TimeOut=y&ReturnPage=" + Server.UrlEncode(HttpContext.Current.Request.Url.AbsoluteUri));
        }
        //30 day chart
        GenerateDailyXMLErrorChart(DateTime.Now.AddDays(-29), DateTime.Now, TotalXMLErrorsFor30Days, HertzXMLErrorsFor30Days);
    }
    //holds the report data details
    public class ReportData
    {
        public DateTime date { get; set; }
        public string numberItems { get; set; }
    }
    protected void GridView_MouseOver(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowIndex >= 0)
        {
            e.Row.Attributes.Add("onmouseover", "this.style.background='#FFFFFF'");
            e.Row.Attributes.Add("onmouseout", "this.style.background='#F7F7F7'");
        }
    }
    protected void GenerateDailyXMLErrorChart(DateTime from, DateTime to, HiddenField TotalSales, HiddenField TotalHertzSales)
    {
        DateTime startDate = from;
        DateTime endDate = to;
        TimeSpan startTS = new TimeSpan(0, 0, 0);
        TimeSpan endTS = new TimeSpan(23, 59, 59);
        List<ReportData> reportDataHertz = new List<ReportData>();
        List<ReportData> reportDataProvider = new List<ReportData>();

        int TotalErrors = 0;

        db = new DatabaseEntities();

        var xmlErrorLogs = db.XMLErrorLogs.AsEnumerable().Where(o => startDate.Date < Convert.ToDateTime(o.ErrorDate).Date && endDate.Date > Convert.ToDateTime(o.ErrorDate).Date).OrderBy(o => Convert.ToDateTime(o.ErrorDate).Date).GroupBy(o => Convert.ToDateTime(o.ErrorDate).Date);

        if (xmlErrorLogs.Count() > 0)
        {
            foreach (var error in xmlErrorLogs)
            {
                //Response.Write("Date-------------->" + Convert.ToDateTime(error.FirstOrDefault().ErrorDate).Year + "/" + Convert.ToDateTime(error.FirstOrDefault().ErrorDate).Month + "/" + Convert.ToDateTime(error.FirstOrDefault().ErrorDate).Day + "Count--------------->" + error.Count() + "</br>");
                //Response.Write("Date-------------->" + xmlErrorLogs.FirstOrDefault().k + "Provider---------------->" +xmlErrorLogs.FirstOrDefault().XML_Provider+ "</br>");

                TotalErrors += error.Count();
                reportDataHertz.Add(new ReportData { date = Convert.ToDateTime(Convert.ToDateTime(error.FirstOrDefault().ErrorDate).Year + "/" + Convert.ToDateTime(error.FirstOrDefault().ErrorDate).Month + "/" + Convert.ToDateTime(error.FirstOrDefault().ErrorDate).Day), numberItems = error.Count().ToString() });
            }



            //put the total order figures into relevant hidden fields
            TotalSales.Value = TotalErrors.ToString();

            //go through all dates from start to finish. If any of these dates are missing from our database values put in a zero
            foreach (var date in GetDateRange(startDate, endDate))
            {
                //handle hertz details
                TotalHertzSales.Value += ChartValue(reportDataHertz, date);
            }

            //remove the last coma in the results data
            TotalHertzSales.Value = TotalHertzSales.Value.Remove(TotalHertzSales.Value.Length - 1);
        }
    }

    //get all dates within a period
    private List<DateTime> GetDateRange(DateTime StartingDate, DateTime EndingDate)
    {
        if (StartingDate > EndingDate)
        {
            return null;
        }
        List<DateTime> rv = new List<DateTime>();
        DateTime tmpDate = StartingDate;
        do
        {
            rv.Add(tmpDate);
            tmpDate = tmpDate.AddDays(1);
        } while (tmpDate <= EndingDate);
        return rv;
    }

    //method to return the correct chart value
    protected string ChartValue(List<ReportData> reportData, DateTime date)
    {
        var matchingProviderDate = reportData.Find(p => p.date.ToString("yyyy/MM/dd", System.Globalization.CultureInfo.InvariantCulture) == date.ToString("yyyy/MM/dd", System.Globalization.CultureInfo.InvariantCulture));
        if (matchingProviderDate != null)
        {
            return matchingProviderDate.numberItems + ",";
        }
        else
        {
            return "0,";
        }
    }
    protected void XMLErrorReport_Click(object sender, EventArgs e)
    {
        //pass through the start and end dates for our query and produce an excel sheet - using a grid view so that we can use the gridview export to excel option
        DateTime startDate = Convert.ToDateTime(StartDate.Text + " 00:00:00");
        DateTime endDate = Convert.ToDateTime(EndDate.Text + " 23:59:59");


        var error = (from EL in db.XMLErrorLogs
                     join ES in db.XMLErrorLogSents on EL.XMLErrorLogID equals ES.XMLErrorLogIDFK
                     join ER in db.XMLErrorLogReceiveds on EL.XMLErrorLogID equals ER.XMLErrorLogIDFK
                     where startDate <= EL.ErrorDate && endDate.Date >= EL.ErrorDate
                     orderby EL.ErrorDate
                     select new
                     {
                         EL.ErrorDate,EL.PickUpLocation,EL.PickUpDate,EL.DropOffLocation,EL.DropOffDate,EL.CountryOfResidence,EL.CameFrom,EL.XML_ErrorCode,
                         EL.XML_ErrorMsg,ES.XMLSent,ER.XMLReceived
                     });
        Response.Write(error.Count());
        if (error.Count() == 0)
        {
            XMLErrorReportMsg.Text = "<p>There were no xml messages logged within that period. Please try again.</p>";
            XMLErrorReportMsg.Visible = true;
        }
        else
        {
            this.XMLErrorResults.DataSource = error.ToList();
            this.XMLErrorResults.AllowPaging = false;
            this.XMLErrorResults.DataBind();

            using (StringWriter sw = new StringWriter())
            {
                using (HtmlTextWriter htw = new HtmlTextWriter(sw))
                {
                    //  Create a form to contain the grid
                    Table table = new Table();

                    //  add the header row to the table
                    if (XMLErrorResults.HeaderRow != null)
                    {
                        table.Rows.Add(XMLErrorResults.HeaderRow);
                    }

                    //  add each of the data rows to the table
                    foreach (GridViewRow row in XMLErrorResults.Rows)
                    {
                        table.Rows.Add(row);
                    }

                    //  add the footer row to the table
                    if (XMLErrorResults.FooterRow != null)
                    {
                        table.Rows.Add(XMLErrorResults.FooterRow);
                    }

                    //  render the table into the htmlwriter
                    table.RenderControl(htw);
                }

                Response.Clear();
                string filename = "XMLLoggedReport_" + DateTime.Now.ToString("yyyyMMdd") + ".zip";
                Response.ContentType = "application/zip";
                Response.AddHeader("content-disposition", "filename=" + filename);

                using (ZipFile zip = new ZipFile())
                {
                    zip.AddEntry("XMLLoggedReport_" + DateTime.Now.ToString("yyyyMMdd") + ".xls", sw.ToString());
                    zip.Save(Response.OutputStream);
                }

                Response.End();
            }
        }

    }
}