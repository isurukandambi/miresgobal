﻿<%@ Page Language="C#" MasterPageFile="adminarea.master" AutoEventWireup="true" CodeFile="addThermeonCarDetails.aspx.cs" Inherits="adminarea_addThermeonCarDetails" %>

<%@ Register Src="~/adminarea/controls/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="js/validationEngine/css/validationEngine.jquery.css" rel="stylesheet" type="text/css" />
    <script src="js/validationEngine/jquery.validationEngine.js" type="text/javascript"></script>
    <script src="js/validationEngine/jquery.validationEngine-en.js" type="text/javascript"></script>
    <script type="text/javascript" language="javascript">
        $(document).ready(function () {
            $("#form1").validationEngine();
        });
    </script>
    <style type="text/css">
        input.inline {
             float: none;
        }
        input.shortField {
            width: 72%;
            padding: 2px 5px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="content">
        <uc1:MyMessageBox ID="MsgToShow" runat="server" ShowCloseButton="true" Visible="false" />

        <h3>
            <asp:Literal ID="PageName" runat="server" Text="Add Thermeon Car Details" /></h3>
        <div id="actionArea">
            <fieldset>
                <div>
                    <label for="CountryID" class="inline">Country </label>
                    <asp:DropDownList ID="CountryID" runat="server">
                    </asp:DropDownList>
                </div>
                <div>
                    <label for="CarGroupID" class="inline">Group </label>
                    <asp:DropDownList ID="CarGroupID" runat="server">
                    </asp:DropDownList>
                </div>
                <div class="actionArea">
                    <table class="review" width="100%" cellpadding="3" cellspacing="3" align="center">
                        <caption>
                            Vehicle Details</caption>
                        <tr>
                            <th>Brand
                            </th>
                            <th>Description
                            </th>
                            <th>Image
                            </th>

                        </tr>

                        <asp:Repeater ID="CarDetailsContainer" runat="server">
                            <ItemTemplate>
                                <tr>
                                    <td>
                                        <asp:Literal ID="BrandName" runat="server" Text='<%# Eval("BrandName")%>'></asp:Literal>
                                        <asp:HiddenField ID="BrandID" runat="server" Value='<%# Eval("BrandID")%>'></asp:HiddenField>
                                    </td>
                                    <td style="text-align: center;">
                                        <asp:TextBox runat="server" CssClass="shortField resultUploadTextBox" ID="Description"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:FileUpload ID="filPhoto" CssClass="fileInput inline" runat="server" />
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </table>
                </div>
                <div class="formActions">
                    <asp:Button ID="AddButton" CssClass="btn" OnClick="AddButton_Click" runat="server" Text="Add Car Details" />
                    <a href="manageThermeonVehicleDetails.aspx?Row=<%=countryID() %>" class="CancelButton">Cancel</a>
                </div>
            </fieldset>
        </div>
    </div>
</asp:Content>