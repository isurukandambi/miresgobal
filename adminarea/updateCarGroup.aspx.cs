﻿using System;
using System.Linq;
using System.Text;
using System.Web;
using ORM;
using CMSConfigs;

public partial class updateCarGroup : System.Web.UI.Page
{
    private DatabaseEntities db;

    protected void Page_Load(object sender, EventArgs e)
    {
        Master.Page.Title = CMSSettings.SiteTitle + " - " + PageName.Text;
        if (Session["LoggedInName"] == null)
        {
            Response.Redirect("default.aspx?TimeOut=y&ReturnPage=" + Server.UrlEncode(HttpContext.Current.Request.Url.AbsoluteUri));
        }
        if(!Page.IsPostBack){
            LoadData();
        }
    }
    protected void LoadData() {
        db = new DatabaseEntities();
        int row = rowID();
        var CarGroup = db.CarGroups.SingleOrDefault(c => c.CarGroupID == row);
        CarCode.Text = CarGroup.CarCode;
        
    }
    protected int rowID()
    {
        int rowID;
        bool result = int.TryParse(Request.QueryString["Row"], out rowID);
        if (result)
        {
            return rowID;
        }
        return 0;
    }
    protected void UpdateButton_Click(object sender, EventArgs e)
    {
        try
        {
            // initialize the objectContext
            db = new DatabaseEntities();
            int row = rowID();
            if (!db.CarGroups.Where(c => c.CarCode == CarCode.Text.Trim() && c.CarGroupID != row).Any())
            {
                var CarGroup = db.CarGroups.SingleOrDefault(c => c.CarGroupID == row);
                CarGroup.CarCode = CarCode.Text.Trim();
                db.SaveChanges();

                Response.Redirect("manageCarGroups.aspx?MsgToShow=Car Code updated successfully.&MsgType=2");
            }
            else
            {
                MsgToShow.Show(MyMessageBox.MessageType.Warning, "That Car Code already exists. Please use another  Car Code.");
                MsgToShow.Visible = true;
            }

        }
        catch
        {
            MsgToShow.Show(MyMessageBox.MessageType.Error, "There was an error adding this information. Please try again.");
            MsgToShow.Visible = true;
        }
    }
}