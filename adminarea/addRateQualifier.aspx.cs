﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ORM;
using CMSConfigs;

public partial class adminarea_addRateQualifier : System.Web.UI.Page
{
    private DatabaseEntities db;
    protected void Page_Load(object sender, EventArgs e)
    {
        Master.Page.Title = CMSSettings.SiteTitle + " - " + PageName.Text;
        if (Session["LoggedInName"] == null)
        {
            Response.Redirect("default.aspx?TimeOut=y&ReturnPage=" + Server.UrlEncode(HttpContext.Current.Request.Url.AbsoluteUri));
        }
    }

    protected void AddButton_Click(object sender, EventArgs e)
    {
        try
        {
            // initialize the objectContext
            db = new DatabaseEntities();
            if (RateQualifier.Text.Trim() == null)
            {
                MsgToShow.Show(MyMessageBox.MessageType.Warning, "Rate Qualifier Cannot be null. Please use another  Rate Qualifier.");
                MsgToShow.Visible = true;
            }
            else
            {
                if (!db.Rate_Qulifiers.Where(r => r.Rate_Qulifier == RateQualifier.Text.Trim()).Any())
                {
                    Rate_Qulifiers RQ = new Rate_Qulifiers
                    {
                        Rate_Qulifier = RateQualifier.Text.Trim(),

                    };
                    db.Rate_Qulifiers.Add(RQ);
                    db.SaveChanges();

                    Response.Redirect("manageRateQulifier.aspx?MsgType=2&MsgToShow='" + RateQualifier.Text.Trim() + "' added successfully");

                }
                else
                {
                    MsgToShow.Show(MyMessageBox.MessageType.Warning, "That Rate Qualifier already exists. Please use another Rate Qualifier.");
                    MsgToShow.Visible = true;
                }
            }

        }
        catch
        {
            MsgToShow.Show(MyMessageBox.MessageType.Error, "There was an error adding this information. Please try again.");
            MsgToShow.Visible = true;
        }
    }
}