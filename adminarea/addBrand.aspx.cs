﻿using System;
using System.Linq;
using System.Text;
using System.Web;
using ORM;
using CMSConfigs;

public partial class addBrand : System.Web.UI.Page
{
    private DatabaseEntities db;

    protected void Page_Load(object sender, EventArgs e)
    {
        Master.Page.Title = CMSSettings.SiteTitle + " - " + PageName.Text;
        if (Session["LoggedInName"] == null)
        {
            Response.Redirect("default.aspx?TimeOut=y&ReturnPage=" + Server.UrlEncode(HttpContext.Current.Request.Url.AbsoluteUri));
        }
    }


    protected void AddBrand_Click(object sender, EventArgs e)
    {
        try
        {
            // initialize the objectContext
            db = new DatabaseEntities();

            if (!db.Brands.Where(b => b.BrandName == Brandname.Text).Any())
            {
                Brand brand = new Brand
                {
                    BrandName = Brandname.Text.Trim(),
                  
                };
                db.Brands.Add(brand);
                db.SaveChanges();

                Response.Redirect("manageBrands.aspx?MsgType=2&MsgToShow='" + Brandname.Text + "' added successfully");

            }
            else
            {
                MsgToShow.Show(MyMessageBox.MessageType.Warning, "That Brand Name already exists. Please use another Brand Name.");
                MsgToShow.Visible = true;
            }

        }
        catch
        {
            MsgToShow.Show(MyMessageBox.MessageType.Error, "There was an error adding this information. Please try again.");
            MsgToShow.Visible = true;
        }
    }
}