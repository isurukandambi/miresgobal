﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.HtmlControls;
using ORM;
using CMSConfigs;


public partial class updatePartner : System.Web.UI.Page
{
    private DatabaseEntities db;

    protected void Page_Load(object sender, EventArgs e)
    {
        Master.Page.Title = CMSSettings.SiteTitle + " - " + PageName.Text;
        if (Session["LoggedInName"] == null)
        {
            Response.Redirect("default.aspx?TimeOut=y&ReturnPage=" + Server.UrlEncode(HttpContext.Current.Request.Url.AbsoluteUri));
        }
        if (!Page.IsPostBack)
        {
            //LoadCountries();
           // LoadLanguages();
            LoadData();
        }
    }
    //protected void LoadCountries()
    //{
    //    db = new DatabaseEntities();
    //    CountryID.DataSource = db.Countries.OrderBy(c => c.CountryName);
    //    CountryID.DataValueField = "CountryID";
    //    CountryID.DataTextField = "CountryName";
    //    CountryID.DataBind();
    //}
    //protected void LoadLanguages()
    //{
    //    db = new DatabaseEntities();
    //    LanguageID.DataSource = db.Languages.OrderBy(la => la.LanguageName);
    //    LanguageID.DataValueField = "LanguageID";
    //    LanguageID.DataTextField = "LanguageName";
    //    LanguageID.DataBind();
    //}
    protected void LoadData()
    {
        int row = rowID();
        db = new DatabaseEntities();
        var accessory = db.Partners.SingleOrDefault(a => a.PartnerId == row);
        //var accessoryInfo = db.AccessoryInfoes.SingleOrDefault(ai => ai.AccessoryIDFK == row);
        bool checkvehiclelimit, checkemail;
        if (chkenable.Checked)
        {
            checkvehiclelimit = true;
        }
        else
        {

            checkvehiclelimit = false;
        }
        if (chkemail.Checked)
        {
            checkemail = true;
        }
        else
        {
            checkemail = false;
        }
        if (accessory != null)
        {
            txtname.Text = accessory.DomainName;
            accessory.DomainName = txtname.Text;
            txtAgentdutycode.Text = accessory.AgentDutyCode;
            txtRequestorID.Text = accessory.RequestorID_ID;
            txtRequestorID_Type.Text = Convert.ToString(accessory.RequestorID_Type);
            txtCompanyName_Code.Text = accessory.CompanyName_Code;
            txtCodeContext.Text = accessory.CodeContext;
            if (accessory.VehicleLimtEnable == 1)
            {
                chkenable.Checked = true;
            }
            else
            {
                chkenable.Checked = false;
            }
            if (accessory.ConfimationMailSend == 1)
            {
                chkemail.Checked = true;
            }
            else
            {

                chkemail.Checked = false;
            }
            txtemail.Text = accessory.Email;

            CT_Allowed.SelectedValue = accessory.CT_Allowed.ToString();
          

            
        }
        else
        {
            Response.Redirect("managePartner.aspx?MsgType=4&MsgToShow=Invalid Partner. Partner not found.");
        }
    }
   
    protected int rowID()
    {
        int rowID;
        bool result = int.TryParse(Request.QueryString["Row"], out rowID);
        if (result)
        {
            return rowID;
        }
        return 0;
    }
    protected void UpdateButton_Click(object sender, EventArgs e)
    {
        try
        {
            // initialize the objectContext
            db = new DatabaseEntities();
            int checkvehiclelimit = 1, checkemail = 1;
            if (chkenable.Checked==true)
            {
                
                checkvehiclelimit = 1;
                
                
            }
          
            else
            {
               
                checkvehiclelimit = 0;
                
                
            }
            if (chkemail.Checked)
            {
                checkemail = 1;
            }
            else
            {
                checkemail = 0;
            }
           
             int row = rowID();
                var accessory = db.Partners.SingleOrDefault(a => a.PartnerId == row);
               // var accessoryInfo = db.AccessoryInfoes.SingleOrDefault(ai => ai.AccessoryIDFK == row);
               // int selectedCountryID = Convert.ToInt32(CountryID.SelectedValue);
                //int selectedLangID = Convert.ToInt32(LanguageID.SelectedValue);

                accessory.VehicleLimtEnable = Convert.ToInt32(checkvehiclelimit);
                accessory.Email = txtemail.Text;
                accessory.ConfimationMailSend = Convert.ToInt32(checkemail);
                txtname.Text = accessory.DomainName;
                accessory.AgentDutyCode = txtAgentdutycode.Text;
                accessory.RequestorID_ID = txtRequestorID.Text;
                accessory.RequestorID_Type = Convert.ToInt32(txtRequestorID_Type.Text);
                accessory.CompanyName_Code = txtCompanyName_Code.Text;
                accessory.CodeContext = txtCodeContext.Text;
                accessory.CT_Allowed = Convert.ToInt32(CT_Allowed.SelectedValue);
             

                db.SaveChanges();
                Response.Redirect("managePartner.aspx?MsgType=2&MsgToShow='" + txtname.Text + "' updated successfully");
            

                
                
            }
         
          
               
            
          
        
        catch
        {
            MsgToShow.Show(MyMessageBox.MessageType.Error, "There was an error modifyng this information. Please try again.");
            MsgToShow.Visible = true;
        }
    }
    protected string cleanFilePath(HtmlInputFile fileInfo)
    {
        string fileName = Path.GetFileNameWithoutExtension(fileInfo.PostedFile.FileName);
        string fileExtension = Path.GetExtension(fileInfo.PostedFile.FileName);
        fileName = Server.HtmlEncode(fileName);
        fileName = fileName + DateTime.Now;
        fileName = fileName.Replace(" ", "");
        fileName = fileName.Replace("/", "");
        fileName = fileName.Replace(":", "");
        fileName = fileName.Replace("&", "");
        fileName = fileName.ToLower();
        fileName = fileName + fileExtension;
        return fileName;
    }
}