﻿using System;
using System.Linq;
using ORM;
using CMSConfigs;

public partial class updateUserLogin : System.Web.UI.Page
{
    private DatabaseEntities db;

    protected void Page_Load(object sender, EventArgs e)
    {
        Master.Page.Title = CMSSettings.SiteTitle + " - " + PageName.Text;

        if (!Page.IsPostBack)
        {
            LoadRoles();
            LoadData();
        }
    }

    protected int rowID()
    {
        int rowID;
        bool result = int.TryParse(Request.QueryString["Row"], out rowID);
        if (result)
        {
            return rowID;
        }
        return 0;
    }

    protected void LoadData()
    {
        //initialize the objectContext
        db = new DatabaseEntities();

        int row = rowID();
        var dbValue = db.Logins.Where(l => l.LoginID == row).SingleOrDefault();

        if (dbValue != null)
        {
            LoggedInName.Text = dbValue.LoggedInName;
            SiteRoles.SelectedValue = dbValue.RoleIDFK.ToString();
            Email.Text = dbValue.Email;
        }
        else
        {
            Response.Redirect("manageLogins.aspx?MsgToShow=Invalid item selected. Please try again.&MsgType=3");
        }
    }

    protected void LoadRoles()
    {
        //initialize the objectContext
        db = new DatabaseEntities();

        SiteRoles.DataSource = db.Roles.OrderBy(r => r.RoleName).ToList();
        SiteRoles.DataTextField = "RoleName";
        SiteRoles.DataValueField = "RoleID";
        SiteRoles.DataBind();
    }

    protected void UpdateLogin_Click(object sender, EventArgs e)
    {

        try
        {
            //initialize the objectContext
            db = new DatabaseEntities();


            int row = rowID();
            var login = db.Logins.SingleOrDefault(l => l.LoginID == row);
            login.LoggedInName = LoggedInName.Text;
            login.Email = Email.Text;
            login.RoleIDFK = Convert.ToInt16(SiteRoles.SelectedValue);

            db.SaveChanges();
            Response.Redirect("manageLogins.aspx?MsgToShow=Details added successfully.&MsgType=2");
        }
        catch
        {
            MsgToShow.Show(MyMessageBox.MessageType.Error, "There was an error updating this information. Please try again.");
            MsgToShow.Visible = true;
        }

    }
}
