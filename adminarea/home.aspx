﻿<%@ Page Language="C#" MasterPageFile="adminarea.master" AutoEventWireup="true" CodeFile="home.aspx.cs" Inherits="home" %>
<%@ Register Src="~/adminarea/controls/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>
    
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">google.load("jqueryui", "1.7.2");</script>
    <script type="text/javascript" src="js/Highcharts/highcharts.js"></script>
    <script type="text/javascript" src="js/Highcharts/modules/exporting.js"></script>
    <script src="js/datepicker.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="js/chartControls.js?v=6545533"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#tabs-set-1").tabs();
            $("#tabs-set-2").tabs();

            //set default date popups
            $('.startDate, .endDate').datepicker({
                numberOfMonths: 2,
                showButtonPanel: true,
                dateFormat: 'yy-mm-dd',
                showOn: 'both',
                buttonImage: 'images/calendar-icon.gif',
                buttonImageOnly: true,
                buttonText: 'Show Date Picker'
            });

            //when the pickup date has changed, update the drop off date to be one day extra from that date
            $('.startDate').change(function () {
                var nextDayDate = $('.startDate').datepicker('getDate', '+1d');
                nextDayDate.setDate(nextDayDate.getDate() + 1);
                $('.endDate').datepicker('setDate', nextDayDate);
            });

            //grid 1 settings
            $('#dataGrid').dataTable({
                "sDom": '<"dataTable_header"lf>rt<"dataTable_footer"ip>',
                "bAutoWidth": false,
                "sPaginationType": "full_numbers",
                "iDisplayLength": 10,
                "bStateSave": true,
                "iCookieDuration": 300,
                //customizing columns
                "aoColumns": [
        			{ "bSearchable": false, "bVisible": false }, //setting first column to be hidden from searching and the user - required for knowing what database row to remove
                    { "sType": "html" }, //setting second column to search word of html link
        			null,
                    null,
                    null,
        			null,
        			{ "bSortable": false } //setting last column not to sort onClick
                ]

            });
            //check all checkboxes if image clicked on
            $('#headercheck').click(function (evt) {
                evt.preventDefault();
                $('#dataGrid :checkbox').attr('checked', function () {
                    return !this.checked;
                });
            });

            //don't fire the delete button if no checkbox selected
            $('[id$=DeleteItems]').click(function (evt) {
                if ($("#dataGrid :checked").size() == 0) {
                    evt.preventDefault();
                }
            });

            //grid 2 settings
            $('#dataGrid2').dataTable({
                "sDom": '<"dataTable_header"lf>rt<"dataTable_footer"ip>',
                "bAutoWidth": false,
                "sPaginationType": "full_numbers",
                "iDisplayLength": 10,
                "bStateSave": true,
                "iCookieDuration": 300,
                //customizing columns
                "aoColumns": [
        			{ "bSearchable": false, "bVisible": false }, //setting first column to be hidden from searching and the user - required for knowing what database row to remove
                    { "sType": "html" }, //setting second column to search word of html link
        			null,
                    null,
                    null,
        			null,
        			null,
                    null,
                    null,
        			{ "bSortable": false } //setting last column not to sort onClick
                ]

            });
            //check all checkboxes if image clicked on
            $('#headercheck2').click(function (evt) {
                evt.preventDefault();
                $('#dataGrid2 :checkbox').attr('checked', function () {
                    return !this.checked;
                });
            });

            //don't fire the delete button if no checkbox selected
            $('[id$=DeleteItems2]').click(function (evt) {
                if ($("#dataGrid2 :checked").size() == 0) {
                    evt.preventDefault();
                }
            });


            /**** Sales over 30 days ****/
            var todaysDate = new Date();
            var date30DaysAgo = new Date();
            date30DaysAgo.setDate(todaysDate.getDate() - 29);
            //dailyChart('Sales30DaysContainer', date30DaysAgo, 'Sales over the last 30 days', $('#ContentPlaceHolder1_TotalSales30Days').val(), $('#ContentPlaceHolder1_TotalHertzSales30Days').val(), $('#ContentPlaceHolder1_TotalThriftySales30Days').val(), $('#ContentPlaceHolder1_TotalFireFlyAESales30Days').val(), $('#ContentPlaceHolder1_TotalFireFlyHSales30Days').val(), $('#ContentPlaceHolder1_HertzSalesOver30Days').val(), $('#ContentPlaceHolder1_ThriftySalesOver30Days').val(), $('#ContentPlaceHolder1_FireFlyAESalesOver30Days').val(), $('#ContentPlaceHolder1_FireFlyHSalesOver30Days').val());
            dailyChart('Sales30DaysContainer', date30DaysAgo, 'Sales over the last 30 days', $('#ContentPlaceHolder1_TotalSales30Days').val(), $('#ContentPlaceHolder1_TotalHertzSales30Days').val(), $('#ContentPlaceHolder1_TotalThriftySales30Days').val(), $('#ContentPlaceHolder1_TotalFireFlyAESales30Days').val(), $('#ContentPlaceHolder1_TotalFireFlyHSales30Days').val(), $('#ContentPlaceHolder1_TotalDollarSales30Days').val(), $('#ContentPlaceHolder1_HertzSalesOver30Days').val(), $('#ContentPlaceHolder1_ThriftySalesOver30Days').val(), $('#ContentPlaceHolder1_FireFlyAESalesOver30Days').val(), $('#ContentPlaceHolder1_FireFlyHSalesOver30Days').val(), $('#ContentPlaceHolder1_DollarSalesOver30Days').val());

            /**** Sales over 3 months ****/
            monthlyChart('Sales3MonthsContainer', 'Sales over the last 3 months', $('#ContentPlaceHolder1_TotalSales3Months').val(), $('#ContentPlaceHolder1_TotalHertzSales3Months').val(), $('#ContentPlaceHolder1_TotalThriftySales3Months').val(), $('#ContentPlaceHolder1_TotalFireFlyAESales3Months').val(), $('#ContentPlaceHolder1_TotalFireFlyHSales3Months').val(), $('#ContentPlaceHolder1_TotalDollarSales3Months').val(), $('#ContentPlaceHolder1_MonthsSelected3Months').val(), $('#ContentPlaceHolder1_HertzSalesOver3Months').val(), $('#ContentPlaceHolder1_ThriftySalesOver3Months').val(), $('#ContentPlaceHolder1_FireFlyAESalesOver3Months').val(), $('#ContentPlaceHolder1_FireFlyHSalesOver3Months').val(), $('#ContentPlaceHolder1_DollarSalesOver3Months').val());

            /**** Sales over 6 months ****/
            monthlyChart('Sales6MonthsContainer', 'Sales over the last 6 months', $('#ContentPlaceHolder1_TotalSales6Months').val(), $('#ContentPlaceHolder1_TotalHertzSales6Months').val(), $('#ContentPlaceHolder1_TotalThriftySales6Months').val(), $('#ContentPlaceHolder1_TotalFireFlyAESales6Months').val(), $('#ContentPlaceHolder1_TotalFireFlyHSales6Months').val(), $('#ContentPlaceHolder1_TotalDollarSales6Months').val(), $('#ContentPlaceHolder1_MonthsSelected6Months').val(), $('#ContentPlaceHolder1_HertzSalesOver6Months').val(), $('#ContentPlaceHolder1_ThriftySalesOver6Months').val(), $('#ContentPlaceHolder1_FireFlyAESalesOver6Months').val(), $('#ContentPlaceHolder1_FireFlyHSalesOver6Months').val(), $('#ContentPlaceHolder1_DollarSalesOver6Months').val());

            /**** Sales over 12 months ****/
            monthlyChart('Sales12MonthsContainer', 'Sales over the last 12 months', $('#ContentPlaceHolder1_TotalSales12Months').val(), $('#ContentPlaceHolder1_TotalHertzSales12Months').val(), $('#ContentPlaceHolder1_TotalThriftySales12Months').val(), $('#ContentPlaceHolder1_TotalFireFlyAESales12Months').val(), $('#ContentPlaceHolder1_TotalFireFlyHSales12Months').val(), $('#ContentPlaceHolder1_TotalDollarSales12Months').val(), $('#ContentPlaceHolder1_MonthsSelected12Months').val(), $('#ContentPlaceHolder1_HertzSalesOver12Months').val(), $('#ContentPlaceHolder1_ThriftySalesOver12Months').val(), $('#ContentPlaceHolder1_FireFlyAESalesOver12Months').val(), $('#ContentPlaceHolder1_FireFlyHSalesOver12Months').val(), $('#ContentPlaceHolder1_DollarSalesOver12Months').val());

            /**** Top Accessories ****/
            pieChart('TopAccessoriesContainer', 'Top Accessories Booked (from all sales)', $('#ContentPlaceHolder1_AccessoryValues').val());

            /**** Top Car Classes ****/
            pieChart('TopCarClassesContainer', 'Top Car Classes Booked (from all sales)', $('#ContentPlaceHolder1_CarClassValues').val());

            /**** Top 10 Pick up locations ****/
            pieChart('Top10PickupLocationsContainer', 'Top 10 pick up locations (from all sales)', $('#ContentPlaceHolder1_PickupLocationValues').val());

            /**** Top 10 Drop off locations ****/
            pieChart('Top10DropoffLocationsContainer', 'Top 10 drop off locations (from all sales)', $('#ContentPlaceHolder1_DropoffLocationValues').val());

            //Timeout Report
            /**** Sales over 30 days ****/
            var todaysDateTimeout = new Date();
            var date30DaysAgoTimeout = new Date();
            date30DaysAgoTimeout.setDate(todaysDateTimeout.getDate() - 29);

            requestTimeoutdailyChart('RequestTimeout30DaysContainer', date30DaysAgo, 'Request Timeout over the last 30 days', $('#ContentPlaceHolder1_TotalTimeout30Days').val(), $('#ContentPlaceHolder1_TotalHertzTimeout30Days').val(), $('#ContentPlaceHolder1_TotalThriftyTimeout30Days').val(), $('#ContentPlaceHolder1_TotalFireFlyTimeout30Days').val(), $('#ContentPlaceHolder1_TotalDollarTimeout30Days').val(), $('#ContentPlaceHolder1_HertzTimeout30Days').val(), $('#ContentPlaceHolder1_ThriftyTimeout30Days').val(), $('#ContentPlaceHolder1_FireFlyTimeout30Days').val(), $('#ContentPlaceHolder1_DollarTimeout30Days').val());

            /**** Sales over 3 months ****/
            //RequestTimeout3MonthsContainer
            requestTimeoutMonthlyChart('RequestTimeout3MonthsContainer', 'Request Timeout over the last 3 Months', $('#ContentPlaceHolder1_TotalTimeout3Months').val(), $('#ContentPlaceHolder1_TotalHertzTimeout3Months').val(), $('#ContentPlaceHolder1_TotalThriftyTimeout3Months').val(), $('#ContentPlaceHolder1_TotalFireFlyTimeout3Months').val(), $('#ContentPlaceHolder1_TotalDollarTimeout3Months').val(), $('#ContentPlaceHolder1_Timeout3MonthSelected').val(), $('#ContentPlaceHolder1_HertzTimeout3Months').val(), $('#ContentPlaceHolder1_ThriftyTimeout3Months').val(), $('#ContentPlaceHolder1_FireFlyTimeout3Months').val(), $('#ContentPlaceHolder1_DollarTimeout3Months').val());

            ///**** Sales over 6 months ****/
            requestTimeoutMonthlyChart('RequestTimeout6MonthsContainer', 'Request Timeout over the last 6 Months', $('#ContentPlaceHolder1_TotalTimeout6Months').val(), $('#ContentPlaceHolder1_TotalHertzTimeout6Months').val(), $('#ContentPlaceHolder1_TotalThriftyTimeout6Months').val(), $('#ContentPlaceHolder1_TotalFireFlyTimeout6Months').val(), $('#ContentPlaceHolder1_TotalDollarTimeout6Months').val(), $('#ContentPlaceHolder1_Timeout6MonthSelected').val(), $('#ContentPlaceHolder1_HertzTimeout6Months').val(), $('#ContentPlaceHolder1_ThriftyTimeout6Months').val(), $('#ContentPlaceHolder1_FireFlyTimeout6Months').val(), $('#ContentPlaceHolder1_DollarTimeout6Months').val());

            ///**** Sales over 12 months ****/
            requestTimeoutMonthlyChart('RequestTimeout12MonthsContainer', 'Request Timeout over the last 12 Months', $('#ContentPlaceHolder1_TotalTimeout12Months').val(), $('#ContentPlaceHolder1_TotalHertzTimeout12Months').val(), $('#ContentPlaceHolder1_TotalThriftyTimeout12Months').val(), $('#ContentPlaceHolder1_TotalFireFlyTimeout12Months').val(), $('#ContentPlaceHolder1_TotalDollarTimeout12Months').val(), $('#ContentPlaceHolder1_Timeout12MonthSelected').val(), $('#ContentPlaceHolder1_HertzTimeout12Months').val(), $('#ContentPlaceHolder1_ThriftyTimeout12Months').val(), $('#ContentPlaceHolder1_FireFlyTimeout12Months').val(), $('#ContentPlaceHolder1_DollarTimeout12Months').val());


            //Change report from time limit
            $('#TimeLimit').change(function () {
                showRequestTimeoutChart($(this).val());
            });
            $('#TimeLimit').val(1);
            showRequestTimeoutChart(1);

        });

        function showRequestTimeoutChart(val) {
            if (val == 1) {
                console.log('1')
                $('#RequestTimeout30Days').css("display", "block")
                $('#RequestTimeout3Month').css("display", "none")
                $('#RequestTimeout6Month').css("display", "none")
                $('#RequestTimeout12Month').css("display", "none")
            }
            else if (val == 2) {
                console.log('2')
                $('#RequestTimeout30Days').css("display", "none")
                $('#RequestTimeout3Month').css("display", "block")
                $('#RequestTimeout6Month').css("display", "none")
                $('#RequestTimeout12Month').css("display", "none")

            }
            else if (val == 3) {
                console.log('3')
                $('#RequestTimeout30Days').css("display", "none")
                $('#RequestTimeout3Month').css("display", "none")
                $('#RequestTimeout6Month').css("display", "block")
                $('#RequestTimeout12Month').css("display", "none")

            }
            else if (val == 4) {
                console.log('4')
                $('#RequestTimeout30Days').css("display", "none")
                $('#RequestTimeout3Month').css("display", "none")
                $('#RequestTimeout6Month').css("display", "none")
                $('#RequestTimeout12Month').css("display", "block")
            }
        }

        function hidecharts() {

            document.getElementById('tabs2-1').style.display = "none";
            document.getElementById('tabs2-2').style.display = "none";
            document.getElementById('tabs2-3').style.display = "none";
            document.getElementById('tabs2-4').style.display = "none";
            document.getElementById('tabs-set-2').style.display = "none";

        }
        function show() {

            document.getElementById('tabs2-1').style.display = "block";
            document.getElementById('tabs2-2').style.display = "block";
            document.getElementById('tabs2-3').style.display = "block";
            document.getElementById('tabs2-4').style.display = "block";
            document.getElementById('tabs-set-2').style.display = "block";
        }

        function showValidationMessage() {
            $("#ProductSalesMsg").hide();
            if (window.location.hash == "#tabs1-6") {
                $("#ProductSalesMsg").show();
            }
        }
    </script>

    <link rel="stylesheet" type="text/css" href="css/smoothness/jquery-ui.css" media="screen" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="content" class="homeContent">
        <h2>Dashboard</h2> 
        <uc1:MyMessageBox ID="MsgToShow" runat="server" ShowCloseButton="true" />  

         <div id="tabs-set-3" class="tabulation">

         <div>
            <label for="PartnerName" class="inline2" >Partner </label>
            <asp:DropDownList ID="PartnerID" runat="server" OnSelectedIndexChanged="PageRefresh" AutoPostBack="true" > 
            </asp:DropDownList>
        </div>

             </div>

         <div id="tabs-set-1" class="tabulation">
	        <ul>
		        <%-- <li><a href="#tabs1-1">Expiring Offers</a></li>--%>
		        <li><a href="#tabs1-2">Sales: 30 Days</a></li>
		        <li><a href="#tabs1-3">Sales: 3 Months</a></li>
		        <li><a href="#tabs1-4">Sales: 6 Months</a></li>
		        <li><a href="#tabs1-5">Sales: 12 Months</a></li>
		        <li id="ExportReportHeader" runat="server"><a href="#tabs1-6">Export Report</a></li>
	            <li id="RequestTimeoutReport" runat="server"><a id="hide" onclick="hidecharts()" href="#tabs1-7">Request Timeout Report</a></li>
            </ul>
            
            <div id="tabs1-2">
                <asp:HiddenField ID="HertzSalesOver30Days" runat="server" />
                <asp:HiddenField ID="ThriftySalesOver30Days" runat="server" />
                <asp:HiddenField ID="FireFlyAESalesOver30Days" runat="server" />
                <asp:HiddenField ID="FireFlyHSalesOver30Days" runat="server" />
                <asp:HiddenField ID="DollarSalesOver30Days" runat="server" />

                <asp:HiddenField ID="TotalSales30Days" runat="server" />
                <asp:HiddenField ID="TotalHertzSales30Days" runat="server" />
                <asp:HiddenField ID="TotalThriftySales30Days" runat="server" />
                <asp:HiddenField ID="TotalFireFlyAESales30Days" runat="server" />
                <asp:HiddenField ID="TotalFireFlyHSales30Days" runat="server" />
                <asp:HiddenField ID="TotalDollarSales30Days" runat="server" />

                <div id="Sales30DaysContainer" style="width: 90%; height: 450px; margin: 0 auto"></div>
            </div>
 
            <div id="tabs1-3">   
                <asp:HiddenField ID="MonthsSelected3Months" runat="server" />
                <asp:HiddenField ID="HertzSalesOver3Months" runat="server" />
                <asp:HiddenField ID="ThriftySalesOver3Months" runat="server" />
                <asp:HiddenField ID="FireFlyAESalesOver3Months" runat="server" />
                <asp:HiddenField ID="FireFlyHSalesOver3Months" runat="server" />
                <asp:HiddenField ID="DollarSalesOver3Months" runat="server" />

                <asp:HiddenField ID="TotalSales3Months" runat="server" />
                <asp:HiddenField ID="TotalHertzSales3Months" runat="server" />
                <asp:HiddenField ID="TotalThriftySales3Months" runat="server" />
                <asp:HiddenField ID="TotalFireFlyAESales3Months" runat="server" />
                <asp:HiddenField ID="TotalFireFlyHSales3Months" runat="server" />
                <asp:HiddenField ID="TotalDollarSales3Months" runat="server" />

                <div id="Sales3MonthsContainer" style="width: 900px; height: 450px; margin: 0 auto"></div>
            </div>
 
            <div id="tabs1-4">    
                <asp:HiddenField ID="MonthsSelected6Months" runat="server" />
                <asp:HiddenField ID="HertzSalesOver6Months" runat="server" />
                <asp:HiddenField ID="ThriftySalesOver6Months" runat="server" />
                <asp:HiddenField ID="FireFlyAESalesOver6Months" runat="server" />
                <asp:HiddenField ID="FireFlyHSalesOver6Months" runat="server" />
                <asp:HiddenField ID="DollarSalesOver6Months" runat="server" />

                <asp:HiddenField ID="TotalSales6Months" runat="server" />
                <asp:HiddenField ID="TotalHertzSales6Months" runat="server" />
                <asp:HiddenField ID="TotalThriftySales6Months" runat="server" />
                <asp:HiddenField ID="TotalFireFlyAESales6Months" runat="server" />
                <asp:HiddenField ID="TotalFireFlyHSales6Months" runat="server" />
                <asp:HiddenField ID="TotalDollarSales6Months" runat="server" />

                <div id="Sales6MonthsContainer" style="width: 900px; height: 450px; margin: 0 auto"></div>
            </div>
 
            <div id="tabs1-5">    
                <asp:HiddenField ID="MonthsSelected12Months" runat="server" />
                <asp:HiddenField ID="HertzSalesOver12Months" runat="server" />
                <asp:HiddenField ID="ThriftySalesOver12Months" runat="server" />
                <asp:HiddenField ID="FireFlyAESalesOver12Months" runat="server" />
                <asp:HiddenField ID="FireFlyHSalesOver12Months" runat="server" />
                <asp:HiddenField ID="DollarSalesOver12Months" runat="server" />

                <asp:HiddenField ID="TotalSales12Months" runat="server" />
                <asp:HiddenField ID="TotalHertzSales12Months" runat="server" />
                <asp:HiddenField ID="TotalThriftySales12Months" runat="server" />
                <asp:HiddenField ID="TotalFireFlyAESales12Months" runat="server" />
                <asp:HiddenField ID="TotalFireFlyHSales12Months" runat="server" />
                <asp:HiddenField ID="TotalDollarSales12Months" runat="server" />

                <div id="Sales12MonthsContainer" style="width: 900px; height: 450px; margin: 0 auto"></div>
            </div>        
              <div id="tabs1-6">    
                <div id="ProductSales">Select From - To dates to export all bookings within that period.</div>

                    <fieldset>
                        <div>
                            <label for="StartDate" class="inline">Start Date:
                                <asp:RequiredFieldValidator ID="StartDateEntered" ControlToValidate="StartDate" Display="Dynamic" ValidationGroup="ProductSalesReport" runat="server" ErrorMessage="Start Date Required" />                                
                            </label>
                            <asp:TextBox ID="StartDate" CssClass="text shortField startDate" Width="200" runat="server" />
                        </div>

                        
                        <div>
                            <label for="EndDate" class="inline">End Date:
                            <asp:RequiredFieldValidator ID="EndDateEntered" ControlToValidate="EndDate" Display="Dynamic" runat="server" ValidationGroup="ProductSalesReport" ErrorMessage="End Date Required" />                            
                            </label>
                            <asp:TextBox ID="EndDate" CssClass="text shortField endDate" Width="200" runat="server" />
                        </div>


                    <div class="formActions">
                        <asp:Button ID="ProductSalesReport" runat="server" Text="Create Report" CssClass="btnSameLine" ValidationGroup="ProductSalesReport" onclick="ProductSalesReport_Click" />
                    </div>
                    <asp:Literal ID="ProductSalesMsg" runat="server" Visible="false" />
                    
                    </fieldset>
                    <asp:GridView ID="ProductSalesGV" runat="server" AutoGenerateColumns="false" CssClass="gridview gridview-classic">
                    <Columns>
                        <asp:BoundField HeaderText="Customer Name" DataField="CustomerName" />
                        <asp:BoundField HeaderText="Customer Email" DataField="CustomerEmail" />
                        <asp:BoundField HeaderText="Customer Phone" DataField="CustomerPhone" />
                        <asp:BoundField HeaderText="Hertz #1" DataField="HertzNum1" />
                        <asp:BoundField HeaderText="Car Booked" DataField="CarBooked" />
                        <asp:BoundField HeaderText="Vehicle Type" DataField="VehicleType" />
                        <asp:BoundField HeaderText="Country Of Residence" DataField="CountryOfResidence" />
                        <asp:BoundField HeaderText="Language" DataField="LanguageCode" />
                        <asp:BoundField HeaderText="Pick Up Date" DataField="PickUpDate" />
                        <asp:BoundField HeaderText="Drop Off Date" DataField="DropOffDate" />
                        <asp:BoundField HeaderText="Order Date" DataField="OrderDate" />
                        <asp:BoundField HeaderText="Pick Up Location" DataField="PickUpLocation" />
                        <asp:BoundField HeaderText="Drop Off Location" DataField="DropOffLocation" />
                        <asp:BoundField HeaderText="No Days Hired" DataField="NoDaysHired" />
                        <asp:BoundField HeaderText="Came From Link" DataField="CameFrom" />
                        <asp:BoundField HeaderText="Amount Paid" DataField="AmountPaid" />
                        <asp:BoundField HeaderText="Currency" DataField="AmountPaidCurrency" />
                        <asp:BoundField HeaderText="XML Provider" DataField="XML_Provider" />
                        <asp:BoundField HeaderText="Pick up Supplier" DataField="XML_Provider_PickupSupplier" />
                        <asp:BoundField HeaderText="Drop off Supplier" DataField="XML_Provider_DropoffSupplier" />
                        <asp:BoundField HeaderText="Margin Rate" DataField="XML_Provider_MarginShare" />
                        <asp:BoundField HeaderText="Cancelled Booking" DataField="CancelledBooking" />                        
                        <asp:BoundField HeaderText="Modified Booking" DataField="Modified" />
                    </Columns>
                    </asp:GridView>
            </div>  
                         <div id="tabs1-7">
                <div>
                    <label for="TimeLimit" class="inline2">Time Limit </label>
                    <select id="TimeLimit">
                        <option value="1">30 Days</option>
                        <option value="2">3 Months</option>
                        <option value="3">6 Months</option>
                        <option value="4">12 Months</option>
                    </select>
                </div>

                <div>
                    <div id="RequestTimeout30Days" style="display: none">
                        <asp:HiddenField ID="HertzTimeout30Days" runat="server" />
                        <asp:HiddenField ID="ThriftyTimeout30Days" runat="server" />
                        <asp:HiddenField ID="FireFlyTimeout30Days" runat="server" />
                        <asp:HiddenField ID="DollarTimeout30Days" runat="server" />

                        <asp:HiddenField ID="TotalTimeout30Days" runat="server" />
                        <asp:HiddenField ID="TotalHertzTimeout30Days" runat="server" />
                        <asp:HiddenField ID="TotalThriftyTimeout30Days" runat="server" />
                        <asp:HiddenField ID="TotalFireFlyTimeout30Days" runat="server" />
                        <asp:HiddenField ID="TotalDollarTimeout30Days" runat="server" />

                        <div id="RequestTimeout30DaysContainer" style="width: 900px; height: 450px; margin: 0 auto"></div>
                    </div>


                    <div id="RequestTimeout3Month" style="display: none">
                        <asp:HiddenField ID="Timeout3MonthSelected" runat="server" />
                        <asp:HiddenField ID="HertzTimeout3Months" runat="server" />
                        <asp:HiddenField ID="ThriftyTimeout3Months" runat="server" />
                        <asp:HiddenField ID="FireFlyTimeout3Months" runat="server" />
                        <asp:HiddenField ID="DollarTimeout3Months" runat="server" />

                        <asp:HiddenField ID="TotalTimeout3Months" runat="server" />
                        <asp:HiddenField ID="TotalHertzTimeout3Months" runat="server" />
                        <asp:HiddenField ID="TotalThriftyTimeout3Months" runat="server" />
                        <asp:HiddenField ID="TotalFireFlyTimeout3Months" runat="server" />
                        <asp:HiddenField ID="TotalDollarTimeout3Months" runat="server" />

                        <div id="RequestTimeout3MonthsContainer" style="width: 900px; height: 450px; margin: 0 auto"></div>

                    </div>


                    <div id="RequestTimeout6Month" style="display: none">
                        <asp:HiddenField ID="Timeout6MonthSelected" runat="server" />
                        <asp:HiddenField ID="HertzTimeout6Months" runat="server" />
                        <asp:HiddenField ID="ThriftyTimeout6Months" runat="server" />
                        <asp:HiddenField ID="FireFlyTimeout6Months" runat="server" />
                        <asp:HiddenField ID="DollarTimeout6Months" runat="server" />

                        <asp:HiddenField ID="TotalTimeout6Months" runat="server" />
                        <asp:HiddenField ID="TotalHertzTimeout6Months" runat="server" />
                        <asp:HiddenField ID="TotalThriftyTimeout6Months" runat="server" />
                        <asp:HiddenField ID="TotalFireFlyTimeout6Months" runat="server" />
                        <asp:HiddenField ID="TotalDollarTimeout6Months" runat="server" />

                        <div id="RequestTimeout6MonthsContainer" style="width: 900px; height: 450px; margin: 0 auto"></div>

                    </div>

                    <div id="RequestTimeout12Month" style="display: none">
                        <asp:HiddenField ID="Timeout12MonthSelected" runat="server" />
                        <asp:HiddenField ID="HertzTimeout12Months" runat="server" />
                        <asp:HiddenField ID="ThriftyTimeout12Months" runat="server" />
                        <asp:HiddenField ID="FireFlyTimeout12Months" runat="server" />
                        <asp:HiddenField ID="DollarTimeout12Months" runat="server" />


                        <asp:HiddenField ID="TotalTimeout12Months" runat="server" />
                        <asp:HiddenField ID="TotalHertzTimeout12Months" runat="server" />
                        <asp:HiddenField ID="TotalThriftyTimeout12Months" runat="server" />
                        <asp:HiddenField ID="TotalFireFlyTimeout12Months" runat="server" />
                        <asp:HiddenField ID="TotalDollarTimeout12Months" runat="server" />

                        <div id="RequestTimeout12MonthsContainer" style="width: 900px; height: 450px; margin: 0 auto"></div>
                    </div>
                </div>

            </div>                              
        </div>  <!-- end tabs-set-1 -->
    
        <div id="tabs-set-2" class="tabulation"> 
	        <ul>
	            <li><a href="#tabs2-1">Top Extras</a></li>
		        <li><a href="#tabs2-2">Top Car Classes</a></li>
		        <li><a href="#tabs2-3">Top Pick Up Locations</a></li>
		        <li><a href="#tabs2-4">Top Drop Off Locations</a></li>
	        </ul>   
	            
	        <div id="tabs2-1"> 
                <asp:HiddenField ID="AccessoryValues" runat="server" />
                <div id="TopAccessoriesContainer" style="width: 900px; height: 450px; margin: 0 auto"></div>
            </div>
            
            <div id="tabs2-2">
                <asp:HiddenField ID="CarClassValues" runat="server" />
                <div id="TopCarClassesContainer" style="width: 900px; height: 450px; margin: 0 auto"></div>
            </div>
       
            <div id="tabs2-3">
                <asp:HiddenField ID="PickupLocationValues" runat="server" />
                <div id="Top10PickupLocationsContainer" style="width: 900px; height: 450px; margin: 0 auto"></div>
            </div>
       
            <div id="tabs2-4">
                <asp:HiddenField ID="DropoffLocationValues" runat="server" />
                <div id="Top10DropoffLocationsContainer" style="width: 900px; height: 450px; margin: 0 auto"></div>
            </div>
        </div> <!-- end tabs-set-2 -->
       
    </div>
</asp:Content>
