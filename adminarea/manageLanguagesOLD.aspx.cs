﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ORM;
using System.Linq;
using CMSConfigs;
using CMSUtils;

public partial class manageLanguages : System.Web.UI.Page
{
    private DatabaseEntities db;
    protected void Page_Load(object sender, EventArgs e)
    {
        Master.Page.Title = CMSSettings.SiteTitle + " - Edit Section - Manage Languages";

        if (Session["LoggedInName"] == null)
        {
            Response.Redirect("default.aspx?TimeOut=y&ReturnPage=" + Server.UrlEncode(HttpContext.Current.Request.Url.AbsoluteUri));
        }
        if (Request.QueryString["DeleteRow"] != null)
        {
            
            int deleteItemID = Convert.ToInt16(Request.QueryString["DeleteRow"]);
            DeleteItem(deleteItemID);
        }
        MsgToShow.Visible = false;
        if (Request.QueryString["MsgToShow"] != null)
        {
            int MsgType = Convert.ToInt16(Request.QueryString["MsgType"]);
            switch (MsgType)
            {
                case 1:
                    MsgToShow.Show(MyMessageBox.MessageType.Info, Request.QueryString["MsgToShow"].ToString());
                    break;
                case 2:
                    MsgToShow.Show(MyMessageBox.MessageType.Success, Request.QueryString["MsgToShow"].ToString());
                    break;
                case 3:
                    MsgToShow.Show(MyMessageBox.MessageType.Warning, Request.QueryString["MsgToShow"].ToString());
                    break;
                case 4:
                    MsgToShow.Show(MyMessageBox.MessageType.Error, Request.QueryString["MsgToShow"].ToString());
                    break;
                default:
                    goto case 1;
            }
            MsgToShow.Visible = true;
        }
        LoadLanguages();
    }
    protected void DeleteItem(int deleteItemID)
    {
        try 
        { 
            db = new DatabaseEntities();
            var item = db.Languages.SingleOrDefault(la => la.LanguageID == deleteItemID);
            db.Languages.Remove(item);
            db.SaveChanges();
            Response.Redirect("manageLanguages.aspx?MsgType=2&MsgToShow=Language Deleted.");
        }
        catch
        {
            Response.Redirect("manageLanguages.aspx?MsgType=4&MsgToShow=" + CMSUtilities.FKconstrainsFail());

        }
    }
    protected void LoadLanguages() { 
        db = new DatabaseEntities();
        GridView1.DataSource = (from la in db.Languages
                                orderby la.LanguageName
                                select new {la.LanguageID , la.LanguageName, la.LanguageCode }).ToList();
        GridView1.DataBind();
    }
    protected void GridView_MouseOver(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowIndex >= 0)
        {
            e.Row.Attributes.Add("onmouseover", "this.style.background='#FFFFFF'");
            e.Row.Attributes.Add("onmouseout", "this.style.background='#F7F7F7'");
        }
    }
}
