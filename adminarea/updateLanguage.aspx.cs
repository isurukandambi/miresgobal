﻿using System;
using System.Linq;
using System.Text;
using System.Web;
using ORM;
using CMSConfigs;

public partial class updateCountry : System.Web.UI.Page
{
    private DatabaseEntities db;

    protected void Page_Load(object sender, EventArgs e)
    {
        Master.Page.Title = CMSSettings.SiteTitle + " - " + PageName.Text;
        if (Session["LoggedInName"] == null)
        {
            Response.Redirect("default.aspx?TimeOut=y&ReturnPage=" + Server.UrlEncode(HttpContext.Current.Request.Url.AbsoluteUri));
        }
        if(!Page.IsPostBack){
            LoadData();
        }
    }
    protected void LoadData() {
        db = new DatabaseEntities();
        int row = rowID();
        var language = db.Languages.SingleOrDefault(la => la.LanguageID == row);
        if (language != null)
        {
            LanguageName.Text = language.LanguageName;
            LanguageCode.Text = language.LanguageCode;
        }
        else
        {
            Response.Redirect("manageLanguages.aspx?MsgToShow=Language not found.&MsgType=4");
        }

    }
    protected int rowID()
    {
        int rowID;
        bool result = int.TryParse(Request.QueryString["Row"], out rowID);
        if (result)
        {
            return rowID;
        }
        return 0;
    }
    protected void UpdateButton_Click(object sender, EventArgs e)
    {
        try
        {
            // initialize the objectContext
            db = new DatabaseEntities();
            int row = rowID();
            if (!db.Languages.Where(la => la.LanguageCode == LanguageCode.Text && la.LanguageID != row).Any())
            {
                var language = db.Languages.SingleOrDefault(la => la.LanguageID == row);
                language.LanguageCode = LanguageCode.Text.Trim();
                language.LanguageName = LanguageName.Text.Trim();
                db.SaveChanges();

                Response.Redirect("manageLanguages.aspx?MsgToShow=Language updated successfully.&MsgType=2");
            }
            else
            {
                MsgToShow.Show(MyMessageBox.MessageType.Warning, "That Language Code already exists. Please use another Language Code.");
                MsgToShow.Visible = true;
            }

        }
        catch
        {
            MsgToShow.Show(MyMessageBox.MessageType.Error, "There was an error adding this information. Please try again.");
            MsgToShow.Visible = true;
        }
    }
}