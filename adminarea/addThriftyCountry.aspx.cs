﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ORM;
using CMSConfigs;

public partial class adminarea_addThriftyCountry : System.Web.UI.Page
{
    private DatabaseEntities db;
    protected void Page_Load(object sender, EventArgs e)
    {
        Master.Page.Title = CMSSettings.SiteTitle + " - " + PageName.Text;
        if (Session["LoggedInName"] == null)
        {
            Response.Redirect("default.aspx?TimeOut=y&ReturnPage=" + Server.UrlEncode(HttpContext.Current.Request.Url.AbsoluteUri));
        }
        if (!Page.IsPostBack)
        {
            LoadCountries();
            LoadCountryCode();
        }
    }

    protected int rowID()
    {
        int rowID;
        bool result = int.TryParse(Request.QueryString["Row"], out rowID);
        if (result)
        {
            return rowID;
        }
        return 0;
    }

    protected void LoadCountries()
    {
        db = new DatabaseEntities();
        Countryname.DataSource = db.Countries.OrderBy(c => c.CountryID).ToList();
        Countryname.DataValueField = "CountryID";
        Countryname.DataTextField = "CountryName";
        Countryname.DataBind();
        Countryname.Items.Insert(0, new ListItem("Choose a Country", "0"));
        Countryname.SelectedValue = rowID().ToString();
    }

    protected void CountryDDL_Changed(object sender, EventArgs e)
    {
        Response.Redirect("addThriftyCountry.aspx?Row=" + Convert.ToInt32(Countryname.SelectedValue));
    }

    protected void LoadCountryCode()
    {
        int countryID = rowID();
        var Country = db.Countries.FirstOrDefault(r => r.CountryID == countryID);
        if (Country != null)
        {
            CountryCode.Text = Country.CountryCode.ToString();
        }
        
    }

    protected void AddButton_Click(object sender, EventArgs e)
    {
        try
        {
            // initialize the objectContext
            db = new DatabaseEntities();
            int countryID = Convert.ToInt32(Countryname.SelectedValue);
            if (!db.TC4R_List.Where(c => c.CountryIDFK == countryID).Any())
            {
                TC4R_List country = new TC4R_List
                {
                    CountryIDFK = rowID(),
                };
                db.TC4R_List.Add(country);
                db.SaveChanges();

                Response.Redirect("manageThriftyCountries.aspx?MsgType=2&MsgToShow='" + Countryname.SelectedItem.Text + "' added successfully");

            }
            else
            {
                MsgToShow.Show(MyMessageBox.MessageType.Warning, "Country Code already exists. Please use another Country Code.");
                MsgToShow.Visible = true;
            }

        }
        catch
        {
            MsgToShow.Show(MyMessageBox.MessageType.Error, "There was an error adding this information. Please try again.");
            MsgToShow.Visible = true;
        }
    }
}