﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using RyanairDataModel;

public partial class addLocationForProvider : System.Web.UI.Page
{
    private DatabaseEntities db;
    protected void Page_Load(object sender, EventArgs e)
    {
        Master.Page.Title = WebsiteSettings.SiteTitle + " - " + PageName.Text;
        if (Session["LoggedInName"] == null)
        {
            Response.Redirect("default.aspx?TimeOut=y&ReturnPage=" + Server.UrlEncode(HttpContext.Current.Request.Url.AbsoluteUri));
        }
        if (!Page.IsPostBack)
        {
            LoadProviders();
            int maxVehicleLimit = Convert.ToInt32(WebsiteSettings.MaxVehicleLimit);
            AdvisorText.InnerHtml = "Brands (Select brands and vehicle limit. (total vehicle limit of all brands not exceed " + maxVehicleLimit + "))";
        }
        LoadBrands();
    }
    
    private string checkFormIsValid()
    {
        string errorMsg = "";
       
        //check brand checked from brand list
        if (BrandsPanel.Controls.OfType<Panel>().Where(p => (p.Controls.OfType<CheckBox>().Where(c => c.Checked == true).Count() > 0)).Count() == 0)
        {
            errorMsg += "<li>Please select one or many Brand</li>";
        }

        return errorMsg;
    }
    protected void LoadProviders()
    {

        db = new DatabaseEntities();
        ProvidersDDL.DataSource = db.Providers.OrderBy(b => b.ProviderName);
        ProvidersDDL.DataTextField = "ProviderName";
        ProvidersDDL.DataValueField = "Pro_Id";
        ProvidersDDL.DataBind();
        ProvidersDDL.Items.Insert(0, new ListItem("All Providers", "0"));
        // PartnerIDFK.SelectedValue = PartnerID().ToString();
    }
    protected void AddButton_Click(object sender, EventArgs e)
    {

        MsgToShow.Visible = false;
        string errorMsg = checkFormIsValid();
        if (string.IsNullOrEmpty(errorMsg))
        {
            try
            {
                //Response.Write("test");
                var checkedBrands = (from d in BrandsPanel.Controls.OfType<Panel>()
                                    where ((CheckBox)d.Controls[0]).Checked == true
                                    select new
                                    {
                                        BrandID = Convert.ToInt32(((CheckBox)d.Controls[0]).ID),
                                        //providerID = Convert.ToInt32(((DropDownList)d.Controls[1]).SelectedValue),
                                        ratetype = ((CheckBoxList)((Panel)((Panel)d.Controls[2]).Controls[1]).Controls[0]).Items.OfType<ListItem>().Where(f => f.Selected == true), //((CheckBoxList)((Panel)((CheckBoxList)((Panel)d.Controls[0]).Controls[0]).Controls[2]).Controls[0]).Items.OfType<ListItem>().Where(f => f.Selected == true),
                                        sCountries = ((CheckBoxList)((Panel)d.Controls[3]).Controls[0]).Items.OfType<ListItem>().Where(f => f.Selected == true),
                                        destLocations = ((CheckBoxList)((Panel)((Panel)d.Controls[4]).Controls[2]).Controls[0]).Items.OfType<ListItem>().Where(f => f.Selected == true)
                                    });
                //Response.Write(checkedBrands.ToString());

                foreach (var brands in checkedBrands)
                {
                    foreach (var rate in brands.ratetype)
                    {
                        int rateID = Convert.ToInt32(rate.Value);
                        foreach (var country in brands.sCountries)
                        {
                            int countId = Convert.ToInt32(country.Value);
                            foreach (var loc in brands.destLocations)
                            {
                                int locationId = Convert.ToInt32(loc.Value);
                                int providerId = Convert.ToInt32(ProvidersDDL.SelectedValue);
                                int validat = checkRecords(brands.BrandID, providerId, locationId, countId, rateID);

                                if (validat == 1)
                                {
                                    ProviderRatesLocationsMapping prlm = new ProviderRatesLocationsMapping
                                    {
                                        SourceCountryIDFK = countId,
                                        DestLocationIDFK = locationId,
                                        RateCodeIDFK = rateID,
                                        BrandIDFK = brands.BrandID,
                                        ProviderCodeIDFK = providerId
                                    };
                                    db.ProviderRatesLocationsMappings.AddObject(prlm);
                                    db.SaveChanges();
                                    
                                }

                                else
                                {
                                    Response.Redirect("Providers mapping for locations already exists");
                                }
                            }
                        }
                    }
                    db.SaveChanges();
                }

                Response.Redirect("manageProviderRateLocationMapping.aspx?MsgType=2&MsgToShow=Providers mapping for locations added successfully");
            }
            catch (Exception ex)
            {
                MsgToShow.Show(MyMessageBox.MessageType.Error, "Unexepected Error occured while processing." + ex.Message + ex.StackTrace);
                MsgToShow.Visible = true;
            }
        }
        else
        {
            MsgToShow.Show(MyMessageBox.MessageType.Error, "<ul>" + errorMsg + "</ul>");
        }

    }

    protected int checkRecords(int brand, int provider, int location, int country, int rate)
    {
        db = new DatabaseEntities();
        var providerLocationD = (from pl in db.ProviderRatesLocationsMappings
                                 where pl.ProviderCodeIDFK == provider && pl.SourceCountryIDFK == country && pl.RateCodeIDFK == rate && pl.BrandIDFK == brand && pl.DestLocationIDFK == location
                                 select new
                                 {
                                     pl.PRLM_Id
                                 });
        if (providerLocationD.Count() > 0)
        {
            return 0;
        }
        else 
        {
            return 1;
        }
    }
    protected void AutoSelectExistingLocations()
    {
        //var locationItems = Locationsddl.Items.OfType<ListItem>().OrderBy(l => l.Text);
        var checkedBrands = (from d in BrandsPanel.Controls.OfType<Panel>().Select(c => c.Controls.OfType<CheckBox>())
                             where d.Where(c => c.Checked == true).Count() > 0
                             select new
                             {
                                 BrandID = Convert.ToInt32(d.First().ID),

                             });

            foreach (var brand in checkedBrands)
            {
                //var locationItems = Locationsddl.Items.OfType<ListItem>().OrderBy(l => l.Text);
                //if (db.BrandLocations.Where(b => b.LocationIDFK == locID && b.BrandIDFK == brand.BrandID).Count() > 0)
                //{
                //    locItem.Selected = true;
                //    locItem.Enabled = false;
                //}
                //else
                //{
                //    locItem.Selected = false;
                //    locItem.Enabled = true;
                //}
            }
        
    }

    protected void countryDDL_Changed(object sender, EventArgs e)
    {
        int providerid = Convert.ToInt32(ProvidersDDL.SelectedValue);
        DropDownList destinationCountryDDL = (DropDownList)sender;
        CheckBoxList destinationList = (CheckBoxList)((Panel)destinationCountryDDL.Parent.Controls[2]).Controls[0];
        //Response.Write(destinationList + " " + destinationCountryDDL.SelectedValue);
        var loadlocations = (from d in BrandsPanel.Controls.OfType<Panel>()
                             where ((DropDownList)((Panel)d.Controls[4]).Controls[0]).SelectedIndex != 0
                             select new
                             {
                                 BrandID = Convert.ToInt32(((CheckBox)d.Controls[0]).ID)
                             });

        int countryId = Convert.ToInt32(destinationCountryDDL.SelectedValue);
        
        var queriedLocations = db.Locations.Where(l => l.CountryIDFK == countryId).OrderBy(l => l.LocationName);
        destinationList.DataSource = queriedLocations;
        destinationList.DataBind();
        destinationList.Visible = true;

        foreach (var location in loadlocations)
        {
            int brandId = Convert.ToInt32(location.BrandID);

            foreach (ListItem item in destinationList.Items)
            {
                item.Selected = true;
                int locID = Convert.ToInt32(item.Value);
                bool isExisting = (db.ProviderRatesLocationsMappings.Where(b => b.DestLocationIDFK == locID && b.BrandIDFK == brandId && b.ProviderCodeIDFK == providerid).Count() > 0);
                item.Selected = isExisting;
                item.Enabled = !isExisting;
            }
        }
    }

    //protected void BrandChecked_Changed(object sender, EventArgs e)
    //{
    //    AutoSelectExistingLocations();
    //}
    
    protected void LoadBrands()
    {
        db = new DatabaseEntities();
        var brands = db.Brands.OrderBy(b => b.BrandName);
        foreach (var brand in brands)
        {
            //adding main panel for all controls
            Panel controlsPanel = new Panel();
            controlsPanel.CssClass = "BrandHolderControlPanel";

            //Rate Checkbox
            CheckBox brandCheckBox = new CheckBox();
            brandCheckBox.CssClass = "BrandIndicatorCheckBox";
            brandCheckBox.Text = brand.BrandName;
            //brandCheckBox.AutoPostBack = true;

            //validate locations by brand when selecting brands
            //brandCheckBox.CheckedChanged += new EventHandler(this.BrandChecked_Changed);
            brandCheckBox.ID = brand.BrandID.ToString();

            Panel controlsPanelRate = new Panel();
            controlsPanelRate.CssClass = "DesHolderControlPanel";

            //Add and populate Locations
            CheckBoxList rateList = new CheckBoxList();
            rateList.DataSource = db.Rates.OrderBy(r => r.R_Id);
            rateList.DataValueField = "R_Id";
            rateList.DataTextField = "RateType";
            rateList.DataBind();

            foreach (ListItem item in rateList.Items)
            {
                item.Selected = true;
            }

            //Add location to panel to add overflow scroll behavior to checkbox list
            Panel overflowPanelrate = new Panel();
            overflowPanelrate.CssClass = "checkboxListsInRatePanel";
            overflowPanelrate.Controls.Add(rateList);


            var providerBrand = (from pb in db.BrandProviderMappings
                                 join p in db.Providers on pb.ProviderIDFK equals p.Pro_Id
                                 where pb.BrandIDFK == brand.BrandID
                                     select new
                                     {
                                         p.Pro_Id,
                                         p.ProviderName
                                     });
            //Add Provider selct box
            DropDownList providerddl = new DropDownList();
            providerddl.DataSource = providerBrand;
            //providerddl.DataSource = db.BrandProviderMappings.Where(p => p.BrandIDFK == brand.BrandID);
            providerddl.DataValueField = "Pro_Id";
            providerddl.DataTextField = "ProviderName";
            providerddl.DataBind();

            providerddl.Items.Insert(0, new ListItem("Select Xml Provider", "0"));

        

            //Populate Vehicle limit dropdown list
            //for (int i = maxVehicleLimit; i >= 1; i--)
            //{
            //    vehLimitddl.Items.Insert(1, new ListItem(i.ToString(), i.ToString()));
            //}
            providerddl.SelectedValue = "0";

            //Add Country title for checkbox list
            Label CountryLabel = new Label();
            CountryLabel.Text = "<input type=\"checkbox\" class=\"toggleAllCountries\" style=\"margin-left: 6%;\" checked=\"true\" /> <b>Source Country</b>";

            //Add and populate Countries
            CheckBoxList countryList = new CheckBoxList();
            countryList.DataSource = db.Countries.OrderBy(c => c.CountryID);
            countryList.DataValueField = "CountryID";
            countryList.DataTextField = "CountryName";
            countryList.DataBind();

            foreach (ListItem item in countryList.Items)
            {
                item.Selected = true;
            }

            countryList.AutoPostBack = true;
            //countryList.SelectedIndexChanged += countryDDL_Changed;

            //Add countries to panel to add overflow scroll behavior to checkbox list
            Panel overflowPanel = new Panel();
            overflowPanel.CssClass = "checkboxListsInRatePanel";
            overflowPanel.Controls.Add(countryList);


            //Panel 2

            Panel controlsPanel2 = new Panel();
            controlsPanel2.CssClass = "DesHolderControlPanel";

            //Add Destination Country selct box
            DropDownList desCountryddl = new DropDownList();
            desCountryddl.DataSource = db.Countries.OrderBy(c => c.CountryID);
            desCountryddl.DataValueField = "CountryID";
            desCountryddl.DataTextField = "CountryName";
            desCountryddl.DataBind();

            desCountryddl.Items.Insert(0, new ListItem("Select Destination Country", "0"));

            desCountryddl.SelectedValue = "0";

            desCountryddl.AutoPostBack = true;
            desCountryddl.SelectedIndexChanged += countryDDL_Changed;

            //Add Destination title for checkbox list
            Label DestinationLabel = new Label();
            DestinationLabel.Text = "<input type=\"checkbox\" class=\"toggleAllLocations\" style=\"margin-left: 6%;margin-top: 6%;\" checked=\"true\" /> <b>Destination Country</b>";

            //Add and populate Locations
            CheckBoxList locationList = new CheckBoxList();
                locationList.DataValueField = "LocationID";
                locationList.DataTextField = "LocationName";

            foreach (ListItem item in locationList.Items)
            {
                item.Selected = true;
            }

            //Add location to panel to add overflow scroll behavior to checkbox list
            Panel overflowPanel2 = new Panel();
            overflowPanel2.CssClass = "checkboxListsInRatePanel";
            overflowPanel2.Controls.Add(locationList);

            //Adding All Controls and panels to Main Brandpanel
            controlsPanelRate.Controls.Add(overflowPanelrate);

            controlsPanel2.Controls.Add(desCountryddl);
            controlsPanel2.Controls.Add(DestinationLabel);
            controlsPanel2.Controls.Add(overflowPanel2);
            //DestinationPanel.Controls.Add(controlsPanel2);

            controlsPanel.Controls.Add(brandCheckBox);
            //controlsPanel.Controls.Add(providerddl);
            controlsPanel.Controls.Add(controlsPanelRate);    
            controlsPanel.Controls.Add(CountryLabel);
            controlsPanel.Controls.Add(overflowPanel);
            controlsPanel.Controls.Add(controlsPanel2);
            BrandsPanel.Controls.Add(controlsPanel);
        }
    }
    protected void Provider_Changed(object sender, EventArgs e)
    {

        var loadlocations = (from d in BrandsPanel.Controls.OfType<Panel>()
                             where ((DropDownList)((Panel)d.Controls[4]).Controls[0]).SelectedIndex != 0
                             select new
                             {
                                 Descount = ((DropDownList)((Panel)d.Controls[4]).Controls[0]),
                                 destLocations = ((CheckBoxList)((Panel)((Panel)d.Controls[4]).Controls[2]).Controls[0])
                             });

        foreach (var location in loadlocations)
        {
            location.destLocations.Visible = false;
            location.Descount.SelectedIndex = 0;
        }

       
    }
}