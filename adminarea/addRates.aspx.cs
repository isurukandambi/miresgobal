﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ORM;
using CMSConfigs;

public partial class adminarea_addRates : System.Web.UI.Page
{
    private DatabaseEntities db;
    protected void Page_Load(object sender, EventArgs e)
    {
        Master.Page.Title = CMSSettings.SiteTitle + " - " + PageName.Text;
        if (Session["LoggedInName"] == null)
        {
            Response.Redirect("default.aspx?TimeOut=y&ReturnPage=" + Server.UrlEncode(HttpContext.Current.Request.Url.AbsoluteUri));
        }
    }

    protected void AddButton_Click(object sender, EventArgs e)
    {
        try
        {
            // initialize the objectContext
            db = new DatabaseEntities();
            if (RateType.Text.Trim() == null)
            {
                MsgToShow.Show(MyMessageBox.MessageType.Warning, "Rate Type Cannot be null. Please use another  Rate Type.");
                MsgToShow.Visible = true;
            }
            else
            {
                if (!db.Rates.Where(r => r.RateType == RateType.Text.Trim()).Any())
                {
                    Rate Rate = new Rate
                    {
                        RateType = RateType.Text.Trim(),

                    };
                    db.Rates.Add(Rate);
                    db.SaveChanges();

                    Response.Redirect("manageRates.aspx?MsgType=2&MsgToShow='" + RateType.Text.Trim() + "' added successfully");

                }
                else
                {
                    MsgToShow.Show(MyMessageBox.MessageType.Warning, "That Rate already exists. Please use another Rate.");
                    MsgToShow.Visible = true;
                }
            }

        }
        catch
        {
            MsgToShow.Show(MyMessageBox.MessageType.Error, "There was an error adding this information. Please try again.");
            MsgToShow.Visible = true;
        }
    }
}