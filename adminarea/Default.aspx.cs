﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using ORM;
using System.Linq;

public partial class _DefaultEdit : System.Web.UI.Page
{
    private DatabaseEntities db;
    protected void Page_Load(object sender, EventArgs e)
    {
        //Page.Title = CMSSettings.SiteTitle + " - Edit Section - Login";

        MsgToShow.Visible = false;

        if (Request.QueryString["TimeOut"] != null)
        {
            MsgToShow.Show(MyMessageBox.MessageType.Warning, "Your session has expired.<br />Please log in again.");
            MsgToShow.Visible = true;
        }

        if (Page.IsPostBack)
        {
            db = new DatabaseEntities();
            var login = db.Logins.SingleOrDefault(l => l.Username == Username.Text && l.Password == Password.Text);

            if (login != null)
            {
                //check that the password entered matches the password in the database
                string usernameInDB = login.Username;
                string passwordInDB = login.Password;

                Session["LoginID"] = login.LoginID;
                Session["Username"] = usernameInDB;
                Session["Password"] = passwordInDB;
                Session["LoggedInName"] = login.LoggedInName;
                Session["RoleID"] = login.RoleIDFK;
                Session["LoggedInEmail"] = login.Email;
                Session["IsAuthorized"] = true;
                Session["IsFranchisee"] = false;
                if (!string.IsNullOrEmpty(login.CountryIDFK.ToString()))
                {
                    Session["IsFranchisee"] = true;
                    Session["FCountryIDFK"] = login.CountryIDFK;
                    Session["FCountryCode"] = "NA";
                }

                //load in roles system
                var roleAccess = (from ra in db.RoleSiteAccesses
                                  where ra.RoleIDFK == login.RoleIDFK
                                  select new
                                  {
                                      ra.SectionOnSite
                                  });
                List<string> AccessAreas = new List<string>();
                foreach (var area in roleAccess)
                {
                    AccessAreas.Add(area.SectionOnSite);
                }
                Session["SectionOnSite"] = AccessAreas;
                //update the last date logged in for this user
                login.LastLoggedIn = DateTime.Now;
                db.SaveChanges();
                //if user timed out bring them to the last page they were on, else default to home.aspx
                string redirectToPage = Request.QueryString["ReturnPage"] ?? "home.aspx";
                Response.Redirect(redirectToPage);

            }
            else
            {
                BadLogin();
            }
        }

        // check if LoginID contains a value
        // then do a check to see if the user should be brought to another page on the site they were previously working on
        if (Session["LoginID"] != null)
        {
            if (Request.QueryString["ReturnPage"] != null)
            {
                Response.Redirect(Request.QueryString["ReturnPage"]);
            }
            else
            {
                Response.Redirect("home.aspx");
            }
        }
    }
    protected void BadLogin()
    {
        MsgToShow.Show(MyMessageBox.MessageType.Error, "Login details incorrect. Please try again.");
        MsgToShow.Visible = true;
    }
}
