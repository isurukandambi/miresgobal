﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ORM;
using System.Linq;
using CMSConfigs;

public partial class manageBrandMapping : System.Web.UI.Page
{
    private DatabaseEntities db;
    protected void Page_Load(object sender, EventArgs e)
    {
        Master.Page.Title = CMSSettings.SiteTitle + " - Edit Section - Manage Brand mapping";

        if (Session["LoggedInName"] == null)
        {
            Response.Redirect("default.aspx?TimeOut=y&ReturnPage=" + Server.UrlEncode(HttpContext.Current.Request.Url.AbsoluteUri));
        }
        if (Request.QueryString["DeleteRow"] != null)
        {
            int brandLocID = Convert.ToInt16(Request.QueryString["DeleteRow"]);
            DeleteItem(brandLocID);
        }
        MsgToShow.Visible = false;
        if (Request.QueryString["MsgToShow"] != null)
        {
            int MsgType = Convert.ToInt16(Request.QueryString["MsgType"]);
            switch (MsgType)
            {
                case 1:
                    MsgToShow.Show(MyMessageBox.MessageType.Info, Request.QueryString["MsgToShow"].ToString());
                    break;
                case 2:
                    MsgToShow.Show(MyMessageBox.MessageType.Success, Request.QueryString["MsgToShow"].ToString());
                    break;
                case 3:
                    MsgToShow.Show(MyMessageBox.MessageType.Warning, Request.QueryString["MsgToShow"].ToString());
                    break;
                case 4:
                    MsgToShow.Show(MyMessageBox.MessageType.Error, Request.QueryString["MsgToShow"].ToString());
                    break;
                default:
                    goto case 1;
            }
            MsgToShow.Visible = true;
        }

        if (!Page.IsPostBack)
        {
            LoadCountries();
            LoadBrands();
            LoadMappedLocations();
        }
    }
    protected int CountryID()
    {
        int rowID;
        bool result = int.TryParse(Request.QueryString["Country"], out rowID);
        if (result)
        {
            return rowID;
        }
        return 0;
    }

    protected int BrandID()
    {
        int rowID;
        bool result = int.TryParse(Request.QueryString["Brand"], out rowID);
        if (result)
        {
            return rowID;
        }
        return 0;
    }
    protected void LoadCountries()
    {
        db = new DatabaseEntities();
        ddlCountries.DataSource = db.Countries.OrderBy(c => c.CountryName).ToList();
        ddlCountries.DataTextField = "CountryName";
        ddlCountries.DataValueField = "CountryID";
        ddlCountries.DataBind();
        ddlCountries.Items.Insert(0, new ListItem("Choose a Country", "0"));
        ddlCountries.SelectedValue = CountryID().ToString();
    }

    protected void LoadBrands()
    {
        db = new DatabaseEntities();
        ddlBrands.DataSource = db.Brands.OrderBy(b => b.BrandName).ToList();
        ddlBrands.DataTextField = "BrandName";
        ddlBrands.DataValueField = "BrandID";
        ddlBrands.DataBind();
        ddlBrands.Items.Insert(0, new ListItem("All Brands", "0"));
        ddlBrands.SelectedValue = BrandID().ToString();
    }

    protected void CountryDDL_Changed(object sender, EventArgs e)
    {
        Response.Redirect("manageBrandMapping.aspx?Country=" + Convert.ToInt32(ddlCountries.SelectedValue) + "&Brand=" + Convert.ToInt32(ddlBrands.SelectedValue));
    }
    protected void BrandDDL_Changed(object sender, EventArgs e)
    {
        Response.Redirect("manageBrandMapping.aspx?Country=" + Convert.ToInt32(ddlCountries.SelectedValue) + "&Brand=" + Convert.ToInt32(ddlBrands.SelectedValue));
    }
    protected void LoadMappedLocations()
    {
        db = new DatabaseEntities();
        int countryID = CountryID();
        int brandID = BrandID();
        if (countryID != 0 && brandID != 0)
        {
            GridView1.DataSource = (from bl in db.BrandLocations
                                    join l in db.Locations on bl.LocationIDFK equals l.LocationID
                                    join b in db.Brands on bl.BrandIDFK equals b.BrandID
                                    where bl.CountryIDFK == countryID && bl.BrandIDFK == brandID
                                    select new
                                    {
                                        bl.BrandLocationID,
                                        l.LocationName,
                                        l.LocationID,
                                        BrandCount = db.BrandLocations.Where(bc => bc.LocationIDFK == l.LocationID).Count(),
                                        b.BrandName,
                                        bl.VehicleLimit
                                    }).ToList();
        }
        else
        {
            GridView1.DataSource = (from bl in db.BrandLocations
                                    join l in db.Locations on bl.LocationIDFK equals l.LocationID
                                    join b in db.Brands on bl.BrandIDFK equals b.BrandID
                                    where bl.CountryIDFK == countryID
                                    select new
                                    {
                                        bl.BrandLocationID,
                                        l.LocationName,
                                        l.LocationID,
                                        BrandCount = db.BrandLocations.Where(bc => bc.LocationIDFK == l.LocationID).Count(),
                                        b.BrandName,
                                        bl.VehicleLimit
                                    }).ToList();
        }
        GridView1.DataBind();
    }
    protected void DeleteItem(int brandLocID)
    {
        db = new DatabaseEntities();
        var brandLocation = db.BrandLocations.SingleOrDefault(bl => bl.BrandLocationID == brandLocID);
        if (brandLocation != null)
        {
            db.BrandLocations.Remove(brandLocation);
            var carGroupsInBrandLoc = db.CarGroupBrandMappings.Where(cg => cg.BrandLocationIDFK == brandLocID);
            foreach (var cargroup in carGroupsInBrandLoc)
            {
                db.CarGroupBrandMappings.Remove(cargroup);
            }
            db.SaveChanges();
            Response.Redirect("manageBrandMapping.aspx?MsgType=2&MsgToShow=Mapped Location Removed.&Country=" + brandLocation.CountryIDFK);
        }
        else
        {
            Response.Redirect("manageBrandMapping.aspx?MsgType=4&MsgToShow=No Mapped Location found to Remove.");
        }

    }
    protected void GridView_MouseOver(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowIndex >= 0)
        {
            e.Row.Attributes.Add("onmouseover", "this.style.background='#FFFFFF'");
            e.Row.Attributes.Add("onmouseout", "this.style.background='#F7F7F7'");
        }
    }
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            int brandLocID = Convert.ToInt32(((HiddenField)e.Row.FindControl("BrandLocationID")).Value);
            int brandCount = Convert.ToInt32(((HiddenField)e.Row.FindControl("BrandCount")).Value);

            db = new DatabaseEntities();
            //Response.Write((e.Row.RowIndex+1) + " ------------- " + brandCount + " ---------------------- " + (e.Row.RowIndex+1) % brandCount + "</br>");
            //if ((e.Row.RowIndex+1) % brandCount == 0)
            //{
            //    e.Row.Cells[0].Attributes.Add("rowspan", brandCount.ToString());
            //}

            var carGroups = (from cgb in db.CarGroupBrandMappings
                             join cg in db.CarGroups on cgb.CarGroupIDFK equals cg.CarGroupID
                             where cgb.BrandLocationIDFK == brandLocID
                             select new
                             {
                                 cg.CarCode
                             });
            foreach(var cg in carGroups){
                ((Literal)e.Row.FindControl("CarCodes")).Text += cg.CarCode + ", ";
            }
            //remove last comma from car groups
            if (!string.IsNullOrEmpty(((Literal)e.Row.FindControl("CarCodes")).Text))
            {
                ((Literal)e.Row.FindControl("CarCodes")).Text = ((Literal)e.Row.FindControl("CarCodes")).Text.Substring(0, ((Literal)e.Row.FindControl("CarCodes")).Text.Length - 2);
            }
        }
    }
}
