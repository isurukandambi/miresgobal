﻿using System;
using System.Linq;
using System.Text;
using System.Web;
using ORM;
using CMSConfigs;
public partial class updateBrand : System.Web.UI.Page
{
    private DatabaseEntities db;

    protected void Page_Load(object sender, EventArgs e)
    {
        Master.Page.Title = CMSSettings.SiteTitle + " - " + PageName.Text;
        if (Session["LoggedInName"] == null)
        {
            Response.Redirect("default.aspx?TimeOut=y&ReturnPage=" + Server.UrlEncode(HttpContext.Current.Request.Url.AbsoluteUri));
        }
        if(!Page.IsPostBack){
            LoadData();
        }
    }
    protected void LoadData() {
        db = new DatabaseEntities();
        int row = rowID();
        var brand = db.Brands.SingleOrDefault(b => b.BrandID == row);
        if (brand != null)
        {
            Brandname.Text = brand.BrandName;
           
        }
        else
        {
            Response.Redirect("manageBrands.aspx?MsgToShow=Brand not found.&MsgType=4");
        }

    }
    protected int rowID()
    {
        int rowID;
        bool result = int.TryParse(Request.QueryString["Row"], out rowID);
        if (result)
        {
            return rowID;
        }
        return 0;
    }
    protected void UpdateBrand_Click(object sender, EventArgs e)
    {
        try
        {
            // initialize the objectContext
            db = new DatabaseEntities();
            int row = rowID();
            if (!db.Brands.Where(b => b.BrandName == Brandname.Text && b.BrandID != row).Any())
            {
                var brand = db.Brands.SingleOrDefault(b => b.BrandID == row);
                brand.BrandName = Brandname.Text.Trim();
               
                db.SaveChanges();

                Response.Redirect("manageBrands.aspx?MsgToShow=Brand updated successfully.&MsgType=2");
            }
            else
            {
                MsgToShow.Show(MyMessageBox.MessageType.Warning, "That Brand Name already exists. Please use another Brand Name.");
                MsgToShow.Visible = true;
            }

        }
        catch
        {
            MsgToShow.Show(MyMessageBox.MessageType.Error, "There was an error adding this information. Please try again.");
            MsgToShow.Visible = true;
        }
    }
}