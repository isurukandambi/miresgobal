﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Text;
using System.Linq;
using ORM;
using CMSConfigs;
using CMSUtils;
public partial class ForgotPassword_edit : System.Web.UI.Page
{
    private DatabaseEntities db;
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = CMSSettings.SiteTitle + " - Edit Section - Forgot Password";


        MsgToShow.Visible = false;


    }


    protected void ResetButton_Click(object sender, EventArgs e)
    {

        string LoggedInName = "", UsersEmail = "", UsersPassword = "";

        db = new DatabaseEntities();


        var dbValue = db.Logins.FirstOrDefault(l => l.Email == Usernametxt.Text);
        if (dbValue != null)
        {
            UsersPassword = dbValue.Password;
            LoggedInName = dbValue.LoggedInName;
            UsersEmail = dbValue.Email;
            //send notification to Head Office that a new provider is interested in signing up
            string EmailMessage = BuildEmail(UsersPassword, LoggedInName);
            SendEmail.SendMessage(CMSSettings.SiteEmailsSentFrom, UsersEmail, "Password reset for " + CMSSettings.SiteTitle + " admin area.", EmailMessage, true);
            Response.Redirect("ForgotPasswordSent.aspx");
        }
        else
        {
            MsgToShow.Show(MyMessageBox.MessageType.Error, "Email address not found. Please use your registered email address.");
            MsgToShow.Visible = true;
        }


    }
    //email that is sent to user
    protected string BuildEmail(string Password, string LoggedInName)
    {
        StringBuilder EmailTemplate = new StringBuilder();

        EmailTemplate.Append("<table>");
        EmailTemplate.Append("<tr><td>");

        EmailTemplate.Append("Hello " + LoggedInName + ",<br /><br />Your password has now been reset.");
        EmailTemplate.Append("<br /><br />");

        EmailTemplate.Append("To log into the system simply click on the link and enter in the username and password shown below:<br />");
        EmailTemplate.Append("Link: <a href=\"http://" + CMSSettings.SiteUrl + "/adminarea\">http://" + CMSSettings.SiteUrl + "/adminarea</a><br />");
        EmailTemplate.Append("New Password: " + Password + "<br />");
        EmailTemplate.Append("<br />");

        EmailTemplate.Append("Please note that the password is case sensitive so please ensure you enter it exactly as it is shown above. Once you have logged in you can change your password by going to the 'Update My Login' section of the site.");
        EmailTemplate.Append("<br /><br />");

        EmailTemplate.Append("Regards,<br />Just Perfect IT Support Team.");

        EmailTemplate.Append("</td></tr>");
        EmailTemplate.Append("</table>");

        return EmailTemplate.ToString();
    }
}