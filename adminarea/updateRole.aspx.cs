﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using ORM;
using CMSConfigs;

public partial class addRole : System.Web.UI.Page
{
    private DatabaseEntities db;

    protected void Page_Load(object sender, EventArgs e)
    {
        Master.Page.Title = CMSSettings.SiteTitle + " - " + PageName.Text;
        
        // load in the sections of the site for the roles.
        LoadSiteSections();
        LoadData();
    }
    protected int rowID()
    {
        int rowID;
        bool result = int.TryParse(Request.QueryString["Row"], out rowID);
        if (result)
        {
            return rowID;
        }
        return 0;
    }
    protected void LoadSiteSections()
    {
        // site sections are located in the web.config and are coma separated
        db = new DatabaseEntities();

        string[] sectionsList = new string[7];
        sectionsList[0] = "Dashboard|Dashboard";
        sectionsList[1] = "Manage Brands Mapping|ManageBrandMapping";
        sectionsList[2] = "Manage Vehicle Details|ManageVehicalDetails";
        sectionsList[3] = "Manage Vehicle Groups|ManageVehicleGroup";
        sectionsList[4] = "Manage Brands|ManageBrands";
        sectionsList[5] = "XML Report|XMLReport";
        sectionsList[6] = "Manage logins and Roles|LoginsRoles";

        //go through all 'sections' and adjust the repeater with the relevant name
        foreach (var section in sectionsList)
        {
            // add the template and text formatting
            StringBuilder sb = new StringBuilder();
			sb.Append("<tr><td>" + section.Split('|')[0] + "</td>");
			sb.Append("<td>");
			RadioButtonPlaceholder.Controls.Add(new LiteralControl(sb.ToString()));

            //add radio button list
            RadioButtonList radioButton = new RadioButtonList();
            radioButton.ID = section.Split('|')[1];
            radioButton.CssClass = "radioList";
            radioButton.RepeatLayout = RepeatLayout.Flow;
            radioButton.RepeatDirection = RepeatDirection.Horizontal;
            radioButton.Items.Add(new ListItem("Allow", "Allow"));
            radioButton.Items.Add(new ListItem("Deny", "Deny"));
            radioButton.SelectedIndex = 1;
            RadioButtonPlaceholder.Controls.Add(radioButton);

            //close off table format
            RadioButtonPlaceholder.Controls.Add(new LiteralControl("</td></tr>"));	
        }
    }

    protected void LoadData() {
        db = new DatabaseEntities();
        int row = rowID();
        var role = db.Roles.SingleOrDefault(r => r.RoleID == row);
        RoleNametxt.Text = role.RoleName;
        var siteFunctionsinRole = db.RoleSiteAccesses.Where(r=> r.RoleIDFK == row);
        foreach(var sitefunc in  siteFunctionsinRole){
            ((RadioButtonList)SiteArea.FindControl(sitefunc.SectionOnSite)).Items[0].Selected = true;
            ((RadioButtonList)SiteArea.FindControl(sitefunc.SectionOnSite)).Items[1].Selected = false;
        }
    }

    protected void AddButton_Click(object sender, EventArgs e)
    {
        // make sure at lease one section of the site has been selected for access
        List<string> AccessAreas = new List<string>();
        foreach (Control c in RadioButtonPlaceholder.Controls)
        {
            if ((c != null) && (c is RadioButtonList))
            {
                if (((RadioButtonList)c).SelectedValue == "Allow")
                {
                    AccessAreas.Add(((RadioButtonList)c).ID);
                }
            }
        }

        if (AccessAreas.Count == 0)
        {
            MsgToShow.Show(MyMessageBox.MessageType.Warning, "You must grant permission to at least one section in the admin area to add this role.");
            MsgToShow.Visible = true;
        }
        else
        {

            db = new DatabaseEntities();

                var roles = db.Roles;
                int row = rowID();
                if (!roles.Where(r => r.RoleName == RoleNametxt.Text && r.RoleID != row).Any())
                {
                    // add role name
                    var role = roles.SingleOrDefault(r => r.RoleID == row);

                    role.RoleName = RoleNametxt.Text;

                    var dbRoleSiteFunctions = db.RoleSiteAccesses.Where(rs => rs.RoleIDFK == row);
                    foreach (var sitefunc in dbRoleSiteFunctions)
                    {
                        db.RoleSiteAccesses.Remove(sitefunc);
                    }
                    foreach (var area in AccessAreas)
                    {
                        RoleSiteAccess RoleSiteAccess = new RoleSiteAccess
                        {
                            RoleIDFK = row,
                            SectionOnSite = area
                        };
                        db.RoleSiteAccesses.Add(RoleSiteAccess);
                    }
                    db.SaveChanges();

                    Response.Redirect("manageRoles.aspx?MsgToShow=Role updated successfully.&MsgType=2");
                }
                else
                {
                    MsgToShow.Show(MyMessageBox.MessageType.Warning, "That role already exists. Please use a different role name.");
                    MsgToShow.Visible = true;
                }
        }
    }
}
