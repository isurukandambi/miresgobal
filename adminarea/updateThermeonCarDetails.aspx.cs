﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using ORM;
using System.Globalization;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;
using CMSConfigs;
using CMSUtils;

public partial class adminarea_updateThermeonCarDetails : System.Web.UI.Page
{
    private DatabaseEntities db;
    protected void Page_Load(object sender, EventArgs e)
    {
        Master.Page.Title = CMSSettings.SiteTitle + " - " + PageName.Text;
        if (Session["LoggedInName"] == null)
        {
            Response.Redirect("default.aspx?TimeOut=y&ReturnPage=" + Server.UrlEncode(HttpContext.Current.Request.Url.AbsoluteUri));
        }
        if (!Page.IsPostBack)
        {
            LoadGroups();
            LoadBrands();
            LoadCountries();
            LoadData();
        }
    }

    protected void LoadGroups()
    {
        db = new DatabaseEntities();
        CarGroupID.DataSource = db.CarGroups.OrderBy(c => c.CarCode).ToList();
        CarGroupID.DataValueField = "CarGroupID";
        CarGroupID.DataTextField = "CarCode";
        CarGroupID.DataBind();
    }
    protected void LoadBrands()
    {
        db = new DatabaseEntities();
        BrandID.DataSource = db.Brands.OrderBy(c => c.BrandID).ToList();
        BrandID.DataValueField = "BrandID";
        BrandID.DataTextField = "BrandName";
        BrandID.DataBind();
    }
    protected void LoadCountries()
    {
        db = new DatabaseEntities();
        CountryID.DataSource = db.Countries.OrderBy(c => c.CountryName).ToList();
        CountryID.DataValueField = "CountryID";
        CountryID.DataTextField = "CountryName";
        CountryID.DataBind();
    }

    protected void LoadData()
    {
        db = new DatabaseEntities();
        int row = rowID();
        var VehicleResources = db.Thermeon_CarMapping.SingleOrDefault(l => l.TCMId == row);
        var VehicleDetails = db.Thermeon_CarDetails.SingleOrDefault(l => l.TCId == VehicleResources.ThermeonCarId);
        if (VehicleResources != null)
        {
            CountryID.SelectedValue = VehicleResources.CountryId.ToString();
            ExistingCountryID.Value = VehicleResources.CountryId.ToString();
            CarGroupID.SelectedValue = VehicleResources.CarCode.ToString();
            Description.Text = VehicleDetails.Car_Description.Trim();
            //BrandID.Text = VehicleDetails.BrandIDFK.ToString();
            OldImage.Value = VehicleDetails.CarImage_Path;
            if (!string.IsNullOrEmpty(OldImage.Value))
            {
                CurrentImage.Text = "<img src=\"../Pics/Thermeon_CarImages/" + VehicleDetails.CarImage_Path + "\" />";
            }
            else
            {
                CurrentImage.Text = "no image uploaded";
            }

        }
        else
        {
            Response.Redirect("manageThermeonVehicleDetails.aspx?MsgType=4&MsgToShow=No Car Group found to Modify.");
        }
    }
    protected int rowID()
    {
        int rowID;
        bool result = int.TryParse(Request.QueryString["Row"], out rowID);
        if (result)
        {
            return rowID;
        }
        return 0;
    }
    public void RemoveFiles(string ImageToRemove)
    {
        string UpPath = Request.PhysicalApplicationPath + "Pics\\Thermeon_CarImages\\" + ImageToRemove;
        Response.Write(UpPath);
        FileInfo OriginalFile = new FileInfo(UpPath);
        if (OriginalFile.Exists)
        {
            File.Delete(UpPath);
        }
    }
    protected void hideCurrentImageControls()
    {
        CurrentImageText.Visible = false;
        CurrentImageControlsHolder.Visible = false;
    }

    protected void UpdateButton_Click(object sender, EventArgs e)
    {

        try
        {
            db = new DatabaseEntities();
            string MsgToShowText = "";
            string filename = "";
            string appPath = Request.PhysicalApplicationPath;
            string FilePathOrig = "";
            string UpPath = appPath + "\\Pics\\Thermeon_CarImages\\";
            if (!Directory.Exists(UpPath))
            {
                Directory.CreateDirectory(UpPath);
            }
            bool ErrorsWithFile = false;

            if (filPhoto.PostedFile.ContentLength == 0)
            {

                filename = "";
                if (!string.IsNullOrEmpty(OldImage.Value))
                {
                    filename = OldImage.Value;
                }
            }
            else if ((filPhoto.PostedFile != null) && (filPhoto.PostedFile.ContentLength > 0))
            {
                filename = cleanFilePath(filPhoto);

                //check filename does not contain any illegal character
                if (!CMSUtilities.FilenameChecker(filename))
                {
                    MsgToShowText += "The uploaded file is not a valid file type.<br />";
                    ErrorsWithFile = true;
                }
                //check file is a valid image file and correct extension
                System.Text.RegularExpressions.Regex imageFilenameRegex = new System.Text.RegularExpressions.Regex(@"(.*?)\.(jpg|jpeg|png|gif)$");
                if ((!CMSUtilities.ImageChecker(filename, filPhoto.PostedFile.InputStream)) || (!imageFilenameRegex.IsMatch(filename)))
                {
                    MsgToShowText += "The file uploaded is not a valid image file.<br />";
                    ErrorsWithFile = true;
                }
                //check file is not too big
                if (filPhoto.PostedFile.ContentLength > 3145728) //3MB
                {
                    MsgToShowText += "Image too big. Try again.<br />";
                    ErrorsWithFile = true;
                }
                //check filename does not already exist
                if (File.Exists(UpPath + filename))
                {
                    MsgToShowText += "An image with the same filename already exists. Please rename your file before uploading.<br />";
                    //ErrorsWithFile = true;
                    RemoveFiles(filename);

                }

                //if everything above is ok save the image
                if (ErrorsWithFile == false)
                {
                    FilePathOrig = UpPath + filename;
                    CMSUtilities.Resize(FilePathOrig, 140, 80, false, filPhoto.PostedFile.InputStream);
                }

            }
            if (ErrorsWithFile == false)
            {
                int row = rowID();
                int selectedCarGroupID = Convert.ToInt32(CarGroupID.SelectedValue);
                int selectedBrandID = Convert.ToInt32(BrandID.SelectedValue);
                int selectedCountryID = Convert.ToInt32(CountryID.SelectedValue);


                var anyCargroupsForCountry = (from vdb in db.Thermeon_CarMapping
                                              where vdb.CountryId == selectedCountryID && vdb.CarCode == selectedCarGroupID
                                              select new
                                              {
                                                  vdb.ThermeonCarId
                                              });
                int existingCountryID = Convert.ToInt32(ExistingCountryID.Value);
                if (anyCargroupsForCountry.Count() == 0)
                {
                    try
                    {
                        var VehicleResourceDetails = db.Thermeon_CarMapping.SingleOrDefault(a => a.TCMId == row);
                        var VehicleDetails = db.Thermeon_CarDetails.SingleOrDefault(b => b.TCId == VehicleResourceDetails.ThermeonCarId);

                        int brandID = Convert.ToInt32(BrandID.SelectedValue);

                        VehicleDetails.CarImage_Path = filename;
                        VehicleDetails.Car_Description = Description.Text;
                        VehicleResourceDetails.CarCode = selectedCarGroupID;
                        //VehicleDetails.BrandIDFK = brandID;
                        //var VehicleResourceDetails = db.VehicleResources.SingleOrDefault(a => a.VDBIDFK == row && a.CountryIDFK == existingCountryID);
                        VehicleResourceDetails.CountryId = selectedCountryID;
                        //Response.Write(row + " ----------- "+ selectedCountryID+"-------------------" +VehicleResourceDetails.CountryIDFK);
                        db.SaveChanges();
                        Response.Redirect("manageThermeonVehicleDetails.aspx?MsgToShow=Vehical Details updated successfully.&MsgType=2&Row=" + selectedCountryID);
                    }
                    catch (Exception ex)
                    {
                        MsgToShow.Show(MyMessageBox.MessageType.Error, "There was an error adding this information. Please try again." + ex.Message + "</br>" + ex.StackTrace);
                        MsgToShow.Visible = true;
                    }
                }
                else
                {
                    MsgToShow.Show(MyMessageBox.MessageType.Warning, "That Car Brand or Car Group already for selected Country. Please use another Car Brand or Car Group for another country.");
                    MsgToShow.Visible = true;
                }

            }
            else
            {
                MsgToShow.Show(MyMessageBox.MessageType.Warning, MsgToShowText);
                MsgToShow.Visible = true;
            }



        }
        catch (Exception ex)
        {
            MsgToShow.Show(MyMessageBox.MessageType.Error, "There was an error adding this information. Please try again." + ex.Message + "</br>" + ex.StackTrace);
            MsgToShow.Visible = true;
        }


    }

    protected string cleanFilePath(HtmlInputFile fileInfo)
    {
        string fileName = Path.GetFileNameWithoutExtension(fileInfo.PostedFile.FileName);
        string fileExtension = Path.GetExtension(fileInfo.PostedFile.FileName);
        fileName = Server.HtmlEncode(fileName);
        //fileName = fileName + DateTime.Now;
        fileName = fileName.Replace(".", "");
        fileName = fileName.Replace(" ", "");
        fileName = fileName.Replace("/", "");
        fileName = fileName.Replace(":", "");
        fileName = fileName.Replace("&", "");
        fileName = fileName.ToUpper();
        fileName = fileName + fileExtension;
        return fileName;
    }
}