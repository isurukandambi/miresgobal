﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ORM;
using System.Linq;
using CMSConfigs;

public partial class manageBrandMapping : System.Web.UI.Page
{
    private DatabaseEntities db;
    protected void Page_Load(object sender, EventArgs e)
    {
        Master.Page.Title = CMSSettings.SiteTitle + " - Edit Section - Manage Brand mapping";

        if (Session["LoggedInName"] == null)
        {
            Response.Redirect("default.aspx?TimeOut=y&ReturnPage=" + Server.UrlEncode(HttpContext.Current.Request.Url.AbsoluteUri));
        }

        if (!Page.IsPostBack)
        {
            DisplayActionMessage();
            LoadCountries();
            LoadBrands();
            LoadPartners();
            LoadMappedLocations();
            
        }
    }
    protected int CountryID()
    {
        int rowID;
        bool result = int.TryParse(Request.QueryString["Country"], out rowID);
        if (result)
        {
            return rowID;
        }
        return 0;
    }

    protected int PartnerID()
    {
        int rowID;
        bool result = int.TryParse(Request.QueryString["Partner"], out rowID);
        if (result)
        {
            return rowID;
        }
        return 0;
    }

    protected int BrandID()
    {
        int rowID;
        bool result = int.TryParse(Request.QueryString["Brand"], out rowID);
        if (result)
        {
            return rowID;
        }
        return 0;
    }

    protected void LoadCountries()
    {
        db = new DatabaseEntities();
        ddlCountries.DataSource = db.Countries.OrderBy(c => c.CountryName).ToList();
        ddlCountries.DataTextField = "CountryName";
        ddlCountries.DataValueField = "CountryID";
        ddlCountries.DataBind();
        ddlCountries.Items.Insert(0, new ListItem("Choose a Country", "0"));
        ddlCountries.SelectedValue = CountryID().ToString();
    }

    protected void LoadBrands()
    {
        db = new DatabaseEntities();
        ddlBrands.DataSource = db.Brands.OrderBy(b => b.BrandName).ToList();
        ddlBrands.DataTextField = "BrandName";
        ddlBrands.DataValueField = "BrandID";
        ddlBrands.DataBind();
        ddlBrands.Items.Insert(0, new ListItem("All Brands", "0"));
        ddlBrands.SelectedValue = BrandID().ToString();
    }

    protected void LoadPartners()
    {
        db = new DatabaseEntities();
        ddlPartners.DataSource = db.Partners.OrderBy(b => b.DomainName).ToList();
        ddlPartners.DataTextField = "DomainName";
        ddlPartners.DataValueField = "PartnerId";
        ddlPartners.DataBind();
        ddlPartners.Items.Insert(0, new ListItem("All Partners", "0"));
        ddlPartners.SelectedValue = PartnerID().ToString();
    }

    protected void CountryDDL_Changed(object sender, EventArgs e)
    {
        Response.Redirect("manageBrandMapping.aspx?Country=" + Convert.ToInt32(ddlCountries.SelectedValue) + "&Brand=" + Convert.ToInt32(ddlBrands.SelectedValue) + "&Partner=" + Convert.ToInt32(ddlPartners.SelectedValue));
    }
    protected void BrandDDL_Changed(object sender, EventArgs e)
    {
        Response.Redirect("manageBrandMapping.aspx?Country=" + Convert.ToInt32(ddlCountries.SelectedValue) + "&Brand=" + Convert.ToInt32(ddlBrands.SelectedValue) + "&Partner="+Convert.ToInt32(ddlPartners.SelectedValue));
    }

    protected void PartnerDDL_Changed(object sender, EventArgs e)
    {
        Response.Redirect("manageBrandMapping.aspx?Country=" + Convert.ToInt32(ddlCountries.SelectedValue) + "&Brand=" + Convert.ToInt32(ddlBrands.SelectedValue) + "&Partner=" + Convert.ToInt32(ddlPartners.SelectedValue));
    } 
    protected void LoadMappedLocations()
    {
        db = new DatabaseEntities();
        int countryID = CountryID();
        int brandID = BrandID();
        int partnerID = PartnerID();
        if (countryID != 0 && brandID != 0 && partnerID != 0)
        {
            var BrandLocation = (from bl in db.BrandLocations
                                 join l in db.Locations on bl.LocationIDFK equals l.LocationID
                                 join b in db.Brands on bl.BrandIDFK equals b.BrandID
                                 join p in db.Partners on bl.PartnerIDFK equals p.PartnerId
                                 where bl.CountryIDFK == countryID && bl.PartnerIDFK == partnerID 
                                 &&  bl.BrandIDFK == brandID
                                 orderby l.LocationName
                                 select new
                                 {
                                     bl.BrandLocationID,
                                     l.LocationName,
                                     l.LocationID,
                                     BrandCount = db.BrandLocations.Where(bc => bc.LocationIDFK == l.LocationID).Count(),
                                     b.BrandName,
                                     p.DomainName,
                                     bl.VehicleLimit
                                 });
            if (BrandLocation.Count() > 0)
            {
                InformationTable.Visible = true;
                DeleteItems.Visible = true;
            }
            else
            {
                NoRecordsFound.Visible = true;
            }
            InformationTable.DataSource = BrandLocation.ToList();
            InformationTable.DataBind();

        }
        else if (countryID != 0 && brandID != 0)
        {
            var BrandLocation= (from bl in db.BrandLocations
                                    join l in db.Locations on bl.LocationIDFK equals l.LocationID
                                    join b in db.Brands on bl.BrandIDFK equals b.BrandID
                                    join p in db.Partners on bl.PartnerIDFK equals p.PartnerId
                                    where bl.CountryIDFK == countryID && bl.BrandIDFK == brandID
                                    orderby l.LocationName
                                    select new
                                    {
                                        bl.BrandLocationID,
                                        l.LocationName,
                                        l.LocationID,
                                        BrandCount = db.BrandLocations.Where(bc => bc.LocationIDFK == l.LocationID).Count(),
                                        b.BrandName,
                                        p.DomainName,
                                        bl.VehicleLimit
                                    });
            if (BrandLocation.Count() > 0)
            {
                InformationTable.Visible = true;
                DeleteItems.Visible = true;
            }
            else
            {
                NoRecordsFound.Visible = true;
            }
            InformationTable.DataSource = BrandLocation.ToList();
            InformationTable.DataBind();
        }
        else if (countryID != 0 && partnerID != 0)
        {
            var BrandLocation= (from bl in db.BrandLocations
                                    join l in db.Locations on bl.LocationIDFK equals l.LocationID
                                    join b in db.Brands on bl.BrandIDFK equals b.BrandID
                                    join p in db.Partners on bl.PartnerIDFK equals p.PartnerId
                                    where bl.CountryIDFK == countryID && bl.PartnerIDFK == partnerID
                                    orderby l.LocationName
                                    select new
                                    {
                                        bl.BrandLocationID,
                                        l.LocationName,
                                        l.LocationID,
                                        BrandCount = db.BrandLocations.Where(bc => bc.LocationIDFK == l.LocationID).Count(),
                                        b.BrandName,
                                        p.DomainName,
                                        bl.VehicleLimit
                                    });
            if (BrandLocation.Count() > 0)
            {
                InformationTable.Visible = true;
                DeleteItems.Visible = true;
            }
            else
            {
                NoRecordsFound.Visible = true;
            }
            InformationTable.DataSource = BrandLocation.ToList();
            InformationTable.DataBind();
        }
        else
        {

            var BrandLocationD = (from bl in db.BrandLocations
                                    join l in db.Locations on bl.LocationIDFK equals l.LocationID
                                    join b in db.Brands on bl.BrandIDFK equals b.BrandID
                                    join p in db.Partners on bl.PartnerIDFK equals p.PartnerId
                                  where bl.PartnerIDFK == partnerID
                                  orderby l.LocationName
                                    select new
                                    {
                                        bl.BrandLocationID,
                                        l.LocationName,
                                        l.LocationID,
                                        BrandCount = db.BrandLocations.Where(bc => bc.LocationIDFK == l.LocationID).Count(),
                                        b.BrandName,
                                        p.DomainName,
                                        bl.VehicleLimit
                                    });
            if (BrandLocationD.Count() > 0)
            {
                InformationTable.Visible = true;
                DeleteItems.Visible = true;
            }
            else
            {
                NoRecordsFound.Visible = true;
            }
            InformationTable.DataSource = BrandLocationD.ToList();
            InformationTable.DataBind();

        }
        
    }
    protected void DeleteItems_Click(object sender, EventArgs e)
    {
        ConfirmBox.ShowConfirmBox("Delete Brand Mappings", "Are you sure you want to remove selected Brand Mappings from system?", "Delete|Cancel");
        ConfirmBox.Visible = true;
    }
    protected void Confirm_Fired(object sender, EventArgs e)
    {
        bool confirm = (bool)sender;
        if (confirm == true)
        {
            DeleteDatabaseItem(InformationTable);
        }
    }
    protected void DeleteDatabaseItem(Repeater RepeaterID)
    {
        string message = "";
        //initialize the objectContext
        db = new DatabaseEntities();
        foreach (RepeaterItem item in RepeaterID.Items)
        {
            CheckBox cb = (CheckBox)item.FindControl("rowcheck");
            if (cb != null && cb.Checked)
            {
                Literal DBrow = (Literal)item.FindControl("rowID");
                int row = Convert.ToInt16(DBrow.Text);
                message += DeleteItem(row, db);
            }
        }
        if (string.IsNullOrEmpty(message))
        {
            Response.Redirect("manageBrandMapping.aspx?MsgType=2&MsgToShow=Item(s) deleted successfully.&Country=" + CountryID() + "&Brand="+BrandID());
        }
        else
        {
            Response.Redirect("manageBrandMapping.aspx?MsgType=4&MsgToShow=" + message+"&Country=" + CountryID() + "&Brand="+BrandID());
        }

    }
   

    protected string DeleteItem(int deleteItemID, DatabaseEntities dbCon)
    {
        string message = "";
        var item = dbCon.BrandLocations.SingleOrDefault(bl => bl.BrandLocationID == deleteItemID);
        try
        {
            var carGroupsInBrandLoc = dbCon.CarGroupBrandMappings.Where(cg => cg.BrandLocationIDFK == deleteItemID);
            foreach (var cargroup in carGroupsInBrandLoc)
            {
                dbCon.CarGroupBrandMappings.Remove(cargroup);
                
            }
            dbCon.BrandLocations.Remove(item);
            dbCon.SaveChanges();
        }
        catch
        {
            //dbCon.BrandLocations.Detach(item);
            message = "Error occurred while deleting " + item.Location.LocationName + " - " + item.Brand.BrandName + ". Brand mapping already in use. |";
        }
        return message;
    }

    protected void GridView_MouseOver(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowIndex >= 0)
        {
            e.Row.Attributes.Add("onmouseover", "this.style.background='#FFFFFF'");
            e.Row.Attributes.Add("onmouseout", "this.style.background='#F7F7F7'");
        }
    }
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            int LocID = Convert.ToInt32(((HiddenField)e.Row.FindControl("LocationID")).Value);
            //Response.Write(LocID);
            ////Response.Write((e.Row.RowIndex+1) + " ------------- " + brandCount + " ---------------------- " + (e.Row.RowIndex+1) % brandCount + "</br>");
            ////if ((e.Row.RowIndex+1) % brandCount == 0)
            ////{
            ////    e.Row.Cells[0].Attributes.Add("rowspan", brandCount.ToString());
            ////}
        }
    }
    protected void InformationTable_RowDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            int locID = Convert.ToInt16(((HiddenField)e.Item.FindControl("LocationID")).Value);
            int brandLocID = Convert.ToInt16(((Literal)e.Item.FindControl("rowID")).Text);
            Label LocationLink = ((Label)e.Item.FindControl("LocationLink"));
            //if (e.Item.ItemIndex != 0)
            //{
            //  int prevID =  Convert.ToInt16(((HiddenField)InformationTable.Items[e.Item.ItemIndex-1].FindControl("LocationID")).Value);
            //  Response.Write(prevID + " ----- " + locID + "</br>");
            //  if (prevID != locID)
            //  {
            //      LocationLink.Visible = true;
            //  }
            //  else
            //  {
            //      LocationLink.Visible = false;
            //  }
            //}


            Literal CarCodesLit = ((Literal)e.Item.FindControl("CarCodes"));
            //LoadCarGroups(((Literal)e.Item.FindControl("CarCodes")), brandLocID);
            db = new DatabaseEntities();
            try
            {
                var carGroups = (from cgb in db.CarGroupBrandMappings
                                 join cg in db.CarGroups on cgb.CarGroupIDFK equals cg.CarGroupID
                                 where cgb.BrandLocationIDFK == brandLocID
                                 select new
                                 {
                                     cg.CarCode
                                 });
                foreach (var cg in carGroups)
                {
                    CarCodesLit.Text += cg.CarCode + ", ";
                }
                //remove last comma from car groups
                if (!string.IsNullOrEmpty(CarCodesLit.Text))
                {
                    CarCodesLit.Text = CarCodesLit.Text.Substring(0, CarCodesLit.Text.Length - 2);
                }
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
            }
        }
    }
    protected void DisplayActionMessage()
    {
        if ((Request.QueryString["MsgType"] != null) && (Request.QueryString["MsgToShow"] != null))
        {
            string msgToShow = Request.QueryString["MsgToShow"].ToString();
            int rowID;
            bool result = int.TryParse(Request.QueryString["MsgType"], out rowID);
            if ((result) && !String.IsNullOrEmpty(msgToShow))
            {
                switch (Convert.ToInt16(Request.QueryString["MsgType"]))
                {
                    case 1:
                        MsgToShow.Show(MyMessageBox.MessageType.Info, msgToShow);
                        break;
                    case 2:
                        MsgToShow.Show(MyMessageBox.MessageType.Success, msgToShow);
                        break;
                    case 3:
                        MsgToShow.Show(MyMessageBox.MessageType.Warning, msgToShow);
                        break;
                    case 4:
                        MsgToShow.Show(MyMessageBox.MessageType.Error, msgToShow.Replace("|", "</br>"));
                        break;
                    default:
                        goto case 1;
                }
                MsgToShow.Visible = true;
            }
        }
    }
}
