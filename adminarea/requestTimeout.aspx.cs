﻿using System;
using System.Linq;
using System.Web.UI.WebControls;
using ORM;
using System.Web.UI;
using System.Collections.Generic;
using System.Web;
using CMSConfigs;

public partial class salesReports : System.Web.UI.Page
{
    private DatabaseEntities db;
    //   salesReports
    protected void Page_Load(object sender, EventArgs e)
    {
        Master.Page.Title = CMSSettings.SiteTitle + " - " + "Timeout Request Report Log";

        if (!Page.IsPostBack)
        {
            PopulateTables();
        }
    }

    protected void PopulateTables()
    {
        //initialize the objectContext
        db = new DatabaseEntities();
        //var partnerBrand = (from m in db.RequestTimeOutLogs
        //                    orderby m.LogDate
        //                    select new { m.RequestTimeoutID, m.RequestCameTime, m.XMLProviderSendTime, m.XMLProviderReceviedTime, m.RequestDomain,m.Brand, m.LogDate });
        
        //}

        //InformationTable.DataSource = partnerBrand;
        //InformationTable.DataBind();

        //if (partnerBrand.Count() > 0)
        //{
        //    InformationTable.Visible = true;
        //    DeleteItems.Visible = true;
        //}
        //else
        //{
        //    NoRecordsFound.Visible = true;
        //}
            

        var Jobs = (from m in db.RequestTimeOutLogs
                            orderby m.LogDate
                            select new { m.RequestTimeoutID, 
                                         m.RequestCameTime, 
                                         m.XMLProviderSendTime, 
                                         m.XMLProviderReceviedTime, 
                                         m.RequestDomain,m.Brand, m.LogDate });

        InformationTable.DataSource = Jobs.ToList();
        InformationTable.DataBind();

        if (Jobs.Count() > 0)
        {
            InformationTable.Visible = true;
        }
        else
        {
            NoRecordsFound.Visible = true;
        }
    }

    //protected string getJobSuites(int jobID)
    //{
    //    db = new DatabaseEntities();
    //    string suites = "";
    //    var testSuites = (from js in db.JobSuites
    //                      join j in db.Jobs on js.JobIDFK equals jobID
    //                      join su in db.Suites on js.SuiteIDFK equals su.SuiteID
    //                      select new
    //                      {
    //                          su.SuiteName
    //                      }
    //                  ).Distinct().ToList();
    //    suites = String.Join(",", testSuites.ToList());
    //    suites = suites.Replace("{", "");
    //    suites = suites.Replace("}", "");
    //    suites = suites.Replace("=", "");
    //    suites = suites.Replace("SuiteName", "");
    //    suites = suites.Replace(",", "</br>");
    //    if (string.IsNullOrEmpty(suites)) {
    //        suites = "-";
    //    }
    //    return suites;
    //}
    //protected string getJobTests(int jobID)
    //{
    //    db = new DatabaseEntities();
    //    string tests = "";
    //    var testsDB = (from jt in db.JobTests
    //                      join j in db.Jobs on jt.JobIDFK equals jobID
    //                      join t in db.Tests on jt.TestIDFK equals t.TestID
    //                      select new
    //                      {
    //                          t.TestName
    //                      }
    //                  ).Distinct().ToList();
    //    tests = String.Join(",", testsDB.ToList());
    //    tests = tests.Replace("{", "");
    //    tests = tests.Replace("}", "");
    //    tests = tests.Replace("=", "");
    //    tests = tests.Replace("TestName", "");
    //    tests = tests.Replace(",", "</br>");
    //    if (string.IsNullOrEmpty(tests))
    //    {
    //        tests = "-";
    //    }
    //    return tests;
    //}
}