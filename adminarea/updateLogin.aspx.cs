﻿using System;
using System.Linq;
using ORM;
using CMSConfigs;

public partial class updateLogin : System.Web.UI.Page
{
    private DatabaseEntities db;

    protected void Page_Load(object sender, EventArgs e)
    {
        Master.Page.Title = CMSSettings.SiteTitle + " - " + PageName.Text;

        if (!Page.IsPostBack)
        {
            LoadData();
        }
    }

    protected int rowID()
    {
        int rowID;
        bool result = int.TryParse(Session["LoginID"].ToString(), out rowID);
        if (result)
        {
            return rowID;
        }
        return 0;
    }

    protected void LoadData()
    {
        //initialize the objectContext
        db = new DatabaseEntities();

        int row = rowID();
        var dbValue = db.Logins.SingleOrDefault(l => l.LoginID == row);

        if (dbValue != null)
        {
            LoggedInName.Text = dbValue.LoggedInName;
            Username.Text = dbValue.Username;
            Email.Text = dbValue.Email;
        }
        else
        {
            Response.Redirect("home.aspx");
        }
    }

    protected void UpdateLogin_Click(object sender, EventArgs e)
    {
        if (Password.Text != ConfPassword.Text)
        {
            MsgToShow.Show(MyMessageBox.MessageType.Warning, "Passwords do not match. Please re-enter password.");
            MsgToShow.Visible = true;
        }
        else
        {
            try
            {
                //initialize the objectContext
                db = new DatabaseEntities();

                int row = rowID();
                var login = db.Logins.SingleOrDefault(l => l.LoginID == row);
                if (!db.Logins.Where(l => l.Username == Username.Text && l.LoginID != row).Any())
                {
                    login.LoggedInName = LoggedInName.Text;
                    login.Username = Username.Text;
                    login.Password = Password.Text;
                    login.Email = Email.Text;

                    db.SaveChanges();


                    //update your session to reflect the new changes
                    Session["Username"] = login.Username;
                    Session["Password"] = login.Password;
                    Session["LoggedInName"] = login.LoggedInName;
                    Session["LoggedInEmail"] = login.Email;

                    MsgToShow.Show(MyMessageBox.MessageType.Success, "Login updated successfully.");
                    MsgToShow.Visible = true;
                }
                else
                {
                    MsgToShow.Show(MyMessageBox.MessageType.Warning, "That username already exists. Please use another username.");
                    MsgToShow.Visible = true;
                }
            }
            catch
            {
                MsgToShow.Show(MyMessageBox.MessageType.Error, "There was an error updating this information. Please try again.");
                MsgToShow.Visible = true;
            }
        }
    }
}
