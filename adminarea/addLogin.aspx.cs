﻿using System;
using System.Linq;
using System.Text;
using ORM;
using CMSConfigs;
using CMSUtils;

public partial class addLogin : System.Web.UI.Page
{
    private DatabaseEntities db;

    protected void Page_Load(object sender, EventArgs e)
    {
        Master.Page.Title = CMSSettings.SiteTitle + " - " + PageName.Text;

        if (!Page.IsPostBack)
        {
            LoadRoles();
        }
    }

    protected void LoadRoles()
    {
        // initialize the objectContext
        db = new DatabaseEntities();

        SiteRoles.DataSource = db.Roles.OrderBy(r => r.RoleName).ToList();
        SiteRoles.DataTextField = "RoleName";
        SiteRoles.DataValueField = "RoleID";
        SiteRoles.DataBind();

    }

    protected void AddLogin_Click(object sender, EventArgs e)
    {
        if (Password.Text != ConfPassword.Text)
        {
            MsgToShow.Show(MyMessageBox.MessageType.Warning, "Passwords do not match. Please re-enter password.");
            MsgToShow.Visible = true;
        }
        else
        {
            Login Login = new Login
            {
                LoggedInName = LoggedInName.Text,
                Username = Username.Text,
                Password = Password.Text,
                Email = Email.Text,
                RoleIDFK = Convert.ToInt16(SiteRoles.SelectedValue)
            };

            try
            {
                // initialize the objectContext
                db = new DatabaseEntities();

                if (!db.Logins.Where(l => l.Username == Username.Text).Any())
                {
                    db.Logins.Add(Login);
                    db.SaveChanges();

                    // email the user their login details
                    string EmailMessage = BuildEmail(Username.Text, Password.Text, LoggedInName.Text);
                    CMSUtilities.SendMessage(CMSSettings.SiteEmailsSentFrom, Email.Text, "Your " + CMSSettings.SiteTitle + " admin area login details.", EmailMessage, true);

                    MsgToShow.Show(MyMessageBox.MessageType.Success, "'" + Username.Text + "' saved successfully.<br />Use the form below to add another item or <a href=\"manageLogins.aspx\">click here to manage existing items.</a>");
                    MsgToShow.Visible = true;

                    LoggedInName.Text = "";
                    Username.Text = "";
                    Password.Text = "";
                    ConfPassword.Text = "";
                    Email.Text = "";
                    SiteRoles.SelectedIndex = 0;
                }
                else
                {
                    MsgToShow.Show(MyMessageBox.MessageType.Warning, "That username already exists. Please use another username.");
                    MsgToShow.Visible = true;
                }

            }
            catch
            {
                MsgToShow.Show(MyMessageBox.MessageType.Error, "There was an error adding this information. Please try again.");
                MsgToShow.Visible = true;
            }
        }
    }

    protected string BuildEmail(string Username, string Password, string LoggedInName)
    {
        StringBuilder EmailTemplate = new StringBuilder();

        EmailTemplate.Append("<table>");
        EmailTemplate.Append("<tr><td>");

        EmailTemplate.Append("Hello " + LoggedInName + ",<br /><br />Your account for the " + CMSSettings.SiteTitle + " admin area is now set up.");
        EmailTemplate.Append("<br /><br />");

        EmailTemplate.Append("To log into the system simply click on the link below and enter in the username and password shown below:<br />");
        EmailTemplate.Append("Link: <a href=\"http://" + CMSSettings.SiteUrl + "/adminarea\">http://" + CMSSettings.SiteUrl + "/adminarea</a><br />");
        EmailTemplate.Append("Username: " + Username + "<br />");
        EmailTemplate.Append("Password: " + Password + "<br />");
        EmailTemplate.Append("<br />");

        EmailTemplate.Append("Once you have logged in you can change your password by going to the Manage Login section of the site.");
        EmailTemplate.Append("<br /><br />");

        EmailTemplate.Append("Regards,<br />The " + CMSSettings.SiteTitle + " Admin Area system.");

        EmailTemplate.Append("</td></tr>");
        EmailTemplate.Append("</table>");

        return EmailTemplate.ToString();
    }
}