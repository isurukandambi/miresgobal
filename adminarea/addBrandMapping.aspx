﻿<%@ Page Language="C#" MasterPageFile="adminarea.master" AutoEventWireup="true" CodeFile="addBrandMapping.aspx.cs" Inherits="addBrandMapping" %>

<%@ Register Src="~/adminarea/controls/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="js/validationEngine/css/validationEngine.jquery.css" rel="stylesheet" type="text/css" />
    <script src="js/validationEngine/jquery.validationEngine.js" type="text/javascript"></script>
    <script src="js/validationEngine/jquery.validationEngine-en.js" type="text/javascript"></script>
    <script type="text/javascript" language="javascript">
        $(document).ready(function () {
            $("#form1").validationEngine();
            $('.toggleAllCarGroups').attr('checked', true);

            $('#toggleAllLocations').change(function (evt) {
                $('#ContentPlaceHolder1_Locationsddl :checkbox').attr('checked', function () {
                    if ($(this).attr("disabled") == false) {
                        return !this.checked;
                    }
                });
            });
            $('.toggleAllCarGroups').change(function (evt) {
                $(this).parent().next().find(":checkbox").attr('checked', function () {
                    return !this.checked;
                });
            });

            $('.toggleAllCarGroups').parent().next().find(":checkbox").attr('checked', function () {
                return this.checked;
            });
        });
    </script>
    <style type="text/css">
        .brandsHolder {
            width: 99%;
        }

            .brandsHolder select {
                margin: 15px 6%;
                width: 88%;
            }

        .VehicleLimitddl {
            width: 145px;
        }

        .brandName {
            font-weight: bold;
            width: 200px;
        }

        table {
            width: 350px;
        }

        .brandsHolder table {
            width: 88%;
            margin: 5px 6%;
        }

        .BrandHolderControlPanel {
            width: 26%;
            float: left;
            border: 1px #8bafc1 solid;
            margin: 2px 2%;
            border-radius: 5px;
        }

        .BrandIndicatorCheckBox {
            margin-left: 5%;
            color: darkgray;
            font-size: 20px;
        }

        .BrandHolderControlPanel:first-child {
            margin-left: 0px;
        }

        .checkboxLists {
            max-height: 150px;
            border: 1px solid aliceblue;
            overflow-y: scroll;
        }

        .checkboxListsInBrandPanel {
            max-height: 150px;
            border: 1px solid aliceblue;
            overflow-y: scroll;
            width: 87%;
            margin: 5px 6%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="content">
        <uc1:MyMessageBox ID="MsgToShow" runat="server" ShowCloseButton="true" Visible="false" />

        <h3>
            <asp:Literal ID="PageName" runat="server" Text="Map Locations for Brand" /></h3>
        <div id="actionArea">
            <div class="tableLegend">
                <h4>Functionality guide</h4>
                <dl>
                    <dd style="margin-left: 0px; margin-top: 19px;">
                        <img src="images/icons/delete.png" width="15" height="15" alt="duplicate icon" />
                        Mapped locations are already selected and disabled. You can modify already assigned locations in edit form. You can Only map brand new locations here. </dd>
                </dl>
            </div>
            <fieldset>
                <div>
                    <label for="PartnerIDFK" class="inline" style="width: 100%; margin-bottom: 0px;">Select Partner</label>
                    <asp:DropDownList ID="PartnerIDFK" runat="server" AutoPostBack="true" OnSelectedIndexChanged="PartnerDDL_Changed" AppendDataBoundItems="true">
                    </asp:DropDownList>
                </div>
                <div>
                    <label for="CountryIDFK" class="inline" style="width: 100%; margin-bottom: 0px;">Select Country</label>
                    <asp:DropDownList ID="CountryIDFK" runat="server" AutoPostBack="true" OnSelectedIndexChanged="CountryDDL_Changed" AppendDataBoundItems="true">
                    </asp:DropDownList>
                </div>

                <div>
                    <label for="VehicleLimitAndBrands" class="inline" style="width: 100%;" runat="server" id="AdvisorText"></label>
                    <asp:Panel ID="BrandsPanel" runat="server" CssClass="brandsHolder">
                    </asp:Panel>
                </div>
                <div>
                    <label for="Locations" class="inline" style="width: 100%; margin-bottom: 0px;">
                        <input type="checkbox" id="toggleAllLocations" style="margin-left: 2px;" />Select Locations</label>
                    <asp:CheckBoxList ID="Locationsddl" runat="server" CssClass="checkboxLists">
                    </asp:CheckBoxList>
                </div>
                <div class="formActions">
                    <asp:Button ID="AddEntry" CssClass="btn" OnClick="AddButton_Click" runat="server" Text="Add Mapped Locations" />
                    <a href="manageBrandMapping.aspx?Country=<%=CountryIDFK.SelectedValue%>" class="CancelButton">Cancel</a>
                </div>
            </fieldset>
        </div>
    </div>
</asp:Content>
