﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ORM;
using System.Linq;
using CMSConfigs;
using CMSUtils;

public partial class manageLocations : System.Web.UI.Page
{
    private DatabaseEntities db;
    protected void Page_Load(object sender, EventArgs e)
    {
        Master.Page.Title = CMSSettings.SiteTitle + " - Edit Section - Manage Locations";

        if (Session["LoggedInName"] == null)
        {
            Response.Redirect("default.aspx?TimeOut=y&ReturnPage=" + Server.UrlEncode(HttpContext.Current.Request.Url.AbsoluteUri));
        }
        if (Request.QueryString["DeleteRow"] != null)
        {

            int locID = Convert.ToInt16(Request.QueryString["DeleteRow"]);
            DeleteItem(locID);
        }
        MsgToShow.Visible = false;
        if (Request.QueryString["MsgToShow"] != null)
        {
            int MsgType = Convert.ToInt16(Request.QueryString["MsgType"]);
            switch (MsgType)
            {
                case 1:
                    MsgToShow.Show(MyMessageBox.MessageType.Info, Request.QueryString["MsgToShow"].ToString());
                    break;
                case 2:
                    MsgToShow.Show(MyMessageBox.MessageType.Success, Request.QueryString["MsgToShow"].ToString());
                    break;
                case 3:
                    MsgToShow.Show(MyMessageBox.MessageType.Warning, Request.QueryString["MsgToShow"].ToString());
                    break;
                case 4:
                    MsgToShow.Show(MyMessageBox.MessageType.Error, Request.QueryString["MsgToShow"].ToString());
                    break;
                default:
                    goto case 1;
            }
            MsgToShow.Visible = true;
        }

        if (!Page.IsPostBack)
        {
            LoadCountries();
            LoadLocations();
        }
    }
    protected int rowID()
    {
        int rowID;
        bool result = int.TryParse(Request.QueryString["Row"], out rowID);
        if (result)
        {
            return rowID;
        }
        return 0;
    }
    protected void LoadCountries()
    {
        db = new DatabaseEntities();
        ddlCountries.DataSource = db.Countries.OrderBy(c => c.CountryName).ToList();
        ddlCountries.DataTextField = "CountryName";
        ddlCountries.DataValueField = "CountryID";
        ddlCountries.DataBind();
        ddlCountries.Items.Insert(0, new ListItem("Choose a Country", "0"));
        ddlCountries.SelectedValue = rowID().ToString();
    }

    protected void CountryDDL_Changed(object sender, EventArgs e)
    {
        Response.Redirect("manageLocations.aspx?Row=" + Convert.ToInt32(ddlCountries.SelectedValue));
    }
    protected void LoadLocations()
    {
        db = new DatabaseEntities();
        int countryID = rowID();
        GridView1.DataSource = (from l in db.Locations
                                orderby l.LocationName
                                where l.CountryIDFK == countryID
                                select new { l.LocationID, l.LocationName, l.LocationCode, l.ExtendedLocationCode });
        GridView1.DataBind();
    }
    protected void DeleteItem(int locationID)
    {
        try
        {
            db = new DatabaseEntities();
            var location = db.Locations.SingleOrDefault(l => l.LocationID == locationID);
            if (location != null)
            {
                db.Locations.Remove(location);
                db.SaveChanges();
                Response.Redirect("manageLocations.aspx?MsgType=2&MsgToShow=Location Removed.&Row=" + location.CountryIDFK);
            }
            else
            {
                Response.Redirect("manageLocations.aspx?MsgType=4&MsgToShow=No Location found to Remove.");
            }
        }
        catch
        {
            Response.Redirect("manageLocations.aspx?MsgType=4&MsgToShow=" + SendEmail.FKconstrainsFail());

        }

    }
    protected void GridView_MouseOver(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowIndex >= 0)
        {
            e.Row.Attributes.Add("onmouseover", "this.style.background='#FFFFFF'");
            e.Row.Attributes.Add("onmouseout", "this.style.background='#F7F7F7'");
        }
    }
}
