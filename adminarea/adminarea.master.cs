﻿using System;
using System.Collections.Generic;
using System.Linq;

public partial class adminareaMaster : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            LoadRoles();
        }
        LoadLoginName();
    }

    protected void LoadLoginName()
    {
        if (Session["LoggedInName"] != null)
        {
            UserLoggedIn.Text = Session["LoggedInName"].ToString();
        }

        //only show Dragnet admin stuff to Dragnet user
        if (Session["Username"].ToString().Trim() != "dragnetsystems")
        {
            DragnetAdmin.Visible = false;
        }
    }

    protected void LoadRoles()
    {
        //go through each section of the site and turn on or off pages depending on access levels
        // make sure the name of the values stored in the database match the name you give the ID's of the HTML elements on the nav
        List<string> SectionOnSite = (List<string>)Session["SectionOnSite"];
        foreach (string section in SectionOnSite.Where(q => q != "Newsletter" && q != "BrokerSection"))
        {

            if ((section == "Dashboard") || (section == "DashboardNoReport"))
            {
                (NavMenu.FindControl("Dashboard")).Visible = true;
            }
            else
            {
                if (NavMenu.FindControl(section) != null)
                    (NavMenu.FindControl(section)).Visible = true;
            }
            
        }

        //use a flag to know when to show or hide the cancel button on the UpdateLogin.aspx page.
        //if left on for 'Update My Login' users below then they could get back to ManageLogins.aspx page
        Session["UpdateLoginNoBackButton"] = false;

        if (LoginsRoles.Visible == false)
        {
            //ManageLoginsLink.NavigateUrl = "updateLogin.aspx";
            //ManageLoginsLink.Text = "Update My Login";
            Session["UpdateLoginNoBackButton"] = true;
            //LoginsRoles.Visible = true;
        }
    }
}
