﻿using System;
using System.Linq;
using System.Text;
using System.Web;
using ORM;
using CMSConfigs;

public partial class addCountry : System.Web.UI.Page
{
    private DatabaseEntities db;

    protected void Page_Load(object sender, EventArgs e)
    {
        Master.Page.Title = CMSSettings.SiteTitle + " - " + PageName.Text;
        if (Session["LoggedInName"] == null)
        {
            Response.Redirect("default.aspx?TimeOut=y&ReturnPage=" + Server.UrlEncode(HttpContext.Current.Request.Url.AbsoluteUri));
        }
        if (!Page.IsPostBack)
        {
            LoadPartners();
            LoadBrands();
        }
    }


    protected void AddButton_Click(object sender, EventArgs e)
    {
        try
        {
         //   Response.Write("Vendor   " + VendorCode.Text.Trim() + "     ");
         //   Response.Write("Rate   " + RateQualifire.Text.Trim() + "<br");
            // initialize the objectContext
            db = new DatabaseEntities();
            int partnerId = Convert.ToInt32(PartnerID.SelectedValue);
            int brandId = Convert.ToInt32(BrandID.SelectedValue);
            int vheRef = 0;




            if (!db.PartnerBrandCodeMappings.Where(p => (p.PartnerId == partnerId && p.BrandId == brandId)).Any())
            {

                PartnerBrandCodeMapping psartnerbarand = new PartnerBrandCodeMapping
                {
                    PartnerId = partnerId,
                    BrandId = brandId,
                    
                    IataCode = IataCodetxt.Text.Trim(),
                    PromotionCode = PromotionCodetxt.Text.Trim(),
                    TravellerNumber=TravellerNumbertxt.Text,
                    Tour_code=Tour_codetxt.Text
                };
                db.PartnerBrandCodeMappings.Add(psartnerbarand);
                db.SaveChanges();

                Response.Redirect("managePartnerBrandCode.aspx?MsgType=2&MsgToShow=Partner with Brand added successfully");



            }
            else
            {
                MsgToShow.Show(MyMessageBox.MessageType.Warning, "Partner and Brand already exists. Please use another Partner and Brand.");
                MsgToShow.Visible = true;
            }

        }
        catch(Exception exx)
        {
            MsgToShow.Show(MyMessageBox.MessageType.Error, "There was an error adding this information. Please try again."+exx.StackTrace);
            MsgToShow.Visible = true;
        }
    }

    protected void LoadBrands()
    {
        db = new DatabaseEntities();
        BrandID.DataSource = db.Brands.OrderBy(c => c.BrandID).ToList();
        BrandID.DataValueField = "BrandID";
        BrandID.DataTextField = "BrandName";
        BrandID.DataBind();
    }

    protected void LoadPartners()
    {
        db = new DatabaseEntities();
        PartnerID.DataSource = db.Partners.OrderBy(c => c.PartnerId).ToList();
        PartnerID.DataValueField = "PartnerId";
        PartnerID.DataTextField = "DomainName";
        PartnerID.DataBind();
    }
}