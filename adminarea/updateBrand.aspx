﻿<%@ Page Language="C#" MasterPageFile="adminarea.master" AutoEventWireup="true" CodeFile="updateBrand.aspx.cs" Inherits="updateBrand" %>
<%@ Register Src="~/adminarea/controls/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <link href="js/validationEngine/css/validationEngine.jquery.css" rel="stylesheet" type="text/css" />
    <script src="js/validationEngine/jquery.validationEngine.js" type="text/javascript"></script>
    <script src="js/validationEngine/jquery.validationEngine-en.js" type="text/javascript"></script>
    <script type="text/javascript" language="javascript">
        $(document).ready(function () {            
            $("#form1").validationEngine();
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div id="content">
    <uc1:MyMessageBox ID="MsgToShow" runat="server" ShowCloseButton="true" Visible="false" />

    <h3><asp:Literal ID="PageName" runat="server" Text="Update Brand" /></h3>
    <div id="actionArea">
    <fieldset>
        <div>
            <label for="Brandname" class="inline">Brand Name </label>
            <asp:TextBox ID="Brandname" CssClass="text inline validate[required]" runat="server" MaxLength="30"></asp:TextBox>
        </div>
        
        <div class="formActions">
            <asp:Button ID="UpdateBrand" CssClass="btn" OnClick="UpdateBrand_Click" runat="server" Text="Update Brand" />
            <a href="manageBrands.aspx" class="CancelButton">Cancel</a>
        </div>
    </fieldset>
    </div>
</div>
</asp:Content>

