﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ORM;
using CMSConfigs;

public partial class adminarea_updateRateQualifier : System.Web.UI.Page
{
    private DatabaseEntities db;
    protected void Page_Load(object sender, EventArgs e)
    {
        Master.Page.Title = CMSSettings.SiteTitle + " - " + PageName.Text;
        if (Session["LoggedInName"] == null)
        {
            Response.Redirect("default.aspx?TimeOut=y&ReturnPage=" + Server.UrlEncode(HttpContext.Current.Request.Url.AbsoluteUri));
        }
        if (!Page.IsPostBack)
        {
            LoadData();
        }
    }

    protected void LoadData()
    {
        db = new DatabaseEntities();
        int row = rowID();
        var RQ = db.Rate_Qulifiers.SingleOrDefault(r => r.RQ_ID == row);
        RateQualifier.Text = RQ.Rate_Qulifier;

    }
    protected int rowID()
    {
        int rowID;
        bool result = int.TryParse(Request.QueryString["Row"], out rowID);
        if (result)
        {
            return rowID;
        }
        return 0;
    }

    protected void UpdateButton_Click(object sender, EventArgs e)
    {
        try
        {
            // initialize the objectContext
            db = new DatabaseEntities();
            int row = rowID();
            if (!db.Rate_Qulifiers.Where(r => r.Rate_Qulifier == RateQualifier.Text.Trim() && r.RQ_ID != row).Any())
            {
                var RateQ = db.Rate_Qulifiers.SingleOrDefault(r => r.RQ_ID == row);
                RateQ.Rate_Qulifier = RateQualifier.Text.Trim();
                db.SaveChanges();

                Response.Redirect("manageRateQulifier.aspx?MsgToShow=Rate Qualifier updated successfully.&MsgType=2");
            }
            else
            {
                MsgToShow.Show(MyMessageBox.MessageType.Warning, "That Rate Qualifier already exists. Please use another  Rate Qualifier.");
                MsgToShow.Visible = true;
            }

        }
        catch
        {
            MsgToShow.Show(MyMessageBox.MessageType.Error, "There was an error adding this information. Please try again.");
            MsgToShow.Visible = true;
        }
    }
}