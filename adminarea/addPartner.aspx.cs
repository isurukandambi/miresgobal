﻿using System;
using System.Linq;
using System.Text;
using ORM;
using System.Collections;

public partial class addPartner : System.Web.UI.Page
{
    private DatabaseEntities db;

    protected void Page_Load(object sender, EventArgs e)
    {
        
    }

   
    protected void Addpartner_Click(object sender, EventArgs e)
    {
        if (txtdn.Text == "")
        {
            MsgToShow.Show(MyMessageBox.MessageType.Warning, "Enter Partner Domian Name.");
            MsgToShow.Visible = true;
            return;
        }
      
        try
        {
            int checkvehiclelimit,checkemail;
            if (chkemail.Checked)
            {
                checkemail = 1;
            }
            else
            {
                checkemail = 0;
            }
            if (chkenable.Checked)
            {
                checkvehiclelimit = 1;
            }
            else
            {

                checkvehiclelimit = 0;
            }
            db = new DatabaseEntities();
           
            Partner p1 = new Partner
            {
                //PartnerId=int.Parse(txtid.Text),
                DomainName = txtdn.Text,
                // VehicleLimtEnable = int.Parse(txtvle.Text)
                // VehicleLimtEnable=Convert.ToInt32(txtvle.Text)
                VehicleLimtEnable = checkvehiclelimit,
                AgentDutyCode = txtAgentdutycode.Text,
                RequestorID_ID = txtRequestorID.Text,
                RequestorID_Type = Convert.ToInt32(RequestorID_Type.Text),
                CompanyName_Code = txtCompanyName_Code.Text,
                CodeContext = txtCodeContext.Text,
                Email = txtemail.Text,
                ConfimationMailSend = checkemail,
                CT_Allowed = Convert.ToInt32(CT_Allowed.SelectedValue)

            };
            if (!db.Partners.Where(l => l.DomainName == txtdn.Text).Any())
            {
                db.Partners.Add(p1);
                db.SaveChanges();
                MsgToShow.Show(MyMessageBox.MessageType.Success, "Succesfully Inserted.");
                MsgToShow.Visible = true;
            }
            Response.Redirect("managePartner.aspx?MsgType=2&MsgToShow='" + txtdn.Text + "' added successfully");


        }
        catch (Exception ex)
        {
            Response.Write(ex.StackTrace+"    "+ex.Message);
            MsgToShow.Show(MyMessageBox.MessageType.Error, "There was an error adding this information. Please try again.");
            MsgToShow.Visible = true;
        }                
    }
   

    
}