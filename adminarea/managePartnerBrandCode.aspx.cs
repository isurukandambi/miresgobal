﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ORM;
using System.Linq;
using CMSConfigs;
using CMSUtils;

public class PartnerBrandData
{
    public string mappingId { get; set; }
    public string partnerId { get; set; }
    public string partnerName { get; set; }
    public string brandId { get; set; }
    public string brandName { get; set; }

    public string IataCode { get; set; }
    public string PromotionCode { get; set; }
    public string TravellerNumber { get; set; }

    public string Tour_code { get; set; }

}
public partial class manageCountries : System.Web.UI.Page
{
    private DatabaseEntities db;
    protected void Page_Load(object sender, EventArgs e)
    {
        Master.Page.Title = CMSSettings.SiteTitle + " - Edit Section - Manage Partner Brand";

        if (Session["LoggedInName"] == null)
        {
            Response.Redirect("default.aspx?TimeOut=y&ReturnPage=" + Server.UrlEncode(HttpContext.Current.Request.Url.AbsoluteUri));
        }
        if (!Page.IsPostBack)
        {
            LoadPartnerBrands();
        }
        MsgToShow.Visible = false;
        if (Request.QueryString["MsgToShow"] != null)
        {
            int MsgType = Convert.ToInt16(Request.QueryString["MsgType"]);
            switch (MsgType)
            {
                case 1:
                    MsgToShow.Show(MyMessageBox.MessageType.Info, Request.QueryString["MsgToShow"].ToString());
                    break;
                case 2:
                    MsgToShow.Show(MyMessageBox.MessageType.Success, Request.QueryString["MsgToShow"].ToString());
                    break;
                case 3:
                    MsgToShow.Show(MyMessageBox.MessageType.Warning, Request.QueryString["MsgToShow"].ToString());
                    break;
                case 4:
                    MsgToShow.Show(MyMessageBox.MessageType.Error, Request.QueryString["MsgToShow"].ToString());
                    break;
                default:
                    goto case 1;
            }
            MsgToShow.Visible = true;
        }

    }
    protected void DeleteItems_Click(object sender, EventArgs e)
    {
        ConfirmBox.ShowConfirmBox("Delete Partner wit Brand", "Are you sure you want to remove selected Partner with Brand from system?", "Delete|Cancel");
        ConfirmBox.Visible = true;
    }
    protected void Confirm_Fired(object sender, EventArgs e)
    {
        bool confirm = (bool)sender;
        if (confirm == true)
        {
            DeleteDatabaseItem(InformationTable);
        }
    }

    protected void DeleteDatabaseItem(Repeater RepeaterID)
    {
        try
        {
            //initialize the objectContext
            db = new DatabaseEntities();

            foreach (RepeaterItem item in RepeaterID.Items)
            {

                CheckBox cb = (CheckBox)item.FindControl("rowcheck");
                if (cb != null && cb.Checked)
                {
                    Literal DBrow = (Literal)item.FindControl("rowID");
                    int row = Convert.ToInt16(DBrow.Text);
                    DeleteItem(row, db);
                }
            }
            db.SaveChanges();
            Response.Redirect("managePartnerBrandCode.aspx?MsgType=1&MsgToShow=Item(s) deleted successfully.");
        }
        catch (Exception ex)
        {
            //Response.Redirect("manageCountries.aspx?MsgType=4&MsgToShow=" + CommonTasks.FKconstrainsFail());
            //Response.Redirect("manageBrands.aspx?MsgType=4&MsgToShow=" + ex.Message+ ex.StackTrace);
            MsgToShow.Show(MyMessageBox.MessageType.Error, CMSUtilities.FKconstrainsFail());
            MsgToShow.Visible = true;
        }
    }
    protected void DeleteItem(int mappingId, DatabaseEntities dbCon)
    {
        try
        {
            //db = new DatabaseEntities();
            var partnerbrand = dbCon.PartnerBrandCodeMappings.SingleOrDefault(c => c.MappingId == mappingId);
            dbCon.PartnerBrandCodeMappings.Remove(partnerbrand);
            dbCon.SaveChanges();
            Response.Redirect("managePartnerBrandCode.aspx?MsgType=2&MsgToShow=Partner Brand Deleted.");
        }
        catch (Exception ex)
        {

            //Response.Redirect("manageCountries.aspx?MsgType=4&MsgToShow=" + CommonTasks.FKconstrainsFail());
            //Response.Write(ex);
            //MsgToShow.Show(MyMessageBox.MessageType.Error, "There was an error adding this information. Please try again." + ex.Message + "</br>" + ex.StackTrace);
            //MsgToShow.Visible = true;

        }
    }
    protected void LoadPartnerBrands()
    {
        db = new DatabaseEntities();
        List<PartnerBrandData> patnerList = new List<PartnerBrandData>();
        var partnerBrand = (from m in db.PartnerBrandCodeMappings
                            join b in db.Brands on m.BrandId equals b.BrandID
                            join p in db.Partners on m.PartnerId equals p.PartnerId
                            orderby m.MappingId
                            select new { m.MappingId, m.PartnerId, p.DomainName, m.BrandId, b.BrandName, m.IataCode, m.PromotionCode, m.TravellerNumber, m.Tour_code });
        foreach (var partnerbrandEntry in partnerBrand)
        {
            





            PartnerBrandData pb = new PartnerBrandData
            {
                mappingId = partnerbrandEntry.MappingId.ToString(),
                partnerId = partnerbrandEntry.PartnerId.ToString(),
                partnerName = partnerbrandEntry.DomainName.ToString(),
                brandId = partnerbrandEntry.BrandId.ToString(),
                brandName = partnerbrandEntry.BrandName.ToString(),
                IataCode = partnerbrandEntry.IataCode.ToString(),
                PromotionCode = partnerbrandEntry.PromotionCode.ToString(),
                TravellerNumber = partnerbrandEntry.TravellerNumber.ToString(),
                Tour_code = partnerbrandEntry.Tour_code.ToString(),

            };

            patnerList.Add(pb);
        }

        InformationTable.DataSource = patnerList.ToList();
        InformationTable.DataBind();
        if (partnerBrand.Count() > 0)
        {
            InformationTable.Visible = true;
            DeleteItems.Visible = true;
        }
        else
        {
            NoRecordsFound.Visible = true;
        }


    }
    protected void GridView_MouseOver(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowIndex >= 0)
        {
            e.Row.Attributes.Add("onmouseover", "this.style.background='#FFFFFF'");
            e.Row.Attributes.Add("onmouseout", "this.style.background='#F7F7F7'");
        }
    }

    //protected void InformationTable_RowDataBound(object sender, RepeaterItemEventArgs e)
    //{
    //    Response.Write("555555555555555");
    //    if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
    //    {
    //        int vehiPerStatus = Convert.ToInt32((e.Item.FindControl("VehiclePrefEnable")));

    //        Response.Write("sss _ " + vehiPerStatus);

    //        Literal VehPrefEnableLit = ((Literal)e.Item.FindControl("VehiclePrefEnableStatus"));


    //        if (vehiPerStatus == 1)
    //        {
    //            VehPrefEnableLit.Text = "Enable";
    //        }
    //        else
    //        {
    //            VehPrefEnableLit.Text = "Disable";
    //        }


    //    }
    //}
}
