﻿using System;
using System.Linq;
using System.Text;
using System.Web;
using ORM;
using System.Globalization;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;
using CMSConfigs;
using CMSUtils;

public partial class addVehicalDetails : System.Web.UI.Page
{
    private DatabaseEntities db;

    protected void Page_Load(object sender, EventArgs e)
    {
        Master.Page.Title = CMSSettings.SiteTitle + " - " + PageName.Text;
        if (Session["LoggedInName"] == null)
        {
            Response.Redirect("default.aspx?TimeOut=y&ReturnPage=" + Server.UrlEncode(HttpContext.Current.Request.Url.AbsoluteUri));
        }
        if (!Page.IsPostBack)
        {
            LoadGroups();
            LoadCountries();
            LoadBrands();
        }
    }
    protected int countryID()
    {
        int rowID;
        bool result = int.TryParse(Request.QueryString["Country"], out rowID);
        if (result)
        {
            return rowID;
        }
        return 0;
    }
    protected void LoadGroups()
    {
        db = new DatabaseEntities();
        CarGroupID.DataSource = db.CarGroups.OrderBy(c => c.CarCode).ToList();
        CarGroupID.DataValueField = "CarGroupID";
        CarGroupID.DataTextField = "CarCode";
        CarGroupID.DataBind();
    }
    protected void LoadCountries()
    {
        db = new DatabaseEntities();
        CountryID.DataSource = db.Countries.OrderBy(c => c.CountryName).ToList();
        CountryID.DataValueField = "CountryID";
        CountryID.DataTextField = "CountryName";
        CountryID.DataBind();
        CountryID.SelectedValue = countryID().ToString();
    }
    protected void LoadBrands()
    {
        // initialize the objectContext
        db = new DatabaseEntities();
        //var dbValue = db.Brands.SingleOrDefault(f => f.JobID == JobID);


        var jobs = (from j in db.Brands
                    orderby j.BrandName
                    select new { j.BrandName, j.BrandID });
        CarDetailsContainer.DataSource = jobs.ToList();
        CarDetailsContainer.DataBind();

    }
    protected void AddButton_Click(object sender, EventArgs e)
    {
        AddDetails(CarDetailsContainer);
    }
    protected void AddDetails(Repeater RepeaterID)
    {
        
        string message = "";
        try
        {
            // initialize the objectContext
            db = new DatabaseEntities();
            int selectedCarGroupID = Convert.ToInt32(CarGroupID.SelectedValue);
            int selectedCountryID = Convert.ToInt32(CountryID.SelectedValue);

            foreach (RepeaterItem item in RepeaterID.Items)
            {
                Literal brandNameField = (Literal)item.FindControl("BrandName");
                FileUpload fileUploadField = (FileUpload)item.FindControl("filPhoto");
                TextBox Description = (TextBox)item.FindControl("Description");
                HiddenField BrandIDField = (HiddenField)item.FindControl("BrandID");
                int brandID = Convert.ToInt32(BrandIDField.Value);
                var anyCargroupsForCountry = (from vdb in db.VehicleDetailsByBrands
                                              join vr in db.VehicleResources on vdb.VDBID equals vr.VDBIDFK
                                              join b in db.Brands on vdb.BrandIDFK equals b.BrandID
                                              where vr.CountryIDFK == selectedCountryID && vdb.BrandIDFK == brandID
                                              && vdb.CarGroupIDFK == selectedCarGroupID
                                              select new
                                              {
                                                  b.BrandName
                                              });
                if (anyCargroupsForCountry.Count() > 0)
                {
                    message += anyCargroupsForCountry.FirstOrDefault().BrandName + " Already Exist for selected Country and car group. |";
                }
                else
                {
                   string filename = uploadImage(fileUploadField);
                    if (!string.IsNullOrEmpty(filename) || !string.IsNullOrEmpty(Description.Text))
                    {
                        VehicleDetailsByBrand VehicleDetails = new VehicleDetailsByBrand
                        {

                            CarImagePath = filename,
                            CarDescription = Description.Text,
                            CarGroupIDFK = selectedCarGroupID,
                            BrandIDFK = brandID
                        };
                        db.VehicleDetailsByBrands.Add(VehicleDetails);
                        db.SaveChanges();

                        VehicleResource VehicleResources = new VehicleResource
                        {
                            VDBIDFK = VehicleDetails.VDBID,
                            CountryIDFK = selectedCountryID,
                        };
                        db.VehicleResources.Add(VehicleResources);
                        db.SaveChanges();
                    }
                    else
                    {
                        message += brandNameField.Text + " Not added. Car Description or Image fields were empty. |";
                        
                    }
                }
            }
            //success
            if (string.IsNullOrEmpty(message))
            {
                Response.Redirect("manageVehicalDetails.aspx?MsgType=2&MsgToShow=Vehical Details for car groups added successfully&Row=" + selectedCountryID);
            }
            else
            {
                Response.Redirect("manageVehicalDetails.aspx?MsgType=1&MsgToShow="+message+"&Row=" + selectedCountryID);
            }
            
        }
        catch
        {
            Response.Redirect("manageVehicalDetails.aspx?MsgType=1&MsgToShow=" + message + "&Row=" + CountryID.SelectedValue);
        }
    }

    protected string uploadImage(FileUpload filPhoto)
    {
        string MsgToShowText = "";
        string filename = "";
        string appPath = Request.PhysicalApplicationPath;
        string FilePathOrig = "";
        string UpPath = appPath + "\\Pics\\CarImages\\";
        if (!Directory.Exists(UpPath))
        {
            Directory.CreateDirectory(UpPath);
        }

        bool ErrorsWithFile = false;
        //start logic for Image 1
        if ((filPhoto.PostedFile != null) && (filPhoto.PostedFile.ContentLength > 0))
        {
            filename = cleanFilePath(filPhoto);

            //check filename does not contain any illegal character
            if (!CMSUtilities.FilenameChecker(filename))
            {
                MsgToShowText += "The uploaded file is not a valid file type.<br />";
                ErrorsWithFile = true;
            }
            //check file is a valid image file and correct extension
            System.Text.RegularExpressions.Regex imageFilenameRegex = new System.Text.RegularExpressions.Regex(@"(.*?)\.(jpg|jpeg|png|gif)$");
            if ((!CMSUtilities.ImageChecker(filename, filPhoto.PostedFile.InputStream)) || (!imageFilenameRegex.IsMatch(filename)))
            {
                MsgToShowText += "The file uploaded is not a valid image file.<br />";
                ErrorsWithFile = true;
            }
            //check file is not too big
            if (filPhoto.PostedFile.ContentLength > 3145728) //3MB
            {
                MsgToShowText += "Image too big. Try again.<br />";
                ErrorsWithFile = true;
            }
            //check filename does not already exist
            if (File.Exists(UpPath + filename))
            {
                MsgToShowText += "An image with the same filename already exists. Please rename your file before uploading.<br />";
                ErrorsWithFile = true;
            }

            //if everything above is ok save the image
            if (ErrorsWithFile == false)
            {
                FilePathOrig = UpPath + filename;
                CMSUtilities.Resize(FilePathOrig, 220, 128, false, filPhoto.PostedFile.InputStream);
            }
        }
        return filename;
    }

    protected string cleanFilePath(FileUpload fileInfo)
    {
        string fileName = Path.GetFileNameWithoutExtension(fileInfo.PostedFile.FileName);
        string fileExtension = Path.GetExtension(fileInfo.PostedFile.FileName);
        fileName = Server.HtmlEncode(fileName);
        //fileName = fileName +DateTime.Now;
        fileName = fileName.Replace(".", "");
        fileName = fileName.Replace(" ", "");
        fileName = fileName.Replace("/", "");
        fileName = fileName.Replace(":", "");
        fileName = fileName.Replace("&", "");
        fileName = fileName.ToUpper();
        fileName = fileName + fileExtension;
        return fileName;
    }
}