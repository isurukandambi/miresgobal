﻿<%@ Page Language="C#" MasterPageFile="adminarea.master" AutoEventWireup="true" CodeFile="requestTimeout.aspx.cs"
    Inherits="salesReports" %>

<%@ Register Src="~/adminarea/controls/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="js/dataTables/jquery.dataTables.min.js" type="text/javascript"></script>
    <script src="js/dataTables/extras/TableTools/media/js/TableTools.min.js"
        type="text/javascript"></script>
   <%-- <script type="text/javascript">        google.load("jqueryui", "<%=CMSSettings.GetJQueryUIVer%>");</script>--%>
    <link rel="stylesheet" type="text/css" href="css/smoothness/jquery-ui.css" media="screen" />
    <%--<link rel="stylesheet" type="text/css" href="js/dataTables/media/css/jquery.dataTables.css" media="screen" />--%>
    <link rel="stylesheet" type="text/css" href="js/dataTables/extras/TableTools/media/css/TableTools.css"
        media="screen" />
    <script src="js/datePicker/datepicker.js" type="text/javascript"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.9/jquery-ui.min.js" type="text/javascript"></script>
    <script type="text/javascript" language="javascript">



        $(document).ready(function () {

            $('.startDate, .endDate').datepicker({
                numberOfMonths: 1,
                showButtonPanel: true,
                dateFormat: 'yy-mm-dd',
                showOn: 'both',
                buttonImage: 'images/calendar-icon.gif',
                buttonImageOnly: true,
                buttonText: 'Show Date Picker'
            });

            //grid 1 settings
            var oTable = $('#dataGrid').dataTable({
                "sDom": 'T<"clear">lfrtip',
                "oTableTools": {
                    "sSwfPath": "js/dataTables/extras/TableTools/media/swf/copy_csv_xls.swf",
                    "aButtons": [
				        {
				            "sExtends": "xls",
				            "sButtonText": "Export to Excel"
				        }
			        ]
                },
                "bAutoWidth": false,
                "sPaginationType": "full_numbers",
                "iDisplayLength": 50,
                "bStateSave": true,
                "iCookieDuration": 300,
                //customizing columns
                "aaSorting": [[1, "asc"]],
                "aoColumns": [
        			{ "bSearchable": false, "bVisible": false }, //setting first column to be hidden from searching and the user - required for knowing what database row to remove
                    {"sType": "html" }, //setting second column to search word of html link
        			null,
                    null,
                    null,
                    null,
        			{ "bSortable": true} //setting last column not to sort onClick
        		]

            });

            $('.endDate').change(function () { oTable.fnDraw(); });
            $('.startDate').change(function () { oTable.fnDraw(); });
        });

        $.fn.dataTableExt.afnFiltering.push(function (oSettings, aData, iDataIndex) {
            var endDate = $('.endDate').val();
            var startDate = $('.startDate').val();
            var jobDate = aData[5];
            var splitDate = jobDate.split(' ');
            splitDate = splitDate[0].split('/');
            var FormatedDate=splitDate[2]+"-"+splitDate[1]+"-"+splitDate[0];

           // console.log("wwwwww   ", FormatedDate);
           // console.log("qqqq  ", startDate);
            // console.log(endDate, startDate, aData[0]);

           // console.log("endDate", endDate);
           // console.log("startDate", startDate);
           // console.log("aData", aData[5]);

            if (endDate == "" && startDate == "") {
                return true;
            }
            else if (endDate == "" && startDate != "") {
                var sDate = new Date(startDate).setHours(0,0,0,0);
                var jDate = new Date(FormatedDate).setHours(0, 0, 0, 0);
               // console.log("1111   ",jDate);
                //console.log("2222    ",sDate);
                if (jDate >= sDate) {
                    return true;
                }
            }
            else if (endDate != "" && startDate == "") {
                var eDate = new Date(endDate).setHours(0, 0, 0, 0);
                var jDate = new Date(FormatedDate).setHours(0, 0, 0, 0);
                //console.log("s  ","t  ",jDate,"v  ",eDate);
                if (jDate <= eDate) {
                    return true;
                }
            }
            else if (endDate != "" && startDate != "") {
                var sDate = new Date(startDate).setHours(0, 0, 0, 0);
                var eDate = new Date(endDate).setHours(0, 0, 0, 0);
                var jDate = new Date(FormatedDate).setHours(0, 0, 0, 0);
                //console.log("ccc  ", jDate, eDate, sDate);
                if (jDate <= eDate && jDate >= sDate) {
                    return true;
                }
            }
            else {
                return false;
            }
        });
    </script>
    <style type="text/css">
        .dataTables_length
        {
            width: auto;
            float: left;
            margin-left: 10px;
            vertical-align: middle;
        }
        .dataTables_filter
        {
            width: auto;
            float: right;
            text-align: right;
            vertical-align: middle;
            margin-right: 10px;
        }
        .search-panel
        {
            text-align: center;
        }
        #dataGrid_wrapper
        {
            background-color: rgb(219, 219, 219);
            padding: 12px 0px 25px;
            border-radius: 6px;
        }
        table
        {
            width: 100%;
        }
        .dataTables_paginate
        {
            margin-top: 3px;
        }
        div.DTTT_container
        {
            margin-right: 7px;
        }
        .ui-datepicker-trigger
        {
            position: relative;
            top: 9px;
        }
        #dataGrid_paginate > *
        {
            margin: 4px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="content">
        <h2>
            <asp:Literal ID="PageName" runat="server" Text="Rquest Timeout Report" /></h2>
        <uc1:MyMessageBox ID="MsgToShow" runat="server" ShowCloseButton="true" Visible="false" />
        <div id="actionArea">
            <div class="search-panel">

                Request Log Start Date :
                <asp:TextBox ID="DateStarttxt" CssClass="text shortField startDate" Width="200" runat="server" />
                </br> 
                Request Log End Date :
                <asp:TextBox ID="DateEndtxt" CssClass="text shortField endDate" Width="200" runat="server" />
            </div>
            <asp:Literal ID="NoRecordsFound" runat="server" Text="No Reports found." Visible="false" />
            <asp:Repeater ID="InformationTable" runat="server" Visible="false">
                <HeaderTemplate>
                    <table cellpadding="0" cellspacing="0" border="0" id="dataGrid">
                        <thead>
                            <tr>
                                <th>
                                    ID
                                </th>
                                <th class="leftCell">
                                    Request Time
                                </th>
                                <th>
                                    Hertz Request Time
                                </th>
                                <th>
                                    Hertz Response Time
                                </th>
                                <th>
                                    Request Domain
                                </th>
                                <th>
                                    Brand
                                </th>
                                <th>
                                    Log Date
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td>
                            <asp:Literal ID="rowID" runat="server" Text='<%# Eval("RequestTimeoutID")  %>' />
                        </td>
                        <td>
                            <%# Eval("RequestCameTime")%>
                        </td>
                        <td class="cnrCell">
                            <%# Eval("XMLProviderSendTime")%></br>
                        </td>
                        <td class="cnrCell">
                            <%# Eval("XMLProviderReceviedTime")%>
                        </td>
                        <td class="cnrCell">
                           <%# Eval("RequestDomain")%>
                        </td>
                        <td class="cnrCell">
                          <%# Eval("Brand")%>
                        </td>
                        <td class="cnrCell">
                          <%# Eval("LogDate")%>
                        </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </tbody> </table>
                </FooterTemplate>
            </asp:Repeater>
        </div>
    </div>
    <asp:Literal ID="BarcodePrinterLink" runat="server" Text="" />
</asp:Content>
