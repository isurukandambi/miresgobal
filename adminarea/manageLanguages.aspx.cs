﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ORM;
using System.Linq;
using CMSConfigs;

public partial class manageLanguages : System.Web.UI.Page
{
    private DatabaseEntities db;
    protected void Page_Load(object sender, EventArgs e)
    {
        Master.Page.Title = CMSSettings.SiteTitle + " - Edit Section - Manage Languages";

        if (Session["LoggedInName"] == null)
        {
            Response.Redirect("default.aspx?TimeOut=y&ReturnPage=" + Server.UrlEncode(HttpContext.Current.Request.Url.AbsoluteUri));
        }
        if (!Page.IsPostBack)
        {
            LoadLanguages();
            DisplayActionMessage();
        }
        if (Page.IsPostBack)
        {
            //Response.Redirect("manageLocations.aspx?row=" + Server.HtmlEncode(ddlCountries.SelectedValue));
        }
    }
    protected void DeleteItems_Click(object sender, EventArgs e)
    {
        MsgToShow.Visible = false;
        ConfirmBox.ShowConfirmBox("Delete Languages", "Are you sure you want to remove selected Languages from system?", "Delete|Cancel");
        ConfirmBox.Visible = true;
        
    }
    protected void Confirm_Fired(object sender, EventArgs e)
    {
        bool confirm = (bool)sender;
        if (confirm == true)
        {
            DeleteDatabaseItem(InformationTable);
        }
    }

    protected void DeleteDatabaseItem(Repeater RepeaterID)
    {
        string message = "";
        //initialize the objectContext
        db = new DatabaseEntities();
        foreach (RepeaterItem item in RepeaterID.Items)
        {
            CheckBox cb = (CheckBox)item.FindControl("rowcheck");
            if (cb != null && cb.Checked)
            {
                Literal DBrow = (Literal)item.FindControl("rowID");
                int row = Convert.ToInt16(DBrow.Text);
                message += DeleteItem(row, db);
            }
        }
        if (string.IsNullOrEmpty(message))
        {
            Response.Redirect("manageLanguages.aspx?MsgType=1&MsgToShow=Item(s) deleted successfully.");
        }
        else
        {
            MsgToShow.Show(MyMessageBox.MessageType.Error, message);
            MsgToShow.Visible = true;
        }
    }
    protected string DeleteItem(int deleteItemID, DatabaseEntities dbCon)
    {
        string message = "";
        var listWithChildren = dbCon.Languages.Where(la => la.LanguageCode.Any());
        var item = dbCon.Languages.SingleOrDefault(la => la.LanguageID == deleteItemID);
        if (listWithChildren.Count() > 0)
        {
            message = "Error occurred while deleting " + item.LanguageName + ". Language already used. </br>";
        }
        else
        {

            dbCon.Languages.Remove(item);
            dbCon.SaveChanges();
        }

        return message;
    }
    protected void LoadLanguages()
    {
        db = new DatabaseEntities();
        var Language = (from la in db.Languages
                        orderby la.LanguageName
                        select new { la.LanguageID, la.LanguageName, la.LanguageCode });
        InformationTable.DataSource = Language.ToList();
        InformationTable.DataBind();
        if (Language.Count() > 0)
        {
            InformationTable.Visible = true;
            DeleteItems.Visible = true;
        }
        else
        {
            NoRecordsFound.Visible = true;
        }
    }
    protected void GridView_MouseOver(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowIndex >= 0)
        {
            e.Row.Attributes.Add("onmouseover", "this.style.background='#FFFFFF'");
            e.Row.Attributes.Add("onmouseout", "this.style.background='#F7F7F7'");
        }
    }

    protected void DisplayActionMessage()
    {
        if ((Request.QueryString["MsgType"] != null) && (Request.QueryString["MsgToShow"] != null))
        {
            string msgToShow = Request.QueryString["MsgToShow"].ToString();
            int rowID;
            bool result = int.TryParse(Request.QueryString["MsgType"], out rowID);
            if ((result) && !String.IsNullOrEmpty(msgToShow))
            {
                switch (Convert.ToInt16(Request.QueryString["MsgType"]))
                {
                    case 1:
                        MsgToShow.Show(MyMessageBox.MessageType.Info, msgToShow);
                        break;
                    case 2:
                        MsgToShow.Show(MyMessageBox.MessageType.Success, msgToShow);
                        break;
                    case 3:
                        MsgToShow.Show(MyMessageBox.MessageType.Warning, msgToShow);
                        break;
                    case 4:
                        MsgToShow.Show(MyMessageBox.MessageType.Error, msgToShow);
                        break;
                    default:
                        goto case 1;
                }
                MsgToShow.Visible = true;
            }
        }
    }
}
