﻿//Chart controls: Daily Chart, Monthly Chart and Pie Chart

function dailyChart(renderContainer, startDate, chartTitle, totalBookings, hertzBookings, thriftyBookings, fireflyAEBookings, fireflyHBookings, dollarBookings, HertzSales, ThriftySales, FireflyAESales, FireflyHSales, DollarSales) {
    //chart data
    HertzSales = HertzSales.split(",");
    HertzSales = $.map(HertzSales, function (item) {
        return parseFloat(item);
    });

    FireflyHSales = FireflyHSales.split(",");
    FireflyHSales = $.map(FireflyHSales, function (item) {
        return parseFloat(item);
    });

    ThriftySales = ThriftySales.split(",");
    ThriftySales = $.map(ThriftySales, function (item) {
        return parseFloat(item);
    });

    FireflyAESales = FireflyAESales.split(",");
    FireflyAESales = $.map(FireflyAESales, function (item) {
        return parseFloat(item);
    });

    DollarSales = DollarSales.split(",");
    DollarSales = $.map(DollarSales, function (item) {
        return parseFloat(item);
    });

    //chart control
    var chart = new Highcharts.Chart({
        credits: {
            enabled: false
        },
        chart: {
            renderTo: renderContainer,
            defaultSeriesType: 'area',
            marginRight: 140
        },
        title: {
            text: chartTitle
        },
        subtitle: {
            text: 'Total Bookings: ' + totalBookings + ' (Hertz: ' + hertzBookings + ' / Thrifty: ' + thriftyBookings + /*' /  FireFly Auto Europe: ' + fireflyAEBookings +*/ ' / FireFly_Hertz: ' + fireflyHBookings + ' / Dollar: ' + dollarBookings +')'
        },
        xAxis: {
            type: 'datetime',
            labels: { rotation: 90, align: 'left' }
        },
        yAxis: {
            title: {
                text: 'Bookings'
            }
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'top',
            x: -10,
            y: 100
        },
        tooltip: {
            formatter: function () {
                return this.y + ' sales on ' + Highcharts.dateFormat('%a, %e %b %Y', this.x);
            }
        },
        plotOptions: {
            area: {
                fillOpacity: 0.5
            }
        },
        series: [{
            pointInterval: 24 * 3600 * 1000,
            pointStart: startDate.getTime(),
            color: '#FFDE00',
            name: 'Hertz',
            data: HertzSales
        }, {
            pointInterval: 24 * 3600 * 1000,
            pointStart: startDate.getTime(),
            color: '#AA4643',
            name: 'Thrifty',
            data: ThriftySales
        },/* {
            pointInterval: 24 * 3600 * 1000,
            pointStart: startDate.getTime(),
            color: '#4572A7',
            name: 'FireFly Auto Europe',
            data: FireflyAESales
        }, */{
            pointInterval: 24 * 3600 * 1000,
            pointStart: startDate.getTime(),
            color: '#3A9700',
            name: 'FireFly Hertz',
            data: FireflyHSales
        }, {
            pointInterval: 24 * 3600 * 1000,
            pointStart: startDate.getTime(),
            color: '#4572A7',
            name: 'Dollar',
            data: DollarSales
        }]

    });
}
function monthlyChart(renderContainer, chartTitle, totalBookings, hertzBookings, thriftyBookings, fireflyAEBookings, fireflyHBookings, dollarBookings, monthsSelected, HertzSales, ThriftySales, FireflyAESales, FireflyHSales, DollarSales) {
    //chart data
    HertzSales = HertzSales.split(",");
    HertzSales = $.map(HertzSales, function (item) {
        return parseFloat(item);
    });

    ThriftySales = ThriftySales.split(",");
    ThriftySales = $.map(ThriftySales, function (item) {
        return parseFloat(item);
    });

    FireflyAESales = FireflyAESales.split(",");
    FireflyAESales = $.map(FireflyAESales, function (item) {
        return parseFloat(item);
    });

    FireflyHSales = FireflyHSales.split(",");
    FireflyHSales = $.map(FireflyHSales, function (item) {
        return parseFloat(item);
    });

    DollarSales = DollarSales.split(",");
    DollarSales = $.map(DollarSales, function (item) {
        return parseFloat(item);
    });

    monthsSelected = monthsSelected.split(",");
    monthsSelected = $.map(monthsSelected, function (item) {
        return (item);
    });

    //chart control
    var chart = new Highcharts.Chart({
        credits: {
            enabled: false
        },
        chart: {
            renderTo: renderContainer,
            defaultSeriesType: 'area',
            marginRight: 140
        },
        title: {
            text: chartTitle
        },
        subtitle: {
            text: 'Total Bookings: ' + totalBookings + ' (Hertz: ' + hertzBookings + ' / Thrifty: ' + thriftyBookings + /*' /  FireFly Auto Europe: ' + fireflyAEBookings +*/ ' / FireFly Hertz: ' + fireflyHBookings + ' / Dollar: ' + dollarBookings + ')'
        },
        xAxis: {
            categories: monthsSelected
        },
        yAxis: {
            title: {
                text: 'Bookings'
            }
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'top',
            x: -10,
            y: 100
        },
        tooltip: {
            formatter: function () {
                return this.y + ' sales in ' + this.x;
            }
        },
        plotOptions: {
            area: {
                fillOpacity: 0.5
            }
        },
        series: [{
            color: '#FFDE00',
            name: 'Hertz',
            data: HertzSales
        }, {
            color: '#AA4643',
            name: 'Thrifty',
            data: ThriftySales
        },/* {
            color: '#4572A7',
            name: 'FireFly Auto Europe',
            data: FireflyAESales
        }, */{
            color: '#3A9700',
            name: 'FireFly Hertz',
            data: FireflyHSales
        }, {
            color: '#4572A7',
            name: 'Dollar',
            data: DollarSales
        }]

    });
}

function pieChart(renderContainer, chartTitle, pieChartValues) {
    var chart = new Highcharts.Chart({
        credits: {
            enabled: false
        },
        chart: {
            renderTo: renderContainer,
            margin: [50, 200, 60, 170]
        },
        title: {
            text: chartTitle
        },
        plotArea: {
            shadow: null,
            borderWidth: null,
            backgroundColor: null
        },
        tooltip: {
            formatter: function () {
                return '<b>' + this.point.name + '</b>: ' + this.y;
            }
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer'
            }
        },
        legend: {
            layout: 'vertical',
            style: {
                left: 'auto',
                bottom: 'auto',
                right: '50px',
                top: '100px'
            }
        },
        series: [{
            type: 'pie',
            name: 'hertzData',
            data: eval(pieChartValues)
        }]
    });
}


function requestTimeoutdailyChart(renderContainer, startDate, chartTitle, totalBookings, hertzBookings, thriftyBookings, fireflyBookings, dollarBookings, HertzSales, ThriftySales, FireflySales, DollarSales) {
    //chart data
    HertzSales = HertzSales.split(",");
    HertzSales = $.map(HertzSales, function (item) {
        return parseFloat(item);
    });

    FireflySales = FireflySales.split(",");
    FireflySales = $.map(FireflySales, function (item) {
        return parseFloat(item);
    });

    ThriftySales = ThriftySales.split(",");
    ThriftySales = $.map(ThriftySales, function (item) {
        return parseFloat(item);
    });

    DollarSales = DollarSales.split(",");
    DollarSales = $.map(DollarSales, function (item) {
        return parseFloat(item);
    });


    //chart control
    var chart = new Highcharts.Chart({
        credits: {
            enabled: false
        },
        chart: {
            renderTo: renderContainer,
            defaultSeriesType: 'area',
            marginRight: 140
        },
        title: {
            text: chartTitle
        },
        subtitle: {
            text: 'Total Bookings: ' + totalBookings + ' (Hertz: ' + hertzBookings + ' / Thrifty: ' + thriftyBookings + ' / FireFly : ' + fireflyBookings + ' / Dollar: ' + dollarBookings + ')'
        },
        xAxis: {
            type: 'datetime',
            labels: { rotation: 90, align: 'left' }
        },
        yAxis: {
            title: {
                text: 'Request Timeout'
            }
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'top',
            x: -10,
            y: 100
        },
        tooltip: {
            formatter: function () {
                return this.y + ' sales on ' + Highcharts.dateFormat('%a, %e %b %Y', this.x);
            }
        },
        plotOptions: {
            area: {
                fillOpacity: 0.5
            }
        },
        series: [{
            pointInterval: 24 * 3600 * 1000,
            pointStart: startDate.getTime(),
            color: '#FFDE00',
            name: 'Hertz',
            data: HertzSales
        }, {
            pointInterval: 24 * 3600 * 1000,
            pointStart: startDate.getTime(),
            color: '#AA4643',
            name: 'Thrifty',
            data: ThriftySales
        },/* {
            pointInterval: 24 * 3600 * 1000,
            pointStart: startDate.getTime(),
            color: '#4572A7',
            name: 'FireFly Auto Europe',
            data: FireflyAESales
        }, */{
            pointInterval: 24 * 3600 * 1000,
            pointStart: startDate.getTime(),
            color: '#3A9700',
            name: 'FireFly',
            data: FireflySales
        }, {
            pointInterval: 24 * 3600 * 1000,
            pointStart: startDate.getTime(),
            color: '#4572A7',
            name: 'Dollar',
            data: DollarSales
        }]

    });
}



function requestTimeoutMonthlyChart(renderContainer, chartTitle, totalBookings, hertzBookings, thriftyBookings, fireflyAEBookings, dollarBookings, monthsSelected, HertzSales, ThriftySales, FireflyAESales, DollarSales) {
    //chart data
    HertzSales = HertzSales.split(",");
    HertzSales = $.map(HertzSales, function (item) {
        return parseFloat(item);
    });

    ThriftySales = ThriftySales.split(",");
    ThriftySales = $.map(ThriftySales, function (item) {
        return parseFloat(item);
    });

    FireflyAESales = FireflyAESales.split(",");
    FireflyAESales = $.map(FireflyAESales, function (item) {
        return parseFloat(item);
    });

    DollarSales = DollarSales.split(",");
    DollarSales = $.map(DollarSales, function (item) {
        return parseFloat(item);
    });

    //FireflyHSales = FireflyHSales.split(",");
    //FireflyHSales = $.map(FireflyHSales, function (item) {
    //    return parseFloat(item);
    //});

    monthsSelected = monthsSelected.split(",");
    monthsSelected = $.map(monthsSelected, function (item) {
        return (item);
    });

    //chart control
    var chart = new Highcharts.Chart({
        credits: {
            enabled: false
        },
        chart: {
            renderTo: renderContainer,
            defaultSeriesType: 'area',
            marginRight: 140
        },
        title: {
            text: chartTitle
        },
        subtitle: {
            text: 'Total Request Timeout: ' + totalBookings + ' (Hertz: ' + hertzBookings + ' / Thrifty: ' + thriftyBookings + /*' /  FireFly Auto Europe: ' + fireflyAEBookings +*/ ' / FireFly : ' + fireflyAEBookings + ' / Dollar: ' + dollarBookings + ')'
        },
        xAxis: {
            categories: monthsSelected
        },
        yAxis: {
            title: {
                text: 'Request Timeout'
            }
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'top',
            x: -10,
            y: 100
        },
        tooltip: {
            formatter: function () {
                return this.y + ' sales in ' + this.x;
            }
        },
        plotOptions: {
            area: {
                fillOpacity: 0.5
            }
        },
        series: [{
            color: '#FFDE00',
            name: 'Hertz',
            data: HertzSales
        }, {
            color: '#AA4643',
            name: 'Thrifty',
            data: ThriftySales
        },/* {
            color: '#4572A7',
            name: 'FireFly Auto Europe',
            data: FireflyAESales
        }, */{
            color: '#3A9700',
            name: 'FireFly',
            data: FireflyAESales
        }, {
            color: '#4572A7',
            name: 'Dollar',
            data: DollarSales
        }]

    });
}