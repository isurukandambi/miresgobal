﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ForgotPasswordSent.aspx.cs" Inherits="ForgotPasswordSent_edit" %>

<%@ Register Src="~/adminarea/controls/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <script src="https://www.google.com/jsapi" type="text/javascript"></script> 
    <script type="text/javascript">google.load("jquery", "1.4.1")</script>
    <link href="css/validationEngine.jquery.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery.validationEngine.js" type="text/javascript"></script>
    <script src="js/jquery.validationEngine-en.js" type="text/javascript"></script>
    <script  type="text/javascript">
        $(document).ready(function () {
            $("#form1").validationEngine();
        });
    </script>
    <title><%=CMSSettings.SiteTitle%></title>
    <link rel="stylesheet" type="text/css" href="css/edit_styles.css" media="screen"/>
    <!--[if lte IE 6]>
    <link href="css/ie6styles.css" rel="stylesheet" type="text/css" media="all" />
    <![endif]-->
</head>
<body>
    <form id="form1" runat="server">
    <div id="wrapper">
        <div id="edit_header">
    	    <%--<a href="http://www.justperfectit.com/" target="_blank"><img src="images/header-arrow.gif" alt="Control Panel licenced by Just Perfect IT" border="0" class="headerArrow" /></a>--%> 
            <h1>Admin Area for <%=CMSSettings.SiteTitle%></h1>
        </div>
        <div id="contentLogin">
            <h3><asp:Literal ID="PageName" runat="server" Text="New password sent to your email account." /></h3>
            <fieldset>
                <div>
                    We have sent your account password to your registered email account. Please follow the instructions in the email to log back into the site.
                </div>
            </fieldset>
            <a href="Default.aspx" class="btn">Login</a>
        </div>
        <div class="push"></div>    <!-- for sticky footer -->
    </div> <!-- end wrapper -->
    <div id="footer">
        <span>For problems or support, please contact us: <a href="mailto:clientsupport@justperfectit.com">clientsupport@justperfectit.com</a>.</span>
        Just Perfect IT Admin Area Tools Licenced by <a href="http://www.justperfectit.com" target="_blank">Just Perfect IT Limited</a> | &copy;<script type="text/javascript" language="javascript">var thedate = new Date(); var year = thedate.getFullYear(); document.write(year);</script> Just Perfect IT Ltd.
    </div>
    </form>
</body>
</html>