﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="forgotpassword.aspx.cs" Inherits="ForgotPassword_edit" %>
<%@ Register Src="~/adminarea/controls/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <script src="https://www.google.com/jsapi" type="text/javascript"></script> 
    <script type="text/javascript">google.load("jquery", "1.4.1")</script>
    <link href="css/validationEngine.jquery.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery.validationEngine.js" type="text/javascript"></script>
    <script src="js/jquery.validationEngine-en.js" type="text/javascript"></script>
    <script  type="text/javascript">
        $(document).ready(function () {
            $("#form1").validationEngine();
        });
    </script>
    <title><%=CMSSettings.SiteTitle%></title>
    <link rel="stylesheet" type="text/css" href="css/edit_styles.css" media="screen"/>
    <!--[if lte IE 6]>
    <link href="css/ie6styles.css" rel="stylesheet" type="text/css" media="all" />
    <![endif]-->
</head>
<body>
    <form id="form1" runat="server">
    <div id="wrapper">
        <div id="edit_header">
            <h1>Admin Area for <%=CMSSettings.SiteTitle%></h1>
        </div>
        <div id="contentLogin">
            <h2><asp:Literal ID="PageName" runat="server" Text="Reset my password" /></h2>
            <uc1:MyMessageBox ID="MsgToShow" runat="server" ShowCloseButton="false" Visible="false" />
            <fieldset>
                <div>
                    <label for="Email">Please enter your email address</label>
                    <asp:TextBox ID="Usernametxt" CssClass="text validate[required]" runat="server" MaxLength="100"></asp:TextBox>
                </div>
            </fieldset>
            <asp:Button ID="ResetButton" runat="server" CssClass="btn" Text="Reset Password"  onclick="ResetButton_Click" />
            <div  class="littleLink"><a href="default.aspx">Back to login screen</a></div>
        </div>

        <div class="push"></div>    <!-- for sticky footer -->
    </div> <!-- end wrapper -->
    <div id="footer">
        <span>For problems or support, please contact us: <a href="mailto:clientsupport@justperfectit.com">clientsupport@justperfectit.com</a>.</span>
        JPIT Admin Area Tools Licenced by <a href="http://www.justperfectit.com" target="_blank">Just Perfect IT Ltd</a> | &copy;<script type="text/javascript" language="javascript">var thedate = new Date(); var year = thedate.getFullYear(); document.write(year);</script> Just Perfect IT Ltd.
    </div>
    </form>
</body>
</html>
