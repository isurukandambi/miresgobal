﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ORM;
using System.Linq;
using CMSConfigs;
using CMSUtils;


public partial class adminarea_manageThriftyCountries : System.Web.UI.Page
{
    private DatabaseEntities db;
    string countMessage;
    protected void Page_Load(object sender, EventArgs e)
    {
        Master.Page.Title = CMSSettings.SiteTitle + " - Edit Section - Manage Countries";

        if (Session["LoggedInName"] == null)
        {
            Response.Redirect("default.aspx?TimeOut=y&ReturnPage=" + Server.UrlEncode(HttpContext.Current.Request.Url.AbsoluteUri));
        }
        if (!Page.IsPostBack)
        {
            LoadCountries();
        }
        MsgToShow.Visible = false;
        if (Request.QueryString["MsgToShow"] != null)
        {
            int MsgType = Convert.ToInt16(Request.QueryString["MsgType"]);
            switch (MsgType)
            {
                case 1:
                    MsgToShow.Show(MyMessageBox.MessageType.Info, Request.QueryString["MsgToShow"].ToString());
                    break;
                case 2:
                    MsgToShow.Show(MyMessageBox.MessageType.Success, Request.QueryString["MsgToShow"].ToString());
                    break;
                case 3:
                    MsgToShow.Show(MyMessageBox.MessageType.Warning, Request.QueryString["MsgToShow"].ToString());
                    break;
                case 4:
                    MsgToShow.Show(MyMessageBox.MessageType.Error, Request.QueryString["MsgToShow"].ToString());
                    break;
                default:
                    goto case 1;
            }
            MsgToShow.Visible = true;
        }
    }

    protected void DeleteItems_Click(object sender, EventArgs e)
    {
        ConfirmBox.ShowConfirmBox("Delete Countries", "Are you sure you want to remove selected Countries from system?", "Delete|Cancel");
        ConfirmBox.Visible = true;
    }
    protected void Confirm_Fired(object sender, EventArgs e)
    {
        bool confirm = (bool)sender;
        if (confirm == true)
        {
            DeleteDatabaseItem(InformationTable);
        }
    }

    protected void DeleteDatabaseItem(Repeater RepeaterID)
    {
        string message = "";
        //initialize the objectContext
        db = new DatabaseEntities();
        foreach (RepeaterItem item in RepeaterID.Items)
        {
            CheckBox cb = (CheckBox)item.FindControl("rowcheck");
            if (cb != null && cb.Checked)
            {
                Literal DBrow = (Literal)item.FindControl("rowID");
                int row = Convert.ToInt16(DBrow.Text);
                message += DeleteItem(row, db);
            }
        }
        if (string.IsNullOrEmpty(message))
        {
            Response.Redirect("manageThriftyCountries.aspx?MsgType=2&MsgToShow=Item(s) deleted successfully.");
        }
        else
        {
            Response.Redirect("manageThriftyCountries.aspx?MsgType=4&MsgToShow=" + message);
        }
    }

    protected string DeleteItem(int deleteItemID, DatabaseEntities dbCon)
    {
        string message = "";
        var item = dbCon.TC4R_List.SingleOrDefault(c => c.ThriftyCountry_ID == deleteItemID);
        try
        {
            dbCon.TC4R_List.Remove(item);
            dbCon.SaveChanges();
        }
        catch
        {
            // dbCon.CarGroups.Detach(item);
            message = "Error occurred while deleting " + item.CountryIDFK + ". Country already in use. |";
        }
        return message;
    }

    protected void LoadCountries()
    {
        db = new DatabaseEntities();
        var country = (from t in db.TC4R_List
                       join c in db.Countries on t.CountryIDFK equals c.CountryID
                       orderby c.CountryName
                       select new { t.ThriftyCountry_ID, c.CountryName, c.CountryCode });

        InformationTable.DataSource = country.ToList();
        InformationTable.DataBind();
        if (country.Count() > 0)
        {
            InformationTable.Visible = true;
            DeleteItems.Visible = true;
        }
        else
        {
            NoRecordsFound.Visible = true;
        }


    }
    protected void GridView_MouseOver(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowIndex >= 0)
        {
            e.Row.Attributes.Add("onmouseover", "this.style.background='#FFFFFF'");
            e.Row.Attributes.Add("onmouseout", "this.style.background='#F7F7F7'");
        }
    }
}