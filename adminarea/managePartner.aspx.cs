﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ORM;
using System.Linq;
using CMSConfigs;

public partial class managePartner : System.Web.UI.Page
{
    private DatabaseEntities db;
    protected void Page_Load(object sender, EventArgs e)
    {
        Master.Page.Title = CMSSettings.SiteTitle + " - Edit Section - Manage Accessories";

        if (Session["LoggedInName"] == null)
        {
            Response.Redirect("default.aspx?TimeOut=y&ReturnPage=" + Server.UrlEncode(HttpContext.Current.Request.Url.AbsoluteUri));
        }
        if (!Page.IsPostBack)
        {
            DisplayActionMessage();
            PopulateTables();
        }
    }
   
    protected string YesNoWording(int valueInDatabase)
    {
        return valueInDatabase == 1 ? "Enable" : "Disable";
    }
    protected string nullcheck(string valueInDatabase)
    {
        return valueInDatabase == "" ? "-" : valueInDatabase;
    }

    protected void DeleteItems_Click(object sender, EventArgs e)
    {
        ConfirmBox.ShowConfirmBox("Delete Partner", "Are you sure you want to remove selected Partner from system?", "Delete|Cancel");
        ConfirmBox.Visible = true;
    }
    protected void Confirm_Fired(object sender, EventArgs e)
    {
        bool confirm = (bool)sender;
        if (confirm == true)
        {
            DeleteDatabaseItem(InformationTable);
        }
    }

    protected void DeleteDatabaseItem(Repeater RepeaterID)
    {
        string message = "";
        //initialize the objectContext
        db = new DatabaseEntities();
        foreach (RepeaterItem item in RepeaterID.Items)
        {
            CheckBox cb = (CheckBox)item.FindControl("rowcheck");
            if (cb != null && cb.Checked)
            {
                Literal DBrow = (Literal)item.FindControl("rowID");
                int row = Convert.ToInt16(DBrow.Text);
                message += DeleteItem(row, db);
            }
        }
        if (string.IsNullOrEmpty(message))
        {
            Response.Redirect("managePartner.aspx?MsgType=2&MsgToShow=Item(s) deleted successfully.");
        }
        else
        {
            Response.Redirect("managePartner.aspx?MsgType=4&MsgToShow=" + message);
        }
    }
    protected string DeleteItem(int deleteItemID, DatabaseEntities dbCon)
    {
        string message = "";
        var item = dbCon.Partners.SingleOrDefault(l => l.PartnerId == deleteItemID);
        try
        {
            //var dbAccessoriesInfoValues = db.AccessoryInfoes.Where(ai => ai.AccessoryIDFK == deleteItemID).SingleOrDefault();
            //db.AccessoryInfoes.DeleteObject(dbAccessoriesInfoValues);
            dbCon.Partners.Remove(item);
            dbCon.SaveChanges();
        }
        catch
        {
           // dbCon.Partners.Detach(item);
           
            message = "Error occurred while deleting " + item.DomainName + ". Partner already in use. |";
        }
        return message;
    }
    protected void PopulateTables()
    {
        //initialize the objectContext
        db = new DatabaseEntities();

        var accessories = (from a in db.Partners
                      select new {
                          a.PartnerId,
                          a.DomainName,
                          a.VehicleLimtEnable,
                          a.Email,
                          a.ConfimationMailSend,
                          a.AgentDutyCode,
                          a.RequestorID_Type,
                          a.RequestorID_ID,
                          a.CompanyName_Code,
                          a.CT_Allowed,
                          a.CodeContext

                      });

        InformationTable.DataSource = accessories.ToList();
        InformationTable.DataBind();

        if (accessories.Count() > 0)
        {
            InformationTable.Visible = true;
            DeleteItems.Visible = true;
        }
        else
        {
            NoRecordsFound.Visible = true;
        }
    }
    protected void DisplayActionMessage()
    {
        if ((Request.QueryString["MsgType"] != null) && (Request.QueryString["MsgToShow"] != null))
        {
            string msgToShow = Request.QueryString["MsgToShow"].ToString();
            int rowID;
            bool result = int.TryParse(Request.QueryString["MsgType"], out rowID);
            if ((result) && !String.IsNullOrEmpty(msgToShow))
            {
                switch (Convert.ToInt16(Request.QueryString["MsgType"]))
                {
                    case 1:
                        MsgToShow.Show(MyMessageBox.MessageType.Info, msgToShow);
                        break;
                    case 2:
                        MsgToShow.Show(MyMessageBox.MessageType.Success, msgToShow);
                        break;
                    case 3:
                        MsgToShow.Show(MyMessageBox.MessageType.Warning, msgToShow);
                        break;
                    case 4:
                        MsgToShow.Show(MyMessageBox.MessageType.Error, msgToShow.Replace("|", "</br>"));
                        break;
                    default:
                        goto case 1;
                }
                MsgToShow.Visible = true;
            }
        }
    }
}