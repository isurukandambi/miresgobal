﻿<%@ Page Language="C#" AutoEventWireup="true"  CodeFile="Default.aspx.cs" Inherits="_DefaultEdit" %>
<%@ Register Src="~/adminarea/controls/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <script src="https://www.google.com/jsapi" type="text/javascript"></script> 
    <script type="text/javascript">google.load("jquery", "1.4.1")</script>
    <link href="css/validationEngine.jquery.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery.validationEngine.js" type="text/javascript"></script>
    <script src="js/jquery.validationEngine-en.js" type="text/javascript"></script>
    <script  type="text/javascript">
        $(document).ready(function () {
            $("#form1").validationEngine();
        });
    </script>
    <title><%=CMSConfigs.CMSSettings.SiteTitle%></title>
    <link rel="stylesheet" type="text/css" href="css/edit_styles.css" media="screen"/>
    <!--[if lte IE 6]>
    <link href="css/ie6styles.css" rel="stylesheet" type="text/css" media="all" />
    <![endif]-->
</head>
<body>
    <form id="form1" runat="server">
    <div id="wrapper">
        <div id="edit_header">
            <h1>Admin Area for <%=CMSConfigs.CMSSettings.SiteTitle%></h1>
        </div>
        <div id="contentLogin">
            <h2>Login</h2>
            <uc1:MyMessageBox ID="MsgToShow" runat="server" ShowCloseButton="false" />
            <fieldset>
                <div>
                    <label for="Username">Username</label>
                    <asp:TextBox ID="Username" CssClass="text validate[required]" runat="server" MaxLength="30" TabIndex="1"></asp:TextBox>
                </div>
                
                <div>
                    <label for="Password">Password </label>          
                    <asp:TextBox ID="Password" CssClass="text validate[required]" runat="server" MaxLength="30" TextMode="Password" TabIndex="2"></asp:TextBox>
                </div>
            </fieldset>
            <asp:Button ID="LoginButton" runat="server" CssClass="btn" Text="Login" TabIndex="3" />
            <div  class="littleLink"><a href="forgotpassword.aspx">Forgot your password?</a></div>
        </div>
        <div class="push"></div>    <!-- for sticky footer -->
    </div> <!-- end wrapper -->
    <div id="footer">
        <span>For problems or support, please contact us: <a href="mailto:clientsupport@justperfectit.com">clientsupport@justperfectit.com</a>.</span>
        JPIT Admin Area Tools Licenced by <a href="http://www.justperfectit.com" target="_blank">Just Perfect IT Ltd</a> | &copy;<script type="text/javascript" language="javascript">var thedate=new Date();var year=thedate.getFullYear();document.write(year);</script> Just Perfect IT Ltd.
    </div>
    </form>
</body>
</html>