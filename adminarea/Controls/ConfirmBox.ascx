﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ConfirmBox.ascx.cs" Inherits="Controls_ConfirmBox" %>
<%@ Register Src="~/adminarea/controls/ModalBlanket.ascx" TagName="ModalBlanket" TagPrefix="mb" %>
<link rel="stylesheet" type="text/css" href="../adminarea/css/bootstrap.min.css"
    media="screen" />
<script type="text/javascript">
    $(function () {

    });
</script>
<style type="text/css">
    .modal-header
    {
        padding: 1px 0px;
        width: auto;
        border-bottom: 1px solid #eee;
        background-color: #8bafc1;
    }
</style>
<asp:Panel ID="ConfirmBoxPanel" runat="server" CssClass="modal fade in">
    <div class="modal-header">
        <h3>
            <asp:Literal ID="ConfirmTitle" runat="server" />
        </h3>
    </div>
    <div class="modal-body">
        <p style="font-size: initial; margin-bottom: 5px; color: darkorange;">
            <asp:Literal ID="ConfirmMsg" runat="server" /></p>
    </div>
    <div class="modal-footer">
        <asp:Button ID="ConfirmButton" CssClass="btn btn-override" runat="server" Text="Ok"
            OnClick="Ok_Click" />
        <asp:Button ID="CancelButton" CssClass="btn btn-override" runat="server" Text="Cancel"
            OnClick="Cancel_Click" />
    </div>
</asp:Panel>
<mb:ModalBlanket ID="ModalBlanket" runat="server" Visible="true" />
