﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Controls_ConfirmBox : System.Web.UI.UserControl
{
    public event EventHandler FireConfirm;
    #region Show control
    public void ShowConfirmBox(string confirmTitle, string confirmMsg, string btnText)
    {
        ConfirmBoxPanel.Visible = true;
        ModalBlanket.Visible = true;
        ConfirmTitle.Text = confirmTitle;
        ConfirmMsg.Text = confirmMsg;
        CancelButton.Text = btnText.Split('|')[1];
        ConfirmButton.Text = btnText.Split('|')[0];
    }
    #endregion

    #region Hide control
    public void Hide(bool state)
    {
        ConfirmBoxPanel.Visible = false;
        ModalBlanket.Visible = false;
        FireConfirm(state, EventArgs.Empty);
    }
    #endregion

    protected void Cancel_Click(object sender, EventArgs e)
    {
        this.Hide(false);
    }
    protected void Ok_Click(object sender, EventArgs e)
    {
        this.Hide(true);
    }
    protected void Page_Load(object sender, EventArgs e)
    {

    }
}