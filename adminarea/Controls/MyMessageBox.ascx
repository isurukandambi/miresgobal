﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MyMessageBox.ascx.cs"
    Inherits="MyMessageBox" %>

<div class="container">
    <asp:Panel ID="MessageBox" runat="server">
        <asp:HyperLink runat="server" ID="CloseButton">
            <asp:Image runat="server" class="CloseX" ImageUrl="~/adminarea/images/cross.png" AlternateText="Click here to close this message" />
        </asp:HyperLink>
        <p>
            <asp:Literal ID="litMessage" runat="server"></asp:Literal>
        </p>
    </asp:Panel>
</div>
