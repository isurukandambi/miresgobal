﻿using System;
using CMSConfigs;

public partial class ForgotPasswordSent_edit : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = CMSSettings.SiteTitle + " - " + PageName.Text;
    }
}