﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ORM;
using CMSConfigs;

public partial class adminarea_addBrandCountryMap : System.Web.UI.Page
{
    private DatabaseEntities db;
    protected void Page_Load(object sender, EventArgs e)
    {
        Master.Page.Title = CMSSettings.SiteTitle + " - " + PageName.Text;
        if (Session["LoggedInName"] == null)
        {
            Response.Redirect("default.aspx?TimeOut=y&ReturnPage=" + Server.UrlEncode(HttpContext.Current.Request.Url.AbsoluteUri));
        }
        if (!Page.IsPostBack)
        {
            int maxVehicleLimit = Convert.ToInt32(CMSSettings.MaxVehicleLimit);
            AdvisorText.InnerHtml = "Brands (Select brands and vehicle limit. (total vehicle limit of all brands not exceed " + maxVehicleLimit + "))";
            LoadCountries();

        }
        LoadBrands();
    }
    protected int countryID()
    {
        int rowID;
        bool result = int.TryParse(Request.QueryString["Country"], out rowID);
        if (result)
        {
            return rowID;
        }
        return 0;
    }

    protected void LoadCountries()
    {
        db = new DatabaseEntities();
        CountryIDFK.DataSource = db.Countries.OrderBy(c => c.CountryName).ToList();
        CountryIDFK.DataValueField = "CountryID";
        CountryIDFK.DataTextField = "CountryName";
        CountryIDFK.DataBind();
        CountryIDFK.Items.Insert(0, new ListItem("Select Country", "0"));
        CountryIDFK.SelectedValue = countryID().ToString();
        //LoadLocations(countryID(), Convert.ToInt32(PartnerIDFK.SelectedValue));
    }

    private string checkFormIsValid()
    {
        string errorMsg = "";
        int maxVehicleLimit = Convert.ToInt32(CMSSettings.MaxVehicleLimit);
        //check country was selected
        if (CountryIDFK.SelectedValue == "0")
        {
            errorMsg = "<li>Please select Country</li>";
        }

        //check brand checked from brand list
        if (BrandsPanel.Controls.OfType<Panel>().Where(p => (p.Controls.OfType<CheckBox>().Where(c => c.Checked == true).Count() > 0)).Count() == 0)
        {
            errorMsg += "<li>Please select one or many Brands</li>";
        }

        var selectedTotalLimit = (from d in BrandsPanel.Controls.OfType<Panel>()
                                  where ((CheckBox)d.Controls[0]).Checked == true
                                  group d by ((DropDownList)d.Controls[1]).Visible into t
                                  select new
                                  {
                                      TotalLimit = t.Sum(f => Convert.ToInt32(((DropDownList)f.Controls[1]).SelectedValue))
                                  });
        ////check total limit of all brands more than 9
        if (selectedTotalLimit.Count() > 0)
        {
            if (selectedTotalLimit.First().TotalLimit > maxVehicleLimit)
            {
                errorMsg += "<li>Total vehicle limit from all brands may not exceed " + maxVehicleLimit + "</li>";
            }
        }

        return errorMsg;
    }

    protected void AddButton_Click(object sender, EventArgs e)
    {

        MsgToShow.Visible = false;
        string errorMsg = checkFormIsValid();
        if (string.IsNullOrEmpty(errorMsg))
        {
            try
            {
                int countryID = Convert.ToInt32(CountryIDFK.SelectedValue);

                var checkedBrands = (from d in BrandsPanel.Controls.OfType<Panel>()
                                     where ((CheckBox)d.Controls[0]).Checked == true
                                     select new
                                     {
                                         BrandID = Convert.ToInt32(((CheckBox)d.Controls[0]).ID),
                                         VehicleLimit = Convert.ToInt32(((DropDownList)d.Controls[1]).SelectedValue),
                                         Cargroups = ((CheckBoxList)((Panel)d.Controls[3]).Controls[0]).Items.OfType<ListItem>().Where(f => f.Selected == true)
                                     });

                    foreach (var brand in checkedBrands)
                    {
                        BrandCountry brandLocation = new BrandCountry
                        {
                            BrandIDFK = brand.BrandID,
                            CountryIDFK = countryID,
                            VehicleLimit = brand.VehicleLimit
                        };
                        db.BrandCountries.Add(brandLocation);
                        db.SaveChanges();

                        foreach (var carGroup in brand.Cargroups)
                        {
                            int carGroupID = Convert.ToInt32(carGroup.Value);
                            CarGroupBrandMapping cgbm = new CarGroupBrandMapping
                            {
                                BrandLocationIDFK = brandLocation.BrandCountry_ID,
                                CarGroupIDFK = carGroupID
                            };
                            db.CarGroupBrandMappings.Add(cgbm);
                        }
                        db.SaveChanges();
                    }

                    Response.Redirect("manageBrandCountriesMapping.aspx?MsgType=2&MsgToShow=Brands mapping for Countries added successfully&Country=" + countryID);
            }
            catch (Exception ex)
            {
                MsgToShow.Show(MyMessageBox.MessageType.Error, "Unexepected Error occured while processing." + ex.Message + ex.StackTrace);
                MsgToShow.Visible = true;
            }
        }
        else
        {
            MsgToShow.Show(MyMessageBox.MessageType.Error, "<ul>" + errorMsg + "</ul>");
        }

    }

    protected void LoadBrands()
    {
        db = new DatabaseEntities();
        var brands = db.Brands.OrderBy(b => b.BrandName);
        int maxVehicleLimit = Convert.ToInt32(CMSSettings.MaxVehicleLimit);
        foreach (var brand in brands)
        {
            //adding main panel for all controls
            Panel controlsPanel = new Panel();
            controlsPanel.CssClass = "BrandHolderControlPanel";

            //Brand Checkbox
            CheckBox brandCheckBox = new CheckBox();
            brandCheckBox.CssClass = "BrandIndicatorCheckBox";
            brandCheckBox.Text = brand.BrandName;
            brandCheckBox.AutoPostBack = true;

            //validate locations by brand when selecting brands
            //brandCheckBox.CheckedChanged += new EventHandler(this.BrandChecked_Changed);
            brandCheckBox.ID = brand.BrandID.ToString();

            //Add Vehicle limit selct box
            DropDownList vehLimitddl = new DropDownList();
            vehLimitddl.CssClass = "VehicleLimitddl";
            vehLimitddl.ID = "VehicleLimitDDL" + brand.BrandID;
            vehLimitddl.Items.Insert(0, new ListItem("Select Vehicle Limit", "0"));

            //Populate Vehicle limit dropdown list
            for (int i = maxVehicleLimit; i >= 1; i--)
            {
                vehLimitddl.Items.Insert(1, new ListItem(i.ToString(), i.ToString()));
            }
            vehLimitddl.SelectedValue = "0";

            //Add vehicle group title for checkbox list
            Label vehicleGrpsLabel = new Label();
            vehicleGrpsLabel.Text = "<input type=\"checkbox\" class=\"toggleAllCarGroups\" style=\"margin-left: 6%;\" checked=\"true\" /> <b>Vehicle Groups</b>";

            //Add and populate Car groups
            CheckBoxList vehicleGroupsList = new CheckBoxList();
            vehicleGroupsList.DataSource = db.CarGroups.OrderBy(cg => cg.CarCode).ToList();
            vehicleGroupsList.DataValueField = "CarGroupID";
            vehicleGroupsList.DataTextField = "CarCode";
            vehicleGroupsList.DataBind();

            foreach (ListItem item in vehicleGroupsList.Items)
            {
                item.Selected = true;
            }

            //Add car groups to panel to add overflow scroll behavior to checkbox list
            Panel overflowPanel = new Panel();
            overflowPanel.CssClass = "checkboxListsInBrandPanel";
            overflowPanel.Controls.Add(vehicleGroupsList);

            //Adding All Controls and panels to Main Brandpanel
            controlsPanel.Controls.Add(brandCheckBox);
            controlsPanel.Controls.Add(vehLimitddl);
            controlsPanel.Controls.Add(vehicleGrpsLabel);
            controlsPanel.Controls.Add(overflowPanel);
            BrandsPanel.Controls.Add(controlsPanel);
        }
    }
}