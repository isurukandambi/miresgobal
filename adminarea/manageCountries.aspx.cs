﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ORM;
using System.Linq;
using CMSConfigs;
using CMSUtils;

public partial class manageCountries : System.Web.UI.Page
{
    private DatabaseEntities db;
    string countMessage;
    protected void Page_Load(object sender, EventArgs e)
    {
        Master.Page.Title = CMSSettings.SiteTitle + " - Edit Section - Manage Countries";

        if (Session["LoggedInName"] == null)
        {
            Response.Redirect("default.aspx?TimeOut=y&ReturnPage=" + Server.UrlEncode(HttpContext.Current.Request.Url.AbsoluteUri));
        }
        if (!Page.IsPostBack)
        {
            LoadCountries();
        }
        MsgToShow.Visible = false;
        if (Request.QueryString["MsgToShow"] != null)
        {
            int MsgType = Convert.ToInt16(Request.QueryString["MsgType"]);
            switch (MsgType)
            {
                case 1:
                    MsgToShow.Show(MyMessageBox.MessageType.Info, Request.QueryString["MsgToShow"].ToString());
                    break;
                case 2:
                    MsgToShow.Show(MyMessageBox.MessageType.Success, Request.QueryString["MsgToShow"].ToString());
                    break;
                case 3:
                    MsgToShow.Show(MyMessageBox.MessageType.Warning, Request.QueryString["MsgToShow"].ToString());
                    break;
                case 4:
                    MsgToShow.Show(MyMessageBox.MessageType.Error, Request.QueryString["MsgToShow"].ToString());
                    break;
                default:
                    goto case 1;
            }
            MsgToShow.Visible = true;
        }
        
    }
    protected void DeleteItems_Click(object sender, EventArgs e)
    {
        CheckMap(InformationTable);
        if (countMessage == "")
        {
            ConfirmBox.ShowConfirmBox("Delete Countries", "Are you sure you want to remove selected Countries from system?", "Delete|Cancel");
            ConfirmBox.Visible = true;
        }
        else
        {
            ConfirmBox.ShowConfirmBox("Delete Countries", "Are you sure you want to remove selected Countries from system?</br>And "+countMessage+" with Child Mappings", "Delete|Cancel");
            ConfirmBox.Visible = true;
        }
    }
    protected void Confirm_Fired(object sender, EventArgs e)
    {
        bool confirm = (bool)sender;
        if (confirm == true)
        {
            DeleteDatabaseItem(InformationTable);
        }
    }

    protected void CheckMap(Repeater RepeaterID)
    {
        db = new DatabaseEntities();
        foreach (RepeaterItem item in RepeaterID.Items)
        {

            CheckBox cb = (CheckBox)item.FindControl("rowcheck");
            if (cb != null && cb.Checked)
            {
                Literal DBrow = (Literal)item.FindControl("rowID");
                int row = Convert.ToInt16(DBrow.Text);
                var locations = (from l in db.Locations
                                 join c in db.Countries on l.CountryIDFK equals c.CountryID
                                 where l.CountryIDFK == row
                                 select new
                                 {
                                     l.LocationName,
                                     c.CountryName
                                 });
                if (locations.Count() > 0)
                {
                    countMessage = locations.FirstOrDefault().CountryName+", ";
                }
                else
                {
                    countMessage = "";
                }

            }
        }
    }
   
    protected void DeleteDatabaseItem(Repeater RepeaterID)
    {
        try
        {
            //initialize the objectContext
            db = new DatabaseEntities();
            foreach (RepeaterItem item in RepeaterID.Items)
            {
               
                CheckBox cb = (CheckBox)item.FindControl("rowcheck");
                if (cb != null && cb.Checked)
                {
                    Literal DBrow = (Literal)item.FindControl("rowID");
                    int row = Convert.ToInt16(DBrow.Text);
                    RemoveLocations(row);
                    DeleteItem(row, db);    
                }
            }
            db.SaveChanges();
            Response.Redirect("manageCountries.aspx?MsgType=1&MsgToShow=Item(s) deleted successfully");
        }
        catch(Exception ex)
        {
            //Response.Redirect("manageCountries.aspx?MsgType=4&MsgToShow=" + CommonTasks.FKconstrainsFail());
            //Response.Redirect("manageBrands.aspx?MsgType=4&MsgToShow=" + ex.Message+ ex.StackTrace);
            MsgToShow.Show(MyMessageBox.MessageType.Error, CMSUtilities.FKconstrainsFail());
            MsgToShow.Visible = true;
        }
    }
    protected void DeleteItem(int countryID, DatabaseEntities dbCon)
    {
        try
        {
            //db = new DatabaseEntities();
            var country = dbCon.Countries.SingleOrDefault(c => c.CountryID == countryID);
            dbCon.Countries.Remove(country);
            dbCon.SaveChanges();
            //Response.Redirect("manageCountries.aspx?MsgType=2&MsgToShow=Country Deleted.");
        }
        catch(Exception ex)
        {
            
            //Response.Redirect("manageCountries.aspx?MsgType=4&MsgToShow=" + CommonTasks.FKconstrainsFail());
            //Response.Write(ex);
            MsgToShow.Show(MyMessageBox.MessageType.Error, "There was an error adding this information. Please try again." + ex.Message + "</br>" + ex.StackTrace);
            MsgToShow.Visible = true;

        }
    }

    private void RemoveLocations(int id)
    {
        db = new DatabaseEntities();
        var locations = (from l in db.Locations
                         where l.CountryIDFK == id
                         select new
                         {
                             l.LocationID
                         });
        if (locations.Count() > 0)
        {
            foreach (var loc in locations)
            {
                var locatin = db.Locations.SingleOrDefault(l => l.LocationID == loc.LocationID);
                db.Locations.Remove(locatin);
                
            }
            db.SaveChanges();
        }
    }
    protected void LoadCountries() { 
        db = new DatabaseEntities();
         var country= (from c in db.Countries
                                orderby c.CountryName
                                select new {c.CountryID , c.CountryName, c.CountryCode});
         
         InformationTable.DataSource = country.ToList();
        InformationTable.DataBind();
        if (country.Count() > 0)
        {
            InformationTable.Visible = true;
            DeleteItems.Visible = true;
        }
        else
        {
            NoRecordsFound.Visible = true;
        }

        
    }
    protected void GridView_MouseOver(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowIndex >= 0)
        {
            e.Row.Attributes.Add("onmouseover", "this.style.background='#FFFFFF'");
            e.Row.Attributes.Add("onmouseout", "this.style.background='#F7F7F7'");
        }
    }
}
