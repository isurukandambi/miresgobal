﻿<%@ Page Language="C#" MasterPageFile="adminarea.master" AutoEventWireup="true" CodeFile="addCountry.aspx.cs" Inherits="addCountry" %>
<%@ Register Src="~/adminarea/controls/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <link href="js/validationEngine/css/validationEngine.jquery.css" rel="stylesheet" type="text/css" />
    <script src="js/validationEngine/jquery.validationEngine.js" type="text/javascript"></script>
    <script src="js/validationEngine/jquery.validationEngine-en.js" type="text/javascript"></script>
    <script type="text/javascript" language="javascript">
        $(document).ready(function () {            
            $("#form1").validationEngine();
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div id="content">
    <uc1:MyMessageBox ID="MsgToShow" runat="server" ShowCloseButton="true" Visible="false" />

    <h3><asp:Literal ID="PageName" runat="server" Text="Add Country" /></h3>
    <div id="actionArea">
    <fieldset>
        <div>
            <label for="Countryname" class="inline">Country Name </label>
            <asp:TextBox ID="Countryname" CssClass="text inline validate[required]" runat="server" MaxLength="100"></asp:TextBox>
        </div>
        <div>
            <label for="CountryCode" class="inline">Country Code </label>
            <asp:TextBox ID="CountryCode" CssClass="text inline validate[required]" runat="server" MaxLength="2"></asp:TextBox>
        </div>
        <div class="formActions">
            <asp:Button ID="AddButton" CssClass="btn" OnClick="AddButton_Click" runat="server" Text="Add Country" />
            <a href="manageCountries.aspx" class="CancelButton">Cancel</a>
        </div>
    </fieldset>
    </div>
</div>
</asp:Content>

