﻿<%@ Page Language="C#" MasterPageFile="adminarea.master" AutoEventWireup="true" CodeFile="updateThermeonCarDetails.aspx.cs" Inherits="adminarea_updateThermeonCarDetails" %>

<%@ Register Src="~/adminarea/controls/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <link href="js/validationEngine/css/validationEngine.jquery.css" rel="stylesheet" type="text/css" />
    <script src="js/validationEngine/jquery.validationEngine.js" type="text/javascript"></script>
    <script src="js/validationEngine/jquery.validationEngine-en.js" type="text/javascript"></script>
    <script type="text/javascript" language="javascript">
        $(document).ready(function () {
            $("#form1").validationEngine();
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div id="content">
    <uc1:MyMessageBox ID="MsgToShow" runat="server" ShowCloseButton="true" Visible="false" />

    <h3><asp:Literal ID="PageName" runat="server" Text="Update Vehicle Details" /></h3>
    <div id="actionArea">
    <fieldset>
        <div >
            <label for="CountryID" class="inline">Country </label>
            <asp:DropDownList ID="CountryID" visible="true"  runat="server">
            </asp:DropDownList>
            <asp:HiddenField runat="server" ID="ExistingCountryID" />
        </div>
        <div>
            <label for="CarGroupID" class="inline">Vehicle Group </label>
                    <asp:DropDownList ID="CarGroupID" runat="server">
                    </asp:DropDownList>
        </div>
        <div>
            <label for="BrandID" class="inline">Brand </label>
                    <asp:DropDownList ID="BrandID" runat="server">
                    </asp:DropDownList>
        </div>
        <div>
            <label for="Description" class="inline">Description </label>
            <asp:TextBox ID="Description" CssClass="text inline validate[required]" runat="server" MaxLength="30"></asp:TextBox>
        </div>
        <div class="shaded">
                    <asp:HiddenField runat="server" ID="OldImage" />
                    <label for="File1" class="inline">Vehicle Image</label>
                    <input id="filPhoto" class="fileInput inline" type="file" runat="server" />
                    <span class="advisory fileAdv">File must be less than 3MB</span>
                    <br class="clearBoth">
                    <label class="inline" runat="server" id="CurrentImageText">Current Vehicle Uploaded </label>
                    <span class="inlineContainer" runat="server" id="CurrentImageControlsHolder">
                        <asp:Literal ID="CurrentImage" runat="server" />
                    </span>
                </div>
        <div class="formActions">
            <asp:Button ID="UpdateButton" CssClass="btn" OnClick="UpdateButton_Click" runat="server" Text="Update Car Group" />
            <a href="manageThermeonVehicleDetails.aspx?Row=<%=CountryID.SelectedValue %>" class="CancelButton">Cancel</a>
        </div>
    </fieldset>
    </div>
</div>
</asp:Content>