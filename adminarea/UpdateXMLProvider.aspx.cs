﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ORM;
using CMSConfigs;

public partial class adminarea_UpdateXMLProvider : System.Web.UI.Page
{
    private DatabaseEntities db;
    protected void Page_Load(object sender, EventArgs e)
    {
        Master.Page.Title = CMSSettings.SiteTitle + " - " + PageName.Text;
        if (Session["LoggedInName"] == null)
        {
            Response.Redirect("default.aspx?TimeOut=y&ReturnPage=" + Server.UrlEncode(HttpContext.Current.Request.Url.AbsoluteUri));
        }
        if (!Page.IsPostBack)
        {
            LoadData();
        }
    }

    protected void LoadData()
    {
        db = new DatabaseEntities();
        int row = rowID();
        var XMLProv = db.Providers.SingleOrDefault(p => p.Pro_Id == row);
        XMLProvider.Text = XMLProv.ProviderName;

    }
    protected int rowID()
    {
        int rowID;
        bool result = int.TryParse(Request.QueryString["Row"], out rowID);
        if (result)
        {
            return rowID;
        }
        return 0;
    }

    protected void UpdateButton_Click(object sender, EventArgs e)
    {
        try
        {
            // initialize the objectContext
            db = new DatabaseEntities();
            int row = rowID();
            if (!db.Providers.Where(p => p.ProviderName == XMLProvider.Text.Trim() && p.Pro_Id != row).Any())
            {
                var XMLProvidr = db.Providers.SingleOrDefault(p => p.Pro_Id == row);
                XMLProvidr.ProviderName = XMLProvider.Text.Trim();
                db.SaveChanges();

                Response.Redirect("manageXMLproviders.aspx?MsgToShow=XML Provider updated successfully.&MsgType=2");
            }
            else
            {
                MsgToShow.Show(MyMessageBox.MessageType.Warning, "That XML Provider already exists. Please use another  XML Provider.");
                MsgToShow.Visible = true;
            }

        }
        catch
        {
            MsgToShow.Show(MyMessageBox.MessageType.Error, "There was an error adding this information. Please try again.");
            MsgToShow.Visible = true;
        }
    }
}