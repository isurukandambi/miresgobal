﻿<%@ Page Language="C#" MasterPageFile="adminarea.master" AutoEventWireup="true" CodeFile="addBrandProviders.aspx.cs" Inherits="adminarea_addBrandProviders" %>
<%@ Register Src="~/adminarea/controls/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="js/validationEngine/css/validationEngine.jquery.css" rel="stylesheet" type="text/css" />
    <script src="js/validationEngine/jquery.validationEngine.js" type="text/javascript"></script>
    <script src="js/validationEngine/jquery.validationEngine-en.js" type="text/javascript"></script>
    <script type="text/javascript" language="javascript">
        $(document).ready(function () {
            $("#form1").validationEngine();
        });
    </script>
    <style type="text/css">
        input.inline {
             float: none;
        }
        input.shortField {
            width: 72%;
            padding: 2px 5px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="content">
        <uc1:MyMessageBox ID="MsgToShow" runat="server" ShowCloseButton="true" Visible="false" />

        <h3>
            <asp:Literal ID="PageName" runat="server" Text="Add Provider by Brand" /></h3>
        <div id="actionArea">
            <fieldset>
                <div>
                    <label for="BrandID" class="inline">Brand </label>
                    <asp:DropDownList ID="BrandID" runat="server">
                    </asp:DropDownList>
                </div>
                <div>
                    <label for="ProviderID" class="inline">Provider </label>
                    <asp:DropDownList ID="ProviderID" runat="server">
                    </asp:DropDownList>
                </div>
                <div class="formActions">
                    <asp:Button ID="AddButton" CssClass="btn" OnClick="AddButton_Click" runat="server" Text="Add Provider by Brand" />
                    <a href="manageBrandProviders.aspx?Brand=<%=brandID() %>" class="CancelButton">Cancel</a>
                </div>
            </fieldset>
        </div>
    </div>
</asp:Content>
