﻿using System;
using System.Linq;
using System.Web.UI.WebControls;
using ORM;
using CMSConfigs;

public partial class manageRoles : System.Web.UI.Page
{
    private DatabaseEntities db;

    protected void Page_Load(object sender, EventArgs e)
    {
        Master.Page.Title = CMSSettings.SiteTitle + " - " + PageName.Text;
        
        if (!Page.IsPostBack)
        {
            DisplayActionMessage();
            PopulateTables();
        }
    }

    protected void DeleteItems_Click(object sender, EventArgs e)
    {
        ConfirmBox.ShowConfirmBox("Delete Roles", "Are you sure you want to remove selected Roles from system?", "Delete|Cancel");
        ConfirmBox.Visible = true;
    }
    protected void Confirm_Fired(object sender, EventArgs e)
    {
        bool confirm = (bool)sender;
        if (confirm == true)
        {
            DeleteDatabaseItem(InformationTable);
        }
    }
    protected void DeleteDatabaseItem(Repeater RepeaterID)
    {
        //initialize the objectContext
        db = new DatabaseEntities();
        string message = "";
        foreach (RepeaterItem item in RepeaterID.Items)
        {
            CheckBox cb = (CheckBox)item.FindControl("rowcheck");
            if (cb != null && cb.Checked)
            {
                Literal DBrow = (Literal)item.FindControl("rowID");
                int row = Convert.ToInt16(DBrow.Text);
                var dbValues = db.Roles.Where(r => r.RoleID == row).SingleOrDefault();
                db.Roles.Remove(dbValues);
                var dbRoleSiteFunctions = db.RoleSiteAccesses.Where(rs => rs.RoleIDFK == row);
                foreach(var sitefunc in dbRoleSiteFunctions){
                    db.RoleSiteAccesses.Remove(sitefunc);
                }
                try
                {
                    db.SaveChanges();
                }
                catch
                {
                    message += "Error occurred while deleting "+ dbValues.RoleName+ " Users already assigned. </br>";
                }
                
            }
        }
        if (string.IsNullOrEmpty(message))
        {
            Response.Redirect("manageRoles.aspx?MsgType=2&MsgToShow=Item(s) deleted successfully.");
        }
        else
        {
            MsgToShow.Show(MyMessageBox.MessageType.Error, message);
            MsgToShow.Visible = true;
        }
        
    }

    protected void PopulateTables()
    {
        //initialize the objectContext
        db = new DatabaseEntities();

        var roles = db.Roles.OrderBy(r => r.RoleName);
        InformationTable.DataSource = roles.ToList();
        InformationTable.DataBind();

        if (roles.Count() > 0)
        {
            InformationTable.Visible = true;
            DeleteItems.Visible = true;
        }
        else
        {
            NoRecordsFound.Visible = true;
        }
    }

    protected void InformationTable_RowDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            int rowID = Convert.ToInt16(((Literal)e.Item.FindControl("rowID")).Text);
            
            //if this row is not 1 then allow user update or delete this account.
            if (rowID != 1)
            {
                ((Literal)e.Item.FindControl("RoleNamelbl")).Text = "<a href=\"updateRole.aspx?Row=" + ((Literal)e.Item.FindControl("rowID")).Text + "\">" + ((Literal)e.Item.FindControl("RoleNamelbl")).Text + "</a>";
            }
            else
            {
                //this is the admin role so hide the delete box
                ((CheckBox)e.Item.FindControl("rowcheck")).Visible = false;
            }

            //initialize the objectContext
            db = new DatabaseEntities();

            //get the users within this role - excludes Dragnet Admin account
            var logins = db.Logins.Where(l => l.RoleIDFK == rowID && l.LoginID != 1);
            foreach (var login in logins)
            {
                ((Literal)e.Item.FindControl("UsersInRole")).Text += login.LoggedInName + "<br />";
            }
        }
    }

    protected void DisplayActionMessage()
    {
        if ((Request.QueryString["MsgType"] != null) && (Request.QueryString["MsgToShow"] != null))
        {
            string msgToShow = Request.QueryString["MsgToShow"].ToString();
            int rowID;
            bool result = int.TryParse(Request.QueryString["MsgType"], out rowID);
            if ((result) && !String.IsNullOrEmpty(msgToShow))
            {
                switch (Convert.ToInt16(Request.QueryString["MsgType"]))
                {
                    case 1:
                        MsgToShow.Show(MyMessageBox.MessageType.Info, msgToShow);
                        break;
                    case 2:
                        MsgToShow.Show(MyMessageBox.MessageType.Success, msgToShow);
                        break;
                    case 3:
                        MsgToShow.Show(MyMessageBox.MessageType.Warning, msgToShow);
                        break;
                    case 4:
                        MsgToShow.Show(MyMessageBox.MessageType.Error, msgToShow);
                        break;
                    default:
                        goto case 1;
                }
                MsgToShow.Visible = true;
            }
        }
    }
}
