﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ORM;
using CMSConfigs;

public partial class adminarea_updateBrandCountryMapping : System.Web.UI.Page
{
    private DatabaseEntities db;
    protected void Page_Load(object sender, EventArgs e)
    {
        Master.Page.Title = CMSSettings.SiteTitle + " - " + PageName.Text;
        if (Session["LoggedInName"] == null)
        {
            Response.Redirect("default.aspx?TimeOut=y&ReturnPage=" + Server.UrlEncode(HttpContext.Current.Request.Url.AbsoluteUri));
        }
        if (!Page.IsPostBack)
        {
            int maxVehicleLimit = Convert.ToInt32(CMSSettings.MaxVehicleLimit);
            AdvisorText.InnerHtml = "Brands (Select brands and vehicle limit. (total vehicle limit of all brands not exceed " + maxVehicleLimit + "))";
        }
        LoadBrands();
        LoadData();
    }

    protected void LoadBrands()
    {
        db = new DatabaseEntities();
        var brands = db.Brands.OrderBy(b => b.BrandName);
        int maxVehicleLimit = Convert.ToInt32(CMSSettings.MaxVehicleLimit);
        foreach (var brand in brands)
        {
            //adding main panel for all controls
            Panel controlsPanel = new Panel();
            controlsPanel.CssClass = "BrandHolderControlPanel";

            //Brand Checkbox
            CheckBox brandCheckBox = new CheckBox();
            brandCheckBox.CssClass = "BrandIndicatorCheckBox";
            brandCheckBox.Text = brand.BrandName;
            brandCheckBox.ID = brand.BrandID.ToString();

            //Add Vehicle limit selct box
            DropDownList vehLimitddl = new DropDownList();
            vehLimitddl.CssClass = "VehicleLimitddl";
            vehLimitddl.ID = "VehicleLimitDDL" + brand.BrandID;
            vehLimitddl.Items.Insert(0, new ListItem("Select Vehicle Limit", "0"));

            //Populate Vehicle limit dropdown list
            for (int i = maxVehicleLimit; i >= 1; i--)
            {
                vehLimitddl.Items.Insert(1, new ListItem(i.ToString(), i.ToString()));
            }
            vehLimitddl.SelectedValue = "0";

            //Add vehicle group title for checkbox list
            Label vehicleGrpsLabel = new Label();
            vehicleGrpsLabel.Text = "<input type=\"checkbox\" class=\"toggleAllCarGroups\" style=\"margin-left: 6%;\" /> <b>Vehicle Groups</b>";

            //Add and populate Car groups
            CheckBoxList vehicleGroupsList = new CheckBoxList();
            vehicleGroupsList.DataSource = db.CarGroups.OrderBy(cg => cg.CarCode).ToList();
            vehicleGroupsList.DataValueField = "CarGroupID";
            vehicleGroupsList.DataTextField = "CarCode";
            vehicleGroupsList.DataBind();

            //Add car groups to panel to add overflow scroll behavior to checkbox list
            Panel overflowPanel = new Panel();
            overflowPanel.CssClass = "checkboxListsInBrandPanel";
            overflowPanel.Controls.Add(vehicleGroupsList);

            //Adding All Controls and panels to Main Brandpanel
            controlsPanel.Controls.Add(brandCheckBox);
            controlsPanel.Controls.Add(vehLimitddl);
            controlsPanel.Controls.Add(vehicleGrpsLabel);
            controlsPanel.Controls.Add(overflowPanel);
            BrandsPanel.Controls.Add(controlsPanel);
        }
    }

    protected void LoadData()
    {
        db = new DatabaseEntities();
        int row = rowID();
        var brandLocation = db.BrandCountries.SingleOrDefault(bl => bl.BrandCountry_ID == row);
        if (brandLocation != null)
        {
            CountryName.Text = db.Countries.SingleOrDefault(c => c.CountryID == brandLocation.CountryIDFK).CountryName;
            var allBrands = (from d in BrandsPanel.Controls.OfType<Panel>()
                             select new
                             {
                                 BrandCheckBoxID = Convert.ToInt32(((CheckBox)d.Controls[0]).ID),
                                 BrandCheckBox = ((CheckBox)d.Controls[0]),
                                 VehicleLimitddl = ((DropDownList)d.Controls[1]),
                                 Cargroups = ((CheckBoxList)((Panel)d.Controls[3]).Controls[0])
                             });
            foreach (var loadedBrandControl in allBrands)
            {
                var selectedbrandsAndLimit = db.BrandCountries.FirstOrDefault(bl => bl.BrandIDFK == loadedBrandControl.BrandCheckBoxID);
                //&& bl.PartnerIDFK = brandLocation.PartnerIDFK
                var selectedCarGroups = db.CarGroupBrandMappings.Where(cgbm => cgbm.BrandLocationIDFK == selectedbrandsAndLimit.BrandCountry_ID);
                if (selectedbrandsAndLimit != null)
                {
                    loadedBrandControl.BrandCheckBox.Checked = true;
                    loadedBrandControl.VehicleLimitddl.SelectedValue = selectedbrandsAndLimit.VehicleLimit.ToString();

                    foreach (var cargroup in selectedCarGroups)
                    {
                        loadedBrandControl.Cargroups.Items.Cast<ListItem>().SingleOrDefault(cg => cg.Value == cargroup.CarGroupIDFK.ToString()).Selected = true;
                    }
                }
            }
        }
        else
        {
            Response.Redirect("manageBrandCountriesMapping.aspx?MsgType=4&MsgToShow=Invalid Mapped location. Mapped Country not found.");
        }
    }

    protected int rowID()
    {
        int rowID;
        bool result = int.TryParse(Request.QueryString["Row"], out rowID);
        if (result)
        {
            return rowID;
        }
        return 0;
    }

    private string checkFormIsValid()
    {
        int maxVehicleLimit = Convert.ToInt32(CMSSettings.MaxVehicleLimit);
        string errorMsg = "";
        //check brand checked from brand list
        if (BrandsPanel.Controls.OfType<Panel>().Where(p => (p.Controls.OfType<CheckBox>().Where(c => c.Checked == true).Count() > 0)).Count() == 0)
        {
            errorMsg += "<li>Please select one or many Brands</li>";
        }

        var selectedTotalLimit = (from d in BrandsPanel.Controls.OfType<Panel>()
                                  where ((CheckBox)d.Controls[0]).Checked == true
                                  group d by ((DropDownList)d.Controls[1]).Visible into t
                                  select new
                                  {
                                      TotalLimit = t.Sum(f => Convert.ToInt32(((DropDownList)f.Controls[1]).SelectedValue))
                                  });
        ////check total limit of all brands more than 9
        if (selectedTotalLimit.Count() > 0)
        {
            if (selectedTotalLimit.First().TotalLimit > maxVehicleLimit)
            {
                errorMsg += "<li>Total vehicle limit from all brands may not exceed " + maxVehicleLimit + "</li>";
            }
        }

        return errorMsg;
    }

    protected void UpdateButton_Click(object sender, EventArgs e)
    {
        MsgToShow.Visible = false;
        string errorMsg = checkFormIsValid();
        if (string.IsNullOrEmpty(errorMsg))
        {
            try
            {
                db = new DatabaseEntities();
                int row = rowID();
                var checkedBrands = (from d in BrandsPanel.Controls.OfType<Panel>()
                                     where ((CheckBox)d.Controls[0]).Checked == true
                                     select new
                                     {
                                         BrandID = Convert.ToInt32(((CheckBox)d.Controls[0]).ID),
                                         VehicleLimit = Convert.ToInt32(((DropDownList)d.Controls[1]).SelectedValue),
                                         Cargroups = ((CheckBoxList)((Panel)d.Controls[3]).Controls[0]).Items.OfType<ListItem>().Where(f => f.Selected == true)
                                     });
                var currentBrandLoc = db.BrandCountries.SingleOrDefault(b => b.BrandCountry_ID == row);
                int countryID = currentBrandLoc.CountryIDFK;
                var brandLocations = db.BrandCountries.Where(bl => bl.CountryIDFK == countryID);
                foreach (var brandLoc in brandLocations)
                {
                    db.BrandCountries.Remove(brandLoc);
                    var carGroupsInBrandLoc = db.CarGroupBrandMappings.Where(cg => cg.BrandLocationIDFK == brandLoc.BrandCountry_ID);
                    foreach (var cargroup in carGroupsInBrandLoc)
                    {
                        db.CarGroupBrandMappings.Remove(cargroup);
                    }
                }
                db.SaveChanges();
                foreach (var brand in checkedBrands)
                {
                    BrandCountry brandLocation = new BrandCountry
                    {
                        BrandIDFK = brand.BrandID,
                        CountryIDFK = countryID,
                        VehicleLimit = brand.VehicleLimit
                    };
                    db.BrandCountries.Add(brandLocation);
                    db.SaveChanges();
                    foreach (var carGroup in brand.Cargroups)
                    {
                        int carGroupID = Convert.ToInt32(carGroup.Value);
                        CarGroupBrandMapping cgbm = new CarGroupBrandMapping
                        {
                            BrandLocationIDFK = brandLocation.BrandCountry_ID,
                            CarGroupIDFK = carGroupID
                        };
                        db.CarGroupBrandMappings.Add(cgbm);
                    }
                    db.SaveChanges();
                }
                Response.Redirect("manageBrandCountriesMapping.aspx?MsgType=2&MsgToShow=Brands mapping for countries modified successfully&Country=" + countryID);
            }
            catch (Exception ex)
            {
                MsgToShow.Show(MyMessageBox.MessageType.Error, "Unexepected Error occured while processing." + ex.Message + ex.StackTrace);
                MsgToShow.Visible = true;
            }
        }
        else
        {
            MsgToShow.Show(MyMessageBox.MessageType.Error, "<ul>" + errorMsg + "</ul>");
        }
    }
}