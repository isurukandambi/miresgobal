﻿using System;
using System.Linq;
using System.Text;
using System.Web;
using ORM;
using CMSConfigs;

public partial class addCountry : System.Web.UI.Page
{
    private DatabaseEntities db;

    protected void Page_Load(object sender, EventArgs e)
    {
        Master.Page.Title = CMSSettings.SiteTitle + " - " + PageName.Text;
        if (Session["LoggedInName"] == null)
        {
            Response.Redirect("default.aspx?TimeOut=y&ReturnPage=" + Server.UrlEncode(HttpContext.Current.Request.Url.AbsoluteUri));
        }
    }


    protected void AddButton_Click(object sender, EventArgs e)
    {
        try
        {
            // initialize the objectContext
            db = new DatabaseEntities();

            if (!db.Countries.Where(c => c.CountryCode == CountryCode.Text).Any())
            {
                Country country = new Country
                {
                    CountryName = Countryname.Text.Trim(),
                    CountryCode = CountryCode.Text.Trim()
                };
                db.Countries.Add(country);
                db.SaveChanges();

                Response.Redirect("manageCountries.aspx?MsgType=2&MsgToShow='" + Countryname.Text + "' added successfully");

            }
            else
            {
                MsgToShow.Show(MyMessageBox.MessageType.Warning, "Country Code already exists. Please use another Country Code.");
                MsgToShow.Visible = true;
            }

        }
        catch
        {
            MsgToShow.Show(MyMessageBox.MessageType.Error, "There was an error adding this information. Please try again.");
            MsgToShow.Visible = true;
        }
    }
}