﻿<%@ Page Language="C#" MasterPageFile="adminarea.master" AutoEventWireup="true" CodeFile="manageLanguagesOLD.aspx.cs" Inherits="manageLanguages" %>

<%@ Register Src="~/adminarea/controls/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" />
    <script type="text/javascript" language="javascript">
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
        function EndRequestHandler(sender, args) {
            $(document).ready(function () {
                //used for context menu on grids
                $('.device-actions-toggler').show();
                $('.device-actions-toggler').click(function () { return false; });
                $('.device-actions-list').css({ position: 'absolute', top: '0', right: '0' }).hide();

                $('.device-actions').css({ position: 'relative' }).each(function () {
                    $(this).hoverIntent(function () {
                        $(this).find('.device-actions-list').fadeIn('fast');
                    }, function () {
                        $(this).find('.device-actions-list').fadeOut('fast');
                    });
                });

            });
        }
    </script>
    <div id="content">
        <h2>Manage Languages</h2>
        <div class="buttons">
            <a href="addLanguage.aspx">Add Language</a>
        </div>

        <uc1:MyMessageBox ID="MsgToShow" runat="server" ShowCloseButton="true" />

        <h3>Existing Languages:</h3>

        <div id="actionArea">

            <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="GridViewPanel">
                <ProgressTemplate>
                    <span class="UpdatingPanel">
                        <img alt="indicator" src="images/indicator.gif" />
                        Updating... Please Wait...</span>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <asp:UpdatePanel ID="GridViewPanel" runat="server">
                <ContentTemplate>
                    <asp:GridView
                        ID="GridView1" OnRowCreated="GridView_MouseOver" runat="server"
                        CssClass="gridview gridview-classic narrowTable" AutoGenerateColumns="false"
                        DataKeyNames="LanguageID"
                        AllowPaging="True" AllowSorting="false" PageSize="24">
                        <EmptyDataTemplate>There are no languages in the database.</EmptyDataTemplate>
                        <Columns>
                            <asp:TemplateField HeaderText="Country" SortExpression="CountryName">
                                <ItemTemplate>
                                    <a href="updateLanguage.aspx?Row=<%# DataBinder.Eval(Container.DataItem, "LanguageID") %>"><%# Eval("LanguageName")%> (<%# Eval("LanguageCode")%>)</a>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Action">
                                <ItemStyle HorizontalAlign="Right" Width="50" />
                                <ItemTemplate>
                                    <div class="device-actions">
                                        <a href="#" class="device-actions-toggler">
                                            <img src="images/cog.png" alt="Actions" border="0" />
                                        </a>
                                        <div class="device-actions-list" style="display: none">
                                            <ul>
                                                <li><a href="updateLanguage.aspx?Row=<%# DataBinder.Eval(Container.DataItem, "LanguageID") %>">Update Language</a></li>
                                                <li>
                                                    <asp:LinkButton ID="DeleteItemLnk" runat="server">Delete Language</asp:LinkButton></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <cc1:ModalPopupExtender ID="mdlPopup" runat="server"
                                        TargetControlID="DeleteItemLnk"
                                        PopupControlID="DeleteItemPnl"
                                        BackgroundCssClass="modalBackground"
                                        CancelControlID="CancelActionLnk" />
                                    <asp:Panel ID="DeleteItemPnl" runat="server" CssClass="modalPanalWithButtons" Style="display: none">
                                        <div align="center" style="margin-top: 13px;">
                                            <p>
                                                <img src="images/exclamation.png" alt="warning">Are you sure you want to remove this language?</p>
                                            <div class="buttons">
                                                <a href="manageLanguages.aspx?DeleteRow=<%# DataBinder.Eval(Container.DataItem, "LanguageID") %>">Delete</a>
                                                <asp:LinkButton ID="CancelActionLnk" runat="server">Cancel</asp:LinkButton>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </ContentTemplate>
            </asp:UpdatePanel>
            <asp:UpdateProgress ID="UpdateProgress2" runat="server" AssociatedUpdatePanelID="GridViewPanel">
                <ProgressTemplate>
                    <span class="UpdatingPanel">
                        <img alt="indicator" src="images/indicator.gif" />
                        Updating... Please Wait...</span>
                </ProgressTemplate>
            </asp:UpdateProgress>

        </div>

    </div>
</asp:Content>

