﻿<%@ Page Language="C#" MasterPageFile="adminarea.master" AutoEventWireup="true" CodeFile="updatePartners.aspx.cs" Inherits="updatePartner" %>

<%@ Register Src="~/adminarea/controls/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="js/validationEngine/css/validationEngine.jquery.css" rel="stylesheet" type="text/css" />
    <script src="js/validationEngine/jquery.validationEngine.js" type="text/javascript"></script>
    <script src="js/validationEngine/jquery.validationEngine-en.js" type="text/javascript"></script>
    <script type="text/javascript" language="javascript">
        $(document).ready(function () {
            $("#form1").validationEngine();
        });
    </script>
    <style type="text/css">
        .style1
        {
            width: 185px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="content">
        <uc1:MyMessageBox ID="MsgToShow" runat="server" ShowCloseButton="true" Visible="false" />

        <h3>
            <asp:Literal ID="PageName" runat="server" Text="Update Partners" /></h3>
        <div id="actionArea">
            <fieldset>
                <%--                <div class="cr">
                    <label>
                        <asp:CheckBox ID="VisibleOffer" Text="Tick to hide this item on site" runat="server" />
                    </label>
                </div>

                <div class="cr">
                    <label>
                        <asp:CheckBox ID="DefaultItem" Text="Tick to set this accessory as one of the default accessories on the site" runat="server" />
                    </label>
                </div>--%>

                <div>
                    <label for="Title" class="inline">Domain Name</label>
                    <asp:TextBox ID="txtname" CssClass="text validate[required]" runat="server" 
                        MaxLength="60"></asp:TextBox>
                </div>
                 <div>
                    <label for="email" class="inline">Email</label>
                    <asp:TextBox ID="txtemail" CssClass="text validate[required]" runat="server" 
                        MaxLength="60"></asp:TextBox>
                </div>
                 <div>
            <label for="Agentdutycode" class="inline">Agent Duty Code </label>
            <asp:TextBox ID="txtAgentdutycode" CssClass="text inline validate[required,length[6,20]]"  
                runat="server" MaxLength="60"></asp:TextBox>
        </div>
         <div>
            <label for="RequestorID" class="inline">Requestor ID</label>
            <asp:TextBox ID="txtRequestorID" CssClass="text inline validate[required,length[6,20]]"  
                runat="server" MaxLength="60"></asp:TextBox>
        </div>
          <div>
            <label for="RequestorID_Type" class="inline">Requestor ID Type</label>
            <asp:TextBox ID="txtRequestorID_Type" CssClass="text inline validate[required,length[6,20]]"  
                runat="server" MaxLength="60"></asp:TextBox>
        </div>
        
          <div>
            <label for="CompanyName_Code" class="inline">Company Name Code</label>
            <asp:TextBox ID="txtCompanyName_Code" CssClass="text inline validate[required,length[6,20]]"  
                runat="server" MaxLength="60"></asp:TextBox>
        </div>
         <div>
            <label for="CodeContext" class="inline">Code Context</label>
            <asp:TextBox ID="txtCodeContext" CssClass="text inline validate[required,length[6,20]]"  
                runat="server" MaxLength="60"></asp:TextBox>
        </div>
       <div>
                    <label for="CT_Allowed" class="inline">Allow for CarTrawler </label>
                    <asp:DropDownList ID="CT_Allowed" runat="server">
                        <asp:ListItem Value="1" Text="Yes"></asp:ListItem>
                        <asp:ListItem Value="0" Text="No"></asp:ListItem>
                    </asp:DropDownList>
                </div>

                 <div class="cr">
                 
                    <label>
                        <asp:CheckBox ID="chkenable" Text="Vehicle Limit Enable." runat="server" />
                        
                    </label>
                    
                </div>
                   <div class="cr">
                 
                  <label>  <asp:CheckBox ID="chkemail" Text="Confirmation Email Sent." runat="server" />
                    </label>
                        </div>
                 
              

<%--                <div class="cr">
                    <label>
                        <asp:CheckBox ID="VisibleOffer" Text="Tick to hide this item on site" runat="server" />
                    </label>
                </div>

                <div class="cr">
                    <label>
                        <asp:CheckBox ID="DefaultItem" Text="Tick to set this accessory as one of the default accessories on the site" runat="server" />
                    </label>
                </div>--%>
                
                </div>
<%--                <div class="cr">
                    <label>
                        <asp:CheckBox ID="VisibleOffer" Text="Tick to hide this item on site" runat="server" />
                    </label>
                </div>

                <div class="cr">
                    <label>
                        <asp:CheckBox ID="DefaultItem" Text="Tick to set this accessory as one of the default accessories on the site" runat="server" />
                    </label>
                </div>--%>
                <div class="formActions">
                    <asp:Button ID="UpdateButton" CssClass="btn" OnClick="UpdateButton_Click" runat="server" Text="Update Partners" />
                    <a href="managePartner.aspx" class="CancelButton">Cancel</a>
                </div>
            </fieldset>
        </div>
    </div>
</asp:Content>

