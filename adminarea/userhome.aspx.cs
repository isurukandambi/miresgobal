﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using CMSConfigs;


public partial class userhome : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        
        Master.Page.Title = CMSSettings.SiteTitle + " - Edit Section - dashboard";

        if (Session["LoggedInName"] == null)
        {
            Response.Redirect("default.aspx?TimeOut=y");
        }
        
        if (!Page.IsPostBack)
        {
            //redirect user if they should not be on this page
            bool dashboardAllowed = false;
            List<string> SectionOnSite = (List<string>)Session["SectionOnSite"];
            foreach (string section in SectionOnSite)
            {
                if (section == "Dashboard")
                {
                    dashboardAllowed = true;
                }
            }
            if (dashboardAllowed)
            {
                Response.Redirect("home.aspx");
            }    
        }
    }
}
