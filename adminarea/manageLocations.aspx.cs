﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ORM;
using System.Linq;
using CMSConfigs;

public partial class manageLocations : System.Web.UI.Page
{
    private DatabaseEntities db;
    protected void Page_Load(object sender, EventArgs e)
    {
        Master.Page.Title = CMSSettings.SiteTitle + " - Edit Section - Manage Locations";

        if (Session["LoggedInName"] == null)
        {
            Response.Redirect("default.aspx?TimeOut=y&ReturnPage=" + Server.UrlEncode(HttpContext.Current.Request.Url.AbsoluteUri));
        }

        MsgToShow.Visible = false;
        if (Request.QueryString["MsgToShow"] != null)
        {
            int MsgType = Convert.ToInt16(Request.QueryString["MsgType"]);
            switch (MsgType)
            {
                case 1:
                    MsgToShow.Show(MyMessageBox.MessageType.Info, getAdvisorMessage("|"+Request.QueryString["MsgToShow"].ToString()));
                    break;
                case 2:
                    MsgToShow.Show(MyMessageBox.MessageType.Success, getAdvisorMessage("|" + Request.QueryString["MsgToShow"].ToString()));
                    break;
                case 3:
                    MsgToShow.Show(MyMessageBox.MessageType.Warning, getAdvisorMessage("|" + Request.QueryString["MsgToShow"].ToString()));
                    break;
                case 4:
                    MsgToShow.Show(MyMessageBox.MessageType.Error, getAdvisorMessage("|" + Request.QueryString["MsgToShow"].ToString()));
                    break;
                default:
                    goto case 1;
            }
            MsgToShow.Visible = true;
        }

        if (!Page.IsPostBack)
        {
            LoadCountries();
            LoadLocations();
        }
        /*
        if (Page.IsPostBack)
        {
            Response.Redirect("manageLocations.aspx?Row=" + Server.HtmlEncode(ddlCountries.SelectedValue));
        }
         */
    }

    private string getAdvisorMessage(string message)
    {
        var stripedMsg = "";
        var advisors = message.Split('|');
        foreach(string msg in advisors){
            if (!string.IsNullOrEmpty(msg))
            {
                stripedMsg += "<li>" + msg + "</li>";
            }
           
        }

        stripedMsg = "<ul style='list-style: none;'>" + stripedMsg + "</ul>";
        return stripedMsg;        
    }

    protected int rowID()
    {
        int rowID;
        bool result = int.TryParse(Request.QueryString["Row"], out rowID);
        if (result)
        {
            return rowID;
        }
        return 0;
    }
    protected void LoadCountries()
    {
        db = new DatabaseEntities();
        ddlCountries.DataSource = db.Countries.OrderBy(c => c.CountryName).ToList();
        ddlCountries.DataTextField = "CountryName";
        ddlCountries.DataValueField = "CountryID";
        ddlCountries.DataBind();
        ddlCountries.Items.Insert(0, new ListItem("Choose a Country", "0"));
        ddlCountries.SelectedValue = rowID().ToString();
    }

    protected void CountryDDL_Changed(object sender, EventArgs e)
    {
        Response.Redirect("manageLocations.aspx?Row=" + Convert.ToInt32(ddlCountries.SelectedValue));
    }
    protected void LoadLocations()
    {
        db = new DatabaseEntities();
        int countryID = rowID();
        var Location = (from l in db.Locations
                        orderby l.LocationName
                        where l.CountryIDFK == countryID
                        select new { l.LocationID, l.LocationName, l.LocationCode, l.ExtendedLocationCode });
        InformationTable.DataSource = Location.ToList();
        InformationTable.DataBind();
        if (Location.Count() > 0)
        {
            InformationTable.Visible = true;
            DeleteItems.Visible = true;
        }
        else
        {
            NoRecordsFound.Visible = true;
        }
    }
    protected void DeleteItems_Click(object sender, EventArgs e)
    {
        ConfirmBox.ShowConfirmBox("Delete Locations", "Are you sure you want to remove selected Locations from system?", "Delete|Cancel");
        ConfirmBox.Visible = true;
    }
    protected void Confirm_Fired(object sender, EventArgs e)
    {
        bool confirm = (bool)sender;
        if (confirm == true)
        {
            DeleteDatabaseItem(InformationTable);
        }
    }
    
    protected void DeleteDatabaseItem(Repeater RepeaterID)
    {
        //initialize the objectContext
        db = new DatabaseEntities();
        string message1 = "";

        string message = "";
        foreach (RepeaterItem item in RepeaterID.Items)
        {
            CheckBox cb = (CheckBox)item.FindControl("rowcheck");
            if (cb != null && cb.Checked)
            {
                Literal DBrow = (Literal)item.FindControl("rowID");
                int row = Convert.ToInt16(DBrow.Text);
                message += DeleteItem(row, db);
            }
        }
        if (!string.IsNullOrEmpty(message))
        {
            //error <ul>+message+</ul>
            Response.Redirect("manageLocations.aspx?MsgType=4&MsgToShow="+message);
        }
        else
        {
            //success
            Response.Redirect("manageLocations.aspx?MsgType=2&MsgToShow=Item(s) deleted successfully"+message1+".&Row=" + rowID());
        }
    }
    protected string DeleteItem(int locationID, DatabaseEntities dbCon)
    {

        var location = dbCon.Locations.SingleOrDefault(l => l.LocationID == locationID);
        if (location != null)
        {
            try
            {
                dbCon.Locations.Remove(location);
                dbCon.SaveChanges();
                return "";
            }
            catch
            {
                //dbCon.Locations.Detach(location);
                return "location.LocationName Already used. |";
            }

        }
        else
        {
            return "Location not Found. |";
        }

    }

    protected void GridView_MouseOver(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowIndex >= 0)
        {
            e.Row.Attributes.Add("onmouseover", "this.style.background='#FFFFFF'");
            e.Row.Attributes.Add("onmouseout", "this.style.background='#F7F7F7'");
        }
    }
}
