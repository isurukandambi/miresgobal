﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ORM;
using System.Linq;
using CMSConfigs;
using CMSUtils;


public partial class manageCarGroups : System.Web.UI.Page
{
    private DatabaseEntities db;
    protected void Page_Load(object sender, EventArgs e)
    {
        Master.Page.Title = CMSSettings.SiteTitle + " - Edit Section - Manage Car Groups";

        if (Session["LoggedInName"] == null)
        {
            Response.Redirect("default.aspx?TimeOut=y&ReturnPage=" + Server.UrlEncode(HttpContext.Current.Request.Url.AbsoluteUri));
        }
        if (Request.QueryString["DeleteRow"] != null)
        {

            int CountryID = Convert.ToInt16(Request.QueryString["DeleteRow"]);
            DeleteItem(CountryID);
        }
        MsgToShow.Visible = false;
        if (Request.QueryString["MsgToShow"] != null)
        {
            int MsgType = Convert.ToInt16(Request.QueryString["MsgType"]);
            switch (MsgType)
            {
                case 1:
                    MsgToShow.Show(MyMessageBox.MessageType.Info, Request.QueryString["MsgToShow"].ToString());
                    break;
                case 2:
                    MsgToShow.Show(MyMessageBox.MessageType.Success, Request.QueryString["MsgToShow"].ToString());
                    break;
                case 3:
                    MsgToShow.Show(MyMessageBox.MessageType.Warning, Request.QueryString["MsgToShow"].ToString());
                    break;
                case 4:
                    MsgToShow.Show(MyMessageBox.MessageType.Error, Request.QueryString["MsgToShow"].ToString());
                    break;
                default:
                    goto case 1;
            }
            MsgToShow.Visible = true;
        }
        LoadCarGroups();
    }
    protected void DeleteItem(int CarGroupID)
    {
        try
        {
            db = new DatabaseEntities();

            var CarGroupBrandMapping = db.CarGroupBrandMappings.SingleOrDefault(c => c.CarGroupIDFK == CarGroupID);
            var VehicleDetailsByBrands = db.VehicleDetailsByBrands.SingleOrDefault(c => c.CarGroupIDFK == CarGroupID);
            var CarGroup = db.CarGroups.SingleOrDefault(c => c.CarGroupID == CarGroupID);
            db.CarGroupBrandMappings.Remove(CarGroupBrandMapping);
            db.SaveChanges();
            db.VehicleDetailsByBrands.Remove(VehicleDetailsByBrands);
            db.SaveChanges();
            db.CarGroups.Remove(CarGroup);
            db.SaveChanges();
            Response.Redirect("manageCarGroups.aspx?MsgType=2&MsgToShow=CarGroup Deleted.");
        }
        catch
        {
            Response.Redirect("manageCarGroups.aspx?MsgType=4&MsgToShow=" + CMSUtilities.FKconstrainsFail());
            
        }
    }
    protected void LoadCarGroups()
    {
        db = new DatabaseEntities();
        GridView1.DataSource = (from c in db.CarGroups
                                orderby c.CarCode
                                select new { c.CarGroupID, c.CarCode }).ToList();
        GridView1.DataBind();
    }
    protected void GridView_MouseOver(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowIndex >= 0)
        {
            e.Row.Attributes.Add("onmouseover", "this.style.background='#FFFFFF'");
            e.Row.Attributes.Add("onmouseout", "this.style.background='#F7F7F7'");
        }
    }
}