﻿<%@ Page Language="C#" MasterPageFile="adminarea.master" AutoEventWireup="true" CodeFile="addAccessory.aspx.cs" Inherits="addAccessory" %>

<%@ Register Src="~/adminarea/controls/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="js/validationEngine/css/validationEngine.jquery.css" rel="stylesheet" type="text/css" />
    <script src="js/validationEngine/jquery.validationEngine.js" type="text/javascript"></script>
    <script src="js/validationEngine/jquery.validationEngine-en.js" type="text/javascript"></script>
    <script type="text/javascript" language="javascript">
        $(document).ready(function () {
            $("#form1").validationEngine();
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="content">
        <uc1:MyMessageBox ID="MsgToShow" runat="server" ShowCloseButton="true" Visible="false" />

        <h3>
            <asp:Literal ID="PageName" runat="server" Text="Add Accessory" /></h3>
        <div id="actionArea">
            <fieldset>
<%--                <div>
                    <label for="CountryID" class="inline">Country </label>
                    <asp:DropDownList ID="CountryID" runat="server"></asp:DropDownList>
                </div>

                <div>
                    <label for="LanguageID" class="inline">Language</label>
                    <asp:DropDownList ID="LanguageID" runat="server"></asp:DropDownList>
                </div>--%>

                <div>
                    <label for="Title" class="inline">Title</label>
                    <asp:TextBox ID="Title" CssClass="text validate[required]" runat="server" MaxLength="100"></asp:TextBox>
                </div>

                <div>
                    <label for="Price" class="inline">Price</label>
                    <asp:TextBox ID="Price" CssClass="text inline validate[required]" runat="server" MaxLength="255"></asp:TextBox>
                </div>

                <div>
                    <label for="HertzCode" class="inline">Hertz Code</label>
                    <asp:TextBox ID="HertzCode" CssClass="text inline validate[required]" runat="server" MaxLength="25"></asp:TextBox>
                </div>

<%--                <div>
                    <label for="LinkTitle" class="inline">'More Info' Link Title <span class="small">(optional)</span></label>
                    <asp:TextBox ID="LinkTitle" CssClass="text inline" runat="server" MaxLength="30"></asp:TextBox>
                </div>--%>
                <div>
                    <label for="LinkDetails" class="inline">'More Info' Link Details<br />
                        <span class="small">(required if Link Title contains a value)</span></label>
                    <asp:TextBox ID="LinkDetails" CssClass="text inline" runat="server" TextMode="MultiLine"></asp:TextBox>
                </div>
                <div class="shaded">
                    <asp:HiddenField runat="server" ID="OldImage" />
                    <label for="File1" class="inline">Accessory Image</label>
                    <input id="filPhoto" class="fileInput inline" type="file" runat="server" />
                    <span class="advisory fileAdv">File must be less than 3MB</span>
                    <br class="clearBoth">
                    <label class="inline" runat="server" id="CurrentImageText">Current Image File 1 Uploaded </label>
                    <span class="inlineContainer" runat="server" id="CurrentImageControlsHolder">
                        <asp:Literal ID="CurrentImage" runat="server" />
                    </span>
                </div>

                <div>
                    <label for="ThermeonCode" class="inline">Thermeon Code</label>
                    <asp:TextBox ID="ThermeonCode" CssClass="text inline validate[required]" runat="server" MaxLength="25"></asp:TextBox>
                </div>
<%--                <div class="cr">
                    <label>
                        <asp:CheckBox ID="VisibleOffer" Text="Tick to hide this item on site" runat="server" />
                    </label>
                </div>

                <div class="cr">
                    <label>
                        <asp:CheckBox ID="DefaultItem" Text="Tick to set this accessory as one of the default accessories on the site" runat="server" />
                    </label>
                </div>--%>
                <div class="formActions">
                    <asp:Button ID="AddButton" CssClass="btn" OnClick="AddButton_Click" runat="server" Text="Add Accessory" />
                    <a href="manageAccessories.aspx" class="CancelButton">Cancel</a>
                </div>
            </fieldset>
        </div>
    </div>
</asp:Content>

