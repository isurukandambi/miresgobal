﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ORM;
using System.Linq;
using CMSConfigs;

public partial class manageBrands : System.Web.UI.Page
{
    private DatabaseEntities db;
    protected void Page_Load(object sender, EventArgs e)
    {
        Master.Page.Title = CMSSettings.SiteTitle + " - Edit Section - Manage Logins";

        if (Session["LoggedInName"] == null)
        {
            Response.Redirect("default.aspx?TimeOut=y&ReturnPage=" + Server.UrlEncode(HttpContext.Current.Request.Url.AbsoluteUri));
        }
        if (!Page.IsPostBack)
        {
            DisplayActionMessage();
            PopulateTables();
        }
    }

    protected void DeleteItems_Click(object sender, EventArgs e)
    {
        ConfirmBox.ShowConfirmBox("Delete Brands", "Are you sure you want to remove selected Brands from system?", "Delete|Cancel");
        ConfirmBox.Visible = true;
    }
    protected void Confirm_Fired(object sender, EventArgs e)
    {
        bool confirm = (bool)sender;
        if (confirm == true)
        {
            DeleteDatabaseItem(InformationTable);
        }
    }


    protected void DeleteDatabaseItem(Repeater RepeaterID)
    {
        string message = "";
        //initialize the objectContext
        db = new DatabaseEntities();
        foreach (RepeaterItem item in RepeaterID.Items)
        {
            CheckBox cb = (CheckBox)item.FindControl("rowcheck");
            if (cb != null && cb.Checked)
            {
                Literal DBrow = (Literal)item.FindControl("rowID");
                int row = Convert.ToInt16(DBrow.Text);
                message += DeleteItem(row, db);
            }
        }
        if (string.IsNullOrEmpty(message))
        {
            Response.Redirect("manageBrands.aspx?MsgType=2&MsgToShow=Item(s) deleted successfully.");
        }
        else
        {
            Response.Redirect("manageBrands.aspx?MsgType=4&MsgToShow=" + message);
        }
    }
    protected string DeleteItem(int deleteItemID, DatabaseEntities dbCon)
    {
        string message = "";
        var item = dbCon.Brands.SingleOrDefault(c => c.BrandID == deleteItemID);
        try
        {
            dbCon.Brands.Remove(item);
            dbCon.SaveChanges();
        }
        catch
        {
            //dbCon.Brands.Detach(item);
            message = "Error occurred while deleting " + item.BrandName + ". Brand already in use. |";
        }
        return message;
    }

    protected void PopulateTables()
    {
        //initialize the objectContext
        db = new DatabaseEntities();

        var brands = (from b in db.Brands
                      select new { b.BrandID, b.BrandName, b.VendorCode, b.RateQualifier });

        InformationTable.DataSource = brands.ToList();
        InformationTable.DataBind();

        if (brands.Count() > 0)
        {
            InformationTable.Visible = true;
            DeleteItems.Visible = true;
        }
        else
        {
            NoRecordsFound.Visible = true;
        }
    }
    protected void DisplayActionMessage()
    {
        if ((Request.QueryString["MsgType"] != null) && (Request.QueryString["MsgToShow"] != null))
        {
            string msgToShow = Request.QueryString["MsgToShow"].ToString();
            int rowID;
            bool result = int.TryParse(Request.QueryString["MsgType"], out rowID);
            if ((result) && !String.IsNullOrEmpty(msgToShow))
            {
                switch (Convert.ToInt16(Request.QueryString["MsgType"]))
                {
                    case 1:
                        MsgToShow.Show(MyMessageBox.MessageType.Info, msgToShow);
                        break;
                    case 2:
                        MsgToShow.Show(MyMessageBox.MessageType.Success, msgToShow);
                        break;
                    case 3:
                        MsgToShow.Show(MyMessageBox.MessageType.Warning, msgToShow);
                        break;
                    case 4:
                        MsgToShow.Show(MyMessageBox.MessageType.Error, msgToShow.Replace("|", "</br>"));
                        break;
                    default:
                        goto case 1;
                }
                MsgToShow.Visible = true;
            }
        }
    }
}