﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ORM;
using System.Linq;
using System.IO;
using CMSConfigs;

public partial class manageThermeonVehicleDetails : System.Web.UI.Page
{
    private DatabaseEntities db;
    protected void Page_Load(object sender, EventArgs e)
    {
        Master.Page.Title = CMSSettings.SiteTitle + " - Edit Section - Manage Locations";

        if (Session["LoggedInName"] == null)
        {
            Response.Redirect("default.aspx?TimeOut=y&ReturnPage=" + Server.UrlEncode(HttpContext.Current.Request.Url.AbsoluteUri));
        }

        if (!Page.IsPostBack)
        {
            DisplayActionMessage();
            LoadCountries();
            LoadVehicalDetails();
            DirectoryInfo di = new DirectoryInfo(Request.PhysicalApplicationPath + "Pics\\Thermeon_CarImages\\");
            LoadGridView(di);
        }
    }

    protected int rowID()
    {
        int rowID;
        bool result = int.TryParse(Request.QueryString["Row"], out rowID);
        if (result)
        {
            return rowID;
        }
        return 0;
    }
    public string FolderSelected()
    {
        return "Thermeon_CarImages";
    }

    protected void FolderToFilterBy_OnSelectedIndexChanged(Object sender, EventArgs e)
    {
        DirectoryInfo di = new DirectoryInfo(Request.PhysicalApplicationPath + "Pics\\Thermeon_CarImages\\");
        LoadGridView(di);
    }
    public void RemoveFiles(string ImageToRemove)
    {
        string UpPath = Request.PhysicalApplicationPath + "Pics\\Thermeon_CarImages\\" + ImageToRemove;

        FileInfo OriginalFile = new FileInfo(UpPath);

        if (OriginalFile.Exists)
        {
            File.Delete(UpPath);
        }
    }

    protected void LoadCountries()
    {
        db = new DatabaseEntities();
        ddlCountries.DataSource = db.Countries.OrderBy(c => c.CountryName).ToList();
        ddlCountries.DataTextField = "CountryName";
        ddlCountries.DataValueField = "CountryID";
        ddlCountries.DataBind();
        ddlCountries.Items.Insert(0, new ListItem("Choose a Country", "0"));
        ddlCountries.SelectedValue = rowID().ToString();
    }

    protected void CountryDDL_Changed(object sender, EventArgs e)
    {
        Response.Redirect("manageThermeonVehicleDetails.aspx?Row=" + Convert.ToInt32(ddlCountries.SelectedValue));
    }

    protected void LoadVehicalDetails()
    {
        db = new DatabaseEntities();
        int countryID = rowID();
        var VehicleDetailsByBrands = (from m in db.Thermeon_CarMapping
                                      join v in db.Thermeon_CarDetails on m.ThermeonCarId equals v.TCId
                                      join c in db.Countries on m.CountryId equals c.CountryID
                                      join g in db.CarGroups on m.CarCode equals g.CarGroupID
                                      orderby m.CountryId
                                      where m.CountryId == countryID
                                      select new { m.TCMId, g.CarCode, v.Car_Description, v.CarImage_Path });
        InformationTable.DataSource = VehicleDetailsByBrands.ToList();
        InformationTable.DataBind();
        if (VehicleDetailsByBrands.Count() > 0)
        {
            InformationTable.Visible = true;
            DeleteItems.Visible = true;
        }
        else
        {
            NoRecordsFound.Visible = true;
        }
    }

    protected void LoadGridView(DirectoryInfo di)
    {
        db = new DatabaseEntities();
        int countryID = rowID();
        var VehicleDetailsByBrands = (from m in db.Thermeon_CarMapping
                                      join v in db.Thermeon_CarDetails on m.ThermeonCarId equals v.TCId
                                      join c in db.Countries on m.CountryId equals c.CountryID
                                      join g in db.CarGroups on m.CarCode equals g.CarGroupID
                                      orderby m.CountryId
                                      where m.CountryId == countryID
                                      select new { m.TCMId, g.CarCode, v.Car_Description, v.CarImage_Path });
        InformationTable.DataSource = VehicleDetailsByBrands.ToList();
        InformationTable.DataBind();
        if (VehicleDetailsByBrands.Count() > 0)
        {
            InformationTable.Visible = true;
            DeleteItems.Visible = true;
        }
        else
        {
            NoRecordsFound.Visible = true;
        }
    }

    protected void DeleteItems_Click(object sender, EventArgs e)
    {
        ConfirmBox.ShowConfirmBox("Delete Vehical Details", "Are you sure you want to remove selected Vehical Details from system?", "Delete|Cancel");
        ConfirmBox.Visible = true;
    }
    protected void Confirm_Fired(object sender, EventArgs e)
    {
        bool confirm = (bool)sender;
        if (confirm == true)
        {
            DeleteDatabaseItem(InformationTable);
        }
    }
    protected void DeleteDatabaseItem(Repeater RepeaterID)
    {
        string message = "";
        //initialize the objectContext
        db = new DatabaseEntities();
        foreach (RepeaterItem item in RepeaterID.Items)
        {
            CheckBox cb = (CheckBox)item.FindControl("rowcheck");
            if (cb != null && cb.Checked)
            {
                Literal DBrow = (Literal)item.FindControl("rowID");
                int row = Convert.ToInt16(DBrow.Text);
                message += DeleteItem(row, db);
            }
        }
        if (string.IsNullOrEmpty(message))
        {
            Response.Redirect("manageThermeonVehicleDetails.aspx?MsgType=2&MsgToShow=Item(s) deleted successfully.&Row=" + ddlCountries.SelectedValue);
        }
        else
        {
            Response.Redirect("manageThermeonVehicleDetails.aspx?MsgType=4&MsgToShow=" + message + "&Row=" + ddlCountries.SelectedValue);
        }
    }
    protected string DeleteItem(int deleteItemID, DatabaseEntities dbCon)
    {
        string message = "";
        var item = dbCon.Thermeon_CarDetails.SingleOrDefault(l => l.TCId == deleteItemID);
        try
        {
            var VehicleResource = db.Thermeon_CarMapping.SingleOrDefault(l => l.ThermeonCarId == deleteItemID);
            db.Thermeon_CarMapping.Remove(VehicleResource);
            dbCon.Thermeon_CarDetails.Remove(item);
            dbCon.SaveChanges();
        }
        catch
        {
            // dbCon.VehicleDetailsByBrands.Detach(item);
            message = "Error occurred while deleting " + item.Car_Description + ". Vehicle Details already in use. |";
        }
        if (string.IsNullOrEmpty(message))
        {
            RemoveFiles(item.CarImage_Path);
        }
        return message;
    }
    protected void GridView_MouseOver(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowIndex >= 0)
        {
            e.Row.Attributes.Add("onmouseover", "this.style.background='#FFFFFF'");
            e.Row.Attributes.Add("onmouseout", "this.style.background='#F7F7F7'");
        }
    }

    protected void DisplayActionMessage()
    {
        if ((Request.QueryString["MsgType"] != null) && (Request.QueryString["MsgToShow"] != null))
        {
            string msgToShow = Request.QueryString["MsgToShow"].ToString();
            int rowID;
            bool result = int.TryParse(Request.QueryString["MsgType"], out rowID);
            if ((result) && !String.IsNullOrEmpty(msgToShow))
            {
                switch (Convert.ToInt16(Request.QueryString["MsgType"]))
                {
                    case 1:
                        MsgToShow.Show(MyMessageBox.MessageType.Info, msgToShow.Replace("|", "</br>"));
                        break;
                    case 2:
                        MsgToShow.Show(MyMessageBox.MessageType.Success, msgToShow);
                        break;
                    case 3:
                        MsgToShow.Show(MyMessageBox.MessageType.Warning, msgToShow.Replace("|", "</br>"));
                        break;
                    case 4:
                        MsgToShow.Show(MyMessageBox.MessageType.Error, msgToShow.Replace("|", "</br>"));
                        break;
                    default:
                        goto case 1;
                }
                MsgToShow.Visible = true;
            }
        }
    }
}