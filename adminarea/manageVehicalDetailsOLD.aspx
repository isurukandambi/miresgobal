﻿<%@ Page Language="C#" MasterPageFile="adminarea.master" AutoEventWireup="true" CodeFile="manageVehicalDetailsOLD.aspx.cs" Inherits="manageLocations" %>

<%@ Register Src="~/adminarea/controls/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="js/imagepreviewer.js" type="text/javascript"></script>
    <style type="text/css">
        table.narrowTable {
            width: auto;
        }

        label.inline {
            width: 99px;
            margin: 0.2em 0.5em 0.5em 0;
            float: left;
            line-height: 2em;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" />
    <script type="text/javascript" language="javascript">
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
        function EndRequestHandler(sender, args) {
            $(document).ready(function () {
                //used for context menu on grids
                $('.device-actions-toggler').show();
                $('.device-actions-toggler').click(function () { return false; });
                $('.device-actions-list').css({ position: 'absolute', top: '0', right: '0' }).hide();

                $('.device-actions').css({ position: 'relative' }).each(function () {
                    $(this).hoverIntent(function () {
                        $(this).find('.device-actions-list').fadeIn('fast');
                    }, function () {
                        $(this).find('.device-actions-list').fadeOut('fast');
                    });
                });
                if (<%=GridView1.Rows.Count%> != 0){
                    $('#<%=GridView1.ClientID%> a').imgPreview({
                        containerID: 'imgPreviewWithStyles'
                    });
                }

            });

        }
    </script>
    <div id="content">
        <h2>Manage Vehical Details</h2>
        <div class="buttons">
            <a href="addVehicalDetails.aspx">Add Vehical Details</a>
        </div>

        <uc1:MyMessageBox ID="MsgToShow" runat="server" ShowCloseButton="true" />

        <h3>Existing Vehical Details:</h3>
        <div id="actionArea">
            <fieldset>
                <div>
                    <label for="ddlCountries" class="inline">Filter By Country</label>
                    <asp:DropDownList ID="ddlCountries" runat="server" AutoPostBack="true" OnSelectedIndexChanged="CountryDDL_Changed" AppendDataBoundItems="true">
                    </asp:DropDownList>
                </div>
            </fieldset>
            <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="GridViewPanel">
                <ProgressTemplate>
                    <span class="UpdatingPanel">
                        <img alt="loading indicator" src="images/indicator.gif" />
                        Updating... Please Wait...</span>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <asp:UpdatePanel ID="GridViewPanel" runat="server">
                <ContentTemplate>
                    <asp:GridView
                        ID="GridView1" OnRowCreated="GridView_MouseOver" runat="server"
                        CssClass="gridview gridview-classic narrowTable" AutoGenerateColumns="false"
                        DataKeyNames="VDBID"
                        AllowSorting="True">
                        <EmptyDataTemplate>There are no locations in the database or Country not selected from drop down.</EmptyDataTemplate>
                        <Columns>
                            <asp:TemplateField HeaderText="Car Code" SortExpression="CarCode">
                                <ItemTemplate>
                                    <a href="updateVehicalDetails.aspx?Row=<%# Eval("VDBID") %>"><%# Eval("CarCode")%></a>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Car Description" >
                                <ItemTemplate>
                                    </a><%# Eval("CarDescription")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Car Image Path">
                                <ItemTemplate>
                                    <a href='../Pics/<%# FolderSelected()%>/<%# Eval("CarImagePath") %>' rel="nofollow" class="imagePreview"><asp:Literal ID="filenameWithoutLink" runat="server" Text='<%# Eval("CarImagePath") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="BrandName">
                                <ItemTemplate>
                                    <%# Eval("BrandName")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Action">
                                <ItemStyle HorizontalAlign="Right" Width="50" />
                                <ItemTemplate>
                                    <div class="device-actions">
                                        <a href="#" class="device-actions-toggler">
                                            <img src="images/cog.png" alt="Actions" border="0" />
                                        </a>
                                        <div class="device-actions-list" style="display: none">
                                            <ul>
                                                <li><a href="updateVehicalDetails.aspx?Row=<%# Eval("VDBID") %>">Update Vehical Details</a></li>
                                                <li>
                                                    <asp:LinkButton ID="DeleteItemLnk" runat="server">Delete Vehical Details</asp:LinkButton></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <cc1:ModalPopupExtender ID="mdlPopup" runat="server"
                                        TargetControlID="DeleteItemLnk"
                                        PopupControlID="DeleteItemPnl"
                                        BackgroundCssClass="modalBackground"
                                        CancelControlID="CancelActionLnk" />
                                    <asp:Panel ID="DeleteItemPnl" runat="server" CssClass="modalPanalWithButtons" Style="display: none">
                                        <div align="center" style="margin-top: 13px;">
                                            <p>
                                                <img src="images/exclamation.png" alt="warning">Are you sure you want to remove this location?</p>
                                            <div class="buttons">
                                                <a href="manageVehicalDetails.aspx?DeleteRow=<%# Eval("VDBID") %>">Delete</a>
                                                <asp:LinkButton ID="CancelActionLnk" runat="server">Cancel</asp:LinkButton>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </ContentTemplate>
            </asp:UpdatePanel>
            <asp:UpdateProgress ID="UpdateProgress2" runat="server" AssociatedUpdatePanelID="GridViewPanel">
                <ProgressTemplate>
                    <span class="UpdatingPanel">
                        <img src="images/indicator.gif" />
                        Updating... Please Wait...</span>
                </ProgressTemplate>
            </asp:UpdateProgress>
        </div>
    </div>
</asp:Content>

