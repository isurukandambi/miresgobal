﻿<%@ Page Language="C#" MasterPageFile="adminarea.master" AutoEventWireup="true" CodeFile="manageBrandMapping.aspx.cs" Inherits="manageBrandMapping" %>
<%@ Register Src="~/adminarea/controls/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>
<%@ Register Src="~/adminarea/Controls/ConfirmBox.ascx" TagName="ConfirmBox" TagPrefix="cb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
     <style type="text/css">
        table.narrowTable {
            width: auto;
        }

        label.inline {
            width: 99px;
            margin: 0.2em 0.5em 0.5em 0;
            float: left;
            line-height: 2em;
        }
    </style>
    <script src="js/dataTables/jquery.dataTables.min.js" type="text/javascript"></script>
    <script type="text/javascript" language="javascript">
        $(document).ready(function () {
            //grid 1 settings
            $('#dataGrid').dataTable({
                "sDom": '<"dataTable_header"lf>rt<"dataTable_footer"ip>',
                "bAutoWidth": false,
                "sPaginationType": "full_numbers",
                "iDisplayLength": 50,
                "bStateSave": true,
                "iCookieDuration": 300,
                //customizing columns
                "aaSorting": [[1, "asc"]],
                "aoColumns": [
        			{ "bSearchable": false, "bVisible": false }, //setting first column to be hidden from searching and the user - required for knowing what database row to remove
                    { "sType": "html" }, //setting second column to search word of html link
                    null,
                    null,
                    null,
                    null,
        			{ "bSortable": false } //setting last column not to sort onClick
                ]

            });
            //check all checkboxes if image clicked on
            $('#headercheck').click(function (evt) {
                evt.preventDefault();
                $('#dataGrid :checkbox').attr('checked', function () {
                    return !this.checked;
                });
            });

            //don't fire the delete button if no checkbox selected
            $('[id$=DeleteItems]').click(function (evt) {
                if ($("#dataGrid :checked").size() == 0) {
                    evt.preventDefault();
                }
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<div id="content">
    <h2><asp:Literal ID="PageName" runat="server" Text="Manage Locations by Brand" /></h2>
    <div class="buttons">
        <a href="addBrandMapping.aspx?Country=0">Add Locations for Brand</a> 
        <a href="manageBrandCountriesMapping.aspx">Brand Country Mapping</a>
    </div>
    
    <uc1:MyMessageBox ID="MsgToShow" runat="server" ShowCloseButton="true" Visible="false" />

    <h3>Existing Mapped Locations by Brands:</h3>  
    <div id="actionArea">
        <fieldset>
                <div>
                    <label for="ddlBrands" class="inline">Filter By Partners</label>
                    <asp:DropDownList ID="ddlPartners" runat="server" AutoPostBack="true" OnSelectedIndexChanged="PartnerDDL_Changed" AppendDataBoundItems="true">
                    </asp:DropDownList>
                </div>
                <div>
                    <label for="ddlCountries" class="inline">Filter By Country</label>
                    <asp:DropDownList ID="ddlCountries" runat="server" AutoPostBack="true" OnSelectedIndexChanged="CountryDDL_Changed" AppendDataBoundItems="true">
                    </asp:DropDownList>
                </div>
  
                <div>
                    <label for="ddlBrands" class="inline">Filter By Brands</label>
                    <asp:DropDownList ID="ddlBrands" runat="server" AutoPostBack="true" OnSelectedIndexChanged="BrandDDL_Changed" AppendDataBoundItems="true">
                    </asp:DropDownList>
                </div>
            </fieldset>
        <asp:Literal ID="NoRecordsFound" runat="server" Text="<p>There are no mapped locations in the database or Country not selected from drop down.</p>" Visible="false" />
        <asp:Repeater ID="InformationTable" runat="server" Visible="false" OnItemDataBound="InformationTable_RowDataBound">
            <HeaderTemplate>
                <table cellpadding="0" cellspacing="0" border="0" id="dataGrid">
                    <thead>
                        <tr>
                            <th>RowID</th>
                            <th class="cnrCell">Location</th>
                            <th class="cnrCell">Partner</th>
                            <th class="cnrCell">Brand</th>
                            <th class="cnrCell">Vehicle Limit</th>
                            <th class="cnrCell">Car groups</th>
                            <th><a href="#" id="headercheck"><img src="images/icons/delete.png" width="15" height="15" alt="delete icon" /></a></th>
                        </tr>
                    </thead>
                    <tbody>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td><asp:HiddenField ID="LocationID" runat="server" Value='<%# Eval("LocationID")  %>' /> <asp:Literal ID="rowID" runat="server" Text='<%# Eval("BrandLocationID")  %>' /></td>
                    <td><asp:Label runat="server" ID="LocationLink"><a href="updateBrandMapping.aspx?Row=<%# Eval("BrandLocationID") %>"><%# Eval("LocationName")%> </a></asp:Label></td>
                    <td class="cnrCell"><%# Eval("DomainName")%></td>
                    <td class="cnrCell"><%# Eval("BrandName")%></td>
                    <td class="cnrCell"><%# Eval("VehicleLimit")%></td>
                    <td class="cnrCell"><asp:Literal ID="CarCodes" runat="server" /></td>
                    <td class="cnrCell"><asp:CheckBox ID="rowcheck" runat="server" /></td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </tbody> </table>
            </FooterTemplate>
        </asp:Repeater>
        <fieldset>
            <div class="formActions">
                <asp:Button ID="DeleteItems" CssClass="btn" runat="server" Text="Delete Selected Brand Mappings" OnClick="DeleteItems_Click" Visible="false" />
            </div>
        </fieldset>
    </div>
    <cb:ConfirmBox ID="ConfirmBox" runat="server" Visible="false" OnFireConfirm="Confirm_Fired" />
</div>    
</asp:Content>
