﻿using System;
using System.Linq;
using System.Text;
using System.Web;
using ORM;
using CMSConfigs;

public partial class addLocation : System.Web.UI.Page
{
    private DatabaseEntities db;

    protected void Page_Load(object sender, EventArgs e)
    {
        Master.Page.Title = CMSSettings.SiteTitle + " - " + PageName.Text;
        if (Session["LoggedInName"] == null)
        {
            Response.Redirect("default.aspx?TimeOut=y&ReturnPage=" + Server.UrlEncode(HttpContext.Current.Request.Url.AbsoluteUri));
        }
        if(!Page.IsPostBack){
            LoadCountries();
            MainView.ActiveViewIndex = 0;
        }
    }

    protected void LoadCountries() {
        db = new DatabaseEntities();
        CountryID.DataSource = db.Countries.OrderBy(c => c.CountryName).ToList();
        CountryID.DataValueField = "CountryID";
        CountryID.DataTextField = "CountryName";
        CountryID.DataBind();
        CountryID.SelectedValue = countryID().ToString();
    }

    protected int countryID()
    {
        int rowID;
        bool result = int.TryParse(Request.QueryString["Country"], out rowID);
        if (result)
        {
            return rowID;
        }
        return 0;
    }

    protected void Tab1_Click(object sender, EventArgs e)
    {
        MainView.ActiveViewIndex = 0;
    }

    protected void Tab2_Click(object sender, EventArgs e)
    {
        MainView.ActiveViewIndex = 1;
    }

    protected void Tab3_Click(object sender, EventArgs e)
    {
        MainView.ActiveViewIndex = 2;
    }
    protected void Tab4_Click(object sender, EventArgs e)
    {
        MainView.ActiveViewIndex = 3;
    }
    protected void Tab5_Click(object sender, EventArgs e)
    {
        MainView.ActiveViewIndex = 4;
    }

    protected void AddButton_Click(object sender, EventArgs e)
    {
        try
        {
            // initialize the objectContext
            db = new DatabaseEntities();

            if (LocationName.Text.Trim() == string.Empty || LocationCode.Text.Trim() == string.Empty || ExtendedLocationCode.Text.Trim() == string.Empty || Latitude.Text.Trim() == string.Empty || Longitude.Text.Trim() == string.Empty || City.Text.Trim() == string.Empty || Address1.Text.Trim() == string.Empty || CarTrawlerLocCode.Text.Trim() == string.Empty || ThermeonLocCode.Text.Trim() == string.Empty)
            {
                LocationName.CssClass = "text inline validate[required]";
                LocationCode.CssClass = "text inline validate[required]";
                ExtendedLocationCode.CssClass = "text inline validate[required]";
                Latitude.CssClass = "text inline validate[required]";
                Longitude.CssClass = "text inline validate[required]";
                City.CssClass = "text inline validate[required]";
                Address1.CssClass = "text inline validate[required]";
                CarTrawlerLocCode.CssClass = "text inline validate[required]";
                ThermeonLocCode.CssClass = "text inline validate[required]";

                MsgToShow.Show(MyMessageBox.MessageType.Warning, "Fill the Mandetory Fields.");
                MsgToShow.Visible = true;
            }
            else
            {
                if (!db.Locations.Where(l => (l.LocationCode == LocationCode.Text && l.ExtendedLocationCode == ExtendedLocationCode.Text) || l.LocationCode == LocationCode.Text).Any())
                {
                    int selectedCountryID = Convert.ToInt32(CountryID.SelectedValue);
                    int thrifty = 0;
                    int hertz = 0;

                    if (Thrifty.Checked)
                    {
                        thrifty = 1;
                    }
                    else if (Hertz.Checked)
                    {
                        hertz = 1;
                    }

                    Location location = new Location
                    {
                        LocationName = LocationName.Text.Trim(),
                        LocationCode = LocationCode.Text.Trim(),
                        CountryIDFK = selectedCountryID,
                        HertzAdvantage = HertzAdvantage.Checked,
                        LocationType = LocationType.SelectedValue,
                        ExtendedLocationCode = ExtendedLocationCode.Text.Trim(),
                        City = City.Text.Trim(),
                        Address_Line1 = Address1.Text.Trim(),
                        Address_Line2 = Address2.Text.Trim(),
                        Address_Line3 = Address3.Text.Trim(),
                        Zip_PostCode = PostCode.Text.Trim(),
                        Phone = Phone.Text.Trim(),
                        Latitude = Latitude.Text.Trim(),
                        Longitude = Longitude.Text.Trim(),
                        Open1Mon = Open1Mon.Text.Trim(),
                        Close1Mon = Close1Mon.Text.Trim(),
                        Open2Mon = Open2Mon.Text.Trim(),
                        Close2Mon = Close2Mon.Text.Trim(),
                        Open3Mon = Open3Mon.Text.Trim(),
                        Close3Mon = Close3Mon.Text.Trim(),
                        Open1Tue = Open1Tue.Text.Trim(),
                        Close1Tue = Close1Tue.Text.Trim(),
                        Open2Tue = Open2Tue.Text.Trim(),
                        Close2Tue = Close2Tue.Text.Trim(),
                        Open3Tue = Open3Tue.Text.Trim(),
                        Close3Tue = Close3Tue.Text.Trim(),
                        Open1Wed = Open1Wed.Text.Trim(),
                        Close1Wed = Close1Wed.Text.Trim(),
                        Open2Wed = Open2Wed.Text.Trim(),
                        Close2Wed = Close2Wed.Text.Trim(),
                        Open3Wed = Open3Wed.Text.Trim(),
                        Close3Wed = Close3Wed.Text.Trim(),
                        Open1Thu = Open1Thu.Text.Trim(),
                        Close1Thu = Close1Thu.Text.Trim(),
                        Open2Thu = Open2Thu.Text.Trim(),
                        Close2Thu = Close2Thu.Text.Trim(),
                        Open3Thu = Open3Thu.Text.Trim(),
                        Close3Thu = Close3Thu.Text.Trim(),
                        Open1Fri = Open1Fri.Text.Trim(),
                        Close1Fri = Close1Fri.Text.Trim(),
                        Open2Fri = Open2Fri.Text.Trim(),
                        Close2Fri = Close2Fri.Text.Trim(),
                        Open3Fri = Open3Fri.Text.Trim(),
                        Close3Fri = Close3Fri.Text.Trim(),
                        Open1Sat = Open1Sat.Text.Trim(),
                        Close1Sat = Close1Sat.Text.Trim(),
                        Open2Sat = Open2Sat.Text.Trim(),
                        Close2Sat = Close2Sat.Text.Trim(),
                        Open3Sat = Open3Sat.Text.Trim(),
                        Close3Sat = Close3Sat.Text.Trim(),
                        Open1Sun = Open1Sun.Text.Trim(),
                        Close1Sun = Close1Sun.Text.Trim(),
                        Open2Sun = Open2Sun.Text.Trim(),
                        Close2Sun = Close2Sun.Text.Trim(),
                        Open3Sun = Open3Sun.Text.Trim(),
                        Close3Sun = Close3Sun.Text.Trim(),
                        CTLocationId = Convert.ToInt32(CarTrawlerLocCode.Text.Trim()),
                        ThermeonLocation = ThermeonLocCode.Text,
                        Country = CountryID.SelectedItem.ToString(),
                        ThriftyLocation = thrifty,
                        HertzLocation = hertz,
                        PREFERRED_OAG = Preferred_OAG.Text.Trim(),
                        STATEPROVINCE_CODE = StateProviceCode.Text.Trim(),
                        ISO_STATEPROVINCE_CODE = IsoStateCode.Text.Trim(),
                        NUMBER = Number.Text.Trim(),
                        ALT_PHONE = AltPhone.Text.Trim(),
                        FAX = Fax.Text.Trim(),
                        TELEX = Telex.Text.Trim(),
                        SERVED_BY = Served_By.Text.Trim(),
                        EMAIL_ADDRESS = Email_Add.Text.Trim(),
                        WEBSITE = WebSite.Text.Trim(),
                        BOOKABLE = Bookable.Text.Trim(),
                        OPT_HLE = Opt_HLE.Text.Trim(),
                        OPT_INSURANCE_REPLACE = Opt_Insurance_Replace.Text.Trim(),
                        OPT_NEVERLOST = Opt_Neverlost.Text.Trim(),
                        OPT_CHILD_SEATS = Opt_Child_Seats.Text.Trim(),
                        OPT_HAND_CONTROL = Opt_Hand_Control.Text.Trim(),
                        OPT_GOLD_CUSTOMER = Opt_Gold_Customer.Text.Trim(),
                        OPT_GOLD_CANOPY = Opt_Gold_Canopy.Text.Trim(),
                        OPT_PORTABLE_PHONES = Opt_Portable_Phones.Text.Trim(),
                        OPT_SKIERIZED = Opt_Skierized.Text.Trim(),
                        OPT_MOBILE_PHONES = Opt_Mobile_Phones.Text.Trim(),
                        OPT_LUGGAGE_RACK = Opt_Luggqage_Rack.Text.Trim(),
                        OPT_PRESTIGE_SERVICE = Opt_Prestige_Services.Text.Trim(),
                        OPT_RETURN_CENTER = Opt_Return_Center.Text.Trim(),
                        OPT_HTV = Opt_HTV.Text.Trim(),
                        OPT_HEAVY_TRUCKS = Opt_Heavy_Trucks.Text.Trim(),
                        OPT_PICKUP_SERVICE = Opt_Pickup_Services.Text.Trim(),
                        MAJOR_AIRPORT = Major_AirPort.Text.Trim(),
                        SERVED_BY_DATA = Served_BY_Data.Text.Trim(),
                        ADDRESS4 = Address4.Text.Trim(),
                        ADDRESS5 = Address5.Text.Trim(),
                        CITY_ADDR = CityAdd.Text.Trim(),
                        NOTES1 = Notes_1.Text.Trim(),
                        NOTES2 = Notes_2.Text.Trim(),
                        NOTES3 = Notes_3.Text.Trim(),
                        NOTES4 = Notes_4.Text.Trim(),
                        NOTES5 = Notes_5.Text.Trim(),
                        CAPITAL = Capital.Text.Trim(),
                        STATEPROVINCE_NAME = StateProvineName.Text.Trim(),
                        CITY_VARIANTS = City_Variants.Text.Trim(),
                        DISPLAY_ADDR = Display_Add.Text.Trim(),
                        DISPLAY_DL = Display_Dl.Text.Trim(),
                        DISPLAY_DEST = Display_Dest.Text.Trim(),
                        AREA = Area.Text.Trim(),
                        LOCATOR_CODE = Locator_Code.Text.Trim(),
                        TOP_LOCATION = Top_Location.Text.Trim(),
                        FBO = Fbo.Text.Trim(),
                        SPECIAL_INSTRUCTION = Spec_Inst.Text.Trim(),
                        SPECIAL_INSTRUCTION_DATA = Spec_Inst_Data.Text.Trim(),
                        DOUBLE_CLICK_BANNER = DoubleClickBanner.Text.Trim(),
                        HZ_LOCID = HZ_LocId.Text.Trim(),
                        COUNTER_BYPASS_ENABLED = Counter_Bypass_Enable.Text.Trim(),
                        OPT_WIFI = Opt_Wifi.Text.Trim(),
                        OPT_PLATE_PASS = Opt_Plate_Pass.Text.Trim(),
                        OPT_EXPRESS_RETURN = Opt_Express_Return.Text.Trim(),
                        OPT_NO_WALKINS = Opt_No_Walkings.Text.Trim(),
                        OPT_AFTER_HOURS_DROP = Opt_After_Hours_Drop.Text.Trim(),
                        OPT_AFTER_HOURS_PICKUP = Opt_After_Hours_Pickup.Text.Trim(),
                        OPT_INFANT_SEAT = Opt_Infant_Seat.Text.Trim(),
                        OPT_BOOSTER_SEATS = Opt_Boosters_Seats.Text.Trim(),
                        OPT_MILITARY_LOC = Opt_Military_Loc.Text.Trim(),
                        OPT_HOTEL_GUEST_REQ = Opt_Hotel_Guest_Req.Text.Trim(),
                        OPT_LEASING_LOC = Opt_Leasing_Loc.Text.Trim(),
                        OPT_RE_FUEL = Opt_RE_Fuel.Text.Trim(),
                        OPT_ROADSIDE_ASSSIST = Opt_Roadside_Assist.Text.Trim(),
                        OPT_KIOSK_LOC = Opt_Kiosk_Loc.Text.Trim(),
                        OPT_CHAFFEUR_LOC = Opt_Chaffeur_Loc.Text.Trim(),
                        OPT_TRAIN_LOC = Opt_Train_Loc.Text.Trim(),
                        OPT_SHIP_LOC = Opt_Ship_Loc.Text.Trim(),
                        OPT_CRUISE_LOC = Opt_Cruise_Loc.Text.Trim(),
                        OPT_BUS_LOC = Opt_Bus_Loc.Text.Trim(),
                        OPT_SUBWAY_LOC = Opt_Subway_Loc.Text.Trim(),
                        OPT_RET_SERV_LOC = Opt_Ret_Serv_Loc.Text.Trim(),
                        OPT_MEET_PF = Opt_Meet_Pf.Text.Trim(),
                        OPT_MEET_ADV_RES = Opt_Meet_Acv_Res.Text.Trim(),
                        OPT_FLIGHT_MAND_ALL = Opt_Flight_Mand_All.Text.Trim(),
                        OPT_FLIGHT_MAND_PRIVATE = Opt_Flight_Mand_Private.Text.Trim(),
                        OPT_HOD = Opt_HOD.Text.Trim(),
                        OPT_GOLD_CHOICE = Opt_Gold_Choice.Text.Trim(),
                        OPT_HERTZ_EQUIP = Opt_Hertz_Equip.Text.Trim(),
                        OPT_PRIVATE_FLIGHT_ONLY = Opt_Private_Flight_Only.Text.Trim(),
                        DISPLAY_HLE_TEXT = Display_Hle_Txt.Text.Trim(),
                        OPT_MGB = Opt_MGB.Text.Trim(),
                        OPT_RETURN_TO_SERVICING_LOC = Opt_Return_To_Servicing_Loc.Text.Trim(),
                        REFER_TO_QUAL_REQ = Refer_To_Qual_Req.Text.Trim(),
                        HOURS_OF_OPERATION_2 = HOURS_OF_OPERATION_2.Text.Trim(),
                        HOURS_OF_OPERATION_3 = HOURS_OF_OPERATION_3.Text.Trim(),
                        VRAC_VEHICLE_UPGRADE = Vrac_Vehicle_Upgrade.Text.Trim(),
                        HOD_LOCID = Hod_LocId.Text.Trim(),
                        OPT_INS_REPLACE_ONLY = Opt_Ins_Replace_Only.Text.Trim(),
                        HOD_IMAGE = Hod_Image.Text.Trim(),
                        OPT_GOLD_ANYTIME = Opt_Gold_Anytime.Text.Trim(),
                        HOURS_OF_OPERATION = HOURS_OF_OPERATION.Text.Trim()
                    };
                    db.Locations.Add(location);
                    db.SaveChanges();

                    Response.Redirect("manageLocations.aspx?MsgType=2&MsgToShow='" + LocationName.Text + "' added successfully&Row=" + selectedCountryID);

                }
                else
                {
                    MsgToShow.Show(MyMessageBox.MessageType.Warning, "That Location Code already exists. Please use different Location Code.");
                    MsgToShow.Visible = true;
                }
            }

        }
        catch
        {
            MsgToShow.Show(MyMessageBox.MessageType.Error, "There was an error adding this information. Please try again.");
            MsgToShow.Visible = true;
        }
    }
}