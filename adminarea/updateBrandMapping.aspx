﻿<%@ Page Language="C#" MasterPageFile="adminarea.master" AutoEventWireup="true" CodeFile="updateBrandMapping.aspx.cs" Inherits="updateBrandMapping" %>
<%@ Register Src="~/adminarea/controls/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <link href="js/validationEngine/css/validationEngine.jquery.css" rel="stylesheet" type="text/css" />
    <script src="js/validationEngine/jquery.validationEngine.js" type="text/javascript"></script>
    <script src="js/validationEngine/jquery.validationEngine-en.js" type="text/javascript"></script>
    <script type="text/javascript" language="javascript">
        $(document).ready(function () {
            $("#form1").validationEngine();
            $('.toggleAllCarGroups').change(function (evt) {
                $(this).parent().next().find(":checkbox").attr('checked', function () {
                    return !this.checked;
                });
            });
        });
    </script>
    <style type="text/css">
        .brandsHolder {
            width: 99%;
        }

        .brandsHolder select {
            margin: 15px 6%;
            width: 88%;
        }

        .VehicleLimitddl {
            width: 145px;
        }

        .brandName {
            font-weight: bold;
            width: 200px;
        }
        table{
            width : 350px;
        }
        .brandsHolder table {
            width : 88%;
            margin : 5px 6%;
        }
        .BrandHolderControlPanel {
            width: 26%;
            float: left;
            border: 1px #8bafc1 solid;
            margin: 2px 2%;
            border-radius: 5px;
        }
        .BrandIndicatorCheckBox {
            margin-left: 5%;
            color: darkgray;
            font-size: 20px;
        }
        .BrandHolderControlPanel:first-child {
            margin-left: 0px;
         }
        .checkboxLists{
            max-height:150px;  
            border:1px solid aliceblue;  
            overflow-y:scroll; 
        }
        .checkboxListsInBrandPanel{
            max-height:150px;  
            border:1px solid aliceblue;  
            overflow-y:scroll; 
            width: 87%;
            margin: 5px 6%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div id="content">
    <uc1:MyMessageBox ID="MsgToShow" runat="server" ShowCloseButton="true" Visible="false" />

    <h3><asp:Literal ID="PageName" runat="server" Text="Update Locations for Brand" /></h3>
    <div id="actionArea">
    <fieldset>
        <div>
            <label for="PartnerName" class="inline" style="font-size: 18px; margin-top: 2px;"><asp:Literal ID="PartnerName" runat="server"></asp:Literal></label>
        </div>

        <div>
            <label for="LocationName" class="inline" style="font-size: 18px; margin-top: 2px;"><asp:Literal ID="CountryName" runat="server"></asp:Literal></label>
        </div>
        <div>
            <label for="LocationName" class="inline" style="font-size: 18px; margin-top: 2px; margin-bottom: 0px;"><asp:Literal ID="LocationName" runat="server"></asp:Literal></label>
        </div>
        <div>
            <label for="VehicleLimitAndBrands" class="inline" style="width: 100%; margin-top: 0px;" runat="server" id="AdvisorText"></label>
            <asp:Panel ID="BrandsPanel" runat="server" CssClass="brandsHolder">
            </asp:Panel>
        </div>
        <div class="formActions">
            <asp:Button ID="UpdateButton" CssClass="btn" OnClick="UpdateButton_Click" runat="server" Text="Update Mapped Location" />
            <a href="<%=Request.UrlReferrer.ToString()%>" class="CancelButton">Cancel</a>
        </div>
    </fieldset>
    </div>
</div>
</asp:Content>

