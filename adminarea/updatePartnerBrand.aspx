﻿<%@ Page Language="C#" MasterPageFile="adminarea.master" AutoEventWireup="true" CodeFile="updatePartnerBrand.aspx.cs" Inherits="updateCountry" %>
<%@ Register Src="~/adminarea/controls/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <link href="../Plugins/validationEngine/css/validationEngine.jquery.css" rel="stylesheet" type="text/css" />
    <script src="../Plugins/validationEngine/jquery.validationEngine.js" type="text/javascript"></script>
    <script src="../Plugins/validationEngine/jquery.validationEngine-en.js" type="text/javascript"></script>
    <script type="text/javascript" language="javascript">
        $(document).ready(function () {            
            $("#form1").validationEngine();
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div id="content">
    <uc1:MyMessageBox ID="MsgToShow" runat="server" ShowCloseButton="true" Visible="false" />

    <h3><asp:Literal ID="PageName" runat="server" Text="Update Partner Brand" /></h3>
    <div id="actionArea">
    <fieldset>
        <div>
            <label for="PartnerName" class="inline">Partner Name </label>
            <asp:DropDownList ID="PartnerID" runat="server"> </asp:DropDownList>
        </div>
        <div>
            <label for="BrandName" class="inline">Brand Name </label>
            <asp:DropDownList ID="BrandID" runat="server"></asp:DropDownList>
        </div>

        <div>
            <label for="VendorCode" class="inline">Vendor Code </label>
            <asp:TextBox ID="VendorCode" CssClass="text inline validate[required]" runat="server" MaxLength="100"></asp:TextBox>
        </div>

        <div>
            <label for="RequestVendorCode" class="inline">Request Vendor Code </label>
            <asp:TextBox ID="RequestVendorCode" CssClass="text inline validate[required]" runat="server" MaxLength="100"></asp:TextBox>
        </div>

        <div>
            <label for="PartnerRateQualifire" class="inline">Partner Rate Qualifire </label>
            <asp:TextBox ID="PartnerRateQualifire" CssClass="text inline validate[required]" runat="server" MaxLength="100"></asp:TextBox>
        </div>
        <div>
            <label for="xmlProviderRate" class="inline">XML Provider Rate Qualifire </label>
            <asp:TextBox ID="xmlProviderRate" CssClass="text inline validate[required]" runat="server" MaxLength="15"></asp:TextBox>
        </div>
        
        <div>
          <label>
              <asp:CheckBox ID="VehiclePrefEnable" Text="Vehicle Pref Enable" runat="server" />
        </label>
        </div>

       
        <div class="formActions">
            <asp:Button ID="UpdateButton" CssClass="btn" OnClick="UpdateButton_Click" runat="server" Text="Update Partner Brand" />
            <a href="managePartnerBrand.aspx" class="CancelButton">Cancel</a>
        </div>
    </fieldset>
    </div>
</div>
</asp:Content>

