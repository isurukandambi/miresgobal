﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ORM;
using System.Linq;
using System.IO;
using CMSConfigs;
using CMSUtils;

public partial class manageLocations : System.Web.UI.Page
{
    private DatabaseEntities db;
    protected void Page_Load(object sender, EventArgs e)
    {
        Master.Page.Title = CMSSettings.SiteTitle + " - Edit Section - Manage Locations";

        if (Session["LoggedInName"] == null)
        {
            Response.Redirect("default.aspx?TimeOut=y&ReturnPage=" + Server.UrlEncode(HttpContext.Current.Request.Url.AbsoluteUri));
        }
        if (Request.QueryString["DeleteRow"] != null)
        {

            int locID = Convert.ToInt16(Request.QueryString["DeleteRow"]);
            DeleteItem(locID);
        }
        MsgToShow.Visible = false;
        if (Request.QueryString["MsgToShow"] != null)
        {
            int MsgType = Convert.ToInt16(Request.QueryString["MsgType"]);
            switch (MsgType)
            {
                case 1:
                    MsgToShow.Show(MyMessageBox.MessageType.Info, Request.QueryString["MsgToShow"].ToString());
                    break;
                case 2:
                    MsgToShow.Show(MyMessageBox.MessageType.Success, Request.QueryString["MsgToShow"].ToString());
                    break;
                case 3:
                    MsgToShow.Show(MyMessageBox.MessageType.Warning, Request.QueryString["MsgToShow"].ToString());
                    break;
                case 4:
                    MsgToShow.Show(MyMessageBox.MessageType.Error, Request.QueryString["MsgToShow"].ToString());
                    break;
                default:
                    goto case 1;
            }
            MsgToShow.Visible = true;
        }

        if (!Page.IsPostBack)
        {
            LoadCountries();
            LoadVehicalDetails();
        }
    }
    protected int rowID()
    {
        int rowID;
        bool result = int.TryParse(Request.QueryString["Row"], out rowID);
        if (result)
        {
            return rowID;
        }
        return 0;
    }
    public string FolderSelected()
    {
        return "\\CarImages\\";
    }
    protected void FolderToFilterBy_OnSelectedIndexChanged(Object sender, EventArgs e)
    {
        DirectoryInfo di = new DirectoryInfo(Request.PhysicalApplicationPath + "\\CarImages\\");
        LoadGridView(di);
    }
    public void RemoveFiles(string ImageToRemove)
    {
        string UpPath = Request.PhysicalApplicationPath + "\\CarImages\\" + ImageToRemove;
        FileInfo OriginalFile = new FileInfo(UpPath);
        if (OriginalFile.Exists)
        {
            File.Delete(UpPath);
        }
    }
    protected void LoadCountries()
    {
        db = new DatabaseEntities();
        ddlCountries.DataSource = db.Countries.OrderBy(c => c.CountryName).ToList();
        ddlCountries.DataTextField = "CountryName";
        ddlCountries.DataValueField = "CountryID";
        ddlCountries.DataBind();
        ddlCountries.Items.Insert(0, new ListItem("Choose a Country", "0"));
        ddlCountries.SelectedValue = rowID().ToString();
    }

    protected void CountryDDL_Changed(object sender, EventArgs e)
    {
        Response.Redirect("manageVehicalDetails.aspx?Row=" + Convert.ToInt32(ddlCountries.SelectedValue));
    }
    protected void LoadVehicalDetails()
    {
        db = new DatabaseEntities();
        int countryID = rowID();
        GridView1.DataSource = (from l in db.VehicleDetailsByBrands
                                join c in db.VehicleResources on l.VDBID equals c.VDBIDFK
                                join b in db.Brands on l.BrandIDFK equals b.BrandID
                                join g in db.CarGroups on l.CarGroupIDFK equals g.CarGroupID
                                orderby c.CountryIDFK
                                where c.CountryIDFK == countryID
                                select new { l.VDBID, g.CarCode, b.BrandName, l.CarDescription, l.CarImagePath }).ToList();
        GridView1.DataBind();
    }
    protected void LoadGridView(DirectoryInfo di)
    {
        db = new DatabaseEntities();
        int countryID = rowID();
        GridView1.DataSource = (from l in db.VehicleDetailsByBrands
                                join c in db.VehicleResources on l.VDBID equals c.VDBIDFK
                                join b in db.Brands on l.BrandIDFK equals b.BrandID
                                join g in db.CarGroups on l.CarGroupIDFK equals g.CarGroupID
                                orderby c.CountryIDFK
                                where c.CountryIDFK == countryID
                                select new { l.VDBID, g.CarCode, b.BrandName, l.CarDescription, l.CarImagePath }).ToList();
        GridView1.DataBind();
    }

    protected void DeleteItem(int VDBID)
    {
        try
        {
            db = new DatabaseEntities();
            var VehicleDetailsByBrand = db.VehicleDetailsByBrands.SingleOrDefault(l => l.VDBID == VDBID);
            var VehicleResource = db.VehicleResources.SingleOrDefault(l => l.VDBIDFK == VDBID);
            if (VehicleDetailsByBrand != null && VehicleResource != null)
            {
                db.VehicleResources.Remove(VehicleResource);
                db.SaveChanges();
                db.VehicleDetailsByBrands.Remove(VehicleDetailsByBrand);
                db.SaveChanges();
                Response.Redirect("manageVehicalDetails.aspx?MsgType=2&MsgToShow=Vehical Details Removed.&Row=" + VehicleDetailsByBrand.VDBID);
            }
            else
            {
                Response.Redirect("manageVehicalDetails.aspx?MsgType=4&MsgToShow=No Vehical Details found to Remove.");
            }
        }
        catch
        {
            Response.Redirect("manageVehicalDetails.aspx?MsgType=4&MsgToShow=" + CMSUtilities.FKconstrainsFail() );

        }

    }
    protected void GridView_MouseOver(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowIndex >= 0)
        {
            e.Row.Attributes.Add("onmouseover", "this.style.background='#FFFFFF'");
            e.Row.Attributes.Add("onmouseout", "this.style.background='#F7F7F7'");
        }
    }
}
