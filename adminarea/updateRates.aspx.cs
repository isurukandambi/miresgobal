﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ORM;
using CMSConfigs;

public partial class adminarea_updateRates : System.Web.UI.Page
{
    private DatabaseEntities db;
    protected void Page_Load(object sender, EventArgs e)
    {
        Master.Page.Title = CMSSettings.SiteTitle + " - " + PageName.Text;
        if (Session["LoggedInName"] == null)
        {
            Response.Redirect("default.aspx?TimeOut=y&ReturnPage=" + Server.UrlEncode(HttpContext.Current.Request.Url.AbsoluteUri));
        }
        if (!Page.IsPostBack)
        {
            LoadData();
        }
    }
    protected void LoadData()
    {
        db = new DatabaseEntities();
        int row = rowID();
        var XMLProv = db.Rates.SingleOrDefault(r => r.R_Id == row);
        RateType.Text = XMLProv.RateType;

    }
    protected int rowID()
    {
        int rowID;
        bool result = int.TryParse(Request.QueryString["Row"], out rowID);
        if (result)
        {
            return rowID;
        }
        return 0;
    }

    protected void UpdateButton_Click(object sender, EventArgs e)
    {
        try
        {
            // initialize the objectContext
            db = new DatabaseEntities();
            int row = rowID();
            if (!db.Rates.Where(r => r.RateType == RateType.Text.Trim() && r.R_Id != row).Any())
            {
                var Rate = db.Rates.SingleOrDefault(r => r.R_Id == row);
                Rate.RateType = RateType.Text.Trim();
                db.SaveChanges();

                Response.Redirect("manageRates.aspx?MsgToShow=Rate updated successfully.&MsgType=2");
            }
            else
            {
                MsgToShow.Show(MyMessageBox.MessageType.Warning, "That Rate already exists. Please use another  Rate Type.");
                MsgToShow.Visible = true;
            }

        }
        catch
        {
            MsgToShow.Show(MyMessageBox.MessageType.Error, "There was an error adding this information. Please try again.");
            MsgToShow.Visible = true;
        }
    }
}