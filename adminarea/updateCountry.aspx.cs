﻿using System;
using System.Linq;
using System.Text;
using System.Web;
using ORM;
using CMSConfigs;

public partial class updateCountry : System.Web.UI.Page
{
    private DatabaseEntities db;

    protected void Page_Load(object sender, EventArgs e)
    {
        Master.Page.Title = CMSSettings.SiteTitle + " - " + PageName.Text;
        if (Session["LoggedInName"] == null)
        {
            Response.Redirect("default.aspx?TimeOut=y&ReturnPage=" + Server.UrlEncode(HttpContext.Current.Request.Url.AbsoluteUri));
        }
        if(!Page.IsPostBack){
            LoadData();
        }
    }
    protected void LoadData() {
        db = new DatabaseEntities();
        int row = rowID();
        var country = db.Countries.SingleOrDefault(c => c.CountryID == row);
        Countryname.Text = country.CountryName;
        CountryCode.Text = country.CountryCode;
    }
    protected int rowID()
    {
        int rowID;
        bool result = int.TryParse(Request.QueryString["Row"], out rowID);
        if (result)
        {
            return rowID;
        }
        return 0;
    }
    protected void UpdateButton_Click(object sender, EventArgs e)
    {
        try
        {
            // initialize the objectContext
            db = new DatabaseEntities();
            int row = rowID();
            if (!db.Countries.Where(c => c.CountryCode == CountryCode.Text  && c.CountryID != row).Any())
            {
                var country = db.Countries.SingleOrDefault(c => c.CountryID == row);
                country.CountryName = Countryname.Text.Trim();
                country.CountryCode = CountryCode.Text.Trim();
                db.SaveChanges();

                Response.Redirect("manageCountries.aspx?MsgToShow=Country updated successfully.&MsgType=2");
            }
            else
            {
                MsgToShow.Show(MyMessageBox.MessageType.Warning, "Country Code already exists. Please use another Country Code.");
                MsgToShow.Visible = true;
            }

        }
        catch
        {
            MsgToShow.Show(MyMessageBox.MessageType.Error, "There was an error adding this information. Please try again.");
            MsgToShow.Visible = true;
        }
    }
}