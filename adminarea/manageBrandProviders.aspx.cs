﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ORM;
using CMSConfigs;

public partial class adminarea_manageBrandProviders : System.Web.UI.Page
{
    private DatabaseEntities db;
    protected void Page_Load(object sender, EventArgs e)
    {
        Master.Page.Title = CMSSettings.SiteTitle + " - Edit Section - Manage Brand mapping";

        if (Session["LoggedInName"] == null)
        {
            Response.Redirect("default.aspx?TimeOut=y&ReturnPage=" + Server.UrlEncode(HttpContext.Current.Request.Url.AbsoluteUri));
        }

        if (!Page.IsPostBack)
        {
            DisplayActionMessage();
            LoadBrands();
            LoadMappedLocations();

        }
    }

    protected int BrandID()
    {
        int rowID;
        bool result = int.TryParse(Request.QueryString["Brand"], out rowID);
        if (result)
        {
            return rowID;
        }
        return 0;
    }

    protected void LoadBrands()
    {
        db = new DatabaseEntities();
        ddlBrands.DataSource = db.Brands.OrderBy(b => b.BrandName).ToList();
        ddlBrands.DataTextField = "BrandName";
        ddlBrands.DataValueField = "BrandID";
        ddlBrands.DataBind();
        ddlBrands.Items.Insert(0, new ListItem("Choose a Brands", "0"));
        ddlBrands.SelectedValue = BrandID().ToString();
    }

    protected void BrandDDL_Changed(object sender, EventArgs e)
    {
        Response.Redirect("manageBrandProviders.aspx?&Brand=" + Convert.ToInt32(ddlBrands.SelectedValue));
    }

    protected void LoadMappedLocations()
    {
        db = new DatabaseEntities();
        int brandID = BrandID();
        if (brandID != 0)
        {
            var BrandProviders = (from bp in db.BrandProviderMappings
                                  join b in db.Brands on bp.BrandIDFK equals b.BrandID
                                  join p in db.Providers on bp.ProviderIDFK equals p.Pro_Id
                                  where bp.BrandIDFK == brandID
                                  orderby b.BrandName
                                 select new
                                 {
                                     bp.BPMID,
                                     p.ProviderName,
                                     b.BrandName,
                                 });
            if (BrandProviders.Count() > 0)
            {
                InformationTable.Visible = true;
                DeleteItems.Visible = true;
            }
            else
            {
                NoRecordsFound.Visible = true;
            }
            InformationTable.DataSource = BrandProviders.ToList();
            InformationTable.DataBind();
        }
        else
        {

            var BrandProviders = (from bp in db.BrandProviderMappings
                                  join b in db.Brands on bp.BrandIDFK equals b.BrandID
                                  join p in db.Providers on bp.ProviderIDFK equals p.Pro_Id
                                  orderby b.BrandName
                                  select new
                                  {
                                      bp.BPMID,
                                      p.ProviderName,
                                      b.BrandName,
                                  });
            if (BrandProviders.Count() > 0)
            {
                InformationTable.Visible = true;
                DeleteItems.Visible = true;
            }
            else
            {
                NoRecordsFound.Visible = true;
            }
            InformationTable.DataSource = BrandProviders.ToList();
            InformationTable.DataBind();

        }

    }

    protected void DeleteItems_Click(object sender, EventArgs e)
    {
        ConfirmBox.ShowConfirmBox("Delete Brand Mappings", "Are you sure you want to remove selected Brand Mappings from system?", "Delete|Cancel");
        ConfirmBox.Visible = true;
    }
    protected void Confirm_Fired(object sender, EventArgs e)
    {
        bool confirm = (bool)sender;
        if (confirm == true)
        {
            DeleteDatabaseItem(InformationTable);
        }
    }
    protected void DeleteDatabaseItem(Repeater RepeaterID)
    {
        string message = "";
        //initialize the objectContext
        db = new DatabaseEntities();
        foreach (RepeaterItem item in RepeaterID.Items)
        {
            CheckBox cb = (CheckBox)item.FindControl("rowcheck");
            if (cb != null && cb.Checked)
            {
                Literal DBrow = (Literal)item.FindControl("rowID");
                int row = Convert.ToInt16(DBrow.Text);
                message += DeleteItem(row, db);
            }
        }
        if (string.IsNullOrEmpty(message))
        {
            Response.Redirect("manageBrandProviders.aspx?MsgType=2&MsgToShow=Item(s) deleted successfully.&Brand=" + BrandID());
        }
        else
        {
            Response.Redirect("manageBrandProviders.aspx?MsgType=4&MsgToShow=" + message + "&Brand=" + BrandID());
        }

    }


    protected string DeleteItem(int deleteItemID, DatabaseEntities dbCon)
    {
        string message = "";
        var item = dbCon.BrandProviderMappings.SingleOrDefault(bp => bp.BPMID == deleteItemID);
        try
        {
            dbCon.BrandProviderMappings.Remove(item);
            dbCon.SaveChanges();
        }
        catch
        {
            //dbCon.BrandProviderMappings.Detach(item);
            message = "Error occurred while deleting " + item.Brand.BrandName + ". Brand mapping already in use. |";
        }
        return message;
    }

    protected void InformationTable_RowDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            //int locID = Convert.ToInt16(((HiddenField)e.Item.FindControl("LocationID")).Value);
            int brandLocID = Convert.ToInt16(((Literal)e.Item.FindControl("rowID")).Text);
            //Label LocationLink = ((Label)e.Item.FindControl("LocationLink"));
        }
    }

    protected void GridView_MouseOver(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowIndex >= 0)
        {
            e.Row.Attributes.Add("onmouseover", "this.style.background='#FFFFFF'");
            e.Row.Attributes.Add("onmouseout", "this.style.background='#F7F7F7'");
        }
    }

    protected void DisplayActionMessage()
    {
        if ((Request.QueryString["MsgType"] != null) && (Request.QueryString["MsgToShow"] != null))
        {
            string msgToShow = Request.QueryString["MsgToShow"].ToString();
            int rowID;
            bool result = int.TryParse(Request.QueryString["MsgType"], out rowID);
            if ((result) && !String.IsNullOrEmpty(msgToShow))
            {
                switch (Convert.ToInt16(Request.QueryString["MsgType"]))
                {
                    case 1:
                        MsgToShow.Show(MyMessageBox.MessageType.Info, msgToShow);
                        break;
                    case 2:
                        MsgToShow.Show(MyMessageBox.MessageType.Success, msgToShow);
                        break;
                    case 3:
                        MsgToShow.Show(MyMessageBox.MessageType.Warning, msgToShow);
                        break;
                    case 4:
                        MsgToShow.Show(MyMessageBox.MessageType.Error, msgToShow.Replace("|", "</br>"));
                        break;
                    default:
                        goto case 1;
                }
                MsgToShow.Visible = true;
            }
        }
    }
}