﻿using System;
using System.Linq;
using System.Text;
using System.Web;
using ORM;
using CMSConfigs;

public partial class addCarGroup : System.Web.UI.Page
{
    private DatabaseEntities db;

    protected void Page_Load(object sender, EventArgs e)
    {
        Master.Page.Title = CMSSettings.SiteTitle + " - " + PageName.Text;
        if (Session["LoggedInName"] == null)
        {
            Response.Redirect("default.aspx?TimeOut=y&ReturnPage=" + Server.UrlEncode(HttpContext.Current.Request.Url.AbsoluteUri));
        }
    }


    protected void AddButton_Click(object sender, EventArgs e)
    {
        try
        {
            // initialize the objectContext
            db = new DatabaseEntities();
            if (CarCode.Text.Trim() == null)
            {
                MsgToShow.Show(MyMessageBox.MessageType.Warning, "Car Code Cannot be null. Please use another  Car Code.");
                MsgToShow.Visible = true;
            }
            else 
            { 
                if (!db.CarGroups.Where(c => c.CarCode == CarCode.Text.Trim()).Any())
                {
                    CarGroup CarGroup = new CarGroup
                    {
                        CarCode = CarCode.Text.Trim(),
                    
                    };
                    db.CarGroups.Add(CarGroup);
                    db.SaveChanges();

                    Response.Redirect("manageCarGroups.aspx?MsgType=2&MsgToShow='" + CarCode.Text.Trim() + "' added successfully");

                }
                else
                {
                    MsgToShow.Show(MyMessageBox.MessageType.Warning, "That Car Code already exists. Please use another  Car Code.");
                    MsgToShow.Visible = true;
                }
            }

        }
        catch
        {
            MsgToShow.Show(MyMessageBox.MessageType.Error, "There was an error adding this information. Please try again.");
            MsgToShow.Visible = true;
        }
    }
}