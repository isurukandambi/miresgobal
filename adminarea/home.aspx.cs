﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Net;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Linq;
using Ionic.Zip;
using System.Text;
using ORM;
using CMSConfigs;


public partial class home : System.Web.UI.Page
{
    private DatabaseEntities db;
    SqlConnection conn = new SqlConnection(CMSSettings.connctionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        
        Master.Page.Title = CMSSettings.SiteTitle + " - Edit Section - dashboard";

        if (Session["LoggedInName"] == null)
        {
            Response.Redirect("default.aspx?TimeOut=y");
        }
        

        MsgToShow.Visible = false;
        if (Request.QueryString["MsgToShow"] != null)
        {
            int MsgType = Convert.ToInt16(Request.QueryString["MsgType"]);
            switch (MsgType)
            {
                case 1:
                    MsgToShow.Show(MyMessageBox.MessageType.Info, Request.QueryString["MsgToShow"].ToString());
                    break;
                case 2:
                    MsgToShow.Show(MyMessageBox.MessageType.Success, Request.QueryString["MsgToShow"].ToString());
                    break;
                case 3:
                    MsgToShow.Show(MyMessageBox.MessageType.Warning, Request.QueryString["MsgToShow"].ToString());
                    break;
                case 4:
                    MsgToShow.Show(MyMessageBox.MessageType.Error, Request.QueryString["MsgToShow"].ToString());
                    break;
                default:
                    goto case 1;
            }
            MsgToShow.Visible = true;
        }


        if (!Page.IsPostBack)
        {
            LoadPartners();
            PageAccess();
            LoadRequestTimeoutChart();

            if (Request.QueryString["partnerId"] != null)
            {
                PartnerID.SelectedValue = Request.QueryString["partnerId"].ToString();
            }

            //string partnerName = PartnerID.SelectedItem.ToString(); 
            //Load content for this page
            //ViewExpiringOffers();

            if(Request.QueryString["partnerName"] != null)
            {
                 LoadChart(Request.QueryString["partnerName"].ToString());
            }


        }
        else
        {
            //Response.Write("1234");
        }

       // Response.Write(PartnerID.SelectedItem);
    }
    protected void PageAccess()
    {
        bool dashboardAllowed = false;
        List<string> SectionOnSite = (List<string>)Session["SectionOnSite"];
        foreach (string section in SectionOnSite)
        {
            if ((section == "Dashboard") || (section == "DashboardNoReport"))
            {
                dashboardAllowed = true;
                if (section == "DashboardNoReport")
                {
                    ExportReportHeader.Attributes["style"] = "display:none;";
                }
            }
        }
        if (!dashboardAllowed)
        {
            Response.Redirect("userhome.aspx");
        }
    }

    protected string YesNoWording(int ValueInDatabase)
    {
        if (ValueInDatabase == 1)
            return "yes";
        else
            return "no";
    }

    //holds the report data details
    public class ReportData
    {
        public DateTime date { get; set; }
        public string numberOrders { get; set; }
    }

    protected void GenerateDailySalesChart(string provider, DateTime from, DateTime to, HiddenField TotalSales, HiddenField TotalHertzSales, HiddenField TotalThriftySales, HiddenField TotalFireflyHSales, HiddenField TotalDollarSales, HiddenField HertzSales, HiddenField ThriftySales,  HiddenField FireflyHSales, HiddenField DollarSales, string partnerName)
    {
        
        DateTime startDate = from;
        DateTime endDate = to;
        //string provider1 = "Hertz";
        TimeSpan startTS = new TimeSpan(0, 0, 0);
        TimeSpan endTS = new TimeSpan(23, 59, 59);
        List<ReportData> reportDataHertz = new List<ReportData>();
        List<ReportData> reportDataThrifty = new List<ReportData>();
        List<ReportData> reportDataFireflyAE = new List<ReportData>();
        List<ReportData> reportDataFireflyH = new List<ReportData>();
        List<ReportData> reportDataDollar = new List<ReportData>();
        int TotalNumOrders = 0;
        int TotalHertzOrders = 0;
        int TotalThriftyOrders = 0;
       // int TotalFireflyAEOrders = 0;
        int TotalFireflyHOrders = 0;
        int TotalDollarOrders = 0;
        db = new DatabaseEntities();

        //Response.Write(startDate + " " + endDate + "</br>");
        var orders = db.Orders.AsEnumerable().Where(o => startDate.Date <= Convert.ToDateTime(o.OrderDate).Date && endDate.Date >= Convert.ToDateTime(o.OrderDate).Date && o.CameFrom == partnerName).OrderBy(o => Convert.ToDateTime(o.OrderDate).Date).GroupBy(o => new { Convert.ToDateTime(o.OrderDate).Date, o.XML_Provider });

        foreach (var order in orders)
        {
            //Response.Write("Provider---->" + order.FirstOrDefault().XML_Provider +"Date-------->"+order.FirstOrDefault().OrderDate+ "</br>");
            if (order.FirstOrDefault().XML_Provider == "Hertz")
            {
               // Response.Write("Date-------------->" + Convert.ToDateTime(order.FirstOrDefault().OrderDate).Year + "/" + Convert.ToDateTime(order.FirstOrDefault().OrderDate).Month + "/" + Convert.ToDateTime(order.FirstOrDefault().OrderDate).Day + "Count--------------->" + order.Count() + "</br>");
                TotalNumOrders += order.Count();
                TotalHertzOrders += order.Count();
                reportDataHertz.Add(new ReportData { date = Convert.ToDateTime(Convert.ToDateTime(order.FirstOrDefault().OrderDate).Year + "/" + Convert.ToDateTime(order.FirstOrDefault().OrderDate).Month + "/" + Convert.ToDateTime(order.FirstOrDefault().OrderDate).Day), numberOrders = order.Count().ToString() });
            }
            else if (order.FirstOrDefault().XML_Provider == "Thrifty")
            {

               // Response.Write("Date-------------->" + Convert.ToDateTime(order.FirstOrDefault().OrderDate).Year + "/" + Convert.ToDateTime(order.FirstOrDefault().OrderDate).Month + "/" + Convert.ToDateTime(order.FirstOrDefault().OrderDate).Day + "Count--------------->" + order.Count() + "</br>");
                TotalNumOrders += order.Count();
                TotalThriftyOrders += order.Count();
                reportDataThrifty.Add(new ReportData { date = Convert.ToDateTime(Convert.ToDateTime(order.FirstOrDefault().OrderDate).Year + "/" + Convert.ToDateTime(order.FirstOrDefault().OrderDate).Month + "/" + Convert.ToDateTime(order.FirstOrDefault().OrderDate).Day), numberOrders = order.Count().ToString() });
            }
            else if (order.FirstOrDefault().XML_Provider == "FireFly")
            {
               // Response.Write("Date-------------->" + Convert.ToDateTime(order.FirstOrDefault().OrderDate).Year + "/" + Convert.ToDateTime(order.FirstOrDefault().OrderDate).Month + "/" + Convert.ToDateTime(order.FirstOrDefault().OrderDate).Day + "Count--------------->" + order.Count() + "</br>");
                TotalNumOrders += order.Count();
                TotalFireflyHOrders += order.Count();
                reportDataFireflyH.Add(new ReportData { date = Convert.ToDateTime(Convert.ToDateTime(order.FirstOrDefault().OrderDate).Year + "/" + Convert.ToDateTime(order.FirstOrDefault().OrderDate).Month + "/" + Convert.ToDateTime(order.FirstOrDefault().OrderDate).Day), numberOrders = order.Count().ToString() });
            }
            else if (order.FirstOrDefault().XML_Provider == "Dollar")
            {
               // Response.Write("Date-------------->" + Convert.ToDateTime(order.FirstOrDefault().OrderDate).Year + "/" + Convert.ToDateTime(order.FirstOrDefault().OrderDate).Month + "/" + Convert.ToDateTime(order.FirstOrDefault().OrderDate).Day + "Count--------------->" + order.Count() + "</br>");
                TotalNumOrders += order.Count();
                TotalDollarOrders += order.Count();
                reportDataDollar.Add(new ReportData { date = Convert.ToDateTime(Convert.ToDateTime(order.FirstOrDefault().OrderDate).Year + "/" + Convert.ToDateTime(order.FirstOrDefault().OrderDate).Month + "/" + Convert.ToDateTime(order.FirstOrDefault().OrderDate).Day), numberOrders = order.Count().ToString() });
            }
        }
       

        //put the total order figures into relevant hidden fields
        
        TotalSales.Value = TotalNumOrders.ToString();
        TotalHertzSales.Value = TotalHertzOrders.ToString();
        TotalThriftySales.Value = TotalThriftyOrders.ToString();
        //TotalFireflyAESales.Value = TotalFireflyAEOrders.ToString();
        TotalFireflyHSales.Value = TotalFireflyHOrders.ToString();
        TotalDollarSales.Value = TotalDollarOrders.ToString();

        //go through all dates from start to finish. If any of these dates are missing from our database values put in a zero
        foreach (var date in GetDateRange(startDate, endDate))
        {
            //handle hertz details
            HertzSales.Value += ChartValue(reportDataHertz, date);

            //handle thrifty details
            ThriftySales.Value += ChartValue(reportDataThrifty, date);

            //handle FireFlyAE details
           // FireflyAESales.Value += ChartValue(reportDataFireflyAE, date);

            //handle FireflyH details
            FireflyHSales.Value += ChartValue(reportDataFireflyH, date);

            //handle Dollar details
            DollarSales.Value += ChartValue(reportDataDollar, date);
        }

        //remove the last coma in the results data
        HertzSales.Value = HertzSales.Value.Remove(HertzSales.Value.Length - 1);
        //Response.Write(HertzSales.Value);
        ThriftySales.Value = ThriftySales.Value.Remove(ThriftySales.Value.Length - 1);
        //Response.Write("Thrifty Sales value" + ThriftySales.Value + "</br>");
       // FireflyAESales.Value = FireflyAESales.Value.Remove(FireflyAESales.Value.Length - 1);
        FireflyHSales.Value = FireflyHSales.Value.Remove(FireflyHSales.Value.Length - 1);

        DollarSales.Value = DollarSales.Value.Remove(DollarSales.Value.Length - 1);
    }


    protected void GenerateMonthlySalesCharts(string provider, DateTime from, DateTime to, HiddenField TotalSales, HiddenField TotalHertzSales, HiddenField TotalThriftySales, HiddenField TotalFireflyAESales, HiddenField TotalFireflyHSales, HiddenField TotalDollarSales, HiddenField HertzSales, HiddenField ThriftySales, HiddenField FireflyAESales, HiddenField FireflyHSales, HiddenField DollarSales, HiddenField MonthsSelected, string partnerName)
    {
        DateTime startDate = new DateTime(from.Year, from.Month, 1);
        DateTime endDate = to;
        int sMonth = Convert.ToInt32(startDate.Month);
        int lMonth = Convert.ToInt32(endDate.Month);
        int sYear = startDate.Year;
        int lYear = endDate.Year;
        TimeSpan startTS = new TimeSpan(0, 0, 0);
        TimeSpan endTS = new TimeSpan(23, 59, 59);
        List<ReportData> reportDataHertz = new List<ReportData>();
        List<ReportData> reportDataThrifty = new List<ReportData>();
        List<ReportData> reportDataFireflyAE = new List<ReportData>();
        List<ReportData> reportDataFireflyH = new List<ReportData>();
        List<ReportData> reportDataDollar = new List<ReportData>();
        string brand, mnth, mnth1, mnth2, mnth3;
        int gap = ((lYear - sYear) * 12) + lMonth - sMonth + 1;

        int TotalNumOrders = 0;
        int TotalHertzOrders = 0;
        int TotalThriftyOrders = 0;
        int TotalFireflyAEOrders = 0;
        int TotalFireflyHOrders = 0;
        int TotalDollarOrders = 0;
        db = new DatabaseEntities();
        //var orders = db.Orders.AsEnumerable().Where(o => startDate.Date <= Convert.ToDateTime(o.OrderDate).Date && endDate.Date >= Convert.ToDateTime(o.OrderDate).Date && o.CameFrom == partnerName).OrderBy(o => Convert.ToDateTime(o.OrderDate).Month).GroupBy(o => new { Convert.ToDateTime(o.OrderDate).Month, o.XML_Provider });

        using (var command = new SqlCommand("SelectMonthlyData", conn))
        {
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.Add("@todayMonth", SqlDbType.Int).Value = lMonth;
            command.Parameters.Add("@lastMonth", SqlDbType.Int).Value = sMonth;
            command.Parameters.Add("@todayYear", SqlDbType.Int).Value = lYear;
            command.Parameters.Add("@lastYear", SqlDbType.Int).Value = sYear;
            command.Parameters.Add("@gap", SqlDbType.Int).Value = gap;
            conn.Open();

            var sqlReader = command.ExecuteReader();
            if (sqlReader.HasRows)
            {
                while (sqlReader.Read())
                {
                    brand = sqlReader["Brand"].ToString();
                    if (brand == "Hertz")
                    {
                      
                        TotalNumOrders += Convert.ToInt32(sqlReader["OrderCount"]);
                        TotalHertzOrders += Convert.ToInt32(sqlReader["OrderCount"]);
                        mnth = sqlReader["MonthDet"].ToString();
                        mnth = mnth.Replace("_", "");
                        reportDataHertz.Add(new ReportData { date = Convert.ToDateTime(sqlReader["yearDet"] + "/" + Convert.ToInt32(mnth) + "/01"), numberOrders = (sqlReader["OrderCount"]).ToString() });
                    }
                    else if (brand == "Thrifty")
                    {
                        TotalNumOrders += Convert.ToInt32(sqlReader["OrderCount"]);
                        TotalThriftyOrders += Convert.ToInt32(sqlReader["OrderCount"]);
                        mnth1 = sqlReader["MonthDet"].ToString();
                        mnth1 = mnth1.Replace("_", "");
                        //reportDataThrifty.Add(new ReportData { date = Convert.ToDateTime(Convert.ToDateTime(order.FirstOrDefault().OrderDate).Year + "/" + Convert.ToDateTime(order.FirstOrDefault().OrderDate).Month + "/01"), numberOrders = order.Count().ToString() });
                        reportDataThrifty.Add(new ReportData { date = Convert.ToDateTime(sqlReader["yearDet"] + "/" + Convert.ToInt32(mnth1) + "/01"), numberOrders = (sqlReader["OrderCount"]).ToString() });
                    }
                    else if (brand == "Firefly")
                    {
                        TotalNumOrders += Convert.ToInt32(sqlReader["OrderCount"]);
                        TotalFireflyHOrders += Convert.ToInt32(sqlReader["OrderCount"]);
                        mnth2 = sqlReader["MonthDet"].ToString();
                        mnth2 = mnth2.Replace("_", "");
                        //reportDataFireflyH.Add(new ReportData { date = Convert.ToDateTime(Convert.ToDateTime(order.FirstOrDefault().OrderDate).Year + "/" + Convert.ToDateTime(order.FirstOrDefault().OrderDate).Month + "/01"), numberOrders = order.Count().ToString() });
                        reportDataFireflyH.Add(new ReportData { date = Convert.ToDateTime(sqlReader["yearDet"] + "/" + Convert.ToInt32(mnth2) + "/01"), numberOrders = (sqlReader["OrderCount"]).ToString() });
                    }
                    else if (brand == "Dollar")
                    {
                        TotalNumOrders += Convert.ToInt32(sqlReader["OrderCount"]);
                        TotalDollarOrders += Convert.ToInt32(sqlReader["OrderCount"]);
                        mnth3 = sqlReader["MonthDet"].ToString();
                        mnth3 = mnth3.Replace("_", "");
                        //reportDataThrifty.Add(new ReportData { date = Convert.ToDateTime(Convert.ToDateTime(order.FirstOrDefault().OrderDate).Year + "/" + Convert.ToDateTime(order.FirstOrDefault().OrderDate).Month + "/01"), numberOrders = order.Count().ToString() });
                        reportDataDollar.Add(new ReportData { date = Convert.ToDateTime(sqlReader["yearDet"] + "/" + Convert.ToInt32(mnth3) + "/01"), numberOrders = (sqlReader["OrderCount"]).ToString() });
                    }
                }
            }
        }
        conn.Close();
        //foreach (var order in orders)
        //{
        //    if (order.FirstOrDefault().XML_Provider == "Hertz")
        //    {
        //       // Response.Write("Date-------------->" + Convert.ToDateTime(order.FirstOrDefault().OrderDate).Year + "/" + Convert.ToDateTime(order.FirstOrDefault().OrderDate).Month + "/01" + "Count--------------->" + order.Count() + "</br>");
        //        TotalNumOrders += order.Count();
        //        TotalHertzOrders += order.Count();
        //        reportDataHertz.Add(new ReportData { date = Convert.ToDateTime(Convert.ToDateTime(order.FirstOrDefault().OrderDate).Year + "/" + Convert.ToDateTime(order.FirstOrDefault().OrderDate).Month + "/01"), numberOrders = order.Count().ToString() });
        //    }
        //    else if (order.FirstOrDefault().XML_Provider == "Thrifty")
        //    {

        //      //  Response.Write("Date-------------->" + Convert.ToDateTime(order.FirstOrDefault().OrderDate).Year + "/" + Convert.ToDateTime(order.FirstOrDefault().OrderDate).Month + "/01" + "Count--------------->" + order.Count() + "</br>");
        //        TotalNumOrders += order.Count();
        //        TotalThriftyOrders += order.Count();
        //        reportDataThrifty.Add(new ReportData { date = Convert.ToDateTime(Convert.ToDateTime(order.FirstOrDefault().OrderDate).Year + "/" + Convert.ToDateTime(order.FirstOrDefault().OrderDate).Month + "/01"), numberOrders = order.Count().ToString() });
        //    }
        //    else if (order.FirstOrDefault().XML_Provider == "FireFly")
        //    {
        //       // Response.Write("Date-------------->" + Convert.ToDateTime(order.FirstOrDefault().OrderDate).Year + "/" + Convert.ToDateTime(order.FirstOrDefault().OrderDate).Month + "/01" + "Count--------------->" + order.Count() + "</br>");
        //        TotalNumOrders += order.Count();
        //        TotalFireflyHOrders += order.Count();
        //        reportDataFireflyH.Add(new ReportData { date = Convert.ToDateTime(Convert.ToDateTime(order.FirstOrDefault().OrderDate).Year + "/" + Convert.ToDateTime(order.FirstOrDefault().OrderDate).Month + "/01"), numberOrders = order.Count().ToString() });
        //    }
        //}
     
        //put the total order figures into relevant hidden fields
        TotalSales.Value = Convert.ToInt32(TotalNumOrders.ToString()).ToString("N0");
        TotalHertzSales.Value = Convert.ToInt32(TotalHertzOrders.ToString()).ToString("N0");
        TotalThriftySales.Value = Convert.ToInt32(TotalThriftyOrders.ToString()).ToString("N0");
        TotalFireflyAESales.Value = Convert.ToInt32(TotalFireflyAEOrders.ToString()).ToString("N0");
        TotalFireflyHSales.Value = Convert.ToInt32(TotalFireflyHOrders.ToString()).ToString("N0");
        TotalDollarSales.Value = Convert.ToInt32(TotalDollarOrders.ToString()).ToString("N0");

        //go through all dates from start to finish. If any of these dates are missing from our database values put in a zero
        foreach (var date in GetMonthRange(startDate, endDate))
        {

            //handle hertz details
            HertzSales.Value += ChartValue(reportDataHertz, date);

            //handle thrifty details
            ThriftySales.Value += ChartValue(reportDataThrifty, date);

            //handle FireFlyAE details
            FireflyAESales.Value += ChartValue(reportDataFireflyAE, date);

            //handle FireflyH details
            FireflyHSales.Value += ChartValue(reportDataFireflyH, date);

            //handle Dollar details
            DollarSales.Value += ChartValue(reportDataDollar, date);

            //put the month name into the container for the chart X axis values
            MonthsSelected.Value += date.ToString("MMM", System.Globalization.CultureInfo.InvariantCulture) + ",";
        }

        //remove the last coma in the results data
        MonthsSelected.Value = MonthsSelected.Value.Remove(MonthsSelected.Value.Length - 1);
        HertzSales.Value = HertzSales.Value.Remove(HertzSales.Value.Length - 1);
        ThriftySales.Value = ThriftySales.Value.Remove(ThriftySales.Value.Length - 1);
        FireflyAESales.Value = FireflyAESales.Value.Remove(FireflyAESales.Value.Length - 1);
        FireflyHSales.Value = FireflyHSales.Value.Remove(FireflyHSales.Value.Length - 1);
        DollarSales.Value = DollarSales.Value.Remove(DollarSales.Value.Length - 1);
    }

    protected void GeneratePieChart(string storedProcedure, string databaseText, string databaseValue, HiddenField pieChartData, string partnerName)
    {
        if (storedProcedure == "SellingAccessories")
        {
            int AcceType = 0;
            int quantity = 0;
            string cartTypeName = "";

            using (var command = new SqlCommand("SelectAccessoriesData", conn))
            {
                command.CommandType = CommandType.StoredProcedure;
                // command.Parameters.Add("@SiteLocation", SqlDbType.NVarChar).Value = HttpContext.Current.Server.HtmlEncode(langCode);
                conn.Open();
                var sqlReader = command.ExecuteReader();
                if (sqlReader.HasRows)
                {
                    pieChartData.Value = "[";
                    while (sqlReader.Read())
                    {
                        AcceType = Convert.ToInt32(sqlReader["Accessory"]);
                        quantity = Convert.ToInt32(sqlReader["Quantity"]);

                        switch (AcceType)
                        {
                            case 7:
                                cartTypeName = "Infant Seat";
                                break;
                            case 8:
                                cartTypeName = "Child Seat";
                                break;
                            case 9:
                                cartTypeName = "Booster Seat";
                                break;
                            case 13:
                                cartTypeName = "Neverlost";
                                break;
                            case 14:
                                cartTypeName = "Winter Tyres";
                                break;
                        }
                        if (!String.IsNullOrEmpty(cartTypeName))
                        {
                            //  Response.Write("car Type-------------->" + carType + " ----- Count --- " + Accessories.allaccessoryCount + "</br>");
                            pieChartData.Value += "['" + cartTypeName + "'," + quantity + "],";
                        }
                    }
                    if (pieChartData.Value.Length > 0)
                        pieChartData.Value = pieChartData.Value.Remove(pieChartData.Value.Length - 1);
                    pieChartData.Value += "]";
                    //Response.Write("SellingAccessories" + pieChartData.Value + "<br>");
                }
            }
            conn.Close();
            //db = new DatabaseEntities();
            //var AccessoriesOrdered = (from a in db.AccessoriesOrdereds 
            //                          join o in db.Orders on a.OrderIDFK equals o.OrderID
            //                          where o.CameFrom == partnerName
            //                          group a by a.Accessory into g
            //                          select new
            //                          {
            //                              accessoryName = g.FirstOrDefault().Accessory,
            //                              allaccessoryCount = g.Count()
            //                          }
            //                             );


            ////db.AccessoriesOrdereds.GroupBy(o => o.Accessory);
            //pieChartData.Value = "[";
            //if (AccessoriesOrdered.Count() > 0)
            //{
            //    foreach (var Accessories in AccessoriesOrdered)
            //    {
            //        string carType = Accessories.accessoryName;

            //        switch (carType)
            //        {
            //            case "7":
            //                carType = "Infant Seat";
            //                break;
            //            case "8":
            //                carType = "Child Seat";
            //                break;
            //            case "9":
            //                carType = "Booster Seat";
            //                break;
            //            case "13":
            //                carType = "Neverlost";
            //                break;
            //            case "14":
            //                carType = "Winter Tyres";
            //                break;
            //        }
            //        if (!String.IsNullOrEmpty(carType))
            //        {
            //            //  Response.Write("car Type-------------->" + carType + " ----- Count --- " + Accessories.allaccessoryCount + "</br>");
            //            pieChartData.Value += "['" + carType + "'," + Accessories.allaccessoryCount + "],";
            //        }
            //    }
            //}

            ////remove the last coma in the results data
            //if (pieChartData.Value.Length > 0)
            //    pieChartData.Value = pieChartData.Value.Remove(pieChartData.Value.Length - 1);
            //pieChartData.Value += "]";
            //// Response.Write(pieChartData.Value);
        }
        else if (storedProcedure == "VehicleType")
        {
            string VehicleType;
            int quantity = 0;
            //string cartTypeName = "";



            using (var command = new SqlCommand("SelectVehicleData", conn))
            {
                command.CommandType = CommandType.StoredProcedure;
                // command.Parameters.Add("@SiteLocation", SqlDbType.NVarChar).Value = HttpContext.Current.Server.HtmlEncode(langCode);
                conn.Open();
                var sqlReader = command.ExecuteReader();
                //Response.Write("<br>" + sqlReader.Read() + "<br>");
                if (sqlReader.HasRows)
                {
                    pieChartData.Value = "[";
                    //int i = 0;
                    while (sqlReader.Read())
                    {
                        //Response.Write("count : " + i + sqlReader["VehicleType"] + "<br>");
                        //++i;

                        VehicleType = sqlReader["VehicleType"].ToString();
                        quantity = Convert.ToInt32(sqlReader["Quantity"]);


                        if (!String.IsNullOrEmpty(VehicleType))
                        {
                            //  Response.Write("car Type-------------->" + carType + " ----- Count --- " + Accessories.allaccessoryCount + "</br>");
                            pieChartData.Value += "['" + VehicleType + "'," + quantity + "],";
                        }
                    }
                    if (pieChartData.Value.Length > 0)
                        //Response.Write(pieChartData.Value.Length + "<br>");
                        //Response.Write(pieChartData.Value + "<br>");
                        pieChartData.Value = pieChartData.Value.Remove(pieChartData.Value.Length - 1);
                    pieChartData.Value += "]";
                    //Response.Write("VehicleType" + pieChartData.Value + "<br>");
                }
            }
            conn.Close();
            //db = new DatabaseEntities();

            //var Topcars = (from a in db.Orders
            //               where a.CameFrom == partnerName 
            //               group a by a.VehicleType into g
            //               orderby g.Count() descending
            //               select new
            //               {
            //                   vehicalType = g.FirstOrDefault().VehicleType,
            //                   allVehicalCount = g.Count()
            //               }
            //                             ).Take(10);

            //pieChartData.Value = "[";
            //if (Topcars.Count() > 0)
            //{
            //    foreach (var Top10car in Topcars)
            //    {
            //        // Response.Write("car Type-------------->" + Top10car.vehicalType + " ----- Count --- " + Top10car.allVehicalCount + "</br>");
            //        pieChartData.Value += "['" + Top10car.vehicalType + "'," + Top10car.allVehicalCount + "],";
            //    }
            //}
            //if (pieChartData.Value.Length > 0)
            //    pieChartData.Value = pieChartData.Value.Remove(pieChartData.Value.Length - 1);
            //pieChartData.Value += "]";
        }

        else if (storedProcedure == "PickLocation")
        {
            //Response.Write(storedProcedure);
            string VehicleType;
            int quantity = 0;
            //string cartTypeName = "";



            using (var command = new SqlCommand("Select_PickupLoc", conn))
            {
                command.CommandType = CommandType.StoredProcedure;
                // command.Parameters.Add("@SiteLocation", SqlDbType.NVarChar).Value = HttpContext.Current.Server.HtmlEncode(langCode);
                conn.Open();
                var sqlReader = command.ExecuteReader();
                if (sqlReader.HasRows)
                {
                    pieChartData.Value = "[";
                    while (sqlReader.Read())
                    {
                        VehicleType = sqlReader["Location"].ToString();
                        quantity = Convert.ToInt32(sqlReader["Pickup_Count"]);


                        if (!String.IsNullOrEmpty(VehicleType))
                        {
                            //  Response.Write("car Type-------------->" + carType + " ----- Count --- " + Accessories.allaccessoryCount + "</br>");
                            pieChartData.Value += "['" + VehicleType + "'," + quantity + "],";
                        }
                    }
                    if (pieChartData.Value.Length > 0)
                        pieChartData.Value = pieChartData.Value.Remove(pieChartData.Value.Length - 1);
                    pieChartData.Value += "]";
                    //Response.Write("PickLocation" + pieChartData.Value + "<br>");
                }
            }
            conn.Close();
            //db = new DatabaseEntities();

            //var PickUpLocations = (from o in db.Orders
            //                       join l in db.Locations on o.PickUpLocation equals l.LocationCode //l.ExtendedLocationCode
            //                       where o.CameFrom == partnerName 
            //                       group l by l.LocationID into g //  l.LocationName into g
            //                       orderby g.Count() descending
            //                       select new
            //                       {

            //                           LocationName = g.FirstOrDefault().LocationName,
            //                           allVehicalCount = g.Count()
            //                       }
            //                             ).Take(10);

            //pieChartData.Value = "[";
            ////Response.Write(PickUpLocations.Count());
            //if (PickUpLocations.Count() > 0)
            //{
            //    foreach (var Top10car in PickUpLocations)
            //    {
            //         // Response.Write("car Type-------------->" + Top10car.LocationName + " ----- Count --- " + Top10car.allVehicalCount + "</br>");
            //        pieChartData.Value += "['" + Top10car.LocationName.Replace("'","`") + "'," + Top10car.allVehicalCount + "],";
            //    }
            //}
            //if (pieChartData.Value.Length > 0)
            //    pieChartData.Value = pieChartData.Value.Remove(pieChartData.Value.Length - 1);
            //pieChartData.Value += "]";
        }
        else if (storedProcedure == "DropOffLocation")
        {
            //Response.Write(storedProcedure);
            string VehicleType;
            int quantity = 0;
            //string cartTypeName = "";



            using (var command = new SqlCommand("Select_DropupLoc", conn))
            {
                command.CommandType = CommandType.StoredProcedure;
                // command.Parameters.Add("@SiteLocation", SqlDbType.NVarChar).Value = HttpContext.Current.Server.HtmlEncode(langCode);
                conn.Open();
                var sqlReader = command.ExecuteReader();

                if (sqlReader.HasRows)
                {
                    pieChartData.Value = "[";
                    while (sqlReader.Read())
                    {
                        VehicleType = sqlReader["Location"].ToString();
                        quantity = Convert.ToInt32(sqlReader["Dropoff_Count"]);


                        if (!String.IsNullOrEmpty(VehicleType))
                        {
                            //  Response.Write("car Type-------------->" + carType + " ----- Count --- " + Accessories.allaccessoryCount + "</br>");
                            pieChartData.Value += "['" + VehicleType + "'," + quantity + "],";
                        }
                    }
                    if (pieChartData.Value.Length > 0)
                        pieChartData.Value = pieChartData.Value.Remove(pieChartData.Value.Length - 1);
                    pieChartData.Value += "]";
                    //Response.Write("DropOffLocation" + pieChartData.Value + "<br>");
                }
            }
            conn.Close();
            //db = new DatabaseEntities();

            //var PickUpLocations = (from o in db.Orders
            //                       join l in db.Locations on o.DropOffLocation equals l.LocationCode // l.ExtendedLocationCode
            //                       where o.CameFrom == partnerName
            //                       group l by l.LocationID into g
            //                       orderby g.Count() descending
            //                       select new
            //                       {

            //                           LocationName = g.FirstOrDefault().LocationName,
            //                           allVehicalCount = g.Count()
            //                       }
            //                             ).Take(10);

            //pieChartData.Value = "[";
            ////Response.Write(PickUpLocations.Count());
            //if (PickUpLocations.Count() > 0)
            //{
            //    foreach (var Top10car in PickUpLocations)
            //    {
            //        // Response.Write("car Type-------------->" + Top10car.LocationName + " ----- Count --- " + Top10car.allVehicalCount + "</br>");
            //        pieChartData.Value += "['" + Top10car.LocationName.Replace("'","`") + "'," + Top10car.allVehicalCount + "],";
            //    }
            //}
            //if (pieChartData.Value.Length > 0)
            //    pieChartData.Value = pieChartData.Value.Remove(pieChartData.Value.Length - 1);
            //pieChartData.Value += "]";
        }
            
    }

    protected void ProductSalesReport_Click(object sender, EventArgs e)
    {
        string partnerName = PartnerID.SelectedItem.ToString();
        //pass through the start and end dates for our query and produce an excel sheet - using a grid view so that we can use the gridview export to excel option

        DateTime startDate = Convert.ToDateTime(StartDate.Text + " 00:00:00");
        DateTime endDate = Convert.ToDateTime(EndDate.Text + " 23:59:59");
        db = new DatabaseEntities();

        // var orders = db.Orders.AsEnumerable().Where(o => startDate.Date <= Convert.ToDateTime(o.OrderDate).Date && endDate.Date >= Convert.ToDateTime(o.OrderDate).Date).OrderBy(o => Convert.ToDateTime(o.OrderDate).Date);
        var orders = (from o in db.Orders
                      where startDate <= o.OrderDate && endDate.Date >= o.OrderDate && o.CameFrom == partnerName 
                      orderby o.OrderDate
                      select new
                      {
                          o.CustomerName,o.CustomerEmail,o.CustomerPhone,o.CarBooked,o.VehicleType,o.CountryOfResidence,o.LanguageCode,o.PickUpDate,o.DropOffDate,
                          o.OrderDate,o.PickUpLocation,o.DropOffLocation,o.NoDaysHired,o.AmountPaid,o.AmountPaidCurrency,o.CameFrom,o.IncludeInEshot,o.HertzNum1,
                          o.XML_Provider_MarginShare,o.XML_Provider,o.CancelledBooking,o.XML_Provider_PickupSupplier,o.XML_Provider_DropoffSupplier,o.Modified
                      });
        //Response.Write("order count-------------->" + orders.Count());

        if (orders.Count() == 0)
        {
            Response.Redirect("home.aspx?partnerId=" + PartnerID.SelectedValue + "&partnerName=" + PartnerID.SelectedItem.ToString() + "#tabs1-6");
            ProductSalesMsg.Text = "<p>Sorry, there were no bookings within that period. Please try again.</p>";
            ProductSalesMsg.Visible = true;
        }


        else
        {
            // store the details in the gridview
            this.ProductSalesGV.DataSource = orders.ToList() ;
            this.ProductSalesGV.AllowPaging = false;
            this.ProductSalesGV.DataBind();


            // take the values in the gridview, convert to CSV and zip it before sending to the end user
            StringBuilder csvData = new StringBuilder();

            // Write columns
            csvData.Append(ProductSalesGV.Columns[0].HeaderText);
            for (int i = 1; i < ProductSalesGV.Columns.Count; i++)
                csvData.Append("," + ProductSalesGV.Columns[i].HeaderText);
            csvData.Append("\n");

            // Write values - excludes any headers
            for (int x = 0; x < ProductSalesGV.Rows.Count; x++)
            {
                csvData.Append(ProductSalesGV.Rows[x].Cells[0].Text.Replace(",", ""));
                for (int i = 1; i < ProductSalesGV.Rows[x].Cells.Count; i++)
                    csvData.Append("," + Server.HtmlDecode(ProductSalesGV.Rows[x].Cells[i].Text).Replace(",", "").Replace("&nbsp;", "")).Replace("&euro;", "€").Replace("&pound;", "£").Replace("&#36;", "$");

                csvData.AppendLine();
            }

            Response.Clear();
            string filename = "BookingReport_" + DateTime.Now.ToString("yyyyMMdd") + ".zip";
            Response.ContentType = "application/zip";
            Response.AddHeader("content-disposition", "filename=" + filename);

            using (ZipFile zip = new ZipFile())
            {
                zip.AddEntry("BookingReport_" + DateTime.Now.ToString("yyyyMMdd") + ".csv", csvData.ToString());
                zip.Save(Response.OutputStream);
            }

            Response.End();
        }

    }

    //get all dates within a period
    private List<DateTime> GetDateRange(DateTime StartingDate, DateTime EndingDate)
    {
        if (StartingDate > EndingDate)
        {
            return null;
        }
        List<DateTime> rv = new List<DateTime>();
        DateTime tmpDate = StartingDate;
        do
        {
            rv.Add(tmpDate);
            tmpDate = tmpDate.AddDays(1);
        } while (tmpDate <= EndingDate);
        return rv;
    }

    //get all months within a period
    private List<DateTime> GetMonthRange(DateTime StartingDate, DateTime EndingDate)
    {
        if (StartingDate > EndingDate)
        {
            return null;
        }
        List<DateTime> rv = new List<DateTime>();
        DateTime tmpDate = StartingDate;
        do
        {
            rv.Add(tmpDate);
            tmpDate = tmpDate.AddMonths(1);
        } while (tmpDate <= EndingDate);
        return rv;
    }

    //method to return the correct chart value
    protected string ChartValue(List<ReportData> reportData, DateTime date)
    {
        var matchingProviderDate = reportData.Find(p => p.date.ToString("yyyy/MM/dd", System.Globalization.CultureInfo.InvariantCulture) == date.ToString("yyyy/MM/dd", System.Globalization.CultureInfo.InvariantCulture));
        if (matchingProviderDate != null)
        {
            return matchingProviderDate.numberOrders + ",";
        }
        else
        {
            return "0,";
        }
    }

    protected void LoadPartners()
    {
        db = new DatabaseEntities();
        PartnerID.DataSource = db.Partners.OrderBy(c => c.PartnerId).ToList();
        PartnerID.DataValueField = "PartnerId";
        PartnerID.DataTextField = "DomainName";
        PartnerID.DataBind();
        PartnerID.Items.Insert(0, new ListItem("Choose a Partner", "0"));
        //PartnerID.SelectedValue = PartnerID.ToString();
    }

    public void LoadChart(string partnerName)
    {     
            //30day chart
        GenerateDailySalesChart("AutoEurope", DateTime.Now.AddDays(-29), DateTime.Now, TotalSales30Days, TotalHertzSales30Days, TotalThriftySales30Days, TotalFireFlyHSales30Days, TotalDollarSales30Days, HertzSalesOver30Days, ThriftySalesOver30Days, FireFlyHSalesOver30Days, DollarSalesOver30Days, partnerName);
            //3month chart
            DateTime monthlyEndDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).AddMonths(1).AddDays(-1);
            GenerateMonthlySalesCharts("AutoEurope", DateTime.Now.AddMonths(-2), monthlyEndDate, TotalSales3Months, TotalHertzSales3Months, TotalThriftySales3Months, TotalFireFlyAESales3Months, TotalFireFlyHSales3Months, TotalDollarSales3Months, HertzSalesOver3Months, ThriftySalesOver3Months, FireFlyAESalesOver3Months, FireFlyHSalesOver3Months, DollarSalesOver3Months, MonthsSelected3Months,partnerName);
            //6month chart
            GenerateMonthlySalesCharts("AutoEurope", DateTime.Now.AddMonths(-5), monthlyEndDate, TotalSales6Months, TotalHertzSales6Months, TotalThriftySales6Months, TotalFireFlyAESales6Months, TotalFireFlyHSales6Months, TotalDollarSales6Months, HertzSalesOver6Months, ThriftySalesOver6Months, FireFlyAESalesOver6Months, FireFlyHSalesOver6Months, DollarSalesOver6Months, MonthsSelected6Months, partnerName);
            //12month chart
            GenerateMonthlySalesCharts("AutoEurope", DateTime.Now.AddMonths(-11), monthlyEndDate, TotalSales12Months, TotalHertzSales12Months, TotalThriftySales12Months, TotalFireFlyAESales12Months, TotalFireFlyHSales12Months, TotalDollarSales12Months, HertzSalesOver12Months, ThriftySalesOver12Months, FireFlyAESalesOver12Months, FireFlyHSalesOver12Months, DollarSalesOver12Months, MonthsSelected12Months, partnerName);
            //top selling accessories pie chart
            GeneratePieChart("SellingAccessories", "Accessory", "ItemsSold", AccessoryValues, partnerName);
            //top selling car types pie chart
            GeneratePieChart("VehicleType", "VehicleType", "ItemsSold", CarClassValues, partnerName);
            //top pick up locations pie chart
            GeneratePieChart("PickLocation", "PickUpLocation", "NumTimesPickedUp", PickupLocationValues, partnerName);
            //top drop off locations pie chart
            GeneratePieChart("DropOffLocation", "DropOffLocation", "NumTimesDroppedOff", DropoffLocationValues, partnerName);
            ProductSalesMsg.Visible = true;
            ProductSalesMsg.Text = "<p id='ProductSalesMsg'>Sorry, there were no bookings within that period. Please try again.</p>";
            Page.ClientScript.RegisterStartupScript(this.GetType(), "checkHashTag", "showValidationMessage();", true);

    }



    public void PageRefresh(object sender, EventArgs e)
    {
        Response.Redirect("home.aspx?partnerId=" + PartnerID.SelectedValue + "&partnerName=" + PartnerID.SelectedItem.ToString() + "");
    }



    public void LoadRequestTimeoutChart()
    {

        //  //30day chart

        GenerateDailyRequestTimeoutChart(DateTime.Now.AddDays(-29), DateTime.Now, TotalTimeout30Days, TotalHertzTimeout30Days, TotalThriftyTimeout30Days, TotalFireFlyTimeout30Days, TotalDollarTimeout30Days, HertzTimeout30Days, ThriftyTimeout30Days, FireFlyTimeout30Days, DollarTimeout30Days);


        DateTime monthlyEndDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).AddMonths(1).AddDays(-1);

        GenerateMonthlyRequestTimeoutCharts(DateTime.Now.AddMonths(-2), monthlyEndDate, TotalTimeout3Months, TotalHertzTimeout3Months, TotalThriftyTimeout3Months, TotalFireFlyTimeout3Months, TotalDollarTimeout3Months, HertzTimeout3Months, ThriftyTimeout3Months, FireFlyTimeout3Months, DollarTimeout3Months, Timeout3MonthSelected);

        GenerateMonthlyRequestTimeoutCharts(DateTime.Now.AddMonths(-5), monthlyEndDate, TotalTimeout6Months, TotalHertzTimeout6Months, TotalThriftyTimeout6Months, TotalFireFlyTimeout6Months, TotalDollarTimeout6Months, HertzTimeout6Months, ThriftyTimeout6Months, FireFlyTimeout6Months, DollarTimeout6Months, Timeout6MonthSelected);

        GenerateMonthlyRequestTimeoutCharts(DateTime.Now.AddMonths(-11), monthlyEndDate, TotalTimeout12Months, TotalHertzTimeout12Months, TotalThriftyTimeout12Months, TotalFireFlyTimeout12Months, TotalDollarTimeout12Months, HertzTimeout12Months, ThriftyTimeout12Months, FireFlyTimeout12Months, DollarTimeout12Months, Timeout12MonthSelected);



        ProductSalesMsg.Visible = true;
        ProductSalesMsg.Text = "<p id='ProductSalesMsg'>Sorry, there were no bookings within that period. Please try again.</p>";
        Page.ClientScript.RegisterStartupScript(this.GetType(), "checkHashTag", "showValidationMessage();", true);

    }


    protected void GenerateDailyRequestTimeoutChart(DateTime from, DateTime to, HiddenField TotalTimeout, HiddenField TotalHertzTimeout, HiddenField TotalThriftyTimeout, HiddenField TotalFireflyTimeout, HiddenField TotalDollarTimeout, HiddenField HertzTimeout, HiddenField ThriftyTimeout, HiddenField FireflyTimeout, HiddenField DollarTimeout)
    {
        DateTime startDate = from;
        DateTime endDate = to;
        List<ReportData> reportDataHertz = new List<ReportData>();
        List<ReportData> reportDataThrifty = new List<ReportData>();
        List<ReportData> reportDataFireflyH = new List<ReportData>();
        List<ReportData> reportDataDollar = new List<ReportData>();
        int TotalNumOrders = 0;
        int TotalHertzOrders = 0;
        int TotalThriftyOrders = 0;
        int TotalFireflyOrders = 0;
        int TotalDollarOrders = 0;
        db = new DatabaseEntities();

        //Response.Write(startDate + " " + endDate + "</br>");
        var RQTimeout = db.RequestTimeOutLogs.AsEnumerable().Where(r => startDate.Date <= Convert.ToDateTime(r.LogDate).Date && endDate.Date >= Convert.ToDateTime(r.LogDate).Date).OrderBy(r => Convert.ToDateTime(r.LogDate).Date).GroupBy(r => new { Convert.ToDateTime(r.LogDate).Date, r.Brand });


        foreach (var reqTimeout in RQTimeout)
        {
            //Response.Write("Provider---->" + order.FirstOrDefault().XML_Provider +"Date-------->"+order.FirstOrDefault().OrderDate+ "</br>");
            if (reqTimeout.FirstOrDefault().Brand == "Hertz")
            {
                //Response.Write("Hertz Date-------------->" + Convert.ToDateTime(reqTimeout.FirstOrDefault().LogDate).Year + "/" + Convert.ToDateTime(reqTimeout.FirstOrDefault().LogDate).Month + "/" + Convert.ToDateTime(reqTimeout.FirstOrDefault().LogDate).Day + "Count--------------->" + reqTimeout.Count() + "</br>");
                TotalNumOrders += reqTimeout.Count();
                TotalHertzOrders += reqTimeout.Count();
                reportDataHertz.Add(new ReportData { date = Convert.ToDateTime(Convert.ToDateTime(reqTimeout.FirstOrDefault().LogDate).Year + "/" + Convert.ToDateTime(reqTimeout.FirstOrDefault().LogDate).Month + "/" + Convert.ToDateTime(reqTimeout.FirstOrDefault().LogDate).Day), numberOrders = reqTimeout.Count().ToString() });
            }
            else if (reqTimeout.FirstOrDefault().Brand == "Thrifty")
            {

                //Response.Write("Thrifty Date-------------->" + Convert.ToDateTime(reqTimeout.FirstOrDefault().LogDate).Year + "/" + Convert.ToDateTime(reqTimeout.FirstOrDefault().LogDate).Month + "/" + Convert.ToDateTime(reqTimeout.FirstOrDefault().LogDate).Day + "Count--------------->" + reqTimeout.Count() + "</br>");
                TotalNumOrders += reqTimeout.Count();
                TotalThriftyOrders += reqTimeout.Count();
                reportDataThrifty.Add(new ReportData { date = Convert.ToDateTime(Convert.ToDateTime(reqTimeout.FirstOrDefault().LogDate).Year + "/" + Convert.ToDateTime(reqTimeout.FirstOrDefault().LogDate).Month + "/" + Convert.ToDateTime(reqTimeout.FirstOrDefault().LogDate).Day), numberOrders = reqTimeout.Count().ToString() });
            }
            else if (reqTimeout.FirstOrDefault().Brand == "FireFly")
            {
                //Response.Write("FireFly Date-------------->" + Convert.ToDateTime(reqTimeout.FirstOrDefault().LogDate).Year + "/" + Convert.ToDateTime(reqTimeout.FirstOrDefault().LogDate).Month + "/" + Convert.ToDateTime(reqTimeout.FirstOrDefault().LogDate).Day + "Count--------------->" + reqTimeout.Count() + "</br>");
                TotalNumOrders += reqTimeout.Count();
                TotalFireflyOrders += reqTimeout.Count();
                reportDataFireflyH.Add(new ReportData { date = Convert.ToDateTime(Convert.ToDateTime(reqTimeout.FirstOrDefault().LogDate).Year + "/" + Convert.ToDateTime(reqTimeout.FirstOrDefault().LogDate).Month + "/" + Convert.ToDateTime(reqTimeout.FirstOrDefault().LogDate).Day), numberOrders = reqTimeout.Count().ToString() });
            }
            else if (reqTimeout.FirstOrDefault().Brand == "Dollar")
            {
                //Response.Write("FireFly Date-------------->" + Convert.ToDateTime(reqTimeout.FirstOrDefault().LogDate).Year + "/" + Convert.ToDateTime(reqTimeout.FirstOrDefault().LogDate).Month + "/" + Convert.ToDateTime(reqTimeout.FirstOrDefault().LogDate).Day + "Count--------------->" + reqTimeout.Count() + "</br>");
                TotalNumOrders += reqTimeout.Count();
                TotalDollarOrders += reqTimeout.Count();
                reportDataDollar.Add(new ReportData { date = Convert.ToDateTime(Convert.ToDateTime(reqTimeout.FirstOrDefault().LogDate).Year + "/" + Convert.ToDateTime(reqTimeout.FirstOrDefault().LogDate).Month + "/" + Convert.ToDateTime(reqTimeout.FirstOrDefault().LogDate).Day), numberOrders = reqTimeout.Count().ToString() });
            }
        }


        //put the total order figures into relevant hidden fields
        TotalTimeout.Value = TotalNumOrders.ToString();
        TotalHertzTimeout.Value = TotalHertzOrders.ToString();
        TotalThriftyTimeout.Value = TotalThriftyOrders.ToString();
        TotalFireflyTimeout.Value = TotalFireflyOrders.ToString();
        TotalDollarTimeout.Value = TotalDollarOrders.ToString();

        //go through all dates from start to finish. If any of these dates are missing from our database values put in a zero
        foreach (var date in GetDateRange(startDate, endDate))
        {
            //handle hertz details
            HertzTimeout.Value += ChartValue(reportDataHertz, date);
            //handle thrifty details
            ThriftyTimeout.Value += ChartValue(reportDataThrifty, date);
            //handle FireFlyAE details
            FireflyTimeout.Value += ChartValue(reportDataFireflyH, date);
            //handle Dollar details
            DollarTimeout.Value += ChartValue(reportDataDollar, date);
        }
        //remove the last coma in the results data
        HertzTimeout.Value = HertzTimeout.Value.Remove(HertzTimeout.Value.Length - 1);
        //Response.Write(HertzTimeout.Value+"</br>");
        ThriftyTimeout.Value = ThriftyTimeout.Value.Remove(ThriftyTimeout.Value.Length - 1);
        // Response.Write(ThriftyTimeout.Value + "</br>");  
        FireflyTimeout.Value = FireflyTimeout.Value.Remove(FireflyTimeout.Value.Length - 1);

        DollarTimeout.Value = DollarTimeout.Value.Remove(DollarTimeout.Value.Length - 1);
    }


    protected void GenerateMonthlyRequestTimeoutCharts(DateTime from, DateTime to, HiddenField TotalTimeout, HiddenField TotalHertzTimeout, HiddenField TotalThriftyTimeout, HiddenField TotalFireflyTimeout, HiddenField TotalDollarTimeout, HiddenField HertzTimeout, HiddenField ThriftyTimeout, HiddenField FireflyTimeout, HiddenField DollarTimeout, HiddenField MonthsSelected)
    {
        DateTime startDate = new DateTime(from.Year, from.Month, 1);
        DateTime endDate = to;
        //TimeSpan startTS = new TimeSpan(0, 0, 0);
        //TimeSpan endTS = new TimeSpan(23, 59, 59);
        List<ReportData> reportDataHertz = new List<ReportData>();
        List<ReportData> reportDataThrifty = new List<ReportData>();
        List<ReportData> reportDataFireflyAE = new List<ReportData>();
        List<ReportData> reportDataDollar = new List<ReportData>();

        int TotalNumOrders = 0;
        int TotalHertzOrders = 0;
        int TotalThriftyOrders = 0;
        int TotalFireflyHOrders = 0;
        int TotalDollarOrders = 0;
        db = new DatabaseEntities();




        var RQTimeout = db.RequestTimeOutLogs.AsEnumerable().Where(r => startDate.Date <= Convert.ToDateTime(r.LogDate).Date && endDate.Date >= Convert.ToDateTime(r.LogDate).Date).OrderBy(r => Convert.ToDateTime(r.LogDate).Month).GroupBy(r => new { Convert.ToDateTime(r.LogDate).Month, r.Brand });



        foreach (var requestTimeout in RQTimeout)
        {
            if (requestTimeout.FirstOrDefault().Brand == "Hertz")
            {
                // Response.Write("Date-------------->" + Convert.ToDateTime(order.FirstOrDefault().OrderDate).Year + "/" + Convert.ToDateTime(order.FirstOrDefault().OrderDate).Month + "/01" + "Count--------------->" + order.Count() + "</br>");
                TotalNumOrders += requestTimeout.Count();
                TotalHertzOrders += requestTimeout.Count();
                reportDataHertz.Add(new ReportData { date = Convert.ToDateTime(Convert.ToDateTime(requestTimeout.FirstOrDefault().LogDate).Year + "/" + Convert.ToDateTime(requestTimeout.FirstOrDefault().LogDate).Month + "/01"), numberOrders = requestTimeout.Count().ToString() });
            }
            else if (requestTimeout.FirstOrDefault().Brand == "Thrifty")
            {

                //  Response.Write("Date-------------->" + Convert.ToDateTime(order.FirstOrDefault().OrderDate).Year + "/" + Convert.ToDateTime(order.FirstOrDefault().OrderDate).Month + "/01" + "Count--------------->" + order.Count() + "</br>");
                TotalNumOrders += requestTimeout.Count();
                TotalThriftyOrders += requestTimeout.Count();
                reportDataThrifty.Add(new ReportData { date = Convert.ToDateTime(Convert.ToDateTime(requestTimeout.FirstOrDefault().LogDate).Year + "/" + Convert.ToDateTime(requestTimeout.FirstOrDefault().LogDate).Month + "/01"), numberOrders = requestTimeout.Count().ToString() });
            }
            else if (requestTimeout.FirstOrDefault().Brand == "FireFly")
            {
                // Response.Write("Date-------------->" + Convert.ToDateTime(requestTimeout.FirstOrDefault().LogDate).Year + "/" + Convert.ToDateTime(requestTimeout.FirstOrDefault().LogDate).Month + "/01" + "Count--------------->" + requestTimeout.Count() + "</br>");
                TotalNumOrders += requestTimeout.Count();
                TotalFireflyHOrders += requestTimeout.Count();

                reportDataFireflyAE.Add(new ReportData { date = Convert.ToDateTime(Convert.ToDateTime(requestTimeout.FirstOrDefault().LogDate).Year + "/" + Convert.ToDateTime(requestTimeout.FirstOrDefault().LogDate).Month + "/01"), numberOrders = requestTimeout.Count().ToString() });
            }
            else if (requestTimeout.FirstOrDefault().Brand == "Dollar")
            {
                // Response.Write("Date-------------->" + Convert.ToDateTime(requestTimeout.FirstOrDefault().LogDate).Year + "/" + Convert.ToDateTime(requestTimeout.FirstOrDefault().LogDate).Month + "/01" + "Count--------------->" + requestTimeout.Count() + "</br>");
                TotalNumOrders += requestTimeout.Count();
                TotalDollarOrders += requestTimeout.Count();

                reportDataDollar.Add(new ReportData { date = Convert.ToDateTime(Convert.ToDateTime(requestTimeout.FirstOrDefault().LogDate).Year + "/" + Convert.ToDateTime(requestTimeout.FirstOrDefault().LogDate).Month + "/01"), numberOrders = requestTimeout.Count().ToString() });
            }
        }

        //put the total order figures into relevant hidden fields
        TotalTimeout.Value = Convert.ToInt32(TotalNumOrders.ToString()).ToString("N0");
        TotalHertzTimeout.Value = Convert.ToInt32(TotalHertzOrders.ToString()).ToString("N0");
        TotalThriftyTimeout.Value = Convert.ToInt32(TotalThriftyOrders.ToString()).ToString("N0");
        TotalFireflyTimeout.Value = Convert.ToInt32(TotalFireflyHOrders.ToString()).ToString("N0");
        TotalDollarTimeout.Value = Convert.ToInt32(TotalDollarOrders.ToString()).ToString("N0");

        //go through all dates from start to finish. If any of these dates are missing from our database values put in a zero
        foreach (var date in GetMonthRange(startDate, endDate))
        {

            //handle hertz details
            HertzTimeout.Value += ChartValue(reportDataHertz, date);

            //handle thrifty details
            ThriftyTimeout.Value += ChartValue(reportDataThrifty, date);

            //handle FireFlyAE details
            FireflyTimeout.Value += ChartValue(reportDataFireflyAE, date);

            //handle FireFlyAE details
            DollarTimeout.Value += ChartValue(reportDataDollar, date);

            //put the month name into the container for the chart X axis values
            MonthsSelected.Value += date.ToString("MMM", System.Globalization.CultureInfo.InvariantCulture) + ",";
        }

        //remove the last coma in the results data
        MonthsSelected.Value = MonthsSelected.Value.Remove(MonthsSelected.Value.Length - 1);
        HertzTimeout.Value = HertzTimeout.Value.Remove(HertzTimeout.Value.Length - 1);
        ThriftyTimeout.Value = ThriftyTimeout.Value.Remove(ThriftyTimeout.Value.Length - 1);
        FireflyTimeout.Value = FireflyTimeout.Value.Remove(FireflyTimeout.Value.Length - 1);
        DollarTimeout.Value = DollarTimeout.Value.Remove(DollarTimeout.Value.Length - 1);
    }



}
