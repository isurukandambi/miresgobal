﻿using System;
using System.Linq;
using System.Text;
using System.Web;
using ORM;
using CMSConfigs;

public partial class updateLocation : System.Web.UI.Page
{
    private DatabaseEntities db;

    protected void Page_Load(object sender, EventArgs e)
    {
        Master.Page.Title = CMSSettings.SiteTitle + " - " + PageName.Text;
        if (Session["LoggedInName"] == null)
        {
            Response.Redirect("default.aspx?TimeOut=y&ReturnPage=" + Server.UrlEncode(HttpContext.Current.Request.Url.AbsoluteUri));
        }
        if(!Page.IsPostBack){
            LoadCountries();
            LoadData();
            MainView.ActiveViewIndex = 0;
        }
    }
    protected void LoadCountries()
    {
        db = new DatabaseEntities();
        CountryID.DataSource = db.Countries.OrderBy(c => c.CountryName).ToList();
        CountryID.DataValueField = "CountryID";
        CountryID.DataTextField = "CountryName";
        CountryID.DataBind();
    }
    protected void LoadData() {
        db = new DatabaseEntities();
        int row = rowID();
        var location = db.Locations.SingleOrDefault(l => l.LocationID == row);
        if (location != null)
        {
            CountryID.SelectedValue = location.CountryIDFK.ToString();
            LocationName.Text = location.LocationName.Trim();
            LocationCode.Text = location.LocationCode.Trim();
            ExtendedLocationCode.Text = location.ExtendedLocationCode.Trim();
            if (location.City != null)
            {
                City.Text = location.City.Trim();
            }
            if (location.Address_Line1 != null)
            {
                Address1.Text = location.Address_Line1.Trim();
            }
            if (location.Address_Line2 != null)
            {
                Address2.Text = location.Address_Line2.Trim();
            }
            if (location.Address_Line3 != null)
            {
                Address3.Text = location.Address_Line3.Trim();
            }
            if (location.Zip_PostCode != null)
            {
                PostCode.Text = location.Zip_PostCode.Trim();
            }
            if (location.Phone != null)
            {
                Phone.Text = location.Phone.Trim();
            }
            if (location.Latitude != null)
            {
                Latitude.Text = location.Latitude.Trim();
            }
            if (location.Longitude != null)
            {
                Longitude.Text = location.Longitude.Trim();
            }
            if (location.Open1Mon != null)
            {
                Open1Mon.Text = location.Open1Mon.Trim();
            }
            if (location.Close1Mon != null)
            {
                Close1Mon.Text = location.Close1Mon.Trim();
            }
            if (location.Open2Mon != null)
            {
                Open2Mon.Text = location.Open2Mon.Trim();
            }
            if (location.Close2Mon != null)
            {
                Close2Mon.Text = location.Close2Mon.Trim();
            }
            if (location.Open3Mon != null)
            {
                Open3Mon.Text = location.Open3Mon.Trim();
            }
            if (location.Close3Mon != null)
            {
                Close3Mon.Text = location.Close3Mon.Trim();
            }
            if (location.Open1Tue != null)
            {
                Open1Tue.Text = location.Open1Tue.Trim();
            }
            if (location.Close1Tue != null)
            {
                Close1Tue.Text = location.Close1Tue.Trim();
            }
            if (location.Open2Tue != null)
            {
                Open2Tue.Text = location.Open2Tue.Trim();
            }
            if (location.Close2Tue != null)
            {
                Close2Tue.Text = location.Close2Tue.Trim();
            }
            if (location.Open3Tue != null)
            {
                Open3Tue.Text = location.Open3Tue.Trim();
            }
            if (location.Close3Tue != null)
            {
                Close3Tue.Text = location.Close3Tue.Trim();
            }
            if (location.Open1Wed != null)
            {
                Open1Wed.Text = location.Open1Wed.Trim();
            }
            if (location.Close1Wed != null)
            {
                Close1Wed.Text = location.Close1Wed.Trim();
            }
            if (location.Open2Wed != null)
            {
                Open2Wed.Text = location.Open2Wed.Trim();
            }
            if (location.Close2Wed != null)
            {
                Close2Wed.Text = location.Close2Wed.Trim();
            }
            if (location.Open3Wed != null)
            {
                Open3Wed.Text = location.Open3Wed.Trim();
            }
            if (location.Close3Wed != null)
            {
                Close3Wed.Text = location.Close3Wed.Trim();
            }
            if (location.Open1Thu != null)
            {
                Open1Thu.Text = location.Open1Thu.Trim();
            }
            if (location.Close1Thu != null)
            {
                Close1Thu.Text = location.Close1Thu.Trim();
            }
            if (location.Open2Thu != null)
            {
                Open2Thu.Text = location.Open2Thu.Trim();
            }
            if (location.Close2Thu != null)
            {
                Close2Thu.Text = location.Close2Thu.Trim();
            }
            if (location.Open3Thu != null)
            {
                Open3Thu.Text = location.Open3Thu.Trim();
            }
            if (location.Close3Thu != null)
            {
                Close3Thu.Text = location.Close3Thu.Trim();
            }
            if (location.Open1Fri != null)
            {
                Open1Fri.Text = location.Open1Fri.Trim();
            }
            if (location.Close1Fri != null)
            {
                Close1Fri.Text = location.Close1Fri.Trim();
            }
            if (location.Open2Fri != null)
            {
                Open2Fri.Text = location.Open2Fri.Trim();
            }
            if (location.Close2Fri != null)
            {
                Close2Fri.Text = location.Close2Fri.Trim();
            }
            if (location.Open3Fri != null)
            {
                Open3Fri.Text = location.Open3Fri.Trim();
            }
            if (location.Close3Fri != null)
            {
                Close3Fri.Text = location.Close3Fri.Trim();
            }
            if (location.Open1Sat != null)
            {
                Open1Sat.Text = location.Open1Sat.Trim();
            }
            if (location.Close1Sat != null)
            {
                Close1Sat.Text = location.Close1Sat.Trim();
            }
            if (location.Open2Sat != null)
            {
                Open2Sat.Text = location.Open2Sat.Trim();
            }
            if (location.Close2Sat != null)
            {
                Close2Sat.Text = location.Close2Sat.Trim();
            }
            if (location.Open3Sat != null)
            {
                Open3Sat.Text = location.Open3Sat.Trim();
            }
            if (location.Close3Sat != null)
            {
                Close3Sat.Text = location.Close3Sat.Trim();
            }
            if (location.Open1Sun != null)
            {
                Open1Sun.Text = location.Open1Sun.Trim();
            }
            if (location.Close1Sun != null)
            {
                Close1Sun.Text = location.Close1Sun.Trim();
            }
            if (location.Open2Sun != null)
            {
                Open2Sun.Text = location.Open2Sun.Trim();
            }
            if (location.Close2Sun != null)
            {
                Close2Sun.Text = location.Close2Sun.Trim();
            }
            if (location.Open3Sun != null)
            {
                Open3Sun.Text = location.Open3Sun.Trim();
            }
            if (location.Close3Sun != null)
            {
                Close3Sun.Text = location.Close3Sun.Trim();
            }
            if (location.CTLocationId != null)
            {
                CarTrawlerLocCode.Text = location.CTLocationId.ToString();
            }
            if (location.ThermeonLocation != null)
            {
                ThermeonLocCode.Text = location.ThermeonLocation.Trim();
            }
            if (location.PREFERRED_OAG != null)
            {
                Preferred_OAG.Text = location.PREFERRED_OAG.Trim();
            }
            if (location.STATEPROVINCE_CODE != null)
            {
                StateProviceCode.Text = location.STATEPROVINCE_CODE.Trim();
            }
            if (location.ISO_STATEPROVINCE_CODE != null)
            {
                IsoStateCode.Text = location.ISO_STATEPROVINCE_CODE.Trim();
            }
            if (location.NUMBER != null)
            {
                Number.Text = location.NUMBER.Trim();
            }
            if (location.ALT_PHONE != null)
            {
                AltPhone.Text = location.ALT_PHONE.Trim();
            }
            if (location.FAX != null)
            {
                Fax.Text = location.FAX.Trim();
            }
            if (location.TELEX != null)
            {
                Telex.Text = location.TELEX.Trim();
            }
            if (location.SERVED_BY != null)
            {
                Served_By.Text = location.SERVED_BY.Trim();
            }
            if (location.EMAIL_ADDRESS != null)
            {
                Email_Add.Text = location.EMAIL_ADDRESS.Trim();
            }
            if (location.WEBSITE != null)
            {
                WebSite.Text = location.WEBSITE.Trim();
            }
            if (location.BOOKABLE != null)
            {
                Bookable.Text = location.BOOKABLE.Trim();
            }
            if (location.OPT_HLE != null)
            {
                Opt_HLE.Text = location.OPT_HLE.Trim();
            }
            if (location.OPT_INSURANCE_REPLACE != null)
            {
                Opt_Insurance_Replace.Text = location.OPT_INSURANCE_REPLACE.Trim();
            }
            if (location.OPT_NEVERLOST != null)
            {
                Opt_Neverlost.Text = location.OPT_NEVERLOST.Trim();
            }
            if (location.OPT_CHILD_SEATS != null)
            {
                Opt_Child_Seats.Text = location.OPT_CHILD_SEATS.Trim();
            }
            if (location.OPT_HAND_CONTROL != null)
            {
                Opt_Hand_Control.Text = location.OPT_HAND_CONTROL.Trim();
            }
            if (location.OPT_GOLD_CUSTOMER != null)
            {
                Opt_Gold_Customer.Text = location.OPT_GOLD_CUSTOMER.Trim();
            }
            if (location.OPT_GOLD_CANOPY != null)
            {
                Opt_Gold_Canopy.Text = location.OPT_GOLD_CANOPY.Trim();
            }
            if (location.OPT_PORTABLE_PHONES != null)
            {
                Opt_Portable_Phones.Text = location.OPT_PORTABLE_PHONES.Trim();
            }
            if (location.OPT_SKIERIZED != null)
            {
                Opt_Skierized.Text = location.OPT_SKIERIZED.Trim();
            }
            if (location.OPT_MOBILE_PHONES != null)
            {
                Opt_Mobile_Phones.Text = location.OPT_MOBILE_PHONES.Trim();
            }
            if (location.OPT_LUGGAGE_RACK != null)
            {
                Opt_Luggqage_Rack.Text = location.OPT_LUGGAGE_RACK.Trim();
            }
            if (location.OPT_PRESTIGE_SERVICE != null)
            {
                Opt_Prestige_Services.Text = location.OPT_PRESTIGE_SERVICE.Trim();
            }
            if (location.OPT_RETURN_CENTER != null)
            {
                Opt_Return_Center.Text = location.OPT_RETURN_CENTER.Trim();
            }
            if (location.OPT_HTV != null)
            {
                Opt_HTV.Text = location.OPT_HTV.Trim();
            }
            if (location.OPT_HEAVY_TRUCKS != null)
            {
                Opt_Heavy_Trucks.Text = location.OPT_HEAVY_TRUCKS.Trim();
            }
            if (location.OPT_PICKUP_SERVICE != null)
            {
                Opt_Pickup_Services.Text = location.OPT_PICKUP_SERVICE.Trim();
            }
            if (location.MAJOR_AIRPORT != null)
            {
                Major_AirPort.Text = location.MAJOR_AIRPORT.Trim();
            }
            if (location.SERVED_BY_DATA != null)
            {
                Served_BY_Data.Text = location.SERVED_BY_DATA.Trim();
            }
            if (location.ADDRESS4 != null)
            {
                Address4.Text = location.ADDRESS4.Trim();
            }
            if (location.ADDRESS5 != null)
            {
                Address5.Text = location.ADDRESS5.Trim();
            }
            if (location.CITY_ADDR != null)
            {
                CityAdd.Text = location.CITY_ADDR.Trim();
            }
            if (location.NOTES1 != null)
            {
                Notes_1.Text = location.NOTES1.Trim();
            }
            if (location.NOTES2 != null)
            {
                Notes_2.Text = location.NOTES2.Trim();
            }
            if (location.NOTES3 != null)
            {
                Notes_3.Text = location.NOTES3.Trim();
            }
            if (location.NOTES4 != null)
            {
                Notes_4.Text = location.NOTES4.Trim();
            }
            if (location.NOTES5 != null)
            {
                Notes_5.Text = location.NOTES5.Trim();
            }
            if (location.CAPITAL != null)
            {
                Capital.Text = location.CAPITAL.Trim();
            }
            if (location.STATEPROVINCE_NAME != null)
            {
                StateProvineName.Text = location.STATEPROVINCE_NAME.Trim();
            }
            if (location.CITY_VARIANTS != null)
            {
                City_Variants.Text = location.CITY_VARIANTS.Trim();
            }
            if (location.DISPLAY_ADDR != null)
            {
                Display_Add.Text = location.DISPLAY_ADDR.Trim();
            }
            if (location.DISPLAY_DL != null)
            {
                Display_Dl.Text = location.DISPLAY_DL.Trim();
            }
            if (location.DISPLAY_DEST != null)
            {
                Display_Dest.Text = location.DISPLAY_DEST.Trim();
            }
            if (location.AREA != null)
            {
                Area.Text = location.AREA.Trim();
            }
            if (location.LOCATOR_CODE != null)
            {
                Locator_Code.Text = location.LOCATOR_CODE.Trim();
            }
            if (location.TOP_LOCATION != null)
            {
                Top_Location.Text = location.TOP_LOCATION.Trim();
            }
            if (location.FBO != null)
            {
                Fbo.Text = location.FBO.Trim();
            }
            if (location.SPECIAL_INSTRUCTION != null)
            {
                Spec_Inst.Text = location.SPECIAL_INSTRUCTION.Trim();
            }
            if (location.SPECIAL_INSTRUCTION_DATA != null)
            {
                Spec_Inst_Data.Text = location.SPECIAL_INSTRUCTION_DATA.Trim();
            }
            if (location.DOUBLE_CLICK_BANNER != null)
            {
                DoubleClickBanner.Text = location.DOUBLE_CLICK_BANNER.Trim();
            }
            if (location.HZ_LOCID != null)
            {
                HZ_LocId.Text = location.HZ_LOCID.Trim();
            }
            if (location.COUNTER_BYPASS_ENABLED != null)
            {
                Counter_Bypass_Enable.Text = location.COUNTER_BYPASS_ENABLED.Trim();
            }
            if (location.OPT_WIFI != null)
            {
                Opt_Wifi.Text = location.OPT_WIFI.Trim();
            }
            if (location.OPT_PLATE_PASS != null)
            {
                Opt_Plate_Pass.Text = location.OPT_PLATE_PASS.Trim();
            }
            if (location.OPT_EXPRESS_RETURN != null)
            {
                Opt_Express_Return.Text = location.OPT_EXPRESS_RETURN.Trim();
            }
            if (location.OPT_NO_WALKINS != null)
            {
                Opt_No_Walkings.Text = location.OPT_NO_WALKINS.Trim();
            }
            if (location.OPT_AFTER_HOURS_DROP != null)
            {
                Opt_After_Hours_Drop.Text = location.OPT_AFTER_HOURS_DROP.Trim();
            }
            if (location.OPT_AFTER_HOURS_PICKUP != null)
            {
                Opt_After_Hours_Pickup.Text = location.OPT_AFTER_HOURS_PICKUP.Trim();
            }
            if (location.OPT_INFANT_SEAT != null)
            {
                Opt_Infant_Seat.Text = location.OPT_INFANT_SEAT.Trim();
            }
            if (location.OPT_BOOSTER_SEATS != null)
            {
                Opt_Boosters_Seats.Text = location.OPT_BOOSTER_SEATS.Trim();
            }
            if (location.OPT_MILITARY_LOC != null)
            {
                Opt_Military_Loc.Text = location.OPT_MILITARY_LOC.Trim();
            }
            if (location.OPT_HOTEL_GUEST_REQ != null)
            {
                Opt_Hotel_Guest_Req.Text = location.OPT_HOTEL_GUEST_REQ.Trim();
            }
            if (location.OPT_LEASING_LOC != null)
            {
                Opt_Leasing_Loc.Text = location.OPT_LEASING_LOC.Trim();
            }
            if (location.OPT_RE_FUEL != null)
            {
                Opt_RE_Fuel.Text = location.OPT_RE_FUEL.Trim();
            }
            if (location.OPT_ROADSIDE_ASSSIST != null)
            {
                Opt_Roadside_Assist.Text = location.OPT_ROADSIDE_ASSSIST.Trim();
            }
            if (location.OPT_KIOSK_LOC != null)
            {
                Opt_Kiosk_Loc.Text = location.OPT_KIOSK_LOC.Trim();
            }
            if (location.OPT_CHAFFEUR_LOC != null)
            {
                Opt_Chaffeur_Loc.Text = location.OPT_CHAFFEUR_LOC.Trim();
            }
            if (location.OPT_TRAIN_LOC != null)
            {
                Opt_Train_Loc.Text = location.OPT_TRAIN_LOC.Trim();
            }
            if (location.OPT_SHIP_LOC != null)
            {
                Opt_Ship_Loc.Text = location.OPT_SHIP_LOC.Trim();
            }
            if (location.OPT_CRUISE_LOC != null)
            {
                Opt_Cruise_Loc.Text = location.OPT_CRUISE_LOC.Trim();
            }
            if (location.OPT_BUS_LOC != null)
            {
                Opt_Bus_Loc.Text = location.OPT_BUS_LOC.Trim();
            }
            if (location.OPT_SUBWAY_LOC != null)
            {
                Opt_Subway_Loc.Text = location.OPT_SUBWAY_LOC.Trim();
            }
            if (location.OPT_RET_SERV_LOC != null)
            {
                Opt_Ret_Serv_Loc.Text = location.OPT_RET_SERV_LOC.Trim();
            }
            if (location.OPT_MEET_PF != null)
            {
                Opt_Meet_Pf.Text = location.OPT_MEET_PF.Trim();
            }
            if (location.OPT_MEET_ADV_RES != null)
            {
                Opt_Meet_Acv_Res.Text = location.OPT_MEET_ADV_RES.Trim();
            }
            if (location.OPT_FLIGHT_MAND_ALL != null)
            {
                Opt_Flight_Mand_All.Text = location.OPT_FLIGHT_MAND_ALL.Trim();
            }
            if (location.OPT_FLIGHT_MAND_PRIVATE != null)
            {
                Opt_Flight_Mand_Private.Text = location.OPT_FLIGHT_MAND_PRIVATE.Trim();
            }
            if (location.OPT_HOD != null)
            {
                Opt_HOD.Text = location.OPT_HOD.Trim();
            }
            if (location.OPT_GOLD_CHOICE != null)
            {
                Opt_Gold_Choice.Text = location.OPT_GOLD_CHOICE.Trim();
            }
            if (location.OPT_HERTZ_EQUIP != null)
            {
                Opt_Hertz_Equip.Text = location.OPT_HERTZ_EQUIP.Trim();
            }
            if (location.OPT_PRIVATE_FLIGHT_ONLY != null)
            {
                Opt_Private_Flight_Only.Text = location.OPT_PRIVATE_FLIGHT_ONLY.Trim();
            }
            if (location.DISPLAY_HLE_TEXT != null)
            {
                Display_Hle_Txt.Text = location.DISPLAY_HLE_TEXT.Trim();
            }
            if (location.OPT_MGB != null)
            {
                Opt_MGB.Text = location.OPT_MGB.Trim();
            }
            if (location.OPT_RETURN_TO_SERVICING_LOC != null)
            {
                Opt_Return_To_Servicing_Loc.Text = location.OPT_RETURN_TO_SERVICING_LOC.Trim();
            }
            if (location.REFER_TO_QUAL_REQ != null)
            {
                Refer_To_Qual_Req.Text = location.REFER_TO_QUAL_REQ.Trim();
            }
            if (location.HOURS_OF_OPERATION_2 != null)
            {
                HOURS_OF_OPERATION_2.Text = location.HOURS_OF_OPERATION_2.Trim();
            }
            if (location.HOURS_OF_OPERATION_3 != null)
            {
                HOURS_OF_OPERATION_3.Text = location.HOURS_OF_OPERATION_3.Trim();
            }
            if (location.VRAC_VEHICLE_UPGRADE != null)
            {
                Vrac_Vehicle_Upgrade.Text = location.VRAC_VEHICLE_UPGRADE.Trim();
            }
            if (location.HOD_LOCID != null)
            {
                Hod_LocId.Text = location.HOD_LOCID.Trim();
            }
            if (location.OPT_INS_REPLACE_ONLY != null)
            {
                Opt_Ins_Replace_Only.Text = location.OPT_INS_REPLACE_ONLY.Trim();
            }
            if (location.HOD_IMAGE != null)
            {
                Hod_Image.Text = location.HOD_IMAGE.Trim();
            }
            if (location.OPT_GOLD_ANYTIME != null)
            {
                Opt_Gold_Anytime.Text = location.OPT_GOLD_ANYTIME.Trim();
            }
            if (location.HOURS_OF_OPERATION != null)
            {
                HOURS_OF_OPERATION.Text = location.HOURS_OF_OPERATION.Trim();
            }
            if (location.ThriftyLocation == 1)
            {
                Thrifty.Checked = true;
                Hertz.Checked = false;
            }
            if (location.HertzLocation == 1)
            {
                Thrifty.Checked = false;
                Hertz.Checked = true;
            }
            LocationType.SelectedValue = location.LocationType;
            HertzAdvantage.Checked = Convert.ToBoolean(location.HertzAdvantage);
        }
        else
        {
            Response.Redirect("manageLocations.aspx?MsgType=4&MsgToShow=No Location found to Modify.");
        }
    }
    protected int rowID()
    {
        int rowID;
        bool result = int.TryParse(Request.QueryString["Row"], out rowID);
        if (result)
        {
            return rowID;
        }
        return 0;
    }
    protected string countryID()
    {
        return CountryID.SelectedValue;
    }

    protected void Tab1_Click(object sender, EventArgs e)
    {
        MainView.ActiveViewIndex = 0;
    }

    protected void Tab2_Click(object sender, EventArgs e)
    {
        MainView.ActiveViewIndex = 1;
    }

    protected void Tab3_Click(object sender, EventArgs e)
    {
        MainView.ActiveViewIndex = 2;
    }
    protected void Tab4_Click(object sender, EventArgs e)
    {
        MainView.ActiveViewIndex = 3;
    }
    protected void Tab5_Click(object sender, EventArgs e)
    {
        MainView.ActiveViewIndex = 4;
    }
    protected void UpdateButton_Click(object sender, EventArgs e)
    {
        try
        {
            // initialize the objectContext
            db = new DatabaseEntities();
            int row = rowID();

            if (LocationName.Text.Trim() == string.Empty || LocationCode.Text.Trim() == string.Empty || ExtendedLocationCode.Text.Trim() == string.Empty || Latitude.Text.Trim() == string.Empty || Longitude.Text.Trim() == string.Empty || City.Text.Trim() == string.Empty || Address1.Text.Trim() == string.Empty || CarTrawlerLocCode.Text.Trim() == string.Empty || ThermeonLocCode.Text.Trim() == string.Empty)
            {
                LocationName.CssClass = "text inline validate[required]";
                LocationCode.CssClass = "text inline validate[required]";
                ExtendedLocationCode.CssClass = "text inline validate[required]";
                Latitude.CssClass = "text inline validate[required]";
                Longitude.CssClass = "text inline validate[required]";
                City.CssClass = "text inline validate[required]";
                Address1.CssClass = "text inline validate[required]";
                CarTrawlerLocCode.CssClass = "text inline validate[required]";
                ThermeonLocCode.CssClass = "text inline validate[required]";

                MsgToShow.Show(MyMessageBox.MessageType.Warning, "Fill the Mandetory Fields.");
                MsgToShow.Visible = true;
            }
            else
            {
                int thrifty = 0;
                int hertz = 0;

                if (Thrifty.Checked)
                {
                    thrifty = 1;
                }
                else if (Hertz.Checked)
                {
                    hertz = 1;
                }

                if (!db.Locations.Where(l => l.LocationCode == LocationCode.Text && l.LocationID != row).Any())
                {
                    var location = db.Locations.SingleOrDefault(l => l.LocationID == row);
                    location.LocationName = LocationName.Text;
                    location.CountryIDFK = Convert.ToInt32(CountryID.SelectedValue);
                    location.LocationCode = LocationCode.Text;
                    location.ExtendedLocationCode = ExtendedLocationCode.Text;
                    location.City = City.Text;
                    location.Address_Line1 = Address1.Text;
                    location.Address_Line2 = Address2.Text;
                    location.Address_Line3 = Address3.Text;
                    location.Zip_PostCode = PostCode.Text;
                    location.Phone = Phone.Text;
                    location.Latitude = Latitude.Text;
                    location.Longitude = Longitude.Text;
                    location.Open1Mon = Open1Mon.Text;
                    location.Close1Mon = Close1Mon.Text;
                    location.Open2Mon = Open2Mon.Text;
                    location.Close2Mon = Close2Mon.Text;
                    location.Open3Mon = Open3Mon.Text;
                    location.Close3Mon = Close3Mon.Text;
                    location.Open1Tue = Open1Tue.Text;
                    location.Close1Tue = Close1Tue.Text;
                    location.Open2Tue = Open2Tue.Text;
                    location.Close2Tue = Close2Tue.Text;
                    location.Open3Tue = Open3Tue.Text;
                    location.Close3Tue = Close3Tue.Text;
                    location.Open1Wed = Open1Wed.Text;
                    location.Close1Wed = Close1Wed.Text;
                    location.Open2Wed = Open2Wed.Text;
                    location.Close2Wed = Close2Wed.Text;
                    location.Open3Wed = Open3Wed.Text;
                    location.Close3Wed = Close3Wed.Text;
                    location.Open1Thu = Open1Thu.Text;
                    location.Close1Thu = Close1Thu.Text;
                    location.Open2Thu = Open2Thu.Text;
                    location.Close2Thu = Close2Thu.Text;
                    location.Open3Thu = Open3Thu.Text;
                    location.Close3Thu = Close3Thu.Text;
                    location.Open1Fri = Open1Fri.Text;
                    location.Close1Fri = Close1Fri.Text;
                    location.Open2Fri = Open2Fri.Text;
                    location.Close2Fri = Close2Fri.Text;
                    location.Open3Fri = Open3Fri.Text;
                    location.Close3Fri = Close3Fri.Text;
                    location.Open1Sat = Open1Sat.Text;
                    location.Close1Sat = Close1Sat.Text;
                    location.Open2Sat = Open2Sat.Text;
                    location.Close2Sat = Close2Sat.Text;
                    location.Open3Sat = Open3Sat.Text;
                    location.Close3Sat = Close3Sat.Text;
                    location.Open1Sun = Open1Sun.Text;
                    location.Close1Sun = Close1Sun.Text;
                    location.Open2Sun = Open2Sun.Text;
                    location.Close2Sun = Close2Sun.Text;
                    location.Open3Sun = Open3Sun.Text;
                    location.Close3Sun = Close3Sun.Text;
                    location.ThriftyLocation = thrifty;
                    location.HertzLocation = hertz;
                    location.CTLocationId = Convert.ToInt32(CarTrawlerLocCode.Text);
                    location.ThermeonLocation = ThermeonLocCode.Text;
                    location.Country = CountryID.SelectedItem.ToString();
                    location.LocationType = LocationType.SelectedValue;
                    location.HertzAdvantage = HertzAdvantage.Checked;
                    location.PREFERRED_OAG = Preferred_OAG.Text;
                    location.STATEPROVINCE_CODE = StateProviceCode.Text;
                    location.ISO_STATEPROVINCE_CODE = IsoStateCode.Text;
                    location.NUMBER = Number.Text;
                    location.ALT_PHONE = AltPhone.Text;
                    location.FAX = Fax.Text;
                    location.TELEX = Telex.Text;
                    location.SERVED_BY = Served_By.Text;
                    location.EMAIL_ADDRESS = Email_Add.Text;
                    location.WEBSITE = WebSite.Text;
                    location.BOOKABLE = Bookable.Text;
                    location.OPT_HLE = Opt_HLE.Text;
                    location.OPT_INSURANCE_REPLACE = Opt_Insurance_Replace.Text;
                    location.OPT_NEVERLOST = Opt_Neverlost.Text;
                    location.OPT_CHILD_SEATS = Opt_Child_Seats.Text;
                    location.OPT_HAND_CONTROL = Opt_Hand_Control.Text;
                    location.OPT_GOLD_CUSTOMER = Opt_Gold_Customer.Text;
                    location.OPT_GOLD_CANOPY = Opt_Gold_Canopy.Text;
                    location.OPT_PORTABLE_PHONES = Opt_Portable_Phones.Text;
                    location.OPT_SKIERIZED = Opt_Skierized.Text;
                    location.OPT_MOBILE_PHONES = Opt_Mobile_Phones.Text;
                    location.OPT_LUGGAGE_RACK = Opt_Luggqage_Rack.Text;
                    location.OPT_PRESTIGE_SERVICE = Opt_Prestige_Services.Text;
                    location.OPT_RETURN_CENTER = Opt_Return_Center.Text;
                    location.OPT_HTV = Opt_HTV.Text;
                    location.OPT_HEAVY_TRUCKS = Opt_Heavy_Trucks.Text;
                    location.OPT_PICKUP_SERVICE = Opt_Pickup_Services.Text;
                    location.MAJOR_AIRPORT = Major_AirPort.Text;
                    location.SERVED_BY_DATA = Served_BY_Data.Text;
                    location.ADDRESS4 = Address4.Text;
                    location.ADDRESS5 = Address5.Text;
                    location.CITY_ADDR = CityAdd.Text;
                    location.NOTES1 = Notes_1.Text;
                    location.NOTES2 = Notes_2.Text;
                    location.NOTES3 = Notes_3.Text;
                    location.NOTES4 = Notes_4.Text;
                    location.NOTES5 = Notes_5.Text;
                    location.CAPITAL = Capital.Text;
                    location.STATEPROVINCE_NAME = StateProvineName.Text;
                    location.CITY_VARIANTS = City_Variants.Text;
                    location.DISPLAY_ADDR = Display_Add.Text;
                    location.DISPLAY_DL = Display_Dl.Text;
                    location.DISPLAY_DEST = Display_Dest.Text;
                    location.AREA = Area.Text;
                    location.LOCATOR_CODE = Locator_Code.Text;
                    location.TOP_LOCATION = Top_Location.Text;
                    location.FBO = Fbo.Text;
                    location.SPECIAL_INSTRUCTION = Spec_Inst.Text;
                    location.SPECIAL_INSTRUCTION_DATA = Spec_Inst_Data.Text;
                    location.DOUBLE_CLICK_BANNER = DoubleClickBanner.Text;
                    location.HZ_LOCID = HZ_LocId.Text;
                    location.COUNTER_BYPASS_ENABLED = Counter_Bypass_Enable.Text;
                    location.OPT_WIFI = Opt_Wifi.Text;
                    location.OPT_PLATE_PASS = Opt_Plate_Pass.Text;
                    location.OPT_EXPRESS_RETURN = Opt_Express_Return.Text;
                    location.OPT_NO_WALKINS = Opt_No_Walkings.Text;
                    location.OPT_AFTER_HOURS_DROP = Opt_After_Hours_Drop.Text;
                    location.OPT_AFTER_HOURS_PICKUP = Opt_After_Hours_Pickup.Text;
                    location.OPT_INFANT_SEAT = Opt_Infant_Seat.Text;
                    location.OPT_BOOSTER_SEATS = Opt_Boosters_Seats.Text;
                    location.OPT_MILITARY_LOC = Opt_Military_Loc.Text;
                    location.OPT_HOTEL_GUEST_REQ = Opt_Hotel_Guest_Req.Text;
                    location.OPT_LEASING_LOC = Opt_Leasing_Loc.Text;
                    location.OPT_RE_FUEL = Opt_RE_Fuel.Text;
                    location.OPT_ROADSIDE_ASSSIST = Opt_Roadside_Assist.Text;
                    location.OPT_KIOSK_LOC = Opt_Kiosk_Loc.Text;
                    location.OPT_CHAFFEUR_LOC = Opt_Chaffeur_Loc.Text;
                    location.OPT_TRAIN_LOC = Opt_Train_Loc.Text;
                    location.OPT_SHIP_LOC = Opt_Ship_Loc.Text;
                    location.OPT_CRUISE_LOC = Opt_Cruise_Loc.Text;
                    location.OPT_BUS_LOC = Opt_Bus_Loc.Text;
                    location.OPT_SUBWAY_LOC = Opt_Subway_Loc.Text;
                    location.OPT_RET_SERV_LOC = Opt_Ret_Serv_Loc.Text;
                    location.OPT_MEET_PF = Opt_Meet_Pf.Text;
                    location.OPT_MEET_ADV_RES = Opt_Meet_Acv_Res.Text;
                    location.OPT_FLIGHT_MAND_ALL = Opt_Flight_Mand_All.Text;
                    location.OPT_FLIGHT_MAND_PRIVATE = Opt_Flight_Mand_Private.Text;
                    location.OPT_HOD = Opt_HOD.Text;
                    location.OPT_GOLD_CHOICE = Opt_Gold_Choice.Text;
                    location.OPT_HERTZ_EQUIP = Opt_Hertz_Equip.Text;
                    location.OPT_PRIVATE_FLIGHT_ONLY = Opt_Private_Flight_Only.Text;
                    location.DISPLAY_HLE_TEXT = Display_Hle_Txt.Text;
                    location.OPT_MGB = Opt_MGB.Text;
                    location.OPT_RETURN_TO_SERVICING_LOC = Opt_Return_To_Servicing_Loc.Text;
                    location.REFER_TO_QUAL_REQ = Refer_To_Qual_Req.Text;
                    location.HOURS_OF_OPERATION_2 = HOURS_OF_OPERATION_2.Text;
                    location.HOURS_OF_OPERATION_3 = HOURS_OF_OPERATION_3.Text;
                    location.VRAC_VEHICLE_UPGRADE = Vrac_Vehicle_Upgrade.Text;
                    location.HOD_LOCID = Hod_LocId.Text;
                    location.OPT_INS_REPLACE_ONLY = Opt_Ins_Replace_Only.Text;
                    location.HOD_IMAGE = Hod_Image.Text;
                    location.OPT_GOLD_ANYTIME = Opt_Gold_Anytime.Text;
                    location.HOURS_OF_OPERATION = HOURS_OF_OPERATION.Text;
                    db.SaveChanges();
                    Response.Redirect("manageLocations.aspx?MsgToShow=Location updated successfully.&MsgType=2&Row=" + CountryID.SelectedValue);
                }
                else
                {
                    MsgToShow.Show(MyMessageBox.MessageType.Warning, "That Location Code already exists. Please use another  Location Code.");
                    MsgToShow.Visible = true;
                }
            }

        }
        catch
        {
            MsgToShow.Show(MyMessageBox.MessageType.Error, "There was an error adding this information. Please try again.");
            MsgToShow.Visible = true;
        }
    }
}