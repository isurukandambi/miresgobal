﻿<%@ Page Language="C#" MasterPageFile="adminarea.master" AutoEventWireup="true" CodeFile="addLogin.aspx.cs" Inherits="addLogin" %>
<%@ Register Src="~/adminarea/controls/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <link href="js/validationEngine/css/validationEngine.jquery.css" rel="stylesheet" type="text/css" />
    <script src="js/validationEngine/jquery.validationEngine.js" type="text/javascript"></script>
    <script src="js/validationEngine/jquery.validationEngine-en.js" type="text/javascript"></script>
    <script type="text/javascript" language="javascript">
        $(document).ready(function () {            
            $("#form1").validationEngine();
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div id="content">
    <uc1:MyMessageBox ID="MsgToShow" runat="server" ShowCloseButton="true" Visible="false" />

    <h3><asp:Literal ID="PageName" runat="server" Text="Add Admin Area Login" /></h3>
    <div id="actionArea">
    <fieldset>
        <div>
            <label for="Username" class="inline">Username </label>
            <asp:TextBox ID="Username" CssClass="text inline validate[required]" runat="server" MaxLength="30"></asp:TextBox>
        </div>
        <div>
            <label for="Password" class="inline">Password </label>
            <asp:TextBox ID="Password" CssClass="text inline validate[required,length[6,20]]" TextMode="Password" runat="server" MaxLength="20"></asp:TextBox>
        </div>
        <div>
            <label for="ConfirmPassword" class="inline">Confirm Password </label>
            <asp:TextBox ID="ConfPassword" CssClass="text inline validate[required,confirm[ctl00_ContentPlaceHolder1_Password]]" TextMode="Password" runat="server" MaxLength="20"></asp:TextBox>
        </div>
        <div>
            <label for="LoggedInName" class="inline">Logged In Name </label>
            <asp:TextBox ID="LoggedInName" CssClass="text inline validate[required]" runat="server" MaxLength="50"></asp:TextBox>
        </div>
        <div>
            <label for="Email" class="inline">Email </label>
            <asp:TextBox ID="Email" CssClass="text inline validate[required,custom[email]]" runat="server" MaxLength="100"></asp:TextBox>
        </div>
        <div>
            <label for="SiteRoles" class="inline">Admin Area Role:</label>
            <asp:DropDownList ID="SiteRoles" runat="server">
            </asp:DropDownList>
        </div>
        
        <div class="formActions">
            <asp:Button ID="AddLogin" CssClass="btn" OnClick="AddLogin_Click" runat="server" Text="Add details and email user" />
            <a href="manageLogins.aspx" class="CancelButton">Cancel</a>
        </div>
    </fieldset>
    </div>
</div>
</asp:Content>

