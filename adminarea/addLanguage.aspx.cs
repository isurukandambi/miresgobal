﻿using System;
using System.Linq;
using System.Text;
using System.Web;
using ORM;
using CMSConfigs;

public partial class addCountry : System.Web.UI.Page
{
    private DatabaseEntities db;

    protected void Page_Load(object sender, EventArgs e)
    {
        Master.Page.Title = CMSSettings.SiteTitle + " - " + PageName.Text;
        if (Session["LoggedInName"] == null)
        {
            Response.Redirect("default.aspx?TimeOut=y&ReturnPage=" + Server.UrlEncode(HttpContext.Current.Request.Url.AbsoluteUri));
        }
    }


    protected void AddButton_Click(object sender, EventArgs e)
    {
        try
        {
            // initialize the objectContext
            db = new DatabaseEntities();

            if (!db.Languages.Where(la => la.LanguageCode == LanguageCode.Text).Any())
            {
                Language language = new Language
                {
                    LanguageName = LanguageName.Text.Trim(),
                    LanguageCode = LanguageCode.Text.Trim()
                };
                db.Languages.Add(language);
                db.SaveChanges();

                Response.Redirect("manageLanguages.aspx?MsgType=2&MsgToShow='" + LanguageName.Text + "' added successfully");

            }
            else
            {
                MsgToShow.Show(MyMessageBox.MessageType.Warning, "That Language Code already exists. Please use another Language Code.");
                MsgToShow.Visible = true;
            }

        }
        catch(Exception ex)
        {
            MsgToShow.Show(MyMessageBox.MessageType.Error, "There was an error adding this information. Please try again.");
            MsgToShow.Visible = true;
        }
    }
}