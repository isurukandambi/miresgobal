﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ORM;
using System.Linq;
using CMSConfigs;
using CMSUtils;

public partial class manageCountries : System.Web.UI.Page
{
    private DatabaseEntities db;
    protected void Page_Load(object sender, EventArgs e)
    {
        Master.Page.Title = CMSSettings.SiteTitle + " - Edit Section - Manage Countries";

        if (Session["LoggedInName"] == null)
        {
            Response.Redirect("default.aspx?TimeOut=y&ReturnPage=" + Server.UrlEncode(HttpContext.Current.Request.Url.AbsoluteUri));
        }
        if (Request.QueryString["DeleteRow"] != null)
        {
            
            int CountryID = Convert.ToInt16(Request.QueryString["DeleteRow"]);
            DeleteItem(CountryID);
        }
        MsgToShow.Visible = false;
        if (Request.QueryString["MsgToShow"] != null)
        {
            int MsgType = Convert.ToInt16(Request.QueryString["MsgType"]);
            switch (MsgType)
            {
                case 1:
                    MsgToShow.Show(MyMessageBox.MessageType.Info, Request.QueryString["MsgToShow"].ToString());
                    break;
                case 2:
                    MsgToShow.Show(MyMessageBox.MessageType.Success, Request.QueryString["MsgToShow"].ToString());
                    break;
                case 3:
                    MsgToShow.Show(MyMessageBox.MessageType.Warning, Request.QueryString["MsgToShow"].ToString());
                    break;
                case 4:
                    MsgToShow.Show(MyMessageBox.MessageType.Error, Request.QueryString["MsgToShow"].ToString());
                    break;
                default:
                    goto case 1;
            }
            MsgToShow.Visible = true;
        }
        LoadCountries();
    }
    protected void DeleteItem(int countryID) {
        try
        {
            db = new DatabaseEntities();
            var country = db.Countries.SingleOrDefault(c => c.CountryID == countryID);
            db.Countries.Remove(country);
            db.SaveChanges();
            Response.Redirect("manageCountries.aspx?MsgType=2&MsgToShow=Country Deleted.");
        }
        catch(Exception ex)
        {
            
            Response.Redirect("manageCountries.aspx?MsgType=4&MsgToShow=" + CMSUtilities.FKconstrainsFail());
            Response.Write(ex);
            //MsgToShow.Show(MyMessageBox.MessageType.Error, "There was an error adding this information. Please try again." + ex.Message + "</br>" + ex.StackTrace);
            //MsgToShow.Visible = true;

        }
    }
    protected void LoadCountries() { 
        db = new DatabaseEntities();
        GridView1.DataSource = (from c in db.Countries
                                orderby c.CountryName
                                select new {c.CountryID , c.CountryName, c.CountryCode }).ToList();
        GridView1.DataBind();
    }
    protected void GridView_MouseOver(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowIndex >= 0)
        {
            e.Row.Attributes.Add("onmouseover", "this.style.background='#FFFFFF'");
            e.Row.Attributes.Add("onmouseout", "this.style.background='#F7F7F7'");
        }
    }
}
