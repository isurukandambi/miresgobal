﻿using System;
using System.Linq;
using System.Text;
using System.Web;
using ORM;
using CMSConfigs;

public partial class updateCountry : System.Web.UI.Page
{
    private DatabaseEntities db;

    protected void Page_Load(object sender, EventArgs e)
    {
        Master.Page.Title = CMSSettings.SiteTitle + " - " + PageName.Text;
        if (Session["LoggedInName"] == null)
        {
            Response.Redirect("default.aspx?TimeOut=y&ReturnPage=" + Server.UrlEncode(HttpContext.Current.Request.Url.AbsoluteUri));
        }
        if(!Page.IsPostBack){
            LoadPartners();
            LoadBrands();
            
            LoadData();
        }
    }
    protected void LoadData() {
        db = new DatabaseEntities();
        int row = rowID();
        var partnerbrand = db.PatnerBrandMappings.SingleOrDefault(p => p.MappingId == row);
        if (partnerbrand != null)
        {
            PartnerID.SelectedValue = partnerbrand.PartnerId.ToString();
            BrandID.SelectedValue = partnerbrand.BrandId.ToString();
            VendorCode.Text = partnerbrand.VendorCode.ToString().Trim();
            RequestVendorCode.Text = partnerbrand.RequestVendorCode.ToString().Trim();
            PartnerRateQualifire.Text = partnerbrand.PartnerRateQualifier.ToString().Trim();
            VehiclePrefEnable.Checked = Convert.ToBoolean(partnerbrand.VehiclePrefEnable);
            xmlProviderRate.Text = partnerbrand.XMLProviderRateQualifier.ToString();
        }
    }
    protected int rowID()
    {
        int rowID;
        bool result = int.TryParse(Request.QueryString["Row"], out rowID);
        if (result)
        {
            return rowID;
        }
        return 0;
    }
    protected void UpdateButton_Click(object sender, EventArgs e)
    {
        try
        {
            // initialize the objectContext
            db = new DatabaseEntities();

            int partnerId = Convert.ToInt32(PartnerID.SelectedValue);
            int brandId = Convert.ToInt32(BrandID.SelectedValue);
            int vheRef = 0;
            if (VehiclePrefEnable.Checked)
            {
                vheRef = 1;
            }


            int row = rowID();
            //if (!db.PatnerBrandMappings.Where(p => (p.PartnerId == partnerId && p.BrandId == brandId && p.MappingId != row)).Any())
            //{
                var partnerbrand = db.PatnerBrandMappings.SingleOrDefault(p => p.MappingId == row);
                partnerbrand.PartnerId = partnerId;
                partnerbrand.BrandId = brandId;
                partnerbrand.VendorCode = VendorCode.Text.Trim();
                partnerbrand.RequestVendorCode = RequestVendorCode.Text.Trim();
                partnerbrand.PartnerRateQualifier = PartnerRateQualifire.Text.Trim();
                partnerbrand.XMLProviderRateQualifier = xmlProviderRate.Text.Trim();
                partnerbrand.VehiclePrefEnable = vheRef;
                db.SaveChanges();

                Response.Redirect("managePartnerBrand.aspx?MsgToShow=Partner Brand updated successfully.&MsgType=2");
            //}
            //else
            //{
            //    MsgToShow.Show(MyMessageBox.MessageType.Warning, "Partner and Brand already exists. Please use another Partner and Brand..");
            //    MsgToShow.Visible = true;
            //}

        }
        catch
        {
            MsgToShow.Show(MyMessageBox.MessageType.Error, "There was an error adding this information. Please try again.");
            MsgToShow.Visible = true;
        }
    }

    protected void LoadBrands()
    {
        db = new DatabaseEntities();
        BrandID.DataSource = db.Brands.OrderBy(c => c.BrandID).ToList();
        BrandID.DataValueField = "BrandID";
        BrandID.DataTextField = "BrandName";
        BrandID.DataBind();
    }

    protected void LoadPartners()
    {
        db = new DatabaseEntities();
        PartnerID.DataSource = db.Partners.OrderBy(c => c.PartnerId).ToList();
        PartnerID.DataValueField = "PartnerId";
        PartnerID.DataTextField = "DomainName";
        PartnerID.DataBind();
    }
}