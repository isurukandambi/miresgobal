﻿<%@ Page Language="C#" MasterPageFile="adminarea.master" AutoEventWireup="true" CodeFile="manageLogins.aspx.cs" Inherits="manageLogins" %>
<%@ Register Src="~/adminarea/controls/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>
<%@ Register Src="~/adminarea/Controls/ConfirmBox.ascx" TagName="ConfirmBox" TagPrefix="cb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script src="js/dataTables/jquery.dataTables.min.js" type="text/javascript"></script>
    <script type="text/javascript" language="javascript">
        $(document).ready(function () {
            //grid 1 settings
            $('#dataGrid').dataTable({
                "sDom": '<"dataTable_header"lf>rt<"dataTable_footer"ip>',
                "bAutoWidth": false,
                "sPaginationType": "full_numbers",
                "iDisplayLength": 50,
                "bStateSave": true,
                "iCookieDuration": 300,
                //customizing columns
                "aaSorting": [[1, "asc"]],
                "aoColumns": [
        			{ "bSearchable": false, "bVisible": false }, //setting first column to be hidden from searching and the user - required for knowing what database row to remove
                    { "sType": "html" }, //setting second column to search word of html link
        			null,
                    null,
        			{ "bSortable": false } //setting last column not to sort onClick
                ]

            });
            //check all checkboxes if image clicked on
            $('#headercheck').click(function (evt) {
                evt.preventDefault();
                $('#dataGrid :checkbox').attr('checked', function () {
                    return !this.checked;
                });
            });

            //don't fire the delete button if no checkbox selected
            $('[id$=DeleteItems]').click(function (evt) {
                if ($("#dataGrid :checked").size() == 0) {
                    evt.preventDefault();
                }
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<div id="content">
    <h2><asp:Literal ID="PageName" runat="server" Text="Manage Logins &amp; Roles" /></h2>
    <div class="buttons">
        <a href="addLogin.aspx">Setup new user</a> 
        <a href="manageRoles.aspx">Manage Roles</a>
        <a href="updatelogin.aspx">Change your login</a>
    </div>

    <uc1:MyMessageBox ID="MsgToShow" runat="server" ShowCloseButton="true" Visible="false" />

    <h3>Existing Logins:</h3>  
    <div id="actionArea">
        <asp:Literal ID="NoRecordsFound" runat="server" Text="No details found." Visible="false" />
        <asp:Repeater ID="InformationTable" runat="server" Visible="false">
            <HeaderTemplate>
                <table cellpadding="0" cellspacing="0" border="0" id="dataGrid">
                    <thead>
                        <tr>
                            <th>RowID</th>
                            <th class="leftCell">User</th>
                            <th>Role Assigned</th>
                            <th>Last Logged In</th>
                            <th><a href="#" id="headercheck"><img src="images/icons/delete.png" width="15" height="15" alt="delete icon" /></a></th>
                        </tr>
                    </thead>
                    <tbody>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td><asp:Literal ID="rowID" runat="server" Text='<%# Eval("LoginID")  %>' /></td>
                    <td><a href="updateUserLogin.aspx?Row=<%# Eval("LoginID") %>"><%# Eval("LoggedInName") + " <span style='font-weight: 500;'>(" + Eval("Username") + ")</span> "%></a></td>
                    <td class="cnrCell"><%# Eval("RoleName")%></td>
                    <td class="cnrCell"><%# FormatDate((Eval("LastLoggedIn") == null) ? string.Empty : Eval("LastLoggedIn").ToString())%></td>
                    <td class="cnrCell"><asp:CheckBox ID="rowcheck" runat="server" /></td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </tbody> </table>
            </FooterTemplate>
        </asp:Repeater>
        <fieldset>
            <div class="formActions">
                <asp:Button ID="DeleteItems" CssClass="btn" runat="server" Text="Delete Selected Users" OnClick="DeleteItems_Click" Visible="false" />
            </div>
        </fieldset>
    </div>
    <cb:ConfirmBox ID="ConfirmBox" runat="server" Visible="false" OnFireConfirm="Confirm_Fired" />
</div>    
</asp:Content>

