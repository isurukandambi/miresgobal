﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ORM;
using CMSConfigs;

public partial class adminarea_updateBrandProviders : System.Web.UI.Page
{
    private DatabaseEntities db;
    protected void Page_Load(object sender, EventArgs e)
    {
        Master.Page.Title = CMSSettings.SiteTitle + " - " + PageName.Text;
        if (Session["LoggedInName"] == null)
        {
            Response.Redirect("default.aspx?TimeOut=y&ReturnPage=" + Server.UrlEncode(HttpContext.Current.Request.Url.AbsoluteUri));
        }
        if (!Page.IsPostBack)
        {
            LoadProviders();
            LoadBrands();

            LoadData();
        }
    }

    protected void LoadData()
    {
        db = new DatabaseEntities();
        int row = rowID();
        var providerbrand = db.BrandProviderMappings.SingleOrDefault(p => p.BPMID == row);
        if (providerbrand != null)
        {
            ProviderID.SelectedValue = providerbrand.ProviderIDFK.ToString();
            BrandID.SelectedValue = providerbrand.BrandIDFK.ToString();
        }
    }

    protected int rowID()
    {
        int rowID;
        bool result = int.TryParse(Request.QueryString["Row"], out rowID);
        if (result)
        {
            return rowID;
        }
        return 0;
    }

    protected void LoadBrands()
    {
        db = new DatabaseEntities();
        BrandID.DataSource = db.Brands.OrderBy(c => c.BrandID).ToList();
        BrandID.DataValueField = "BrandID";
        BrandID.DataTextField = "BrandName";
        BrandID.DataBind();
    }

    protected void LoadProviders()
    {
        db = new DatabaseEntities();
        ProviderID.DataSource = db.Providers.OrderBy(c => c.Pro_Id).ToList();
        ProviderID.DataValueField = "Pro_Id";
        ProviderID.DataTextField = "ProviderName";
        ProviderID.DataBind();
    }

    protected void UpdateButton_Click(object sender, EventArgs e)
    {
        try
        {
            db = new DatabaseEntities();

            int providerId = Convert.ToInt32(ProviderID.SelectedValue);
            int brandId = Convert.ToInt32(BrandID.SelectedValue);
            
            int row = rowID();
            if (!db.BrandProviderMappings.Where(p => (p.ProviderIDFK == providerId && p.BrandIDFK == brandId)).Any())
            {
                var providerbrand = db.BrandProviderMappings.SingleOrDefault(p => p.BPMID == row);
                providerbrand.ProviderIDFK = providerId;
                providerbrand.BrandIDFK = brandId;
                db.SaveChanges();

                Response.Redirect("manageBrandProviders.aspx?MsgToShow=Provider Brand updated successfully.&MsgType=2");
            }
            else
            {
                MsgToShow.Show(MyMessageBox.MessageType.Warning, "Provider and Brand already exists. Please use another Provider and Brand..");
                MsgToShow.Visible = true;
            }

        }
        catch
        {
            MsgToShow.Show(MyMessageBox.MessageType.Error, "There was an error adding this information. Please try again.");
            MsgToShow.Visible = true;
        }
    }
}