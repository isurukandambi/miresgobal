﻿<%@ Page Language="C#" MasterPageFile="adminarea.master" AutoEventWireup="true" CodeFile="addPartnerBrandcode.aspx.cs" Inherits="addCountry" %>
<%@ Register Src="~/adminarea/controls/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <link href="js/validationEngine/css/validationEngine.jquery.css" rel="stylesheet" type="text/css" />
    <script src="js/validationEngine/jquery.validationEngine.js" type="text/javascript"></script>
    <script src="js/validationEngine/jquery.validationEngine-en.js" type="text/javascript"></script>
    <script type="text/javascript" language="javascript">
        $(document).ready(function () {            
            $("#form1").validationEngine();
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div id="content">
    <uc1:MyMessageBox ID="MsgToShow" runat="server" ShowCloseButton="true" Visible="false" />

    <h3><asp:Literal ID="PageName" runat="server" Text="Add Code Mapping" /></h3>
    <div id="actionArea">
    <fieldset>
        <div>
            <label for="PartnerName" class="inline">Partner Name </label>
            <asp:DropDownList ID="PartnerID" runat="server"> 
            </asp:DropDownList>
        </div>
        <div>
            <label for="BrandName" class="inline">Brand Name </label>
            <asp:DropDownList ID="BrandID" runat="server"> 
            </asp:DropDownList>
        </div>

        

        <div>
            <label for="IataCode" class="inline">IATA Code </label>
            <asp:TextBox ID="IataCodetxt"  runat="server" MaxLength="10"></asp:TextBox>
        </div>
        
        <div>
            <label for="PromotionCode" class="inline">Promotion Code </label>
            <asp:TextBox ID="PromotionCodetxt"  runat="server" MaxLength="10"></asp:TextBox>
        </div>
        
        <div>
            <label for="TravellerNumber" class="inline">Traveller Number </label>
            <asp:TextBox ID="TravellerNumbertxt"  runat="server" MaxLength="10"></asp:TextBox>
        </div>
        <div>
            <label for="Tour_code" class="inline">Tour Code </label>
            <asp:TextBox ID="Tour_codetxt"  runat="server" MaxLength="10"></asp:TextBox>
        </div>
        <div>

      


        </div>

        <div class="formActions">
            <asp:Button ID="AddButton" CssClass="btn" OnClick="AddButton_Click" runat="server" Text="Add Code" />
            <a href="managePartnerBrandCode.aspx" class="CancelButton">Cancel</a>
        </div>
    </fieldset>
    </div>
</div>
</asp:Content>

