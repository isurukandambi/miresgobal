﻿<%@ Page Language="C#" MasterPageFile="adminarea.master" ValidateRequest="false" AutoEventWireup="true" CodeFile="addRole.aspx.cs" Inherits="addRole" %>
<%@ Register Src="~/adminarea/controls/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <link href="js/validationEngine/css/validationEngine.jquery.css" rel="stylesheet" type="text/css" />
    <script src="js/validationEngine/jquery.validationEngine.js" type="text/javascript"></script>
    <script src="js/validationEngine/jquery.validationEngine-en.js" type="text/javascript"></script>
    <script  type="text/javascript">
        $(document).ready(function () {
            $("#form1").validationEngine();
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div id="content">
    <uc1:MyMessageBox ID="MsgToShow" runat="server" ShowCloseButton="true" Visible="false" />

    <h3><asp:Literal ID="PageName" runat="server" Text="Add new role to the system" /></h3>
    
    <div id="actionArea">
    <fieldset>
        <div>
            <label for="RoleName" class="inline">Role Name </label>
            <asp:TextBox ID="RoleNametxt" CssClass="text inline validate[required]" runat="server" MaxLength="100"></asp:TextBox> 
        </div>
    
    <asp:Panel ID="SiteArea" runat="server">
        <table class="narrowTable">
            <tr>
                <th style="width:200px;"><strong>Section of site</strong></th>
                <th style="width:150px;"><strong>Permission</strong></th>
            </tr>
            <asp:PlaceHolder ID="RadioButtonPlaceholder" runat="server"></asp:PlaceHolder>
        </table>
    </asp:Panel>

    <div class="formActions">
        <asp:Button ID="AddButton" CssClass="btn" runat="server" onclick="AddButton_Click" Text="Add Details" />
        <a href="manageRoles.aspx" class="CancelButton">Cancel</a>
    </div>

    </fieldset>
    </div>    

</div>
</asp:Content>

