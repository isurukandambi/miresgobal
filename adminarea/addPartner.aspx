﻿<%@ Page Language="C#" MasterPageFile="adminarea.master" AutoEventWireup="true" CodeFile="addPartner.aspx.cs" Inherits="addPartner" %>
<%@ Register Src="~/adminarea/controls/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <link href="js/validationEngine/css/validationEngine.jquery.css" rel="stylesheet" type="text/css" />
    <script src="js/validationEngine/jquery.validationEngine.js" type="text/javascript"></script>
    <script src="js/validationEngine/jquery.validationEngine-en.js" type="text/javascript"></script>
    <script type="text/javascript" language="javascript">
        $(document).ready(function () {            
            $("#form1").validationEngine();
        });
    </script>
     <script type="text/javascript" language="javascript">
         $(document).ready(function () {
             //hover for grid
             $('#dataGrid tbody tr').hover(function () {
                 $(this).addClass('highlight');
             }, function () {
                 $(this).removeClass('highlight');
             });
             //grid settings
             $('#dataGrid').dataTable({
                 "sDom": '<"dataTable_header"lf>rt<"dataTable_footer"ip>',
                 "bAutoWidth": false,
                 "sPaginationType": "full_numbers",
                 "iDisplayLength": 50,
                 "bStateSave": true,
                 "iCookieDuration": 300,
                 //customizing columns
                 "aoColumns": [
        			{ "bSearchable": false, "bVisible": false }, //set column to be hidden from searching and the user - required for knowing what database row to remove
                 //{ "sType": "html" }, //setting second column to search word of html link
                    null,
                    null,
        			null,
        			null,
        			null,
                    { "bSortable": false },
        			{ "bSortable": false} //setting last column not to sort onClick
                ]

             });
             //check all checkboxes if image clicked on
             $('#headercheck').click(function (evt) {
                 evt.preventDefault();
                 $('#dataGrid :checkbox').attr('checked', function () {
                     return !this.checked;
                 });
             });

             //don't fire the delete button if no checkbox selected
             $('[id$=DeleteItems]').click(function (evt) {
                 if ($("#dataGrid :checked").size() == 0) {
                     evt.preventDefault();
                 }
             });
         });
    </script>
    <style type="text/css"">
        dd {
            margin-left: 0px;
            margin-bottom: 10px;
            margin-top: 4px;
        }
        dd img{
            margin-right: 5px;
        }
        .style1
        {
            width: 217px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div id="content">
    <uc1:MyMessageBox ID="MsgToShow" runat="server" ShowCloseButton="true" Visible="false" />

    <h3><asp:Literal ID="PageName" runat="server" Text="Add Partners" /></h3>
    <div id="actionArea">
    <fieldset>
        
        <div>
            <label for="name" class="inline">Domain Name </label>
            <asp:TextBox ID="txtdn" CssClass="text inline validate[required,length[6,20]]"  
                runat="server" MaxLength="60"></asp:TextBox>
        </div>
         <div>
            <label for="Email" class="inline">Email </label>
            <asp:TextBox ID="txtemail" CssClass="text inline validate[required,length[6,20]]"  
                runat="server" MaxLength="60"></asp:TextBox>
        </div>
        <div>
            <label for="Agentdutycode" class="inline">Agent Duty Code </label>
            <asp:TextBox ID="txtAgentdutycode" CssClass="text inline validate[required,length[6,20]]"  
                runat="server" MaxLength="60"></asp:TextBox>
        </div>
         <div>
            <label for="RequestorID" class="inline">Requestor ID</label>
            <asp:TextBox ID="txtRequestorID" CssClass="text inline validate[required,length[6,20]]"  
                runat="server" MaxLength="60"></asp:TextBox>
        </div>
          <div>
            <label for="RequestorID_Type" class="inline">Requestor ID Type</label>
            <asp:TextBox ID="RequestorID_Type" CssClass="text inline validate[required,length[6,20]]"  
                runat="server" MaxLength="60"></asp:TextBox>
        </div>
        
          <div>
            <label for="CompanyName_Code" class="inline">Company Name Code</label>
            <asp:TextBox ID="txtCompanyName_Code" CssClass="text inline validate[required,length[6,20]]"  
                runat="server" MaxLength="60"></asp:TextBox>
        </div>
         <div>
            <label for="CodeContext" class="inline">Code Context</label>
            <asp:TextBox ID="txtCodeContext" CssClass="text inline validate[required,length[6,20]]"  
                runat="server" MaxLength="60"></asp:TextBox>
        </div>
        <div>
                    <label for="CT_Allowed" class="inline">Allow for CarTrawler </label>
                    <asp:DropDownList ID="CT_Allowed" runat="server">
                        <asp:ListItem Value="1" Text="Yes"></asp:ListItem>
                        <asp:ListItem Value="0" Text="No"></asp:ListItem>
                    </asp:DropDownList>
                </div>
        <div class="cr">
                 
                    <label>
                        <asp:CheckBox ID="chkenable" Text="Vehicle Limit Enable." runat="server" />
                        
                    </label>
                    
                </div>
                   <div class="cr">
                 
                  <label>  <asp:CheckBox ID="chkemail" Text="Confirmation Email Sent." runat="server" />
                    </label>
                        </div>
        
        <div class="formActions">
            <asp:Button ID="addpartner" CssClass="btn" OnClick="Addpartner_Click" 
                runat="server" Text="Add Partner" Width="116px" />
            <a href="managePartner.aspx" class="CancelButton">Cancel</a>
        </div><br /><br />
    </fieldset>
        
    </div>
</div>
</asp:Content>

