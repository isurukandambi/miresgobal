﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ORM;
using CMSConfigs;

public partial class adminarea_addXMLprovider : System.Web.UI.Page
{
    private DatabaseEntities db;
    protected void Page_Load(object sender, EventArgs e)
    {
        Master.Page.Title = CMSSettings.SiteTitle + " - " + PageName.Text;
        if (Session["LoggedInName"] == null)
        {
            Response.Redirect("default.aspx?TimeOut=y&ReturnPage=" + Server.UrlEncode(HttpContext.Current.Request.Url.AbsoluteUri));
        }
    }

    protected void AddButton_Click(object sender, EventArgs e)
    {
        try
        {
            // initialize the objectContext
            db = new DatabaseEntities();
            if (XMLProvider.Text.Trim() == null)
            {
                MsgToShow.Show(MyMessageBox.MessageType.Warning, "XMl Provider Cannot be null. Please use another  XML Provider.");
                MsgToShow.Visible = true;
            }
            else
            {
                if (!db.Providers.Where(p => p.ProviderName == XMLProvider.Text.Trim()).Any())
                {
                    Provider Provider = new Provider
                    {
                        ProviderName = XMLProvider.Text.Trim(),

                    };
                    db.Providers.Add(Provider);
                    db.SaveChanges();

                    Response.Redirect("manageXMLproviders.aspx?MsgType=2&MsgToShow='" + XMLProvider.Text.Trim() + "' added successfully");

                }
                else
                {
                    MsgToShow.Show(MyMessageBox.MessageType.Warning, "That Provider already exists. Please use another Provider.");
                    MsgToShow.Visible = true;
                }
            }

        }
        catch
        {
            MsgToShow.Show(MyMessageBox.MessageType.Error, "There was an error adding this information. Please try again.");
            MsgToShow.Visible = true;
        }
    }
}