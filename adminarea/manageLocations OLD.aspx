﻿<%@ Page Language="C#" MasterPageFile="adminarea.master" AutoEventWireup="true" CodeFile="manageLocations OLD.aspx.cs" Inherits="manageLocations" %>

<%@ Register Src="~/adminarea/controls/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        table.narrowTable {
            width: auto;
        }

        label.inline {
            width: 99px;
            margin: 0.2em 0.5em 0.5em 0;
            float: left;
            line-height: 2em;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" />
    <script type="text/javascript" language="javascript">
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
        function EndRequestHandler(sender, args) {
            $(document).ready(function () {
                //used for context menu on grids
                $('.device-actions-toggler').show();
                $('.device-actions-toggler').click(function () { return false; });
                $('.device-actions-list').css({ position: 'absolute', top: '0', right: '0' }).hide();

                $('.device-actions').css({ position: 'relative' }).each(function () {
                    $(this).hoverIntent(function () {
                        $(this).find('.device-actions-list').fadeIn('fast');
                    }, function () {
                        $(this).find('.device-actions-list').fadeOut('fast');
                    });
                });

            });
        }
    </script>
    <div id="content">
        <h2>Manage Locations</h2>
        <div class="buttons">
            <a href="addLocation.aspx">Add Location</a>
        </div>

        <uc1:MyMessageBox ID="MsgToShow" runat="server" ShowCloseButton="true" />

        <h3>Existing Locations:</h3>
        <div id="actionArea">
            <fieldset>
                <div>
                    <label for="ddlCountries" class="inline">Filter By Country</label>
                    <asp:DropDownList ID="ddlCountries" runat="server" AutoPostBack="true" OnSelectedIndexChanged="CountryDDL_Changed" AppendDataBoundItems="true">
                    </asp:DropDownList>
                </div>
            </fieldset>
            <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="GridViewPanel">
                <ProgressTemplate>
                    <span class="UpdatingPanel">
                        <img alt="loading indicator" src="images/indicator.gif" />
                        Updating... Please Wait...</span>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <asp:UpdatePanel ID="GridViewPanel" runat="server">
                <ContentTemplate>
                    <asp:GridView
                        ID="GridView1" OnRowCreated="GridView_MouseOver" runat="server"
                        CssClass="gridview gridview-classic narrowTable" AutoGenerateColumns="false"
                        DataKeyNames="LocationID"
                        AllowSorting="True">
                        <EmptyDataTemplate>There are no locations in the database or Country not selected from drop down.</EmptyDataTemplate>
                        <Columns>
                            <asp:TemplateField HeaderText="Location" SortExpression="LocationName">
                                <ItemTemplate>
                                    <a href="updateLocation.aspx?Row=<%# Eval("LocationID") %>"><%# Eval("LocationName")%></a>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Location Code" SortExpression="AirportCode">
                                <ItemTemplate>
                                    <%# Eval("LocationCode")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Extended Location Code" SortExpression="ExtendedLocationCode">
                                <ItemTemplate>
                                    <%# Eval("ExtendedLocationCode")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Action">
                                <ItemStyle HorizontalAlign="Right" Width="50" />
                                <ItemTemplate>
                                    <div class="device-actions">
                                        <a href="#" class="device-actions-toggler">
                                            <img src="images/cog.png" alt="Actions" border="0" />
                                        </a>
                                        <div class="device-actions-list" style="display: none">
                                            <ul>
                                                <li><a href="updateLocation.aspx?Row=<%# Eval("LocationID") %>">Update</a></li>
                                                <li>
                                                    <asp:LinkButton ID="DeleteItemLnk" runat="server">Delete Item</asp:LinkButton></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <cc1:ModalPopupExtender ID="mdlPopup" runat="server"
                                        TargetControlID="DeleteItemLnk"
                                        PopupControlID="DeleteItemPnl"
                                        BackgroundCssClass="modalBackground"
                                        CancelControlID="CancelActionLnk" />
                                    <asp:Panel ID="DeleteItemPnl" runat="server" CssClass="modalPanalWithButtons" Style="display: none">
                                        <div align="center" style="margin-top: 13px;">
                                            <p>
                                                <img src="images/exclamation.png" alt="warning">Are you sure you want to remove this location?</p>
                                            <div class="buttons">
                                                <a href="manageLocations.aspx?DeleteRow=<%# Eval("LocationID") %>">Delete</a>
                                                <asp:LinkButton ID="CancelActionLnk" runat="server">Cancel</asp:LinkButton>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </ContentTemplate>
            </asp:UpdatePanel>
            <asp:UpdateProgress ID="UpdateProgress2" runat="server" AssociatedUpdatePanelID="GridViewPanel">
                <ProgressTemplate>
                    <span class="UpdatingPanel">
                        <img src="images/indicator.gif" />
                        Updating... Please Wait...</span>
                </ProgressTemplate>
            </asp:UpdateProgress>
        </div>
    </div>
</asp:Content>

