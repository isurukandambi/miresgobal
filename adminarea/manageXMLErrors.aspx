﻿<%@ Page Language="C#" MasterPageFile="adminarea.master" AutoEventWireup="true" CodeFile="manageXMLErrors.aspx.cs" Inherits="manageXMLErrors" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server"> 
    <script type="text/javascript" src="js/Highcharts/highcharts.js"></script>
    <script type="text/javascript" src="js/Highcharts/modules/exporting.js"></script>
    <script src="js/datepicker.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            //set default date popups
            $('.startDate, .endDate').datepicker({
                numberOfMonths: 2,
                showButtonPanel: true,
                dateFormat: 'yy-mm-dd',
                showOn: 'both',
                buttonImage: 'images/calendar-icon.gif',
                buttonImageOnly: true,
                buttonText: 'Show Date Picker'
            });

            //when the pickup date has changed, update the drop off date to be one day extra from that date
            $('.startDate').change(function () {
                var nextDayDate = $('.startDate').datepicker('getDate', '+1d');
                nextDayDate.setDate(nextDayDate.getDate() + 1);
                $('.endDate').datepicker('setDate', nextDayDate);
            });

            /**** XML Errors over 30 days ****/
            var todaysDate = new Date();
            var date30DaysAgo = new Date();
            date30DaysAgo.setDate(todaysDate.getDate() - 29);
            dailyChart('XMLErrors30DaysContainer', date30DaysAgo, 'XML errors logged over the last 30 days', $('#ContentPlaceHolder1_TotalXMLErrorsFor30Days').val(), $('#ContentPlaceHolder1_HertzXMLErrorsFor30Days').val());

        });

        function dailyChart(renderContainer, startDate, chartTitle, totalBookings, HertzXML) {
            //chart data
            HertzXML = HertzXML.split(",");
            HertzXML = $.map(HertzXML, function (item) {
                return parseFloat(item);
            });

            //chart control
            var chart = new Highcharts.Chart({
                credits: {
                    enabled: false
                },
                chart: {
                    renderTo: renderContainer,
                    defaultSeriesType: 'area',
                    marginRight: 140
                },
                title: {
                    text: chartTitle
                },
                subtitle: {
                    text: 'Total number of XML messages logged over 30 days: ' + totalBookings
                },
                xAxis: {
                    type: 'datetime',
                    labels: { rotation: 90, align: 'left' }
                },
                yAxis: {
                    title: {
                        text: 'Number of XML error messages logged'
                    }
                },
                legend: {
                    enabled: false,
                    layout: 'vertical',
                    align: 'right',
                    verticalAlign: 'top',
                    x: -10,
                    y: 100
                },
                tooltip: {
                    formatter: function () {
                        return this.y + ' xml errors on ' + Highcharts.dateFormat('%a, %e %b %Y', this.x);
                    }
                },
                plotOptions: {
                    area: {
                        fillOpacity: 0.5
                    }
                },
                series: [{
                    pointInterval: 24 * 3600 * 1000,
                    pointStart: startDate.getTime(),
                    color: '#4572A7',
                    name: 'Hertz',
                    data: HertzXML
                }]

            });
        }
    </script>

    <link rel="stylesheet" type="text/css" href="css/smoothness/jquery-ui.css" media="screen" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:ScriptManager ID="ScriptManager1" runat="server" />

<div id="content">
    <h2>Export XML logs</h2>

    <h3>Filter XML Logs by date &amp; export to excel:</h3> 
    <div class="actionArea">
        <fieldset>
            <div>
                <label for="StartDate" class="inline">Start Date:</label>
                    <asp:RequiredFieldValidator ID="StartDateEntered" ControlToValidate="StartDate" Display="Dynamic" ValidationGroup="GenerateXMLReport" runat="server" ErrorMessage="Start Date Required" /> 
                <asp:TextBox ID="StartDate" CssClass="text shortField startDate" runat="server" />
            </div>
            
            <div>
                <label for="EndDate" class="inline">End Date:</label>
                <asp:RequiredFieldValidator ID="EndDateEntered" ControlToValidate="EndDate" Display="Dynamic" runat="server" ValidationGroup="GenerateXMLReport" ErrorMessage="End Date Required" />
                <asp:TextBox ID="EndDate" CssClass="text shortField endDate" runat="server" />
            </div>
            <div class="formActions">           
                <asp:Button ID="GenerateXMLErrorReport" runat="server" Text="Create Report" CssClass="btnSameLine" ValidationGroup="GenerateXMLReport" onclick="XMLErrorReport_Click" />
            </div>

        </fieldset>
        
        <asp:Literal ID="XMLErrorReportMsg" runat="server" Visible="false" />
        
        <asp:GridView 
        ID="XMLErrorResults" runat="server" AutoGenerateColumns="false" CssClass="gridview gridview-classic">
        <Columns>
            <asp:BoundField HeaderText="Logged Date" DataField="ErrorDate" />
            <asp:BoundField HeaderText="Source Country" DataField="CountryOfResidence" />
            <asp:BoundField HeaderText="Pick-Up Date" DataField="PickUpDate" />
            <asp:BoundField HeaderText="Drop-Off Date" DataField="DropOffDate" />
            <asp:BoundField HeaderText="Pick-Up Location" DataField="PickUpLocation" />
            <asp:BoundField HeaderText="Drop-Off Location" DataField="DropOffLocation" />
            <asp:BoundField HeaderText="Link Came From" DataField="CameFrom" />
            <asp:BoundField HeaderText="XML Error Code" DataField="XML_ErrorCode" />
            <asp:BoundField HeaderText="XML Error Msg" DataField="XML_ErrorMsg" />
            <asp:BoundField HeaderText="XML Sent" DataField="XMLSent" />
            <asp:BoundField HeaderText="XML Received" DataField="XMLReceived" />
        </Columns>
        </asp:GridView>
        
     </div>
     <h3>XML Messages Logged over last 30 days</h3> 
     
     <div class="actionArea">  
        <asp:HiddenField ID="HertzXMLErrorsFor30Days" runat="server" />
        <asp:HiddenField ID="TotalXMLErrorsFor30Days" runat="server" />

        <div id="XMLErrors30DaysContainer" style="width: 900px; height: 450px; margin: 0 auto"></div>
    </div>
</div>
</asp:Content>

