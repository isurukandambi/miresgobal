﻿<%@ Page Language="C#" MasterPageFile="adminarea.master" AutoEventWireup="true" CodeFile="managePartner.aspx.cs" Inherits="managePartner" %>
<%@ Register Src="~/adminarea/controls/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>
<%@ Register Src="~/adminarea/Controls/ConfirmBox.ascx" TagName="ConfirmBox" TagPrefix="cb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server"> 
    <script type="text/javascript" src="js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" language="javascript">
        $(document).ready(function () {
            //hover for grid
            $('#dataGrid tbody tr').hover(function () {
                $(this).addClass('highlight');
            }, function () {
                $(this).removeClass('highlight');
            });
            //grid settings
            $('#dataGrid').dataTable({
                "sDom": '<"dataTable_header"lf>rt<"dataTable_footer"ip>',
                "bAutoWidth": false,
                "sPaginationType": "full_numbers",
                "iDisplayLength": 50,
                "bStateSave": true,
                "iCookieDuration": 300,
                //customizing columns
                "aoColumns": [
        			{ "bSearchable": false, "bVisible": false }, //set column to be hidden from searching and the user - required for knowing what database row to remove
                    //{ "sType": "html" }, //setting second column to search word of html link
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    { "bSortable": true },
        			{ "bSortable": false }
                     //setting last column not to sort onClick
                ]

            });
            //check all checkboxes if image clicked on
            $('#headercheck').click(function (evt) {
                evt.preventDefault();
                $('#dataGrid :checkbox').attr('checked', function () {
                    return !this.checked;
                });
            });

            //don't fire the delete button if no checkbox selected
            $('[id$=DeleteItems]').click(function (evt) {
                if ($("#dataGrid :checked").size() == 0) {
                    evt.preventDefault();
                }
            });
        });
    </script>
    <style type="text/css"">
        dd {
            margin-left: 0px;
            margin-bottom: 10px;
            margin-top: 4px;
        }
        dd img{
            margin-right: 5px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<div id="content">
    <h2>Manage Partners</h2>
    <div class="buttons">
        <a href="addPartner.aspx">Add Partners</a>
    </div>
    <uc1:MyMessageBox ID="MsgToShow" runat="server" ShowCloseButton="true" Visible="false" />

    <h3>Existing Partners:</h3>
    
    <div id="actionArea">
    
    <div class="tableLegend">
        <h4>Table Icons: functionality guide</h4>
        <dl>            
            <dd><img src="images/icons/delete.png" width="15" height="15" alt="delete icon" /> Tick the checkbox in this column to select the item for deletion. Click the icon at the top of the column to tick all the checkboxes. When you have the items you want to delete ticked, click the 'Delete Selected Items' button below the table.</dd>            
        </dl>
    </div>
        <asp:Literal ID="NoRecordsFound" runat="server" Text="No details found." Visible="false" />
        <asp:Repeater ID="InformationTable" runat="server">
            <HeaderTemplate>
                <table cellpadding="5" cellspacing="3" border="" id="dataGrid">
            	<thead>
            		<tr>
                        <th >RowID</th>
            			<th class="leftCell">Domain Name</th>
                        <th class="cnrCell">Vehicle Limit Enable</th >
                        <th class="cnrCell">Email</th >
                         <th class="cnrCell">Confirmation Email</th >
                         <th class="cnrCell">Agent Duty Code</th >
                        <th class="cnrCell">Requestor ID</th >
                        <th class="cnrCell">RequestorID Type</th >
                         <th class="cnrCell">Company Name Code</th >
                          <th class="cnrCell">Code Context</th >
                       <th class="cnrCell">CarTrawler Allowed</th >
            			
                        
                        <th><a href="#" id="headercheck"><img src="images/icons/delete.png" width="15" height="15" alt="delete icon" /></a></th>
            		</tr>
            	</thead>
            	<tbody>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td><asp:Literal ID="rowID" runat="server" Text='<%# Eval("PartnerId")  %>' /></td>
        			<td><a href="updatePartners.aspx?Row=<%# Eval("PartnerId") %>"><%# Eval("Domainname")%></a></td>
                  <%--  <td><asp:Literal ID="Images" runat="server" Text='<%# Eval("VehicleLimtEnable")  %>' /></td>--%>
                   <%-- <td class="cnrCell"><%# Eval("VehicleLimtEnable")%></td>--%>
                      <td class="cnrCell"><%# YesNoWording(Convert.ToInt16(Eval("VehicleLimtEnable")))%></td>
                      <td class="cnrCell"><%# nullcheck(Convert.ToString(Eval("Email")))%></td>
                      <td class="cnrCell"><%# YesNoWording(Convert.ToInt16(Eval("ConfimationMailSend")))%></td>
                    <td class="cnrCell"><%# nullcheck(Convert.ToString(Eval("AgentDutyCode")))%></td>
                    <td class="cnrCell"><%# nullcheck(Convert.ToString(Eval("RequestorID_Type")))%></td>
                     <td class="cnrCell"><%# nullcheck(Convert.ToString(Eval("RequestorID_ID")))%></td>
                      <td class="cnrCell"><%# nullcheck(Convert.ToString(Eval("CompanyName_Code")))%></td>
                       <td class="cnrCell"><%# nullcheck(Convert.ToString(Eval("CodeContext")))%></td>
                    <td class="cnrCell"><%# nullcheck(Convert.ToString(Eval("CT_Allowed")))%></td>
       	 		
                  
                    <td class="cnrCell"><asp:CheckBox ID="rowcheck" runat="server" /></td>
        		</tr>
            </ItemTemplate>
            <FooterTemplate>
                </tbody>
                </table>
            </FooterTemplate>
        </asp:Repeater>
        

        <fieldset>
            <div class="formActions">
                <asp:Button ID="DeleteItems" CssClass="btn" runat="server" Text="Delete Selected Items" OnClick="DeleteItems_Click" />
            </div>
        </fieldset>
    </div> <!-- end actionArea -->
    <cb:ConfirmBox ID="ConfirmBox" runat="server" Visible="false" OnFireConfirm="Confirm_Fired" />
</div>
</asp:Content>

