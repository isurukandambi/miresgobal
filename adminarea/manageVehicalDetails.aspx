﻿<%@ Page Language="C#" MasterPageFile="adminarea.master" AutoEventWireup="true" CodeFile="manageVehicalDetails.aspx.cs" Inherits="manageLocations" %>

<%@ Register Src="~/adminarea/controls/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/adminarea/Controls/ConfirmBox.ascx" TagName="ConfirmBox" TagPrefix="cb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script src="js/imagepreviewer.js" type="text/javascript"></script>
    <style type="text/css">
        table.narrowTable {
            width: auto;
        }

        label.inline {
            width: 99px;
            margin: 0.2em 0.5em 0.5em 0;
            float: left;
            line-height: 2em;
        }
    </style>
    <script src="js/dataTables/jquery.dataTables.min.js" type="text/javascript"></script>
    <script type="text/javascript" language="javascript">
        $(document).ready(function () {
            //grid 1 settings
            $('#dataGrid').dataTable({
                "sDom": '<"dataTable_header"lf>rt<"dataTable_footer"ip>',
                "bAutoWidth": false,
                "sPaginationType": "full_numbers",
                "iDisplayLength": 50,
                "bStateSave": true,
                "iCookieDuration": 300,
                //customizing columns
                "aaSorting": [[1, "asc"]],
                "aoColumns": [
        			{ "bSearchable": false, "bVisible": false }, //setting first column to be hidden from searching and the user - required for knowing what database row to remove
                    { "sType": "html" }, //setting second column to search word of html link
                    null,
                    null,
                    null,
        			{ "bSortable": false } //setting last column not to sort onClick
                ]

            });
            //check all checkboxes if image clicked on
            $('#headercheck').click(function (evt) {
                evt.preventDefault();
                $('#dataGrid :checkbox').attr('checked', function () {
                    return !this.checked;
                });
            });

            //don't fire the delete button if no checkbox selected
            $('[id$=DeleteItems]').click(function (evt) {
                if ($("#dataGrid :checked").size() == 0) {
                    evt.preventDefault();
                }
            });
            if (<%=InformationTable.Items.Count%> != 0){
                $('#dataGrid_wrapper a').imgPreview({
                    containerID: 'imgPreviewWithStyles'
                });
            }
        });
        
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<div id="content">
    <h2><asp:Literal ID="PageName" runat="server" Text="Manage Vehicle Details" /></h2>
    <div class="buttons">
        <a href="addVehicalDetails.aspx?Country=<%=rowID() %>">Add Vehicle Details</a> 
        <a href="manageThermeonVehicleDetails.aspx">Manage Thermeon Vehicle Details</a> 
    </div>

    <uc1:MyMessageBox ID="MsgToShow" runat="server" ShowCloseButton="true" Visible="false" />

    <h3>Existing Vehicle Details:</h3>  
    <div id="actionArea">
       <fieldset>
                <div>
                    <label for="ddlCountries" class="inline">Filter By Country</label>
                    <asp:DropDownList ID="ddlCountries" runat="server" AutoPostBack="true" OnSelectedIndexChanged="CountryDDL_Changed" AppendDataBoundItems="true">
                    </asp:DropDownList>
                </div>
            </fieldset>
        <asp:Literal ID="NoRecordsFound" runat="server" Text="<p>There are no mapped vehicle details in the database or Country not selected from drop down.</p>" Visible="false" />
        <asp:Repeater ID="InformationTable" runat="server" Visible="false">
            <HeaderTemplate>
                <table cellpadding="0" cellspacing="0" border="0" id="dataGrid">
                    <thead>
                        <tr>
                            <th>RowID</th>
                            <th class="leftCell">Car Code</thclass="leftCell>
                            <th class="leftCell">Car Description</th>
                            <th class="leftCell">Car Image Path</th>
                            <th class="cnrCell">Brand Name</th>
                            <th><a href="#" id="headercheck"><img src="images/icons/delete.png" width="15" height="15" alt="delete icon" /></a></th>
                        </tr>
                    </thead>
                    <tbody>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td><asp:Literal ID="rowID" runat="server" Text='<%# Eval("VDBID")  %>' /></td>
                    <td><a href="updateVehicalDetails.aspx?Row=<%# Eval("VDBID") %>"><%# Eval("CarCode")%> </a></td>
                    <td class="leftCell"><%# Eval("CarDescription")%></td>
                    <td class="leftCell"><a href='../Pics/<%# FolderSelected()%>/<%# Eval("CarImagePath") %>' target="_blank" rel="nofollow" class="imagePreview"><asp:Literal ID="filenameWithoutLink" runat="server" Text='<%# Eval("CarImagePath") %>' /></td>
                    <td class="cnrCell"><%# Eval("BrandName")%></td>
                    <td class="cnrCell"><asp:CheckBox ID="rowcheck" runat="server" /></td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </tbody> </table>
            </FooterTemplate>
        </asp:Repeater>
        <fieldset>
            <div class="formActions">
                <asp:Button ID="DeleteItems" CssClass="btn" runat="server" Text="Delete Selected Vehicle Details" OnClick="DeleteItems_Click" Visible="false" />
            </div>
        </fieldset>
    </div>
    <cb:ConfirmBox ID="ConfirmBox" runat="server" Visible="false" OnFireConfirm="Confirm_Fired" />
</div>    
</asp:Content>