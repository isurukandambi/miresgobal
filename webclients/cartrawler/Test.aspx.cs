﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Clients_cartrawler_Test : System.Web.UI.Page
{
    AppHttpClient client;
    protected void Page_Load(object sender, EventArgs e)
    {
        Logger.Debug("Test......................");
        Response.ContentType = "text/xml";
        Response.ContentEncoding = System.Text.Encoding.UTF8;
        if (Request.QueryString["type"] == "ping")
        {
            createPingRequest();
        }
        else if (Request.QueryString["type"] == "vehavail")
        {
            createVehicleAvailabilityRequest();
        }
    }

    private void createVehicleAvailabilityRequest()
    {
        client = new AppHttpClient();

        string response = client.createPostRequest(AppSettings.CarTrawlerXmlEndPoint, getAvailabilityRequestBody());
        Response.Write(response);
    }

    private void createPingRequest()
    {
        client = new AppHttpClient();
        string response = client.createPostRequest(AppSettings.CarTrawlerXmlEndPoint, pingRequestBody());
        Response.Write(response);
    }

    private string pingRequestBody()
    {
        return "<?xml version=\"1.0\" encoding=\"UTF-8\" ?> " +
                    "<OTA_PingRQ " +
                    " xmlns=\"http://www.opentravel.org/OTA/2003/05\" " +
                    " xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" " +
                    " xsi:schemaLocation=\"http://www.opentravel.org/OTA/2003/05 OTA_PingRQ.xsd\" " +
                    " Target=\"Test\" Version=\"1.003\" PrimaryLangID=\"EN\"> " +
                    "  <EchoData>Hello!</EchoData> " +
                    "</OTA_PingRQ> ";
    }

    private string getAvailabilityRequestBody()
    {
        return "<?xml version=\"1.0\" encoding=\"UTF-8\"?> " +
 "<OTA_VehAvailRateRQ " +
 "  xmlns=\"http://www.opentravel.org/OTA/2003/05\" " +
 "  xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" " +
 "  xsi:schemaLocation=\"http://www.opentravel.org/OTA/2003/05 OTA_VehAvailRateRQ.xsd\" " +
 "  Target=\"Test\" Version=\"1.005\"> " +
 "  <POS> " +
 "    <Source ISOCurrency=\"EUR\"> " +
 "      <RequestorID Type=\"16\" ID=\"621233\" ID_Context=\"CARTRAWLER\" /> " +
 "    </Source> " +
 "  </POS> " +
 "  <VehAvailRQCore Status=\"Available\"> " +
 "    <VehRentalCore PickUpDateTime=\"2016-04-01T07:00:00\" ReturnDateTime=\"2016-04-09T19:00:00\"> " +
 "      <PickUpLocation CodeContext=\"CARTRAWLER\" LocationCode=\"71\" /> " +
 "      <ReturnLocation CodeContext=\"CARTRAWLER\" LocationCode=\"71\" /> " +
 "    </VehRentalCore> " +
 "    <DriverType Age='30'/> " +
 "  </VehAvailRQCore> " +
 "  <VehAvailRQInfo PassengerQty='3'> " +
 "    <Customer> " +
 "      <Primary> " +
 "        <CitizenCountryName Code='IE' /> " +
 "      </Primary> " +
 "    </Customer> " +
 "    <TPA_Extensions> " +
 "      <ConsumerIP>182.456.432.123</ConsumerIP> " +
 "    </TPA_Extensions> " +
 "  </VehAvailRQInfo> " +
 "</OTA_VehAvailRateRQ> ";
    }

    private string getExtrasAvailabilityRequestBody()
    {
        return "";
    }

    private string getReservationRequestBody()
    {
        return "";
    }
}