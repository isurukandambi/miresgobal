﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;

public partial class webclients_miresglobal_Test : System.Web.UI.Page
{
    AppHttpClient client;
    protected void Page_Load(object sender, EventArgs e)
    {
        Response.ContentType = "text/xml";
        Response.ContentEncoding = System.Text.Encoding.UTF8;
        if (Request.QueryString["type"] == "vehavail" && Request.QueryString["brand"] == "thrifty" && Request.QueryString["provider"] == "hertz")
        {
            createHertzVehicleAvailabilityRequest(Request.QueryString["brand"]);
        }
        else if (Request.QueryString["type"] == "vehavail" && Request.QueryString["brand"] == "hertz" && Request.QueryString["provider"] == "hertz")
        {
            createHertzVehicleAvailabilityRequest(Request.QueryString["brand"]);
        }
        else if (Request.QueryString["type"] == "vehavail" && Request.QueryString["brand"] == "firefly" && Request.QueryString["provider"] == "hertz")
        {
            createHertzVehicleAvailabilityRequest(Request.QueryString["brand"]);
        }
        if (Request.QueryString["type"] == "vehres" && Request.QueryString["brand"] == "thrifty" && Request.QueryString["provider"] == "hertz")
        {
            createHertzVehicleReservationRequest();
        }
        else if (Request.QueryString["type"] == "vehres" && Request.QueryString["brand"] == "hertz" && Request.QueryString["provider"] == "hertz")
        {
            createHertzVehicleReservationRequest();
        }
        else if (Request.QueryString["type"] == "vehres" && Request.QueryString["brand"] == "firefly" && Request.QueryString["provider"] == "hertz")
        {
            createHertzVehicleReservationRequest();
        }
        if (Request.QueryString["type"] == "vehavail" && Request.QueryString["brand"] == "thrifty" && Request.QueryString["provider"] == "cartrawler")
        {
            createCarTrawlerVehicleAvailabilityRequest();
        }
        else if (Request.QueryString["type"] == "vehavail" && Request.QueryString["brand"] == "hertz" && Request.QueryString["provider"] == "cartrawler")
        {
            createCarTrawlerVehicleAvailabilityRequest();
        }
        else if (Request.QueryString["type"] == "vehavail" && Request.QueryString["brand"] == "firefly" && Request.QueryString["provider"] == "cartrawler")
        {
            createCarTrawlerVehicleAvailabilityRequest();
        }
    }

    private void createCarTrawlerVehicleAvailabilityRequest()
    {
        client = new AppHttpClient();
        string response = client.createPostRequest("http://dev.miresglobal.com/thrifty", getCartrawlerAvailabilityRequestBody());
        Response.Write(response);
    }

    private void createHertzVehicleAvailabilityRequest(string brand)
    {
        client = new AppHttpClient();
        string response = client.createPostRequest("http://dev.miresglobal.com/" + brand, getHertzAvailabilityRequestBody());
        Response.Write(response);
    }

    private void createHertzVehicleReservationRequest()
    {
        client = new AppHttpClient();
        string response = client.createPostRequest("http://dev.miresglobal.com/hertz", getHertzReservationRequestBody());
        Response.Write(response);
    }

    private string getHertzAvailabilityRequestBody()
    {
        return "<OTA_VehAvailRateRQ xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns=\"http://www.opentravel.org/OTA/2003/05\" xsi:schemaLocation=\"http://www.opentravel.org/OTA/2003/05 OTA_VehAvailRateRQ.xsd\" Version=\"1.008\" MaxResponses=\"6\" > " +
 "  <POS> " +
 "    <Source ISOCountry=\"IE\" AgentDutyCode=\"R18Y25A114N\"> " +
 "      <RequestorID Type=\"4\" ID=\"T390\"> " +
 "        <CompanyName Code=\"CP\" CodeContext=\"XM77\"></CompanyName> " +
 "    </RequestorID> " +
 "  </Source> " +
 "</POS> " +
 "  <VehAvailRQCore Status=\"All\"> " +
 "    <VehRentalCore PickUpDateTime=\"2015-10-14T09:00:00\" ReturnDateTime=\"2015-10-17T16:00:00\"> " +
 "      <PickUpLocation LocationCode=\"SZG\" CodeContext=\"IATA\"></PickUpLocation> " +
 "      <ReturnLocation LocationCode=\"SZG\" CodeContext=\"IATA\"></ReturnLocation> " +
 "  </VehRentalCore> " +
 "    <RateQualifier CorpDiscountNmbr=\"709295\" RateQualifier=\"PPP\"> " +
 "  </RateQualifier> " +
 "</VehAvailRQCore> " +
 "  <VehAvailRQInfo> " +
 "    <ArrivalDetails TransportationCode=\"14\"> " +
 "      <OperatingCompany Code=\"FR\"> " +
 "    </OperatingCompany> " +
 "  </ArrivalDetails> " +
 "</VehAvailRQInfo> " +
 "</OTA_VehAvailRateRQ>";
    }

    private string getHertzReservationRequestBody()
    {
        return "<?xml version=\"1.0\" encoding=\"UTF-8\"?> " +
 "<OTA_VehResRQ xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns=\"http://www.opentravel.org/OTA/2003/05\" xsi:schemaLocation=\"http://www.opentravel.org/OTA/2003/05 OTA_VehResRQ.xsd\" Version=\"1.008\" > " +
 "  <POS> " +
 "    <Source ISOCountry=\"IE\" AgentDutyCode=\"R18Y25A114N\"> " +
 "      <RequestorID Type=\"4\" ID=\"T390\"> " +
 "        <CompanyName Code=\"CP\" CodeContext=\"XM77\"> " +
 "      </CompanyName> " +
 "    </RequestorID> " +
 "  </Source> " +
 "</POS> " +
 "  <VehResRQCore Status=\"All\"> " +
 "    <VehRentalCore PickUpDateTime=\"2015-10-14T09:00:00\" ReturnDateTime=\"2015-10-17T16:00:00\"> " +
 "      <PickUpLocation LocationCode=\"DUB\" CodeContext=\"IATA\"> " +
 "    </PickUpLocation> " +
 "      <ReturnLocation LocationCode=\"DUB\" CodeContext=\"IATA\"> " +
 "    </ReturnLocation> " +
 "  </VehRentalCore> " +
 "    <Customer> " +
 "      <Primary> " +
 "        <PersonName> " +
 "          <GivenName>Sajith</GivenName> " +
 "          <Surname>Dulanga</Surname> " +
 "        </PersonName> " +
 "        <Telephone PhoneTechType=\"1\" AreaCityCode=\"4\" PhoneNumber=\"94772531888\" FormattedInd=\"false\" DefaultInd=\"false\"> " +
 "      </Telephone> " +
 "        <Email DefaultInd=\"false\">dmstestreply@gmail.com</Email> " +
 "        <Address DefaultInd=\"false\" FormattedInd=\"false\"> " +
 "          <AddressLine>Colombo, Colombo</AddressLine> " +
 "          <CityName>Colombo</CityName> " +
 "          <PostalCode>12345</PostalCode> " +
 "          <CountryName Code=\"IE\"> " +
 "        </CountryName> " +
 "      </Address> " +
 "    </Primary> " +
 "  </Customer> " +
 "    <RateQualifier ArriveByFlight=\"false\" CorpDiscountNmbr=\"709295\" RateQualifier=\"\"> " +
 "  </RateQualifier> " +
 "</VehResRQCore> " +
 "  <VehResRQInfo GasPrePay=\"false\" SmokingAllowed=\"false\"> " +
 "    <ArrivalDetails TransportationCode=\"14\" Number=\"1931\"> " +
 "      <OperatingCompany Code=\"FR\"> " +
 "    </OperatingCompany> " +
 "  </ArrivalDetails> " +
 "    <RentalPaymentPref> " +
 "      <PaymentCard CardType=\"1\" CardCode=\"VI\" CardNumber=\"4012000033330026\" ExpireDate=\"1216\"> " +
 "    </PaymentCard> " +
 "  </RentalPaymentPref> " +
 "    <Reference Type=\"16\" ID=\"RTU5YB8TCU46046-2502\"> " +
 "  </Reference> " +
 "    <WrittenConfInst LanguageID=\"EN        IE\" ConfirmInd=\"true\"> " +
 "      <Email DefaultInd=\"true\">dmstestreply@gmail.com</Email> " +
 "    </WrittenConfInst> " +
 "  </VehResRQInfo> " +
 "</OTA_VehResRQ> ";
    }

    private string getCartrawlerAvailabilityRequestBody()
    {
        return "<OTA_VehAvailRateRQ xmlns=\"http://www.opentravel.org/OTA/2003/05\"  " +
 "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"  " +
 "xsi:schemaLocation=\"http://www.opentravel.org/OTA/2003/05  " +
 "OTA_VehAvailRateRQ.xsd\" Version=\"1.008\" MaxResponses=\"10\"> " +
 "  <POS> " +
 "    <Source ISOCountry=\"IE\" AgentDutyCode=\"R18Y25A114N\"> " +
 "      <RequestorID Type=\"4\" ID=\"T383\"> " +
 "        <CompanyName Code=\"CP\" CodeContext=\"B4IQ\" /> " +
 "      </RequestorID> " +
 "    </Source> " +
 "    <Source> " +
 "      <RequestorID Type=\"5\" ID=\"74004604\" /> " +
 "    </Source> " +
 "  </POS> " +
 "  <VehAvailRQCore Status=\"All\"> " +
 "    <VehRentalCore PickUpDateTime=\"2015-10-11T10:30:00\" ReturnDateTime=\"2015-" +
 "10-12T10:30:00\"> " +
 "      <PickUpLocation LocationCode=\"DUB\" CodeContext=\"IATA\" /> " +
 "      <ReturnLocation LocationCode=\"DUB\" CodeContext=\"IATA\" /> " +
 "    </VehRentalCore> " +
 "    <RateQualifier TravelPurpose=\"2\" RateQualifier=\"PPP\" /> " +
 "  </VehAvailRQCore> " +
 "  <VehAvailRQInfo> " +
 "    <ArrivalDetails TransportationCode=\"14\"> " +
 "      <OperatingCompany Code=\"FR\" /> " +
 "    </ArrivalDetails> " +
 "    <Vendor Code=\"ZE\" /> " +
 "  </VehAvailRQInfo> " +
 "</OTA_VehAvailRateRQ> ";
    }
}