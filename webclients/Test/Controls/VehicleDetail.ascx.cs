﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TestClient;

public partial class Test_Controls_VehicleDetail : System.Web.UI.UserControl
{
    private Booking _booking = new Booking();
    private Car _selectedCar = new Car();
    protected void Page_Load(object sender, EventArgs e)
    {
        _booking = _booking.Session;
        LoadCar();
    }
    protected void LoadCar()
    {
        string brand = Request.QueryString["Brand"].ToString();
        string sippCode = Request.QueryString["Car"].ToString();
        var brandSelected = _booking.Brands.Where(b => b.CarProvider.ToString() == brand).SingleOrDefault();
        if (brandSelected != null)
        {
            var car = brandSelected.Cars.Where(c => c.SippCode == sippCode);
            VehicleDetailContainer.DataSource = car;
            VehicleDetailContainer.DataBind();
            _selectedCar = car.FirstOrDefault();
            _selectedCar.IsSelected = true;
            _booking.Session = _booking;
        }
    }
}