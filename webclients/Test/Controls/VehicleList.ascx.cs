﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using TestClient;

public partial class Test_Controls_VehicleList : System.Web.UI.UserControl
{
    private Booking _booking = new Booking();
    protected void Page_Load(object sender, EventArgs e)
    {
        _booking = _booking.Session;
        populateVehiclesGrid();
    }
    protected void populateVehiclesGrid()
    {
        var thriftyBrandCars = _booking.Brands.Where(b => b.CarProvider == CarProvider.Thrifty).SingleOrDefault();

        if (thriftyBrandCars != null)
        {
            var cars = thriftyBrandCars.Cars.Where(c => c.IsResRetReturnedCar == false);
            ThriftyCarResultsContainer.DataSource = cars;
            ThriftyCarResultsContainer.DataBind();
        }

        var hertzBrandCars = _booking.Brands.Where(b => b.CarProvider == CarProvider.Hertz).SingleOrDefault();

        if (hertzBrandCars != null)
        {
            var cars = hertzBrandCars.Cars.Where(c => c.IsResRetReturnedCar == false);
            HertzCarResultsContainer.DataSource = cars;
            HertzCarResultsContainer.DataBind();
        }

        var fireflyBrandCars = _booking.Brands.Where(b => b.CarProvider == CarProvider.FireFly).SingleOrDefault();

        if (fireflyBrandCars != null)
        {
            var cars = fireflyBrandCars.Cars.Where(c => c.IsResRetReturnedCar == false);
            FireflyCarResultsContainer.DataSource = cars;
            FireflyCarResultsContainer.DataBind();
        }

        var dollarBrandCars = _booking.Brands.Where(b => b.CarProvider == CarProvider.Dollar).SingleOrDefault();

        if (dollarBrandCars != null)
        {
            var cars = dollarBrandCars.Cars.Where(c => c.IsResRetReturnedCar == false);
            DollarCarResultsContainer.DataSource = cars;
            DollarCarResultsContainer.DataBind();
        }

        if (!string.IsNullOrEmpty(_booking.ErrorDetails.ErrorMsg))
        {
            errorsContainer.Visible = true;
            ErrorList.Text = _booking.ErrorDetails.ErrorMsg;
        }

    }

}
