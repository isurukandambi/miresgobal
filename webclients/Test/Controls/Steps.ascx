﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Steps.ascx.cs" Inherits="Test_Controls_Steps" %>
<style type="text/css">
.step {
    text-align: center;
    margin-left: 0px;
    margin-bottom: 10px;
    margin-right: 0px;
}

.step .col-md-2 {
    background-color: #fff;
    border: 1px solid #C0C0C0;
    border-right: none;
    height: 118px;
}

.step .col-md-2:last-child {
    border: 1px solid #C0C0C0;
}

.step .col-md-2:first-child {
    border-radius: 5px 0 0 5px;
}

.step .col-md-2:last-child {
    border-radius: 0 5px 5px 0;
}

.step .col-md-2:hover {
    color: #F58723;
    cursor: pointer;
}

.step .activestep {
    color: #F58723;
    margin-top: 0px;
    padding-top: 7px;
    border-left: 6px solid #5CB85C !important;
    border-right: 6px solid #5CB85C !important;
    border-top: 3px solid #5CB85C !important;
    border-bottom: 3px solid #5CB85C !important;
    vertical-align: central;
}

.step .glyphicon {
    padding-top: 15px;
    font-size: 40px;
    color: #424242;
}

</style>
<div class="row step">
    <div runat="server" id="step1" class="col-md-2 col-md-offset-2">
        <a href="/Test/Test.aspx"><span class="glyphicon glyphicon-pencil"></span></a>
        <p>Make Reservation</p>
    </div>
    <div runat="server" id="step2" class="col-md-2">
        <a href="/Test/Test.aspx"><span class="glyphicon glyphicon-circle-arrow-right"></span></a>
        <p>Choose Vehicle</p>
    </div>
    <div runat="server" id="step3" class="col-md-2">
        <span class="glyphicon glyphicon-plus"></span>
        <p>Add Accessories</p>
    </div>
    <div runat="server" id="step4" class="col-md-2">
        <span class="glyphicon glyphicon-shopping-cart"></span>
        <p>Pay</p>
    </div>
</div>