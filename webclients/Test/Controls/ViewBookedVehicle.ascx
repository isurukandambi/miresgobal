﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ViewBookedVehicle.ascx.cs" Inherits="ViewBookedVehicle" %>
<div class="row">
    <script>
        function myFunction() {

            var r = confirm("If you want to Cancel Booking");
            if (r == true) {

            } else {
                return false;
            }
        }
     </script>
     <asp:Image runat="server" ImageUrl="" ID="brandImg" ImageAlign="Right" Width="75px" CssClass="brandLogo" />
    <asp:Repeater ID="VehicleDetailContainer" runat="server">
        <ItemTemplate>
            <div class="panel panel-default">
                <div class="panel-heading">
                      
                    <h3 style="width: 60%;" class="panel-title">Your Selected Car</h3>
                </div>
                <div class="panel-body">

                    <div class="media">
                        <a class="pull-left" href="#">
                            <img class="media-object" alt="128x128" src="<%#Eval("Image") %>" style="width: 200px;">
                        </a>
                        <div class="media-body">
                            <div class="row">
                                <div class="col-xs-12 col-md-8">
                                    <h4 class="media-heading"><%#Eval("Name") %> - <%#Eval("SippCode") %></h4>
                                    <p><%#Eval("VehClassSize") %> Passengers, <%#Eval("DoorCount") %> Doors, <%#Eval("TransmissionType") %></p>
                                </div>
                                <div class="col-xs-12 col-md-4">
                                    <div class="row">
                                        <div class="col-xs-12 col-md-11" style="border: 1px #ccc solid; border-radius: 5px;">
                                            <h2 style="text-align: center; margin-top: 5px;">
                                                <%#Eval("Price") %>
                                            </h2>
                                            <p style="text-align: center; font-size: 11px; margin-top: 3px;"><%#Eval("Currency") %> <%#Eval("Price") %> per day</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
             </div>

        </ItemTemplate>
    </asp:Repeater>

                <div class="panel panel-default" runat="server" id="bookingDtl">

                <div class="panel-heading">
                    <h3 style="width: 60%;" class="panel-title">Your reservation confirmation number is <asp:Literal runat="server" ID="BookingConfNoText"></asp:Literal></h3>
                </div>
                <div class="panel-body">
                    <p> 
                        You may modify some of your booking details or cancel the booking entirely.
                        <br />
                        Before Modifying or Cancelling your reservation, please confirm that you understand and accept our Rental qualifications and requirements.
                    </p>
                    <asp:Button CssClass="btn btn-primary" Text="Start a New Reservation" OnClick="startANewReservationBtn_Clicked" runat="server" />
                    <asp:Button CssClass="btn btn-warning pull-right" Text="Modify Reservation" OnClick="modifyReservationBtn_Clicked" runat="server" />
                    <asp:Button CssClass="btn btn-warning pull-right push-margin" Text="Cancel Reservation" OnClientClick="myFunction()" OnClick="cancelReservationBtn_Clicked" runat="server" />
                </div>
            </div>

           <div class="panel panel-default" id="CancleMsg" runat="server">

                <div class="panel-heading">
                    <h3 style="width: 60%;" class="panel-title"> <asp:Literal runat="server" ID="CancleConfId"></asp:Literal></h3>
                </div>
                <div class="panel-body">
                  <div class="row" style="margin-left: 0px; margin-right: 0px;" runat="server" visible="false" id="ValidationMessage">
                        <div class="alert alert-info" role="alert" style="margin-top: 50px;">
                            <asp:Literal runat="server" ID="AlertMessage"></asp:Literal>
                        </div>
                     </div>
                <asp:Button CssClass="btn btn-primary" Text="Start a New Reservation" OnClick="startANewReservationBtn_Clicked" runat="server"  />
                </div>
            </div>

</div>