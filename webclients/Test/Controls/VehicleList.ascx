﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="VehicleList.ascx.cs" Inherits="Test_Controls_VehicleList" %>
<div class="col-xs-12 col-md-12">
    <div class="alert alert-error" runat="server" id="errorsContainer" visible="false"><asp:Literal ID="ErrorList" runat="server"></asp:Literal></div>
    <asp:Repeater ID="ThriftyCarResultsContainer" runat="server">
        <ItemTemplate>
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <img src="../Test/Images/thrifty-header-logo.gif" style="width: 75px; float: right; margin-top: -5px;" />
                    <h3 style="width: 60%;" class="panel-title"><%#Eval("Name") %> - <%#Eval("SippCode") %></h3>
                </div>
                <div class="panel-body">
                    <div class="media">
                        <a class="pull-left" href="#">
                            <img class="media-object" alt="128x128" src="<%#Eval("Image") %>" style="width: 200px;">
                        </a>
                        <div class="media-body">
                            <div class="row">
                                <div class="col-xs-12 col-md-8">
                                    <h4 class="media-heading"><%#Eval("Name") %></h4>
                                    <p><%#Eval("VehClassSize") %> Passengers, <%#Eval("DoorCount") %> Doors, <%#Eval("TransmissionType") %></p>
                                </div>
                                <div class="col-xs-12 col-md-4">
                                    <div class="row">
                                        <div class="col-xs-12 col-md-10" style="border: 1px #ccc solid; border-radius: 5px;">
                                            <h2 style="text-align: center; margin-top: 5px;">
                                                <%#Eval("Price") %>
                                            </h2>
                                            <p style="text-align: center; font-size: 11px; margin-top: 3px;"><%#Eval("Currency") %> <%#Eval("Price") %> per day</p>
                                            <a href="Extras.aspx?Brand=Thrifty&Car=<%# Eval("SippCode") %>&PrePaid=<%# (bool)Eval("IsPrePaid") ? "y" : "n" %>"" class="btn btn-warning" style="width: 100%; margin-bottom: 5px;">BOOK</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ItemTemplate>
    </asp:Repeater>
    <asp:Repeater ID="HertzCarResultsContainer" runat="server">
        <ItemTemplate>
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <img src="../Test/Images/hertz-header-logo.gif" style="width: 75px; float: right; margin-top: -5px;" />
                    <h3 style="width: 60%;" class="panel-title"><%#Eval("Name") %> - <%#Eval("SippCode") %></h3>
                </div>
                <div class="panel-body">
                    <div class="media">
                        <a class="pull-left" href="#">
                            <img class="media-object" alt="128x128" src="<%#Eval("Image") %>" style="width: 200px;">
                        </a>
                        <div class="media-body">
                            <div class="row">
                                <div class="col-xs-12 col-md-8">
                                    <h4 class="media-heading"><%#Eval("Name") %></h4>
                                    <p><%#Eval("VehClassSize") %> Passengers, <%#Eval("DoorCount") %> Doors, <%#Eval("TransmissionType") %></p>
                                </div>
                                <div class="col-xs-12 col-md-4">
                                    <div class="row">
                                        <div class="col-xs-12 col-md-10" style="border: 1px #ccc solid; border-radius: 5px;">
                                            <h2 style="text-align: center; margin-top: 5px;">
                                                <%#Eval("Price") %>
                                            </h2>
                                            <p style="text-align: center; font-size: 11px; margin-top: 3px;"><%#Eval("Currency") %> <%#Eval("Price") %> per day</p>
                                            <a href="Extras.aspx?Brand=Hertz&Car=<%# Eval("SippCode") %>&PrePaid=<%# (bool)Eval("IsPrePaid") ? "y" : "n" %>"" class="btn btn-warning" style="width: 100%; margin-bottom: 5px;">BOOK</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ItemTemplate>
    </asp:Repeater>
    <asp:Repeater ID="FireflyCarResultsContainer" runat="server">
        <ItemTemplate>
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <img src="../Test/Images/firefly-header-logo.gif" style="width: 75px; float: right; margin-top: -5px;" />
                    <h3 style="width: 60%;" class="panel-title"><%#Eval("Name") %> - <%#Eval("SippCode") %></h3>
                </div>
                <div class="panel-body">
                    <div class="media">
                        <a class="pull-left" href="#">
                            <img class="media-object" alt="128x128" src="<%#Eval("Image") %>" style="width: 200px;">
                        </a>
                                                <div class="media-body">
                            <div class="row">
                                <div class="col-xs-12 col-md-8">
                                    <h4 class="media-heading"><%#Eval("Name") %></h4>
                                    <p><%#Eval("VehClassSize") %> Passengers, <%#Eval("DoorCount") %> Doors, <%#Eval("TransmissionType") %></p>
                                </div>
                                <div class="col-xs-12 col-md-4">
                                    <div class="row">
                                        <div class="col-xs-12 col-md-10" style="border: 1px #ccc solid; border-radius: 5px;">
                                            <h2 style="text-align: center; margin-top: 5px;">
                                                <%#Eval("Price") %>
                                            </h2>
                                            <p style="text-align: center; font-size: 11px; margin-top: 3px;"><%#Eval("Currency") %> <%#Eval("Price") %> per day</p>
                                            <a href="Extras.aspx?Brand=FireFly&Car=<%# Eval("SippCode") %>&PrePaid=<%# (bool)Eval("IsPrePaid") ? "y" : "n" %>"" class="btn btn-warning" style="width: 100%; margin-bottom: 5px;">BOOK</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ItemTemplate>
    </asp:Repeater>
    <asp:Repeater ID="DollarCarResultsContainer" runat="server">
        <ItemTemplate>
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <img src="../Test/Images/dollar-header-logo.gif" style="width: 75px; float: right; margin-top: -5px;" />
                    <h3 style="width: 60%;" class="panel-title"><%#Eval("Name") %> - <%#Eval("SippCode") %></h3>
                </div>
                <div class="panel-body">
                    <div class="media">
                        <a class="pull-left" href="#">
                            <img class="media-object" alt="128x128" src="<%#Eval("Image") %>" style="width: 200px;">
                        </a>
                                                <div class="media-body">
                            <div class="row">
                                <div class="col-xs-12 col-md-8">
                                    <h4 class="media-heading"><%#Eval("Name") %></h4>
                                    <p><%#Eval("VehClassSize") %> Passengers, <%#Eval("DoorCount") %> Doors, <%#Eval("TransmissionType") %></p>
                                </div>
                                <div class="col-xs-12 col-md-4">
                                    <div class="row">
                                        <div class="col-xs-12 col-md-10" style="border: 1px #ccc solid; border-radius: 5px;">
                                            <h2 style="text-align: center; margin-top: 5px;">
                                                <%#Eval("Price") %>
                                            </h2>
                                            <p style="text-align: center; font-size: 11px; margin-top: 3px;"><%#Eval("Currency") %> <%#Eval("Price") %> per day</p>
                                            <a href="Extras.aspx?Brand=Dollar&Car=<%# Eval("SippCode") %>&PrePaid=<%# (bool)Eval("IsPrePaid") ? "y" : "n" %>"" class="btn btn-warning" style="width: 100%; margin-bottom: 5px;">BOOK</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ItemTemplate>
    </asp:Repeater>
</div>