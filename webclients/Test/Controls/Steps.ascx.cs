﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TestClient;

public partial class Test_Controls_Steps : System.Web.UI.UserControl
{
    private Booking _booking = new Booking();
    protected void Page_Load(object sender, EventArgs e)
    {
        _booking = _booking.Session;
        //activestep
        string sPagePath = System.Web.HttpContext.Current.Request.Url.AbsolutePath;
        System.IO.FileInfo oFileInfo = new System.IO.FileInfo(sPagePath);
        string sPageName = oFileInfo.Name;
        //Response.Write(sPageName);
        switch (sPageName)
        {
            case "default.aspx":
                if (string.IsNullOrEmpty(_booking.CountryOfResidenceCode))
                {
                    step1.Attributes.Add("class", "col-md-2 col-md-offset-2 activestep");
                }
                else
                {
                    step2.Attributes.Add("class", "col-md-2 activestep");
                }
                break;
            case "Extras.aspx":
                step3.Attributes.Add("class", "col-md-2 activestep");
                break;
            case "Review.aspx":
                step4.Attributes.Add("class", "col-md-2 activestep");
                break;
            default:
                step1.Attributes.Add("class", "col-md-2 col-md-offset-2 activestep");
                break;
        }
    }
}