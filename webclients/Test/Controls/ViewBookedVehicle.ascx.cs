﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using TestClient;
using CMSConfigs;

public partial class ViewBookedVehicle : System.Web.UI.UserControl
{
    private Booking _booking = new Booking();
    private Car _selectedCar = new Car();
    private CarProvider _selectedbrand = new CarProvider();
    protected void Page_Load(object sender, EventArgs e)
    {
        _booking = _booking.Session;

        if (_booking.ErrorDetails.ErrorMsg != null)
        {
            bookingDtl.Visible = false;
            ValidationMessage.Visible = true;
            AlertMessage.Text = _booking.ErrorDetails.ErrorMsg;
            brandImg.Visible = false;
        }
        else
        {
            //CommonTasks.SendXmlForDebugging(_selectedbrand.ToString(), "Check Book");
            CancleMsg.Visible = false;
            //Response.Write(_booking.Brands.FirstOrDefault().Cars.FirstOrDefault().TransmissionType);
            LoadCar();
            LoadConfId();

             //Response.Write("Conf ID " + _booking.ConfirmationId+"<br>");
             //Response.Write("SurName " + _booking.Customer.Surname+"<br>");
             //Response.Write("Agent " + _booking.agentDutyCode + "<br>");
             //Response.Write("Country " + _booking.CountryOfResidenceCode + "<br>");
        }

    }
    protected void LoadConfId()
    {
        BookingConfNoText.Text = _booking.ConfirmationId;
    }
    protected void LoadCar()
    {
        if (_booking.OriginalBooking.CarProvider == CarProvider.Hertz)
        {
            brandImg.ImageUrl = "../Images/hertz-header-logo.gif";
        }
        else if (_booking.OriginalBooking.CarProvider == CarProvider.FireFly)
        {
            brandImg.ImageUrl = "../Images/firefly-header-logo.gif";
        }
        else if (_booking.OriginalBooking.CarProvider == CarProvider.Thrifty)
        {
            brandImg.ImageUrl = "../Images/thrifty-header-logo.gif";
        }
        else if (_booking.OriginalBooking.CarProvider == CarProvider.Dollar)
        {
            brandImg.ImageUrl = "../Images/dollar-header-logo.gif";
        }

        //brandImg.ImageUrl = _selectedCar.Image;
        
        var carSelected = _booking.OriginalBooking.BookedCar;

        if (carSelected != null)
        {
            List<Car> cars = new List<Car>();
            cars.Add(carSelected);
            VehicleDetailContainer.DataSource = cars;
            VehicleDetailContainer.DataBind();

            _booking.Session = _booking;
        }
    }
    protected void startANewReservationBtn_Clicked(object sender, EventArgs e)
    {
        _booking.Session = new Booking();
        Response.Redirect("/webclients/Test/Test.aspx");
    }

    public void cancel_Booking()
    {
        DateTime requestTime = DateTime.Now;
        string bookingResponse = "";
        _booking.ModifyProcess = true;
        if (_booking.ModifyProcess == true)
        {
            //Call Cancle Reservation......
            //string cancleRequestRyanair = "<OTA_VehCancelRQ xmlns=\"http://www.opentravel.org/OTA/2003/05\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.opentravel.org/OTA/2003/05 OTA_VehCancelRQ.xsd\" Version=\"1.008\"><POS> <Source ISOCountry=\"IE\" AgentDutyCode=\"R18Y25A114N\"> <RequestorID Type=\"4\" ID=\"T390\"> <CompanyName Code=\"CP\" CodeContext=\"XM77\" /> </RequestorID> </Source> <Source> <RequestorID Type=\"5\" ID=\"74004604\" /> </Source> </POS> <VehCancelRQCore> <UniqueID Type=\"14\" ID=\"" + _booking.ConfirmationId + "\" /><PersonName><Surname>" + _booking.Customer.Surname + "</Surname></PersonName></VehCancelRQCore></OTA_VehCancelRQ>";

            //string cancleRequest = "<?xml version=\"1.0\" encoding=\"utf-8\"?><OTA_VehCancelRQ xmlns=\"http://www.opentravel.org/OTA/2003/05\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" version=\"1.008\" xsi:schemaLocation=\"http://www.opentravel.org/OTA/2003/05 OTA_VehCancelRQ.xsd\"><POS><Source AgentDutyCode=\"" + _booking.PartnerDetails.agentDutyCode + "\" ISOCountry=\"GR\"><RequestorID Type=\"" + _booking.PartnerDetails.requestorIDType + "\" ID=\"" + _booking.PartnerDetails.requestorID + "\"><CompanyName Code=\"" + _booking.PartnerDetails.companyNameCode + "\" CodeContext=\"" + _booking.PartnerDetails.codeContext + "\" /></RequestorID></Source><Source><RequestorID Type=\"5\" ID=\"00109911\" /></Source></POS><VehCancelRQCore CancelType=\"Book\"><UniqueID Type=\"14\" ID=\"" + _booking.ConfirmationId + "\" /><PersonName><Surname>" + _booking.Customer.Surname + "</Surname></PersonName></VehCancelRQCore></OTA_VehCancelRQ>";
            string cancelRequest = "<OTA_VehCancelRQ xmlns=\"http://www.opentravel.org/OTA/2003/05\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.opentravel.org/OTA/2003/05 OTA_VehCancelRQ.xsd\" Version=\"1.008\">" +
                    "<POS>" +
                    "<Source ISOCountry=\"" + _booking.CountryOfResidenceCode + "\" AgentDutyCode=\"" + _booking.PartnerDetails.agentDutyCode + "\">" +
                      "<RequestorID Type=\"" + _booking.PartnerDetails.requestorIDType + "\" ID=\"" + _booking.PartnerDetails.requestorID + "\">" +
                        "<CompanyName Code=\"" + _booking.PartnerDetails.companyNameCode + "\" CodeContext=\"" + _booking.PartnerDetails.codeContext + "\" />" +
                      "</RequestorID>" +
                    "</Source>" +
                    "<Source>" +
                      "<RequestorID Type=\"5\" ID=\"74004604\" />" +
                    "</Source>" +
                    "</POS>" +
                    "<VehCancelRQCore>" +
                    "<UniqueID Type=\"14\" ID=\"" + _booking.ConfirmationId + "\"/>" +
                    "<PersonName>" +
                      "<Surname>" + _booking.Customer.Surname + "</Surname>" +
                    " </PersonName>" +
                    "</VehCancelRQCore>" +
                    "</OTA_VehCancelRQ>";

            bookingResponse = SendXml.HttpPost("http://" + CMSSettings.SiteUrl + "/" + _selectedbrand.ToString().ToLower(), cancelRequest, _selectedbrand.ToString(), "VehCancelRQ");
        }

        var xmlDoc = new XmlDocument();
        xmlDoc.LoadXml(bookingResponse);

        var namespaceMgr = new XmlNamespaceManager(xmlDoc.NameTable);
        namespaceMgr.AddNamespace("a1", "http://www.opentravel.org/OTA/2003/05");

        _booking.ErrorDetails.ErrorMsg = "";
        _booking.ErrorDetails.ErrorCode = "";
        XmlNodeList errorList = xmlDoc.DocumentElement.SelectNodes(".//a1:Error", namespaceMgr);
        if (errorList.Count > 0)
        {
            //CommonTasks.SendXmlForDebugging(bookingResponse, " Check is errors");
            //CommonTasks.SendXmlForDebugging(errorList.Count.ToString(), " Check is errors count");
            foreach (XmlNode node in errorList)
            {
                if (node != null)
                {
                    if (node.Attributes != null)
                    {
                        _booking.ErrorDetails.ErrorMsg += node.Attributes["ShortText"].Value + "</br>";
                        try
                        {
                            //CommonTasks.SendXmlForDebugging("Error2", " Check is errors");
                            _booking.ErrorDetails.ErrorCode = node.Attributes["Code"].Value;
                        }
                        catch
                        {

                        }

                    }
                }

            }
        }

        _booking.Session = _booking;

        if (!string.IsNullOrEmpty(_booking.ErrorDetails.ErrorCode))
        {
            bookingDtl.Visible = false;
            CancleMsg.Visible = true;
            ValidationMessage.Visible = true;
            AlertMessage.Text = _booking.ErrorDetails.ErrorMsg;

        }
        else
        {
            bookingDtl.Visible = false;
            CancleMsg.Visible = true;
            ValidationMessage.Visible = true;
            AlertMessage.Text = "Your Booking Cancelled";
        }
    }

    protected void cancelReservationBtn_Clicked(object sender, EventArgs e)
    {
        cancel_Booking();
    }

    public string DateTimeForXml(DateTime date)
    {
        return date.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss");
    }

    protected void modifyReservationBtn_Clicked(object sender, EventArgs e)
    {
        _booking = _booking.Session;
        _booking.ModifyProcess = true;
        _booking.Session = _booking;

        Response.Redirect("/webclients/Test/Test.aspx");
    }

}