﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using ORM;
using System.Globalization;
using System.IO;
using System.Xml.Serialization;
using System.ServiceModel.Channels;
using CMSConfigs;
using TestClient;
using System.Net;
using System.Net.Sockets;

public partial class Test_Controls_BookingForm : System.Web.UI.UserControl
{
    private DatabaseEntities db;
    private static readonly string XmlNamespace = "http://www.opentravel.org/OTA/2003/05";
    private XmlDocument thriftyVehAvailRateRSXML;
    private XmlDocument thermeonVehAvailRateRSXML;
    private XmlDocument fireflyVehAvailRateRSXML;
    private XmlDocument hertzVehAvailRateRSXML;
    private XmlDocument dollarVehAvailRateRSXML;
    private XmlDocument crosssellRateRSXML;
    private XmlDocument vehicleRetResRSXML;
    string Partner;

    private XmlDocument vehicleModificationRequest;

    private Booking _booking = new Booking();
    private BrandT _brandSelected = new BrandT();
    protected void Page_Load(object sender, EventArgs e)
    {
        _booking = _booking.Session;
        if (!Page.IsPostBack)
        {
            pickupDate.Text = DateTime.Now.AddDays(3).ToString("yyyy-MM-dd");
            dropoffDate.Text = DateTime.Now.AddDays(6).ToString("yyyy-MM-dd");
            LoadCountries();
            LoadLanguages();
            LoadData();
            LoadPartner();
        }
    }

    protected void LoadData()
    {
        if (_booking.ModifyProcess == true)
        {
            pickupCountry.SelectedValue = _booking.PickUpCountryCode;
            LoadLocations(pickupLocation, pickupCountry.SelectedValue);
            pickupLocation.SelectedValue = _booking.PickUpLocationCode;
            dropoffCountry.SelectedValue = _booking.DropOffCountryCode;
            LoadLocations(dropoffLocation, dropoffCountry.SelectedValue);
            dropoffLocation.SelectedValue = _booking.DropOffLocationCode;
            selectpartner.SelectedValue = _booking.agentDutyCode;
            dropoffCountry.Enabled = false;
            dropoffLocation.Enabled = false;
            pickupCountry.Enabled = false;
            pickupLocation.Enabled = false;
            CountryOfResidenceContainer.Visible = true;
            countryofResidence.SelectedValue = _booking.CountryOfResidenceCode;
            LanguageContainer.Visible = false;
            viewReservationContainer.Visible = true;
            confirmCode.Text = _booking.ConfirmationId;
            mailaddress.Text = _booking.Customer.Email;
            lastname.Text = _booking.Customer.Surname;
            SearchBtn.Text = "Get a Quote";

            pickupDate.Text = _booking.PickupDateTime.ToString("yyyy-MM-dd");
            dropoffDate.Text = _booking.DropOffDateTime.ToString("yyyy-MM-dd");
            pickUpHrs.SelectedValue = _booking.PickupDateTime.Hour.ToString("D2");
            pickupMins.SelectedValue = _booking.PickupDateTime.Minute.ToString("D2");
            dropOffHrs.SelectedValue = _booking.DropOffDateTime.Hour.ToString("D2");
            dropoffMins.SelectedValue = _booking.DropOffDateTime.Minute.ToString("D2");
        }
        else
        {
            if (!string.IsNullOrEmpty(_booking.PickUpCountryCode))
            {
                selectpartner.SelectedValue = _booking.agentDutyCode;
                pickupCountry.SelectedValue = _booking.PickUpCountryCode;
                pickupLocation.SelectedValue = _booking.PickUpLocationCode;
                LoadLocations(pickupLocation, pickupCountry.SelectedValue);
                dropoffCountry.SelectedValue = _booking.DropOffCountryCode;
                dropoffLocation.SelectedValue = _booking.DropOffLocationCode;
                LoadLocations(dropoffLocation, dropoffCountry.SelectedValue);
                countryofResidence.SelectedValue = _booking.CountryOfResidenceCode;
                language.SelectedValue = _booking.Language.Trim().ToUpper();
                pickupDate.Text = _booking.PickupDateTime.ToString("yyyy-MM-dd");
                dropoffDate.Text = _booking.DropOffDateTime.ToString("yyyy-MM-dd");
                pickUpHrs.SelectedValue = _booking.PickupDateTime.Hour.ToString("D2");
                pickupMins.SelectedValue = _booking.PickupDateTime.Minute.ToString("D2");
                dropOffHrs.SelectedValue = _booking.DropOffDateTime.Hour.ToString("D2");
                dropoffMins.SelectedValue = _booking.DropOffDateTime.Minute.ToString("D2");
            }
            viewReservationContainer.Visible = false;
            SearchBtn.Text = "GET CAR";
        }
    }

    protected void LoadCountries()
    {
        db = new DatabaseEntities();
        var countries = db.Countries.OrderBy(c => c.CountryName);
        pickupCountry.DataSource = countries.ToList();
        pickupCountry.DataValueField = "CountryCode";
        pickupCountry.DataTextField = "CountryName";
        pickupCountry.DataBind();

        LoadLocations(pickupLocation, pickupCountry.SelectedValue);

        dropoffCountry.DataSource = countries.ToList();
        dropoffCountry.DataValueField = "CountryCode";
        dropoffCountry.DataTextField = "CountryName";
        dropoffCountry.DataBind();

        LoadLocations(dropoffLocation, dropoffCountry.SelectedValue);

        countryofResidence.DataSource = countries.ToList();
        countryofResidence.DataValueField = "CountryCode";
        countryofResidence.DataTextField = "CountryName";
        countryofResidence.DataBind();


    }
    protected void LoadLanguages()
    {
        db = new DatabaseEntities();
        var languages = db.Languages.OrderBy(l => l.LanguageName);
        language.DataSource = languages.ToList();
        language.DataValueField = "LanguageCode";
        language.DataTextField = "LanguageName";
        language.DataBind();
    }

    protected void LoadPartner()
    {
        db = new DatabaseEntities();


        var partners = db.Partners.OrderBy(p => p.DomainName);
        selectpartner.DataSource = partners.ToList();
        selectpartner.DataValueField = "AgentDutyCode";
        selectpartner.DataTextField = "DomainName";
        selectpartner.DataBind();
        lblpartner.Text = selectpartner.SelectedValue;

    }

    protected void pickupCountry_Change(object sender, EventArgs e)
    {
        LoadLocations(pickupLocation, pickupCountry.SelectedValue);
    }
    protected void dropoffCountry_Change(object sender, EventArgs e)
    {
        LoadLocations(dropoffLocation, dropoffCountry.SelectedValue);
    }

    protected void selectpartner_Changed(object sender, EventArgs e)
    {
        lblpartner.Text = selectpartner.SelectedValue;

    }

    protected void selectprovider_Changed(object sender, EventArgs e)
    {

    }

    protected void LoadLocations(DropDownList locationFiled, string selectedCountry)
    {
        db = new DatabaseEntities();
        var locations = (from l in db.Locations
                         join c in db.Countries on l.CountryIDFK equals c.CountryID
                         where c.CountryCode == selectedCountry
                         orderby l.LocationName ascending
                         select new
                         {
                             LocationCode = l.LocationCode.Trim(),
                             LocationName = l.LocationName.Trim()
                         });
        locationFiled.DataSource = locations.ToList();
        locationFiled.DataValueField = "LocationCode";
        locationFiled.DataTextField = "LocationName";
        locationFiled.DataBind();
    }

    public void Get_Request()
    {
        List<XmlDocument> vehicleResponses = new List<XmlDocument>();

        string pickupDatetime = pickupDate.Text.Trim() + "T" + pickUpHrs.SelectedValue + ":" + pickupMins.SelectedValue + ":00";
        string dropoffDatetime = dropoffDate.Text.Trim() + "T" + dropOffHrs.SelectedValue + ":" + dropoffMins.SelectedValue + ":00";
        DateTime requestTime = DateTime.Now;

        if (_booking.ModifyProcess == true)
        {
            db = new DatabaseEntities();

            var partner = (from p in db.Partners
                           where p.DomainName == selectpartner.SelectedItem.Text
                           select new
                           {
                               p.AgentDutyCode,
                               p.RequestorID_ID,
                               p.RequestorID_Type,
                               p.CompanyName_Code,
                               p.CodeContext

                           });
            if (partner.FirstOrDefault() != null)
            {

                _booking.PartnerDetails.agentDutyCode = partner.FirstOrDefault().AgentDutyCode;
                _booking.PartnerDetails.requestorID = partner.FirstOrDefault().RequestorID_ID;
                _booking.PartnerDetails.requestorIDType = partner.FirstOrDefault().RequestorID_Type.ToString();
                _booking.PartnerDetails.companyNameCode = partner.FirstOrDefault().CompanyName_Code;
                _booking.PartnerDetails.codeContext = partner.FirstOrDefault().CodeContext;

            }

            _booking.PickUpLocationCode = pickupLocation.SelectedValue;
            _booking.DropOffLocationCode = dropoffLocation.SelectedValue;

            var selectedCar = _booking.OriginalBooking.BookedCar;
            if (selectedCar != null)
            {
                _booking.CountryOfResidenceCode = countryofResidence.SelectedValue;
                _booking.hertzRQ = HertzRateQualifier.Text;
                _booking.thriftyRQ = ThriftyRateQualifier.Text;
                _booking.fireflyRQ = FireFlyRateQualifier.Text;
                string modifyVehicleResponse = "";

                if (_booking.OriginalBooking.CarProvider == CarProvider.Hertz)
                {
                    modifyVehicleResponse = SendXml.HttpPost("http://" + CMSSettings.SiteUrl + "/" + _booking.OriginalBooking.CarProvider.ToString().ToLower(), "<OTA_VehAvailRateRQ xmlns=\"http://www.opentravel.org/OTA/2003/05\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" version=\"1.008\" xsi:schemaLocation=\"http://www.opentravel.org/OTA/2003/05 OTA_VehAvailRateRQ.xsd\" MaxResponses=\"10\"><POS><Source AgentDutyCode=\"" + _booking.PartnerDetails.agentDutyCode + "\" ISOCountry=\"" + _booking.CountryOfResidenceCode + "\"><RequestorID Type=\"" + _booking.PartnerDetails.requestorIDType + "\" ID=\"" + _booking.PartnerDetails.requestorID + "\"><CompanyName Code=\"" + _booking.PartnerDetails.companyNameCode + "\" CodeContext=\"" + _booking.PartnerDetails.codeContext + "\" /></RequestorID></Source><Source><RequestorID Type=\"5\" ID=\"00109911\" /></Source></POS><VehAvailRQCore Status=\"All\"><VehRentalCore PickUpDateTime=\"" + pickupDatetime + "\" ReturnDateTime=\"" + dropoffDatetime + "\"><PickUpLocation LocationCode=\"" + pickupLocation.SelectedValue.Trim() + "\" CodeContext=\"IATA\" /><ReturnLocation LocationCode=\"" + dropoffLocation.SelectedValue.Trim() + "\" CodeContext=\"IATA\" /></VehRentalCore><RateQualifier RateQualifier=\"" + HertzRateQualifier.Text + "\" /></VehAvailRQCore><VehAvailRQInfo><ArrivalDetails TransportationCode=\"14\"><OperatingCompany Code=\"FR\"></OperatingCompany></ArrivalDetails></VehAvailRQInfo></OTA_VehAvailRateRQ>", requestTime, "dev.miresglobal.com", _booking.OriginalBooking.CarProvider.ToString(), "OTA_VehAvailRateRQ").ToString();
                }
                else if (_booking.OriginalBooking.CarProvider == CarProvider.Thrifty)
                {
                    modifyVehicleResponse = SendXml.HttpPost("http://" + CMSSettings.SiteUrl + "/" + _booking.OriginalBooking.CarProvider.ToString().ToLower(), "<OTA_VehAvailRateRQ xmlns=\"http://www.opentravel.org/OTA/2003/05\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" version=\"1.008\" xsi:schemaLocation=\"http://www.opentravel.org/OTA/2003/05 OTA_VehAvailRateRQ.xsd\" MaxResponses=\"10\"><POS><Source AgentDutyCode=\"" + _booking.PartnerDetails.agentDutyCode + "\" ISOCountry=\"" + _booking.CountryOfResidenceCode + "\"><RequestorID Type=\"" + _booking.PartnerDetails.requestorIDType + "\" ID=\"" + _booking.PartnerDetails.requestorID + "\"><CompanyName Code=\"" + _booking.PartnerDetails.companyNameCode + "\" CodeContext=\"" + _booking.PartnerDetails.codeContext + "\" /></RequestorID></Source><Source><RequestorID Type=\"5\" ID=\"00109911\" /></Source></POS><VehAvailRQCore Status=\"All\"><VehRentalCore PickUpDateTime=\"" + pickupDatetime + "\" ReturnDateTime=\"" + dropoffDatetime + "\"><PickUpLocation LocationCode=\"" + pickupLocation.SelectedValue.Trim() + "\" CodeContext=\"IATA\" /><ReturnLocation LocationCode=\"" + dropoffLocation.SelectedValue.Trim() + "\" CodeContext=\"IATA\" /></VehRentalCore><RateQualifier RateQualifier=\"" + ThriftyRateQualifier.Text + "\" /></VehAvailRQCore><VehAvailRQInfo><ArrivalDetails TransportationCode=\"14\"><OperatingCompany Code=\"FR\"></OperatingCompany></ArrivalDetails></VehAvailRQInfo></OTA_VehAvailRateRQ>", requestTime, "dev.miresglobal.com", _booking.OriginalBooking.CarProvider.ToString(), "OTA_VehAvailRateRQ").ToString();
                }
                else if (_booking.OriginalBooking.CarProvider == CarProvider.FireFly)
                {
                    modifyVehicleResponse = SendXml.HttpPost("http://" + CMSSettings.SiteUrl + "/" + _booking.OriginalBooking.CarProvider.ToString().ToLower(), "<OTA_VehAvailRateRQ xmlns=\"http://www.opentravel.org/OTA/2003/05\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" version=\"1.008\" xsi:schemaLocation=\"http://www.opentravel.org/OTA/2003/05 OTA_VehAvailRateRQ.xsd\" MaxResponses=\"10\"><POS><Source AgentDutyCode=\"" + _booking.PartnerDetails.agentDutyCode + "\" ISOCountry=\"" + _booking.CountryOfResidenceCode + "\"><RequestorID Type=\"" + _booking.PartnerDetails.requestorIDType + "\" ID=\"" + _booking.PartnerDetails.requestorID + "\"><CompanyName Code=\"" + _booking.PartnerDetails.companyNameCode + "\" CodeContext=\"" + _booking.PartnerDetails.codeContext + "\" /></RequestorID></Source><Source><RequestorID Type=\"5\" ID=\"00109911\" /></Source></POS><VehAvailRQCore Status=\"All\"><VehRentalCore PickUpDateTime=\"" + pickupDatetime + "\" ReturnDateTime=\"" + dropoffDatetime + "\"><PickUpLocation LocationCode=\"" + pickupLocation.SelectedValue.Trim() + "\" CodeContext=\"IATA\" /><ReturnLocation LocationCode=\"" + dropoffLocation.SelectedValue.Trim() + "\" CodeContext=\"IATA\" /></VehRentalCore><RateQualifier RateQualifier=\"" + FireFlyRateQualifier.Text + "\" /></VehAvailRQCore><VehAvailRQInfo><ArrivalDetails TransportationCode=\"14\"><OperatingCompany Code=\"FR\"></OperatingCompany></ArrivalDetails></VehAvailRQInfo></OTA_VehAvailRateRQ>", requestTime, "dev.miresglobal.com", _booking.OriginalBooking.CarProvider.ToString(), "OTA_VehAvailRateRQ").ToString();
                }
                else if (_booking.OriginalBooking.CarProvider == CarProvider.Dollar)
                {
                    modifyVehicleResponse = SendXml.HttpPost("http://" + CMSSettings.SiteUrl + "/" + _booking.OriginalBooking.CarProvider.ToString().ToLower(), "<OTA_VehAvailRateRQ xmlns=\"http://www.opentravel.org/OTA/2003/05\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" version=\"1.008\" xsi:schemaLocation=\"http://www.opentravel.org/OTA/2003/05 OTA_VehAvailRateRQ.xsd\" MaxResponses=\"10\"><POS><Source AgentDutyCode=\"" + _booking.PartnerDetails.agentDutyCode + "\" ISOCountry=\"" + _booking.CountryOfResidenceCode + "\"><RequestorID Type=\"" + _booking.PartnerDetails.requestorIDType + "\" ID=\"" + _booking.PartnerDetails.requestorID + "\"><CompanyName Code=\"" + _booking.PartnerDetails.companyNameCode + "\" CodeContext=\"" + _booking.PartnerDetails.codeContext + "\" /></RequestorID></Source><Source><RequestorID Type=\"5\" ID=\"00109911\" /></Source></POS><VehAvailRQCore Status=\"All\"><VehRentalCore PickUpDateTime=\"" + pickupDatetime + "\" ReturnDateTime=\"" + dropoffDatetime + "\"><PickUpLocation LocationCode=\"" + pickupLocation.SelectedValue.Trim() + "\" CodeContext=\"IATA\" /><ReturnLocation LocationCode=\"" + dropoffLocation.SelectedValue.Trim() + "\" CodeContext=\"IATA\" /></VehRentalCore><RateQualifier RateQualifier=\"" + DollarRateQualifier.Text + "\" /></VehAvailRQCore><VehAvailRQInfo><ArrivalDetails TransportationCode=\"14\"><OperatingCompany Code=\"FR\"></OperatingCompany></ArrivalDetails></VehAvailRQInfo></OTA_VehAvailRateRQ>", requestTime, "dev.miresglobal.com", _booking.OriginalBooking.CarProvider.ToString(), "OTA_VehAvailRateRQ").ToString();
                }
                
                vehicleModificationRequest = new XmlDocument();
                vehicleModificationRequest.LoadXml(modifyVehicleResponse);
                _booking.PickupDateTime = Convert.ToDateTime(pickupDatetime);
                _booking.DropOffDateTime = Convert.ToDateTime(dropoffDatetime);
                _booking.Brands.Clear();
                LoadCars(vehicleModificationRequest, _booking.OriginalBooking.CarProvider);
            }
            
        }

        else
        {
            _booking = new Booking
            {
                Brands = new List<BrandT>(),
                OriginalBooking = new OriginalBooking(),
                Accessories = new List<AccessoryT>(),
                Country = countryofResidence.SelectedValue,
                agentDutyCode = selectpartner.SelectedValue,
                CountryOfResidenceCode = countryofResidence.SelectedValue,
                PickUpCountryCode = pickupCountry.SelectedValue,
                PickUpLocationCode = pickupLocation.SelectedValue,
                DropOffCountryCode = dropoffCountry.SelectedValue,
                DropOffLocationCode = dropoffLocation.SelectedValue,
                PickupDateTime = Convert.ToDateTime(pickupDatetime),
                DropOffDateTime = Convert.ToDateTime(dropoffDatetime),
                hertzRQ = HertzRateQualifier.Text,
                thriftyRQ = ThriftyRateQualifier.Text,
                fireflyRQ = FireFlyRateQualifier.Text,
                dollarRQ = DollarRateQualifier.Text,
                Language = language.SelectedValue.ToUpper().Trim()
            };


            db = new DatabaseEntities();

            var partner = (from p in db.Partners
                           where p.DomainName == selectpartner.SelectedItem.Text
                           select new
                           {
                               p.AgentDutyCode,
                               p.RequestorID_ID,
                               p.RequestorID_Type,
                               p.CompanyName_Code,
                               p.CodeContext

                           });
            if (partner.FirstOrDefault() != null)
            {
                _booking.PartnerDetails.agentDutyCode = partner.FirstOrDefault().AgentDutyCode;
                _booking.PartnerDetails.requestorID = partner.FirstOrDefault().RequestorID_ID;
                _booking.PartnerDetails.requestorIDType = partner.FirstOrDefault().RequestorID_Type.ToString();
                _booking.PartnerDetails.companyNameCode = partner.FirstOrDefault().CompanyName_Code;
                _booking.PartnerDetails.codeContext = partner.FirstOrDefault().CodeContext;
            }

            string testPart = "<Customer><Primary BirthDate=\"1959-06-13\"></Primary></Customer>";

            string thriftyResponse = SendXml.HttpPost("http://" + CMSSettings.SiteUrl + "/thrifty", "<OTA_VehAvailRateRQ xmlns=\"http://www.opentravel.org/OTA/2003/05\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" version=\"1.008\" xsi:schemaLocation=\"http://www.opentravel.org/OTA/2003/05 OTA_VehAvailRateRQ.xsd\" MaxResponses=\"10\"><POS><Source AgentDutyCode=\"" + _booking.PartnerDetails.agentDutyCode + "\" ISOCountry=\"" + countryofResidence.SelectedValue.Trim() + "\"><RequestorID Type=\"" + _booking.PartnerDetails.requestorIDType + "\" ID=\"" + _booking.PartnerDetails.requestorID + "\"><CompanyName Code=\"" + _booking.PartnerDetails.companyNameCode + "\" CodeContext=\"" + _booking.PartnerDetails.codeContext + "\" /></RequestorID></Source><Source><RequestorID Type=\"5\" ID=\"00109911\" /></Source></POS><VehAvailRQCore Status=\"All\"><VehRentalCore PickUpDateTime=\"" + pickupDatetime + "\" ReturnDateTime=\"" + dropoffDatetime + "\"><PickUpLocation LocationCode=\"" + pickupLocation.SelectedValue.Trim() + "\" CodeContext=\"IATA\" /><ReturnLocation LocationCode=\"" + dropoffLocation.SelectedValue.Trim() + "\" CodeContext=\"IATA\" /></VehRentalCore><RateQualifier RateQualifier=\"" + _booking.thriftyRQ + "\" /></VehAvailRQCore><VehAvailRQInfo><TPA_Extensions><ConsumerIP>86.47.96.55</ConsumerIP></TPA_Extensions></VehAvailRQInfo></OTA_VehAvailRateRQ>", requestTime, "dev.miresglobal.com", "Thrifty", "OTA_VehAvailRateRQ").ToString();

            string hertzResponse = SendXml.HttpPost("http://" + CMSSettings.SiteUrl + "/hertz", "<OTA_VehAvailRateRQ xmlns=\"http://www.opentravel.org/OTA/2003/05\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" version=\"1.008\" xsi:schemaLocation=\"http://www.opentravel.org/OTA/2003/05 OTA_VehAvailRateRQ.xsd\" MaxResponses=\"10\"><POS><Source AgentDutyCode=\"" + _booking.PartnerDetails.agentDutyCode + "\" ISOCountry=\"" + countryofResidence.SelectedValue.Trim() + "\"><RequestorID Type=\"" + _booking.PartnerDetails.requestorIDType + "\" ID=\"" + _booking.PartnerDetails.requestorID + "\"><CompanyName Code=\"" + _booking.PartnerDetails.companyNameCode + "\" CodeContext=\"" + _booking.PartnerDetails.codeContext + "\" /></RequestorID></Source><Source><RequestorID Type=\"5\" ID=\"00109911\" /></Source></POS><VehAvailRQCore Status=\"All\"><VehRentalCore PickUpDateTime=\"" + pickupDatetime + "\" ReturnDateTime=\"" + dropoffDatetime + "\"><PickUpLocation LocationCode=\"" + pickupLocation.SelectedValue.Trim() + "\" CodeContext=\"IATA\" /><ReturnLocation LocationCode=\"" + dropoffLocation.SelectedValue.Trim() + "\" CodeContext=\"IATA\" /></VehRentalCore><RateQualifier RateQualifier=\"" + _booking.hertzRQ + "\" /></VehAvailRQCore><VehAvailRQInfo><TPA_Extensions><ConsumerIP>86.47.96.55</ConsumerIP></TPA_Extensions></VehAvailRQInfo></OTA_VehAvailRateRQ>", requestTime, "dev.miresglobal.com", "Hertz", "OTA_VehAvailRateRQ").ToString();

            string fireflyResponse = SendXml.HttpPost("http://" + CMSSettings.SiteUrl + "/firefly", "<OTA_VehAvailRateRQ xmlns=\"http://www.opentravel.org/OTA/2003/05\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" version=\"1.008\" xsi:schemaLocation=\"http://www.opentravel.org/OTA/2003/05 OTA_VehAvailRateRQ.xsd\" MaxResponses=\"10\"><POS><Source AgentDutyCode=\"" + _booking.PartnerDetails.agentDutyCode + "\" ISOCountry=\"" + countryofResidence.SelectedValue.Trim() + "\"><RequestorID Type=\"" + _booking.PartnerDetails.requestorIDType + "\" ID=\"" + _booking.PartnerDetails.requestorID + "\"><CompanyName Code=\"" + _booking.PartnerDetails.companyNameCode + "\" CodeContext=\"" + _booking.PartnerDetails.codeContext + "\" /></RequestorID></Source><Source><RequestorID Type=\"5\" ID=\"00109911\" /></Source></POS><VehAvailRQCore Status=\"All\"><VehRentalCore PickUpDateTime=\"" + pickupDatetime + "\" ReturnDateTime=\"" + dropoffDatetime + "\"><PickUpLocation LocationCode=\"" + pickupLocation.SelectedValue.Trim() + "\" CodeContext=\"IATA\" /><ReturnLocation LocationCode=\"" + dropoffLocation.SelectedValue.Trim() + "\" CodeContext=\"IATA\" /></VehRentalCore><RateQualifier RateQualifier=\"" + _booking.fireflyRQ + "\" /></VehAvailRQCore><VehAvailRQInfo><TPA_Extensions><ConsumerIP>86.47.96.55</ConsumerIP></TPA_Extensions></VehAvailRQInfo></OTA_VehAvailRateRQ>", requestTime, "dev.miresglobal.com", "Firefly", "OTA_VehAvailRateRQ").ToString();

            string dollarResponse = SendXml.HttpPost("http://" + CMSSettings.SiteUrl + "/dollar", "<OTA_VehAvailRateRQ xmlns=\"http://www.opentravel.org/OTA/2003/05\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" version=\"1.008\" xsi:schemaLocation=\"http://www.opentravel.org/OTA/2003/05 OTA_VehAvailRateRQ.xsd\" MaxResponses=\"10\"><POS><Source AgentDutyCode=\"" + _booking.PartnerDetails.agentDutyCode + "\" ISOCountry=\"" + countryofResidence.SelectedValue.Trim() + "\"><RequestorID Type=\"" + _booking.PartnerDetails.requestorIDType + "\" ID=\"" + _booking.PartnerDetails.requestorID + "\"><CompanyName Code=\"" + _booking.PartnerDetails.companyNameCode + "\" CodeContext=\"" + _booking.PartnerDetails.codeContext + "\" /></RequestorID></Source><Source><RequestorID Type=\"5\" ID=\"00400646\" /></Source></POS><VehAvailRQCore Status=\"All\"><VehRentalCore PickUpDateTime=\"" + pickupDatetime + "\" ReturnDateTime=\"" + dropoffDatetime + "\"><PickUpLocation LocationCode=\"" + pickupLocation.SelectedValue.Trim() + "\" CodeContext=\"IATA\" /><ReturnLocation LocationCode=\"" + dropoffLocation.SelectedValue.Trim() + "\" CodeContext=\"IATA\" /></VehRentalCore><RateQualifier RateQualifier=\"" + _booking.dollarRQ + "\" /></VehAvailRQCore><VehAvailRQInfo><ArrivalDetails TransportationCode=\"14\"><OperatingCompany Code=\"CX\" /></ArrivalDetails><TPA_Extensions><ConsumerIP>86.47.96.55</ConsumerIP></TPA_Extensions></VehAvailRQInfo></OTA_VehAvailRateRQ>", requestTime, "dev.miresglobal.com", "Dollar", "OTA_VehAvailRateRQ").ToString();


            thriftyVehAvailRateRSXML = new XmlDocument();
            thriftyVehAvailRateRSXML.LoadXml(thriftyResponse);

            fireflyVehAvailRateRSXML = new XmlDocument();
            fireflyVehAvailRateRSXML.LoadXml(fireflyResponse);

            hertzVehAvailRateRSXML = new XmlDocument();
            hertzVehAvailRateRSXML.LoadXml(hertzResponse);

            dollarVehAvailRateRSXML = new XmlDocument();
            dollarVehAvailRateRSXML.LoadXml(dollarResponse);

            vehicleResponses.Add(thriftyVehAvailRateRSXML);
            vehicleResponses.Add(hertzVehAvailRateRSXML);
            vehicleResponses.Add(fireflyVehAvailRateRSXML);
            vehicleResponses.Add(dollarVehAvailRateRSXML);

            int i = 0;

            foreach (var xml in vehicleResponses)
            {
                LoadCars(xml, (i == 0 ? CarProvider.Thrifty : (i == 1 ? CarProvider.Hertz : (i == 2 ? CarProvider.FireFly : CarProvider.Dollar))));
                //LoadCars(xml, (i == 0 ? CarProvider.Thrifty : (i == 1 ? CarProvider.Hertz : CarProvider.Dollar)));
                ++i;
            }

            _booking.Session = _booking;
            Response.Redirect("/webclients/Test/Test.aspx");
        }
    }

    protected void searchBtn_Clicked(object sender, EventArgs e)
    {
        Get_Request();   
    }

    protected void crosssellBtn_Clicked(object sender, EventArgs e)
    {
        //  DateTime requestTime = DateTime.Now;
        ////  CarXmlAction bookingCallLocal = new CarXmlAction();

        //  string pickupDatetime = pickupDate.Text.Trim() + "T" + pickUpHrs.SelectedValue + ":" + pickupMins.SelectedValue + ":00";
        //  string dropoffDatetime = dropoffDate.Text.Trim() + "T" + dropOffHrs.SelectedValue + ":" + dropoffMins.SelectedValue + ":00";

        //  string crosssellResponse =  SendXml.HttpPost("http://" + CMSSettings.SiteUrl + "/crosssell", "<OTA_VehAvailRateRQ xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns=\"http://www.opentravel.org/OTA/2003/05\" xsi:schemaLocation=\"http://www.opentravel.org/OTA/2003/05 OTA_VehAvailRateRQ.xsd\" Version=\"1.008\" MaxResponses=\"6\" ><POS><Source ISOCountry=\"" + countryofResidence.SelectedValue.Trim() + "\" AgentDutyCode=\"R18Y25A114N\"><RequestorID Type=\"4\" ID=\"T390\"><CompanyName Code=\"CP\" CodeContext=\"XM77\"></CompanyName></RequestorID></Source></POS><VehAvailRQCore Status=\"All\"><VehRentalCore PickUpDateTime=\"" + pickupDatetime + "\" ReturnDateTime=\"" + dropoffDatetime + "\"><PickUpLocation LocationCode=\"" + pickupLocation.SelectedValue.Trim() + "\" CodeContext=\"IATA\"></PickUpLocation><ReturnLocation LocationCode=\"" + dropoffLocation.SelectedValue.Trim() + "\" CodeContext=\"IATA\"></ReturnLocation></VehRentalCore><VehPrefs><VehPref AirConditionInd=\"true\" TransmissionType=\"Manual\"><VehType VehicleCategory=\"1\" DoorCount=\"4\"></VehType><VehClass Size=\"3\"></VehClass></VehPref></VehPrefs><RateQualifier CorpDiscountNmbr=\"709295\" RateQualifier=\"RYAN\"></RateQualifier></VehAvailRQCore><VehAvailRQInfo GasPrePay=\"false\" SmokingAllowed=\"false\"><Customer><Primary></Primary></Customer><ArrivalDetails TransportationCode=\"14\"><OperatingCompany Code=\"FR\"></OperatingCompany></ArrivalDetails></VehAvailRQInfo></OTA_VehAvailRateRQ>", requestTime, "dev.miresglobal.com", "Cross", "OTA_VehAvailRateRQ").ToString();

        //  _booking = new Booking
        //  {
        //      Brands = new List<BrandT>(),
        //      OriginalBooking = new OriginalBooking(),
        //      Accessories = new List<AccessoryT>(),
        //      Country = countryofResidence.SelectedValue,
        //      agentDutyCode = selectpartner.SelectedValue,
        //      CountryOfResidenceCode = countryofResidence.SelectedValue,
        //      PickUpCountryCode = pickupCountry.SelectedValue,
        //      PickUpLocationCode = pickupLocation.SelectedValue,
        //      DropOffCountryCode = dropoffCountry.SelectedValue,
        //      DropOffLocationCode = dropoffLocation.SelectedValue,
        //      PickupDateTime = Convert.ToDateTime(pickupDatetime),
        //      DropOffDateTime = Convert.ToDateTime(dropoffDatetime),
        //      Language = language.SelectedValue
        //  };

        //  crosssellRateRSXML = new XmlDocument();
        //  crosssellRateRSXML.LoadXml(crosssellResponse);
        //  var namespaceMgr = new XmlNamespaceManager(crosssellRateRSXML.NameTable);
        //  namespaceMgr.AddNamespace("a1", XmlNamespace);

        //  XmlNode vendorNode = crosssellRateRSXML.DocumentElement.SelectSingleNode("//a1:Vendor", namespaceMgr);

        //  if (vendorNode != null)
        //  {
        //      if (vendorNode.Attributes != null)
        //      {
        //          switch (vendorNode.Attributes["Code"].Value.Trim())
        //          {
        //              case "FF": LoadCars(crosssellRateRSXML, CarProvider.FireFly);
        //                  break;
        //              case "ZT": LoadCars(crosssellRateRSXML, CarProvider.Thrifty);
        //                  break;
        //              case "ZE": LoadCars(crosssellRateRSXML, CarProvider.Hertz);
        //                  break;
        //          }
        //      }
        //  }



        //  _booking.Session = _booking;
        //  Response.Redirect("/Test/Test.aspx");
    }


    protected void viewReturnedReservationBtn_Clicked(object sender, EventArgs e)
    {
        Response.Redirect("/Test/ViewBooking.aspx");
    }

    public void Retrival_Booking()
    {
        DateTime requestTime = DateTime.Now;
        string confimation_code = confirmCode.Text;
        string last_name = lastname.Text;
        string mailAddress = mailaddress.Text;
        string partner = _booking.PartnerDetails.agentDutyCode;

        string bookingResponse = SendXml.HttpPost("http://" + CMSSettings.SiteUrl + "/hertz", "<?xml version=\"1.0\" encoding=\"utf-8\"?><OTA_VehRetResRQ xmlns=\"http://www.opentravel.org/OTA/2003/05\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" version=\"1.008\" xsi:schemaLocation=\"http://www.opentravel.org/OTA/2003/05 OTA_VehRetResRQ.xsd\"><POS><Source AgentDutyCode=\"" + _booking.PartnerDetails.agentDutyCode + "\"><RequestorID Type=\"" + _booking.PartnerDetails.requestorIDType + "\" ID=\"" + _booking.PartnerDetails.requestorID + "\"><CompanyName Code=\"" + _booking.PartnerDetails.companyNameCode + "\" CodeContext=\"" + _booking.PartnerDetails.codeContext + "\" /></RequestorID></Source></POS><VehRetResRQCore><UniqueID Type=\"14\" ID=\"" + confimation_code + "\" /><PersonName><Surname>" + last_name + "</Surname></PersonName></VehRetResRQCore></OTA_VehRetResRQ>", requestTime, "dev.miresglobal.com", "viewBooking", "OTA_VehRetResRQ");

        vehicleRetResRSXML = new XmlDocument();
        vehicleRetResRSXML.LoadXml(bookingResponse);
        LoadReservedVehicle(vehicleRetResRSXML, partner);

        _booking.Customer.Email = mailAddress;
        _booking.Customer.Surname = last_name;

        _booking.Session = _booking;
        Response.Redirect("/webclients/Test/ViewBooking.aspx");
    }

    protected void viewBookingBtn_Clicked(object sender, EventArgs e)
    {
        setRetrivaldata();
        Retrival_Booking();
    }

    public void setRetrivaldata()
    {
        _booking = _booking.Session;
        string ConfCode = confirmCode.Text;

        db = new DatabaseEntities();

        var orderPartner = (from o in db.Orders
                            where o.ConfirmationID == ConfCode
                            select new 
                            { 
                                o.CountryOfResidence,
                                o.CameFrom,
                                o.XML_Provider_PickupSupplier
                            });

        foreach (var part in orderPartner)
        {
            if (orderPartner.Count() > 0)
            {
                Partner = orderPartner.FirstOrDefault().CameFrom;
                _brandSelected.brandName = orderPartner.FirstOrDefault().XML_Provider_PickupSupplier;
                _booking.CountryOfResidenceCode = orderPartner.FirstOrDefault().CountryOfResidence;
            }
        }

        var partner = (from p in db.Partners
                       where p.DomainName == Partner
                       select new
                       {
                           p.AgentDutyCode,
                           p.RequestorID_ID,
                           p.RequestorID_Type,
                           p.CompanyName_Code,
                           p.CodeContext

                       });
        if (partner.FirstOrDefault() != null)
        {

            PartnerDetails partnerDetails = new PartnerDetails();
            partnerDetails.agentDutyCode = partner.FirstOrDefault().AgentDutyCode;
            partnerDetails.requestorID = partner.FirstOrDefault().RequestorID_ID;
            partnerDetails.requestorIDType = partner.FirstOrDefault().RequestorID_Type.ToString();
            partnerDetails.companyNameCode = partner.FirstOrDefault().CompanyName_Code;
            partnerDetails.codeContext = partner.FirstOrDefault().CodeContext;
            _booking.PartnerDetails = partnerDetails;


        }
        _booking.Session = _booking;
    }
    public CarProvider GetCarProviderByVendorCode(string vendorCode)
    {
        
        db = new DatabaseEntities();
        CarProvider carProvider = CarProvider.Hertz;
        var brand = db.Brands.SingleOrDefault(b => b.VendorCode == vendorCode);
        if (brand != null)
        {
            switch (_brandSelected.brandName.ToLower())
            {
                case "thrifty":
                    carProvider = CarProvider.Thrifty;
                    break;
                case "firefly":
                    carProvider = CarProvider.FireFly;
                    break;
                case "hertz":
                    carProvider = CarProvider.Hertz;
                    break;
                case "dollar":
                    carProvider = CarProvider.Dollar;
                    break;
            }
        }
        return carProvider;
    }

    public void GetCustomerInfoByReservedInfo(XmlDocument vehAvailRateRSXML)
    {
        var namespaceMgr = new XmlNamespaceManager(vehAvailRateRSXML.NameTable);
        namespaceMgr.AddNamespace("a1", XmlNamespace);
        XmlNode surnameNode = vehAvailRateRSXML.DocumentElement.SelectSingleNode("//a1:Surname", namespaceMgr);
        if (surnameNode != null)
        {
            Customer customer = new Customer();
            customer.Surname = surnameNode.InnerText;
            _booking.Customer = customer;
        }
    }

    public void GetConfIdInBookingRet(XmlDocument vehAvailRateRSXML)
    {
        var namespaceMgr = new XmlNamespaceManager(vehAvailRateRSXML.NameTable);
        namespaceMgr.AddNamespace("a1", XmlNamespace);
        XmlNode confirmIdNode = vehAvailRateRSXML.DocumentElement.SelectSingleNode("//a1:ConfID", namespaceMgr);
        if (confirmIdNode != null)
        {
            _booking.ConfirmationId = confirmIdNode.Attributes["ID"].Value;
        }
    }

    public void GetVehRentalCoreDataFromBookingRet(XmlDocument vehAvailRateRSXML)
    {
        var namespaceMgr = new XmlNamespaceManager(vehAvailRateRSXML.NameTable);
        namespaceMgr.AddNamespace("a1", XmlNamespace);
        XmlNode vehRentalCoreNode = vehAvailRateRSXML.DocumentElement.SelectSingleNode("//a1:VehRentalCore", namespaceMgr);
        if (vehRentalCoreNode != null)
        {
            _booking.PickUpLocationCode = vehRentalCoreNode.SelectSingleNode("//a1:PickUpLocation", namespaceMgr).Attributes["LocationCode"].Value;
            _booking.DropOffLocationCode = vehRentalCoreNode.SelectSingleNode("//a1:ReturnLocation", namespaceMgr).Attributes["LocationCode"].Value;
            _booking.PickupDateTime = Convert.ToDateTime(vehRentalCoreNode.Attributes["PickUpDateTime"].Value);
            _booking.DropOffDateTime = Convert.ToDateTime(vehRentalCoreNode.Attributes["ReturnDateTime"].Value);
            Dictionary<string, string> pickupCountry = getCountryByLocationCode(_booking.PickUpLocationCode);
            Dictionary<string, string> dropOffCountry = getCountryByLocationCode(_booking.DropOffLocationCode);
            _booking.PickUpCountryCode = pickupCountry["countryCode"];
            _booking.PickUpCountryWord = pickupCountry["CountryName"];
            _booking.DropOffCountryCode = dropOffCountry["countryCode"];
            _booking.DropOffCountryWord = dropOffCountry["CountryName"];
            _booking.Language = "en";
            _booking.CountryOfResidenceCode = "GB";

            _booking.OriginalBooking.PickUpLocationCode = vehRentalCoreNode.SelectSingleNode("//a1:PickUpLocation", namespaceMgr).Attributes["LocationCode"].Value;
            _booking.OriginalBooking.DropOffLocationCode = vehRentalCoreNode.SelectSingleNode("//a1:ReturnLocation", namespaceMgr).Attributes["LocationCode"].Value;
            _booking.OriginalBooking.PickupDateTime = Convert.ToDateTime(vehRentalCoreNode.Attributes["PickUpDateTime"].Value);
            _booking.OriginalBooking.DropOffDateTime = Convert.ToDateTime(vehRentalCoreNode.Attributes["ReturnDateTime"].Value);
            _booking.OriginalBooking.PickUpCountryCode = pickupCountry["countryCode"];
            _booking.OriginalBooking.PickUpCountryWord = pickupCountry["CountryName"];
            _booking.OriginalBooking.DropOffCountryCode = dropOffCountry["countryCode"];
            _booking.OriginalBooking.DropOffCountryWord = dropOffCountry["CountryName"];

        }
    }
    public Dictionary<string, string> getCountryByLocationCode(string pickupLocationCode)
    {
        db = new DatabaseEntities();
        var countryEntry = (from l in db.Locations
                            join c in db.Countries on l.CountryIDFK equals c.CountryID
                            where (l.LocationCode == pickupLocationCode || l.ExtendedLocationCode == pickupLocationCode)
                            select new
                            {
                                c.CountryCode,
                                c.CountryName
                            }).FirstOrDefault();

        Dictionary<string, string> countryData = new Dictionary<string, string>();
        countryData["countryCode"] = countryEntry.CountryCode;
        countryData["CountryName"] = countryEntry.CountryName;
        return countryData;

    }

    protected void LoadReservedVehicle(XmlDocument vehRetResRSXML, string partner)
    {
        _booking = new Booking();

        var namespaceMgr = new XmlNamespaceManager(vehRetResRSXML.NameTable);
        namespaceMgr.AddNamespace("a1", XmlNamespace);

        XmlNode vendorNode = vehRetResRSXML.DocumentElement.SelectSingleNode("//a1:Vendor", namespaceMgr);
        if (vendorNode != null)
        {
            CarProvider provider = GetCarProviderByVendorCode(vendorNode.Attributes["Code"].Value.Trim());
            LoadReservedCarDetails(vehRetResRSXML, provider);
            GetCustomerInfoByReservedInfo(vehRetResRSXML);
            GetConfIdInBookingRet(vehRetResRSXML);
            GetVehRentalCoreDataFromBookingRet(vehRetResRSXML);
            GetBookedAccessories(vehRetResRSXML);
        }
        else
        {
            XmlNodeList errorList = vehRetResRSXML.DocumentElement.SelectNodes(".//a1:Error", namespaceMgr);
            if (errorList != null)
            {
                foreach (XmlNode node in errorList)
                {
                    _booking.ErrorDetails.ErrorMsg += node.Attributes["ShortText"].Value + "</br>";
                }
            }
        }

        var patner = (from p in db.Partners
                      where p.AgentDutyCode == partner
                       select new
                       {
                           p.AgentDutyCode,
                           p.RequestorID_ID,
                           p.RequestorID_Type,
                           p.CompanyName_Code,
                           p.CodeContext

                       });
        if (patner.FirstOrDefault() != null)
        {

            PartnerDetails partnerDetails = new PartnerDetails();
            partnerDetails.agentDutyCode = patner.FirstOrDefault().AgentDutyCode;
            partnerDetails.requestorID = patner.FirstOrDefault().RequestorID_ID;
            partnerDetails.requestorIDType = patner.FirstOrDefault().RequestorID_Type.ToString();
            partnerDetails.companyNameCode = patner.FirstOrDefault().CompanyName_Code;
            partnerDetails.codeContext = patner.FirstOrDefault().CodeContext;
            _booking.PartnerDetails = partnerDetails;
        }

        _booking.Session = _booking;

    }

    protected void GetBookedAccessories(XmlDocument vehAvailRateRSXML)
    {
        var namespaceMgr = new XmlNamespaceManager(vehAvailRateRSXML.NameTable);
        namespaceMgr.AddNamespace("a1", XmlNamespace);

        XmlNodeList nodeList = vehAvailRateRSXML.DocumentElement.SelectNodes("//a1:PricedEquip", namespaceMgr);
        if (nodeList != null)
        {
            foreach (XmlNode node in nodeList)
            {
                XmlNode equipment = node.SelectSingleNode(".//a1:Equipment", namespaceMgr);
                if ((equipment != null) && (equipment.Attributes != null))
                {
                    string quantity = equipment.Attributes["Quantity"].Value;
                    string equipType = equipment.Attributes["EquipType"].Value;
                    AccessoryT _accessory = new AccessoryT();
                    _accessory.EquipType = equipType;
                    _accessory.Quantity = Convert.ToInt32(quantity);
                    _accessory.IsSelected = true;
                    _accessory.Title = quantity + " x " + db.Accessories.Where(a => a.HertzCode == equipType).FirstOrDefault().Title;
                    XmlNode charge = node.SelectSingleNode(".//a1:Charge", namespaceMgr);
                    if ((charge != null) && (charge.Attributes != null))
                    {
                        decimal amount = Convert.ToDecimal(charge.Attributes["Amount"].Value, System.Globalization.CultureInfo.InvariantCulture);
                        string currencyCode = "";
                        if (charge.Attributes["CurrencyCode"] != null)
                        {
                            currencyCode = charge.Attributes["CurrencyCode"].Value;
                        }
                        if (_accessory != null)
                        {
                            _accessory.Price = String.Format("{0:0.00}", (IncludeTaxRate(vehAvailRateRSXML.SelectSingleNode("//a1:TaxAmount", namespaceMgr), amount)));
                            _accessory.CurrencySymbol = currencyCode;
                        }
                    }
                    _booking.Accessories.Add(_accessory);
                }
            }
        }
    }

    protected void LoadReservedCarDetails(XmlDocument vehAvailRateRSXML, CarProvider _carProvider)
    {
        var namespaceMgr = new XmlNamespaceManager(vehAvailRateRSXML.NameTable);
        namespaceMgr.AddNamespace("a1", XmlNamespace);
        XmlNode vehicleData = vehAvailRateRSXML.DocumentElement.SelectSingleNode("//a1:Vehicle", namespaceMgr);

        if (vehicleData != null)
        {

            _booking.OriginalBooking.CarProvider = _carProvider;

            var car = new Car();

            //vehicle details taken from here
            car = BasicDetails(vehicleData, car);

            // car name - strip off 'or similar' wording and use localized wording instead
            // also adds on Winter Tyre wording if Winter Tyre check is true
            car = CarName(vehicleData.SelectSingleNode(".//a1:VehMakeModel", namespaceMgr), car);


            XmlNode node = vehAvailRateRSXML.DocumentElement.SelectSingleNode("//a1:VehReservation", namespaceMgr);
            // get pricing details
            //XmlNode priceNode = node.SelectSingleNode(".//a1:PaymentRule", namespaceMgr);
            //XmlNode vehicleChargesNode = node.SelectSingleNode(".//a1:VehicleCharges", namespaceMgr);
            XmlNode estPriceNode = node.SelectSingleNode(".//a1:TotalCharge", namespaceMgr);
            car = PrePayPrice(estPriceNode, car);


            // Car Image
            car = CarImage(vehicleData.SelectSingleNode(".//a1:PictureURL", namespaceMgr), car);

            // additional car details
            car = AdditionalCarDetails(vehicleData.SelectSingleNode(".//a1:VehClass", namespaceMgr), car);

            // door count
            car = VehicleDoorDetails(vehicleData.SelectSingleNode(".//a1:VehType", namespaceMgr), car);

            car.IsResRetReturnedCar = true;
            // normal car call - save details into the Car list class
            _booking.OriginalBooking.BookedCar = car;

        }
        else
        {
            XmlNodeList errorList = vehAvailRateRSXML.DocumentElement.SelectNodes(".//a1:Error", namespaceMgr);
            if (errorList != null)
            {
                foreach (XmlNode node in errorList)
                {
                    _booking.ErrorDetails.ErrorMsg += node.Attributes["ShortText"].Value + "</br>";
                }
            }
        }
    }

    protected void LoadCars(XmlDocument vehAvailRateRSXML, CarProvider _carProvider)
    {
        var namespaceMgr = new XmlNamespaceManager(vehAvailRateRSXML.NameTable);
        namespaceMgr.AddNamespace("a1", XmlNamespace);

        XmlNode errorNode = vehAvailRateRSXML.DocumentElement.SelectSingleNode("//a1:Errors", namespaceMgr);
        if (errorNode == null)
        {
            XmlNodeList nodeList = vehAvailRateRSXML.DocumentElement.SelectNodes("//a1:VehAvail", namespaceMgr);
            if (nodeList.Count > 0)
            {

                BrandT _brandSelected = new BrandT();
                _brandSelected.CarProvider = _carProvider;
                _booking.Brands.Add(_brandSelected);
                _brandSelected.Cars = new List<Car>();
                if (_booking.ModifyProcess == true)
                {
                    // _brandSelected = _booking.Brands.FirstOrDefault();
                }

                foreach (XmlNode node in nodeList)
                {
                    var car = new Car();

                    //vehicle details taken from here
                    car = BasicDetails(node.SelectSingleNode(".//a1:Vehicle", namespaceMgr), car);

                    // car name - strip off 'or similar' wording and use localized wording instead
                    // also adds on Winter Tyre wording if Winter Tyre check is true
                    car = CarName(node.SelectSingleNode(".//a1:VehMakeModel", namespaceMgr), car);

                    // Car Image
                    car = CarImage(node.SelectSingleNode(".//a1:PictureURL", namespaceMgr), car);

                    // get pricing details
                    //XmlNode priceNode = node.SelectSingleNode(".//a1:PaymentRule", namespaceMgr);
                    //XmlNode vehicleChargesNode = node.SelectSingleNode(".//a1:VehicleCharges", namespaceMgr);
                    XmlNode estPriceNode = node.SelectSingleNode(".//a1:TotalCharge", namespaceMgr);
                    car = PrePayPrice(estPriceNode, car);


                    // hertz reference code for this car - passed in booking xml
                    car = ReferenceNumber(node.SelectSingleNode(".//a1:Reference", namespaceMgr), car);

                    // additional car details
                    car = AdditionalCarDetails(node.SelectSingleNode(".//a1:VehClass", namespaceMgr), car);

                    // door count
                    car = VehicleDoorDetails(node.SelectSingleNode(".//a1:VehType", namespaceMgr), car);

                    // normal car call - save details into the Car list class
                    _brandSelected.Cars.Add(car);
                }
            }
        }
        else
        {

            XmlNodeList errorList = errorNode.SelectNodes(".//a1:Error", namespaceMgr);
            foreach (XmlNode node in errorList)
            {
                _booking.ErrorDetails.ErrorMsg += node.Attributes["ShortText"].Value + "</br>";
            }

        }
    }

    private Car BasicDetails(XmlNode vehicleNode, Car car)
    {
        if (vehicleNode != null)
        {
            if (vehicleNode.Attributes != null)
            {
                // air con
                if (vehicleNode.Attributes["AirConditionInd"] != null)
                {
                    car.AirConditionInd = vehicleNode.Attributes["AirConditionInd"].Value;
                    if (car.AirConditionInd == "true")
                        car.AirConditionPref = "Preferred";
                }

                // transmission
                if (vehicleNode.Attributes["TransmissionType"] != null)
                {
                    car.TransmissionType = vehicleNode.Attributes["TransmissionType"].Value;
                    car.TransmissionPref = "Preferred";
                }

                car.FuelType = vehicleNode.Attributes["FuelType"] != null ? vehicleNode.Attributes["FuelType"].Value : "Unspecified";

                car.DriveType = vehicleNode.Attributes["DriveType"] != null ? vehicleNode.Attributes["DriveType"].Value : "Unspecified";

                car.SippCode = vehicleNode.Attributes["Code"] != null ? vehicleNode.Attributes["Code"].Value : "";
            }
        }

        return car;
    }

    private Car CarName(XmlNode carNode, Car car)
    {
        if (carNode != null)
        {
            if (carNode.Attributes != null && carNode.Attributes["Name"] != null)
            {
                string carName = carNode.Attributes["Name"].Value;
                car.Name = carName;
                car.LocalizedName = carName;
            }
        }

        return car;
    }

    private Car CarImage(XmlNode pictureNode, Car car)
    {
        if (pictureNode != null)
        {
            String url = pictureNode.InnerText;
            if (url.Contains("http"))
            {
                car.Image = pictureNode.InnerText;
            }
            else
            {
                car.Image = CMSSettings.RyanairDPCarImagePath + pictureNode.InnerText;
                if (!CommonTasks.UrlExists(car.Image))
                {
                    car.Image = CMSSettings.ThermeonCarImagePath + pictureNode.InnerText;
                }
            }

            if (!CommonTasks.UrlExists(car.Image))
            {
                car.Image = CMSSettings.CarImagePath + "Medium/" + pictureNode.InnerText;
            }
        }

        return car;
    }

    public static bool UrlIsValid(string smtpHost)
    {
        bool br = false;
        try
        {
            IPHostEntry ipHost = Dns.Resolve(smtpHost);
            br = true;
        }
        catch (SocketException se)
        {
            br = false;
        }
        return br;
    }

    private Car PrePayPrice(XmlNode priceNode, Car car)
    {
        var xmlDoc = new XmlDocument();
        var namespaceMgr = new XmlNamespaceManager(xmlDoc.NameTable);
        namespaceMgr.AddNamespace("a1", "http://www.opentravel.org/OTA/2003/05");

        // reset pricing details
        car.Price = 0m;
        car.Currency = "";
        car.CurrencySymbol = "";

        // look for new pricing details
        if (priceNode != null)
        {
            if (priceNode.Attributes != null)
            {
                car.Price = Convert.ToDecimal(priceNode.Attributes["EstimatedTotalAmount"].Value, System.Globalization.CultureInfo.InvariantCulture);
                string currencyCode = priceNode.Attributes["CurrencyCode"].Value;
                car.Currency = currencyCode;
            }
        }
        car.IsPrePaid = true;
        return car;
    }
    private Car ReferenceNumber(XmlNode referenceNode, Car car)
    {
        if (referenceNode != null)
        {
            if (referenceNode.Attributes != null)
            {
                car.HertzReference = referenceNode.Attributes["ID"].Value;

                if (referenceNode.Attributes["URL"] != null)
                {
                    car.CTCarURL = referenceNode.Attributes["URL"].Value;
                }
            }
        }
        return car;
    }
    private Car AdditionalCarDetails(XmlNode carTypeNode, Car car)
    {

        if (carTypeNode != null)
        {
            string carSize = "";
            if (carTypeNode.Attributes != null)
                carSize = carTypeNode.Attributes["Size"].Value;
            car.VehClassSize = carSize;
            car.VehClassSizeValue = carSize;
            car.LocalizedVehClassSizeValue = carSize;
        }

        return car;
    }

    private Car VehicleDoorDetails(XmlNode vehTypeNode, Car car)
    {
        var translatedWordForDoor = (string)HttpContext.GetGlobalResourceObject("SiteStrings", "XML_Door");
        car.VehicleCategory = "1";
        car.DoorCount = "4";
        car.LocalizedDoorCount = "2/4" + " " + translatedWordForDoor;

        if (vehTypeNode != null)
        {
            if (vehTypeNode.Attributes != null)
            {
                car.VehicleCategory = vehTypeNode.Attributes["VehicleCategory"].Value;
                if (vehTypeNode.Attributes["DoorCount"] != null)
                {
                    car.DoorCount = vehTypeNode.Attributes["DoorCount"].Value;
                    car.LocalizedDoorCount = vehTypeNode.Attributes["DoorCount"].Value + " " + translatedWordForDoor;
                }
                else
                {
                    car.DoorCount = "4";
                    car.LocalizedDoorCount = "2/4" + " " + translatedWordForDoor;
                }
            }
        }

        return car;
    }

    private decimal IncludeTaxRate(XmlNode taxNode, decimal itemPrice)
    {
        decimal percentage = 0;
        if (taxNode != null)
        {
            if (taxNode.Attributes != null)
                percentage = Convert.ToDecimal(taxNode.Attributes["Percentage"].Value, System.Globalization.CultureInfo.InvariantCulture);
        }

        // new accessory price = current accessory price + (tax percentage of existing accessory price)
        itemPrice = itemPrice + (itemPrice * (percentage / 100));

        return Math.Round(itemPrice, 2);
    }


    protected void LoadReservedVehicleErrors(XmlDocument vehRetResRSXML)
    {
        Response.Write("Errr   " + _booking.ErrorDetails.ErrorMsg + "<br>");

        var namespaceMgr = new XmlNamespaceManager(vehRetResRSXML.NameTable);
        namespaceMgr.AddNamespace("a1", XmlNamespace);

        XmlNodeList errorList = vehRetResRSXML.DocumentElement.SelectNodes(".//a1:Error", namespaceMgr);
        if (errorList != null)
        {
            foreach (XmlNode node in errorList)
            {
                _booking.ErrorDetails.ErrorMsg += node.Attributes["ShortText"].Value + "</br>";
                //   Response.Write("Attt    " + node.Attributes["ShortText"].Value +"<br>");
            }
        }
    }
}