﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="VehicleDetail.ascx.cs" Inherits="Test_Controls_VehicleDetail" %>
<div class="row">
    <asp:Repeater ID="VehicleDetailContainer" runat="server">
        <ItemTemplate>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 style="width: 60%;" class="panel-title">Vehicle Type</h3>
                </div>
                <div class="panel-body">
                    <div class="media">
                        <a class="pull-left" href="#">
                            <img class="media-object" alt="128x128" src="<%#Eval("Image") %>" style="width: 200px;">
                        </a>
                        <div class="media-body">
                            <div class="row">
                                <div class="col-xs-12 col-md-8">
                                    <h4 class="media-heading"><%#Eval("Name") %> - <%#Eval("SippCode") %></h4>
                                    <p><%#Eval("VehClassSize") %> Passengers, <%#Eval("DoorCount") %> Doors, <%#Eval("TransmissionType") %></p>
                                </div>
                                <div class="col-xs-12 col-md-4">
                                    <div class="row">
                                        <div class="col-xs-12 col-md-11" style="border: 1px #ccc solid; border-radius: 5px;">
                                            <h2 style="text-align: center; margin-top: 5px;">
                                                <%#Eval("Price") %>
                                            </h2>
                                            <p style="text-align: center; font-size: 11px; margin-top: 3px;"><%#Eval("Currency") %> <%#Eval("Price") %> per day</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ItemTemplate>
    </asp:Repeater>
</div>