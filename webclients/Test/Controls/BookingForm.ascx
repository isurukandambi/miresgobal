﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="BookingForm.ascx.cs" Inherits="Test_Controls_BookingForm" %>
<div class="col-xs-6 col-md-3 booking-form">

    <div role="tabpanel">

        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active" style="width: 50%;"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Make a Reservation</a></li>
            <li role="presentation" style="width: 50%;"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Manage booking</a></li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="home">
                <div role="tabpanel" class="tab-pane active" id="partnertab">
                    </div>
                 <div class="form-group">
                    <label for="partner">Select Partner</label>
                    <asp:DropDownList ID="selectpartner" CssClass="form-control" runat="server"  AutoPostBack="true" OnSelectedIndexChanged="selectpartner_Changed"></asp:DropDownList>
                </div>
                   <div class="form-group">
                       
                    <label for="lastname">Agent Code :</label>
                 
                       <asp:Literal  ID="lblpartner" runat="server"></asp:Literal>
                       <%-- <label for="partnetload" ID="lblpartner" runat="server"></label>--%>
                   <%-- <asp:TextBox ID="agentcode" runat="server" CssClass="form-control date-control pUpDate"></asp:TextBox>--%>
                </div>
                <div class="form-group">
                    <label for="pickupCountry">Pick up Country</label>
                    <asp:DropDownList ID="pickupCountry" CssClass="form-control" runat="server" AutoPostBack="true" OnSelectedIndexChanged="pickupCountry_Change"></asp:DropDownList>
                </div>
               
                <div class="form-group">
                    <label for="pickupLocation">Pick up Location</label>
                    <asp:DropDownList ID="pickupLocation" CssClass="form-control" runat="server"></asp:DropDownList>
                </div>
                <div class="form-inline form-group">
                    <div class="form-group">
                        <label for="pickupDate">Pick up Date</label>
                        <br />
                        <asp:TextBox ID="pickupDate" runat="server" CssClass="form-control date-control pUpDate"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label for="pickUpHrs">Pick up Time</label>
                        <br />
                        <asp:DropDownList ID="pickUpHrs" CssClass="form-control time-control" runat="server">
                            <asp:ListItem Text="00" Value="00" />
                            <asp:ListItem Text="01" Value="01" />
                            <asp:ListItem Text="02" Value="02" />
                            <asp:ListItem Text="03" Value="03" />
                            <asp:ListItem Text="04" Value="04" />
                            <asp:ListItem Text="05" Value="05" />
                            <asp:ListItem Text="06" Value="06" />
                            <asp:ListItem Text="07" Value="07" />
                            <asp:ListItem Text="08" Value="08" />
                            <asp:ListItem Text="09" Value="09" Selected="True" />
                            <asp:ListItem Text="10" Value="10" />
                            <asp:ListItem Text="11" Value="11" />
                            <asp:ListItem Text="12" Value="12" />
                            <asp:ListItem Text="13" Value="13" />
                            <asp:ListItem Text="14" Value="14" />
                            <asp:ListItem Text="15" Value="15" />
                            <asp:ListItem Text="16" Value="16" />
                            <asp:ListItem Text="17" Value="17" />
                            <asp:ListItem Text="18" Value="18" />
                            <asp:ListItem Text="19" Value="19" />
                            <asp:ListItem Text="20" Value="20" />
                            <asp:ListItem Text="21" Value="21" />
                            <asp:ListItem Text="22" Value="22" />
                            <asp:ListItem Text="23" Value="23" />
                        </asp:DropDownList>
                        <asp:DropDownList ID="pickupMins" CssClass="form-control time-control" runat="server">
                            <asp:ListItem Text="00" Value="00" />
                            <asp:ListItem Text="15" Value="15" />
                            <asp:ListItem Text="30" Value="30" />
                            <asp:ListItem Text="45" Value="45" />
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="form-group">
                    <label for="dropoffCountry">Drop off Country</label>
                    <asp:DropDownList ID="dropoffCountry" CssClass="form-control" runat="server" AutoPostBack="true" OnSelectedIndexChanged="dropoffCountry_Change"></asp:DropDownList>
                </div>
                <div class="form-group">
                    <label for="dropoffLocation">Drop off Location</label>
                    <asp:DropDownList ID="dropoffLocation" CssClass="form-control" runat="server"></asp:DropDownList>
                </div>
                <div class="form-inline form-group">
                    <div class="form-group">
                        <label for="dropoffDate">Drop off Date</label>
                        <br />
                        <asp:TextBox ID="dropoffDate" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label for="dropOffHrs">Drop off Time</label>
                        <br />
                        <asp:DropDownList ID="dropOffHrs" CssClass="form-control time-control" runat="server">
                            <asp:ListItem Text="00" Value="00" />
                            <asp:ListItem Text="01" Value="01" />
                            <asp:ListItem Text="02" Value="02" />
                            <asp:ListItem Text="03" Value="03" />
                            <asp:ListItem Text="04" Value="04" />
                            <asp:ListItem Text="05" Value="05" />
                            <asp:ListItem Text="06" Value="06" />
                            <asp:ListItem Text="07" Value="07" />
                            <asp:ListItem Text="08" Value="08" />
                            <asp:ListItem Text="09" Value="09" />
                            <asp:ListItem Text="10" Value="10" />
                            <asp:ListItem Text="11" Value="11" />
                            <asp:ListItem Text="12" Value="12" />
                            <asp:ListItem Text="13" Value="13" />
                            <asp:ListItem Text="14" Value="14" />
                            <asp:ListItem Text="15" Value="15" />
                            <asp:ListItem Text="16" Value="16" Selected="True" />
                            <asp:ListItem Text="17" Value="17" />
                            <asp:ListItem Text="18" Value="18" />
                            <asp:ListItem Text="19" Value="19" />
                            <asp:ListItem Text="20" Value="20" />
                            <asp:ListItem Text="21" Value="21" />
                            <asp:ListItem Text="22" Value="22" />
                            <asp:ListItem Text="23" Value="23" />
                        </asp:DropDownList>
                        <asp:DropDownList ID="dropoffMins" CssClass="form-control time-control" runat="server">
                            <asp:ListItem Text="00" Value="00" />
                            <asp:ListItem Text="15" Value="15" />
                            <asp:ListItem Text="30" Value="30" />
                            <asp:ListItem Text="45" Value="45" />
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="form-group" runat="server" id="CountryOfResidenceContainer">
                    <label for="countryofResidence">Country of Residence</label>
                    <asp:DropDownList ID="countryofResidence" CssClass="form-control" runat="server"></asp:DropDownList>
                </div>
                <div class="form-group" runat="server" id="LanguageContainer">
                    <label for="language">Language</label>
                    <asp:DropDownList ID="language" CssClass="form-control" runat="server"></asp:DropDownList>
                </div>
                <div class="form-group">
                    <label for="confirmCode">Thrifty Rate Qualifier</label>
                    <br />
                    <asp:TextBox ID="ThriftyRateQualifier" runat="server" CssClass="form-control date-control pUpDate"></asp:TextBox>
                </div>
                <div class="form-group">
                    <label for="confirmCode">Hertz Rate Qualifier</label>
                    <br />
                    <asp:TextBox ID="HertzRateQualifier" runat="server" CssClass="form-control date-control pUpDate"></asp:TextBox>
                </div>
                <div class="form-group">
                    <label for="confirmCode">FireFly Rate Qualifier</label>
                    <br />
                    <asp:TextBox ID="FireFlyRateQualifier" runat="server" CssClass="form-control date-control pUpDate"></asp:TextBox>
                </div>
                <div class="form-group">
                    <label for="confirmCode">Dollar Rate Qualifier</label>
                    <br />
                    <asp:TextBox ID="DollarRateQualifier" runat="server" CssClass="form-control date-control pUpDate"></asp:TextBox>
                </div>
                <div class="row">
                    <div class="col-md-12" runat="server" id="viewReservationContainer" visible="false">
                        <asp:Button CssClass="btn btn-warning booking-form-btn" Text="View Reservation" OnClick="viewReturnedReservationBtn_Clicked" runat="server" />
                    </div>
                    <div class="col-md-12"><asp:Button CssClass="btn btn-success booking-form-btn" Text="Crosssell Banner Call" OnClick="crosssellBtn_Clicked" runat="server" /></div>
                    <div class="col-md-12"><asp:Button CssClass="btn btn-primary booking-form-btn" ID="SearchBtn" Text="Hertz" OnClick="searchBtn_Clicked" runat="server" /></div>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="profile">
                <div class="form-group">
                    <label for="confirmCode">Booking Confirmation Code</label>
                    <br />
                    <asp:TextBox ID="confirmCode" runat="server" CssClass="form-control date-control pUpDate"></asp:TextBox>
                </div>

                <div class="form-group">
                    <label for="mailaddress">Email Address</label>
                    <br />
                    <asp:TextBox ID="mailaddress" runat="server" CssClass="form-control date-control pUpDate"></asp:TextBox>
                </div>
                <div class="form-group">
                    <label for="lastname">Last Name</label>
                    <br />
                    <asp:TextBox ID="lastname" runat="server" CssClass="form-control date-control pUpDate"></asp:TextBox>
                </div>

                <div>
                <asp:Button CssClass="btn btn-primary pull-right" Text="Next" OnClick="viewBookingBtn_Clicked" runat="server" /></div>
               <%-- <div>
                <asp:Button CssClass="btn btn-danger pull-right" Text="Next" OnClick="viewBookingDTagBtn_Clicked" runat="server" /></div>--%>
            </div>
        </div>
    </div>
    <%--    <div class="row">
        <div class="col-xs-12 col-md-12">
            <h2 style="margin-top: 0px; border-bottom: 1px #ccc solid; margin-bottom: 15px;">Make Reservation</h2>
        </div>
    </div>--%>
</div>