﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Test.aspx.cs" Inherits="DefaultPage" %>

<%@ Register Src="~/webclients/Test/Controls/BookingForm.ascx" TagName="FullForm" TagPrefix="ff1" %>
<%@ Register Src="~/webclients/Test/Controls/VehicleList.ascx" TagName="VehicleList" TagPrefix="vl1" %>
<%@ Register Src="~/webclients/Test/Controls/Steps.ascx" TagName="Steps" TagPrefix="st1" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Client for Miresglobal</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <style type="text/css">
        .booking-form {
            padding: 20px;
            border-radius: 10px;
            border: 1px #ECECEC solid;
        }
        .pull-right{
            float: right;
        }
        .booking-form-btn{
            width: 100%;
            margin: 3px;
        }
    </style>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <form role="form" runat="server">
        <div class="container-fluid">
            <div class="row" style="margin-bottom: 20px;">
                <div class="col-xs-12 col-md-12" style="background-color: #1f3469;">
                    <h1 style="color: #fff; letter-spacing: 4px; font-style:italic; font-weight:200;">Mires Global</h1>
                </div>
            </div>
            <div class="row" style="margin-left: 0px; margin-right: 0px;">
                <ff1:FullForm ID="FullForm" runat="server" />
                
                <div class="col-xs-12 col-md-9">
                    <st1:Steps ID="StepsList" runat="server" />
                    <div class="row">
                        <vl1:VehicleList ID="VehicleList" runat="server" />    
                    </div>
                    <%--<mb1:ModifyBooking ID="ModifyBooking1" runat="server" />--%> 
                </div>
            </div>   
        </div>
    </form>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
</body>
</html>