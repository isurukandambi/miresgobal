﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Extras.aspx.cs" Inherits="Extras" EnableEventValidation="false" %>

<%@ Register Src="~/webclients/Test/Controls/VehicleDetail.ascx" TagName="VehicleDetail" TagPrefix="vd1" %>
<%@ Register Src="~/webclients/Test/Controls/BookingForm.ascx" TagName="FullForm" TagPrefix="ff1" %>
<%@ Register Src="~/webclients/Test/Controls/Steps.ascx" TagName="Steps" TagPrefix="st1" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Client for Miresglobal</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <style type="text/css">
        .booking-form {
            padding: 20px;
            border-radius: 10px;
            border: 1px #ECECEC solid;
        }

        .extracallbtn {
            width: 110px;
            float: right;
            margin-bottom: 8px;
        }
        .accessory-row {
            border-bottom: 1px #cecece solid;
        }

        .accessory-row:last-child {
            border-bottom: none;
        }

        .accessory-checkbox {
            margin-left: 20px;
        }
        .booking-form-btn{
            width: 100%;
            margin: 3px;
        }
    </style>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <form role="form" runat="server">
        <div class="container-fluid">
            <div class="row" style="margin-bottom: 20px;">
                <div class="col-xs-12 col-md-12" style="background-color: #1f3469;">
                    <h1 style="color: #fff; letter-spacing: 2px;">Mires Global</h1>
                </div>
            </div>
            <div class="row" style="margin-left: 0px; margin-right: 0px;">
                <ff1:FullForm ID="FullForm" runat="server" />
                <div class="col-xs-12 col-md-9">
                    <st1:Steps ID="StepsList" runat="server" />
                    <asp:Button CssClass="btn btn-warning extracallbtn" Text="PAY NOW" ID="AccessoryCallBtn" runat="server" OnClick="paynowBtn_Clicked" />
                    <asp:Button CssClass="btn btn-primary" Text="Terms" ID="TermsConditionBtn" runat="server" OnClick="termsBtn_Clicked" />
                    <div class="alert alert-error" runat="server" id="NoAccessoriesForSelectedCar" visible="false"><asp:Literal ID="ErrorList" runat="server"></asp:Literal></div>
                    <div class="row" style="margin-left: 0px; margin-right: 0px;">
                        <div class="col-xs-12 col-md-12">
                            <vd1:VehicleDetail ID="SelectedVehicle" runat="server" />
                            <div class="row">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">Available Extras</h3>
                                    </div>
                                    <div class="panel-body">
                                        <asp:Repeater ID="ExtrasContainer" runat="server">
                                            <ItemTemplate>
                                                <div class="row accessory-row">
                                                    <div class="col-xs-12 col-md-3">
                                                        <img src="../Pics/Accessories/Original/<%#Eval("Image1") %>" style="height: 70px; margin-top: 5px;" />
                                                        <asp:HiddenField runat="server" Value='<%#Eval("HertzCode") %>' ID="hertzCode" />
                                                        <asp:HiddenField runat="server" Value='<%#Eval("Image1") %>' ID="extraImage" />
                                                      <%--  <asp:HiddenField runat="server" Value='<%#getAccessoriesTitle("HertzCode") %>' ID="extraTitle" />--%>
                                                        <asp:HiddenField runat="server" Value='<%#Eval("Title") %>' ID="extraTitle" />
                                                    </div>
                                                    <div class="col-xs-12 col-md-6">
                                                        <h3 style="margin-top: 10px;"><%#Eval("Title") %></h3>
                                                        <p><%#Eval("Price") %></p>
                                                    </div>
                                                    <div class="col-xs-12 col-md-3">
                                                        <div class="form-group" style="margin-top: 7px;">
                                                            <div class="input-group">
                                                                <div class="input-group-addon">Quantity</div>
                                                                <asp:TextBox runat="server" CssClass="form-control" ID="Quantity" />
                                                            </div>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                Add This
                                                            </label>
                                                            <asp:CheckBox runat="server" CssClass="accessory-checkbox" ID="AccessoryCheckBox" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <asp:Literal ID="terms" runat="server"></asp:Literal>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </form>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    
</body>
</html>