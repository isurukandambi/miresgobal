﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Review.aspx.cs" Inherits="Test_Review" %>

<%@ Register Src="~/webclients/Test/Controls/VehicleDetail.ascx" TagName="VehicleDetail" TagPrefix="vd1" %>
<%@ Register Src="~/webclients/Test/Controls/BookingForm.ascx" TagName="FullForm" TagPrefix="ff1" %>
<%@ Register Src="~/webclients/Test/Controls/Steps.ascx" TagName="Steps" TagPrefix="st1" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Client for Miresglobal</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <style type="text/css">
        .booking-form {
            padding: 20px;
            border-radius: 10px;
            border: 1px #ECECEC solid;
        }

        .extracallbtn {
            float: right;
        }

        .backbtn {
            width: 110px;
            float: left;
            margin-bottom: 8px;
        }

        .accessory-row {
            border-bottom: 1px #cecece solid;
        }

            .accessory-row:last-child {
                border-bottom: none;
            }

        .booking-form-btn {
            width: 100%;
            margin: 3px;
        }
    </style>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <form role="form" runat="server">
        <div class="container-fluid">
            <div class="row" style="margin-bottom: 20px;">
                <div class="col-xs-12 col-md-12" style="background-color: #1f3469;">
                    <h1 style="color: #fff; letter-spacing: 2px;">Mires Global</h1>
                </div>
            </div>
            <div class="row" style="margin-left: 0px; margin-right: 0px;">
                <ff1:FullForm ID="FullForm" runat="server" />
                <div class="col-xs-12 col-md-8">
                    <st1:Steps ID="StepsList" runat="server" />
                    <asp:HyperLink CssClass="btn btn-danger backbtn" Text="BACK" ID="AccessoryLink" runat="server" />
                    <a href="Test.aspx" class="btn btn-primary" style="float: right;">CHANGE CAR</a>
                    <div class="alert alert-error" runat="server" id="NoAccessoriesForSelectedCar" visible="false">
                        <asp:Literal ID="ErrorList" runat="server"></asp:Literal>
                    </div>
                    <div class="row" style="margin-left: 0px; margin-right: 0px;" runat="server" visible="false" id="ValidationMessage">
                        <div class="alert alert-info" role="alert" style="margin-top: 50px;">
                            <asp:Literal runat="server" ID="AlertMessage"></asp:Literal>
                        </div>
                    </div>
                    <div class="row" style="margin-left: 0px; margin-right: 0px;">
                        <div class="col-xs-12 col-md-12">
                            <vd1:VehicleDetail ID="SelectedVehicle" runat="server" />
                            <div class="row" runat="server" id="ExtrasHolder">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">Selected Extras</h3>
                                    </div>
                                    <div class="panel-body">
                                        <asp:Repeater ID="ExtrasContainer" runat="server">
                                            <ItemTemplate>
                                                <div class="row accessory-row">
                                                    <div class="col-xs-12 col-md-3">
                                                        <img src="<%#Eval("Image") %>" style="height: 70px; margin-top: 5px;" />
                                                        <asp:HiddenField runat="server" Value='<%#Eval("EquipType") %>' ID="hertzCode" />
                                                    </div>
                                                    <div class="col-xs-12 col-md-6">
                                                        <h3 style="margin-top: 10px;"><%#Eval("Title") %></h3>
                                                        <p><%#Eval("CurrencySymbol") %> <%#Eval("Price") %></p>
                                                    </div>
                                                    <div class="col-xs-12 col-md-3">
                                                        <div class="form-group" style="margin-top: 7px;">
                                                            <div class="input-group">
                                                                <div class="input-group-addon">Quantity</div>
                                                                <asp:TextBox runat="server" CssClass="form-control" Text='<%#Eval("Quantity") %>' ReadOnly="true" ID="Quantity" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <%--Payment Details Form--%>
                    <div class="row" style="margin-left: 0px; margin-right: 0px;">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Personal Details</h3>
                            </div>
                            <div class="panel-body">
                                <div class="row form-group">
                                    <div class="form-group col-md-6">
                                        <label for="FirstName">First Name</label>
                                        <asp:TextBox runat="server" CssClass="form-control" ID="FirstName" Text="Mires" />
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="LastName">Last Name</label>
                                        <asp:TextBox runat="server" CssClass="form-control" ID="LastName" Text="Global" />
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="form-group col-md-6">
                                        <label for="FirstName">Telephone Number with Country Code</label>
                                        <asp:TextBox runat="server" CssClass="form-control" ID="TelephoneNumber" Text="0000000000" />
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="LastName">Address Line 1</label>
                                        <asp:TextBox runat="server" CssClass="form-control" ID="Address1" Text="Address 1" />
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="form-group col-md-6">
                                        <label for="LastName">Address Line 2</label>
                                        <asp:TextBox runat="server" CssClass="form-control" ID="Address2" Text="Address 2" />
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="LastName">Town</label>
                                        <asp:TextBox runat="server" CssClass="form-control" ID="Town" Text="Dublin" />
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="form-group col-md-6">
                                        <label for="LastName">Postcode / ZIP Code</label>
                                        <asp:TextBox runat="server" CssClass="form-control" ID="PostalCode" Text="10230" />
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="countryofResidence">Country</label>
                                        <asp:DropDownList ID="Country" CssClass="form-control" runat="server"></asp:DropDownList>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="form-group col-md-6">
                                        <label for="LastName">Email</label>
                                        <asp:TextBox runat="server" CssClass="form-control" ID="Email" Text="dmstestreply@gmail.com" />
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="LastName">Hertz #1 Club number (optional)</label>
                                        <asp:TextBox runat="server" CssClass="form-control" ID="HertzClubNumber" />
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="FlightNum">Flight Number (optional)</label>
                                        <asp:TextBox runat="server" CssClass="form-control" ID="FlightNum" Text="1931" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Payment Details</h3>
                            </div>
                            <div class="panel-body">
                                <div class="row form-group">
                                    <div class="form-group col-md-6">
                                        <label for="FirstName">Name on Card</label>
                                        <asp:TextBox runat="server" CssClass="form-control" ID="NameOnCard" Text="Mires Global" />
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="countryofResidence">Card Type</label>
                                        <asp:DropDownList ID="CardType" CssClass="form-control" runat="server">
                                            <asp:ListItem Text="Visa" Value="VI" />
                                            <asp:ListItem Text="Mastercard" Value="CA" />
                                            <asp:ListItem Text="American Express" Value="AX" />
                                            <asp:ListItem Text="Diners Club" Value="DC" Selected="True" />
                                            <asp:ListItem Text="Discover" Value="DS" />
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="form-group col-md-6">
                                        <label for="FirstName">Card Number</label>
                                        <asp:TextBox runat="server" CssClass="form-control" ID="CardNumber" Text="36259600000004" />
                                    </div>
                                    <div class="form-inline form-group col-md-6">
                                        <div class="form-group">
                                            <label for="pickUpHrs">Expire Date</label>
                                            <br />
                                            <asp:DropDownList ID="ExpireMonth" CssClass="form-control time-control" runat="server">
                                                <asp:ListItem Text="01" Value="01" />
                                                <asp:ListItem Text="02" Value="02" />
                                                <asp:ListItem Text="03" Value="03" />
                                                <asp:ListItem Text="04" Value="04" />
                                                <asp:ListItem Text="05" Value="05" />
                                                <asp:ListItem Text="06" Value="06" />
                                                <asp:ListItem Text="07" Value="07" />
                                                <asp:ListItem Text="08" Value="08" />
                                                <asp:ListItem Text="09" Value="09" />
                                                <asp:ListItem Text="10" Value="10" />
                                                <asp:ListItem Text="11" Value="11" />
                                                <asp:ListItem Text="12" Value="12" Selected="True" />
                                            </asp:DropDownList>
                                            <asp:DropDownList ID="ExpireYear" CssClass="form-control time-control" runat="server">
                                                <asp:ListItem Text="2014" Value="14" />
                                                <asp:ListItem Text="2015" Value="15" Selected="True" />
                                                <asp:ListItem Text="2016" Value="16" />
                                                <asp:ListItem Text="2017" Value="17" />
                                                <asp:ListItem Text="2018" Value="18" />
                                                <asp:ListItem Text="2019" Value="19" />
                                                <asp:ListItem Text="2020" Value="20" />
                                                <asp:ListItem Text="2021" Value="21" />
                                                <asp:ListItem Text="2022" Value="22" />
                                                <asp:ListItem Text="2023" Value="23" />
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="form-group col-md-6">
                                        <label for="LastName">Security Code (CCV)</label>
                                        <asp:TextBox runat="server" CssClass="form-control" ID="CVV" Text="123" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default" runat="server" id="ModifyProcessPaymentContainer" visible="false">
                            <div class="panel-heading">
                                <h3 class="panel-title">Original Reservation</h3>
                            </div>
                            <div class="panel-body">
                                Total due to modify booking :
                                <span class="label label-info" style="padding: 0.2em 0.6em 0.3em; font-size: 20px;">
                                <asp:Literal runat="server" ID="TotalPayementDue"></asp:Literal></span>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Payement Confirmation</h3>
                            </div>
                            <div class="panel-body">
                                Total to secure booking :
                                <span class="label label-info" style="padding: 0.2em 0.6em 0.3em; font-size: 20px;">
                                    <asp:Literal runat="server" ID="TotalPayement"></asp:Literal></span>
                                <asp:Button CssClass="btn btn-lg extracallbtn btn-warning" Text="BOOK CAR" ID="PayCallBtn" runat="server" OnClick="paynowBtn_Clicked" />
                                <%--<asp:Button CssClass="btn btn-lg extracallbtn btn-warning" Text="TEST ORDER" ID="Button1" runat="server" OnClick="ordernowBtn_Clicked" />--%>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
</body>
</html>