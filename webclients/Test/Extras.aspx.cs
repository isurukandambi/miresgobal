﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Linq;
using ORM;
using TestClient;
using CMSConfigs;

public partial class Extras : System.Web.UI.Page
{
    private Booking _booking = new Booking();
    private Car _selectedCar = new Car();
    private CarProvider _selectedbrand = new CarProvider();
    private BrandT _brandSelected = new BrandT();
    private AccessoryT _accessory = new AccessoryT();
    DatabaseEntities db;
    private static readonly XNamespace XmlNamespace = "http://www.opentravel.org/OTA/2003/05";
    protected void Page_Load(object sender, EventArgs e)
    {
        _booking = _booking.Session;
        LoadCar();
        if (!Page.IsPostBack)
        {
            LoadAccessories();
            LoadSelectedAccessories();
        }

    }
    protected void LoadCar()
    {
        string brand = Request.QueryString["Brand"].ToString();
        string sippCode = Request.QueryString["Car"].ToString();
        var brandSelected = _booking.Brands.Where(b => b.CarProvider.ToString() == brand).SingleOrDefault();
        if (brandSelected != null)
        {
            _selectedbrand = brandSelected.CarProvider;
            var car = brandSelected.Cars.Where(c => c.SippCode == sippCode && c.IsResRetReturnedCar == false);
            _selectedCar = car.FirstOrDefault();
        }
    }
    protected void LoadAccessories()
    {
        db = new DatabaseEntities();
        var accessories = (
                            from a in db.Accessories
                            //join c in db.Countries on a.CountryIDFK equals c.CountryID
                            //join l in db.Languages on a.LanguageIDFK equals l.LanguageID
                            //where c.CountryCode == _booking.CountryOfResidenceCode && l.LanguageCode == _booking.Language 
                            group a by a.HertzCode into h
                            select new
                            {
                                h.FirstOrDefault().Details,
                                h.FirstOrDefault().Title,
                                h.FirstOrDefault().HertzCode,
                                h.FirstOrDefault().Image1,
                                h.FirstOrDefault().Price
                            }
                            );
        ExtrasContainer.DataSource = accessories.ToList();
        ExtrasContainer.DataBind();

        //foreach (var accesso in accessories)
        //{

        //}

    }
    protected void LoadSelectedAccessories()
    {
        _booking = _booking.Session;
        for (int i = 0; i < ExtrasContainer.Items.Count; i++)
        {
            var accessoryQty = (TextBox)ExtrasContainer.Items[i].FindControl("Quantity");
            var accessoryType = (HiddenField)ExtrasContainer.Items[i].FindControl("hertzCode");
            var accessoryCheckBox = (CheckBox)ExtrasContainer.Items[i].FindControl("AccessoryCheckBox");

            var selectedExtra = _booking.Accessories.SingleOrDefault(a => a.EquipType == accessoryType.Value);
            if (selectedExtra != null)
            {
                accessoryQty.Text = selectedExtra.Quantity.ToString();
                accessoryCheckBox.Checked = true;
            }

        }
    }
    protected void paynowBtn_Clicked(object sender, EventArgs e)
    {

        Car selectedCar = _selectedCar;
        int accessCounter = 0;
        _booking = _booking.Session;


        _booking.Accessories.RemoveRange(0, _booking.Accessories.Count);
        _booking.Session = _booking;
        for (int i = 0; i < ExtrasContainer.Items.Count; i++)
        {
            var accessoryQty = (TextBox)ExtrasContainer.Items[i].FindControl("Quantity");
            var accessoryType = (HiddenField)ExtrasContainer.Items[i].FindControl("hertzCode");
            var accessoryImage = (HiddenField)ExtrasContainer.Items[i].FindControl("extraImage");
            var accessoryTitle = (HiddenField)ExtrasContainer.Items[i].FindControl("extraTitle");
            if (accessoryQty.Text == "0" || String.IsNullOrEmpty(accessoryQty.Text))
                accessoryQty.Text = "1";

            var accessoryCheckBox = (CheckBox)ExtrasContainer.Items[i].FindControl("AccessoryCheckBox");
            if (accessoryCheckBox.Checked)
            {
                AccessoryT acc = new AccessoryT
                {
                    IsSelected = true,
                    Delete = false,
                    Quantity = Convert.ToInt32(accessoryQty.Text),
                    EquipType = accessoryType.Value,
                    Image = "../Pics/Accessories/Original/" + accessoryImage.Value,
                    Title = accessoryTitle.Value
                };
                _booking.Accessories.Add(acc);
                accessCounter++;
            }
        }
        _booking.Session = _booking;
        callAccessoryRequest();
    }

    protected string getAccessoriesTitle(string hertzCode)
    {
        return HttpContext.GetGlobalResourceObject("SiteStrings", "Extra_" + hertzCode + "_Title").ToString();
    }


    protected void callAccessoryRequest()
    {
        string selectedAccessories = "";
        string accessoryResponse = "";
        if (CarAccessories() != null)
        {
            selectedAccessories = CarAccessories().ToString();
        }

        _booking = _booking.Session;

        if (_selectedbrand == CarProvider.Thrifty)
        {
            accessoryResponse = SendXml.HttpPostClient("http://" + CMSSettings.SiteUrl + "/thrifty", "<OTA_VehAvailRateRQ xmlns=\"http://www.opentravel.org/OTA/2003/05\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" version=\"1.008\" xsi:schemaLocation=\"http://www.opentravel.org/OTA/2003/05 OTA_VehAvailRateRQ.xsd\" MaxResponses=\"10\"><POS><Source AgentDutyCode=\"" + _booking.PartnerDetails.agentDutyCode + "\" ISOCountry=\"" + _booking.CountryOfResidenceCode + "\"><RequestorID Type=\"" + _booking.PartnerDetails.requestorIDType + "\" ID=\"" + _booking.PartnerDetails.requestorID + "\"><CompanyName Code=\"" + _booking.PartnerDetails.companyNameCode + "\" CodeContext=\"" + _booking.PartnerDetails.codeContext + "\" /></RequestorID></Source><Source><RequestorID Type=\"5\" ID=\"00109911\" /></Source></POS><VehAvailRQCore Status=\"All\"><VehRentalCore PickUpDateTime=\"" + DateTimeForXml(_booking.PickupDateTime) + "\" ReturnDateTime=\"" + DateTimeForXml(_booking.DropOffDateTime) + "\"><PickUpLocation LocationCode=\"" + _booking.PickUpLocationCode + "\" CodeContext=\"IATA\" /><ReturnLocation LocationCode=\"" + _booking.DropOffLocationCode + "\" CodeContext=\"IATA\" /></VehRentalCore><RateQualifier RateQualifier=\"" + _booking.thriftyRQ + "\" />" + selectedAccessories + "</VehAvailRQCore></OTA_VehAvailRateRQ>", _selectedbrand.ToString(), "OTA_VehAvailRateRQ").ToString();

        }
        else if (_selectedbrand == CarProvider.Hertz)
        {
            accessoryResponse = SendXml.HttpPostClient("http://" + CMSSettings.SiteUrl + "/hertz", "<OTA_VehAvailRateRQ xmlns=\"http://www.opentravel.org/OTA/2003/05\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" version=\"1.008\" xsi:schemaLocation=\"http://www.opentravel.org/OTA/2003/05 OTA_VehAvailRateRQ.xsd\" MaxResponses=\"10\"><POS><Source AgentDutyCode=\"" + _booking.PartnerDetails.agentDutyCode + "\" ISOCountry=\"" + _booking.CountryOfResidenceCode + "\"><RequestorID Type=\"" + _booking.PartnerDetails.requestorIDType + "\" ID=\"" + _booking.PartnerDetails.requestorID + "\"><CompanyName Code=\"" + _booking.PartnerDetails.companyNameCode + "\" CodeContext=\"" + _booking.PartnerDetails.codeContext + "\" /></RequestorID></Source><Source><RequestorID Type=\"5\" ID=\"00109911\" /></Source></POS><VehAvailRQCore Status=\"All\"><VehRentalCore PickUpDateTime=\"" + DateTimeForXml(_booking.PickupDateTime) + "\" ReturnDateTime=\"" + DateTimeForXml(_booking.DropOffDateTime) + "\"><PickUpLocation LocationCode=\"" + _booking.PickUpLocationCode + "\" CodeContext=\"IATA\" /><ReturnLocation LocationCode=\"" + _booking.DropOffLocationCode + "\" CodeContext=\"IATA\" /></VehRentalCore><RateQualifier RateQualifier=\"" + _booking.hertzRQ + "\" />" + selectedAccessories + "</VehAvailRQCore></OTA_VehAvailRateRQ>", _selectedbrand.ToString(), "OTA_VehAvailRateRQ").ToString();
        }
        else if (_selectedbrand == CarProvider.Dollar)
        {
            accessoryResponse = SendXml.HttpPostClient("http://" + CMSSettings.SiteUrl + "/dollar", "<OTA_VehAvailRateRQ xmlns=\"http://www.opentravel.org/OTA/2003/05\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" version=\"1.008\" xsi:schemaLocation=\"http://www.opentravel.org/OTA/2003/05 OTA_VehAvailRateRQ.xsd\" MaxResponses=\"10\"><POS><Source AgentDutyCode=\"" + _booking.PartnerDetails.agentDutyCode + "\" ISOCountry=\"" + _booking.CountryOfResidenceCode + "\"><RequestorID Type=\"" + _booking.PartnerDetails.requestorIDType + "\" ID=\"" + _booking.PartnerDetails.requestorID + "\"><CompanyName Code=\"" + _booking.PartnerDetails.companyNameCode + "\" CodeContext=\"" + _booking.PartnerDetails.codeContext + "\" /></RequestorID></Source><Source><RequestorID Type=\"5\" ID=\"00109911\" /></Source></POS><VehAvailRQCore Status=\"All\"><VehRentalCore PickUpDateTime=\"" + DateTimeForXml(_booking.PickupDateTime) + "\" ReturnDateTime=\"" + DateTimeForXml(_booking.DropOffDateTime) + "\"><PickUpLocation LocationCode=\"" + _booking.PickUpLocationCode + "\" CodeContext=\"IATA\" /><ReturnLocation LocationCode=\"" + _booking.DropOffLocationCode + "\" CodeContext=\"IATA\" /></VehRentalCore><RateQualifier RateQualifier=\"" + _booking.dollarRQ + "\" />" + selectedAccessories + "</VehAvailRQCore></OTA_VehAvailRateRQ>", _selectedbrand.ToString(), "OTA_VehAvailRateRQ").ToString();
        }
        else if (_selectedbrand == CarProvider.FireFly)
        {
            accessoryResponse = SendXml.HttpPostClient("http://" + CMSSettings.SiteUrl + "/firefly", "<OTA_VehAvailRateRQ xmlns=\"http://www.opentravel.org/OTA/2003/05\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" version=\"1.008\" xsi:schemaLocation=\"http://www.opentravel.org/OTA/2003/05 OTA_VehAvailRateRQ.xsd\" MaxResponses=\"10\"><POS><Source AgentDutyCode=\"" + _booking.PartnerDetails.agentDutyCode + "\" ISOCountry=\"" + _booking.CountryOfResidenceCode + "\"><RequestorID Type=\"" + _booking.PartnerDetails.requestorIDType + "\" ID=\"" + _booking.PartnerDetails.requestorID + "\"><CompanyName Code=\"" + _booking.PartnerDetails.companyNameCode + "\" CodeContext=\"" + _booking.PartnerDetails.codeContext + "\" /></RequestorID></Source><Source><RequestorID Type=\"5\" ID=\"00109911\" /></Source></POS><VehAvailRQCore Status=\"All\"><VehRentalCore PickUpDateTime=\"" + DateTimeForXml(_booking.PickupDateTime) + "\" ReturnDateTime=\"" + DateTimeForXml(_booking.DropOffDateTime) + "\"><PickUpLocation LocationCode=\"" + _booking.PickUpLocationCode + "\" CodeContext=\"IATA\" /><ReturnLocation LocationCode=\"" + _booking.DropOffLocationCode + "\" CodeContext=\"IATA\" /></VehRentalCore><RateQualifier RateQualifier=\"" + _booking.fireflyRQ + "\" />" + selectedAccessories + "</VehAvailRQCore></OTA_VehAvailRateRQ>", _selectedbrand.ToString(), "OTA_VehAvailRateRQ").ToString();
        }

        GetAccessoryPricing(accessoryResponse, _selectedbrand, true);
        _booking.Session = _booking;
        //check has errors
        // display availablity error message if one was returned by XML
        if (!String.IsNullOrEmpty(_booking.ErrorDetails.ErrorCode))
        {
            NoAccessoriesForSelectedCar.Visible = true;
            ErrorList.Text = _booking.ErrorDetails.ErrorMsg;

            // uncheck all tickboxes for accessories and reset values
            for (int i = 0; i < ExtrasContainer.Items.Count; i++)
            {
                var accessoryQty = (TextBox)ExtrasContainer.Items[i].FindControl("Quantity");
                accessoryQty.Text = "0";

                var accessoryCheckBox = (CheckBox)ExtrasContainer.Items[i].FindControl("AccessoryCheckBox");
                accessoryCheckBox.Checked = false;

                List<AccessoryT> accessoriesSelected = _booking.Accessories.Where(a => a.IsSelected).ToList();
                foreach (var acc in accessoriesSelected)
                {
                    acc.IsSelected = false;
                }
            }
            _booking.Session = _booking;
        }
        else
        {
            Response.Redirect("/webclients/Test/Review.aspx?Brand=" + _selectedbrand.ToString() + "&Car=" + _selectedCar.SippCode + "&PrePaid=y");
        }
    }
    public XElement CarAccessories()
    {
        XElement accessoriesXml = null;

        if (_booking.Accessories.Any(a => a.IsSelected && !a.Delete))
        {
            accessoriesXml = new XElement(XmlNamespace + "SpecialEquipPrefs");

            // loop through the accessories and build up the XML
            foreach (var accessory in _booking.Accessories.Where(a => a.IsSelected && !a.Delete))
            {
                var accessoryElement = new XElement(XmlNamespace + "SpecialEquipPref",
                                                         new XAttribute("EquipType", accessory.EquipType),
                                                         new XAttribute("Quantity", accessory.Quantity)
                    );
                accessoriesXml.Add(accessoryElement);
            }
        }
        return accessoriesXml;
    }
    // Hertz XML expects yyyy-mm-ddThh:mm:ss
    public string DateTimeForXml(DateTime date)
    {
        return date.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss");
    }

    // verify accessory requested is available for the car selected
    public void GetAccessoryPricing(string resultsFromHertz, CarProvider carProvider, bool isPrePaid)
    {
        _booking = _booking.Session;

        var xmlDoc = new XmlDocument();
        xmlDoc.LoadXml(resultsFromHertz);

        var namespaceMgr = new XmlNamespaceManager(xmlDoc.NameTable);
        namespaceMgr.AddNamespace("a1", "http://www.opentravel.org/OTA/2003/05");

        //get the pricing
        if (xmlDoc.DocumentElement != null)
        {

            // get pricing details
            XmlNode priceNode = xmlDoc.DocumentElement.SelectSingleNode("//a1:PaymentRule", namespaceMgr);
            XmlNode vehicleChargesNode = xmlDoc.DocumentElement.SelectSingleNode("//a1:VehicleCharges", namespaceMgr);
            XmlNode estPriceNode = xmlDoc.DocumentElement.SelectSingleNode("//a1:TotalCharge", namespaceMgr);
            _selectedCar = PrePayPrice(priceNode, vehicleChargesNode, _selectedCar);

            XmlNodeList nodeList = xmlDoc.DocumentElement.SelectNodes("//a1:PricedEquip", namespaceMgr);
            if (nodeList != null)
            {
                foreach (XmlNode node in nodeList)
                {
                    XmlNode equipment = node.SelectSingleNode(".//a1:Equipment", namespaceMgr);
                    if ((equipment != null) && (equipment.Attributes != null))
                    {
                        string equipType = equipment.Attributes["EquipType"].Value;
                        _accessory = _booking.Accessories.FirstOrDefault(a => a.EquipType == equipType);
                        XmlNode charge = node.SelectSingleNode(".//a1:Charge", namespaceMgr);
                        if ((charge != null) && (charge.Attributes != null))
                        {
                            decimal amount = 0;
                            if (charge.Attributes["Amount"] != null)
                            {
                                amount = Convert.ToDecimal(charge.Attributes["Amount"].Value, System.Globalization.CultureInfo.InvariantCulture);
                            }
                            //string currencyCode = charge.Attributes["CurrencyCode"].Value;
                            if (_accessory != null)
                            {
                                _accessory.Price = String.Format("{0:0.00}", (IncludeTaxRate(xmlDoc.SelectSingleNode("//a1:TaxAmount", namespaceMgr), amount)));
                                _accessory.CurrencySymbol = charge.Attributes["CurrencyCode"].Value;
                            }
                        }
                    }
                }
            }
        }
        _booking.ErrorDetails.ErrorMsg = "";
        _booking.ErrorDetails.ErrorCode = "";
        XmlNodeList errorList = xmlDoc.DocumentElement.SelectNodes(".//a1:Error", namespaceMgr);
        if (errorList.Count > 0)
        {
            foreach (XmlNode node in errorList)
            {
                _booking.ErrorDetails.ErrorMsg += node.Attributes["ShortText"].Value + "</br>";
                if (node.Attributes["Code"] != null)
                {
                    _booking.ErrorDetails.ErrorCode = node.Attributes["Code"].Value;
                }
                else
                {
                    _booking.ErrorDetails.ErrorCode = node.Attributes["Type"].Value;
                }
            }
        }
        _booking.Session = _booking;
    }
    private Car PrePayPrice(XmlNode priceNode, XmlNode vehicleChargesNode, Car car)
    {
        var xmlDoc = new XmlDocument();
        var namespaceMgr = new XmlNamespaceManager(xmlDoc.NameTable);
        namespaceMgr.AddNamespace("a1", "http://www.opentravel.org/OTA/2003/05");

        // reset pricing details
        car.Price = 0m;

        car.Currency = "";
        car.CurrencySymbol = "";
        // look for new pricing details
        if (priceNode != null)
        {
            if (priceNode.Attributes != null && priceNode.Attributes["RuleType"].Value == "2")
            {
                car.Price = Convert.ToDecimal(priceNode.Attributes["Amount"].Value, System.Globalization.CultureInfo.InvariantCulture);
                string currencyCode = priceNode.Attributes["CurrencyCode"].Value;
                car.Currency = currencyCode;
            }
        }
        car.IsPrePaid = true;
        return car;
    }
    private decimal IncludeTaxRate(XmlNode taxNode, decimal itemPrice)
    {
        decimal percentage = 0;
        if (taxNode != null)
        {
            if (taxNode.Attributes != null)
                percentage = Convert.ToDecimal(taxNode.Attributes["Percentage"].Value, System.Globalization.CultureInfo.InvariantCulture);
        }

        // new accessory price = current accessory price + (tax percentage of existing accessory price)
        itemPrice = itemPrice + (itemPrice * (percentage / 100));

        return Math.Round(itemPrice, 2);
    }

    protected void termsBtn_Clicked(object sender, EventArgs e)
    {
        Car selectedCar = _selectedCar;
        _booking = _booking.Session;
        string TermsResponse = "";

        if (_selectedbrand == CarProvider.Thrifty)
        {
            TermsResponse = SendXml.HttpPostClient("http://" + CMSSettings.SiteUrl + "/thrifty", "<CT_RentalConditionsRQ xmlns=\"http://www.opentravel.org/OTA/2003/05\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" version=\"1.008\" xsi:schemaLocation=\"http://www.opentravel.org/OTA/2003/05 CT_RentalConditionsRQ.xsd\" PrimaryLangID=\"" +_booking.Language + "\"><POS><Source AgentDutyCode=\"" + _booking.PartnerDetails.agentDutyCode + "\" ISOCountry=\"" + _booking.CountryOfResidenceCode + "\"><RequestorID Type=\"" + _booking.PartnerDetails.requestorIDType + "\" ID=\"" + _booking.PartnerDetails.requestorID + "\"><CompanyName Code=\"" + _booking.PartnerDetails.companyNameCode + "\" CodeContext=\"" + _booking.PartnerDetails.codeContext + "\" /></RequestorID></Source><Source><RequestorID Type=\"5\" ID=\"145180\" /></Source></POS><VehResRQCore Status=\"All\"><VehRentalCore PickUpDateTime=\"" + DateTimeForXml(_booking.PickupDateTime) + "\" ReturnDateTime=\"" + DateTimeForXml(_booking.DropOffDateTime) + "\"><PickUpLocation CodeContext=\"CARTRAWLER\" LocationCode=\"" + _booking.PickUpLocationCode + "\"/><ReturnLocation CodeContext=\"CARTRAWLER\" LocationCode=\"" + _booking.DropOffLocationCode + "\"/></VehRentalCore><RateQualifier TravelPurpose=\"2\" RateQualifier=\"" + _booking.thriftyRQ + "\" /><Customer><Primary><CitizenCountryName Code=\"" + _booking.CountryOfResidenceCode + "\" /></Primary></Customer></VehResRQCore><VehResRQInfo><Reference Type=\"16\" ID=\"" + _selectedCar.HertzReference + "\" ID_Context=\"CARTRAWLER\" URL=\"" + _selectedCar.CTCarURL + "\" /></VehResRQInfo></CT_RentalConditionsRQ>", _selectedbrand.ToString(), "CT_RentalConditionsRQ").ToString();

            var xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(TermsResponse);

            var namespaceMgr = new XmlNamespaceManager(xmlDoc.NameTable);
            namespaceMgr.AddNamespace("a1", "http://www.opentravel.org/OTA/2003/05");

            string type = XmlUtils.getXmlString(xmlDoc);
            string rentalXML = "";

            XmlNode RentalNode = xmlDoc.DocumentElement.SelectSingleNode("//a1:RentalConditions", namespaceMgr);
            if (RentalNode != null)
            {
                XmlNodeList subSection = RentalNode.SelectNodes(".//a1:SubSection", namespaceMgr);
                foreach (XmlNode sub in subSection)
                {
                    rentalXML = rentalXML + "Title - '" + sub.Attributes["Title"].Value + "' </br></br>";

                    XmlNodeList paragraph = sub.SelectNodes(".//a1:Paragraph", namespaceMgr);
                    foreach (XmlNode paragra in sub)
                    {
                        rentalXML = rentalXML + "*" + paragra.InnerText + "</br>";
                    }
                    rentalXML = rentalXML + "</br></br></br></br>";
                }
            }
            terms.Text = rentalXML;

        }
        else
        {
            terms.Text = "Only can get Thrifty CarTrawler Brands";
        }
    }
}