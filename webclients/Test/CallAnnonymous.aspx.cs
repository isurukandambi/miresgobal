﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TestClient;
using ServiceRateReference;
using ServiceReservationReference;
using CMSConfigs;
using System.Xml;

public partial class webclients_Test_CallAnnonymous : System.Web.UI.Page
{
    private XmlDocument xmlDoc;
    private static readonly string XmlNamespace = "http://www.opentravel.org/OTA/2003/05";
    protected void Page_Load(object sender, EventArgs e)
    {
        //Response.Write("Test");

        string ctDoc = "<?xml version=\"1.0\" encoding=\"UTF-8\"?> " +
            "<OTA_VehAvailRateRQ xmlns=\"http://www.opentravel.org/OTA/2003/05\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.opentravel.org/OTA/2003/05 OTA_VehAvailRateRQ.xsd\" Target=\"Test\" Version=\"1.005\">"+
            "<POS>"+
            "<Source ISOCurrency=\"EUR\">"+
            "<RequestorID Type=\"16\" ID=\"621233\" ID_Context=\"CARTRAWLER\" />" +
            "</Source>"+
            "</POS>"+
            "<VehAvailRQCore Status=\"Available\">"+
            "<VehRentalCore PickUpDateTime=\"2016-03-27T09:00:00\" ReturnDateTime=\"2016-03-30T16:00:00\">" +
            "<PickUpLocation CodeContext=\"CARTRAWLER\" LocationCode=\"219\" />" +
            "<ReturnLocation CodeContext=\"CARTRAWLER\" LocationCode=\"219\" />" +
            "</VehRentalCore>"+
            "<DriverType Age='30'/>"+
            "</VehAvailRQCore>"+
            "<VehAvailRQInfo>"+
            //"<Customer>"+
            //"<Primary>"+
            //"<CitizenCountryName Code='IE' />"+
            //"</Primary>"+
            //"</Customer>"+
            "<TPA_Extensions>"+
            "<ConsumerIP>182.456.432.123</ConsumerIP>"+
            "</TPA_Extensions>"+
            "</VehAvailRQInfo>"+
            "</OTA_VehAvailRateRQ>";

        string ctAvail = "<?xml version=\"1.0\" encoding=\"UTF-8\"?> " +
                         "<CT_RentalConditionsRQ xmlns=\"http://www.opentravel.org/OTA/2003/05\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.opentravel.org/OTA/2003/05 OTA_VehResRQ.xsd\" Target=\"Test\" PrimaryLangID=\"EN\" Version=\"1.000\">"+
                         "<POS>"+
                         "<Source ISOCurrency=\"EUR\">"+
                         "<RequestorID Type=\"16\" ID=\"621233\" ID_Context=\"CARTRAWLER\" />" +
                         "</Source>"+
                         "</POS>"+
                         "<VehResRQCore Status=\"All\">"+
                         "<VehRentalCore PickUpDateTime=\"2016-06-26T12:00:00\" ReturnDateTime=\"2016-06-29T12:00:00\">" +
                         "<PickUpLocation CodeContext=\"CARTRAWLER\" LocationCode=\"2446\"/>" +
                         "<ReturnLocation CodeContext=\"CARTRAWLER\" LocationCode=\"2446\"/>" +
                         "</VehRentalCore>"+
                         "<Customer>"+
                         "<Primary>"+
                         "<CitizenCountryName Code=\"US\" />"+
                         "</Primary>"+
                         "</Customer>"+
                         "</VehResRQCore>"+
                         "<VehResRQInfo>"+
                         "<Reference Type=\"16\" ID=\"620326447\" ID_Context=\"CARTRAWLER\" URL=\"87370621-5038-4a60-849d-f9e0b563775e.63\" />" +
                         "</VehResRQInfo>"+
                         "</CT_RentalConditionsRQ>";

        string ctRental = "<?xml version=\"1.0\" encoding=\"UTF-16\"?>" +
                          "<CT_RentalConditionsRQ xmlns=\"http://www.opentravel.org/OTA/2003/05\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" version=\"1.008\" xsi:schemaLocation=\"http://www.opentravel.org/OTA/2003/05 CT_RentalConditionsRQ.xsd\" PrimaryLangID=\"EN\">" +
                          "<POS>" +
                          "<Source AgentDutyCode=\"J3C15O18R15\" ISOCountry=\"IE\">" +
                          "<RequestorID Type=\"4\" ID=\"T985\">"+
                          "<CompanyName Code=\"SS\" CodeContext=\"DWDX\" />"+
                          "</RequestorID>"+
                          "</Source>"+
                          "</POS>"+
                          "<VehResRQCore Status=\"All\">"+
                          "<VehRentalCore PickUpDateTime=\"2016-06-30T12:00:00\" ReturnDateTime=\"2016-07-03T12:00:00\">" +
                          "<PickUpLocation CodeContext=\"CARTRAWLER\" LocationCode=\"ABY\"/>" +
                          "<ReturnLocation CodeContext=\"CARTRAWLER\" LocationCode=\"ABY\"/>" +
                          "</VehRentalCore>"+
                          "<RateQualifier RateQualifier=\"PPP\" />"+
                          "<Customer>"+
                          "<Primary>"+
                          "<CitizenCountryName Code=\"IE\" />"+
                          "</Primary>"+
                          "</Customer>"+
                          "</VehResRQCore>"+
                          "<VehResRQInfo>"+
                          "<Reference Type=\"16\" ID=\"1340169260\" ID_Context=\"CARTRAWLER\" URL=\"a1dde241-a8a6-4e14-8756-74f0d0fc622c.63\" />" +
                          "</VehResRQInfo>"+
                          "</CT_RentalConditionsRQ>";

        string ctRes = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"+
                       "<OTA_VehResRQ xmlns=\"http://www.opentravel.org/OTA/2003/05\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.opentravel.org/OTA/2003/05 OTA_VehResRQ.xsd\" Target=\"Test\" Version=\"1.005\">"+
                       "<POS>"+
                       "<Source ISOCurrency=\"EUR\">"+
                       "<RequestorID Type=\"16\" ID=\"621233\" ID_Context=\"CARTRAWLER\" />"+
                       "</Source>"+
                       "<Source>"+
                       "<RequestorID Type=\"16\" ID=\"621233\" ID_Context=\"ORDERID\" />"+
                       "</Source>"+
                       "</POS>"+
                       "<VehResRQCore Status=\"All\">"+
                       "<VehRentalCore PickUpDateTime=\"2016-07-19T12:00:00\" ReturnDateTime=\"2016-07-20T12:00:00\">" +
                       "<PickUpLocation CodeContext=\"CARTRAWLER\" LocationCode=\"4513\"/>" +
                       "<ReturnLocation CodeContext=\"CARTRAWLER\" LocationCode=\"4513\"/>" +
                       "</VehRentalCore>"+
                       "<Customer>"+
                       "<Primary>"+
                       "<PersonName>"+
                       "<NamePrefix>Mr.</NamePrefix>"+
                       "<GivenName>Joe</GivenName>"+
                       "<Surname>Bailey</Surname>"+
                       "</PersonName>"+
                       "<Telephone PhoneTechType=\"1\" AreaCityCode=\"405\" PhoneNumber=\"5555828\"/>"+
                       "<Email EmailType=\"2\">JoeBailey@work.com</Email>"+
                       "<Address Type=\"2\">"+
                       "<AddressLine>123 Elm St., Oklahoma City, 73112, Oklahoma</AddressLine>"+
                       "<CountryName Code=\"IE\" />"+
                       "</Address>"+
                       "<CitizenCountryName Code=\"IE\" />"+
                       "</Primary>"+
                       "</Customer>"+
                       "<DriverType Age=\"29\"/>"+
                       //"<SpecialEquipPrefs>"+
                       //"<SpecialEquipPref EquipType=\"8\" Quantity=\"1\"/>"+
                       //"<SpecialEquipPref EquipType=\"9\" Quantity=\"2\"/>"+
                       //"</SpecialEquipPrefs>"+
                       "</VehResRQCore>"+
                       "<VehResRQInfo PassengerQty=\"3\">"+
                       "<ArrivalDetails TransportationCode=\"14\" Number=\"123\">"+
                       "<OperatingCompany>FR</OperatingCompany>"+
                       "</ArrivalDetails>"+
                       "<RentalPaymentPref>"+
                       "<PaymentCard CardType=\"1\" CardCode=\"VI\" CardNumber=\"4263971921001307\" ExpireDate=\"1217\" SeriesCode=\"123\">" +
                       "<CardHolderName>Joe Bailey Jr</CardHolderName>"+
                       "</PaymentCard>"+
                       "</RentalPaymentPref>"+
                       "<Reference Type=\"16\" ID=\"1199415256\" ID_Context=\"CARTRAWLER\" DateTime=\"2016-07-13T10:38:07.302+01:00\" URL=\"cc1f127e-e32a-462e-8aac-fe80794932e5.63\"/>" +
                       "<TPA_Extensions>"+
                       "<CompanyName VAT=\"98765\">Cartrawler Ltd</CompanyName>"+
                       "</TPA_Extensions>"+
                       "</VehResRQInfo>"+
                       "</OTA_VehResRQ>";

        try
        {
            Response.ContentType = "text/xml";
            Response.ContentEncoding = System.Text.Encoding.ASCII;
            //string fireflyResponse = SendXml.HttpPost("http://" + CMSSettings.SiteUrl + "/firefly", "<OTA_VehAvailRateRQ xmlns=\"http://www.opentravel.org/OTA/2003/05\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" version=\"1.008\" xsi:schemaLocation=\"http://www.opentravel.org/OTA/2003/05 OTA_VehAvailRateRQ.xsd\" MaxResponses=\"10\"><POS><Source AgentDutyCode=\"R18Y25A114N\" ISOCountry=\"ES\"><RequestorID Type=\"5\" ID=\"00109911\"><CompanyName Code=\"CP\" CodeContext=\"XM77\" /></RequestorID></Source><Source><RequestorID Type=\"5\" ID=\"00109911\" /></Source></POS><VehAvailRQCore Status=\"All\"><VehRentalCore PickUpDateTime=\"2015-08-27T09:00:00\" ReturnDateTime=\"2015-08-30T16:00:00\"><PickUpLocation LocationCode=\"DUB\" CodeContext=\"IATA\" /><ReturnLocation LocationCode=\"DUB\" CodeContext=\"IATA\" /></VehRentalCore><RateQualifier RateQualifier=\"MIRES\" /></VehAvailRQCore><VehAvailRQInfo><ArrivalDetails TransportationCode=\"14\"><OperatingCompany Code=\"FR\"></OperatingCompany></ArrivalDetails></VehAvailRQInfo></OTA_VehAvailRateRQ>", requestTime, "dev.miresglobal.com", "FireFly", "OTA_VehAvailRateRQ").ToString();
            //string annonResponse = SendXml.HttpPost("http://" + CMSSettings.SiteUrl + "/thrifty", ctRental, "Thrifty", "CT_RentalConditionsRQ").ToString();
            string annonResponse = SendXml.HttpPost("https://external-dev.cartrawler.com/cartrawlerpay", ctRes, "Thrifty", "OTA_VehResRQ").ToString();
            //string annonResponseAvail = SendXml.HttpPost("https://external-dev.cartrawler.com/cartrawlerota", ctAvail, "Thrifty", "CT_RentalConditionsRQ").ToString();
            //SendXml sx = new SendXml();
            //string annonResponse = SendXml.HttpPost("http://" + CMSSettings.SiteUrl + "/thrifty", testDtag, "Thrifty", "OTA_VehAvailRateRQ").ToString();
            //string annonResponse = SendXml.HttpPost("https://www.thermeon.com/options.html?api_version=1.0000&webxg_id=123457&rate_id=1234567890ABC", thermeon, requestTime, "dev.miresglobal.com", "FireFly", "OTA_VehAvailRateRQ").ToString();
            //string annonResponse = sx.createPostRequest("http://otatest.cartrawler.com:20002/cartrawlerota", ctping);
            //Response.Write(ctDoc + "<br/>");
            //Response.Write(annonResponse);

            //xmlDoc = new XmlDocument();
            //xmlDoc.LoadXml(annonResponse);
            //Response.Write(ctAvail+"<br/>");
            //var namespaceMgr = new XmlNamespaceManager(xmlDoc.NameTable);
            //namespaceMgr.AddNamespace("a1", XmlNamespace);

            //XmlNode TypeNode = xmlDoc.DocumentElement.SelectSingleNode("//a1:Type", namespaceMgr);
            //string type = TypeNode.InnerText.ToString();
            //Response.Write(type);
            Response.Write(annonResponse);

            //Response.Write("Test2");
            //Response.Write(xmlDoc.ToString());
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message + "|||");
            Response.Write(ex.StackTrace);
        }
    }


}