﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ViewBooking.aspx.cs" Inherits="Viewbooking" %>

<%@ Register Src="~/webclients/Test/Controls/ViewBookedVehicle.ascx" TagName="ViewBookedVehicle" TagPrefix="vbv1" %>
<%@ Register Src="~/webclients/Test/Controls/BookingForm.ascx" TagName="FullForm" TagPrefix="ff1" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Client for Miresglobal</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <style type="text/css">
        .booking-form {
            padding: 20px;
            border-radius: 10px;
            border: 1px #ECECEC solid;
        }

        .extracallbtn {
            float: right;
        }

        .backbtn {
            width: 110px;
            float: left;
            margin-bottom: 8px;
        }

        .brandLogo {
            margin: 6px;
        }

        .accessory-row {
            border-bottom: 1px #cecece solid;
        }

            .accessory-row:last-child {
                border-bottom: none;
            }
            .push-margin{
                margin-right: 10px;
            }
    </style>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <form role="form" runat="server">
        <div class="container-fluid">
            <div class="row" style="margin-bottom: 20px;">
                <div class="col-xs-12 col-md-12" style="background-color: #1f3469;">
                    <h1 style="color: #fff; letter-spacing: 2px;">Mires Global</h1>
                </div>
            </div>
            <div class="row" style="margin-left: 0px; margin-right: 0px;">
                <%--<ff1:FullForm ID="FullForm" runat="server" />--%>
                <div class="col-xs-12 col-md-12">
                    <div class="row" style="margin-left: 0px; margin-right: 0px;" runat="server" visible="false" id="ValidationMessage">
                        <div class="alert alert-info" role="alert" style="margin-top: 50px;">
                            <asp:Literal runat="server" id="AlertMessage" ></asp:Literal>
                        </div>
                    </div>
                    <div class="row" style="margin-left: 0px; margin-right: 0px;">
                        <div class="col-xs-12 col-md-12">
                            <vbv1:ViewBookedVehicle ID="ViewBookedVehicle" runat="server" />
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </form>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
</body>
</html>