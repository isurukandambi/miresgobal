﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Linq;
using ORM;
using TestClient;
using CMSConfigs;

public partial class Test_Review : System.Web.UI.Page
{
    private Booking _booking = new Booking();
    private Car _selectedCar = new Car();
    private CarProvider _selectedbrand = new CarProvider();
    private BrandT _brandSelected = new BrandT();
    private AccessoryT _accessory = new AccessoryT();
    DatabaseEntities db;
    private static readonly XNamespace XmlNamespace = "http://www.opentravel.org/OTA/2003/05";
    private static readonly XNamespace XmlSchemaInstance = "http://www.w3.org/2001/XMLSchema-instance";
    protected void Page_Load(object sender, EventArgs e)
    {
        _booking = _booking.Session;
        LoadCar();
        if (!Page.IsPostBack)
        {
            LoadAccessories();
            LoadTotalCharges();
            LoadCountries();
        }
        AccessoryLink.NavigateUrl = CMSSettings.HttpType + "://" + CMSSettings.SiteUrl + "/Test/Extras.aspx?Brand=" + _selectedbrand.ToString() + "&Car=" + _selectedCar.SippCode + "&PrePaid=y";

    }
    protected void LoadCountries()
    {
        db = new DatabaseEntities();
        var countries = db.Countries.OrderBy(c => c.CountryName);
        Country.DataSource = countries.ToList();
        Country.DataValueField = "CountryCode";
        Country.DataTextField = "CountryName";
        Country.DataBind();
    }
    protected void LoadCar()
    {
        
        string brand = Request.QueryString["Brand"].ToString();
        string sippCode = Request.QueryString["Car"].ToString();
        var brandSelected = _booking.Brands.Where(b => b.CarProvider.ToString() == brand).SingleOrDefault();
        if (brandSelected != null)
        {
            //Response.Write("Brand = " + brandSelected.CarProvider);
            _selectedbrand = brandSelected.CarProvider;
            var car = brandSelected.Cars.Where(c => c.SippCode == sippCode && c.IsResRetReturnedCar == false);
            _selectedCar = car.FirstOrDefault();
        }
    }
    protected void LoadAccessories()
    {
        _booking = _booking.Session;
        var accessories = _booking.Accessories.Where(a => a.IsSelected == true);
        ExtrasContainer.DataSource = accessories.ToList();
        ExtrasContainer.DataBind();

        if (accessories.Count() == 0)
        {
            ExtrasHolder.Visible = false;
        }
    }

    protected void LoadTotalCharges()
    {
        _booking = _booking.Session;
        var totalAccessoryCharge = _booking.Accessories.Where(a => a.IsSelected).Sum(accessory => Convert.ToDecimal(accessory.Price));
        TotalPayement.Text = _selectedCar.Currency + " " + String.Format("{0:0.00}", (Convert.ToDecimal(_selectedCar.Price) + totalAccessoryCharge));

        if (_booking.ModifyProcess == true)
        {
            ModifyProcessPaymentContainer.Visible = true;
            TotalPayementDue.Text = _selectedCar.Currency + " " + String.Format("{0:0.00}", (Convert.ToDecimal(_booking.OriginalBooking.BookedCar.Price) - (Convert.ToDecimal(_selectedCar.Price) + totalAccessoryCharge)));
        }
        else
        {
            ModifyProcessPaymentContainer.Visible = false;
        }

    }
    protected void paynowBtn_Clicked(object sender, EventArgs e)
    {
        //validation not done
        string selectedAccessories = "";
        string bookingResponse = "";
        if (CarAccessories() != null)
        {
            selectedAccessories = CarAccessories().ToString();
        }
        _booking = _booking.Session;
        DateTime requestTime = DateTime.Now;


        string ISOLangCode = _booking.Language.ToUpper() + _booking.CountryOfResidenceCode.ToUpper();
        //remove this
        //CarXmlAction sellCall = new CarXmlAction();

        string hertzGldNumer = "";
        if (!string.IsNullOrEmpty(HertzClubNumber.Text))
        {
            hertzGldNumer = "<CustLoyalty MembershipID=\"" + HertzClubNumber.Text + "\" ProgramID=\"ZE\" TravelSector=\"2\"/>";
        }

        if (_booking.ModifyProcess == true)
        {
            string modifyRequest = "<OTA_VehModifyRQ xmlns=\"http://www.opentravel.org/OTA/2003/05\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.opentravel.org/OTA/2003/05 OTA_VehModifyRQ.xsd\" Version=\"1.008\">" +
                      "<POS>" +
                        "<Source ISOCountry=\"" + _booking.CountryOfResidenceCode + "\" AgentDutyCode=\"" + _booking.PartnerDetails.agentDutyCode + "\">" +
                          "<RequestorID Type=\"" + _booking.PartnerDetails.requestorIDType + "\" ID=\"" + _booking.PartnerDetails.requestorID + "\">" +
                            "<CompanyName Code=\"" + _booking.PartnerDetails.companyNameCode + "\" CodeContext=\"" + _booking.PartnerDetails.codeContext + "\"/>" +
                          "</RequestorID>" +
                        "</Source>" +
                        "<Source>" +
                          "<RequestorID Type=\"5\" ID=\"74004604\" />" +
                        "</Source>" +
                      "</POS>" +
                      "<VehModifyRQCore Status=\"Confirmed\" ModifyType=\"Book\">" +
                        "<UniqueID Type=\"14\" ID=\"" + _booking.ConfirmationId + "\" />" +
                        "<VehRentalCore PickUpDateTime=\"" + DateTimeForXml(_booking.PickupDateTime) + "\" ReturnDateTime=\"" + DateTimeForXml(_booking.DropOffDateTime) + "\">" +
                          "<PickUpLocation LocationCode=\"" + _booking.PickUpLocationCode + "\" CodeContext=\"IATA\" />" +
                          "<ReturnLocation LocationCode=\"" + _booking.DropOffLocationCode + "\" CodeContext=\"IATA\" />" +
                        "</VehRentalCore>" +
                        "<Customer>" +
                          "<Primary>" +
                            "<PersonName>" +
                              "<GivenName>" + FirstName.Text + "</GivenName>" +
                              "<Surname>" + LastName.Text + "</Surname>" +
                            "</PersonName>" +
                            "<Telephone PhoneTechType=\"1\" AreaCityCode=\"00353\" PhoneNumber=\"" + TelephoneNumber.Text + "\" />" +
                            "<Email>" + Email.Text + "</Email>" +
                            "<Address>" +
                              "<AddressLine>" + Address1.Text + ", " + Address2.Text + "</AddressLine>" +
                              "<CityName>" + Town.Text + "</CityName>" +
                              "<CountryName Code=\"" + Country.SelectedValue + "\" />" +
                            "</Address>" +
                            "<Document DocType=\"4\" DocID=\"123456789\" DocIssueLocation=\"USNY\" ExpireDate=\"2019-06-13\"/>" +
                         " </Primary>" +
                        "</Customer>" +
                        "<VehPref Code=\"" + _selectedCar.SippCode + "\" CodeContext=\"SIPP\" />" +
                        "<RateQualifier TravelPurpose=\"2\" CorpDiscountNmbr=\"2103945\" RateQualifier=\"" + setRQ(_selectedbrand.ToString()) + "\" />" +
                        selectedAccessories +
                      "</VehModifyRQCore>" +
                      "    <Reference Type=\"16\" ID=\"" + _selectedCar.HertzReference + "\"> " +
                    "  </Reference> " +
                    "</OTA_VehModifyRQ>";

            string qoteModifyRQ = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                            "<OTA_VehModifyRQ xmlns=\"http://www.opentravel.org/OTA/2003/05\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" version=\"1.008\" xsi:schemaLocation=\"http://www.opentravel.org/OTA/2003/05 OTA_VehModifyRQ.xsd\">" +
                            "<POS>" +
                            "<Source AgentDutyCode=\"" + _booking.PartnerDetails.agentDutyCode + "\" ISOCountry=\"" + _booking.CountryOfResidenceCode + "\">" +
                              "<RequestorID Type=\"" + _booking.PartnerDetails.requestorIDType + "\" ID=\"" + _booking.PartnerDetails.requestorID + "\">" +
                                "<CompanyName Code=\"" + _booking.PartnerDetails.companyNameCode + "\" CodeContext=\"" + _booking.PartnerDetails.codeContext + "\" />" +
                              "</RequestorID>" +
                            "</Source>" +
                            "<Source>" +
                              "<RequestorID Type=\"5\" ID=\"00109911\" />" +
                            "</Source>" +
                            "</POS>" +
                            "<VehModifyRQCore Status=\"Confirmed\" ModifyType=\"Book\">" +
                            "<UniqueID Type=\"14\" ID=\"" + _booking.ConfirmationId + "\" />" +
                            "<RateQualifier RateQualifier=\"" + setRQ(_selectedbrand.ToString()) + "\" />" +
                            "<SpecialEquipPrefs>" +
                              "<SpecialEquipPref EquipType=\"9\" Quantity=\"1\" Action=\"Delete\" />" +
                            "</SpecialEquipPrefs>" +
                            "</VehModifyRQCore>" +
                            "<VehModifyRQInfo>" +
                            "<Reference Type=\"16\" ID=\"WOT1B5ESYT55346-2501\"/>" +
                            "<SpecialReqPref>XXX</SpecialReqPref>" +
                            "</VehModifyRQInfo>" +
                            "</OTA_VehModifyRQ>";

             bookingResponse = SendXml.HttpPost("http://" + CMSSettings.SiteUrl + "/" + _selectedbrand.ToString().ToLower(), modifyRequest, _selectedbrand.ToString(), "OTA_VehModifyRQ");
        }
        else
        {
            string bookingreq = "<?xml version=\"1.0\" encoding=\"UTF-8\"?> " +
                "<OTA_VehResRQ xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns=\"http://www.opentravel.org/OTA/2003/05\" xsi:schemaLocation=\"http://www.opentravel.org/OTA/2003/05 OTA_VehResRQ.xsd\" Version=\"1.008\" > " +
                "  <POS> " +
                "    <Source ISOCountry=\"" + _booking.CountryOfResidenceCode + "\" AgentDutyCode=\"" + _booking.PartnerDetails.agentDutyCode + "\"> " +
                "      <RequestorID Type=\"" + _booking.PartnerDetails.requestorIDType + "\" ID=\"" + _booking.PartnerDetails.requestorID + "\"> " +
                "        <CompanyName Code=\"" + _booking.PartnerDetails.companyNameCode + "\" CodeContext=\"" + _booking.PartnerDetails.codeContext + "\"> " +
                "      </CompanyName> " +
                "    </RequestorID> " +
                "  </Source> " +
                "<Source>"+
                "<RequestorID Type=\"5\" ID=\"145180\" />"+
                "</Source>"+
                "</POS> " +
                "  <VehResRQCore Status=\"All\"> " +
                "    <VehRentalCore PickUpDateTime=\"" + DateTimeForXml(_booking.PickupDateTime) + "\" ReturnDateTime=\"" + DateTimeForXml(_booking.DropOffDateTime) + "\"> " +
                "      <PickUpLocation LocationCode=\"" + _booking.PickUpLocationCode + "\" CodeContext=\"IATA\"> " +
                "    </PickUpLocation> " +
                "      <ReturnLocation LocationCode=\"" + _booking.DropOffLocationCode + "\" CodeContext=\"IATA\"> " +
                "    </ReturnLocation> " +
                "  </VehRentalCore> " +
                "    <Customer> " +
                "      <Primary BirthDate=\"1959-06-13\"> " +
                "        <PersonName> " +
                "          <GivenName>" + FirstName.Text + "</GivenName> " +
                "          <Surname>" + LastName.Text + "</Surname> " +
                "        </PersonName> " +
                "        <Telephone PhoneTechType=\"1\" AreaCityCode=\"4\" PhoneNumber=\"" + TelephoneNumber.Text + "\" FormattedInd=\"false\" DefaultInd=\"false\"> " +
                "      </Telephone> " +
                "        <Email DefaultInd=\"false\">" + Email.Text + "</Email> " +
                "        <Address DefaultInd=\"false\" FormattedInd=\"false\"> " +
                "          <AddressLine>" + Address1.Text + ", " + Address2.Text + "</AddressLine> " +
                "          <CityName>" + Town.Text + "</CityName> " +
                "          <PostalCode>" + PostalCode.Text + "</PostalCode> " +
                "          <CountryName Code=\"" + Country.SelectedValue + "\"> " +
                "        </CountryName> " +
                "      </Address> " +
                "    <Document DocType=\"4\" DocID=\"123456789\" DocIssueLocation=\"USNY\" ExpireDate=\"2019-06-13\"/>" +
                "      <CustLoyalty MembershipID=\"" + HertzClubNumber.Text + "\" ProgramID=\"ZE\" TravelSector=\"2\"/>" +
                "    </Primary> " +
                "  </Customer> " +
                "<VehPref Code=\"" + _selectedCar.SippCode + "\" CodeContext=\"ACRISS\"></VehPref>" +
                "    <RateQualifier ArriveByFlight=\"false\" CorpDiscountNmbr=\"709295\" RateQualifier=\"" + setRQ(_selectedbrand.ToString()) + "\"> " +
                "  </RateQualifier>" +
                selectedAccessories +
                "</VehResRQCore> " +
                "  <VehResRQInfo GasPrePay=\"false\" SmokingAllowed=\"false\"> " +
                "    <ArrivalDetails TransportationCode=\"14\" Number=\"" + FlightNum.Text + "\"> " +
                "      <OperatingCompany Code=\"FR\"> " +
                "    </OperatingCompany> " +
                "  </ArrivalDetails> " +
                "    <RentalPaymentPref> " +
                //"      <PaymentCard CardType=\"1\" CardCode=\"" + CardType.SelectedValue + "\" CardNumber=\"" + CardNumber.Text + "\" ExpireDate=\"" + ExpireMonth.SelectedValue + ExpireYear.SelectedValue + "\" SeriesCode = \""+ CVV.Text +"\"> " +
                "      <PaymentCard CardType=\"1\" CardCode=\"" + CardType.SelectedValue + "\" CardNumber=\"" + CardNumber.Text + "\" ExpireDate=\"" + ExpireMonth.SelectedValue + ExpireYear.SelectedValue + "\" CVV=\"" + CVV.Text + "\" Name=\"" + NameOnCard.Text + "\">" +
                //"<CardHolderName>" + NameOnCard.Text + "</CardHolderName>" +
                "    </PaymentCard> " +
                "  </RentalPaymentPref> " +
                //"    <Reference Type=\"16\" ID=\"" + _selectedCar.HertzReference + "\" ID_Context=\"CARTRAWLER\" DateTime=\"2016-02-08T07:52:17.827Z\" URL=\"95ff8c22-9027-40e8-8195-9dcc8056f7bb.63\"> " +
                "    <Reference Type=\"16\" ID=\"" + _selectedCar.HertzReference + "\"> " +
                "  </Reference> " +
                "    <WrittenConfInst LanguageID=\"" + Country.SelectedValue + "\" ConfirmInd=\"true\"> " +
                "      <Email DefaultInd=\"true\">" + Email.Text + "</Email> " +
                "    </WrittenConfInst>   " +
                "</VehResRQInfo> " +
                "</OTA_VehResRQ>";

            string CTthrifBook = "<OTA_VehResRQ xmlns=\"http://www.opentravel.org/OTA/2003/05\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.opentravel.org/OTA/2003/05 OTA_VehResRQ.xsd\" Target=\"Test\" Version=\"1.005\">" +
                                       "<POS>" +
                                       "<Source ISOCountry=\"" + _booking.CountryOfResidenceCode + "\" AgentDutyCode=\"" + _booking.agentDutyCode + "\">" +
                                       "<RequestorID Type=\"16\" ID=\"621233\" ID_Context=\"CARTRAWLER\" />" +
                                       "</Source>" +
                                       "<Source>" +
                                       "<RequestorID Type=\"16\" ID=\"621233\" ID_Context=\"ORDERID\" />" +
                                       "</Source>" +
                                       "</POS>" +
                                       "<VehResRQCore Status=\"All\">" +
                                       "<VehRentalCore PickUpDateTime=\"" + DateTimeForXml(_booking.PickupDateTime) + "\" ReturnDateTime=\"" + DateTimeForXml(_booking.DropOffDateTime) + "\">" +
                                       "<PickUpLocation CodeContext=\"CARTRAWLER\" LocationCode=\"" + _booking.PickUpLocationCode + "\"/>" +
                                       "<ReturnLocation CodeContext=\"CARTRAWLER\" LocationCode=\"" + _booking.DropOffLocationCode + "\"/>" +
                                       "</VehRentalCore>" +
                                       "<Customer>" +
                                       "<Primary>" +
                                       "<PersonName>" +
                                       "<NamePrefix>Mr.</NamePrefix>" +
                                       "<GivenName>" + FirstName.Text + "</GivenName>" +
                                       "<Surname>" + LastName.Text + "</Surname>" +
                                       "</PersonName>" +
                                       "<Telephone PhoneTechType=\"1\" AreaCityCode=\"405\" PhoneNumber=\"" + TelephoneNumber.Text + "\"/>" +
                                       "<Email EmailType=\"2\">" + Email.Text + "</Email>" +
                                       "<Address Type=\"2\">" +
                                       "<AddressLine>" + Address1.Text + ", " + Address2.Text + ", " + Town.Text + "</AddressLine>" +
                                       "<CountryName Code=\"" + Country.SelectedValue + "\" />" +
                                       "</Address>" +
                                       "<CitizenCountryName Code=\"" + _booking.CountryOfResidenceCode + "\" />" +
                                       "</Primary>" +
                                       "</Customer>" +
                                       "<DriverType Age=\"30\"/>" +
                                       "<RateQualifier ArriveByFlight=\"false\" CorpDiscountNmbr=\"709295\" RateQualifier=\"" + setRQ(_selectedbrand.ToString()) + "\"> " +
                                       "  </RateQualifier> " +
                                       "</VehResRQCore>" +
                                       "<VehResRQInfo PassengerQty=\"3\">" +
                                       "<ArrivalDetails TransportationCode=\"14\" Number=\"123\">" +
                                       "<OperatingCompany>EI</OperatingCompany>" +
                                       "</ArrivalDetails>" +
                                       "<RentalPaymentPref>" +
                                       "<PaymentCard CardType=\"1\" CardCode=\"" + CardType.SelectedValue + "\" CardNumber=\"" + CardNumber.Text + "\" ExpireDate=\"" + ExpireMonth.SelectedValue + ExpireYear.SelectedValue + "\" SeriesCode=\"123\">" +
                                       "<CardHolderName>" + NameOnCard.Text + "</CardHolderName>" +
                                       "</PaymentCard>" +
                                       "</RentalPaymentPref>" +
                                       "<Reference Type=\"16\" ID=\"" + _selectedCar.HertzReference + "\" ID_Context=\"CARTRAWLER\" DateTime=\"2016-02-08T05:44:23.062Z\" URL=\"da213ddd-60e2-42e0-8129-6752f72c9ee5.63\"/>" +
                //"<Reference Type=\"16\" ID=\"222:1712\" ID_Context=\"CARTRAWLER\" DateTime=\"2016-02-27T14:45:14.156Z\" URL=\"CTDyFJ8dMeoRNFrPCii%2FZSXjoHopVS40JWX0KQrrQwGxvnBHPT%2FT5xGw4sAK%2BF%2FMUzpChnLZGLvuY1%0AFhMhl5T42fyhqTGz5UccbLN%2BNq94cXuTrs9FDHwvBXQer5ujMZm3EVyzb7bWSzoGm5WtaXTtf4ux%0AvvALaJg9\"/>"+
                                       "<TPA_Extensions>" +
                                       "<CompanyName VAT=\"98765\">Cartrawler Ltd</CompanyName>" +
                //"<Reference Type=\"16\" ID=\"CTWIEST01IE\" ID_Context=\"INSURANCE\" Amount=\"22.00\" CurrencyCode=\"EUR\" URL=\"http://193.58.223.118/CTW/IE/EN/CTW_IE_en_TCs.pdf\"/>"+
                                       "</TPA_Extensions>" +
                                       "</VehResRQInfo>" +
                                       "</OTA_VehResRQ>";


            if (_selectedbrand == CarProvider.Thrifty)
            {
                bookingResponse = SendXml.HttpPost("http://" + CMSSettings.SiteUrl + "/thrifty", bookingreq, _selectedbrand.ToString(), "OTA_VehResRQ").ToString();
            }
            else if (_selectedbrand == CarProvider.Hertz)
            {
                bookingResponse = SendXml.HttpPost("http://" + CMSSettings.SiteUrl + "/hertz", bookingreq, _selectedbrand.ToString(), "OTA_VehResRQ").ToString();
            }
            else if (_selectedbrand == CarProvider.Dollar)
            {
                bookingResponse = SendXml.HttpPost("http://" + CMSSettings.SiteUrl + "/dollar", bookingreq, _selectedbrand.ToString(), "OTA_VehResRQ").ToString();
            }
            else if (_selectedbrand == CarProvider.FireFly)
            {
                bookingResponse = SendXml.HttpPost("http://" + CMSSettings.SiteUrl + "/firefly", bookingreq, _selectedbrand.ToString(), "OTA_VehResRQ").ToString();
            }

        }
        var xmlDoc = new XmlDocument();
        xmlDoc.LoadXml(bookingResponse);

        var namespaceMgr = new XmlNamespaceManager(xmlDoc.NameTable);
        namespaceMgr.AddNamespace("a1", "http://www.opentravel.org/OTA/2003/05");

        _booking.ErrorDetails.ErrorMsg = "";
        _booking.ErrorDetails.ErrorCode = "";

        //if (_booking.Provider.ToString() == "4")
        //{          
            var CTerrorNode = xmlDoc.SelectNodes("//OTA_ErrorRS");

            if (CTerrorNode != null)
            {
                foreach (XmlNode node in CTerrorNode)
                {
                    if (node != null)
                    {
                        if (node.Attributes != null)
                        {
                            _booking.ErrorDetails.ErrorMsg += node.Attributes["ErrorMessage"].Value + "</br>";
                            try
                            {
                                _booking.ErrorDetails.ErrorCode = node.Attributes["ErrorCode"].Value;
                            }
                            catch
                            {

                            }
                        }
                    }

                }

                XmlNode errorNode = xmlDoc.DocumentElement.SelectSingleNode("//a1:Errors", namespaceMgr);
                if (errorNode != null)
                {
                    XmlNodeList errorList = xmlDoc.DocumentElement.SelectNodes(".//a1:Error", namespaceMgr);

                    foreach (XmlNode node in errorList)
                    {
                        if (node != null)
                        {
                            if (node.Attributes != null)
                            {
                                _booking.ErrorDetails.ErrorMsg += node.Attributes["ShortText"].Value + "</br>";
                                try
                                {
                                    if (node.Attributes["Type"] != null)
                                    {
                                        _booking.ErrorDetails.ErrorCode = node.Attributes["Type"].Value;
                                    }
                                    else
                                    {
                                        _booking.ErrorDetails.ErrorCode = node.Attributes["Code"].Value;
                                    }
                                }
                                catch
                                {

                                }

                            }
                        }

                    }
                }
            }
            else
            {
                RemoteDebugger.SendXmlForDebugging("", "Test");
                XmlNode errorNode = xmlDoc.DocumentElement.SelectSingleNode("//a1:Errors", namespaceMgr);
                if (errorNode != null)
                {
                    XmlNodeList errorList = errorNode.SelectNodes(".//a1:Error", namespaceMgr);

                    foreach (XmlNode node in errorList)
                    {
                        if (node != null)
                        {
                            if (node.Attributes != null)
                            {
                                _booking.ErrorDetails.ErrorMsg += node.Attributes["ShortText"].Value + "</br>";
                                try
                                {
                                    if (node.Attributes["Type"] != null)
                                    {
                                        _booking.ErrorDetails.ErrorCode = node.Attributes["Type"].Value;
                                    }
                                    else
                                    {
                                        _booking.ErrorDetails.ErrorCode = node.Attributes["Code"].Value;
                                    }
                                }
                                catch
                                {

                                }

                            }
                        }

                    }
                }
            }
        //}
        //else
        //{
            XmlNode errorNode1 = xmlDoc.DocumentElement.SelectSingleNode("//a1:Errors", namespaceMgr);
            if (errorNode1 != null)
            {
                XmlNodeList errorList = errorNode1.SelectNodes(".//a1:Error", namespaceMgr);

                foreach (XmlNode node in errorList)
                {
                    if (node != null)
                    {
                        if (node.Attributes != null)
                        {
                            _booking.ErrorDetails.ErrorMsg += node.Attributes["ShortText"].Value + "</br>";
                            try
                            {
                                if (node.Attributes["Type"] != null)
                                {
                                    _booking.ErrorDetails.ErrorCode = node.Attributes["Type"].Value;
                                }
                                else
                                {
                                    _booking.ErrorDetails.ErrorCode = node.Attributes["Code"].Value;
                                }
                            }
                            catch
                            {

                            }

                        }
                    }

                }
            }

        //}

        string Id;

        XmlNode ConfId = xmlDoc.DocumentElement.SelectSingleNode(".//a1:ConfID", namespaceMgr);
        if (ConfId != null)
        {
            Id = ConfId.Attributes["ID"].Value;
        }
        else
        {
            Id = "Null";
        }

        //  Response.Write("Country = " + _booking.Country);
        _booking.Session = _booking;
        //   Response.Write("Country 2 = " + _booking.Country);

        if (!string.IsNullOrEmpty(_booking.ErrorDetails.ErrorCode))
        {
            ValidationMessage.Visible = true;
            AlertMessage.Text = _booking.ErrorDetails.ErrorMsg;
        }
        else
        {
            ValidationMessage.Visible = true;
            AlertMessage.Text = "Your Booking Completed Successfully</br>Your Booking Cofirmation Id = " + Id;
        }

    }

    private string setRQ(string brand)
    {
        if (brand == CarProvider.Hertz.ToString())
        {
            return _booking.hertzRQ.ToString();
        }
        else if (brand == CarProvider.Thrifty.ToString())
        {
            return _booking.thriftyRQ.ToString();
        }
        else if(brand == CarProvider.FireFly.ToString())
        {
            return _booking.fireflyRQ.ToString();
        }
        return "";
    }
    protected void ordernowBtn_Clicked(object sender, EventArgs e)
    {
        //string bookingReq = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><OTA_VehResRQ xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns=\"http://www.opentravel.org/OTA/2003/05\" xsi:schemaLocation=\"http://www.opentravel.org/OTA/2003/05 OTA_VehResRQ.xsd\" Version=\"1.008\"><POS><Source ISOCountry=\"IE\" AgentDutyCode=\"R18Y25A114N\"><RequestorID Type=\"4\" ID=\"T390\"><CompanyName Code=\"CP\" CodeContext=\"XM77\"></CompanyName></RequestorID></Source></POS><VehResRQCore Status=\"All\"><VehRentalCore PickUpDateTime=\"2014-08-16T06:00:00\" ReturnDateTime=\"2014-08-19T23:00:00\"><PickUpLocation LocationCode=\"DUB       \" CodeContext=\"IATA\"></PickUpLocation><ReturnLocation LocationCode=\"DUB       \" CodeContext=\"IATA\"></ReturnLocation></VehRentalCore><Customer><Primary><PersonName><GivenName>Hashan</GivenName><Surname>Madushanka</Surname></PersonName><Telephone PhoneTechType=\"1\" AreaCityCode=\"4\" PhoneNumber=\"009478078549\" FormattedInd=\"false\" DefaultInd=\"false\"></Telephone><Email DefaultInd=\"false\">hashan41@gmail.com</Email><Address DefaultInd=\"false\" FormattedInd=\"false\"><AddressLine>Address 1, Address 2</AddressLine><CityName>Colombo</CityName><PostalCode>10230</PostalCode><CountryName Code=\"CH\"></CountryName></Address></Primary></Customer><RateQualifier ArriveByFlight=\"false\" CorpDiscountNmbr=\"709295\" RateQualifier=\"RTG\"></RateQualifier><SpecialEquipPrefs xmlns=\"http://www.opentravel.org/OTA/2003/05\"><SpecialEquipPref EquipType=\"13\" Quantity=\"1\" /><SpecialEquipPref EquipType=\"8\" Quantity=\"1\" /></SpecialEquipPrefs></VehResRQCore><VehResRQInfo GasPrePay=\"false\" SmokingAllowed=\"false\"><ArrivalDetails TransportationCode=\"14\" Number=\"1931\"><OperatingCompany Code=\"FR\"></OperatingCompany></ArrivalDetails><RentalPaymentPref><PaymentCard CardType=\"1\" CardCode=\"VI\" CardNumber=\"4012000033330026\" ExpireDate=\"1214\"></PaymentCard></RentalPaymentPref><Reference Type=\"16\" ID=\"MCMWMLREO820194-2501\"></Reference><WrittenConfInst LanguageID=\"ENUS\" ConfirmInd=\"true\"><Email DefaultInd=\"true\">hashan41@gmail.com</Email></WrittenConfInst></VehResRQInfo></OTA_VehResRQ> ";

        //string bookingRes = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><OTA_VehResRS xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns=\"http://www.opentravel.org/OTA/2003/05\" Target=\"Qual\" Version=\"2.007\"><Success></Success><Warnings><Warning Type=\"1\" ShortText=\"RATE REQUIRES AIRLINE TICKET                                   \" RecordID=\"188\"></Warning><Warning Type=\"1\" ShortText=\"NEVERLOST NAVIGATIONAL EQUIPMENT HAS BEEN CONFIRMED            \" RecordID=\"243\"></Warning><Warning Type=\"1\" ShortText=\"QUALIFIERS CHANGED AT TIME OF SELL - NEW RATE RETURNED         \" RecordID=\"276\"></Warning><Warning Type=\"1\" ShortText=\"CANCEL OR NO SHOW FEE MAY APPLY FOR THIS PREPAID RENTAL        \" RecordID=\"551\"></Warning><Warning Type=\"1\" ShortText=\"THE MAXIMUM RENTAL AGE IS 75 YEARS                             \" RecordID=\"605\"></Warning><Warning Type=\"1\" ShortText=\"THE MINIMUM RENTAL AGE IS 25 YEARS, SOME EXCEPTIONS MAY APPLY  \" RecordID=\"651\"></Warning><Warning Type=\"1\" ShortText=\"CHECK DRIVING RESTRICTIONS FOR TOURING EUROPE                  \" RecordID=\"697\"></Warning><Warning Type=\"1\" ShortText=\"RATE MAY BE SUBJECT TO VERIFICATION OF ID                      \" RecordID=\"792\"></Warning></Warnings><VehResRSCore><VehReservation><Customer><Primary><PersonName><GivenName>HASHAN</GivenName><Surname>MADUSHANKA</Surname></PersonName></Primary></Customer><VehSegmentCore><ConfID Type=\"14\" ID=\"G295A186559    \"></ConfID><Vendor Code=\"ZT\"></Vendor><VehRentalCore PickUpDateTime=\"2014-08-16T06:00:00-05:00\" ReturnDateTime=\"2014-08-19T23:00:00-05:00\"><PickUpLocation ExtendedLocationCode=\"DUBT52\" LocationCode=\"DUB\" CodeContext=\"IATA\"></PickUpLocation><ReturnLocation ExtendedLocationCode=\"DUBT52\" LocationCode=\"DUB\" CodeContext=\"IATA\"></ReturnLocation></VehRentalCore><Vehicle PassengerQuantity=\"5\" BaggageQuantity=\"3\" AirConditionInd=\"false\" TransmissionType=\"Automatic\" FuelType=\"Unspecified\" DriveType=\"Unspecified\" Code=\"CCAN\" CodeContext=\"SIPP\"><VehType VehicleCategory=\"1\"></VehType><VehClass Size=\"4\"></VehClass><VehMakeModel Name=\"F MAZDA 3 OR SIMILAR\" Code=\"CCAN\"></VehMakeModel><PictureURL>ZEIECCAN999.jpg</PictureURL></Vehicle><RentalRate><RateDistance Unlimited=\"true\" DistUnitName=\"Km\" VehiclePeriodUnitName=\"RentalPeriod\"></RateDistance><VehicleCharges><VehicleCharge Purpose=\"1\" TaxInclusive=\"false\" GuaranteedInd=\"true\" IncludedInRate=\"false\" Amount=\"147.72\" CurrencyCode=\"EUR\"><TaxAmounts><TaxAmount Total=\"35.35\" CurrencyCode=\"EUR\" Percentage=\"13.50\" Description=\"Tax\"></TaxAmount></TaxAmounts><Calculation UnitCharge=\"36.93\" UnitName=\"Day\" Quantity=\"4\"></Calculation></VehicleCharge><VehicleCharge Purpose=\"23\" TaxInclusive=\"false\" GuaranteedInd=\"false\" IncludedInRate=\"false\" Amount=\"103.56\" CurrencyCode=\"EUR\"></VehicleCharge></VehicleCharges><RateQualifier ArriveByFlight=\"false\" RateQualifier=\"RTG2EU\"></RateQualifier></RentalRate><PricedEquips><PricedEquip Required=\"false\"><Equipment EquipType=\"8\" Quantity=\"1\"></Equipment><Charge TaxInclusive=\"false\" IncludedInRate=\"false\" Amount=\"35.24\" CurrencyCode=\"EUR\"></Charge></PricedEquip><PricedEquip Required=\"false\"><Equipment EquipType=\"13\" Quantity=\"1\"></Equipment><Charge TaxInclusive=\"false\" IncludedInRate=\"false\" Amount=\"56.00\" CurrencyCode=\"EUR\"></Charge></PricedEquip></PricedEquips><Fees><Fee Purpose=\"5\" TaxInclusive=\"false\" Description=\"LOCATION SERVICE CHARGE:\" IncludedInRate=\"false\" Amount=\"22.91\" CurrencyCode=\"EUR\"></Fee></Fees><TotalCharge RateTotalAmount=\"147.72\" EstimatedTotalAmount=\"357.22\" CurrencyCode=\"EUR\"></TotalCharge></VehSegmentCore><VehSegmentInfo><PaymentRules><PaymentRule RuleType=\"2\" Amount=\"253.66\" CurrencyCode=\"EUR\"></PaymentRule></PaymentRules><PricedCoverages><PricedCoverage Required=\"true\"><Coverage CoverageType=\"7\"></Coverage><Charge TaxInclusive=\"false\" IncludedInRate=\"false\" Amount=\"44.00\" CurrencyCode=\"EUR\"></Charge></PricedCoverage><PricedCoverage Required=\"false\"><Coverage CoverageType=\"38\"></Coverage><Charge TaxInclusive=\"false\" IncludedInRate=\"false\" CurrencyCode=\"EUR\"><Calculation UnitCharge=\"5.50\" UnitName=\"Day\" Quantity=\"1\"></Calculation></Charge></PricedCoverage><PricedCoverage Required=\"false\"><Coverage CoverageType=\"59\"></Coverage><Charge TaxInclusive=\"false\" IncludedInRate=\"false\" CurrencyCode=\"EUR\"><Calculation UnitCharge=\"16.00\" UnitName=\"Day\" Quantity=\"1\"></Calculation></Charge></PricedCoverage><PricedCoverage Required=\"true\"><Coverage CoverageType=\"48\"></Coverage><Charge TaxInclusive=\"false\" IncludedInRate=\"false\" Amount=\"16.00\" CurrencyCode=\"EUR\"></Charge></PricedCoverage></PricedCoverages><LocationDetails Code=\"DUB\" Name=\"DUBLIN AIRPORT                \" ExtendedLocationCode=\"DUBT52\"><Address DefaultInd=\"false\" FormattedInd=\"false\"><AddressLine>DUBLIN AIRPORT T1/T2          </AddressLine><AddressLine>DUBLIN AIRPORT                </AddressLine><CityName>DUBLIN                        </CityName><PostalCode>000000000  </PostalCode><CountryName Code=\"IE\">IRELAND                       </CountryName></Address><Telephone PhoneLocationType=\"4\" PhoneTechType=\"1\" PhoneNumber=\"01 8445466          \" FormattedInd=\"false\" DefaultInd=\"false\"></Telephone><AdditionalInfo><OperationSchedules><OperationSchedule><OperationTimes><OperationTime Text=\"MO-SU 0500-0100\"></OperationTime><OperationTime Mon=\"true\" Start=\"00:01\" End=\"01:00\"></OperationTime><OperationTime Mon=\"true\" Start=\"05:00\" End=\"23:59\"></OperationTime><OperationTime Tue=\"true\" Start=\"00:01\" End=\"01:00\"></OperationTime><OperationTime Tue=\"true\" Start=\"05:00\" End=\"23:59\"></OperationTime><OperationTime Weds=\"true\" Start=\"00:01\" End=\"01:00\"></OperationTime><OperationTime Weds=\"true\" Start=\"05:00\" End=\"23:59\"></OperationTime><OperationTime Thur=\"true\" Start=\"00:01\" End=\"01:00\"></OperationTime><OperationTime Thur=\"true\" Start=\"05:00\" End=\"23:59\"></OperationTime><OperationTime Fri=\"true\" Start=\"00:01\" End=\"01:00\"></OperationTime><OperationTime Fri=\"true\" Start=\"05:00\" End=\"23:59\"></OperationTime><OperationTime Sat=\"true\" Start=\"00:01\" End=\"01:00\"></OperationTime><OperationTime Sat=\"true\" Start=\"05:00\" End=\"23:59\"></OperationTime><OperationTime Sun=\"true\" Start=\"00:01\" End=\"01:00\"></OperationTime><OperationTime Sun=\"true\" Start=\"05:00\" End=\"23:59\"></OperationTime></OperationTimes></OperationSchedule></OperationSchedules></AdditionalInfo></LocationDetails></VehSegmentInfo></VehReservation></VehResRSCore></OTA_VehResRS> ";
        //CommonTasks.SaveBookingInDatabaseAndSendEmail(bookingRes, bookingReq, CarProvider.Thrifty);

    }
    // Hertz XML expects yyyy-mm-ddThh:mm:ss
    public string DateTimeForXml(DateTime date)
    {
        return date.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss");
    }
    public XElement CarAccessories()
    {
        XElement accessoriesXml = null;

        if (_booking.Accessories.Any(a => a.IsSelected && !a.Delete))
        {
            accessoriesXml = new XElement(XmlNamespace + "SpecialEquipPrefs");

            // loop through the accessories and build up the XML
            foreach (var accessory in _booking.Accessories.Where(a => a.IsSelected && !a.Delete))
            {
                var accessoryElement = new XElement(XmlNamespace + "SpecialEquipPref",
                                                         new XAttribute("EquipType", accessory.EquipType),
                                                         new XAttribute("Quantity", accessory.Quantity)
                    );
                accessoriesXml.Add(accessoryElement);
            }
        }
        return accessoriesXml;
    }
}